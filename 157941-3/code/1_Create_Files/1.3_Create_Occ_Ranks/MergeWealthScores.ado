program MergeWealthScores

	syntax, occ1950(varname) file_narrow(string) file_broad(string) gen(string) cutoff(string) region(varname)
	
	quietly {
	
		tempvar STH N occbroad totpropb
		
		preserve
		use `file_narrow', clear
		rename occ1950 `occ1950'
		rename STH `STH'
		rename totprop `gen'
		drop occ1950broad
		rename N `N'
		tempfile target_narrow
		save `target_narrow'
		
		use `file_broad', clear
		rename STH `STH'
		rename occ1950broad `occbroad'
		rename totpropb `gen'
		drop Nb
		tempfile target_broad
		save `target_broad'
		restore
		
		gen `STH'=`region'
		tempvar merge1
		merge m:1 `occ1950' `STH' using `target_narrow', keep(1 3) keepusing(`gen' `N') gen(`merge1')
		replace `gen'=. if `N'<`cutoff'
		
		gen `occbroad'=1 if `occ1950'<=99
		replace `occbroad'=2 if `occ1950'>=100 & `occ1950'<=123
		replace `occbroad'=3 if `occ1950'>=200 & `occ1950'<=290
		replace `occbroad'=4 if `occ1950'>=300 & `occ1950'<=390
		replace `occbroad'=5 if `occ1950'>=400 & `occ1950'<=490
		replace `occbroad'=6 if `occ1950'>=500 & `occ1950'<=595
		replace `occbroad'=7 if `occ1950'>=600 & `occ1950'<=690
		replace `occbroad'=8 if `occ1950'>=700 & `occ1950'<=720 
		replace `occbroad'=9 if `occ1950'>=730 & `occ1950'<=790 
		replace `occbroad'=10 if `occ1950'>=800 & `occ1950'<=840
		replace `occbroad'=11 if `occ1950'>=900 & `occ1950'<=970
		
		tempvar merge2
		merge m:1 `occbroad' `STH' using `target_broad', keep(1 3 4 5) update keepusing(`gen') gen(`merge2')
		
	}	
end
