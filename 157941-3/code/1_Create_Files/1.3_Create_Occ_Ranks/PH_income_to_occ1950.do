cd "$repfolder/code/1_Create_Files/1.3_Create_Occ_Ranks"

clear

do ip19002.do

gen code=OCC
 
merge m:1 code using "$repfolder/data/raw/income_score_1900/PH_income_assign.dta"
notes drop _all

replace inc1900=. if code==16 | code==17
replace inc1900=. if OCC1950==100

replace inc1900=584 if OCC1950==100 

keep if SEX==1 

keep if code<961

sum inc1900, detail

sum inc1900 if OCC1950==100
sum inc1900 if OCC1950==690

collapse inc1900 (count) N=inc1900 [w=PERWT], by(OCC1950)

rename OCC1950 occ1950

drop if occ1950>970

save "$repfolder/data/analysis/PH_inc_occ1950.dta", replace






