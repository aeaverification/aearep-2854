cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1850_100pct", clear
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=18 & age<=30 & race==1 & (region<30 | region>=40)

preserve
use "$repfolder/data/confidential/clp_linkage_crosswalks/crosswalk_1850_1880", clear
rename histid_1850 histid_50
rename histid_1880 histid_80
replace histid_50=upper(histid_50)
replace histid_80=upper(histid_80)
keep if abe_exact_conservative==1 & abe_nysiis_conservative==1
keep histid*
merge 1:1 histid* using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1850_1880"
keep if _merge==3
drop _merge
tempfile crosswalk
save `crosswalk'
restore

gen histid_50=upper(histid)
merge 1:1 histid_50 using `crosswalk', keep(1 3)
gen linked=(histid_80~="")
drop _merge histid

keep if linked==1
	
order *, alpha
order histid*, first
foreach x of varlist age-yngch {
	rename `x' `x'_50
}

preserve
use  "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_80=upper(histid)
drop histid
keep if sex==1 & (region<30 | region>40)
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_80
}
tempfile census80
save `census80'
restore
merge 1:1 histid_80 using `census80', keep(3)
drop _merge

		//Require detailed birthplace match
		drop if bpld_50~=bpld_80
	
		//Require race match
		drop if race_50~=race_80
	
		//Drop those who "forget" how to read
		drop if lit_50==4 & lit_80==1
	
keep histid*
compress
save crosswalk_1850_1880_IntPlus, replace
