cd "$repfolder/data/analysis"

//Set the seed for replicability
set seed 210914

//Import 1870 census
use "$repfolder/data/confidential/ipums_data/1870_100pct", clear

//Determine who is in a farmer-headed household
preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
gen realprop_clean=realprop if realprop<999998
collapse (max) isfarm realprop_clean, by(serial)
gen headhasprop=(realprop>0) if realprop_clean~=.
drop realprop_clean
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge

gen farmer=(occ1950>=100 & occ1950<200)
gen hasprop=(realprop>0) if realprop<999998

//Probit regressions to predict classification
probit farmer i.age i.relate i.region i.bpl i.numperhh i.pernum i.nchild i.nsibs i.urban i.gq c.hasprop##c.headhasprop if age>=18 & age<=65 & sex==1 & (region<30 | region>=40) & ((occ1950>=100 & occ1950<200) | (occ1950>=800 & occ1950<900)) & race==1 & isfarm==1 & relate>1 & bpl<100
est save "$repfolder/data/analysis/RecatNative1850", replace

probit farmer i.age i.relate i.region i.bpl i.numperhh i.pernum i.nchild i.nsibs i.urban i.gq c.hasprop##c.headhasprop if age>=18 & age<=65 & sex==1 & (region<30 | region>=40) & ((occ1950>=100 & occ1950<200) | (occ1950>=800 & occ1950<900)) & race==1 & isfarm==1 & relate>1 & bpl>=100
est save "$repfolder/data/analysis/RecatForeign1850", replace

//Import 1850 census
use "$repfolder/data/confidential/ipums_data/1850_100pct", clear

//Determine who is in a farmer-headed household
preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
gen realprop_clean=realprop if realprop<999998
collapse (max) isfarm realprop_clean, by(serial)
gen headhasprop=(realprop>0) if realprop_clean~=.
drop realprop_clean
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge

gen farmer=(occ1950>=100 & occ1950<200)
gen hasprop=(realprop>0) if realprop<999998

//Apply the estimates from 1870 to 1850
est use "$repfolder/data/analysis/RecatNative1850"
predict fprob if age>=18 & age<=65 & sex==1 & (region<30 | region>=40) & ((occ1950>=100 & occ1950<200) | (occ1950>=800 & occ1950<900)) & race==1 & isfarm==1 & relate>1 & bpl<100

est use "$repfolder/data/analysis/RecatForeign1850"
predict fprob_temp if age>=18 & age<=65 & sex==1 & (region<30 | region>=40) & ((occ1950>=100 & occ1950<200) | (occ1950>=800 & occ1950<900)) & race==1 & isfarm==1 & relate>1 & bpl>=100
replace fprob=fprob_temp if fprob==. & fprob_temp~=.

//Randomly assign 1850 classifications based on estimated probabilities
gen random=runiform()
keep if fprob~=.
keep histid fprob random occ1950
gen occ1950_alt=820 if random>=fprob & fprob~=.
replace occ1950_alt=100 if random<fprob & fprob~=.

//Save
keep histid occ1950_alt
rename occ1950_alt occ1950
replace histid=upper(histid)
rename histid histid_50
compress
save RecatProbit1850, replace
