cd "$repfolder/data/analysis"

//Load 1910 census
use  "$repfolder/data/confidential/ipums_data/1910_100pct", clear
replace occ1950=820 if occ1950==830

//Limit the sample to non-southern white men aged 44-64 in the latter year of the sample
keep if sex==1 & age>=44 & age<=64 & race==1 & (region<30 | region>=40)

gen histid_10=upper(histid)

//Merge with linkage crosswalk
merge 1:1 histid_10 using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1880_1910", keep(1 3)
gen linked=(histid_80~="")
drop _merge histid

preserve
//Load 1880 census
use  "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_80=upper(histid)
drop histid
keep if sex==1
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_80
}
tempfile census80
save `census80'
restore

//Limiting successful links to those aged 18-30 in the initial year
rename age age_10
merge m:1 histid_80 using `census80', keep(1 3) keepusing(age_80)
drop _merge
replace linked=0 if linked==1 & (age_80<18 | age_80>30)
replace histid_80="" if linked==0 & histid_80~=""
drop age_80
rename age_10 age

//Merge in occupational ranks
merge 1:1 histid_10 using OccRanks_1910, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

//Limit the sample to nationalities that we attempted to link
keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

//Limit the sample at risk for linkage to those in the US by 1880
drop if linked==0 & foreign==1 & yrimmig>1880

//Export linkage statistics
count if foreign==0
	global all_n=r(N)
count if linked==1 & foreign==0
	global link_n=r(N)

count if foreign==1
	global all_f=r(N)
count if linked==1 & foreign==1
	global link_f=r(N)
	
capture texdoc close
texdoc init "$repfolder/results/TableB1", append force

global fullflinked_n: di %4.3f $link_n / $all_n
global fullflinked_f: di %4.3f $link_f / $all_f

global all_n: di %10.0fc $all_n
global link_n: di %10.0fc $link_n
global all_f: di %10.0fc $all_f
global searched_f: di %10.0fc $searched_f
global link_f: di %10.0fc $link_f

tex 1880--1910 & $all_n & $link_n & $all_f & $link_f \\
tex & & ($fullflinked_n) & & ($fullflinked_f) \\
tex \addlinespace[2pt]

texdoc close
	
foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
	tab bpl if bpl==`x'
	count if bpl==`x'
		local all=r(N)
		global all`x'=r(N)
	count if linked==1 & bpl==`x'
		global link`x'=r(N)
		local link=r(N)
	di `link' / `all'
}

tab bpl if bpl>=410 & bpl<=413
count if bpl>=410 & bpl<=413
	global alluk=r(N)
	local alluk=r(N)
count if linked==1 & bpl>=410 & bpl<=413
	global linkuk=r(N)
	local link=r(N)
di `link' / `alluk'

capture texdoc close
texdoc init "$repfolder/results/TableB2", append force

global flinked_all404: di %4.3f $link404 / $all404
global all404: di %9.0fc $all404
global link404: di %9.0fc $link404

global flinked_all405: di %4.3f $link405 / $all405
global all405: di %9.0fc $all405
global link405: di %9.0fc $link405

global flinked_alluk: di %4.3f $linkuk / $alluk
global alluk: di %9.0fc $alluk
global linkuk: di %9.0fc $linkuk

global flinked_all414: di %4.3f $link414 / $all414
global all414: di %9.0fc $all414
global link414: di %9.0fc $link414

global flinked_all421: di %4.3f $link421 / $all421
global all421: di %9.0fc $all421
global link421: di %9.0fc $link421

global flinked_all425: di %4.3f $link425 / $all425
global all425: di %9.0fc $all425
global link425: di %9.0fc $link425

global flinked_all426: di %4.3f $link426 / $all426
global all426: di %9.0fc $all426
global link426: di %9.0fc $link426

global flinked_all434: di %4.3f $link434 / $all434
global all434: di %9.0fc $all434
global link434: di %9.0fc $link434

global flinked_all450: di %4.3f $link450 / $all450
global all450: di %9.0fc $all450
global link450: di %9.0fc $link450

global flinked_all453: di %4.3f $link453 / $all453
global all453: di %9.0fc $all453
global link453: di %9.0fc $link453

global flinked_all455: di %4.3f $link455 / $all455
global all455: di %9.0fc $all455
global link455: di %9.0fc $link455

global flinked_all465: di %4.3f $link465 / $all465
global all465: di %9.0fc $all465
global link465: di %9.0fc $link465

tex 1880--1910 & Start & $all404 & $all405 & $alluk & $all414 & $all421 & $all425 & $all426 & $all434 & $all450 & $all453 & $all455 & $all465 \\
tex & Linked & $link404 & $link405 & $linkuk & $link414 & $link421 & $link425 & $link426 & $link434 & $link450 & $link453 & $link455 & $link465 \\
tex & Link Share & ($flinked_all404) & ($flinked_all405) & ($flinked_alluk) & ($flinked_all414) & ($flinked_all421) & ($flinked_all425) & ($flinked_all426) & ($flinked_all434) & ($flinked_all450) & ($flinked_all453) & ($flinked_all455) & ($flinked_all465) \\
tex \addlinespace[2pt]

texdoc close

//Create variables
gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850) if occ1950<=970

gen urban_clean=(urban==2) if urban~=.
gen farm_clean=(farm~=1)
gen lit_clean=(lit==4)

gen ffor=1-(fbpl<100)
gen english=(speakeng==2 | (speakeng==0 & foreign==0))
gen own_mort=(ownershp==1 & mortgage==3)
gen own_free=(ownershp==1 & mortgage==1)

gen marst_clean=(marst==1 | marst==2)

gen head=(relate==1)

//Output data to create graphs of representativeness of linked data on observables
preserve
collapse age urban_clean farm_clean lit_clean wc farmer craft operative unskill numprec ffor english own_mort own_free marst_clean proprank proprank_age proprank_state phrank proprank_impute proprank_age_impute proprank_state_impute phrank_impute head proprank_midpoint proprank_upper phrank_midpoint phrank_upper, by(foreign)
gen linked=0
compress
save ObservablesLinkage_1880_1910, replace
restore, preserve
keep if linked==1
collapse age urban_clean farm_clean lit_clean wc farmer craft operative unskill numprec ffor english own_mort own_free marst_clean proprank proprank_age proprank_state phrank proprank_impute proprank_age_impute proprank_state_impute phrank_impute head proprank_midpoint proprank_upper phrank_midpoint phrank_upper, by(foreign)
gen linked=1
append using ObservablesLinkage_1880_1910
compress
save ObservablesLinkage_1880_1910, replace
restore

if $simple == 0 {

	//Create linkage weights, farm family equivalent to farm labor
	gen link_prob=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob=prob if link_prob==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob

	//Create linkage weights, midpoint ranking	
	gen link_prob_midpoint=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob

	//Create linkage weights, farm family equivalent to farmers
	gen link_prob_upper=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob_upper=prob if link_prob_upper==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob

}

//Keep only linked individuals
keep if linked==1
	
order *, alpha
order histid*, first
foreach x of varlist age-yrsusa2 {
	rename `x' `x'_10
}

preserve
use `census80', clear
keep if region_80<30 | region_80>=40
save `census80', replace
restore

//Merge in 1880 data
merge 1:1 histid_80 using `census80', keep(3)
drop _merge

keep if race_80==1

merge 1:1 histid_80 using OccRanks_1880, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

foreach x of varlist lntotprop proprank lntotprop_age proprank_age lntotprop_state proprank_state inc1900 l_inc1900 phrank avg_rank avg_rank_age avg_rank_state *midpoint *upper {
	rename `x' `x'_80
}

//Create variables
gen wc_80=(occ1950_80<100 | (occ1950_80>=200 & occ1950_80<500)) if occ1950_80<=970
gen farmer_80=(occ1950_80>=100 & occ1950_80<200) if occ1950_80<=970
gen craft_80=(occ1950_80>=500 & occ1950_80<600) if occ1950_80<=970
gen operative_80=(occ1950_80>=600 & occ1950_80<700) if occ1950_80<=970
gen unskill_80=(occ1950_80>=700 & occ1950_80<=970 & occ1950_80~=850) if occ1950_80<=970

gen urban_clean_80=(urban_80==2) if urban_80~=.
gen farm_clean_80=(farm_80~=1)
gen lit_clean_80=(lit_80==4)

gen head_80=(relate_80==1)

gen marst_clean_80=(marst_80==1 | marst_80==2)

gen ffor_80=1-(fbpl_80<100)

if $simple == 1 {

	merge 1:1 histid* using ../raw/linkage_probabilities/Probabilities_1880_1910
	keep if _merge==3
	drop _merge

}

compress

//Save
save File_1880_1910, replace
