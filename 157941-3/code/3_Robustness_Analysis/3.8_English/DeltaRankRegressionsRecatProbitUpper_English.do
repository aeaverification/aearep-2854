cd "$repfolder/data/analysis"

capture postclose uncon_con
postfile uncon_con span conditional b s farmfam using UnconditionalConditionalResultsUpper_English, replace

/////////////////////////////

use FileRecatProbit_1850_1880_English, clear

_pctile link_prob_upper_50, percentiles(0.5)
replace link_prob_upper_50=r(r1) if link_prob_upper_50<r(r1)
gen delta_rank_upper=avg_rank_upper_80-avg_rank_upper_50

gen initial_rank_upper=avg_rank_upper_50
label var initial_rank_upper "Initial Avg.~Occ.~Rank"

reg delta_rank_upper foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_upper_50], robust
post uncon_con (1) (0) (_b[foreign_50]) (_se[foreign_50]) (2)

reg delta_rank_upper initial_rank_upper foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_upper_50], robust
post uncon_con (1) (1) (_b[foreign_50]) (_se[foreign_50]) (2)
	
/////////////////////////////

use File_1870_1900_English, clear

_pctile link_prob_upper_00, percentiles(0.5)
replace link_prob_upper_00=r(r1) if link_prob_upper_00<r(r1)
gen delta_rank_upper=avg_rank_upper_00-avg_rank_upper_70

gen initial_rank_upper=avg_rank_upper_70
label var initial_rank_upper "Initial Avg.~Occ.~Rank"

reg delta_rank_upper foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_upper_00], robust
post uncon_con (2) (0) (_b[foreign_00]) (_se[foreign_00]) (2)

reg delta_rank_upper initial_rank_upper foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_upper_00], robust
post uncon_con (2) (1) (_b[foreign_00]) (_se[foreign_00]) (2)
	
/////////////////////////////

use File_1880_1910_English, clear

_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)
gen delta_rank_upper=avg_rank_upper_10-avg_rank_upper_80

gen initial_rank_upper=avg_rank_upper_80
label var initial_rank_upper "Initial Avg.~Occ.~Rank"

reg delta_rank_upper foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_upper_10], robust
post uncon_con (3) (0) (_b[foreign_10]) (_se[foreign_10]) (2)

reg delta_rank_upper initial_rank_upper foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_upper_10], robust
post uncon_con (3) (1) (_b[foreign_10]) (_se[foreign_10]) (2)

/////////////////////////////

use File_1900_1930_English, clear

_pctile link_prob_upper_30, percentiles(0.5)
replace link_prob_upper_30=r(r1) if link_prob_upper_30<r(r1)
gen delta_rank_upper=avg_rank_upper_30-avg_rank_upper_00

gen initial_rank_upper=avg_rank_upper_00
label var initial_rank_upper "Initial Avg.~Occ.~Rank"

reg delta_rank_upper foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_upper_30], robust
post uncon_con (4) (0) (_b[foreign_30]) (_se[foreign_30]) (2)

reg delta_rank_upper initial_rank_upper foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_upper_30], robust
post uncon_con (4) (1) (_b[foreign_30]) (_se[foreign_30]) (2)
	
/////////////////////////////

use File_1910_1940_English, clear

_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)
gen delta_rank_upper=avg_rank_upper_40-avg_rank_upper_10

gen initial_rank_upper=avg_rank_upper_10
label var initial_rank_upper "Initial Avg.~Occ.~Rank"

reg delta_rank_upper foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_upper_10], robust
post uncon_con (5) (0) (_b[foreign_10]) (_se[foreign_10]) (2)

reg delta_rank_upper initial_rank_upper foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_upper_10], robust
post uncon_con (5) (1) (_b[foreign_10]) (_se[foreign_10]) (2)

postclose uncon_con
