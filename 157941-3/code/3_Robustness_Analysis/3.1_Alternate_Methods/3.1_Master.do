cd "$repfolder/data/analysis"

do "$repfolder/code/3_Robustness_Analysis/3.1_Alternate_Methods/3.1.1_ABEE/3.1.1_Master.do"
do "$repfolder/code/3_Robustness_Analysis/3.1_Alternate_Methods/3.1.2_ABEN/3.1.2_Master.do"
do "$repfolder/code/3_Robustness_Analysis/3.1_Alternate_Methods/3.1.3_Int/3.1.3_Master.do"
do "$repfolder/code/3_Robustness_Analysis/3.1_Alternate_Methods/3.1.4_IntPlus/3.1.4_Master.do"
