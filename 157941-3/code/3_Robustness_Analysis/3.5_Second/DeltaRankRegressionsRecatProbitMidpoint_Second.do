cd "$repfolder/data/analysis"

capture postclose uncon_con
postfile uncon_con span conditional b s farmfam using UnconditionalConditionalResultsMidpoint_Second, replace

/////////////////////////////

use FileRecatProbit_1850_1880_Second, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)
gen delta_rank_midpoint=avg_rank_midpoint_80-avg_rank_midpoint_50

gen initial_rank_midpoint=avg_rank_midpoint_50
label var initial_rank_midpoint "Initial Avg.~Occ.~Rank"

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
post uncon_con (1) (0) (_b[foreign_50]) (_se[foreign_50]) (1)

reg delta_rank_midpoint initial_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
post uncon_con (1) (1) (_b[foreign_50]) (_se[foreign_50]) (1)

gen foreign=foreign_50
gen occ1950=occ1950_50
gen bpl=bpl_50
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		if `x'==1850 {
			merge m:1 foreign occ1950 using InitialDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 foreign occ1950 using InitialDistribution`x'_Second, keep(1 3)
		}
		replace share`x'=0 if _merge==1
		drop _merge
		
		if `x'==1850 {
			merge m:1 bpl using InitialNationalityDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 bpl using InitialNationalityDistribution`x'_Second, keep(1 3)
		}
		replace frac`x'=0 if _merge==1 & bpl>=100
		replace frac`x'=1 if _merge==1 & bpl<100
		drop _merge
	}
}

capture postclose output
postfile output base_year weight_year coef se df using DeltaRankRegressionsWeightedRecatProbitMidpoint_Second, replace

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(share1850/share1850)/link_prob_midpoint_50], robust
	post output (1850) (1850) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(share1870/share1850)/link_prob_midpoint_50], robust
	post output (1850) (1870) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(share1880/share1850)/link_prob_midpoint_50], robust
	post output (1850) (1880) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(share1900/share1850)/link_prob_midpoint_50], robust
	post output (1850) (1900) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))
	
reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(share1910/share1850)/link_prob_midpoint_50], robust
	post output (1850) (1910) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))
	

capture postclose natoutput
postfile natoutput base_year weight_year coef se df using DeltaRankRegressionsWeightedNationalityRecatProbitMidpoint_Second, replace

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(frac1850/frac1850)/link_prob_midpoint_50], robust
	post natoutput (1850) (1850) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(frac1870/frac1850)/link_prob_midpoint_50], robust
	post natoutput (1850) (1870) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(frac1880/frac1850)/link_prob_midpoint_50], robust
	post natoutput (1850) (1880) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))

reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(frac1900/frac1850)/link_prob_midpoint_50], robust
	post natoutput (1850) (1900) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))
	
reg delta_rank_midpoint foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=(frac1910/frac1850)/link_prob_midpoint_50], robust
	post natoutput (1850) (1910) (_b[foreign_50]) (_se[foreign_50]) (e(df_r))
	
/////////////////////////////

use File_1870_1900_Second, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)
gen delta_rank_midpoint=avg_rank_midpoint_00-avg_rank_midpoint_70

gen initial_rank_midpoint=avg_rank_midpoint_70
label var initial_rank_midpoint "Initial Avg.~Occ.~Rank"

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
post uncon_con (2) (0) (_b[foreign_00]) (_se[foreign_00]) (1)

reg delta_rank_midpoint initial_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
post uncon_con (2) (1) (_b[foreign_00]) (_se[foreign_00]) (1)

gen foreign=foreign_00
gen occ1950=occ1950_70
gen bpl=bpl_70
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		if `x'==1850 {
			merge m:1 foreign occ1950 using InitialDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 foreign occ1950 using InitialDistribution`x'_Second, keep(1 3)
		}
		replace share`x'=0 if _merge==1
		drop _merge
		
		if `x'==1850 {
			merge m:1 bpl using InitialNationalityDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 bpl using InitialNationalityDistribution`x'_Second, keep(1 3)
		}
		replace frac`x'=0 if _merge==1 & bpl>=100
		replace frac`x'=1 if _merge==1 & bpl<100
		drop _merge
	}
}

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(share1850/share1870)/link_prob_midpoint_00], robust
	post output (1870) (1850) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(share1870/share1870)/link_prob_midpoint_00], robust
	post output (1870) (1870) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(share1880/share1870)/link_prob_midpoint_00], robust
	post output (1870) (1880) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(share1900/share1870)/link_prob_midpoint_00], robust
	post output (1870) (1900) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))
	
reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(share1910/share1870)/link_prob_midpoint_00], robust
	post output (1870) (1910) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))
	

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(frac1850/frac1870)/link_prob_midpoint_00], robust
	post natoutput (1870) (1850) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(frac1870/frac1870)/link_prob_midpoint_00], robust
	post natoutput (1870) (1870) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(frac1880/frac1870)/link_prob_midpoint_00], robust
	post natoutput (1870) (1880) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))

reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(frac1900/frac1870)/link_prob_midpoint_00], robust
	post natoutput (1870) (1900) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))
	
reg delta_rank_midpoint foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 [aw=(frac1910/frac1870)/link_prob_midpoint_00], robust
	post natoutput (1870) (1910) (_b[foreign_00]) (_se[foreign_00]) (e(df_r))
	
/////////////////////////////

use File_1880_1910_Second, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)
gen delta_rank_midpoint=avg_rank_midpoint_10-avg_rank_midpoint_80

gen initial_rank_midpoint=avg_rank_midpoint_80
label var initial_rank_midpoint "Initial Avg.~Occ.~Rank"

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
post uncon_con (3) (0) (_b[foreign_10]) (_se[foreign_10]) (1)

reg delta_rank_midpoint initial_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
post uncon_con (3) (1) (_b[foreign_10]) (_se[foreign_10]) (1)

gen foreign=foreign_10
gen occ1950=occ1950_80
gen bpl=bpl_80
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		if `x'==1850 {
			merge m:1 foreign occ1950 using InitialDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 foreign occ1950 using InitialDistribution`x'_Second, keep(1 3)
		}
		replace share`x'=0 if _merge==1
		drop _merge
		
		if `x'==1850 {
			merge m:1 bpl using InitialNationalityDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 bpl using InitialNationalityDistribution`x'_Second, keep(1 3)
		}
		replace frac`x'=0 if _merge==1 & bpl>=100
		replace frac`x'=1 if _merge==1 & bpl<100
		drop _merge
	}
}

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(share1850/share1880)/link_prob_midpoint_10], robust
	post output (1880) (1850) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(share1870/share1880)/link_prob_midpoint_10], robust
	post output (1880) (1870) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(share1880/share1880)/link_prob_midpoint_10], robust
	post output (1880) (1880) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(share1900/share1880)/link_prob_midpoint_10], robust
	post output (1880) (1900) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))
	
reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(share1910/share1880)/link_prob_midpoint_10], robust
	post output (1880) (1910) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))
	

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(frac1850/frac1880)/link_prob_midpoint_10], robust
	post natoutput (1880) (1850) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(frac1870/frac1880)/link_prob_midpoint_10], robust
	post natoutput (1880) (1870) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(frac1880/frac1880)/link_prob_midpoint_10], robust
	post natoutput (1880) (1880) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(frac1900/frac1880)/link_prob_midpoint_10], robust
	post natoutput (1880) (1900) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))
	
reg delta_rank_midpoint foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 [aw=(frac1910/frac1880)/link_prob_midpoint_10], robust
	post natoutput (1880) (1910) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

/////////////////////////////

use File_1900_1930_Second, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)
gen delta_rank_midpoint=avg_rank_midpoint_30-avg_rank_midpoint_00

gen initial_rank_midpoint=avg_rank_midpoint_00
label var initial_rank_midpoint "Initial Avg.~Occ.~Rank"

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
post uncon_con (4) (0) (_b[foreign_30]) (_se[foreign_30]) (1)

reg delta_rank_midpoint initial_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
post uncon_con (4) (1) (_b[foreign_30]) (_se[foreign_30]) (1)

gen foreign=foreign_30
gen occ1950=occ1950_00
gen bpl=bpl_00
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		if `x'==1850 {
			merge m:1 foreign occ1950 using InitialDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 foreign occ1950 using InitialDistribution`x'_Second, keep(1 3)
		}
		replace share`x'=0 if _merge==1
		drop _merge
		
		if `x'==1850 {
			merge m:1 bpl using InitialNationalityDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 bpl using InitialNationalityDistribution`x'_Second, keep(1 3)
		}
		replace frac`x'=0 if _merge==1 & bpl>=100
		replace frac`x'=1 if _merge==1 & bpl<100
		drop _merge
	}
}

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(share1850/share1900)/link_prob_midpoint_30], robust
	post output (1900) (1850) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(share1870/share1900)/link_prob_midpoint_30], robust
	post output (1900) (1870) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(share1880/share1900)/link_prob_midpoint_30], robust
	post output (1900) (1880) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(share1900/share1900)/link_prob_midpoint_30], robust
	post output (1900) (1900) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))
	
reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(share1910/share1900)/link_prob_midpoint_30], robust
	post output (1900) (1910) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))


reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(frac1850/frac1900)/link_prob_midpoint_30], robust
	post natoutput (1900) (1850) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(frac1870/frac1900)/link_prob_midpoint_30], robust
	post natoutput (1900) (1870) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(frac1880/frac1900)/link_prob_midpoint_30], robust
	post natoutput (1900) (1880) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))

reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(frac1900/frac1900)/link_prob_midpoint_30], robust
	post natoutput (1900) (1900) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))
	
reg delta_rank_midpoint foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 [aw=(frac1910/frac1900)/link_prob_midpoint_30], robust
	post natoutput (1900) (1910) (_b[foreign_30]) (_se[foreign_30]) (e(df_r))
	
/////////////////////////////

use File_1910_1940_Second, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)
gen delta_rank_midpoint=avg_rank_midpoint_40-avg_rank_midpoint_10

gen initial_rank_midpoint=avg_rank_midpoint_10
label var initial_rank_midpoint "Initial Avg.~Occ.~Rank"

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
post uncon_con (5) (0) (_b[foreign_10]) (_se[foreign_10]) (1)

reg delta_rank_midpoint initial_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
post uncon_con (5) (1) (_b[foreign_10]) (_se[foreign_10]) (1)

gen foreign=foreign_10
gen occ1950=occ1950_10
gen bpl=bpl_10
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		if `x'==1850 {
			merge m:1 foreign occ1950 using InitialDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 foreign occ1950 using InitialDistribution`x'_Second, keep(1 3)
		}
		replace share`x'=0 if _merge==1
		drop _merge
		
		if `x'==1850 {
			merge m:1 bpl using InitialNationalityDistributionRecatProbit`x'_Second, keep(1 3)
		}
		else {
			merge m:1 bpl using InitialNationalityDistribution`x'_Second, keep(1 3)
		}
		replace frac`x'=0 if _merge==1 & bpl>=100
		replace frac`x'=1 if _merge==1 & bpl<100
		drop _merge
	}
}

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(share1850/share1910)/link_prob_midpoint_10], robust
	post output (1910) (1850) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(share1870/share1910)/link_prob_midpoint_10], robust
	post output (1910) (1870) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(share1880/share1910)/link_prob_midpoint_10], robust
	post output (1910) (1880) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(share1900/share1910)/link_prob_midpoint_10], robust
	post output (1910) (1900) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))
	
reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(share1910/share1910)/link_prob_midpoint_10], robust
	post output (1910) (1910) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))


reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(frac1850/frac1910)/link_prob_midpoint_10], robust
	post natoutput (1910) (1850) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(frac1870/frac1910)/link_prob_midpoint_10], robust
	post natoutput (1910) (1870) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(frac1880/frac1910)/link_prob_midpoint_10], robust
	post natoutput (1910) (1880) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(frac1900/frac1910)/link_prob_midpoint_10], robust
	post natoutput (1910) (1900) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))
	
reg delta_rank_midpoint foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 [aw=(frac1910/frac1910)/link_prob_midpoint_10], robust
	post natoutput (1910) (1910) (_b[foreign_10]) (_se[foreign_10]) (e(df_r))

postclose output
postclose natoutput
postclose uncon_con

use DeltaRankRegressionsWeightedRecatProbitMidpoint_Second, clear
gen ciup=coef+(invttail(df,.025)*se)
gen cidn=coef+(invttail(df,.975)*se)

reshape wide coef se df ciup cidn, i(base_year) j(weight_year)

gen true=.
gen true_se=.
gen true_up=.
gen true_dn=.
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		replace true=coef`x' if base_year==`x'
		replace true_se=se`x' if base_year==`x'
		replace true_up=ciup`x' if base_year==`x'
		replace true_dn=cidn`x' if base_year==`x'
	}
}

twoway ///
	connected coef1850 base_year, lcolor(black) mcolor(black) msymbol(O) ///
|| ///
	connected coef1870 base_year, lcolor(black) lpattern(-) mcolor(black) msymbol(D) ///
|| ///
	connected coef1880 base_year, lcolor(black) lpattern(-.) mcolor(black) msymbol(S) ///
|| ///
	connected coef1900 base_year, lcolor(black) lpattern(_) mcolor(black) msymbol(T) ///
|| ///
	connected coef1910 base_year, lcolor(black) lpattern(_.) mcolor(black) msymbol(X) ///
|| ///
	line true base_year, lcolor(gray) lwidth(thick) ///
, graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(,angle(0) glwidth(0)) ///
		xtitle("Initial Year") ytitle("Immigrant Upgrading Premium") ///
		legend(region(lcolor(white)) order(1 "1850" 2 "1870" 3 "1880" 4 "1900" 5 "1910" 6 "True") ///
		subtitle("Year of Initial Occupational Distribution"))
graph export "$repfolder/results/FigureH8.pdf", replace


use DeltaRankRegressionsWeightedRecatProbitMidpoint_Second, clear
gen ciup=coef+(invttail(df,.025)*se)
gen cidn=coef+(invttail(df,.975)*se)

reshape wide coef se df ciup cidn, i(weight_year) j(base_year)

gen true=.
gen true_se=.
gen true_up=.
gen true_dn=.
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		replace true=coef`x' if weight_year==`x'
		replace true_se=se`x' if weight_year==`x'
		replace true_up=ciup`x' if weight_year==`x'
		replace true_dn=cidn`x' if weight_year==`x'
	}
}

twoway ///
	connected coef1850 weight_year, lcolor(black) mcolor(black) msymbol(O) ///
|| ///
	connected coef1870 weight_year, lcolor(black) lpattern(-) mcolor(black) msymbol(D) ///
|| ///
	connected coef1880 weight_year, lcolor(black) lpattern(-.) mcolor(black) msymbol(S) ///
|| ///
	connected coef1900 weight_year, lcolor(black) lpattern(_) mcolor(black) msymbol(T) ///
|| ///
	connected coef1910 weight_year, lcolor(black) lpattern(_.) mcolor(black) msymbol(X) ///
|| ///
	line true weight_year, lcolor(gray) lwidth(thick) ///
, graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(,angle(0) glwidth(0)) ///
		xtitle("Initial Year") ytitle("Immigrant Upgrading Premium") ///
		legend(region(lcolor(white)) order(1 "1850" 2 "1870" 3 "1880" 4 "1900" 5 "1910" 6 "True") ///
		subtitle("Basis of Occupational Upgrading"))
graph export "$repfolder/results/FigureH7.pdf", replace


use DeltaRankRegressionsWeightedNationalityRecatProbitMidpoint_Second, clear
gen ciup=coef+(invttail(df,.025)*se)
gen cidn=coef+(invttail(df,.975)*se)

reshape wide coef se df ciup cidn, i(base_year) j(weight_year)

gen true=.
gen true_se=.
gen true_up=.
gen true_dn=.
forvalues x=1850(10)1910 {
	if `x'~=1890 & `x'~=1860 {
		replace true=coef`x' if base_year==`x'
		replace true_se=se`x' if base_year==`x'
		replace true_up=ciup`x' if base_year==`x'
		replace true_dn=cidn`x' if base_year==`x'
	}
}

twoway ///
	connected coef1850 base_year, lcolor(black) mcolor(black) msymbol(O) ///
|| ///
	connected coef1870 base_year, lcolor(black) lpattern(-) mcolor(black) msymbol(D) ///
|| ///
	connected coef1880 base_year, lcolor(black) lpattern(-.) mcolor(black) msymbol(S) ///
|| ///
	line true base_year, lcolor(gray) lwidth(thick) ///
, graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(,angle(0) glwidth(0)) ///
		xtitle("Initial Year") ytitle("Immigrant Upgrading Premium") ///
		legend(region(lcolor(white)) order(1 "1850" 2 "1870" 3 "1880" 4 "True") ///
		subtitle("Year of Initial Nationality Distribution"))
graph export "$repfolder/results/FigureH9.pdf", replace
