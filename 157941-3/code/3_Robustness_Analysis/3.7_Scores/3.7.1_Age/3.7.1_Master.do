cd "$repfolder/data/analysis"

do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/DistributionsFarmFam_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/RankGraphsRecatProbit_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/OccupationalUpgradingGraphsMidpoint_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/DeltaRankRegressionsRecatProbit_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/DeltaRankRegressionsRecatProbitMidpoint_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/DeltaRankRegressionsRecatProbitUpper_Age.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.1_Age/GraphUnconditionalConditionalResults_Age.do"
