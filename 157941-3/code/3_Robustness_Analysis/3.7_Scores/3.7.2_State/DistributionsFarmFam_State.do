cd "$repfolder/data/analysis"

/////////////////////////////

use FileRecatProbit_1850_1880_State, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen count=1
keep if occ1950_50<=970 & occ1950_80<=970
collapse (sum) count [pw=1/link_prob_midpoint_50], by(occ1950_50 foreign_50)
fillin occ1950_50 foreign_50
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_50)
gen share=count/total
keep occ1950_50 share foreign_50
rename foreign_50 foreign
rename occ1950_50 occ1950
rename share share1850
compress
save InitialDistributionRecatProbit1850_State, replace

use FileRecatProbit_1850_1880_State, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen count=1
keep if occ1950_50<=970 & occ1950_80<=970
collapse (sum) count [pw=1/link_prob_midpoint_50], by(bpl_50)
keep if bpl_50>=100
egen total=total(count)
gen frac=count/total
keep bpl_50 frac
rename bpl_50 bpl
rename frac frac1850
compress
save InitialNationalityDistributionRecatProbit1850_State, replace

/////////////////////////////

use File_1870_1900_State, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen count=1
keep if occ1950_70<=970 & occ1950_00<=970
collapse (sum) count [pw=1/link_prob_midpoint_00], by(occ1950_70 foreign_00)
fillin occ1950_70 foreign_00
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_00)
gen share=count/total
keep occ1950_70 share foreign_00
rename foreign_00 foreign
rename occ1950_70 occ1950
rename share share1870
compress
save InitialDistribution1870_State, replace

use File_1870_1900_State, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen count=1
keep if occ1950_70<=970 & occ1950_00<=970
collapse (sum) count [pw=1/link_prob_midpoint_00], by(bpl_00)
keep if bpl_00>=100
egen total=total(count)
gen frac=count/total
keep bpl_00 frac
rename bpl_00 bpl
rename frac frac1870
compress
save InitialNationalityDistribution1870_State, replace

/////////////////////////////

use File_1880_1910_State, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_80<=970 & occ1950_10<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(occ1950_80 foreign_10)
fillin occ1950_80 foreign_10
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_10)
gen share=count/total
keep occ1950_80 share foreign_10
rename foreign_10 foreign
rename occ1950_80 occ1950
rename share share1880
compress
save InitialDistribution1880_State, replace

use File_1880_1910_State, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_80<=970 & occ1950_10<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(bpl_10)
keep if bpl_10>=100
egen total=total(count)
gen frac=count/total
keep bpl_10 frac
rename bpl_10 bpl
rename frac frac1880
compress
save InitialNationalityDistribution1880_State, replace

/////////////////////////////

use File_1900_1930_State, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen count=1
keep if occ1950_00<=970 & occ1950_30<=970
collapse (sum) count [pw=1/link_prob_midpoint_30], by(occ1950_00 foreign_30)
fillin occ1950_00 foreign_30
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_30)
gen share=count/total
keep occ1950_00 share foreign_30
rename foreign_30 foreign
rename occ1950_00 occ1950
rename share share1900
compress
save InitialDistribution1900_State, replace

use File_1900_1930_State, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen count=1
keep if occ1950_00<=970 & occ1950_30<=970
collapse (sum) count [pw=1/link_prob_midpoint_30], by(bpl_30)
keep if bpl_30>=100
egen total=total(count)
gen frac=count/total
keep bpl_30 frac
rename bpl_30 bpl
rename frac frac1900
compress
save InitialNationalityDistribution1900_State, replace

/////////////////////////////

use File_1910_1940_State, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_10<=970 & occ1950_40<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(occ1950_10 foreign_10)
fillin occ1950_10 foreign_10
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_10)
gen share=count/total
keep occ1950_10 share foreign_10
rename foreign_10 foreign
rename occ1950_10 occ1950
rename share share1910
compress
save InitialDistribution1910_State, replace

use File_1910_1940_State, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_10<=970 & occ1950_40<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(bpl_10)
keep if bpl_10>=100
egen total=total(count)
gen frac=count/total
keep bpl_10 frac
rename bpl_10 bpl
rename frac frac1910
compress
save InitialNationalityDistribution1910_State, replace

