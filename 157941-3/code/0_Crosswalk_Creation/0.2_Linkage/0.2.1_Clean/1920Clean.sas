libname storage "1920_3-3";
libname data "Files1920";

proc format cntlin=storage.us1920c_usa_f;
run;

data census1920(rename=(bpl=bpld));
	set storage.cens1920(keep=serial pernum sex age namefrst namelast bpl);
	where sex=1 and namelast~="" and namefrst~="" and namefrst~="[MR]";
	namelast=upcase(namelast);
	namefrst=upcase(namefrst);

	namegivcorr=compress(namefrst,,'ask');
	namelastcorr=compress(namelast,,'ask');
run;

data census1920;
	set census1920;
	bpl=floor(bpld/100);
run;

data census1920(drop=position);
	set census1920;
	position=prxmatch("/ [A-Z]$/",strip(namegivcorr));
	if position=0 then namefrstcorr=namegivcorr;
	else namefrstcorr=strip(substr(namegivcorr,1,position));
	if position>0 then namemidcorr=strip(substr(namegivcorr,position,2));
run;

data census1920;
	set census1920;

	if namefrstcorr='WM' then namefrstcorr='WILLIAM';
	else if namefrstcorr='ALBT' then namefrstcorr='ALBERT';
	else if namefrstcorr='ALFD' then namefrstcorr='ALFRED';
	else if namefrstcorr='ANDW' then namefrstcorr='ANDREW';
	else if namefrstcorr='ARCHD' then namefrstcorr='ARCHIBALD';
	else if namefrstcorr='BENJ' then namefrstcorr='BENJAMIN';
	else if namefrstcorr='CHAS' then namefrstcorr='CHARLES';
	else if namefrstcorr='CLIFD' then namefrstcorr='CLIFFORD';
	else if namefrstcorr='CORUS' then namefrstcorr='CORNELIUS';
	else if namefrstcorr='CUTHBT' then namefrstcorr='CUTHBERT';
	else if namefrstcorr='DELBT' then namefrstcorr='DELBERT';
	else if namefrstcorr='EDWD' then namefrstcorr='EDWARD';
	else if namefrstcorr='FREDK' then namefrstcorr='FREDERICK';
	else if namefrstcorr='FS' then namefrstcorr='FRANCIS';
	else if namefrstcorr='GILBT' then namefrstcorr='GILBERT';
	else if namefrstcorr='HY' then namefrstcorr='HENRY';
	else if namefrstcorr='JERMH' then namefrstcorr='JEREMIAH';
	else if namefrstcorr='JN' then namefrstcorr='JOHN';
	else if namefrstcorr='JNO' then namefrstcorr='JOHN';
	else if namefrstcorr='JNTHN' then namefrstcorr='JONATHAN';
	else if namefrstcorr='MATHW' then namefrstcorr='MATTHEW';
	else if namefrstcorr='NATHL' then namefrstcorr='NATHANIEL';
	else if namefrstcorr='PATK' then namefrstcorr='PATRICK';
	else if namefrstcorr='REGD' then namefrstcorr='REGINALD';
	else if namefrstcorr='RICHD' then namefrstcorr='RICHARD';
	else if namefrstcorr='ROBT' then namefrstcorr='ROBERT';
	else if namefrstcorr='RODK' then namefrstcorr='RODERICK';
	else if namefrstcorr='RPH' then namefrstcorr='RALPH';
	else if namefrstcorr='SAML' then namefrstcorr='SAMUEL';
	else if namefrstcorr='THOS' then namefrstcorr='THOMAS';
	else if namefrstcorr='WILFD' then namefrstcorr='WILFRED';
	else if namefrstcorr='GEO' then namefrstcorr='GEORGE';
	else if namefrstcorr='JAS' then namefrstcorr='JAMES';
run;

data census1920;
	set census1920;

	namefrstcorr=compress(namefrstcorr,,'ak');
	namelastcorr=compress(namelastcorr,,'ak');
	namegivcorr=compress(namegivcorr,,'ak');
run;

data data.census1920_cleaned_for_selflink;
	set census1920(keep=serial pernum age namefrstcorr namelastcorr namemidcorr bpl);
	where namefrstcorr~="" and namelastcorr~="";
run;
