libname storage "1850_3-1";
libname data "Files1850";

proc format cntlin=storage.us1850c_usa_f;
run;

data import(rename=(bpl=bpld));
	set storage.cens1850(keep=serial pernum sex age namefrst namelast bpl);
	where sex=1 and namelast~="" and namefrst~="" and namefrst~="[MR]";
	namelast=upcase(namelast);
	namefrst=upcase(namefrst);

	namegivcorr=compress(namefrst,,'ask');
	namelastcorr=compress(namelast,,'ask');
run;

data import;
	set import;
	bpl=floor(bpld/100);
run;

proc sql;
	create table census1850 as select
	a.serial as serial, a.pernum as pernum, a.age as age,
	a.namelast as namelast, a.namefrst as namefrst,
	a.namegivcorr as namegivcorr, a.namelastcorr as namelastcorr,
	a.bpl as bpl,
	b.mark as mark
	from import as a left join data.census1850_drops as b
	on a.serial=b.serial and a.pernum=b.pernum;
quit;

data census1850(drop=mark);
	set census1850;
	where mark~=1;
run;

data census1850(drop=position);
	set census1850;
	position=prxmatch("/ [A-Z]$/",strip(namegivcorr));
	if position=0 then namefrstcorr=namegivcorr;
	else namefrstcorr=strip(substr(namegivcorr,1,position));
	if position>0 then namemidcorr=strip(substr(namegivcorr,position,2));
run;

data census1850;
	set census1850;

	namefrstcorr=compress(namefrstcorr,,'ak');
	namelastcorr=compress(namelastcorr,,'ak');
	namegivcorr=compress(namegivcorr,,'ak');
run;

data data.census1850_cleaned_start;
	set census1850(keep=serial pernum age namefrstcorr namelastcorr namemidcorr bpl);
	where namefrstcorr~="" and namelastcorr~="" and length(namefrstcorr)>1 and length(namelastcorr)>2;
run;
