//cd

clear
insheet using Uniques5070.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1850_1870, replace

clear
insheet using Uniques8000New.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1880_1900, replace

clear
insheet using Uniques0020.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1900_1920, replace

clear
insheet using Uniques1030.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1910_1930, replace

clear
insheet using Uniques2040.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1920_1940, replace

