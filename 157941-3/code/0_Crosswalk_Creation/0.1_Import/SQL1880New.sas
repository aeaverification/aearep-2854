libname IPUMS "1880_3-3";

proc format cntlin = IPUMS.us1880e_usa_f;
run;

proc sql;
	create table IPUMS.cens1880 as select
	a.rectype as rectype,
	a.year as year,
	a.sample as sample,
	a.serial as serial,
	a.numprec as numprec,
	a.subsamp as subsamp,
	a.hhwtreg as hhwtreg,
	a.dwsize as dwsize,
	a.region as region,
	a.stateicp as stateicp,
	a.statefip as statefip,
	a.sea as sea,
	a.metro as metro,
	a.metarea as metarea,
	a.metdist as metdist,
	a.city as city,
	a.citypop as citypop,
	a.sizepl as sizepl,
	a.urban as urban,
	a.urbarea as urbarea,
	a.gq as gq,
	a.gqtype as gqtype,
	a.gqfunds as gqfunds,
	a.farm as farm,
	a.pageno as pageno,
	a.microseq as microseq,
	a.nfams as nfams,
	a.ncouples as ncouples,
	a.nmothers as nmothers,
	a.nfathers as nfathers,
	a.qgq as qgq,
	a.mcd as mcd,
	a.incorp as incorp,
	a.nengpop as nengpop,
	a.urbpop as urbpop,
	a.hhtype as hhtype,
	a.cntry as cntry,
	a.nsubfam as nsubfam,
	a.nsubfam2 as nsubfam2,
	a.headloc as headloc,
	a.qnumperh as qnumperh,
	a.countynhg as countynhg,
	a.yrstcounty as yrstcounty,
	a.stcounty as stcounty,
	a.appal as appal,
	a.countyicp as countyicp,
	a.hhwt as hhwt,
	a.mcdstr as mcdstr,
	a.incstr as incstr,
	a.gqstr as gqstr,
	a.serial80 as serial80,
	a.mdstatus as mdstatus,
	a.reel as reel,
	a.numperhh as numperhh,
	a.line as line,
	a.enumdist as enumdist,
	a.supdist as supdist,
	a.street as street,
	a.qgqfunds as qgqfunds,
	a.split80 as split80,
	a.split as split,
	a.splithid as splithid,
	a.splitnum as splitnum,
	a.qutp1 as qutp1,
	a.qutp2 as qutp2,
	a.qutp3 as qutp3,
	a.utp as utp,
	a.addressutp as addressutp,
	a.xgps as xgps,
	a.ygps as ygps,
	a.us1880e_0010 as us1880e_0010,
	a.us1880e_0085 as us1880e_0085,
	a.us1880e_0014 as us1880e_0014,
	a.us1880e_0015 as us1880e_0015,
	a.us1880e_0016 as us1880e_0016,
	a.us1880e_0017 as us1880e_0017,
	a.us1880e_0019 as us1880e_0019,
	a.us1880e_0020 as us1880e_0020,
	a.us1880e_0021 as us1880e_0021,
	a.us1880e_0022 as us1880e_0022,
	a.us1880e_0023 as us1880e_0023,
	a.us1880e_0025 as us1880e_0025,
	a.us1880e_0026 as us1880e_0026,
	a.us1880e_0028 as us1880e_0028,
	a.us1880e_0029 as us1880e_0029,
	a.us1880e_0059 as us1880e_0059,
	a.us1880e_0033 as us1880e_0033,
	a.us1880e_0034 as us1880e_0034,
	a.us1880e_0035 as us1880e_0035,
	a.us1880e_0037 as us1880e_0037,
	a.us1880e_0038 as us1880e_0038,
	a.us1880e_0040 as us1880e_0040,
	a.us1880e_0041 as us1880e_0041,
	a.us1880e_0042 as us1880e_0042,
	a.us1880e_0043 as us1880e_0043,
	a.us1880e_0044 as us1880e_0044,
	a.us1880e_0045 as us1880e_0045,
	a.us1880e_0046 as us1880e_0046,
	a.us1880e_0048 as us1880e_0048,
	a.us1880e_0123 as us1880e_0123,
	a.us1880e_0050 as us1880e_0050,
	a.us1880e_0051 as us1880e_0051,
	a.us1880e_0052 as us1880e_0052,
	a.us1880e_0054 as us1880e_0054,
	a.us1880e_0055 as us1880e_0055,
	a.us1880e_0086 as us1880e_0086,
	a.us1880e_0087 as us1880e_0087,
	a.us1880e_0057 as us1880e_0057,
	a.us1880e_0058 as us1880e_0058,
	a.us1880e_0088 as us1880e_0088,
	a.us1880e_0060 as us1880e_0060,
	a.us1880e_0061 as us1880e_0061,
	a.us1880e_0089 as us1880e_0089,
	a.us1880e_0063 as us1880e_0063,
	a.us1880e_0064 as us1880e_0064,
	a.us1880e_0066 as us1880e_0066,
	a.us1880e_0090 as us1880e_0090,
	a.us1880e_0069 as us1880e_0069,
	a.us1880e_0070 as us1880e_0070,
	a.us1880e_0071 as us1880e_0071,
	a.us1880e_0072 as us1880e_0072,
	a.us1880e_0073 as us1880e_0073,
	a.us1880e_0091 as us1880e_0091,
	a.us1880e_0092 as us1880e_0092,
	a.us1880e_0093 as us1880e_0093,
	a.us1880e_0094 as us1880e_0094,
	a.us1880e_0095 as us1880e_0095,
	a.us1880e_0096 as us1880e_0096,
	a.us1880e_0097 as us1880e_0097,
	a.us1880e_0098 as us1880e_0098,
	a.us1880e_0099 as us1880e_0099,
	a.us1880e_0100 as us1880e_0100,
	a.us1880e_0101 as us1880e_0101,		
	a.us1880e_0102 as us1880e_0102,
	a.us1880e_0082 as us1880e_0082,
	a.us1880e_0103 as us1880e_0103,		
	a.us1880e_0104 as us1880e_0104,		
	a.us1880e_0105 as us1880e_0105,	
	a.us1880e_0084 as us1880e_0084,
	a.us1880e_0106 as us1880e_0106,		
	a.us1880e_0107 as us1880e_0107,		
	a.us1880e_0108 as us1880e_0108,		
	a.us1880e_0109 as us1880e_0109,		
	a.us1880e_0110 as us1880e_0110,		
	a.us1880e_0111 as us1880e_0111,		
	a.us1880e_0112 as us1880e_0112,		
	a.us1880e_0113 as us1880e_0113,		
	a.us1880e_0114 as us1880e_0114,		
	a.us1880e_0115 as us1880e_0115,		
	a.us1880e_0116 as us1880e_0116,		
	a.us1880e_0117 as us1880e_0117,		
	a.us1880e_0118 as us1880e_0118,		
	a.us1880e_0119 as us1880e_0119,		
	a.us1880e_0120 as us1880e_0120,		
	a.us1880e_0121 as us1880e_0121,		
	a.us1880e_0122 as us1880e_0122,
	a.us1880e_0081 as us1880e_0081,
	a.us1880e_0078 as us1880e_0078,
	a.us1880e_0079 as us1880e_0079,
	a.us1880e_0080 as us1880e_0080,
	a.us1880e_0075 as us1880e_0075,
	a.us1880e_0076 as us1880e_0076,
	a.us1880e_0077 as us1880e_0077,

	b.rectypep as rectypep,
	b.yearp as yearp,
	b.samplep as samplep,
	b.serialp as serialp,
	b.pernum as pernum,
	b.slwtreg as slwtreg,
	b.perwtreg as perwtreg,
	b.momloc as momloc,
	b.stepmom as stepmom,
	b.momrule_hist as momrule_hist,
	b.poploc as poploc,
	b.steppop as steppop,
	b.poprule_hist as poprule_hist,
	b.sploc as sploc,
	b.sprule_hist as sprule_hist,
	b.famsize as famsize,
	b.nchild as nchild,
	b.nchlt5 as nchlt5,
	b.famunit as famunit,
	b.eldch as eldch,
	b.yngch as yngch,
	b.nsibs as nsibs,
	b.relate as relate,
	b.age as age,
	b.sex as sex,
	b.race as race,
	b.marst as marst,
	b.marrinyr as marrinyr,
	b.bpl as bpl,
	b.nativity as nativity,
	b.hispan as hispan,
	b.spanname as spanname,
	b.school as school,
	b.lit as lit,
	b.labforce as labforce,
	b.occ1950 as occ1950,
	b.occscore as occscore,
	b.sei as sei,
	b.ind1950 as ind1950,
	b.qtrunemp as qtrunemp,
	b.imprel as imprel,
	b.birthqtr as birthqtr,
	b.qage as qage,
	b.qagemont as qagemont,
	b.qbpl as qbpl,
	b.qfbpl as qfbpl,
	b.qmarst as qmarst,
	b.qocc as qocc,
	b.qrace as qrace,
	b.qrelate as qrelate,
	b.qschool as qschool,
	b.qsex as qsex,
	b.racamind as racamind,
	b.racasian as racasian,
	b.racblk as racblk,
	b.racpacis as racpacis,
	b.racother as racother,
	b.racwht as racwht,
	b.agediff as agediff,
	b.racesing as racesing,
	b.probwht as probwht,
	b.proboth as proboth,
	b.probblk as probblk,
	b.probapi as probapi,
	b.probai as probai,
	b.hisprule as hisprule,
	b.relflag as relflag,
	b.presgl as presgl,
	b.erscor50 as erscor50,
	b.edscor50 as edscor50,
	b.npboss50 as npboss50,
	b.occstr as occstr,
	b.occhisco as occhisco,
	b.pernum80 as pernum80,
	b.isrelate as isrelate,
	b.subfam as subfam,
	b.sftype as sftype,
	b.sfrelate as sfrelate,
	b.subfam2 as subfam2,
	b.sftype2 as sftype2,
	b.sfrelate2 as sfrelate2,
	b.recid as recid,
	b.slwt as slwt,
	b.perwt as perwt,
	b.birthyr as birthyr,
	b.occ as occ,
	b.namelast as namelast,
	b.namefrst as namefrst,
	b.bplstr as bplstr,
	b.fbplstr as fbplstr,
	b.mbplstr as mbplstr,
	b.relstr as relstr,
	b.mbpl as mbpl,
	b.fbpl as fbpl,
	b.mounemp as mounemp,
	b.agemonth as agemonth,
	b.birthmo as birthmo,
	b.maimed as maimed,
	b.blind as blind,
	b.deaf as deaf,
	b.idiotic as idiotic,
	b.insane as insane,
	b.sickness as sickness,
	b.sursim as sursim,
	b.qlit as qlit,
	b.qmbpl as qmbpl,
	b.anyallocation as anyallocation,
	b.histid as histid,
	b.us1880e_1001 as us1880e_1001,
	b.us1880e_1002 as us1880e_1002,
	b.us1880e_1003 as us1880e_1003,
	b.us1880e_1004 as us1880e_1004,
	b.us1880e_1005 as us1880e_1005,
	b.us1880e_1006 as us1880e_1006,
	b.us1880e_1007 as us1880e_1007,
	b.us1880e_1008 as us1880e_1008,
	b.us1880e_1009 as us1880e_1009,
	b.us1880e_1010 as us1880e_1010,
	b.us1880e_1011 as us1880e_1011,
	b.us1880e_1012 as us1880e_1012,
	b.us1880e_1013 as us1880e_1013,
	b.us1880e_1014 as us1880e_1014,
	b.us1880e_1016 as us1880e_1016,
	b.us1880e_1018 as us1880e_1018,
	b.us1880e_1019 as us1880e_1019,
	b.us1880e_1021 as us1880e_1021,
	b.us1880e_1022 as us1880e_1022,
	b.us1880e_1023 as us1880e_1023,
	b.us1880e_1024 as us1880e_1024,
	b.us1880e_1025 as us1880e_1025,
	b.us1880e_1027 as us1880e_1027,
	b.us1880e_1028 as us1880e_1028,
	b.us1880e_1030 as us1880e_1030,
	b.us1880e_1033 as us1880e_1033,
	b.us1880e_1034 as us1880e_1034,
	b.us1880e_1035 as us1880e_1035,
	b.us1880e_1037 as us1880e_1037,
	b.us1880e_1038 as us1880e_1038,
	b.us1880e_1039 as us1880e_1039,
	b.us1880e_1040 as us1880e_1040,
	b.us1880e_1041 as us1880e_1041,
	b.us1880e_1042 as us1880e_1042,
	b.us1880e_1043 as us1880e_1043,
	b.us1880e_1045 as us1880e_1045,
	b.us1880e_1047 as us1880e_1047,
	b.us1880e_1048 as us1880e_1048,
	b.us1880e_1050 as us1880e_1050,
	b.us1880e_1052 as us1880e_1052,
	b.us1880e_1054 as us1880e_1054,
	b.us1880e_1055 as us1880e_1055,
	b.us1880e_1057 as us1880e_1057,
	b.us1880e_1059 as us1880e_1059,
	b.us1880e_1061 as us1880e_1061,
	b.us1880e_1063 as us1880e_1063,
	b.us1880e_1065 as us1880e_1065,
	b.us1880e_1066 as us1880e_1066,
	b.us1880e_1069 as us1880e_1069,
	
	b.us1880e_1070 as us1880e_1070,
	b.us1880e_1071 as us1880e_1071,
	b.us1880e_1072 as us1880e_1072,
	b.us1880e_1073 as us1880e_1073,
	b.us1880e_1074 as us1880e_1074,
	b.us1880e_1075 as us1880e_1075,
	b.us1880e_1076 as us1880e_1076,
	b.us1880e_1077 as us1880e_1077,
	b.us1880e_1078 as us1880e_1078,
	b.us1880e_1079 as us1880e_1079,
	b.us1880e_1080 as us1880e_1080,
	b.us1880e_1081 as us1880e_1081,
	b.us1880e_1082 as us1880e_1082,
	b.us1880e_1083 as us1880e_1083,
	b.us1880e_1084 as us1880e_1084,
	b.us1880e_1085 as us1880e_1085,
	b.us1880e_1086 as us1880e_1086,
	b.us1880e_1087 as us1880e_1087,
	b.us1880e_1088 as us1880e_1088,
	b.us1880e_1089 as us1880e_1089,
	b.us1880e_1090 as us1880e_1090,
	b.us1880e_1091 as us1880e_1091,
	b.us1880e_1092 as us1880e_1092,
	b.us1880e_1093 as us1880e_1093,
	b.us1880e_1094 as us1880e_1094,
	b.us1880e_1095 as us1880e_1095,
	b.us1880e_1096 as us1880e_1096,
	b.us1880e_1097 as us1880e_1097,
	b.us1880e_1098 as us1880e_1098,
	b.us1880e_1099 as us1880e_1099,
	b.us1880e_1100 as us1880e_1100,
	b.us1880e_1101 as us1880e_1101,
	b.us1880e_1102 as us1880e_1102,
	b.us1880e_1103 as us1880e_1103,
	b.us1880e_1104 as us1880e_1104,
	b.us1880e_1105 as us1880e_1105,
	b.us1880e_1106 as us1880e_1106,
	b.us1880e_1107 as us1880e_1107,
	b.us1880e_1108 as us1880e_1108,
	b.us1880e_1109 as us1880e_1109,
	b.us1880e_1110 as us1880e_1110,
	b.us1880e_1111 as us1880e_1111,
	b.us1880e_1112 as us1880e_1112,
	b.us1880e_1113 as us1880e_1113,
	b.us1880e_1114 as us1880e_1114,
	b.us1880e_1115 as us1880e_1115,
	b.us1880e_1116 as us1880e_1116,
	b.us1880e_1117 as us1880e_1117,
	b.us1880e_1118 as us1880e_1118,
	b.us1880e_1119 as us1880e_1119,
	b.us1880e_1120 as us1880e_1120,
	b.us1880e_1121 as us1880e_1121,
	b.us1880e_1122 as us1880e_1122,
	b.us1880e_1123 as us1880e_1123,
	b.us1880e_1124 as us1880e_1124,
	b.us1880e_1125 as us1880e_1125,
	b.us1880e_1126 as us1880e_1126,
	b.us1880e_1127 as us1880e_1127,
	b.us1880e_1128 as us1880e_1128,
	b.us1880e_1129 as us1880e_1129,
	b.us1880e_1130 as us1880e_1130,
	b.us1880e_1131 as us1880e_1131,
	b.us1880e_1132 as us1880e_1132,
	b.us1880e_1133 as us1880e_1133,
	b.us1880e_1134 as us1880e_1134,
	b.us1880e_1135 as us1880e_1135,
	b.us1880e_1136 as us1880e_1136,
	b.us1880e_1137 as us1880e_1137,
	b.us1880e_1138 as us1880e_1138,
	b.us1880e_1139 as us1880e_1139,
	b.us1880e_1140 as us1880e_1140,
	b.us1880e_1141 as us1880e_1141
	from IPUMS.us1880e_usa(where=(rectype="H")) as a full join IPUMS.us1880e_usa(where=(rectype="P")) as b
	on a.serial=b.serialp;
quit;

proc sort data=IPUMS.cens1880;
	by serialp pernum;
run;
