cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1850_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_50=upper(histid)
merge 1:1 histid_50 using RecatProbit1850, keepusing(occ1950) update replace
drop _merge
drop histid_50

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1850
drop if unskill==.

tempfile trends
save `trends'

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1860_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1860
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1870_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1870
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1880
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1900_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1900
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1910_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1910
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1920_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1920
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1930_100pct", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1930
drop if unskill==.

append using `trends'
save `trends', replace

/////////////////////////////////
/////////////////////////////////
/////////////////////////////////

use  "$repfolder/data/confidential/ipums_data/1940_100pct_male", clear
replace occ1950=820 if occ1950==830

preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge
gen farmfam=(relate>=2 & relate<12 & isfarm==1 & occ1950>=800 & occ1950<900)

keep if sex==1 & age>=18 & age<=65 & race==1 & (region<30 | region>=40)

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850 & farmfam==0) if occ1950<=970
replace farmfam=. if occ1950>970

collapse (count) serial, by(foreign wc farmer craft operative unskill farmfam)
rename serial count
gen year=1940
drop if unskill==.

append using `trends'

/////////////////////////////////

egen total=total(count), by(year foreign)
gen share=count/total

gen occ_cat=1 if wc==1
replace occ_cat=2 if farmer==1
replace occ_cat=3 if craft==1
replace occ_cat=4 if operative==1
replace occ_cat=5 if unskill==1
replace occ_cat=6 if farmfam==1

drop farmfam wc farmer craft operative unskill
drop count total

reshape wide share, i(foreign year) j(occ_cat)

twoway ///
	connected share2 year if foreign==1, lcolor(black) mcolor(black) msize(1) ///
|| ///
	connected share5 year if foreign==1, lcolor(black) lpattern(-) mcolor(black) msymbol(D) msize(1) ///
|| ///
	connected share1 year if foreign==1, lcolor(black) lpattern(_) mcolor(black) msymbol(T) msize(1) ///
, graphregion(color(white)) ///
		ylabel(0(.1).4,angle(0) glwidth(0)) xlabel(1840(10)1940) yscale(range(0 .4)) ///
		xtitle("Year") ytitle("Fraction of Employment") ///
		legend(region(lcolor(white)) order(1 "Farmers" 2 "Unskilled" 3 "White Collar"))
graph export "$repfolder/results/Figure2b.pdf", replace
		
twoway ///
	connected share2 year if foreign==0, lcolor(black) mcolor(black) msize(1) ///
|| ///
	connected share5 year if foreign==0, lcolor(black) lpattern(-) mcolor(black) msymbol(D) msize(1) ///
|| ///
	connected share1 year if foreign==0, lcolor(black) lpattern(_) mcolor(black) msymbol(T) msize(1) ///
, graphregion(color(white)) ///
		ylabel(0(.1).4,angle(0) glwidth(0)) xlabel(1840(10)1940) yscale(range(0 .4)) ///
		xtitle("Year") ytitle("Fraction of Employment") ///
		legend(region(lcolor(white)) order(1 "Farmers" 2 "Unskilled" 3 "White Collar"))
graph export "$repfolder/results/Figure2a.pdf", replace
