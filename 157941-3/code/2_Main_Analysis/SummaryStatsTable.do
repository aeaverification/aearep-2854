cd "$repfolder/data/analysis"

use FileRecatProbit_1850_1880, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen NE=(region_50==11 | region_50==12)
gen MW=(region_50==21 | region_50==22)
gen W=(region_50==41 | region_50==42)

su avg_rank_50 if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_1: di %4.3f r(mean)
	local s_1_1: di %4.3f r(sd)
	local n_1_n=r(N)
su avg_rank_80 if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_2: di %4.3f r(mean)
	local s_1_2: di %4.3f r(sd)
su age_50 if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_3: di %4.3f r(mean)
	local s_1_3: di %4.3f r(sd)	
su NE if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_4: di %4.3f r(mean)
	local s_1_4: di %4.3f r(sd)	
su MW if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_5: di %4.3f r(mean)
	local s_1_5: di %4.3f r(sd)	
su W if foreign_50==0 [aw=1/link_prob_midpoint_50]
	local b_1_6: di %4.3f r(mean)
	local s_1_6: di %4.3f r(sd)	
	
su avg_rank_50 if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_7: di %4.3f r(mean)
	local s_1_7: di %4.3f r(sd)
	local n_1_f=r(N)
su avg_rank_80 if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_8: di %4.3f r(mean)
	local s_1_8: di %4.3f r(sd)
su age_50 if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_9: di %4.3f r(mean)
	local s_1_9: di %4.3f r(sd)	
su NE if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_10: di %4.3f r(mean)
	local s_1_10: di %4.3f r(sd)	
su MW if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_11: di %4.3f r(mean)
	local s_1_11: di %4.3f r(sd)	
su W if foreign_50==1 [aw=1/link_prob_midpoint_50]
	local b_1_12: di %4.3f r(mean)
	local s_1_12: di %4.3f r(sd)	
	
local n_1=`n_1_n'+`n_1_f'
local n_1: di %7.0fc `n_1'
local n_1_n: di %7.0fc `n_1_n'
local n_1_f: di %7.0fc `n_1_f'

////////////////////////////////////////////////

use File_1870_1900, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen NE=(region_70==11 | region_70==12)
gen MW=(region_70==21 | region_70==22)
gen W=(region_70==41 | region_70==42)

su avg_rank_70 if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_1: di %4.3f r(mean)
	local s_2_1: di %4.3f r(sd)
	local n_2_n=r(N)
su avg_rank_00 if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_2: di %4.3f r(mean)
	local s_2_2: di %4.3f r(sd)
su age_70 if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_3: di %4.3f r(mean)
	local s_2_3: di %4.3f r(sd)	
su NE if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_4: di %4.3f r(mean)
	local s_2_4: di %4.3f r(sd)	
su MW if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_5: di %4.3f r(mean)
	local s_2_5: di %4.3f r(sd)	
su W if foreign_00==0 [aw=1/link_prob_midpoint_00]
	local b_2_6: di %4.3f r(mean)
	local s_2_6: di %4.3f r(sd)	
	
su avg_rank_70 if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_7: di %4.3f r(mean)
	local s_2_7: di %4.3f r(sd)
	local n_2_f=r(N)
su avg_rank_00 if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_8: di %4.3f r(mean)
	local s_2_8: di %4.3f r(sd)
su age_70 if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_9: di %4.3f r(mean)
	local s_2_9: di %4.3f r(sd)	
su NE if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_10: di %4.3f r(mean)
	local s_2_10: di %4.3f r(sd)	
su MW if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_11: di %4.3f r(mean)
	local s_2_11: di %4.3f r(sd)	
su W if foreign_00==1 [aw=1/link_prob_midpoint_00]
	local b_2_12: di %4.3f r(mean)
	local s_2_12: di %4.3f r(sd)	
	
local n_2=`n_2_n'+`n_2_f'
local n_2: di %7.0fc `n_2'
local n_2_n: di %7.0fc `n_2_n'
local n_2_f: di %7.0fc `n_2_f'

////////////////////////////////////////////////

use File_1880_1910, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen NE=(region_80==11 | region_80==12)
gen MW=(region_80==21 | region_80==22)
gen W=(region_80==41 | region_80==42)

su avg_rank_80 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_1: di %4.3f r(mean)
	local s_3_1: di %4.3f r(sd)
	local n_3_n=r(N)
su avg_rank_10 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_2: di %4.3f r(mean)
	local s_3_2: di %4.3f r(sd)
su age_80 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_3: di %4.3f r(mean)
	local s_3_3: di %4.3f r(sd)	
su NE if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_4: di %4.3f r(mean)
	local s_3_4: di %4.3f r(sd)	
su MW if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_5: di %4.3f r(mean)
	local s_3_5: di %4.3f r(sd)	
su W if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_3_6: di %4.3f r(mean)
	local s_3_6: di %4.3f r(sd)	
	
su avg_rank_80 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_7: di %4.3f r(mean)
	local s_3_7: di %4.3f r(sd)
	local n_3_f=r(N)
su avg_rank_10 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_8: di %4.3f r(mean)
	local s_3_8: di %4.3f r(sd)
su age_80 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_9: di %4.3f r(mean)
	local s_3_9: di %4.3f r(sd)	
su NE if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_10: di %4.3f r(mean)
	local s_3_10: di %4.3f r(sd)	
su MW if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_11: di %4.3f r(mean)
	local s_3_11: di %4.3f r(sd)	
su W if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_3_12: di %4.3f r(mean)
	local s_3_12: di %4.3f r(sd)	
	
local n_3=`n_3_n'+`n_3_f'
local n_3: di %7.0fc `n_3'
local n_3_n: di %7.0fc `n_3_n'
local n_3_f: di %7.0fc `n_3_f'

////////////////////////////////////////////////

use File_1900_1930, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen NE=(region_00==11 | region_00==12)
gen MW=(region_00==21 | region_00==22)
gen W=(region_00==41 | region_00==42)

su avg_rank_00 if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_1: di %4.3f r(mean)
	local s_4_1: di %4.3f r(sd)
	local n_4_n=r(N)
su avg_rank_30 if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_2: di %4.3f r(mean)
	local s_4_2: di %4.3f r(sd)
su age_00 if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_3: di %4.3f r(mean)
	local s_4_3: di %4.3f r(sd)	
su NE if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_4: di %4.3f r(mean)
	local s_4_4: di %4.3f r(sd)	
su MW if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_5: di %4.3f r(mean)
	local s_4_5: di %4.3f r(sd)	
su W if foreign_30==0 [aw=1/link_prob_midpoint_30]
	local b_4_6: di %4.3f r(mean)
	local s_4_6: di %4.3f r(sd)	
	
su avg_rank_00 if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_7: di %4.3f r(mean)
	local s_4_7: di %4.3f r(sd)
	local n_4_f=r(N)
su avg_rank_30 if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_8: di %4.3f r(mean)
	local s_4_8: di %4.3f r(sd)
su age_00 if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_9: di %4.3f r(mean)
	local s_4_9: di %4.3f r(sd)	
su NE if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_10: di %4.3f r(mean)
	local s_4_10: di %4.3f r(sd)	
su MW if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_11: di %4.3f r(mean)
	local s_4_11: di %4.3f r(sd)	
su W if foreign_30==1 [aw=1/link_prob_midpoint_30]
	local b_4_12: di %4.3f r(mean)
	local s_4_12: di %4.3f r(sd)	
	
local n_4=`n_4_n'+`n_4_f'
local n_4: di %7.0fc `n_4'
local n_4_n: di %7.0fc `n_4_n'
local n_4_f: di %7.0fc `n_4_f'

////////////////////////////////////////////////

use File_1910_1940, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen NE=(region_10==11 | region_10==12)
gen MW=(region_10==21 | region_10==22)
gen W=(region_10==41 | region_10==42)

su avg_rank_10 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_1: di %4.3f r(mean)
	local s_5_1: di %4.3f r(sd)
	local n_5_n=r(N)
su avg_rank_40 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_2: di %4.3f r(mean)
	local s_5_2: di %4.3f r(sd)
su age_10 if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_3: di %4.3f r(mean)
	local s_5_3: di %4.3f r(sd)	
su NE if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_4: di %4.3f r(mean)
	local s_5_4: di %4.3f r(sd)	
su MW if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_5: di %4.3f r(mean)
	local s_5_5: di %4.3f r(sd)	
su W if foreign_10==0 [aw=1/link_prob_midpoint_10]
	local b_5_6: di %4.3f r(mean)
	local s_5_6: di %4.3f r(sd)	
	
su avg_rank_10 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_7: di %4.3f r(mean)
	local s_5_7: di %4.3f r(sd)
	local n_5_f=r(N)
su avg_rank_40 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_8: di %4.3f r(mean)
	local s_5_8: di %4.3f r(sd)
su age_10 if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_9: di %4.3f r(mean)
	local s_5_9: di %4.3f r(sd)	
su NE if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_10: di %4.3f r(mean)
	local s_5_10: di %4.3f r(sd)	
su MW if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_11: di %4.3f r(mean)
	local s_5_11: di %4.3f r(sd)	
su W if foreign_10==1 [aw=1/link_prob_midpoint_10]
	local b_5_12: di %4.3f r(mean)
	local s_5_12: di %4.3f r(sd)	
	
local n_5=`n_5_n'+`n_5_f'
local n_5: di %7.0fc `n_5'
local n_5_n: di %7.0fc `n_5_n'
local n_5_f: di %7.0fc `n_5_f'

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////

capture texdoc close
texdoc init "$repfolder/results/TableA1", replace force

tex \begin{threeparttable}
tex \newcolumntype{d}{D{.}{.}{3}} 
tex \begin{tabular}{lddddd}
tex \hline
tex \addlinespace[2pt]
tex & \multicolumn{1}{c}{(1)} & \multicolumn{1}{c}{(2)} & \multicolumn{1}{c}{(3)} & \multicolumn{1}{c}{(4)} & \multicolumn{1}{c}{(5)} \\
tex \addlinespace[2pt]
tex & \multicolumn{1}{c}{1850--1880} & \multicolumn{1}{c}{1870--1900} & \multicolumn{1}{c}{1880--1910} & \multicolumn{1}{c}{1900--1930} & \multicolumn{1}{c}{1910--1940} \\
tex \hline
tex \addlinespace[2pt]

tex \multicolumn{6}{l}{\emph{Panel A: Natives}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Rank & `b_1_1' & `b_2_1' & `b_3_1' & `b_4_1' & `b_5_1' \\
tex & (`s_1_1') & (`s_2_1') & (`s_3_1') & (`s_4_1') & (`s_5_1') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final Rank & `b_1_2' & `b_2_2' & `b_3_2' & `b_4_2' & `b_5_2' \\
tex & (`s_1_2') & (`s_2_2') & (`s_3_2') & (`s_4_2') & (`s_5_2') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Age & `b_1_3' & `b_2_3' & `b_3_3' & `b_4_3' & `b_5_3' \\
tex & (`s_1_3') & (`s_2_3') & (`s_3_3') & (`s_4_3') & (`s_5_3') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Region \\
tex \hspace*{2em}Northeast & `b_1_4' & `b_2_4' & `b_3_4' & `b_4_4' & `b_5_4' \\
tex \addlinespace[2pt]
tex \hspace*{2em}Midwest & `b_1_5' & `b_2_5' & `b_3_5' & `b_4_5' & `b_5_5' \\
tex \addlinespace[2pt]
tex \hspace*{2em}West & `b_1_6' & `b_2_6' & `b_3_6' & `b_4_6' & `b_5_6' \\
tex \addlinespace[2pt]

tex \hline

tex \addlinespace[2pt]
tex \multicolumn{6}{l}{\emph{Panel B: Immigrants}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Rank & `b_1_7' & `b_2_7' & `b_3_7' & `b_4_7' & `b_5_7' \\
tex & (`s_1_7') & (`s_2_7') & (`s_3_7') & (`s_4_7') & (`s_5_7') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final Rank & `b_1_8' & `b_2_8' & `b_3_8' & `b_4_8' & `b_5_8' \\
tex & (`s_1_8') & (`s_2_8') & (`s_3_8') & (`s_4_8') & (`s_5_8') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Age & `b_1_9' & `b_2_9' & `b_3_9' & `b_4_9' & `b_5_9' \\
tex & (`s_1_9') & (`s_2_9') & (`s_3_9') & (`s_4_9') & (`s_5_9') \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial Region \\
tex \hspace*{2em}Northeast & `b_1_10' & `b_2_10' & `b_3_10' & `b_4_10' & `b_5_10' \\
tex \addlinespace[2pt]
tex \hspace*{2em}Midwest & `b_1_11' & `b_2_11' & `b_3_11' & `b_4_11' & `b_5_11' \\
tex \addlinespace[2pt]
tex \hspace*{2em}West & `b_1_12' & `b_2_12' & `b_3_12' & `b_4_12' & `b_5_12' \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

tex Natives & \multicolumn{1}{r}{`n_1_n'} & \multicolumn{1}{r}{`n_2_n'} & \multicolumn{1}{r}{`n_3_n'} & \multicolumn{1}{r}{`n_4_n'} & \multicolumn{1}{r}{`n_5_n'} \\
tex \addlinespace[2pt]
tex Immigrants & \multicolumn{1}{r}{`n_1_f'} & \multicolumn{1}{r}{`n_2_f'} & \multicolumn{1}{r}{`n_3_f'} & \multicolumn{1}{r}{`n_4_f'} & \multicolumn{1}{r}{`n_5_f'} \\
tex \addlinespace[2pt]
tex Total & \multicolumn{1}{r}{`n_1'} & \multicolumn{1}{r}{`n_2'} & \multicolumn{1}{r}{`n_3'} & \multicolumn{1}{r}{`n_4'} & \multicolumn{1}{r}{`n_5'} \\
tex \addlinespace[2pt]
tex \hline

tex \hline
tex \end{tabular}
tex \end{threeparttable}

texdoc close
