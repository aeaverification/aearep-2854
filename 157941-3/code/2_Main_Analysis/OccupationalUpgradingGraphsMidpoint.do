cd "$repfolder/data/analysis"

///////////////////////////////

use FileRecatProbit_1850_1880, clear
merge 1:1 histid_50 using RescoredRecatProbit_1850, keep(1 3)
gen rescored_50=(_merge==3)
drop _merge
replace unskill_50=0 if rescored_50==1

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_80-avg_rank_midpoint_50
rename foreign_50 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_50
	gen native_craft=native*craft_50
	gen native_operative=native*operative_50
	gen native_unskill=native*unskill_50
	gen native_rescored=native*rescored_50
	gen foreign_wc=foreign*wc_50
	gen foreign_farmer=foreign*farmer_50
	gen foreign_craft=foreign*craft_50
	gen foreign_operative=foreign*operative_50
	gen foreign_unskill=foreign*unskill_50
	gen foreign_rescored=foreign*rescored_50
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6, labsize(medium) angle(0) glwidth(0)) xlabel(, labsize(medium)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants") size(medium))
	graph export "$repfolder/results/Figure9a.pdf", replace
	
	gen irish=(bpl_50==414)
	gen irish_wc=irish*wc_50
	gen irish_farmer=irish*farmer_50
	gen irish_craft=irish*craft_50
	gen irish_operative=irish*operative_50
	gen irish_unskill=irish*unskill_50
	gen irish_rescored=irish*rescored_50
	
	gen german=(bpl_50==453)
	gen german_wc=german*wc_50
	gen german_farmer=german*farmer_50
	gen german_craft=german*craft_50
	gen german_operative=german*operative_50
	gen german_unskill=german*unskill_50
	gen german_rescored=german*rescored_50
	
	gen british=(bpl_50==410 | bpl_50==411 | bpl_50==412 | bpl_50==413)
	gen british_wc=british*wc_50
	gen british_farmer=british*farmer_50
	gen british_craft=british*craft_50
	gen british_operative=british*operative_50
	gen british_unskill=british*unskill_50
	gen british_rescored=british*rescored_50
	
	gen other=(foreign==1 & (bpl_50<410 | bpl_50>414) & bpl_50~=453)
	gen other_wc=other*wc_50
	gen other_farmer=other*farmer_50
	gen other_craft=other*craft_50
	gen other_operative=other*operative_50
	gen other_unskill=other*unskill_50
	gen other_rescored=other*rescored_50
	
	drop main*
	gen main_wc=irish_wc
	gen main_farmer=irish_farmer
	gen main_craft=irish_craft
	gen main_operative=irish_operative
	gen main_unskill=irish_unskill
	gen main_rescored=irish_rescored
	
	reg delta_rank_midpoint native_* main_* german_* british_* other_* c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto irish
	
	drop main*
	gen main_wc=british_wc
	gen main_farmer=british_farmer
	gen main_craft=british_craft
	gen main_operative=british_operative
	gen main_unskill=british_unskill
	gen main_rescored=british_rescored
	
	reg delta_rank_midpoint native_* main_* german_* irish_* other_* c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto british
	
	drop main*
	gen main_wc=german_wc
	gen main_farmer=german_farmer
	gen main_craft=german_craft
	gen main_operative=german_operative
	gen main_unskill=german_unskill
	gen main_rescored=german_rescored
	
	reg delta_rank_midpoint native_* main_* british_* irish_* other_* c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto german
	
	label var main_wc "White Collar"
		label var main_farmer "Farmer"
		label var main_craft "Craft"
		label var main_operative "Operative"
		label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
		
	coefplot ///
		(native, mcolor(blue) ciopts(color(blue))) ///
		(irish, mcolor(green) msymbol(T) ciopts(color(green))) ///
		(british, mcolor(red) msymbol(D) ciopts(color(red))) ///
		(german, mcolor(black) msymbol(X) ciopts(color(black))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		yscale(range(-.2 .6)) ylabel(-.2(.2).6,angle(0) glwidth(0)) xlabel(, labsize(medium)) offset(0) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Irish" 6 "British" 8 "Germans"))
	graph export "$repfolder/results/FigureA6a.pdf", replace
	
///////////////////////////////

use File_1870_1900, clear
merge 1:1 histid_70 using Rescored_1870, keep(1 3)
gen rescored_70=(_merge==3)
drop _merge
replace unskill_70=0 if rescored_70==1

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_00-avg_rank_midpoint_70
rename foreign_00 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_70
	gen native_craft=native*craft_70
	gen native_operative=native*operative_70
	gen native_unskill=native*unskill_70
	gen native_rescored=native*rescored_70
	gen foreign_wc=foreign*wc_70
	gen foreign_farmer=foreign*farmer_70
	gen foreign_craft=foreign*craft_70
	gen foreign_operative=foreign*operative_70
	gen foreign_unskill=foreign*unskill_70
	gen foreign_rescored=foreign*rescored_70
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6, labsize(medium) angle(0) glwidth(0)) xlabel(, labsize(medium)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants") size(medium))
	graph export "$repfolder/results/Figure9b.pdf", replace
	
	gen irish=(bpl_70==414)
	gen irish_wc=irish*wc_70
	gen irish_farmer=irish*farmer_70
	gen irish_craft=irish*craft_70
	gen irish_operative=irish*operative_70
	gen irish_unskill=irish*unskill_70
	gen irish_rescored=irish*rescored_70
	
	gen german=(bpl_70==453)
	gen german_wc=german*wc_70
	gen german_farmer=german*farmer_70
	gen german_craft=german*craft_70
	gen german_operative=german*operative_70
	gen german_unskill=german*unskill_70
	gen german_rescored=german*rescored_70
	
	gen british=(bpl_70==410 | bpl_70==411 | bpl_70==412 | bpl_70==413)
	gen british_wc=british*wc_70
	gen british_farmer=british*farmer_70
	gen british_craft=british*craft_70
	gen british_operative=british*operative_70
	gen british_unskill=british*unskill_70
	gen british_rescored=british*rescored_70

	gen italian=(bpl_70==434)
	gen italian_wc=italian*wc_70
	gen italian_farmer=italian*farmer_70
	gen italian_craft=italian*craft_70
	gen italian_operative=italian*operative_70
	gen italian_unskill=italian*unskill_70
	gen italian_rescored=italian*rescored_70
	
	gen russian=(bpl_70==465)
	gen russian_wc=russian*wc_70
	gen russian_farmer=russian*farmer_70
	gen russian_craft=russian*craft_70
	gen russian_operative=russian*operative_70
	gen russian_unskill=russian*unskill_70
	gen russian_rescored=russian*rescored_70
	
	gen polish=(bpl_70==455)
	gen polish_wc=polish*wc_70
	gen polish_farmer=polish*farmer_70
	gen polish_craft=polish*craft_70
	gen polish_operative=polish*operative_70
	gen polish_unskill=polish*unskill_70
	gen polish_rescored=polish*rescored_70
	
	gen swedish=(bpl_70==405)
	gen swedish_wc=swedish*wc_70
	gen swedish_farmer=swedish*farmer_70
	gen swedish_craft=swedish*craft_70
	gen swedish_operative=swedish*operative_70
	gen swedish_unskill=swedish*unskill_70
	gen swedish_rescored=swedish*rescored_70
	
	gen norwegian=(bpl_70==404)
	gen norwegian_wc=norwegian*wc_70
	gen norwegian_farmer=norwegian*farmer_70
	gen norwegian_craft=norwegian*craft_70
	gen norwegian_operative=norwegian*operative_70
	gen norwegian_unskill=norwegian*unskill_70
	gen norwegian_rescored=norwegian*rescored_70
	
	gen austrian=(bpl_70==450)
	gen austrian_wc=austrian*wc_70
	gen austrian_farmer=austrian*farmer_70
	gen austrian_craft=austrian*craft_70
	gen austrian_operative=austrian*operative_70
	gen austrian_unskill=austrian*unskill_70
	gen austrian_rescored=austrian*rescored_70
	
	gen czechoslovak=(bpl_70==452)
	gen czechoslovak_wc=czechoslovak*wc_70
	gen czechoslovak_farmer=czechoslovak*farmer_70
	gen czechoslovak_craft=czechoslovak*craft_70
	gen czechoslovak_operative=czechoslovak*operative_70
	gen czechoslovak_unskill=czechoslovak*unskill_70
	gen czechoslovak_rescored=czechoslovak*rescored_70
	
	gen other=(foreign==1 & bpl_70~=414 & bpl_70~=453 & bpl_70~=410 & bpl_70~=411 & bpl_70~=412 & bpl_70~=413 & bpl_70~=434 & bpl_70~=465 & bpl_70~=455 & bpl_70~=405 & bpl_70~=404 & bpl_70~=450 & bpl_70~=452)
	gen other_wc=other*wc_70
	gen other_farmer=other*farmer_70
	gen other_craft=other*craft_70
	gen other_operative=other*operative_70
	gen other_unskill=other*unskill_70
	gen other_rescored=other*rescored_70
	
	drop main*
	gen main_wc=irish_wc
	gen main_farmer=irish_farmer
	gen main_craft=irish_craft
	gen main_operative=irish_operative
	gen main_unskill=irish_unskill
	gen main_rescored=irish_rescored
	
	reg delta_rank_midpoint native_* main_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto irish
	
	drop main*
	gen main_wc=british_wc
	gen main_farmer=british_farmer
	gen main_craft=british_craft
	gen main_operative=british_operative
	gen main_unskill=british_unskill
	gen main_rescored=british_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* main_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto british
	
	drop main*
	gen main_wc=german_wc
	gen main_farmer=german_farmer
	gen main_craft=german_craft
	gen main_operative=german_operative
	gen main_unskill=german_unskill
	gen main_rescored=german_rescored
	
	reg delta_rank_midpoint native_* irish_* main_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto german
	
	drop main*
	gen main_wc=italian_wc
	gen main_farmer=italian_farmer
	gen main_craft=italian_craft
	gen main_operative=italian_operative
	gen main_unskill=italian_unskill
	gen main_rescored=italian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* main_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto italian
	
	drop main*
	gen main_wc=russian_wc
	gen main_farmer=russian_farmer
	gen main_craft=russian_craft
	gen main_operative=russian_operative
	gen main_unskill=russian_unskill
	gen main_rescored=russian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* main_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto russian
	
	drop main*
	gen main_wc=polish_wc
	gen main_farmer=polish_farmer
	gen main_craft=polish_craft
	gen main_operative=polish_operative
	gen main_unskill=polish_unskill
	gen main_rescored=polish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* main_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto polish
	
	drop main*
	gen main_wc=swedish_wc
	gen main_farmer=swedish_farmer
	gen main_craft=swedish_craft
	gen main_operative=swedish_operative
	gen main_unskill=swedish_unskill
	gen main_rescored=swedish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* main_* norwegian_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto swedish
	
	drop main*
	gen main_wc=norwegian_wc
	gen main_farmer=norwegian_farmer
	gen main_craft=norwegian_craft
	gen main_operative=norwegian_operative
	gen main_unskill=norwegian_unskill
	gen main_rescored=norwegian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* main_* austrian_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto norwegian

	drop main*
	gen main_wc=austrian_wc
	gen main_farmer=austrian_farmer
	gen main_craft=austrian_craft
	gen main_operative=austrian_operative
	gen main_unskill=austrian_unskill
	gen main_rescored=austrian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* main_* czechoslovak_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto austrian
	
	drop main*
	gen main_wc=czechoslovak_wc
	gen main_farmer=czechoslovak_farmer
	gen main_craft=czechoslovak_craft
	gen main_operative=czechoslovak_operative
	gen main_unskill=czechoslovak_unskill
	gen main_rescored=czechoslovak_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* main_* other_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto czechoslovak
	
	label var main_wc "White Collar"
		label var main_farmer "Farmer"
		label var main_craft "Craft"
		label var main_operative "Operative"
		label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	coefplot ///
		(native, mcolor(blue) msymbol(O) ciopts(color(blue))) ///
		(irish, mcolor(green) msymbol(T) ciopts(color(green))) ///
		(british, mcolor(red) msymbol(D) ciopts(color(red))) ///
		(german, mcolor(black) msymbol(X) ciopts(color(black))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		yscale(range(-.2 .6)) ylabel(-.2(.2).6,angle(0) glwidth(0)) xlabel(, labsize(medium)) offset(0) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Irish" 6 "British" 8 "Germans"))
	graph export "$repfolder/results/FigureA6b.pdf", replace

////////////////////////////

use File_1880_1910, clear
merge 1:1 histid_80 using Rescored_1880, keep(1 3)
gen rescored_80=(_merge==3)
drop _merge
replace unskill_80=0 if rescored_80==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_10-avg_rank_midpoint_80
rename foreign_10 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_80
	gen native_craft=native*craft_80
	gen native_operative=native*operative_80
	gen native_unskill=native*unskill_80
	gen native_rescored=native*rescored_80
	gen foreign_wc=foreign*wc_80
	gen foreign_farmer=foreign*farmer_80
	gen foreign_craft=foreign*craft_80
	gen foreign_operative=foreign*operative_80
	gen foreign_unskill=foreign*unskill_80
	gen foreign_rescored=foreign*rescored_80
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6, labsize(medium) angle(0) glwidth(0)) xlabel(, labsize(medium)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants") size(medium))
	graph export "$repfolder/results/Figure9c.pdf", replace
	
	gen irish=(bpl_80==414)
	gen irish_wc=irish*wc_80
	gen irish_farmer=irish*farmer_80
	gen irish_craft=irish*craft_80
	gen irish_operative=irish*operative_80
	gen irish_unskill=irish*unskill_80
	gen irish_rescored=irish*rescored_80
	
	gen german=(bpl_80==453)
	gen german_wc=german*wc_80
	gen german_farmer=german*farmer_80
	gen german_craft=german*craft_80
	gen german_operative=german*operative_80
	gen german_unskill=german*unskill_80
	gen german_rescored=german*rescored_80
	
	gen british=(bpl_80==410 | bpl_80==411 | bpl_80==412 | bpl_80==413)
	gen british_wc=british*wc_80
	gen british_farmer=british*farmer_80
	gen british_craft=british*craft_80
	gen british_operative=british*operative_80
	gen british_unskill=british*unskill_80
	gen british_rescored=british*rescored_80

	gen italian=(bpl_80==434)
	gen italian_wc=italian*wc_80
	gen italian_farmer=italian*farmer_80
	gen italian_craft=italian*craft_80
	gen italian_operative=italian*operative_80
	gen italian_unskill=italian*unskill_80
	gen italian_rescored=italian*rescored_80
	
	gen russian=(bpl_80==465)
	gen russian_wc=russian*wc_80
	gen russian_farmer=russian*farmer_80
	gen russian_craft=russian*craft_80
	gen russian_operative=russian*operative_80
	gen russian_unskill=russian*unskill_80
	gen russian_rescored=russian*rescored_80
	
	gen polish=(bpl_80==455)
	gen polish_wc=polish*wc_80
	gen polish_farmer=polish*farmer_80
	gen polish_craft=polish*craft_80
	gen polish_operative=polish*operative_80
	gen polish_unskill=polish*unskill_80
	gen polish_rescored=polish*rescored_80
	
	gen swedish=(bpl_80==405)
	gen swedish_wc=swedish*wc_80
	gen swedish_farmer=swedish*farmer_80
	gen swedish_craft=swedish*craft_80
	gen swedish_operative=swedish*operative_80
	gen swedish_unskill=swedish*unskill_80
	gen swedish_rescored=swedish*rescored_80
	
	gen norwegian=(bpl_80==404)
	gen norwegian_wc=norwegian*wc_80
	gen norwegian_farmer=norwegian*farmer_80
	gen norwegian_craft=norwegian*craft_80
	gen norwegian_operative=norwegian*operative_80
	gen norwegian_unskill=norwegian*unskill_80
	gen norwegian_rescored=norwegian*rescored_80
	
	gen austrian=(bpl_80==450)
	gen austrian_wc=austrian*wc_80
	gen austrian_farmer=austrian*farmer_80
	gen austrian_craft=austrian*craft_80
	gen austrian_operative=austrian*operative_80
	gen austrian_unskill=austrian*unskill_80
	gen austrian_rescored=austrian*rescored_80
	
	gen czechoslovak=(bpl_80==452)
	gen czechoslovak_wc=czechoslovak*wc_80
	gen czechoslovak_farmer=czechoslovak*farmer_80
	gen czechoslovak_craft=czechoslovak*craft_80
	gen czechoslovak_operative=czechoslovak*operative_80
	gen czechoslovak_unskill=czechoslovak*unskill_80
	gen czechoslovak_rescored=czechoslovak*rescored_80
	
	gen other=(foreign==1 & bpl_80~=414 & bpl_80~=453 & bpl_80~=410 & bpl_80~=411 & bpl_80~=412 & bpl_80~=413 & bpl_80~=434 & bpl_80~=465 & bpl_80~=455 & bpl_80~=405 & bpl_80~=404 & bpl_80~=450 & bpl_80~=452)
	gen other_wc=other*wc_80
	gen other_farmer=other*farmer_80
	gen other_craft=other*craft_80
	gen other_operative=other*operative_80
	gen other_unskill=other*unskill_80
	gen other_rescored=other*rescored_80
	
	drop main*
	gen main_wc=irish_wc
	gen main_farmer=irish_farmer
	gen main_craft=irish_craft
	gen main_operative=irish_operative
	gen main_unskill=irish_unskill
	gen main_rescored=irish_rescored
	
	reg delta_rank_midpoint native_* main_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto irish
	
	drop main*
	gen main_wc=british_wc
	gen main_farmer=british_farmer
	gen main_craft=british_craft
	gen main_operative=british_operative
	gen main_unskill=british_unskill
	gen main_rescored=british_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* main_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto british
	
	drop main*
	gen main_wc=german_wc
	gen main_farmer=german_farmer
	gen main_craft=german_craft
	gen main_operative=german_operative
	gen main_unskill=german_unskill
	gen main_rescored=german_rescored
	
	reg delta_rank_midpoint native_* irish_* main_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto german
	
	drop main*
	gen main_wc=italian_wc
	gen main_farmer=italian_farmer
	gen main_craft=italian_craft
	gen main_operative=italian_operative
	gen main_unskill=italian_unskill
	gen main_rescored=italian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* main_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto italian
	
	drop main*
	gen main_wc=russian_wc
	gen main_farmer=russian_farmer
	gen main_craft=russian_craft
	gen main_operative=russian_operative
	gen main_unskill=russian_unskill
	gen main_rescored=russian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* main_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto russian
	
	drop main*
	gen main_wc=polish_wc
	gen main_farmer=polish_farmer
	gen main_craft=polish_craft
	gen main_operative=polish_operative
	gen main_unskill=polish_unskill
	gen main_rescored=polish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* main_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto polish
	
	drop main*
	gen main_wc=swedish_wc
	gen main_farmer=swedish_farmer
	gen main_craft=swedish_craft
	gen main_operative=swedish_operative
	gen main_unskill=swedish_unskill
	gen main_rescored=swedish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* main_* norwegian_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto swedish
	
	drop main*
	gen main_wc=norwegian_wc
	gen main_farmer=norwegian_farmer
	gen main_craft=norwegian_craft
	gen main_operative=norwegian_operative
	gen main_unskill=norwegian_unskill
	gen main_rescored=norwegian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* main_* austrian_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto norwegian

	drop main*
	gen main_wc=austrian_wc
	gen main_farmer=austrian_farmer
	gen main_craft=austrian_craft
	gen main_operative=austrian_operative
	gen main_unskill=austrian_unskill
	gen main_rescored=austrian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* main_* czechoslovak_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto austrian
	
	drop main*
	gen main_wc=czechoslovak_wc
	gen main_farmer=czechoslovak_farmer
	gen main_craft=czechoslovak_craft
	gen main_operative=czechoslovak_operative
	gen main_unskill=czechoslovak_unskill
	gen main_rescored=czechoslovak_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* main_* other_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto czechoslovak
	
	label var main_wc "White Collar"
		label var main_farmer "Farmer"
		label var main_craft "Craft"
		label var main_operative "Operative"
		label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	coefplot ///
		(native, mcolor(blue) msymbol(O) ciopts(color(blue))) ///
		(irish, mcolor(green) msymbol(T) ciopts(color(green))) ///
		(british, mcolor(red) msymbol(D) ciopts(color(red))) ///
		(german, mcolor(black) msymbol(X) ciopts(color(black))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		yscale(range(-.2 .6)) ylabel(-.2(.2).6,angle(0) glwidth(0)) xlabel(, labsize(medium)) offset(0) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Irish" 6 "British" 8 "Germans"))
	graph export "$repfolder/results/FigureA6c.pdf", replace
	
////////////////////////////

use File_1900_1930, clear
merge 1:1 histid_00 using Rescored_1900, keep(1 3)
gen rescored_00=(_merge==3)
drop _merge
replace unskill_00=0 if rescored_00==1

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_30-avg_rank_midpoint_00
rename foreign_30 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_00
	gen native_craft=native*craft_00
	gen native_operative=native*operative_00
	gen native_unskill=native*unskill_00
	gen native_rescored=native*rescored_00
	gen foreign_wc=foreign*wc_00
	gen foreign_farmer=foreign*farmer_00
	gen foreign_craft=foreign*craft_00
	gen foreign_operative=foreign*operative_00
	gen foreign_unskill=foreign*unskill_00
	gen foreign_rescored=foreign*rescored_00
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6, labsize(medium) angle(0) glwidth(0)) xlabel(, labsize(medium)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants") size(medium))
	graph export "$repfolder/results/Figure9d.pdf", replace
	
	gen irish=(bpl_00==414)
	gen irish_wc=irish*wc_00
	gen irish_farmer=irish*farmer_00
	gen irish_craft=irish*craft_00
	gen irish_operative=irish*operative_00
	gen irish_unskill=irish*unskill_00
	gen irish_rescored=irish*rescored_00
	
	gen german=(bpl_00==453)
	gen german_wc=german*wc_00
	gen german_farmer=german*farmer_00
	gen german_craft=german*craft_00
	gen german_operative=german*operative_00
	gen german_unskill=german*unskill_00
	gen german_rescored=german*rescored_00
	
	gen british=(bpl_00==410 | bpl_00==411 | bpl_00==412 | bpl_00==413)
	gen british_wc=british*wc_00
	gen british_farmer=british*farmer_00
	gen british_craft=british*craft_00
	gen british_operative=british*operative_00
	gen british_unskill=british*unskill_00
	gen british_rescored=british*rescored_00

	gen italian=(bpl_00==434)
	gen italian_wc=italian*wc_00
	gen italian_farmer=italian*farmer_00
	gen italian_craft=italian*craft_00
	gen italian_operative=italian*operative_00
	gen italian_unskill=italian*unskill_00
	gen italian_rescored=italian*rescored_00
	
	gen russian=(bpl_00==465)
	gen russian_wc=russian*wc_00
	gen russian_farmer=russian*farmer_00
	gen russian_craft=russian*craft_00
	gen russian_operative=russian*operative_00
	gen russian_unskill=russian*unskill_00
	gen russian_rescored=russian*rescored_00
	
	gen polish=(bpl_00==455)
	gen polish_wc=polish*wc_00
	gen polish_farmer=polish*farmer_00
	gen polish_craft=polish*craft_00
	gen polish_operative=polish*operative_00
	gen polish_unskill=polish*unskill_00
	gen polish_rescored=polish*rescored_00
	
	gen swedish=(bpl_00==405)
	gen swedish_wc=swedish*wc_00
	gen swedish_farmer=swedish*farmer_00
	gen swedish_craft=swedish*craft_00
	gen swedish_operative=swedish*operative_00
	gen swedish_unskill=swedish*unskill_00
	gen swedish_rescored=swedish*rescored_00
	
	gen norwegian=(bpl_00==404)
	gen norwegian_wc=norwegian*wc_00
	gen norwegian_farmer=norwegian*farmer_00
	gen norwegian_craft=norwegian*craft_00
	gen norwegian_operative=norwegian*operative_00
	gen norwegian_unskill=norwegian*unskill_00
	gen norwegian_rescored=norwegian*rescored_00
	
	gen austrian=(bpl_00==450)
	gen austrian_wc=austrian*wc_00
	gen austrian_farmer=austrian*farmer_00
	gen austrian_craft=austrian*craft_00
	gen austrian_operative=austrian*operative_00
	gen austrian_unskill=austrian*unskill_00
	gen austrian_rescored=austrian*rescored_00
	
	gen czechoslovak=(bpl_00==452)
	gen czechoslovak_wc=czechoslovak*wc_00
	gen czechoslovak_farmer=czechoslovak*farmer_00
	gen czechoslovak_craft=czechoslovak*craft_00
	gen czechoslovak_operative=czechoslovak*operative_00
	gen czechoslovak_unskill=czechoslovak*unskill_00
	gen czechoslovak_rescored=czechoslovak*rescored_00
	
	gen other=(foreign==1 & bpl_00~=414 & bpl_00~=453 & bpl_00~=410 & bpl_00~=411 & bpl_00~=412 & bpl_00~=413 & bpl_00~=434 & bpl_00~=465 & bpl_00~=455 & bpl_00~=405 & bpl_00~=404 & bpl_00~=450 & bpl_00~=452)
	gen other_wc=other*wc_00
	gen other_farmer=other*farmer_00
	gen other_craft=other*craft_00
	gen other_operative=other*operative_00
	gen other_unskill=other*unskill_00
	gen other_rescored=other*rescored_00
	
	drop main*
	gen main_wc=irish_wc
	gen main_farmer=irish_farmer
	gen main_craft=irish_craft
	gen main_operative=irish_operative
	gen main_unskill=irish_unskill
	gen main_rescored=irish_rescored
	
	reg delta_rank_midpoint native_* main_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto irish
	
	drop main*
	gen main_wc=british_wc
	gen main_farmer=british_farmer
	gen main_craft=british_craft
	gen main_operative=british_operative
	gen main_unskill=british_unskill
	gen main_rescored=british_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* main_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto british
	
	drop main*
	gen main_wc=german_wc
	gen main_farmer=german_farmer
	gen main_craft=german_craft
	gen main_operative=german_operative
	gen main_unskill=german_unskill
	gen main_rescored=german_rescored
	
	reg delta_rank_midpoint native_* irish_* main_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto german
	
	drop main*
	gen main_wc=italian_wc
	gen main_farmer=italian_farmer
	gen main_craft=italian_craft
	gen main_operative=italian_operative
	gen main_unskill=italian_unskill
	gen main_rescored=italian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* main_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto italian
	
	drop main*
	gen main_wc=russian_wc
	gen main_farmer=russian_farmer
	gen main_craft=russian_craft
	gen main_operative=russian_operative
	gen main_unskill=russian_unskill
	gen main_rescored=russian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* main_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto russian
	
	drop main*
	gen main_wc=polish_wc
	gen main_farmer=polish_farmer
	gen main_craft=polish_craft
	gen main_operative=polish_operative
	gen main_unskill=polish_unskill
	gen main_rescored=polish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* main_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto polish
	
	drop main*
	gen main_wc=swedish_wc
	gen main_farmer=swedish_farmer
	gen main_craft=swedish_craft
	gen main_operative=swedish_operative
	gen main_unskill=swedish_unskill
	gen main_rescored=swedish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* main_* norwegian_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto swedish
	
	drop main*
	gen main_wc=norwegian_wc
	gen main_farmer=norwegian_farmer
	gen main_craft=norwegian_craft
	gen main_operative=norwegian_operative
	gen main_unskill=norwegian_unskill
	gen main_rescored=norwegian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* main_* austrian_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto norwegian

	drop main*
	gen main_wc=austrian_wc
	gen main_farmer=austrian_farmer
	gen main_craft=austrian_craft
	gen main_operative=austrian_operative
	gen main_unskill=austrian_unskill
	gen main_rescored=austrian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* main_* czechoslovak_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto austrian
	
	drop main*
	gen main_wc=czechoslovak_wc
	gen main_farmer=czechoslovak_farmer
	gen main_craft=czechoslovak_craft
	gen main_operative=czechoslovak_operative
	gen main_unskill=czechoslovak_unskill
	gen main_rescored=czechoslovak_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* main_* other_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto czechoslovak
	
	label var main_wc "White Collar"
		label var main_farmer "Farmer"
		label var main_craft "Craft"
		label var main_operative "Operative"
		label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	coefplot ///
		(native, mcolor(blue) msymbol(O) ciopts(color(blue))) ///
		(irish, mcolor(green) msymbol(T) ciopts(color(green))) ///
		(british, mcolor(red) msymbol(D) ciopts(color(red))) ///
		(german, mcolor(black) msymbol(X) ciopts(color(black))) ///
		(italian, mcolor(blue) msymbol(+) ciopts(color(blue))) ///
		(russian, mcolor(green) msymbol(Dh) ciopts(color(green))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		yscale(range(-.2 .6)) ylabel(-.2(.2).6,angle(0) glwidth(0)) xlabel(, labsize(medium)) offset(0) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Irish" 6 "British" 8 "Germans" 10 "Italians" 12 "Russians"))
	graph export "$repfolder/results/FigureA6d.pdf", replace
	
////////////////////////////

use File_1910_1940, clear
merge 1:1 histid_10 using Rescored_1910, keep(1 3)
gen rescored_10=(_merge==3)
drop _merge
replace unskill_10=0 if rescored_10==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_40-avg_rank_midpoint_10
rename foreign_10 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_10
	gen native_craft=native*craft_10
	gen native_operative=native*operative_10
	gen native_unskill=native*unskill_10
	gen native_rescored=native*rescored_10
	gen foreign_wc=foreign*wc_10
	gen foreign_farmer=foreign*farmer_10
	gen foreign_craft=foreign*craft_10
	gen foreign_operative=foreign*operative_10
	gen foreign_unskill=foreign*unskill_10
	gen foreign_rescored=foreign*rescored_10
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6, labsize(medium) angle(0) glwidth(0)) xlabel(, labsize(medium)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants") size(medium))
	graph export "$repfolder/results/Figure9e.pdf", replace
	
	gen irish=(bpl_10==414)
	gen irish_wc=irish*wc_10
	gen irish_farmer=irish*farmer_10
	gen irish_craft=irish*craft_10
	gen irish_operative=irish*operative_10
	gen irish_unskill=irish*unskill_10
	gen irish_rescored=irish*rescored_10
	
	gen german=(bpl_10==453)
	gen german_wc=german*wc_10
	gen german_farmer=german*farmer_10
	gen german_craft=german*craft_10
	gen german_operative=german*operative_10
	gen german_unskill=german*unskill_10
	gen german_rescored=german*rescored_10
	
	gen british=(bpl_10==410 | bpl_10==411 | bpl_10==412 | bpl_10==413)
	gen british_wc=british*wc_10
	gen british_farmer=british*farmer_10
	gen british_craft=british*craft_10
	gen british_operative=british*operative_10
	gen british_unskill=british*unskill_10
	gen british_rescored=british*rescored_10

	gen italian=(bpl_10==434)
	gen italian_wc=italian*wc_10
	gen italian_farmer=italian*farmer_10
	gen italian_craft=italian*craft_10
	gen italian_operative=italian*operative_10
	gen italian_unskill=italian*unskill_10
	gen italian_rescored=italian*rescored_10
	
	gen russian=(bpl_10==465)
	gen russian_wc=russian*wc_10
	gen russian_farmer=russian*farmer_10
	gen russian_craft=russian*craft_10
	gen russian_operative=russian*operative_10
	gen russian_unskill=russian*unskill_10
	gen russian_rescored=russian*rescored_10
	
	gen polish=(bpl_10==455)
	gen polish_wc=polish*wc_10
	gen polish_farmer=polish*farmer_10
	gen polish_craft=polish*craft_10
	gen polish_operative=polish*operative_10
	gen polish_unskill=polish*unskill_10
	gen polish_rescored=polish*rescored_10
	
	gen swedish=(bpl_10==405)
	gen swedish_wc=swedish*wc_10
	gen swedish_farmer=swedish*farmer_10
	gen swedish_craft=swedish*craft_10
	gen swedish_operative=swedish*operative_10
	gen swedish_unskill=swedish*unskill_10
	gen swedish_rescored=swedish*rescored_10
	
	gen norwegian=(bpl_10==404)
	gen norwegian_wc=norwegian*wc_10
	gen norwegian_farmer=norwegian*farmer_10
	gen norwegian_craft=norwegian*craft_10
	gen norwegian_operative=norwegian*operative_10
	gen norwegian_unskill=norwegian*unskill_10
	gen norwegian_rescored=norwegian*rescored_10
	
	gen austrian=(bpl_10==450)
	gen austrian_wc=austrian*wc_10
	gen austrian_farmer=austrian*farmer_10
	gen austrian_craft=austrian*craft_10
	gen austrian_operative=austrian*operative_10
	gen austrian_unskill=austrian*unskill_10
	gen austrian_rescored=austrian*rescored_10
	
	gen czechoslovak=(bpl_10==452)
	gen czechoslovak_wc=czechoslovak*wc_10
	gen czechoslovak_farmer=czechoslovak*farmer_10
	gen czechoslovak_craft=czechoslovak*craft_10
	gen czechoslovak_operative=czechoslovak*operative_10
	gen czechoslovak_unskill=czechoslovak*unskill_10
	gen czechoslovak_rescored=czechoslovak*rescored_10
	
	gen other=(foreign==1 & bpl_10~=414 & bpl_10~=453 & bpl_10~=410 & bpl_10~=411 & bpl_10~=412 & bpl_10~=413 & bpl_10~=434 & bpl_10~=465 & bpl_10~=455 & bpl_10~=405 & bpl_10~=404 & bpl_10~=450 & bpl_10~=452)
	gen other_wc=other*wc_10
	gen other_farmer=other*farmer_10
	gen other_craft=other*craft_10
	gen other_operative=other*operative_10
	gen other_unskill=other*unskill_10
	gen other_rescored=other*rescored_10
	
	drop main*
	gen main_wc=irish_wc
	gen main_farmer=irish_farmer
	gen main_craft=irish_craft
	gen main_operative=irish_operative
	gen main_unskill=irish_unskill
	gen main_rescored=irish_rescored
	
	reg delta_rank_midpoint native_* main_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto irish
	
	drop main*
	gen main_wc=british_wc
	gen main_farmer=british_farmer
	gen main_craft=british_craft
	gen main_operative=british_operative
	gen main_unskill=british_unskill
	gen main_rescored=british_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* main_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto british
	
	drop main*
	gen main_wc=german_wc
	gen main_farmer=german_farmer
	gen main_craft=german_craft
	gen main_operative=german_operative
	gen main_unskill=german_unskill
	gen main_rescored=german_rescored
	
	reg delta_rank_midpoint native_* irish_* main_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto german
	
	drop main*
	gen main_wc=italian_wc
	gen main_farmer=italian_farmer
	gen main_craft=italian_craft
	gen main_operative=italian_operative
	gen main_unskill=italian_unskill
	gen main_rescored=italian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* main_* russian_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto italian
	
	drop main*
	gen main_wc=russian_wc
	gen main_farmer=russian_farmer
	gen main_craft=russian_craft
	gen main_operative=russian_operative
	gen main_unskill=russian_unskill
	gen main_rescored=russian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* main_* polish_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto russian
	
	drop main*
	gen main_wc=polish_wc
	gen main_farmer=polish_farmer
	gen main_craft=polish_craft
	gen main_operative=polish_operative
	gen main_unskill=polish_unskill
	gen main_rescored=polish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* main_* swedish_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto polish
	
	drop main*
	gen main_wc=swedish_wc
	gen main_farmer=swedish_farmer
	gen main_craft=swedish_craft
	gen main_operative=swedish_operative
	gen main_unskill=swedish_unskill
	gen main_rescored=swedish_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* main_* norwegian_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto swedish
	
	drop main*
	gen main_wc=norwegian_wc
	gen main_farmer=norwegian_farmer
	gen main_craft=norwegian_craft
	gen main_operative=norwegian_operative
	gen main_unskill=norwegian_unskill
	gen main_rescored=norwegian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* main_* austrian_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto norwegian

	drop main*
	gen main_wc=austrian_wc
	gen main_farmer=austrian_farmer
	gen main_craft=austrian_craft
	gen main_operative=austrian_operative
	gen main_unskill=austrian_unskill
	gen main_rescored=austrian_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* main_* czechoslovak_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto austrian
	
	drop main*
	gen main_wc=czechoslovak_wc
	gen main_farmer=czechoslovak_farmer
	gen main_craft=czechoslovak_craft
	gen main_operative=czechoslovak_operative
	gen main_unskill=czechoslovak_unskill
	gen main_rescored=czechoslovak_rescored
	
	reg delta_rank_midpoint native_* irish_* german_* british_* italian_* russian_* polish_* swedish_* norwegian_* austrian_* main_* other_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto czechoslovak
	
	label var main_wc "White Collar"
		label var main_farmer "Farmer"
		label var main_craft "Craft"
		label var main_operative "Operative"
		label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	coefplot ///
		(native, mcolor(blue) msymbol(O) ciopts(color(blue))) ///
		(irish, mcolor(green) msymbol(T) ciopts(color(green))) ///
		(british, mcolor(red) msymbol(D) ciopts(color(red))) ///
		(german, mcolor(black) msymbol(X) ciopts(color(black))) ///
		(italian, mcolor(blue) msymbol(+) ciopts(color(blue))) ///
		(russian, mcolor(green) msymbol(Dh) ciopts(color(green))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		yscale(range(-.2 .6)) ylabel(-.2(.2).6,angle(0) glwidth(0)) xlabel(, labsize(medium)) offset(0) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)", size(medium)) ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Irish" 6 "British" 8 "Germans" 10 "Italians" 12 "Russians"))
	graph export "$repfolder/results/FigureA6e.pdf", replace
	
	
	
