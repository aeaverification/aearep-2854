cd "$repfolder/data/analysis"

///////////////////////////

use ObservablesLinkageRecatProbit_1850_1880, clear
reshape wide age-phrank_upper, i(foreign) j(linked)

foreach var in age urban_clean farm_clean lit_clean wc farmer craft ///
	operative unskill numperhh realprop_clean proprank proprank_age proprank_state phrank head ///
	proprank_midpoint proprank_upper phrank_midpoint phrank_upper {
		gen `var'_rat=`var'1/`var'0
}

rename age_rat v1
rename urban_clean_rat v2
rename farm_clean_rat v3 
rename lit_clean_rat v4
rename wc_rat v5
rename farmer_rat v6
rename craft_rat v7
rename operative_rat v8
rename unskill_rat v9
rename numperhh_rat v10
rename realprop_clean_rat v11
rename proprank_rat v12
rename proprank_age_rat v13
rename proprank_state_rat v14
rename phrank_rat v15
rename head_rat v16
rename proprank_midpoint_rat v17
rename proprank_upper_rat v18
rename phrank_midpoint_rat v19
rename phrank_upper_rat v20

reshape long v, i(foreign) j(var)

twoway bar v var if foreign==0, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Real Property" ///
	12 "Wealth Score Rank" 13 "Wealth Score Rank, Age" 14 "Wealth Score Rank, State" ///
	15 "PH Rank" 16 "HH Head" 17 "Wealth Rank Mid" 18 "Wealth Rank Upper" 19 "PH Rank Mid" 20 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB1a.pdf", replace

twoway bar v var if foreign==1, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Real Property" ///
	12 "Wealth Score Rank" 13 "Wealth Score Rank, Age" 14 "Wealth Score Rank, State" ///
	15 "PH Rank" 16 "HH Head" 17 "Wealth Rank Mid" 18 "Wealth Rank Upper" 19 "PH Rank Mid" 20 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB2a.pdf", replace

///////////////////////////

use ObservablesLinkage_1870_1900, clear
drop *impute
reshape wide age-phrank_upper, i(foreign) j(linked)

foreach var in age urban_clean farm_clean lit_clean wc farmer craft ///
	operative unskill numperhh ffor english own_mort own_free marst_clean proprank proprank_age proprank_state phrank head ///
	proprank_midpoint proprank_upper phrank_midpoint phrank_upper {
		gen `var'_rat=`var'1/`var'0
}

rename age_rat v1
rename urban_clean_rat v2
rename farm_clean_rat v3 
rename lit_clean_rat v4
rename wc_rat v5
rename farmer_rat v6
rename craft_rat v7
rename operative_rat v8
rename unskill_rat v9
rename numperhh_rat v10
rename own_mort_rat v11
rename own_free_rat v12
rename marst_clean_rat v13
rename proprank_rat v14
rename proprank_age_rat v15
rename proprank_state_rat v16
rename phrank_rat v17
rename head_rat v18
rename ffor_rat v19
rename english_rat v20
rename proprank_midpoint_rat v21
rename proprank_upper_rat v22
rename phrank_midpoint_rat v23
rename phrank_upper_rat v24

reshape long v, i(foreign) j(var)

twoway bar v var if foreign==0, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB1b.pdf", replace

twoway bar v var if foreign==1, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB2b.pdf", replace

///////////////////////////

use ObservablesLinkage_1880_1910, clear
drop *impute
reshape wide age-phrank_upper, i(foreign) j(linked)

foreach var in age urban_clean farm_clean lit_clean wc farmer craft ///
	operative unskill numprec ffor english own_mort own_free marst_clean proprank proprank_age proprank_state phrank head ///
	proprank_midpoint proprank_upper phrank_midpoint phrank_upper {
		gen `var'_rat=`var'1/`var'0
}

rename age_rat v1
rename urban_clean_rat v2
rename farm_clean_rat v3 
rename lit_clean_rat v4
rename wc_rat v5
rename farmer_rat v6
rename craft_rat v7
rename operative_rat v8
rename unskill_rat v9
rename numprec_rat v10
rename own_mort_rat v11
rename own_free_rat v12
rename marst_clean_rat v13
rename proprank_rat v14
rename proprank_age_rat v15
rename proprank_state_rat v16
rename phrank_rat v17
rename head_rat v18
rename ffor_rat v19
rename english_rat v20
rename proprank_midpoint_rat v21
rename proprank_upper_rat v22
rename phrank_midpoint_rat v23
rename phrank_upper_rat v24

reshape long v, i(foreign) j(var)

twoway bar v var if foreign==0, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB1c.pdf", replace

twoway bar v var if foreign==1, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB2c.pdf", replace

///////////////////////////

use ObservablesLinkage_1900_1930, clear
drop *impute
reshape wide age-phrank_upper, i(foreign) j(linked)

foreach var in age urban_clean farm_clean lit_clean wc farmer craft ///
	operative unskill numperhh ffor english own valueh_clean marst_clean proprank proprank_age proprank_state phrank head /// 
	proprank_midpoint proprank_upper phrank_midpoint phrank_upper {
		gen `var'_rat=`var'1/`var'0
}

rename age_rat v1
rename urban_clean_rat v2
rename farm_clean_rat v3 
rename lit_clean_rat v4
rename wc_rat v5
rename farmer_rat v6
rename craft_rat v7
rename operative_rat v8
rename unskill_rat v9
rename numperhh_rat v10
rename own_rat v11
rename valueh_clean_rat v12
rename marst_clean_rat v13
rename proprank_rat v14
rename proprank_age_rat v15
rename proprank_state_rat v16
rename phrank_rat v17
rename head_rat v18
rename ffor_rat v19
rename english_rat v20
rename proprank_midpoint_rat v21
rename proprank_upper_rat v22
rename phrank_midpoint_rat v23
rename phrank_upper_rat v24

reshape long v, i(foreign) j(var)

twoway bar v var if foreign==0, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own Home" 12 "Home Value" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB1d.pdf", replace

twoway bar v var if foreign==1, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own Home" 12 "Home Value" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB2d.pdf", replace

///////////////////////////

use ObservablesLinkage_1910_1940, clear
drop *impute
reshape wide age-phrank_upper, i(foreign) j(linked)

foreach var in age urban_clean farm_clean lit_clean wc farmer craft ///
	operative unskill numprec ffor english own_mort own_free marst_clean proprank proprank_age proprank_state phrank head ///
	proprank_midpoint proprank_upper phrank_midpoint phrank_upper {
		gen `var'_rat=`var'1/`var'0
}

rename age_rat v1
rename urban_clean_rat v2
rename farm_clean_rat v3 
rename lit_clean_rat v4
rename wc_rat v5
rename farmer_rat v6
rename craft_rat v7
rename operative_rat v8
rename unskill_rat v9
rename numprec_rat v10
rename own_mort_rat v11
rename own_free_rat v12
rename marst_clean_rat v13
rename proprank_rat v14
rename proprank_age_rat v15
rename proprank_state_rat v16
rename phrank_rat v17
rename head_rat v18
rename ffor_rat v19
rename english_rat v20
rename proprank_midpoint_rat v21
rename proprank_upper_rat v22
rename phrank_midpoint_rat v23
rename phrank_upper_rat v24

reshape long v, i(foreign) j(var)

twoway bar v var if foreign==0, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB1e.pdf", replace

twoway bar v var if foreign==1, yscale(range(0 1.4)) ylabel(0(.2)1.4, angle(0) glwidth(0)) ///
	barwidth(.75) ytitle("Linked-Full Sample Ratio") graphregion(color(white)) ///
	color(black) yline(1,lcolor(gray) lwidth(.05)) xtitle("") ///
	xlabel(1 "Age" 2 "Urban" 3 "Farm" 4 "Literate" 5 "White Collar" 6 ///
	"Farmer" 7 "Craft" 8 "Operative" 9 "Unskill" 10 "HH Size" 11 "Own (Mortgage)" 12 "Own (Free)" 13 "Married" ///
	14 "Wealth Score Rank" 15 "Wealth Score Rank, Age" 16 "Wealth Score Rank, State" 17 "PH Rank" 18 "HH Head" 19 "Father Foreign" 20 "English" ///
	21 "Wealth Rank Mid" 22" Wealth Rank Upper" 23 "PH Rank Mid" 24 "PH Rank Upper", labsize(2.5) angle(45))
graph export "$repfolder/results/FigureB2e.pdf", replace

///////////////////////////
