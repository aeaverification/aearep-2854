#install.packages('ipumsr') # install if needed
library(ipumsr) # load ipumsr library
library(tools)

# function to extract data
extract_from_ipums <- function(year) {
  setwd(extract_defs)
  extract_name <- paste0("usa_extract_",year,".txt") # name extract
  
  extract_definition <- define_extract_from_json(extract_name)
  submitted_extract <- submit_extract(extract_definition)
  submitted_extract <- get_extract_info(submitted_extract)
  downloadable_extract <- wait_for_extract(submitted_extract, max_delay_seconds = 150)
  ddi_file <- download_extract(downloadable_extract, download_dir = extractbase, overwrite=TRUE)
  
  # unzip dat.gz file
  base_filename <- file_path_sans_ext(basename(ddi_file))
  R.utils::gunzip(file.path(paste0(extractbase, "/", base_filename, ".dat.gz")))
  
  # rename file
  setwd(extractbase)
  file.rename(from = paste0(base_filename, ".dat"), to = paste0(year, "_raw.dat")) # rename dat file
  file.rename(from = paste0(base_filename, ".xml"), to = paste0(year, "_raw.xml")) # rename xml file
  
  print(paste("extract completed for", year, sep=" "))
}

# make changes below:
# note: this file should be in the same directory as the extract definition txt files

# set api key from .Renviron
set_ipums_api_key(Sys.getenv("API_KEY")) 

# directory with extract definitions
extract_defs <- "U:/Documents/Workspace/aearep-2854/157941/extract"

# directory to save extracts
extractbase <- "U:/Documents/Workspace/aearep-2854/157941/data/confidential/ipums_data"


# define years
year <- c(1860, 1870, 1880, 1900, 1910, 1920, 1930, 1940)


# call function for all years
for (i in 1:length(year)) {
  extract_year <- year[i]
  extract_from_ipums(extract_year)
}

