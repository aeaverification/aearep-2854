cd "$repfolder/data/analysis"

capture postclose uncon_con
postfile uncon_con span conditional b s farmfam using UnconditionalConditionalResults_20Yr, replace

////////////////////////////

use FileRecatProbit_1850_1870, clear

_pctile link_prob_50, percentiles(0.5)
replace link_prob_50=r(r1) if link_prob_50<r(r1)
gen delta_rank=avg_rank_70-avg_rank_50

gen initial_rank=avg_rank_50
label var initial_rank "Initial Avg.~Occ.~Rank"

reg delta_rank foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_50], robust
post uncon_con (1) (0) (_b[foreign_50]) (_se[foreign_50]) (0)

reg delta_rank initial_rank foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_50], robust
post uncon_con (1) (1) (_b[foreign_50]) (_se[foreign_50]) (0)
	
/////////////////////////////

use File_1880_1900, clear

_pctile link_prob_00, percentiles(0.5)
replace link_prob_00=r(r1) if link_prob_00<r(r1)
gen delta_rank=avg_rank_00-avg_rank_80

gen initial_rank=avg_rank_80
label var initial_rank "Initial Avg.~Occ.~Rank"

reg delta_rank foreign_00 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_00], robust
post uncon_con (2) (0) (_b[foreign_00]) (_se[foreign_00]) (0)

reg delta_rank initial_rank foreign_00 c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_00], robust
post uncon_con (2) (1) (_b[foreign_00]) (_se[foreign_00]) (0)

/////////////////////////////

use File_1900_1920, clear

_pctile link_prob_20, percentiles(0.5)
replace link_prob_20=r(r1) if link_prob_20<r(r1)
gen delta_rank=avg_rank_20-avg_rank_00

gen initial_rank=avg_rank_00
label var initial_rank "Initial Avg.~Occ.~Rank"

reg delta_rank foreign_20 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_20], robust
post uncon_con (3) (0) (_b[foreign_20]) (_se[foreign_20]) (0)

reg delta_rank initial_rank foreign_20 c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_20], robust
post uncon_con (3) (1) (_b[foreign_20]) (_se[foreign_20]) (0)

/////////////////////////////

use File_1910_1930, clear

_pctile link_prob_30, percentiles(0.5)
replace link_prob_30=r(r1) if link_prob_30<r(r1)
gen delta_rank=avg_rank_30-avg_rank_10

gen initial_rank=avg_rank_10
label var initial_rank "Initial Avg.~Occ.~Rank"

reg delta_rank foreign_30 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_30], robust
post uncon_con (4) (0) (_b[foreign_30]) (_se[foreign_30]) (0)

reg delta_rank initial_rank foreign_30 c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_30], robust
post uncon_con (4) (1) (_b[foreign_30]) (_se[foreign_30]) (0)

/////////////////////////////

use File_1920_1940, clear

_pctile link_prob_20, percentiles(0.5)
replace link_prob_20=r(r1) if link_prob_20<r(r1)
gen delta_rank=avg_rank_40-avg_rank_20

gen initial_rank=avg_rank_20
label var initial_rank "Initial Avg.~Occ.~Rank"

reg delta_rank foreign_20 c.age_20##c.age_20##c.age_20##c.age_20 [aw=1/link_prob_20], robust
post uncon_con (5) (0) (_b[foreign_20]) (_se[foreign_20]) (0)

reg delta_rank initial_rank foreign_20 c.age_20##c.age_20##c.age_20##c.age_20 [aw=1/link_prob_20], robust
post uncon_con (5) (1) (_b[foreign_20]) (_se[foreign_20]) (0)
	
postclose uncon_con

