libname storage "1880_3-3";
libname data "Files1880New";

proc format cntlin=storage.us1880e_usa_f;
run;

data import(rename=(bpl=bpld));
	set storage.cens1880(keep=serial pernum sex age namefrst namelast bpl);
	where sex=1 and namelast~="" and namefrst~="" and namefrst~="[MR]";
	namelast=upcase(namelast);
	namefrst=upcase(namefrst);

	namegivcorr=compress(namefrst,,'ask');
	namelastcorr=compress(namelast,,'ask');
run;

data import;
	set import;
	bpl=floor(bpld/100);
run;

data census1880;
	set import;
	mark=0;
run;

data census1880(drop=position);
	set census1880;
	position=prxmatch("/ [A-Z]$/",strip(namegivcorr));
	if position=0 then namefrstcorr=namegivcorr;
	else namefrstcorr=strip(substr(namegivcorr,1,position));
	if position>0 then namemidcorr=strip(substr(namegivcorr,position,2));
run;

data census1880;
	set census1880;

	namefrstcorr=compress(namefrstcorr,,'ak');
	namelastcorr=compress(namelastcorr,,'ak');
	namegivcorr=compress(namegivcorr,,'ak');
run;

data data.census1880_cleaned_target;
	set census1880(keep=serial pernum age namefrstcorr namelastcorr namemidcorr mark bpl);
	where namefrstcorr~="" and namelastcorr~="" and length(namefrstcorr)>1 and length(namelastcorr)>2;
run;

