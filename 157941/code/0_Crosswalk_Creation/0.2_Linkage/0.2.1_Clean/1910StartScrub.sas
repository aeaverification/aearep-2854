libname storage "1910_3-2";
libname data "Files1910";

proc format cntlin=storage.us1910m_usa_f;
run;

data import(rename=(bpl=bpld));
	set storage.cens1910(keep=serial pernum sex age namefrst namelast bpl);
	where sex=1 and namelast~="" and namefrst~="" and namefrst~="[MR]";
	namelast=upcase(namelast);
	namefrst=upcase(namefrst);

	namegivcorr=compress(namefrst,,'ask');
	namelastcorr=compress(namelast,,'ask');
run;

data import;
	set import;
	bpl=floor(bpld/100);
run;

proc sql;
	create table census1910 as select
	a.serial as serial, a.pernum as pernum, a.age as age,
	a.namelast as namelast, a.namefrst as namefrst,
	a.namegivcorr as namegivcorr, a.namelastcorr as namelastcorr,
	a.bpl as bpl,
	b.mark as mark
	from import as a left join data.census1910_drops as b
	on a.serial=b.serial and a.pernum=b.pernum;
quit;

data census1910(drop=mark);
	set census1910;
	where mark~=1;
run;

data census1910(drop=position);
	set census1910;
	position=prxmatch("/ [A-Z]$/",strip(namegivcorr));
	if position=0 then namefrstcorr=namegivcorr;
	else namefrstcorr=strip(substr(namegivcorr,1,position));
	if position>0 then namemidcorr=strip(substr(namegivcorr,position,2));
run;

data census1910;
	set census1910;

	namefrstcorr=compress(namefrstcorr,,'ak');
	namelastcorr=compress(namelastcorr,,'ak');
	namegivcorr=compress(namegivcorr,,'ak');
run;

data data.census1910_cleaned_start;
	set census1910(keep=serial pernum age namefrstcorr namelastcorr namemidcorr bpl);
	where namefrstcorr~="" and namelastcorr~="" and length(namefrstcorr)>1 and length(namelastcorr)>2;
run;
