cd "$repfolder/data/analysis"

//Import 1870 census
use occ1950 serial sex age region relate race school realprop persprop labforce stateicp using "$repfolder/data/confidential/ipums_data/1870_100pct", clear
replace occ1950=820 if occ1950==830

keep if race==1 & sex==1 & age>=30 & age<=65  & labforce==2
drop if persprop==999998 | realprop==999998

gen totprop=realprop+persprop

fillin occ1950 stateicp
rename _fillin _fillin_a

gen     occ1950broad=1 if occ1950<=99
replace occ1950broad=2 if occ1950>=100 & occ1950<=123
replace occ1950broad=3 if occ1950>=200 & occ1950<=290
replace occ1950broad=4 if occ1950>=300 & occ1950<=390
replace occ1950broad=5 if occ1950>=400 & occ1950<=490
replace occ1950broad=6 if occ1950>=500 & occ1950<=595
replace occ1950broad=7 if occ1950>=600 & occ1950<=690
replace occ1950broad=8 if occ1950>=700 & occ1950<=720 
replace occ1950broad=9 if occ1950>=730 & occ1950<=790 
replace occ1950broad=10 if occ1950>=800 & occ1950<=840
replace occ1950broad=11 if occ1950>=900 & occ1950<=970

label define broadocclabel 1 "professional" 2 "farmers and farm managers" 3 "managers and proprietors" /*
*/ 4 "clerks and similar" 5 "sales" 6 "crafts" 7 "operatives" 8 "priv. hh serv" 9 "not-priv hh serv" /*
*/ 10 "farm labor" 11 "labor"

label values occ1950broad broadocclabel

preserve

//Create file for narrow occupational categories
collapse (mean) totprop occ1950broad (count) N=totprop, by(occ1950 stateicp)
sort occ1950 stateicp

save OccWealth70State_Narrow, replace

restore 

//Create file for broad occupational categories
collapse (mean) totpropb=totprop (count) Nb=totprop, by(occ1950broad stateicp)
sort occ1950broad stateicp

save OccWealth70State_Broad, replace
