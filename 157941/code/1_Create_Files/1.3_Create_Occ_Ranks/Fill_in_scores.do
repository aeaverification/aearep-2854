//Manually classify some remaining 1900-based occupational income scores

gen missing=inc1900==.

replace inc1900=865 if occ1950==70
replace inc1900=865 if occ1950==71
replace inc1900=661 if occ1950==545
replace inc1900=657 if occ1950==550
replace inc1900=521 if occ1950==621 
replace inc1900=255 if occ1950==830 
replace inc1900=373 if occ1950==920 
replace inc1900=546 if occ1950==960 

replace inc1900=425 if occ1950==602 
replace inc1900=485 if occ1950==604
replace inc1900=489 if occ1950==610
replace inc1900=461 if occ1950==611
replace inc1900=482 if occ1950==613
replace inc1900=373 if occ1950==614
replace inc1900=373 if occ1950==615


replace inc1900=865 if inc1900==. & occ1950>=0 & occ1950<=99 
replace inc1900=725 if inc1900==. & occ1950==230 
replace inc1900=758 if inc1900==. & occ1950==280 
replace inc1900=685 if inc1900==. & occ1950==592 
replace inc1900=1500 if inc1900==. & occ1950==480
replace inc1900=350 if inc1900==.  & (occ1950>=700 & occ1950<=720)
replace inc1900=676 if inc1900==. & occ1950==672 

replace inc1900=685 if inc1900==. & occ1950==524 | occ1950==531 | occ1950==535

replace inc1900=661 if inc1900==. & ind1950==246

replace inc1900=572 if inc1900==. & ind1950>=307 & ind1950<=309
replace inc1900=740 if inc1900==. & ind1950==316 
replace inc1900=500 if inc1900==. & ind1950==318 
replace inc1900=747 if inc1900==. & ind1950==319 
replace inc1900=781 if inc1900==. & ind1950==326 
replace inc1900=685 if inc1900==. & (ind1950==336 | ind1950==337 | ind1950==346)

replace inc1900=607 if inc1900==. & ind1950>=406 & ind1950<=426 

replace inc1900=530 if inc1900==. & ind1950>=436 & ind1950<=449 
replace inc1900=743 if inc1900==. & ind1950>=456 & ind1950<=459 
replace inc1900=560 if inc1900==. & ind1950>=466 & ind1950<=477 
 
replace inc1900=600 if inc1900==. & ind1950>=487 & ind1950<=489 
replace inc1900=676 if inc1900==. & ind1950==487 

replace inc1900=639 if inc1900==. & ind1950>=506 & ind1950<=579 
replace inc1900=620 if inc1900==. & ind1950>=606 & ind1950<=699 

replace missing=inc1900==.
