cd "$repfolder/data/analysis"

use FileRecatProbit_1850_1880, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/ProbabilitiesRecatProbit_1850_1880, replace

foreach suffix in ABEE ABEN Int IntPlus Age English Forward Impute Recent Second State {
	use FileRecatProbit_1850_1880_`suffix', clear
	
	keep histid* link_prob*
	di "`suffix'"
	duplicates report histid*

	save ../raw/linkage_probabilities/ProbabilitiesRecatProbit_1850_1880_`suffix', replace
}

////////////

use File_1870_1900, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1870_1900, replace

foreach suffix in ABEE ABEN Int IntPlus Age English Forward Impute Recent Second State {
	use File_1870_1900_`suffix', clear
	
	keep histid* link_prob*
	di "`suffix'"
	duplicates report histid*

	save ../raw/linkage_probabilities/Probabilities_1870_1900_`suffix', replace
}

////////////

use File_1880_1910, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1880_1910, replace

foreach suffix in ABEE ABEN Int IntPlus Age English Forward Impute Recent Second State {
	use File_1880_1910_`suffix', clear
	
	keep histid* link_prob*
	di "`suffix'"
	duplicates report histid*

	save ../raw/linkage_probabilities/Probabilities_1880_1910_`suffix', replace
}

////////////

use File_1900_1930, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1900_1930, replace

foreach suffix in ABEE ABEN Int IntPlus Age English Forward Impute Recent Second State {
	use File_1900_1930_`suffix', clear
	
	keep histid* link_prob*
	di "`suffix'"
	duplicates report histid*

	save ../raw/linkage_probabilities/Probabilities_1900_1930_`suffix', replace
}

////////////

use File_1910_1940, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1910_1940, replace

foreach suffix in ABEE ABEN Int IntPlus Age English Forward Impute Recent Second State {
	use File_1910_1940_`suffix', clear
	
	keep histid* link_prob*
	di "`suffix'"
	duplicates report histid*

	save ../raw/linkage_probabilities/Probabilities_1910_1940_`suffix', replace
}


////////////

use FileRecatProbit_1850_1870, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/ProbabilitiesRecatProbit_1850_1870, replace

////////////

use File_1880_1900, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1880_1900, replace

////////////

use File_1900_1920, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1900_1920, replace

////////////

use File_1910_1930, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1910_1930, replace

////////////

use File_1920_1940, clear

keep histid* link_prob*
duplicates report histid*

save ../raw/linkage_probabilities/Probabilities_1920_1940, replace
