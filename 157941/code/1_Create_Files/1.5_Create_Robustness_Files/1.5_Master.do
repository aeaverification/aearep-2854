cd "$repfolder/data/analysis"

//The code in this folder essentially repeats that in 1.4. The comments in each file mark cases where there is a difference from the benchmark.

do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/1.5.2_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.3_20Yr/1.5.3_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.4_Forward/1.5.4_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.5_Second/1.5.5_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.6_Recent/1.5.6_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.7_Scores/1.5.7_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.8_English/1.5.8_Master.do"
