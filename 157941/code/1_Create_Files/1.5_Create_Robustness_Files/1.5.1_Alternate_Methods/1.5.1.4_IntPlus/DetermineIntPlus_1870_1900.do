cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1900_100pct", clear
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=44 & age<=64 & race==1 & (region<30 | region>=40)

preserve
use "$repfolder/data/confidential/clp_linkage_crosswalks/crosswalk_1870_1900", clear
rename histid_1870 histid_70
rename histid_1900 histid_00
replace histid_70=upper(histid_70)
replace histid_00=upper(histid_00)
keep if abe_exact_conservative==1 & abe_nysiis_conservative==1
keep histid*
merge 1:1 histid* using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1870_1900"
keep if _merge==3
drop _merge
tempfile crosswalk
save `crosswalk'
restore

gen histid_00=upper(histid)
merge 1:1 histid_00 using `crosswalk', keep(1 3)
gen linked=(histid_70~="")
drop _merge histid

preserve
use  "$repfolder/data/confidential/ipums_data/1870_100pct", clear
replace occ1950=820 if occ1950==830

duplicates tag histid, gen(tag)
keep if tag==0
drop tag
gen histid_70=upper(histid)
drop histid
keep if sex==1
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_70
}
tempfile census70
save `census70'
restore
rename age age_00
merge m:1 histid_70 using `census70', keep(3)
drop _merge

		//Require detailed birthplace match
		drop if bpld~=bpld_70
	
		//Drop those who "lose" citizenship
		drop if citizen_70==2 & citizen==3
	
		//Drop those whose nativity changes
		drop if nativity~=nativity_70
	
		//Require racial match
		drop if race~=race_70
	
		//Drop those who "forget" how to read
		drop if lit_70==4 & lit==1
	
keep histid*
compress
save crosswalk_1870_1900_IntPlus, replace
