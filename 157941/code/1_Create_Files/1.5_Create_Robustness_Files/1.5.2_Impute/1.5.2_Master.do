cd "$repfolder/data/analysis"

do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/CreateRecatProbit_1850_1880_Impute.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/Create_1870_1900_Impute.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/Create_1880_1910_Impute.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/Create_1900_1930_Impute.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.2_Impute/Create_1910_1940_Impute.do"
