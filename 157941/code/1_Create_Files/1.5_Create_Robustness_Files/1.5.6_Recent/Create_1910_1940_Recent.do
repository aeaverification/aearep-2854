cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1910_100pct", clear
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=18 & age<=30 & race==1 & (region<30 | region>=40)

gen histid_10=upper(histid)
merge 1:1 histid_10 using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1910_1940", keep(1 3)
gen linked=(histid_40~="")
drop _merge histid

merge 1:1 histid_10 using OccRanks_1910, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

//Drop immigrants reporting an arrival year before 1900
drop if foreign==1 & yrimmig<1900

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850) if occ1950<=970

gen urban_clean=(urban==2) if urban~=.
gen farm_clean=(farm~=1)
gen lit_clean=(lit==4)

gen ffor=1-(fbpl<100)
gen english=(speakeng==2 | (speakeng==0 & foreign==0))
gen own_mort=(ownershp==1 & mortgage==3)
gen own_free=(ownershp==1 & mortgage==1)

gen marst_clean=(marst==1 | marst==2)

gen head=(relate==1)

if $simple == 0 {

	gen link_prob=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob=prob if link_prob==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob
	
	gen link_prob_midpoint=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob
	
	gen link_prob_upper=.
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip ffor own_mort own_free marst_clean if bpl<90
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob

	foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
		tab bpl if bpl==`x'
		probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.statefip english own_mort own_free marst_clean if bpl==`x'
			predict prob if e(sample)
			replace link_prob_upper=prob if link_prob_upper==. & prob~=.
			drop prob
	}

	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.numprec##c.numprec##c.numprec##c.numprec head i.bpl i.statefip english own_mort own_free marst_clean if foreign==1 & bpl~=404 & bpl~=405 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=425 & bpl~=426 & bpl~=434 & bpl~=450 & bpl~=453 & bpl~=455 & bpl~=465
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob
	
}

keep if linked==1
	
order *, alpha
order histid*, first
foreach x of varlist age-yrsusa2 {
	rename `x' `x'_10
}

preserve
use  "$repfolder/data/confidential/ipums_data/1940_100pct_male", clear
replace occ1950=820 if occ1950==830

gen histid_40=upper(histid)
drop histid
keep if sex==1 & (region<30 | region>40)
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_40
}
tempfile census40
save `census40'
restore
merge 1:1 histid_40 using `census40', keep(3)
drop _merge

merge 1:1 histid_40 using OccRanks_1940, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

foreach x of varlist lntotprop proprank lntotprop_age proprank_age lntotprop_state proprank_state inc1900 l_inc1900 phrank avg_rank avg_rank_age avg_rank_state *midpoint *upper {
	rename `x' `x'_40
}

gen wc_40=(occ1950_40<100 | (occ1950_40>=200 & occ1950_40<500)) if occ1950_40<=970
gen farmer_40=(occ1950_40>=100 & occ1950_40<200) if occ1950_40<=970
gen craft_40=(occ1950_40>=500 & occ1950_40<600) if occ1950_40<=970
gen operative_40=(occ1950_40>=600 & occ1950_40<700) if occ1950_40<=970
gen unskill_40=(occ1950_40>=700 & occ1950_40<=970 & occ1950_40~=850) if occ1950_40<=970

gen urban_clean_40=(urban_40==2) if urban_40~=.
gen farm_clean_40=(farm_40~=1)

gen head_40=(relate_40==1)

gen marst_clean_40=(marst_40==1 | marst_40==2 | marst_40==3)

if $simple == 1 {

	merge 1:1 histid* using ../raw/linkage_probabilities/Probabilities_1910_1940_Recent
	keep if _merge==3
	drop _merge

}

compress

save File_1910_1940_Recent, replace
