cd "$repfolder/data/analysis"

do "$repfolder/code/1_Create_Files/1.1_Recategorize_1850/RecatProbit1850.do"
do "$repfolder/code/1_Create_Files/1.2_Create_Wealth_Scores/1.2_Master.do"
do "$repfolder/code/1_Create_Files/1.3_Create_Occ_Ranks/1.3_Master.do"
do "$repfolder/code/1_Create_Files/1.4_Create_Main_Files/1.4_Master.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5_Master.do"

if $simple == 0 {
	do "$repfolder/code/1_Create_Files/ExtractLinkageProbabilities.do"
}
