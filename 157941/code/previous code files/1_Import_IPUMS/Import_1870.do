cd "$repfolder/data/analysis"

set more off

clear
quietly infix                    ///
  int     year          1-4      ///
  long    sample        5-10     ///
  double  serial        11-18    ///
  byte    numprec       19-20    ///
  byte    subsamp       21-22    ///
  double  hhwt          23-32    ///
  int     numperhh      33-36    ///
  int     dwsize        37-40    ///
  double  cluster       41-53    ///
  byte    region        54-55    ///
  byte    stateicp      56-57    ///
  byte    statefip      58-59    ///
  int     countyicp     60-63    ///
  long    countynhg     64-70    ///
  byte    urban         71-71    ///
  byte    metro         72-72    ///
  int     metarea       73-75    ///
  int     metaread      76-79    ///
  int     city          80-83    ///
  long    citypop       84-88    ///
  byte    sizepl        89-90    ///
  long    nengpop       91-95    ///
  long    urbpop        96-100   ///
  int     sea           101-103  ///
  byte    mdstatus      104-104  ///
  int     metdist       105-108  ///
  int     urbarea       109-112  ///
  int     cntry         113-115  ///
  byte    gq            116-116  ///
  byte    gqtype        117-117  ///
  int     gqtyped       118-120  ///
  byte    gqfunds       121-122  ///
  byte    farm          123-123  ///
  byte    nfams         124-125  ///
  byte    ncouples      126-126  ///
  byte    nmothers      127-127  ///
  byte    nfathers      128-128  ///
  int     reel          129-132  ///
  int     line          133-135  ///
  byte    split         136-136  ///
  double  splithid      137-144  ///
  int     splitnum      145-148  ///
  int     pernum        149-152  ///
  double  perwt         153-162  ///
  double  slwt          163-172  ///
  byte    famunit       173-174  ///
  byte    famsize       175-176  ///
  byte    momloc        177-178  ///
  byte    stepmom       179-179  ///
  byte    momrule_hist  180-180  ///
  byte    poploc        181-182  ///
  byte    steppop       183-183  ///
  byte    poprule_hist  184-184  ///
  byte    sploc         185-186  ///
  byte    sprule_hist   187-187  ///
  byte    nchild        188-188  ///
  byte    nchlt5        189-189  ///
  byte    nsibs         190-190  ///
  byte    eldch         191-192  ///
  byte    yngch         193-194  ///
  byte    relate        195-196  ///
  int     related       197-200  ///
  byte    imprel        201-202  ///
  byte    sex           203-203  ///
  int     age           204-206  ///
  byte    agemonth      207-208  ///
  int     birthyr       209-212  ///
  byte    race          213-213  ///
  int     raced         214-216  ///
  byte    hispan        217-217  ///
  int     hispand       218-220  ///
  int     bpl           221-223  ///
  long    bpld          224-228  ///
  byte    nativity      229-229  ///
  byte    citizen       230-230  ///
  byte    hisprule      231-231  ///
  byte    racesing      232-232  ///
  byte    racesingd     233-234  ///
  byte    school        235-235  ///
  byte    lit           236-236  ///
  byte    labforce      237-237  ///
  int     occ           238-241  ///
  int     occ1950       242-244  ///
  int     ind1950       245-247  ///
  long    realprop      248-253  ///
  long    persprop      254-259  ///
  byte    occscore      260-261  ///
  byte    sei           262-263  ///
  double  presgl        264-266  ///
  double  erscor50      267-270  ///
  double  edscor50      271-274  ///
  double  npboss50      275-278  ///
  byte    versionhist   279-280  ///
  str     histid        281-316  ///
  byte    sursim        317-318  ///
  byte    vote          319-319  ///
   using "$repfolder/data/confidential/ipums_data/1870_raw.dat"

replace hhwt         = hhwt         / 100
replace perwt        = perwt        / 100
replace slwt         = slwt         / 100
replace presgl       = presgl       / 10
replace erscor50     = erscor50     / 10
replace edscor50     = edscor50     / 10
replace npboss50     = npboss50     / 10

format serial       %8.0f
format hhwt         %10.2f
format cluster      %13.0f
format splithid     %8.0f
format perwt        %10.2f
format slwt         %10.2f
format presgl       %3.1f
format erscor50     %4.1f
format edscor50     %4.1f
format npboss50     %4.1f

label var year         `"Census year"'
label var sample       `"IPUMS sample identifier"'
label var serial       `"Household serial number"'
label var numprec      `"Number of person records following"'
label var subsamp      `"Subsample number"'
label var hhwt         `"Household weight"'
label var numperhh     `"Number of persons in household"'
label var dwsize       `"Dwelling size"'
label var cluster      `"Household cluster for variance estimation"'
label var region       `"Census region and division"'
label var stateicp     `"State (ICPSR code)"'
label var statefip     `"State (FIPS code)"'
label var countyicp    `"County (ICPSR code)"'
label var countynhg    `"County (NHGIS code)"'
label var urban        `"Urban/rural status"'
label var metro        `"Metropolitan status"'
label var metarea      `"Metropolitan area [general version]"'
label var metaread     `"Metropolitan area [detailed version]"'
label var city         `"City"'
label var citypop      `"City population"'
label var sizepl       `"Size of place"'
label var nengpop      `"New England population in minor civil division"'
label var urbpop       `"Population of urban places"'
label var sea          `"State Economic Area"'
label var mdstatus     `"Metropolitan district status"'
label var metdist      `"Metropolitan district"'
label var urbarea      `"Urbanized area"'
label var cntry        `"Country"'
label var gq           `"Group quarters status"'
label var gqtype       `"Group quarters type [general version]"'
label var gqtyped      `"Group quarters type [detailed version]"'
label var gqfunds      `"Group quarters funding"'
label var farm         `"Farm status"'
label var nfams        `"Number of families in household"'
label var ncouples     `"Number of couples in household"'
label var nmothers     `"Number of mothers in household"'
label var nfathers     `"Number of fathers in household"'
label var reel         `"Microfilm reel number"'
label var line         `"Line number"'
label var split        `"Large group quarters that was split up (100% datasets)"'
label var splithid     `"Household serial number, before large group quarters were split up (100% dataset"'
label var splitnum     `"Number of person records in household, before large group quarters were split up"'
label var pernum       `"Person number in sample unit"'
label var perwt        `"Person weight"'
label var slwt         `"Sample-line weight"'
label var famunit      `"Family unit membership"'
label var famsize      `"Number of own family members in household"'
label var momloc       `"Mother's location in the household"'
label var stepmom      `"Probable step/adopted mother"'
label var momrule_hist `"Rule for linking mother"'
label var poploc       `"Father's location in the household"'
label var steppop      `"Probable step/adopted father"'
label var poprule_hist `"Rule for linking father"'
label var sploc        `"Spouse's location in household"'
label var sprule_hist  `"Rule for linking spouse"'
label var nchild       `"Number of own children in the household"'
label var nchlt5       `"Number of own children under age 5 in household"'
label var nsibs        `"Number of own siblings in household"'
label var eldch        `"Age of eldest own child in household"'
label var yngch        `"Age of youngest own child in household"'
label var relate       `"Relationship to household head [general version]"'
label var related      `"Relationship to household head [detailed version]"'
label var imprel       `"Imputed relationship to household head"'
label var sex          `"Sex"'
label var age          `"Age"'
label var agemonth     `"Age in months"'
label var birthyr      `"Year of birth"'
label var race         `"Race [general version]"'
label var raced        `"Race [detailed version]"'
label var hispan       `"Hispanic origin [general version]"'
label var hispand      `"Hispanic origin [detailed version]"'
label var bpl          `"Birthplace [general version]"'
label var bpld         `"Birthplace [detailed version]"'
label var nativity     `"Foreign birthplace or parentage"'
label var citizen      `"Citizenship status"'
label var hisprule     `"Hispanic origin rule"'
label var racesing     `"Race: Single race identification [general version]"'
label var racesingd    `"Race: Single race identification [detailed version]"'
label var school       `"School attendance"'
label var lit          `"Literacy"'
label var labforce     `"Labor force status"'
label var occ          `"Occupation"'
label var occ1950      `"Occupation, 1950 basis"'
label var ind1950      `"Industry, 1950 basis"'
label var realprop     `"Real estate value"'
label var persprop     `"Value of personal estate"'
label var occscore     `"Occupational income score"'
label var sei          `"Duncan Socioeconomic Index "'
label var presgl       `"Occupational prestige score, Siegel"'
label var erscor50     `"Occupational earnings score, 1950 basis"'
label var edscor50     `"Occupational education score, 1950 basis"'
label var npboss50     `"Nam-Powers-Boyd occupational status score, 1950 basis"'
label var versionhist  `"Release version for historical data  "'
label var histid       `"Consistent historical data person identifier"'
label var sursim       `"Surname similarity"'
label var vote         `"Denied the vote"'

label define year_lbl 1850 `"1850"'
label define year_lbl 1860 `"1860"', add
label define year_lbl 1870 `"1870"', add
label define year_lbl 1880 `"1880"', add
label define year_lbl 1900 `"1900"', add
label define year_lbl 1910 `"1910"', add
label define year_lbl 1920 `"1920"', add
label define year_lbl 1930 `"1930"', add
label define year_lbl 1940 `"1940"', add
label define year_lbl 1950 `"1950"', add
label define year_lbl 1960 `"1960"', add
label define year_lbl 1970 `"1970"', add
label define year_lbl 1980 `"1980"', add
label define year_lbl 1990 `"1990"', add
label define year_lbl 2000 `"2000"', add
label define year_lbl 2001 `"2001"', add
label define year_lbl 2002 `"2002"', add
label define year_lbl 2003 `"2003"', add
label define year_lbl 2004 `"2004"', add
label define year_lbl 2005 `"2005"', add
label define year_lbl 2006 `"2006"', add
label define year_lbl 2007 `"2007"', add
label define year_lbl 2008 `"2008"', add
label define year_lbl 2009 `"2009"', add
label define year_lbl 2010 `"2010"', add
label define year_lbl 2011 `"2011"', add
label define year_lbl 2012 `"2012"', add
label define year_lbl 2013 `"2013"', add
label define year_lbl 2014 `"2014"', add
label define year_lbl 2015 `"2015"', add
label define year_lbl 2016 `"2016"', add
label define year_lbl 2017 `"2017"', add
label define year_lbl 2018 `"2018"', add
label define year_lbl 2019 `"2019"', add
label values year year_lbl

label define sample_lbl 201904 `"2015-2019, PRCS 5-year"'
label define sample_lbl 201903 `"2015-2019, ACS 5-year"', add
label define sample_lbl 201902 `"2019 PRCS"', add
label define sample_lbl 201901 `"2019 ACS"', add
label define sample_lbl 201804 `"2014-2018, PRCS 5-year"', add
label define sample_lbl 201803 `"2014-2018, ACS 5-year"', add
label define sample_lbl 201802 `"2018 PRCS"', add
label define sample_lbl 201801 `"2018 ACS"', add
label define sample_lbl 201704 `"2013-2017, PRCS 5-year"', add
label define sample_lbl 201703 `"2013-2017, ACS 5-year"', add
label define sample_lbl 201702 `"2017 PRCS"', add
label define sample_lbl 201701 `"2017 ACS"', add
label define sample_lbl 201604 `"2012-2016, PRCS 5-year"', add
label define sample_lbl 201603 `"2012-2016, ACS 5-year"', add
label define sample_lbl 201602 `"2016 PRCS"', add
label define sample_lbl 201601 `"2016 ACS"', add
label define sample_lbl 201504 `"2011-2015, PRCS 5-year"', add
label define sample_lbl 201503 `"2011-2015, ACS 5-year"', add
label define sample_lbl 201502 `"2015 PRCS"', add
label define sample_lbl 201501 `"2015 ACS"', add
label define sample_lbl 201404 `"2010-2014, PRCS 5-year"', add
label define sample_lbl 201403 `"2010-2014, ACS 5-year"', add
label define sample_lbl 201402 `"2014 PRCS"', add
label define sample_lbl 201401 `"2014 ACS"', add
label define sample_lbl 201306 `"2009-2013, PRCS 5-year"', add
label define sample_lbl 201305 `"2009-2013, ACS 5-year"', add
label define sample_lbl 201304 `"2011-2013, PRCS 3-year"', add
label define sample_lbl 201303 `"2011-2013, ACS 3-year"', add
label define sample_lbl 201302 `"2013 PRCS"', add
label define sample_lbl 201301 `"2013 ACS"', add
label define sample_lbl 201206 `"2008-2012, PRCS 5-year"', add
label define sample_lbl 201205 `"2008-2012, ACS 5-year"', add
label define sample_lbl 201204 `"2010-2012, PRCS 3-year"', add
label define sample_lbl 201203 `"2010-2012, ACS 3-year"', add
label define sample_lbl 201202 `"2012 PRCS"', add
label define sample_lbl 201201 `"2012 ACS"', add
label define sample_lbl 201106 `"2007-2011, PRCS 5-year"', add
label define sample_lbl 201105 `"2007-2011, ACS 5-year"', add
label define sample_lbl 201104 `"2009-2011, PRCS 3-year"', add
label define sample_lbl 201103 `"2009-2011, ACS 3-year"', add
label define sample_lbl 201102 `"2011 PRCS"', add
label define sample_lbl 201101 `"2011 ACS"', add
label define sample_lbl 201008 `"2010 Puerto Rico 10%"', add
label define sample_lbl 201007 `"2010 10%"', add
label define sample_lbl 201006 `"2006-2010, PRCS 5-year"', add
label define sample_lbl 201005 `"2006-2010, ACS 5-year"', add
label define sample_lbl 201004 `"2008-2010, PRCS 3-year"', add
label define sample_lbl 201003 `"2008-2010, ACS 3-year"', add
label define sample_lbl 201002 `"2010 PRCS"', add
label define sample_lbl 201001 `"2010 ACS"', add
label define sample_lbl 200906 `"2005-2009, PRCS 5-year"', add
label define sample_lbl 200905 `"2005-2009, ACS 5-year"', add
label define sample_lbl 200904 `"2007-2009, PRCS 3-year"', add
label define sample_lbl 200903 `"2007-2009, ACS 3-year"', add
label define sample_lbl 200902 `"2009 PRCS"', add
label define sample_lbl 200901 `"2009 ACS"', add
label define sample_lbl 200804 `"2006-2008, PRCS 3-year"', add
label define sample_lbl 200803 `"2006-2008, ACS 3-year"', add
label define sample_lbl 200802 `"2008 PRCS"', add
label define sample_lbl 200801 `"2008 ACS"', add
label define sample_lbl 200704 `"2005-2007, PRCS 3-year"', add
label define sample_lbl 200703 `"2005-2007, ACS 3-year"', add
label define sample_lbl 200702 `"2007 PRCS"', add
label define sample_lbl 200701 `"2007 ACS"', add
label define sample_lbl 200602 `"2006 PRCS"', add
label define sample_lbl 200601 `"2006 ACS"', add
label define sample_lbl 200502 `"2005 PRCS"', add
label define sample_lbl 200501 `"2005 ACS"', add
label define sample_lbl 200401 `"2004 ACS"', add
label define sample_lbl 200301 `"2003 ACS"', add
label define sample_lbl 200201 `"2002 ACS"', add
label define sample_lbl 200101 `"2001 ACS"', add
label define sample_lbl 200008 `"2000 Puerto Rico 1%"', add
label define sample_lbl 200007 `"2000 1%"', add
label define sample_lbl 200006 `"2000 Puerto Rico 1% sample (old version)"', add
label define sample_lbl 200005 `"2000 Puerto Rico 5%"', add
label define sample_lbl 200004 `"2000 ACS"', add
label define sample_lbl 200003 `"2000 Unweighted 1%"', add
label define sample_lbl 200002 `"2000 1% sample (old version)"', add
label define sample_lbl 200001 `"2000 5%"', add
label define sample_lbl 199007 `"1990 Puerto Rico 1%"', add
label define sample_lbl 199006 `"1990 Puerto Rico 5%"', add
label define sample_lbl 199005 `"1990 Labor Market Area"', add
label define sample_lbl 199004 `"1990 Elderly"', add
label define sample_lbl 199003 `"1990 Unweighted 1%"', add
label define sample_lbl 199002 `"1990 1%"', add
label define sample_lbl 199001 `"1990 5%"', add
label define sample_lbl 198007 `"1980 Puerto Rico 1%"', add
label define sample_lbl 198006 `"1980 Puerto Rico 5%"', add
label define sample_lbl 198005 `"1980 Detailed metro/non-metro"', add
label define sample_lbl 198004 `"1980 Labor Market Area"', add
label define sample_lbl 198003 `"1980 Urban/Rural"', add
label define sample_lbl 198002 `"1980 1%"', add
label define sample_lbl 198001 `"1980 5%"', add
label define sample_lbl 197009 `"1970 Puerto Rico Neighborhood"', add
label define sample_lbl 197008 `"1970 Puerto Rico Municipio"', add
label define sample_lbl 197007 `"1970 Puerto Rico State"', add
label define sample_lbl 197006 `"1970 Form 2 Neighborhood"', add
label define sample_lbl 197005 `"1970 Form 1 Neighborhood"', add
label define sample_lbl 197004 `"1970 Form 2 Metro"', add
label define sample_lbl 197003 `"1970 Form 1 Metro"', add
label define sample_lbl 197002 `"1970 Form 2 State"', add
label define sample_lbl 197001 `"1970 Form 1 State"', add
label define sample_lbl 196002 `"1960 5%"', add
label define sample_lbl 196001 `"1960 1%"', add
label define sample_lbl 195001 `"1950 1%"', add
label define sample_lbl 194002 `"1940 100% database"', add
label define sample_lbl 194001 `"1940 1%"', add
label define sample_lbl 193004 `"1930 100% database"', add
label define sample_lbl 193003 `"1930 Puerto Rico"', add
label define sample_lbl 193002 `"1930 5%"', add
label define sample_lbl 193001 `"1930 1%"', add
label define sample_lbl 192003 `"1920 100% database"', add
label define sample_lbl 192002 `"1920 Puerto Rico sample"', add
label define sample_lbl 192001 `"1920 1%"', add
label define sample_lbl 191004 `"1910 100% database"', add
label define sample_lbl 191003 `"1910 1.4% sample with oversamples"', add
label define sample_lbl 191002 `"1910 1%"', add
label define sample_lbl 191001 `"1910 Puerto Rico"', add
label define sample_lbl 190004 `"1900 100% database"', add
label define sample_lbl 190003 `"1900 1% sample with oversamples"', add
label define sample_lbl 190002 `"1900 1%"', add
label define sample_lbl 190001 `"1900 5%"', add
label define sample_lbl 188003 `"1880 100% database"', add
label define sample_lbl 188002 `"1880 10%"', add
label define sample_lbl 188001 `"1880 1%"', add
label define sample_lbl 187003 `"1870 100% database"', add
label define sample_lbl 187002 `"1870 1% sample with black oversample"', add
label define sample_lbl 187001 `"1870 1%"', add
label define sample_lbl 186003 `"1860 100% database"', add
label define sample_lbl 186002 `"1860 1% sample with black oversample"', add
label define sample_lbl 186001 `"1860 1%"', add
label define sample_lbl 185002 `"1850 100% database"', add
label define sample_lbl 185001 `"1850 1%"', add
label values sample sample_lbl

label define subsamp_lbl 00 `"First 1% subsample"'
label define subsamp_lbl 01 `"2nd 1% subsample"', add
label define subsamp_lbl 02 `"2"', add
label define subsamp_lbl 03 `"3"', add
label define subsamp_lbl 04 `"4"', add
label define subsamp_lbl 05 `"5"', add
label define subsamp_lbl 06 `"6"', add
label define subsamp_lbl 07 `"7"', add
label define subsamp_lbl 08 `"8"', add
label define subsamp_lbl 09 `"9"', add
label define subsamp_lbl 10 `"10"', add
label define subsamp_lbl 11 `"11"', add
label define subsamp_lbl 12 `"12"', add
label define subsamp_lbl 13 `"13"', add
label define subsamp_lbl 14 `"14"', add
label define subsamp_lbl 15 `"15"', add
label define subsamp_lbl 16 `"16"', add
label define subsamp_lbl 17 `"17"', add
label define subsamp_lbl 18 `"18"', add
label define subsamp_lbl 19 `"19"', add
label define subsamp_lbl 20 `"20"', add
label define subsamp_lbl 21 `"21"', add
label define subsamp_lbl 22 `"22"', add
label define subsamp_lbl 23 `"23"', add
label define subsamp_lbl 24 `"24"', add
label define subsamp_lbl 25 `"25"', add
label define subsamp_lbl 26 `"26"', add
label define subsamp_lbl 27 `"27"', add
label define subsamp_lbl 28 `"28"', add
label define subsamp_lbl 29 `"29"', add
label define subsamp_lbl 30 `"30"', add
label define subsamp_lbl 31 `"31"', add
label define subsamp_lbl 32 `"32"', add
label define subsamp_lbl 33 `"33"', add
label define subsamp_lbl 34 `"34"', add
label define subsamp_lbl 35 `"35"', add
label define subsamp_lbl 36 `"36"', add
label define subsamp_lbl 37 `"37"', add
label define subsamp_lbl 38 `"38"', add
label define subsamp_lbl 39 `"39"', add
label define subsamp_lbl 40 `"40"', add
label define subsamp_lbl 41 `"41"', add
label define subsamp_lbl 42 `"42"', add
label define subsamp_lbl 43 `"43"', add
label define subsamp_lbl 44 `"44"', add
label define subsamp_lbl 45 `"45"', add
label define subsamp_lbl 46 `"46"', add
label define subsamp_lbl 47 `"47"', add
label define subsamp_lbl 48 `"48"', add
label define subsamp_lbl 49 `"49"', add
label define subsamp_lbl 50 `"50"', add
label define subsamp_lbl 51 `"51"', add
label define subsamp_lbl 52 `"52"', add
label define subsamp_lbl 53 `"53"', add
label define subsamp_lbl 54 `"54"', add
label define subsamp_lbl 55 `"55"', add
label define subsamp_lbl 56 `"56"', add
label define subsamp_lbl 57 `"57"', add
label define subsamp_lbl 58 `"58"', add
label define subsamp_lbl 59 `"59"', add
label define subsamp_lbl 60 `"60"', add
label define subsamp_lbl 61 `"61"', add
label define subsamp_lbl 62 `"62"', add
label define subsamp_lbl 63 `"63"', add
label define subsamp_lbl 64 `"64"', add
label define subsamp_lbl 65 `"65"', add
label define subsamp_lbl 66 `"66"', add
label define subsamp_lbl 67 `"67"', add
label define subsamp_lbl 68 `"68"', add
label define subsamp_lbl 69 `"69"', add
label define subsamp_lbl 70 `"70"', add
label define subsamp_lbl 71 `"71"', add
label define subsamp_lbl 72 `"72"', add
label define subsamp_lbl 73 `"73"', add
label define subsamp_lbl 74 `"74"', add
label define subsamp_lbl 75 `"75"', add
label define subsamp_lbl 76 `"76"', add
label define subsamp_lbl 77 `"77"', add
label define subsamp_lbl 78 `"78"', add
label define subsamp_lbl 79 `"79"', add
label define subsamp_lbl 80 `"80"', add
label define subsamp_lbl 81 `"81"', add
label define subsamp_lbl 82 `"82"', add
label define subsamp_lbl 83 `"83"', add
label define subsamp_lbl 84 `"84"', add
label define subsamp_lbl 85 `"85"', add
label define subsamp_lbl 86 `"86"', add
label define subsamp_lbl 87 `"87"', add
label define subsamp_lbl 88 `"88"', add
label define subsamp_lbl 89 `"89"', add
label define subsamp_lbl 90 `"90"', add
label define subsamp_lbl 91 `"91"', add
label define subsamp_lbl 92 `"92"', add
label define subsamp_lbl 93 `"93"', add
label define subsamp_lbl 94 `"94"', add
label define subsamp_lbl 95 `"95"', add
label define subsamp_lbl 96 `"96"', add
label define subsamp_lbl 97 `"97"', add
label define subsamp_lbl 98 `"98"', add
label define subsamp_lbl 99 `"99"', add
label values subsamp subsamp_lbl

label define region_lbl 11 `"New England Division"'
label define region_lbl 12 `"Middle Atlantic Division"', add
label define region_lbl 13 `"Mixed Northeast Divisions (1970 Metro)"', add
label define region_lbl 21 `"East North Central Div."', add
label define region_lbl 22 `"West North Central Div."', add
label define region_lbl 23 `"Mixed Midwest Divisions (1970 Metro)"', add
label define region_lbl 31 `"South Atlantic Division"', add
label define region_lbl 32 `"East South Central Div."', add
label define region_lbl 33 `"West South Central Div."', add
label define region_lbl 34 `"Mixed Southern Divisions (1970 Metro)"', add
label define region_lbl 41 `"Mountain Division"', add
label define region_lbl 42 `"Pacific Division"', add
label define region_lbl 43 `"Mixed Western Divisions (1970 Metro)"', add
label define region_lbl 91 `"Military/Military reservations"', add
label define region_lbl 92 `"PUMA boundaries cross state lines-1% sample"', add
label define region_lbl 97 `"State not identified"', add
label define region_lbl 99 `"Not identified"', add
label values region region_lbl

label define stateicp_lbl 01 `"Connecticut"'
label define stateicp_lbl 02 `"Maine"', add
label define stateicp_lbl 03 `"Massachusetts"', add
label define stateicp_lbl 04 `"New Hampshire"', add
label define stateicp_lbl 05 `"Rhode Island"', add
label define stateicp_lbl 06 `"Vermont"', add
label define stateicp_lbl 11 `"Delaware"', add
label define stateicp_lbl 12 `"New Jersey"', add
label define stateicp_lbl 13 `"New York"', add
label define stateicp_lbl 14 `"Pennsylvania"', add
label define stateicp_lbl 21 `"Illinois"', add
label define stateicp_lbl 22 `"Indiana"', add
label define stateicp_lbl 23 `"Michigan"', add
label define stateicp_lbl 24 `"Ohio"', add
label define stateicp_lbl 25 `"Wisconsin"', add
label define stateicp_lbl 31 `"Iowa"', add
label define stateicp_lbl 32 `"Kansas"', add
label define stateicp_lbl 33 `"Minnesota"', add
label define stateicp_lbl 34 `"Missouri"', add
label define stateicp_lbl 35 `"Nebraska"', add
label define stateicp_lbl 36 `"North Dakota"', add
label define stateicp_lbl 37 `"South Dakota"', add
label define stateicp_lbl 40 `"Virginia"', add
label define stateicp_lbl 41 `"Alabama"', add
label define stateicp_lbl 42 `"Arkansas"', add
label define stateicp_lbl 43 `"Florida"', add
label define stateicp_lbl 44 `"Georgia"', add
label define stateicp_lbl 45 `"Louisiana"', add
label define stateicp_lbl 46 `"Mississippi"', add
label define stateicp_lbl 47 `"North Carolina"', add
label define stateicp_lbl 48 `"South Carolina"', add
label define stateicp_lbl 49 `"Texas"', add
label define stateicp_lbl 51 `"Kentucky"', add
label define stateicp_lbl 52 `"Maryland"', add
label define stateicp_lbl 53 `"Oklahoma"', add
label define stateicp_lbl 54 `"Tennessee"', add
label define stateicp_lbl 56 `"West Virginia"', add
label define stateicp_lbl 61 `"Arizona"', add
label define stateicp_lbl 62 `"Colorado"', add
label define stateicp_lbl 63 `"Idaho"', add
label define stateicp_lbl 64 `"Montana"', add
label define stateicp_lbl 65 `"Nevada"', add
label define stateicp_lbl 66 `"New Mexico"', add
label define stateicp_lbl 67 `"Utah"', add
label define stateicp_lbl 68 `"Wyoming"', add
label define stateicp_lbl 71 `"California"', add
label define stateicp_lbl 72 `"Oregon"', add
label define stateicp_lbl 73 `"Washington"', add
label define stateicp_lbl 81 `"Alaska"', add
label define stateicp_lbl 82 `"Hawaii"', add
label define stateicp_lbl 83 `"Puerto Rico"', add
label define stateicp_lbl 96 `"State groupings (1980 Urban/rural sample)"', add
label define stateicp_lbl 97 `"Military/Mil. Reservations"', add
label define stateicp_lbl 98 `"District of Columbia"', add
label define stateicp_lbl 99 `"State not identified"', add
label values stateicp stateicp_lbl

label define statefip_lbl 01 `"Alabama"'
label define statefip_lbl 02 `"Alaska"', add
label define statefip_lbl 04 `"Arizona"', add
label define statefip_lbl 05 `"Arkansas"', add
label define statefip_lbl 06 `"California"', add
label define statefip_lbl 08 `"Colorado"', add
label define statefip_lbl 09 `"Connecticut"', add
label define statefip_lbl 10 `"Delaware"', add
label define statefip_lbl 11 `"District of Columbia"', add
label define statefip_lbl 12 `"Florida"', add
label define statefip_lbl 13 `"Georgia"', add
label define statefip_lbl 15 `"Hawaii"', add
label define statefip_lbl 16 `"Idaho"', add
label define statefip_lbl 17 `"Illinois"', add
label define statefip_lbl 18 `"Indiana"', add
label define statefip_lbl 19 `"Iowa"', add
label define statefip_lbl 20 `"Kansas"', add
label define statefip_lbl 21 `"Kentucky"', add
label define statefip_lbl 22 `"Louisiana"', add
label define statefip_lbl 23 `"Maine"', add
label define statefip_lbl 24 `"Maryland"', add
label define statefip_lbl 25 `"Massachusetts"', add
label define statefip_lbl 26 `"Michigan"', add
label define statefip_lbl 27 `"Minnesota"', add
label define statefip_lbl 28 `"Mississippi"', add
label define statefip_lbl 29 `"Missouri"', add
label define statefip_lbl 30 `"Montana"', add
label define statefip_lbl 31 `"Nebraska"', add
label define statefip_lbl 32 `"Nevada"', add
label define statefip_lbl 33 `"New Hampshire"', add
label define statefip_lbl 34 `"New Jersey"', add
label define statefip_lbl 35 `"New Mexico"', add
label define statefip_lbl 36 `"New York"', add
label define statefip_lbl 37 `"North Carolina"', add
label define statefip_lbl 38 `"North Dakota"', add
label define statefip_lbl 39 `"Ohio"', add
label define statefip_lbl 40 `"Oklahoma"', add
label define statefip_lbl 41 `"Oregon"', add
label define statefip_lbl 42 `"Pennsylvania"', add
label define statefip_lbl 44 `"Rhode Island"', add
label define statefip_lbl 45 `"South Carolina"', add
label define statefip_lbl 46 `"South Dakota"', add
label define statefip_lbl 47 `"Tennessee"', add
label define statefip_lbl 48 `"Texas"', add
label define statefip_lbl 49 `"Utah"', add
label define statefip_lbl 50 `"Vermont"', add
label define statefip_lbl 51 `"Virginia"', add
label define statefip_lbl 53 `"Washington"', add
label define statefip_lbl 54 `"West Virginia"', add
label define statefip_lbl 55 `"Wisconsin"', add
label define statefip_lbl 56 `"Wyoming"', add
label define statefip_lbl 61 `"Maine-New Hampshire-Vermont"', add
label define statefip_lbl 62 `"Massachusetts-Rhode Island"', add
label define statefip_lbl 63 `"Minnesota-Iowa-Missouri-Kansas-Nebraska-S.Dakota-N.Dakota"', add
label define statefip_lbl 64 `"Maryland-Delaware"', add
label define statefip_lbl 65 `"Montana-Idaho-Wyoming"', add
label define statefip_lbl 66 `"Utah-Nevada"', add
label define statefip_lbl 67 `"Arizona-New Mexico"', add
label define statefip_lbl 68 `"Alaska-Hawaii"', add
label define statefip_lbl 72 `"Puerto Rico"', add
label define statefip_lbl 97 `"Military/Mil. Reservation"', add
label define statefip_lbl 99 `"State not identified"', add
label values statefip statefip_lbl

label define countyicp_lbl 0010 `"0010"'
label define countyicp_lbl 0030 `"0030"', add
label define countyicp_lbl 0050 `"0050"', add
label define countyicp_lbl 0070 `"0070"', add
label define countyicp_lbl 0090 `"0090"', add
label define countyicp_lbl 0110 `"0110"', add
label define countyicp_lbl 0130 `"0130"', add
label define countyicp_lbl 0150 `"0150"', add
label define countyicp_lbl 0170 `"0170"', add
label define countyicp_lbl 0190 `"0190"', add
label define countyicp_lbl 0200 `"0200"', add
label define countyicp_lbl 0205 `"0205"', add
label define countyicp_lbl 0210 `"0210"', add
label define countyicp_lbl 0230 `"0230"', add
label define countyicp_lbl 0250 `"0250"', add
label define countyicp_lbl 0270 `"0270"', add
label define countyicp_lbl 0290 `"0290"', add
label define countyicp_lbl 0310 `"0310"', add
label define countyicp_lbl 0330 `"0330"', add
label define countyicp_lbl 0350 `"0350"', add
label define countyicp_lbl 0360 `"0360"', add
label define countyicp_lbl 0370 `"0370"', add
label define countyicp_lbl 0390 `"0390"', add
label define countyicp_lbl 0410 `"0410"', add
label define countyicp_lbl 0430 `"0430"', add
label define countyicp_lbl 0450 `"0450"', add
label define countyicp_lbl 0455 `"0455"', add
label define countyicp_lbl 0470 `"0470"', add
label define countyicp_lbl 0490 `"0490"', add
label define countyicp_lbl 0510 `"0510"', add
label define countyicp_lbl 0530 `"0530"', add
label define countyicp_lbl 0550 `"0550"', add
label define countyicp_lbl 0570 `"0570"', add
label define countyicp_lbl 0590 `"0590"', add
label define countyicp_lbl 0605 `"0605"', add
label define countyicp_lbl 0610 `"0610"', add
label define countyicp_lbl 0630 `"0630"', add
label define countyicp_lbl 0650 `"0650"', add
label define countyicp_lbl 0670 `"0670"', add
label define countyicp_lbl 0690 `"0690"', add
label define countyicp_lbl 0710 `"0710"', add
label define countyicp_lbl 0730 `"0730"', add
label define countyicp_lbl 0750 `"0750"', add
label define countyicp_lbl 0770 `"0770"', add
label define countyicp_lbl 0790 `"0790"', add
label define countyicp_lbl 0810 `"0810"', add
label define countyicp_lbl 0830 `"0830"', add
label define countyicp_lbl 0850 `"0850"', add
label define countyicp_lbl 0870 `"0870"', add
label define countyicp_lbl 0890 `"0890"', add
label define countyicp_lbl 0910 `"0910"', add
label define countyicp_lbl 0930 `"0930"', add
label define countyicp_lbl 0950 `"0950"', add
label define countyicp_lbl 0970 `"0970"', add
label define countyicp_lbl 0990 `"0990"', add
label define countyicp_lbl 1010 `"1010"', add
label define countyicp_lbl 1030 `"1030"', add
label define countyicp_lbl 1050 `"1050"', add
label define countyicp_lbl 1070 `"1070"', add
label define countyicp_lbl 1090 `"1090"', add
label define countyicp_lbl 1110 `"1110"', add
label define countyicp_lbl 1130 `"1130"', add
label define countyicp_lbl 1150 `"1150"', add
label define countyicp_lbl 1170 `"1170"', add
label define countyicp_lbl 1190 `"1190"', add
label define countyicp_lbl 1210 `"1210"', add
label define countyicp_lbl 1230 `"1230"', add
label define countyicp_lbl 1250 `"1250"', add
label define countyicp_lbl 1270 `"1270"', add
label define countyicp_lbl 1290 `"1290"', add
label define countyicp_lbl 1310 `"1310"', add
label define countyicp_lbl 1330 `"1330"', add
label define countyicp_lbl 1350 `"1350"', add
label define countyicp_lbl 1370 `"1370"', add
label define countyicp_lbl 1390 `"1390"', add
label define countyicp_lbl 1410 `"1410"', add
label define countyicp_lbl 1430 `"1430"', add
label define countyicp_lbl 1450 `"1450"', add
label define countyicp_lbl 1470 `"1470"', add
label define countyicp_lbl 1490 `"1490"', add
label define countyicp_lbl 1510 `"1510"', add
label define countyicp_lbl 1530 `"1530"', add
label define countyicp_lbl 1550 `"1550"', add
label define countyicp_lbl 1570 `"1570"', add
label define countyicp_lbl 1590 `"1590"', add
label define countyicp_lbl 1610 `"1610"', add
label define countyicp_lbl 1630 `"1630"', add
label define countyicp_lbl 1650 `"1650"', add
label define countyicp_lbl 1670 `"1670"', add
label define countyicp_lbl 1690 `"1690"', add
label define countyicp_lbl 1710 `"1710"', add
label define countyicp_lbl 1730 `"1730"', add
label define countyicp_lbl 1750 `"1750"', add
label define countyicp_lbl 1770 `"1770"', add
label define countyicp_lbl 1790 `"1790"', add
label define countyicp_lbl 1810 `"1810"', add
label define countyicp_lbl 1830 `"1830"', add
label define countyicp_lbl 1850 `"1850"', add
label define countyicp_lbl 1870 `"1870"', add
label define countyicp_lbl 1875 `"1875"', add
label define countyicp_lbl 1890 `"1890"', add
label define countyicp_lbl 1910 `"1910"', add
label define countyicp_lbl 1930 `"1930"', add
label define countyicp_lbl 1950 `"1950"', add
label define countyicp_lbl 1970 `"1970"', add
label define countyicp_lbl 1990 `"1990"', add
label define countyicp_lbl 2010 `"2010"', add
label define countyicp_lbl 2030 `"2030"', add
label define countyicp_lbl 2050 `"2050"', add
label define countyicp_lbl 2070 `"2070"', add
label define countyicp_lbl 2090 `"2090"', add
label define countyicp_lbl 2110 `"2110"', add
label define countyicp_lbl 2130 `"2130"', add
label define countyicp_lbl 2150 `"2150"', add
label define countyicp_lbl 2170 `"2170"', add
label define countyicp_lbl 2190 `"2190"', add
label define countyicp_lbl 2210 `"2210"', add
label define countyicp_lbl 2230 `"2230"', add
label define countyicp_lbl 2250 `"2250"', add
label define countyicp_lbl 2270 `"2270"', add
label define countyicp_lbl 2290 `"2290"', add
label define countyicp_lbl 2310 `"2310"', add
label define countyicp_lbl 2330 `"2330"', add
label define countyicp_lbl 2350 `"2350"', add
label define countyicp_lbl 2370 `"2370"', add
label define countyicp_lbl 2390 `"2390"', add
label define countyicp_lbl 2410 `"2410"', add
label define countyicp_lbl 2430 `"2430"', add
label define countyicp_lbl 2450 `"2450"', add
label define countyicp_lbl 2470 `"2470"', add
label define countyicp_lbl 2490 `"2490"', add
label define countyicp_lbl 2510 `"2510"', add
label define countyicp_lbl 2530 `"2530"', add
label define countyicp_lbl 2550 `"2550"', add
label define countyicp_lbl 2570 `"2570"', add
label define countyicp_lbl 2590 `"2590"', add
label define countyicp_lbl 2610 `"2610"', add
label define countyicp_lbl 2630 `"2630"', add
label define countyicp_lbl 2650 `"2650"', add
label define countyicp_lbl 2670 `"2670"', add
label define countyicp_lbl 2690 `"2690"', add
label define countyicp_lbl 2710 `"2710"', add
label define countyicp_lbl 2730 `"2730"', add
label define countyicp_lbl 2750 `"2750"', add
label define countyicp_lbl 2770 `"2770"', add
label define countyicp_lbl 2790 `"2790"', add
label define countyicp_lbl 2810 `"2810"', add
label define countyicp_lbl 2830 `"2830"', add
label define countyicp_lbl 2850 `"2850"', add
label define countyicp_lbl 2870 `"2870"', add
label define countyicp_lbl 2890 `"2890"', add
label define countyicp_lbl 2910 `"2910"', add
label define countyicp_lbl 2930 `"2930"', add
label define countyicp_lbl 2950 `"2950"', add
label define countyicp_lbl 2970 `"2970"', add
label define countyicp_lbl 2990 `"2990"', add
label define countyicp_lbl 3010 `"3010"', add
label define countyicp_lbl 3030 `"3030"', add
label define countyicp_lbl 3050 `"3050"', add
label define countyicp_lbl 3070 `"3070"', add
label define countyicp_lbl 3090 `"3090"', add
label define countyicp_lbl 3110 `"3110"', add
label define countyicp_lbl 3130 `"3130"', add
label define countyicp_lbl 3150 `"3150"', add
label define countyicp_lbl 3170 `"3170"', add
label define countyicp_lbl 3190 `"3190"', add
label define countyicp_lbl 3210 `"3210"', add
label define countyicp_lbl 3230 `"3230"', add
label define countyicp_lbl 3250 `"3250"', add
label define countyicp_lbl 3270 `"3270"', add
label define countyicp_lbl 3290 `"3290"', add
label define countyicp_lbl 3310 `"3310"', add
label define countyicp_lbl 3330 `"3330"', add
label define countyicp_lbl 3350 `"3350"', add
label define countyicp_lbl 3370 `"3370"', add
label define countyicp_lbl 3390 `"3390"', add
label define countyicp_lbl 3410 `"3410"', add
label define countyicp_lbl 3430 `"3430"', add
label define countyicp_lbl 3450 `"3450"', add
label define countyicp_lbl 3470 `"3470"', add
label define countyicp_lbl 3490 `"3490"', add
label define countyicp_lbl 3510 `"3510"', add
label define countyicp_lbl 3530 `"3530"', add
label define countyicp_lbl 3550 `"3550"', add
label define countyicp_lbl 3570 `"3570"', add
label define countyicp_lbl 3590 `"3590"', add
label define countyicp_lbl 3610 `"3610"', add
label define countyicp_lbl 3630 `"3630"', add
label define countyicp_lbl 3650 `"3650"', add
label define countyicp_lbl 3670 `"3670"', add
label define countyicp_lbl 3690 `"3690"', add
label define countyicp_lbl 3710 `"3710"', add
label define countyicp_lbl 3730 `"3730"', add
label define countyicp_lbl 3750 `"3750"', add
label define countyicp_lbl 3770 `"3770"', add
label define countyicp_lbl 3790 `"3790"', add
label define countyicp_lbl 3810 `"3810"', add
label define countyicp_lbl 3830 `"3830"', add
label define countyicp_lbl 3850 `"3850"', add
label define countyicp_lbl 3870 `"3870"', add
label define countyicp_lbl 3890 `"3890"', add
label define countyicp_lbl 3910 `"3910"', add
label define countyicp_lbl 3930 `"3930"', add
label define countyicp_lbl 3950 `"3950"', add
label define countyicp_lbl 3970 `"3970"', add
label define countyicp_lbl 3990 `"3990"', add
label define countyicp_lbl 4010 `"4010"', add
label define countyicp_lbl 4030 `"4030"', add
label define countyicp_lbl 4050 `"4050"', add
label define countyicp_lbl 4070 `"4070"', add
label define countyicp_lbl 4090 `"4090"', add
label define countyicp_lbl 4110 `"4110"', add
label define countyicp_lbl 4130 `"4130"', add
label define countyicp_lbl 4150 `"4150"', add
label define countyicp_lbl 4170 `"4170"', add
label define countyicp_lbl 4190 `"4190"', add
label define countyicp_lbl 4210 `"4210"', add
label define countyicp_lbl 4230 `"4230"', add
label define countyicp_lbl 4250 `"4250"', add
label define countyicp_lbl 4270 `"4270"', add
label define countyicp_lbl 4290 `"4290"', add
label define countyicp_lbl 4310 `"4310"', add
label define countyicp_lbl 4330 `"4330"', add
label define countyicp_lbl 4350 `"4350"', add
label define countyicp_lbl 4370 `"4370"', add
label define countyicp_lbl 4390 `"4390"', add
label define countyicp_lbl 4410 `"4410"', add
label define countyicp_lbl 4430 `"4430"', add
label define countyicp_lbl 4450 `"4450"', add
label define countyicp_lbl 4470 `"4470"', add
label define countyicp_lbl 4490 `"4490"', add
label define countyicp_lbl 4510 `"4510"', add
label define countyicp_lbl 4530 `"4530"', add
label define countyicp_lbl 4550 `"4550"', add
label define countyicp_lbl 4570 `"4570"', add
label define countyicp_lbl 4590 `"4590"', add
label define countyicp_lbl 4610 `"4610"', add
label define countyicp_lbl 4630 `"4630"', add
label define countyicp_lbl 4650 `"4650"', add
label define countyicp_lbl 4670 `"4670"', add
label define countyicp_lbl 4690 `"4690"', add
label define countyicp_lbl 4710 `"4710"', add
label define countyicp_lbl 4730 `"4730"', add
label define countyicp_lbl 4750 `"4750"', add
label define countyicp_lbl 4770 `"4770"', add
label define countyicp_lbl 4790 `"4790"', add
label define countyicp_lbl 4810 `"4810"', add
label define countyicp_lbl 4830 `"4830"', add
label define countyicp_lbl 4850 `"4850"', add
label define countyicp_lbl 4870 `"4870"', add
label define countyicp_lbl 4890 `"4890"', add
label define countyicp_lbl 4910 `"4910"', add
label define countyicp_lbl 4930 `"4930"', add
label define countyicp_lbl 4950 `"4950"', add
label define countyicp_lbl 4970 `"4970"', add
label define countyicp_lbl 4990 `"4990"', add
label define countyicp_lbl 5010 `"5010"', add
label define countyicp_lbl 5030 `"5030"', add
label define countyicp_lbl 5050 `"5050"', add
label define countyicp_lbl 5070 `"5070"', add
label define countyicp_lbl 5100 `"5100"', add
label define countyicp_lbl 5200 `"5200"', add
label define countyicp_lbl 5300 `"5300"', add
label define countyicp_lbl 5400 `"5400"', add
label define countyicp_lbl 5500 `"5500"', add
label define countyicp_lbl 5600 `"5600"', add
label define countyicp_lbl 5700 `"5700"', add
label define countyicp_lbl 5800 `"5800"', add
label define countyicp_lbl 5900 `"5900"', add
label define countyicp_lbl 6100 `"6100"', add
label define countyicp_lbl 6300 `"6300"', add
label define countyicp_lbl 6400 `"6400"', add
label define countyicp_lbl 6500 `"6500"', add
label define countyicp_lbl 6600 `"6600"', add
label define countyicp_lbl 6700 `"6700"', add
label define countyicp_lbl 6800 `"6800"', add
label define countyicp_lbl 6900 `"6900"', add
label define countyicp_lbl 7000 `"7000"', add
label define countyicp_lbl 7100 `"7100"', add
label define countyicp_lbl 7200 `"7200"', add
label define countyicp_lbl 7300 `"7300"', add
label define countyicp_lbl 7400 `"7400"', add
label define countyicp_lbl 7500 `"7500"', add
label define countyicp_lbl 7600 `"7600"', add
label define countyicp_lbl 7700 `"7700"', add
label define countyicp_lbl 7800 `"7800"', add
label define countyicp_lbl 7850 `"7850"', add
label define countyicp_lbl 7900 `"7900"', add
label define countyicp_lbl 8000 `"8000"', add
label define countyicp_lbl 8100 `"8100"', add
label define countyicp_lbl 8200 `"8200"', add
label define countyicp_lbl 8300 `"8300"', add
label define countyicp_lbl 8400 `"8400"', add
label values countyicp countyicp_lbl

label define countynhg_lbl 0100010 `"0100010"'
label define countynhg_lbl 0100015 `"0100015"', add
label define countynhg_lbl 0100030 `"0100030"', add
label define countynhg_lbl 0100050 `"0100050"', add
label define countynhg_lbl 0100055 `"0100055"', add
label define countynhg_lbl 0100070 `"0100070"', add
label define countynhg_lbl 0100090 `"0100090"', add
label define countynhg_lbl 0100110 `"0100110"', add
label define countynhg_lbl 0100130 `"0100130"', add
label define countynhg_lbl 0100150 `"0100150"', add
label define countynhg_lbl 0100170 `"0100170"', add
label define countynhg_lbl 0100190 `"0100190"', add
label define countynhg_lbl 0100210 `"0100210"', add
label define countynhg_lbl 0100230 `"0100230"', add
label define countynhg_lbl 0100250 `"0100250"', add
label define countynhg_lbl 0100270 `"0100270"', add
label define countynhg_lbl 0100290 `"0100290"', add
label define countynhg_lbl 0100310 `"0100310"', add
label define countynhg_lbl 0100330 `"0100330"', add
label define countynhg_lbl 0100350 `"0100350"', add
label define countynhg_lbl 0100370 `"0100370"', add
label define countynhg_lbl 0100390 `"0100390"', add
label define countynhg_lbl 0100410 `"0100410"', add
label define countynhg_lbl 0100430 `"0100430"', add
label define countynhg_lbl 0100450 `"0100450"', add
label define countynhg_lbl 0100470 `"0100470"', add
label define countynhg_lbl 0100490 `"0100490"', add
label define countynhg_lbl 0100510 `"0100510"', add
label define countynhg_lbl 0100530 `"0100530"', add
label define countynhg_lbl 0100550 `"0100550"', add
label define countynhg_lbl 0100570 `"0100570"', add
label define countynhg_lbl 0100590 `"0100590"', add
label define countynhg_lbl 0100610 `"0100610"', add
label define countynhg_lbl 0100630 `"0100630"', add
label define countynhg_lbl 0100650 `"0100650"', add
label define countynhg_lbl 0100655 `"0100655"', add
label define countynhg_lbl 0100670 `"0100670"', add
label define countynhg_lbl 0100690 `"0100690"', add
label define countynhg_lbl 0100710 `"0100710"', add
label define countynhg_lbl 0100730 `"0100730"', add
label define countynhg_lbl 0100750 `"0100750"', add
label define countynhg_lbl 0100770 `"0100770"', add
label define countynhg_lbl 0100790 `"0100790"', add
label define countynhg_lbl 0100810 `"0100810"', add
label define countynhg_lbl 0100830 `"0100830"', add
label define countynhg_lbl 0100850 `"0100850"', add
label define countynhg_lbl 0100870 `"0100870"', add
label define countynhg_lbl 0100890 `"0100890"', add
label define countynhg_lbl 0100910 `"0100910"', add
label define countynhg_lbl 0100930 `"0100930"', add
label define countynhg_lbl 0100950 `"0100950"', add
label define countynhg_lbl 0100970 `"0100970"', add
label define countynhg_lbl 0100990 `"0100990"', add
label define countynhg_lbl 0101010 `"0101010"', add
label define countynhg_lbl 0101030 `"0101030"', add
label define countynhg_lbl 0101050 `"0101050"', add
label define countynhg_lbl 0101070 `"0101070"', add
label define countynhg_lbl 0101090 `"0101090"', add
label define countynhg_lbl 0101110 `"0101110"', add
label define countynhg_lbl 0101130 `"0101130"', add
label define countynhg_lbl 0101150 `"0101150"', add
label define countynhg_lbl 0101155 `"0101155"', add
label define countynhg_lbl 0101170 `"0101170"', add
label define countynhg_lbl 0101190 `"0101190"', add
label define countynhg_lbl 0101210 `"0101210"', add
label define countynhg_lbl 0101230 `"0101230"', add
label define countynhg_lbl 0101250 `"0101250"', add
label define countynhg_lbl 0101270 `"0101270"', add
label define countynhg_lbl 0101290 `"0101290"', add
label define countynhg_lbl 0101310 `"0101310"', add
label define countynhg_lbl 0101330 `"0101330"', add
label define countynhg_lbl 0250015 `"0250015"', add
label define countynhg_lbl 0250035 `"0250035"', add
label define countynhg_lbl 0250055 `"0250055"', add
label define countynhg_lbl 0250075 `"0250075"', add
label define countynhg_lbl 0250095 `"0250095"', add
label define countynhg_lbl 0250115 `"0250115"', add
label define countynhg_lbl 0400010 `"0400010"', add
label define countynhg_lbl 0400030 `"0400030"', add
label define countynhg_lbl 0400050 `"0400050"', add
label define countynhg_lbl 0400070 `"0400070"', add
label define countynhg_lbl 0400090 `"0400090"', add
label define countynhg_lbl 0400110 `"0400110"', add
label define countynhg_lbl 0400130 `"0400130"', add
label define countynhg_lbl 0400150 `"0400150"', add
label define countynhg_lbl 0400170 `"0400170"', add
label define countynhg_lbl 0400190 `"0400190"', add
label define countynhg_lbl 0400210 `"0400210"', add
label define countynhg_lbl 0400230 `"0400230"', add
label define countynhg_lbl 0400250 `"0400250"', add
label define countynhg_lbl 0400270 `"0400270"', add
label define countynhg_lbl 0450015 `"0450015"', add
label define countynhg_lbl 0450035 `"0450035"', add
label define countynhg_lbl 0450055 `"0450055"', add
label define countynhg_lbl 0450075 `"0450075"', add
label define countynhg_lbl 0450095 `"0450095"', add
label define countynhg_lbl 0450135 `"0450135"', add
label define countynhg_lbl 0450155 `"0450155"', add
label define countynhg_lbl 0450175 `"0450175"', add
label define countynhg_lbl 0450177 `"0450177"', add
label define countynhg_lbl 0450195 `"0450195"', add
label define countynhg_lbl 0450215 `"0450215"', add
label define countynhg_lbl 0450235 `"0450235"', add
label define countynhg_lbl 0450255 `"0450255"', add
label define countynhg_lbl 0450275 `"0450275"', add
label define countynhg_lbl 0500010 `"0500010"', add
label define countynhg_lbl 0500030 `"0500030"', add
label define countynhg_lbl 0500050 `"0500050"', add
label define countynhg_lbl 0500070 `"0500070"', add
label define countynhg_lbl 0500090 `"0500090"', add
label define countynhg_lbl 0500110 `"0500110"', add
label define countynhg_lbl 0500130 `"0500130"', add
label define countynhg_lbl 0500150 `"0500150"', add
label define countynhg_lbl 0500170 `"0500170"', add
label define countynhg_lbl 0500190 `"0500190"', add
label define countynhg_lbl 0500210 `"0500210"', add
label define countynhg_lbl 0500230 `"0500230"', add
label define countynhg_lbl 0500250 `"0500250"', add
label define countynhg_lbl 0500270 `"0500270"', add
label define countynhg_lbl 0500290 `"0500290"', add
label define countynhg_lbl 0500310 `"0500310"', add
label define countynhg_lbl 0500330 `"0500330"', add
label define countynhg_lbl 0500350 `"0500350"', add
label define countynhg_lbl 0500370 `"0500370"', add
label define countynhg_lbl 0500390 `"0500390"', add
label define countynhg_lbl 0500410 `"0500410"', add
label define countynhg_lbl 0500415 `"0500415"', add
label define countynhg_lbl 0500430 `"0500430"', add
label define countynhg_lbl 0500450 `"0500450"', add
label define countynhg_lbl 0500470 `"0500470"', add
label define countynhg_lbl 0500490 `"0500490"', add
label define countynhg_lbl 0500510 `"0500510"', add
label define countynhg_lbl 0500530 `"0500530"', add
label define countynhg_lbl 0500550 `"0500550"', add
label define countynhg_lbl 0500570 `"0500570"', add
label define countynhg_lbl 0500590 `"0500590"', add
label define countynhg_lbl 0500610 `"0500610"', add
label define countynhg_lbl 0500630 `"0500630"', add
label define countynhg_lbl 0500650 `"0500650"', add
label define countynhg_lbl 0500670 `"0500670"', add
label define countynhg_lbl 0500690 `"0500690"', add
label define countynhg_lbl 0500710 `"0500710"', add
label define countynhg_lbl 0500730 `"0500730"', add
label define countynhg_lbl 0500750 `"0500750"', add
label define countynhg_lbl 0500770 `"0500770"', add
label define countynhg_lbl 0500790 `"0500790"', add
label define countynhg_lbl 0500810 `"0500810"', add
label define countynhg_lbl 0500830 `"0500830"', add
label define countynhg_lbl 0500850 `"0500850"', add
label define countynhg_lbl 0500870 `"0500870"', add
label define countynhg_lbl 0500890 `"0500890"', add
label define countynhg_lbl 0500910 `"0500910"', add
label define countynhg_lbl 0500930 `"0500930"', add
label define countynhg_lbl 0500950 `"0500950"', add
label define countynhg_lbl 0500970 `"0500970"', add
label define countynhg_lbl 0500990 `"0500990"', add
label define countynhg_lbl 0501010 `"0501010"', add
label define countynhg_lbl 0501030 `"0501030"', add
label define countynhg_lbl 0501050 `"0501050"', add
label define countynhg_lbl 0501070 `"0501070"', add
label define countynhg_lbl 0501090 `"0501090"', add
label define countynhg_lbl 0501110 `"0501110"', add
label define countynhg_lbl 0501130 `"0501130"', add
label define countynhg_lbl 0501150 `"0501150"', add
label define countynhg_lbl 0501170 `"0501170"', add
label define countynhg_lbl 0501190 `"0501190"', add
label define countynhg_lbl 0501210 `"0501210"', add
label define countynhg_lbl 0501230 `"0501230"', add
label define countynhg_lbl 0501250 `"0501250"', add
label define countynhg_lbl 0501270 `"0501270"', add
label define countynhg_lbl 0501290 `"0501290"', add
label define countynhg_lbl 0501310 `"0501310"', add
label define countynhg_lbl 0501330 `"0501330"', add
label define countynhg_lbl 0501350 `"0501350"', add
label define countynhg_lbl 0501370 `"0501370"', add
label define countynhg_lbl 0501390 `"0501390"', add
label define countynhg_lbl 0501410 `"0501410"', add
label define countynhg_lbl 0501430 `"0501430"', add
label define countynhg_lbl 0501450 `"0501450"', add
label define countynhg_lbl 0501470 `"0501470"', add
label define countynhg_lbl 0501490 `"0501490"', add
label define countynhg_lbl 0600010 `"0600010"', add
label define countynhg_lbl 0600030 `"0600030"', add
label define countynhg_lbl 0600050 `"0600050"', add
label define countynhg_lbl 0600070 `"0600070"', add
label define countynhg_lbl 0600090 `"0600090"', add
label define countynhg_lbl 0600110 `"0600110"', add
label define countynhg_lbl 0600130 `"0600130"', add
label define countynhg_lbl 0600150 `"0600150"', add
label define countynhg_lbl 0600170 `"0600170"', add
label define countynhg_lbl 0600190 `"0600190"', add
label define countynhg_lbl 0600210 `"0600210"', add
label define countynhg_lbl 0600230 `"0600230"', add
label define countynhg_lbl 0600250 `"0600250"', add
label define countynhg_lbl 0600270 `"0600270"', add
label define countynhg_lbl 0600290 `"0600290"', add
label define countynhg_lbl 0600310 `"0600310"', add
label define countynhg_lbl 0600315 `"0600315"', add
label define countynhg_lbl 0600330 `"0600330"', add
label define countynhg_lbl 0600350 `"0600350"', add
label define countynhg_lbl 0600370 `"0600370"', add
label define countynhg_lbl 0600390 `"0600390"', add
label define countynhg_lbl 0600410 `"0600410"', add
label define countynhg_lbl 0600430 `"0600430"', add
label define countynhg_lbl 0600450 `"0600450"', add
label define countynhg_lbl 0600470 `"0600470"', add
label define countynhg_lbl 0600490 `"0600490"', add
label define countynhg_lbl 0600510 `"0600510"', add
label define countynhg_lbl 0600530 `"0600530"', add
label define countynhg_lbl 0600550 `"0600550"', add
label define countynhg_lbl 0600570 `"0600570"', add
label define countynhg_lbl 0600590 `"0600590"', add
label define countynhg_lbl 0600610 `"0600610"', add
label define countynhg_lbl 0600630 `"0600630"', add
label define countynhg_lbl 0600650 `"0600650"', add
label define countynhg_lbl 0600670 `"0600670"', add
label define countynhg_lbl 0600690 `"0600690"', add
label define countynhg_lbl 0600710 `"0600710"', add
label define countynhg_lbl 0600730 `"0600730"', add
label define countynhg_lbl 0600750 `"0600750"', add
label define countynhg_lbl 0600770 `"0600770"', add
label define countynhg_lbl 0600790 `"0600790"', add
label define countynhg_lbl 0600810 `"0600810"', add
label define countynhg_lbl 0600830 `"0600830"', add
label define countynhg_lbl 0600850 `"0600850"', add
label define countynhg_lbl 0600870 `"0600870"', add
label define countynhg_lbl 0600890 `"0600890"', add
label define countynhg_lbl 0600910 `"0600910"', add
label define countynhg_lbl 0600930 `"0600930"', add
label define countynhg_lbl 0600950 `"0600950"', add
label define countynhg_lbl 0600970 `"0600970"', add
label define countynhg_lbl 0600990 `"0600990"', add
label define countynhg_lbl 0601010 `"0601010"', add
label define countynhg_lbl 0601030 `"0601030"', add
label define countynhg_lbl 0601050 `"0601050"', add
label define countynhg_lbl 0601070 `"0601070"', add
label define countynhg_lbl 0601090 `"0601090"', add
label define countynhg_lbl 0601110 `"0601110"', add
label define countynhg_lbl 0601130 `"0601130"', add
label define countynhg_lbl 0601150 `"0601150"', add
label define countynhg_lbl 0800010 `"0800010"', add
label define countynhg_lbl 0800030 `"0800030"', add
label define countynhg_lbl 0800050 `"0800050"', add
label define countynhg_lbl 0800070 `"0800070"', add
label define countynhg_lbl 0800090 `"0800090"', add
label define countynhg_lbl 0800110 `"0800110"', add
label define countynhg_lbl 0800130 `"0800130"', add
label define countynhg_lbl 0800150 `"0800150"', add
label define countynhg_lbl 0800170 `"0800170"', add
label define countynhg_lbl 0800190 `"0800190"', add
label define countynhg_lbl 0800210 `"0800210"', add
label define countynhg_lbl 0800230 `"0800230"', add
label define countynhg_lbl 0800250 `"0800250"', add
label define countynhg_lbl 0800270 `"0800270"', add
label define countynhg_lbl 0800290 `"0800290"', add
label define countynhg_lbl 0800310 `"0800310"', add
label define countynhg_lbl 0800330 `"0800330"', add
label define countynhg_lbl 0800350 `"0800350"', add
label define countynhg_lbl 0800370 `"0800370"', add
label define countynhg_lbl 0800390 `"0800390"', add
label define countynhg_lbl 0800410 `"0800410"', add
label define countynhg_lbl 0800430 `"0800430"', add
label define countynhg_lbl 0800450 `"0800450"', add
label define countynhg_lbl 0800470 `"0800470"', add
label define countynhg_lbl 0800490 `"0800490"', add
label define countynhg_lbl 0800510 `"0800510"', add
label define countynhg_lbl 0800530 `"0800530"', add
label define countynhg_lbl 0800550 `"0800550"', add
label define countynhg_lbl 0800570 `"0800570"', add
label define countynhg_lbl 0800590 `"0800590"', add
label define countynhg_lbl 0800610 `"0800610"', add
label define countynhg_lbl 0800630 `"0800630"', add
label define countynhg_lbl 0800650 `"0800650"', add
label define countynhg_lbl 0800670 `"0800670"', add
label define countynhg_lbl 0800690 `"0800690"', add
label define countynhg_lbl 0800710 `"0800710"', add
label define countynhg_lbl 0800730 `"0800730"', add
label define countynhg_lbl 0800750 `"0800750"', add
label define countynhg_lbl 0800770 `"0800770"', add
label define countynhg_lbl 0800790 `"0800790"', add
label define countynhg_lbl 0800810 `"0800810"', add
label define countynhg_lbl 0800830 `"0800830"', add
label define countynhg_lbl 0800850 `"0800850"', add
label define countynhg_lbl 0800870 `"0800870"', add
label define countynhg_lbl 0800890 `"0800890"', add
label define countynhg_lbl 0800910 `"0800910"', add
label define countynhg_lbl 0800930 `"0800930"', add
label define countynhg_lbl 0800950 `"0800950"', add
label define countynhg_lbl 0800970 `"0800970"', add
label define countynhg_lbl 0800990 `"0800990"', add
label define countynhg_lbl 0801010 `"0801010"', add
label define countynhg_lbl 0801030 `"0801030"', add
label define countynhg_lbl 0801050 `"0801050"', add
label define countynhg_lbl 0801070 `"0801070"', add
label define countynhg_lbl 0801090 `"0801090"', add
label define countynhg_lbl 0801110 `"0801110"', add
label define countynhg_lbl 0801130 `"0801130"', add
label define countynhg_lbl 0801150 `"0801150"', add
label define countynhg_lbl 0801170 `"0801170"', add
label define countynhg_lbl 0801190 `"0801190"', add
label define countynhg_lbl 0801210 `"0801210"', add
label define countynhg_lbl 0801230 `"0801230"', add
label define countynhg_lbl 0801250 `"0801250"', add
label define countynhg_lbl 0850015 `"0850015"', add
label define countynhg_lbl 0850035 `"0850035"', add
label define countynhg_lbl 0850055 `"0850055"', add
label define countynhg_lbl 0850075 `"0850075"', add
label define countynhg_lbl 0850095 `"0850095"', add
label define countynhg_lbl 0850115 `"0850115"', add
label define countynhg_lbl 0850135 `"0850135"', add
label define countynhg_lbl 0850155 `"0850155"', add
label define countynhg_lbl 0850175 `"0850175"', add
label define countynhg_lbl 0850195 `"0850195"', add
label define countynhg_lbl 0850215 `"0850215"', add
label define countynhg_lbl 0850235 `"0850235"', add
label define countynhg_lbl 0850255 `"0850255"', add
label define countynhg_lbl 0850275 `"0850275"', add
label define countynhg_lbl 0850295 `"0850295"', add
label define countynhg_lbl 0850315 `"0850315"', add
label define countynhg_lbl 0850335 `"0850335"', add
label define countynhg_lbl 0850355 `"0850355"', add
label define countynhg_lbl 0850375 `"0850375"', add
label define countynhg_lbl 0850395 `"0850395"', add
label define countynhg_lbl 0850415 `"0850415"', add
label define countynhg_lbl 0900010 `"0900010"', add
label define countynhg_lbl 0900030 `"0900030"', add
label define countynhg_lbl 0900050 `"0900050"', add
label define countynhg_lbl 0900070 `"0900070"', add
label define countynhg_lbl 0900090 `"0900090"', add
label define countynhg_lbl 0900110 `"0900110"', add
label define countynhg_lbl 0900130 `"0900130"', add
label define countynhg_lbl 0900150 `"0900150"', add
label define countynhg_lbl 0950000 `"0950000"', add
label define countynhg_lbl 0950015 `"0950015"', add
label define countynhg_lbl 0950035 `"0950035"', add
label define countynhg_lbl 0950037 `"0950037"', add
label define countynhg_lbl 0950055 `"0950055"', add
label define countynhg_lbl 0950075 `"0950075"', add
label define countynhg_lbl 0950095 `"0950095"', add
label define countynhg_lbl 0950135 `"0950135"', add
label define countynhg_lbl 0950155 `"0950155"', add
label define countynhg_lbl 0950175 `"0950175"', add
label define countynhg_lbl 0950195 `"0950195"', add
label define countynhg_lbl 0950215 `"0950215"', add
label define countynhg_lbl 0950235 `"0950235"', add
label define countynhg_lbl 0950255 `"0950255"', add
label define countynhg_lbl 0950295 `"0950295"', add
label define countynhg_lbl 0950335 `"0950335"', add
label define countynhg_lbl 0950355 `"0950355"', add
label define countynhg_lbl 0950375 `"0950375"', add
label define countynhg_lbl 0950395 `"0950395"', add
label define countynhg_lbl 0950415 `"0950415"', add
label define countynhg_lbl 0950435 `"0950435"', add
label define countynhg_lbl 0950495 `"0950495"', add
label define countynhg_lbl 0950515 `"0950515"', add
label define countynhg_lbl 0950555 `"0950555"', add
label define countynhg_lbl 0950575 `"0950575"', add
label define countynhg_lbl 0950615 `"0950615"', add
label define countynhg_lbl 0950655 `"0950655"', add
label define countynhg_lbl 0950675 `"0950675"', add
label define countynhg_lbl 0950715 `"0950715"', add
label define countynhg_lbl 0950735 `"0950735"', add
label define countynhg_lbl 0950755 `"0950755"', add
label define countynhg_lbl 0950775 `"0950775"', add
label define countynhg_lbl 0950795 `"0950795"', add
label define countynhg_lbl 0950815 `"0950815"', add
label define countynhg_lbl 0950875 `"0950875"', add
label define countynhg_lbl 0950895 `"0950895"', add
label define countynhg_lbl 0950915 `"0950915"', add
label define countynhg_lbl 0950935 `"0950935"', add
label define countynhg_lbl 0950955 `"0950955"', add
label define countynhg_lbl 0950975 `"0950975"', add
label define countynhg_lbl 0951035 `"0951035"', add
label define countynhg_lbl 0951075 `"0951075"', add
label define countynhg_lbl 0951155 `"0951155"', add
label define countynhg_lbl 0951175 `"0951175"', add
label define countynhg_lbl 0951195 `"0951195"', add
label define countynhg_lbl 0951215 `"0951215"', add
label define countynhg_lbl 0951235 `"0951235"', add
label define countynhg_lbl 0951255 `"0951255"', add
label define countynhg_lbl 0951275 `"0951275"', add
label define countynhg_lbl 0951295 `"0951295"', add
label define countynhg_lbl 0951375 `"0951375"', add
label define countynhg_lbl 0951395 `"0951395"', add
label define countynhg_lbl 0951435 `"0951435"', add
label define countynhg_lbl 0951475 `"0951475"', add
label define countynhg_lbl 0951495 `"0951495"', add
label define countynhg_lbl 0951535 `"0951535"', add
label define countynhg_lbl 0951555 `"0951555"', add
label define countynhg_lbl 0951595 `"0951595"', add
label define countynhg_lbl 0951615 `"0951615"', add
label define countynhg_lbl 0951635 `"0951635"', add
label define countynhg_lbl 0951655 `"0951655"', add
label define countynhg_lbl 0951675 `"0951675"', add
label define countynhg_lbl 0951715 `"0951715"', add
label define countynhg_lbl 0951735 `"0951735"', add
label define countynhg_lbl 0951755 `"0951755"', add
label define countynhg_lbl 0951775 `"0951775"', add
label define countynhg_lbl 0951795 `"0951795"', add
label define countynhg_lbl 0951855 `"0951855"', add
label define countynhg_lbl 1000010 `"1000010"', add
label define countynhg_lbl 1000030 `"1000030"', add
label define countynhg_lbl 1000050 `"1000050"', add
label define countynhg_lbl 1100010 `"1100010"', add
label define countynhg_lbl 1100017 `"1100017"', add
label define countynhg_lbl 1100075 `"1100075"', add
label define countynhg_lbl 1100115 `"1100115"', add
label define countynhg_lbl 1200010 `"1200010"', add
label define countynhg_lbl 1200030 `"1200030"', add
label define countynhg_lbl 1200050 `"1200050"', add
label define countynhg_lbl 1200065 `"1200065"', add
label define countynhg_lbl 1200070 `"1200070"', add
label define countynhg_lbl 1200090 `"1200090"', add
label define countynhg_lbl 1200110 `"1200110"', add
label define countynhg_lbl 1200130 `"1200130"', add
label define countynhg_lbl 1200150 `"1200150"', add
label define countynhg_lbl 1200170 `"1200170"', add
label define countynhg_lbl 1200190 `"1200190"', add
label define countynhg_lbl 1200210 `"1200210"', add
label define countynhg_lbl 1200230 `"1200230"', add
label define countynhg_lbl 1200250 `"1200250"', add
label define countynhg_lbl 1200270 `"1200270"', add
label define countynhg_lbl 1200290 `"1200290"', add
label define countynhg_lbl 1200310 `"1200310"', add
label define countynhg_lbl 1200330 `"1200330"', add
label define countynhg_lbl 1200350 `"1200350"', add
label define countynhg_lbl 1200370 `"1200370"', add
label define countynhg_lbl 1200390 `"1200390"', add
label define countynhg_lbl 1200410 `"1200410"', add
label define countynhg_lbl 1200430 `"1200430"', add
label define countynhg_lbl 1200450 `"1200450"', add
label define countynhg_lbl 1200470 `"1200470"', add
label define countynhg_lbl 1200490 `"1200490"', add
label define countynhg_lbl 1200510 `"1200510"', add
label define countynhg_lbl 1200530 `"1200530"', add
label define countynhg_lbl 1200550 `"1200550"', add
label define countynhg_lbl 1200570 `"1200570"', add
label define countynhg_lbl 1200590 `"1200590"', add
label define countynhg_lbl 1200610 `"1200610"', add
label define countynhg_lbl 1200630 `"1200630"', add
label define countynhg_lbl 1200650 `"1200650"', add
label define countynhg_lbl 1200670 `"1200670"', add
label define countynhg_lbl 1200690 `"1200690"', add
label define countynhg_lbl 1200710 `"1200710"', add
label define countynhg_lbl 1200730 `"1200730"', add
label define countynhg_lbl 1200750 `"1200750"', add
label define countynhg_lbl 1200770 `"1200770"', add
label define countynhg_lbl 1200790 `"1200790"', add
label define countynhg_lbl 1200810 `"1200810"', add
label define countynhg_lbl 1200830 `"1200830"', add
label define countynhg_lbl 1200850 `"1200850"', add
label define countynhg_lbl 1200870 `"1200870"', add
label define countynhg_lbl 1200890 `"1200890"', add
label define countynhg_lbl 1200895 `"1200895"', add
label define countynhg_lbl 1200910 `"1200910"', add
label define countynhg_lbl 1200930 `"1200930"', add
label define countynhg_lbl 1200950 `"1200950"', add
label define countynhg_lbl 1200970 `"1200970"', add
label define countynhg_lbl 1200990 `"1200990"', add
label define countynhg_lbl 1201010 `"1201010"', add
label define countynhg_lbl 1201030 `"1201030"', add
label define countynhg_lbl 1201050 `"1201050"', add
label define countynhg_lbl 1201070 `"1201070"', add
label define countynhg_lbl 1201090 `"1201090"', add
label define countynhg_lbl 1201110 `"1201110"', add
label define countynhg_lbl 1201115 `"1201115"', add
label define countynhg_lbl 1201130 `"1201130"', add
label define countynhg_lbl 1201150 `"1201150"', add
label define countynhg_lbl 1201170 `"1201170"', add
label define countynhg_lbl 1201190 `"1201190"', add
label define countynhg_lbl 1201210 `"1201210"', add
label define countynhg_lbl 1201230 `"1201230"', add
label define countynhg_lbl 1201250 `"1201250"', add
label define countynhg_lbl 1201270 `"1201270"', add
label define countynhg_lbl 1201290 `"1201290"', add
label define countynhg_lbl 1201310 `"1201310"', add
label define countynhg_lbl 1201330 `"1201330"', add
label define countynhg_lbl 1300010 `"1300010"', add
label define countynhg_lbl 1300030 `"1300030"', add
label define countynhg_lbl 1300050 `"1300050"', add
label define countynhg_lbl 1300070 `"1300070"', add
label define countynhg_lbl 1300090 `"1300090"', add
label define countynhg_lbl 1300110 `"1300110"', add
label define countynhg_lbl 1300130 `"1300130"', add
label define countynhg_lbl 1300150 `"1300150"', add
label define countynhg_lbl 1300170 `"1300170"', add
label define countynhg_lbl 1300190 `"1300190"', add
label define countynhg_lbl 1300210 `"1300210"', add
label define countynhg_lbl 1300230 `"1300230"', add
label define countynhg_lbl 1300250 `"1300250"', add
label define countynhg_lbl 1300270 `"1300270"', add
label define countynhg_lbl 1300290 `"1300290"', add
label define countynhg_lbl 1300310 `"1300310"', add
label define countynhg_lbl 1300330 `"1300330"', add
label define countynhg_lbl 1300350 `"1300350"', add
label define countynhg_lbl 1300370 `"1300370"', add
label define countynhg_lbl 1300390 `"1300390"', add
label define countynhg_lbl 1300395 `"1300395"', add
label define countynhg_lbl 1300430 `"1300430"', add
label define countynhg_lbl 1300450 `"1300450"', add
label define countynhg_lbl 1300455 `"1300455"', add
label define countynhg_lbl 1300470 `"1300470"', add
label define countynhg_lbl 1300490 `"1300490"', add
label define countynhg_lbl 1300510 `"1300510"', add
label define countynhg_lbl 1300530 `"1300530"', add
label define countynhg_lbl 1300550 `"1300550"', add
label define countynhg_lbl 1300570 `"1300570"', add
label define countynhg_lbl 1300590 `"1300590"', add
label define countynhg_lbl 1300610 `"1300610"', add
label define countynhg_lbl 1300630 `"1300630"', add
label define countynhg_lbl 1300650 `"1300650"', add
label define countynhg_lbl 1300670 `"1300670"', add
label define countynhg_lbl 1300690 `"1300690"', add
label define countynhg_lbl 1300710 `"1300710"', add
label define countynhg_lbl 1300730 `"1300730"', add
label define countynhg_lbl 1300750 `"1300750"', add
label define countynhg_lbl 1300770 `"1300770"', add
label define countynhg_lbl 1300790 `"1300790"', add
label define countynhg_lbl 1300810 `"1300810"', add
label define countynhg_lbl 1300830 `"1300830"', add
label define countynhg_lbl 1300850 `"1300850"', add
label define countynhg_lbl 1300870 `"1300870"', add
label define countynhg_lbl 1300890 `"1300890"', add
label define countynhg_lbl 1300910 `"1300910"', add
label define countynhg_lbl 1300930 `"1300930"', add
label define countynhg_lbl 1300950 `"1300950"', add
label define countynhg_lbl 1300970 `"1300970"', add
label define countynhg_lbl 1300990 `"1300990"', add
label define countynhg_lbl 1301010 `"1301010"', add
label define countynhg_lbl 1301030 `"1301030"', add
label define countynhg_lbl 1301050 `"1301050"', add
label define countynhg_lbl 1301070 `"1301070"', add
label define countynhg_lbl 1301090 `"1301090"', add
label define countynhg_lbl 1301110 `"1301110"', add
label define countynhg_lbl 1301130 `"1301130"', add
label define countynhg_lbl 1301150 `"1301150"', add
label define countynhg_lbl 1301170 `"1301170"', add
label define countynhg_lbl 1301190 `"1301190"', add
label define countynhg_lbl 1301210 `"1301210"', add
label define countynhg_lbl 1301230 `"1301230"', add
label define countynhg_lbl 1301250 `"1301250"', add
label define countynhg_lbl 1301270 `"1301270"', add
label define countynhg_lbl 1301290 `"1301290"', add
label define countynhg_lbl 1301310 `"1301310"', add
label define countynhg_lbl 1301330 `"1301330"', add
label define countynhg_lbl 1301350 `"1301350"', add
label define countynhg_lbl 1301370 `"1301370"', add
label define countynhg_lbl 1301390 `"1301390"', add
label define countynhg_lbl 1301410 `"1301410"', add
label define countynhg_lbl 1301430 `"1301430"', add
label define countynhg_lbl 1301450 `"1301450"', add
label define countynhg_lbl 1301470 `"1301470"', add
label define countynhg_lbl 1301490 `"1301490"', add
label define countynhg_lbl 1301510 `"1301510"', add
label define countynhg_lbl 1301530 `"1301530"', add
label define countynhg_lbl 1301550 `"1301550"', add
label define countynhg_lbl 1301570 `"1301570"', add
label define countynhg_lbl 1301590 `"1301590"', add
label define countynhg_lbl 1301610 `"1301610"', add
label define countynhg_lbl 1301630 `"1301630"', add
label define countynhg_lbl 1301650 `"1301650"', add
label define countynhg_lbl 1301670 `"1301670"', add
label define countynhg_lbl 1301690 `"1301690"', add
label define countynhg_lbl 1301710 `"1301710"', add
label define countynhg_lbl 1301730 `"1301730"', add
label define countynhg_lbl 1301750 `"1301750"', add
label define countynhg_lbl 1301770 `"1301770"', add
label define countynhg_lbl 1301790 `"1301790"', add
label define countynhg_lbl 1301810 `"1301810"', add
label define countynhg_lbl 1301830 `"1301830"', add
label define countynhg_lbl 1301850 `"1301850"', add
label define countynhg_lbl 1301870 `"1301870"', add
label define countynhg_lbl 1301890 `"1301890"', add
label define countynhg_lbl 1301910 `"1301910"', add
label define countynhg_lbl 1301930 `"1301930"', add
label define countynhg_lbl 1301950 `"1301950"', add
label define countynhg_lbl 1301970 `"1301970"', add
label define countynhg_lbl 1301990 `"1301990"', add
label define countynhg_lbl 1302010 `"1302010"', add
label define countynhg_lbl 1302015 `"1302015"', add
label define countynhg_lbl 1302050 `"1302050"', add
label define countynhg_lbl 1302070 `"1302070"', add
label define countynhg_lbl 1302090 `"1302090"', add
label define countynhg_lbl 1302110 `"1302110"', add
label define countynhg_lbl 1302130 `"1302130"', add
label define countynhg_lbl 1302150 `"1302150"', add
label define countynhg_lbl 1302170 `"1302170"', add
label define countynhg_lbl 1302190 `"1302190"', add
label define countynhg_lbl 1302210 `"1302210"', add
label define countynhg_lbl 1302230 `"1302230"', add
label define countynhg_lbl 1302250 `"1302250"', add
label define countynhg_lbl 1302270 `"1302270"', add
label define countynhg_lbl 1302290 `"1302290"', add
label define countynhg_lbl 1302310 `"1302310"', add
label define countynhg_lbl 1302330 `"1302330"', add
label define countynhg_lbl 1302350 `"1302350"', add
label define countynhg_lbl 1302370 `"1302370"', add
label define countynhg_lbl 1302390 `"1302390"', add
label define countynhg_lbl 1302410 `"1302410"', add
label define countynhg_lbl 1302430 `"1302430"', add
label define countynhg_lbl 1302450 `"1302450"', add
label define countynhg_lbl 1302470 `"1302470"', add
label define countynhg_lbl 1302490 `"1302490"', add
label define countynhg_lbl 1302510 `"1302510"', add
label define countynhg_lbl 1302530 `"1302530"', add
label define countynhg_lbl 1302550 `"1302550"', add
label define countynhg_lbl 1302570 `"1302570"', add
label define countynhg_lbl 1302590 `"1302590"', add
label define countynhg_lbl 1302610 `"1302610"', add
label define countynhg_lbl 1302630 `"1302630"', add
label define countynhg_lbl 1302650 `"1302650"', add
label define countynhg_lbl 1302670 `"1302670"', add
label define countynhg_lbl 1302690 `"1302690"', add
label define countynhg_lbl 1302710 `"1302710"', add
label define countynhg_lbl 1302730 `"1302730"', add
label define countynhg_lbl 1302750 `"1302750"', add
label define countynhg_lbl 1302770 `"1302770"', add
label define countynhg_lbl 1302790 `"1302790"', add
label define countynhg_lbl 1302810 `"1302810"', add
label define countynhg_lbl 1302830 `"1302830"', add
label define countynhg_lbl 1302850 `"1302850"', add
label define countynhg_lbl 1302870 `"1302870"', add
label define countynhg_lbl 1302890 `"1302890"', add
label define countynhg_lbl 1302910 `"1302910"', add
label define countynhg_lbl 1302930 `"1302930"', add
label define countynhg_lbl 1302950 `"1302950"', add
label define countynhg_lbl 1302970 `"1302970"', add
label define countynhg_lbl 1302990 `"1302990"', add
label define countynhg_lbl 1303010 `"1303010"', add
label define countynhg_lbl 1303030 `"1303030"', add
label define countynhg_lbl 1303050 `"1303050"', add
label define countynhg_lbl 1303070 `"1303070"', add
label define countynhg_lbl 1303090 `"1303090"', add
label define countynhg_lbl 1303110 `"1303110"', add
label define countynhg_lbl 1303130 `"1303130"', add
label define countynhg_lbl 1303150 `"1303150"', add
label define countynhg_lbl 1303170 `"1303170"', add
label define countynhg_lbl 1303190 `"1303190"', add
label define countynhg_lbl 1303210 `"1303210"', add
label define countynhg_lbl 1550015 `"1550015"', add
label define countynhg_lbl 1550035 `"1550035"', add
label define countynhg_lbl 1550055 `"1550055"', add
label define countynhg_lbl 1550075 `"1550075"', add
label define countynhg_lbl 1550095 `"1550095"', add
label define countynhg_lbl 1550115 `"1550115"', add
label define countynhg_lbl 1550135 `"1550135"', add
label define countynhg_lbl 1550155 `"1550155"', add
label define countynhg_lbl 1600010 `"1600010"', add
label define countynhg_lbl 1600030 `"1600030"', add
label define countynhg_lbl 1600050 `"1600050"', add
label define countynhg_lbl 1600070 `"1600070"', add
label define countynhg_lbl 1600090 `"1600090"', add
label define countynhg_lbl 1600110 `"1600110"', add
label define countynhg_lbl 1600130 `"1600130"', add
label define countynhg_lbl 1600150 `"1600150"', add
label define countynhg_lbl 1600170 `"1600170"', add
label define countynhg_lbl 1600190 `"1600190"', add
label define countynhg_lbl 1600210 `"1600210"', add
label define countynhg_lbl 1600230 `"1600230"', add
label define countynhg_lbl 1600250 `"1600250"', add
label define countynhg_lbl 1600270 `"1600270"', add
label define countynhg_lbl 1600290 `"1600290"', add
label define countynhg_lbl 1600310 `"1600310"', add
label define countynhg_lbl 1600330 `"1600330"', add
label define countynhg_lbl 1600350 `"1600350"', add
label define countynhg_lbl 1600370 `"1600370"', add
label define countynhg_lbl 1600390 `"1600390"', add
label define countynhg_lbl 1600410 `"1600410"', add
label define countynhg_lbl 1600430 `"1600430"', add
label define countynhg_lbl 1600450 `"1600450"', add
label define countynhg_lbl 1600470 `"1600470"', add
label define countynhg_lbl 1600490 `"1600490"', add
label define countynhg_lbl 1600510 `"1600510"', add
label define countynhg_lbl 1600530 `"1600530"', add
label define countynhg_lbl 1600550 `"1600550"', add
label define countynhg_lbl 1600570 `"1600570"', add
label define countynhg_lbl 1600590 `"1600590"', add
label define countynhg_lbl 1600610 `"1600610"', add
label define countynhg_lbl 1600630 `"1600630"', add
label define countynhg_lbl 1600650 `"1600650"', add
label define countynhg_lbl 1600670 `"1600670"', add
label define countynhg_lbl 1600690 `"1600690"', add
label define countynhg_lbl 1600710 `"1600710"', add
label define countynhg_lbl 1600730 `"1600730"', add
label define countynhg_lbl 1600750 `"1600750"', add
label define countynhg_lbl 1600770 `"1600770"', add
label define countynhg_lbl 1600790 `"1600790"', add
label define countynhg_lbl 1600810 `"1600810"', add
label define countynhg_lbl 1600830 `"1600830"', add
label define countynhg_lbl 1600850 `"1600850"', add
label define countynhg_lbl 1600870 `"1600870"', add
label define countynhg_lbl 1600875 `"1600875"', add
label define countynhg_lbl 1650015 `"1650015"', add
label define countynhg_lbl 1650035 `"1650035"', add
label define countynhg_lbl 1650055 `"1650055"', add
label define countynhg_lbl 1650075 `"1650075"', add
label define countynhg_lbl 1650095 `"1650095"', add
label define countynhg_lbl 1650115 `"1650115"', add
label define countynhg_lbl 1650135 `"1650135"', add
label define countynhg_lbl 1650155 `"1650155"', add
label define countynhg_lbl 1650175 `"1650175"', add
label define countynhg_lbl 1650195 `"1650195"', add
label define countynhg_lbl 1650215 `"1650215"', add
label define countynhg_lbl 1650235 `"1650235"', add
label define countynhg_lbl 1650255 `"1650255"', add
label define countynhg_lbl 1700010 `"1700010"', add
label define countynhg_lbl 1700030 `"1700030"', add
label define countynhg_lbl 1700050 `"1700050"', add
label define countynhg_lbl 1700070 `"1700070"', add
label define countynhg_lbl 1700090 `"1700090"', add
label define countynhg_lbl 1700110 `"1700110"', add
label define countynhg_lbl 1700130 `"1700130"', add
label define countynhg_lbl 1700150 `"1700150"', add
label define countynhg_lbl 1700170 `"1700170"', add
label define countynhg_lbl 1700190 `"1700190"', add
label define countynhg_lbl 1700210 `"1700210"', add
label define countynhg_lbl 1700230 `"1700230"', add
label define countynhg_lbl 1700250 `"1700250"', add
label define countynhg_lbl 1700270 `"1700270"', add
label define countynhg_lbl 1700290 `"1700290"', add
label define countynhg_lbl 1700310 `"1700310"', add
label define countynhg_lbl 1700330 `"1700330"', add
label define countynhg_lbl 1700350 `"1700350"', add
label define countynhg_lbl 1700370 `"1700370"', add
label define countynhg_lbl 1700390 `"1700390"', add
label define countynhg_lbl 1700410 `"1700410"', add
label define countynhg_lbl 1700430 `"1700430"', add
label define countynhg_lbl 1700450 `"1700450"', add
label define countynhg_lbl 1700470 `"1700470"', add
label define countynhg_lbl 1700490 `"1700490"', add
label define countynhg_lbl 1700510 `"1700510"', add
label define countynhg_lbl 1700530 `"1700530"', add
label define countynhg_lbl 1700550 `"1700550"', add
label define countynhg_lbl 1700570 `"1700570"', add
label define countynhg_lbl 1700590 `"1700590"', add
label define countynhg_lbl 1700610 `"1700610"', add
label define countynhg_lbl 1700630 `"1700630"', add
label define countynhg_lbl 1700650 `"1700650"', add
label define countynhg_lbl 1700670 `"1700670"', add
label define countynhg_lbl 1700690 `"1700690"', add
label define countynhg_lbl 1700710 `"1700710"', add
label define countynhg_lbl 1700730 `"1700730"', add
label define countynhg_lbl 1700750 `"1700750"', add
label define countynhg_lbl 1700770 `"1700770"', add
label define countynhg_lbl 1700790 `"1700790"', add
label define countynhg_lbl 1700810 `"1700810"', add
label define countynhg_lbl 1700830 `"1700830"', add
label define countynhg_lbl 1700850 `"1700850"', add
label define countynhg_lbl 1700870 `"1700870"', add
label define countynhg_lbl 1700890 `"1700890"', add
label define countynhg_lbl 1700910 `"1700910"', add
label define countynhg_lbl 1700930 `"1700930"', add
label define countynhg_lbl 1700950 `"1700950"', add
label define countynhg_lbl 1700970 `"1700970"', add
label define countynhg_lbl 1700990 `"1700990"', add
label define countynhg_lbl 1701010 `"1701010"', add
label define countynhg_lbl 1701030 `"1701030"', add
label define countynhg_lbl 1701050 `"1701050"', add
label define countynhg_lbl 1701070 `"1701070"', add
label define countynhg_lbl 1701090 `"1701090"', add
label define countynhg_lbl 1701110 `"1701110"', add
label define countynhg_lbl 1701130 `"1701130"', add
label define countynhg_lbl 1701150 `"1701150"', add
label define countynhg_lbl 1701170 `"1701170"', add
label define countynhg_lbl 1701190 `"1701190"', add
label define countynhg_lbl 1701210 `"1701210"', add
label define countynhg_lbl 1701230 `"1701230"', add
label define countynhg_lbl 1701250 `"1701250"', add
label define countynhg_lbl 1701270 `"1701270"', add
label define countynhg_lbl 1701290 `"1701290"', add
label define countynhg_lbl 1701310 `"1701310"', add
label define countynhg_lbl 1701330 `"1701330"', add
label define countynhg_lbl 1701350 `"1701350"', add
label define countynhg_lbl 1701370 `"1701370"', add
label define countynhg_lbl 1701390 `"1701390"', add
label define countynhg_lbl 1701410 `"1701410"', add
label define countynhg_lbl 1701430 `"1701430"', add
label define countynhg_lbl 1701450 `"1701450"', add
label define countynhg_lbl 1701470 `"1701470"', add
label define countynhg_lbl 1701490 `"1701490"', add
label define countynhg_lbl 1701510 `"1701510"', add
label define countynhg_lbl 1701530 `"1701530"', add
label define countynhg_lbl 1701550 `"1701550"', add
label define countynhg_lbl 1701570 `"1701570"', add
label define countynhg_lbl 1701590 `"1701590"', add
label define countynhg_lbl 1701610 `"1701610"', add
label define countynhg_lbl 1701630 `"1701630"', add
label define countynhg_lbl 1701650 `"1701650"', add
label define countynhg_lbl 1701670 `"1701670"', add
label define countynhg_lbl 1701690 `"1701690"', add
label define countynhg_lbl 1701710 `"1701710"', add
label define countynhg_lbl 1701730 `"1701730"', add
label define countynhg_lbl 1701750 `"1701750"', add
label define countynhg_lbl 1701770 `"1701770"', add
label define countynhg_lbl 1701790 `"1701790"', add
label define countynhg_lbl 1701810 `"1701810"', add
label define countynhg_lbl 1701830 `"1701830"', add
label define countynhg_lbl 1701850 `"1701850"', add
label define countynhg_lbl 1701870 `"1701870"', add
label define countynhg_lbl 1701890 `"1701890"', add
label define countynhg_lbl 1701910 `"1701910"', add
label define countynhg_lbl 1701930 `"1701930"', add
label define countynhg_lbl 1701950 `"1701950"', add
label define countynhg_lbl 1701970 `"1701970"', add
label define countynhg_lbl 1701990 `"1701990"', add
label define countynhg_lbl 1702010 `"1702010"', add
label define countynhg_lbl 1702030 `"1702030"', add
label define countynhg_lbl 1780000 `"1780000"', add
label define countynhg_lbl 1789015 `"1789015"', add
label define countynhg_lbl 1789035 `"1789035"', add
label define countynhg_lbl 1789055 `"1789055"', add
label define countynhg_lbl 1789075 `"1789075"', add
label define countynhg_lbl 1789095 `"1789095"', add
label define countynhg_lbl 1789175 `"1789175"', add
label define countynhg_lbl 1800010 `"1800010"', add
label define countynhg_lbl 1800030 `"1800030"', add
label define countynhg_lbl 1800050 `"1800050"', add
label define countynhg_lbl 1800070 `"1800070"', add
label define countynhg_lbl 1800090 `"1800090"', add
label define countynhg_lbl 1800110 `"1800110"', add
label define countynhg_lbl 1800130 `"1800130"', add
label define countynhg_lbl 1800150 `"1800150"', add
label define countynhg_lbl 1800170 `"1800170"', add
label define countynhg_lbl 1800190 `"1800190"', add
label define countynhg_lbl 1800210 `"1800210"', add
label define countynhg_lbl 1800230 `"1800230"', add
label define countynhg_lbl 1800250 `"1800250"', add
label define countynhg_lbl 1800270 `"1800270"', add
label define countynhg_lbl 1800290 `"1800290"', add
label define countynhg_lbl 1800310 `"1800310"', add
label define countynhg_lbl 1800330 `"1800330"', add
label define countynhg_lbl 1800350 `"1800350"', add
label define countynhg_lbl 1800370 `"1800370"', add
label define countynhg_lbl 1800390 `"1800390"', add
label define countynhg_lbl 1800410 `"1800410"', add
label define countynhg_lbl 1800430 `"1800430"', add
label define countynhg_lbl 1800450 `"1800450"', add
label define countynhg_lbl 1800470 `"1800470"', add
label define countynhg_lbl 1800490 `"1800490"', add
label define countynhg_lbl 1800510 `"1800510"', add
label define countynhg_lbl 1800530 `"1800530"', add
label define countynhg_lbl 1800550 `"1800550"', add
label define countynhg_lbl 1800570 `"1800570"', add
label define countynhg_lbl 1800590 `"1800590"', add
label define countynhg_lbl 1800610 `"1800610"', add
label define countynhg_lbl 1800630 `"1800630"', add
label define countynhg_lbl 1800650 `"1800650"', add
label define countynhg_lbl 1800670 `"1800670"', add
label define countynhg_lbl 1800690 `"1800690"', add
label define countynhg_lbl 1800710 `"1800710"', add
label define countynhg_lbl 1800730 `"1800730"', add
label define countynhg_lbl 1800750 `"1800750"', add
label define countynhg_lbl 1800770 `"1800770"', add
label define countynhg_lbl 1800790 `"1800790"', add
label define countynhg_lbl 1800810 `"1800810"', add
label define countynhg_lbl 1800830 `"1800830"', add
label define countynhg_lbl 1800850 `"1800850"', add
label define countynhg_lbl 1800870 `"1800870"', add
label define countynhg_lbl 1800890 `"1800890"', add
label define countynhg_lbl 1800910 `"1800910"', add
label define countynhg_lbl 1800930 `"1800930"', add
label define countynhg_lbl 1800950 `"1800950"', add
label define countynhg_lbl 1800970 `"1800970"', add
label define countynhg_lbl 1800990 `"1800990"', add
label define countynhg_lbl 1801010 `"1801010"', add
label define countynhg_lbl 1801030 `"1801030"', add
label define countynhg_lbl 1801050 `"1801050"', add
label define countynhg_lbl 1801070 `"1801070"', add
label define countynhg_lbl 1801090 `"1801090"', add
label define countynhg_lbl 1801110 `"1801110"', add
label define countynhg_lbl 1801130 `"1801130"', add
label define countynhg_lbl 1801150 `"1801150"', add
label define countynhg_lbl 1801170 `"1801170"', add
label define countynhg_lbl 1801190 `"1801190"', add
label define countynhg_lbl 1801210 `"1801210"', add
label define countynhg_lbl 1801230 `"1801230"', add
label define countynhg_lbl 1801250 `"1801250"', add
label define countynhg_lbl 1801270 `"1801270"', add
label define countynhg_lbl 1801290 `"1801290"', add
label define countynhg_lbl 1801310 `"1801310"', add
label define countynhg_lbl 1801330 `"1801330"', add
label define countynhg_lbl 1801350 `"1801350"', add
label define countynhg_lbl 1801370 `"1801370"', add
label define countynhg_lbl 1801390 `"1801390"', add
label define countynhg_lbl 1801410 `"1801410"', add
label define countynhg_lbl 1801430 `"1801430"', add
label define countynhg_lbl 1801450 `"1801450"', add
label define countynhg_lbl 1801470 `"1801470"', add
label define countynhg_lbl 1801490 `"1801490"', add
label define countynhg_lbl 1801510 `"1801510"', add
label define countynhg_lbl 1801530 `"1801530"', add
label define countynhg_lbl 1801550 `"1801550"', add
label define countynhg_lbl 1801570 `"1801570"', add
label define countynhg_lbl 1801590 `"1801590"', add
label define countynhg_lbl 1801610 `"1801610"', add
label define countynhg_lbl 1801630 `"1801630"', add
label define countynhg_lbl 1801650 `"1801650"', add
label define countynhg_lbl 1801670 `"1801670"', add
label define countynhg_lbl 1801690 `"1801690"', add
label define countynhg_lbl 1801710 `"1801710"', add
label define countynhg_lbl 1801730 `"1801730"', add
label define countynhg_lbl 1801750 `"1801750"', add
label define countynhg_lbl 1801770 `"1801770"', add
label define countynhg_lbl 1801790 `"1801790"', add
label define countynhg_lbl 1801810 `"1801810"', add
label define countynhg_lbl 1801830 `"1801830"', add
label define countynhg_lbl 1900010 `"1900010"', add
label define countynhg_lbl 1900030 `"1900030"', add
label define countynhg_lbl 1900050 `"1900050"', add
label define countynhg_lbl 1900070 `"1900070"', add
label define countynhg_lbl 1900090 `"1900090"', add
label define countynhg_lbl 1900110 `"1900110"', add
label define countynhg_lbl 1900130 `"1900130"', add
label define countynhg_lbl 1900150 `"1900150"', add
label define countynhg_lbl 1900170 `"1900170"', add
label define countynhg_lbl 1900190 `"1900190"', add
label define countynhg_lbl 1900210 `"1900210"', add
label define countynhg_lbl 1900215 `"1900215"', add
label define countynhg_lbl 1900230 `"1900230"', add
label define countynhg_lbl 1900250 `"1900250"', add
label define countynhg_lbl 1900270 `"1900270"', add
label define countynhg_lbl 1900290 `"1900290"', add
label define countynhg_lbl 1900310 `"1900310"', add
label define countynhg_lbl 1900330 `"1900330"', add
label define countynhg_lbl 1900350 `"1900350"', add
label define countynhg_lbl 1900370 `"1900370"', add
label define countynhg_lbl 1900390 `"1900390"', add
label define countynhg_lbl 1900410 `"1900410"', add
label define countynhg_lbl 1900430 `"1900430"', add
label define countynhg_lbl 1900450 `"1900450"', add
label define countynhg_lbl 1900470 `"1900470"', add
label define countynhg_lbl 1900490 `"1900490"', add
label define countynhg_lbl 1900510 `"1900510"', add
label define countynhg_lbl 1900530 `"1900530"', add
label define countynhg_lbl 1900550 `"1900550"', add
label define countynhg_lbl 1900570 `"1900570"', add
label define countynhg_lbl 1900590 `"1900590"', add
label define countynhg_lbl 1900610 `"1900610"', add
label define countynhg_lbl 1900630 `"1900630"', add
label define countynhg_lbl 1900650 `"1900650"', add
label define countynhg_lbl 1900670 `"1900670"', add
label define countynhg_lbl 1900690 `"1900690"', add
label define countynhg_lbl 1900710 `"1900710"', add
label define countynhg_lbl 1900730 `"1900730"', add
label define countynhg_lbl 1900750 `"1900750"', add
label define countynhg_lbl 1900770 `"1900770"', add
label define countynhg_lbl 1900790 `"1900790"', add
label define countynhg_lbl 1900810 `"1900810"', add
label define countynhg_lbl 1900830 `"1900830"', add
label define countynhg_lbl 1900850 `"1900850"', add
label define countynhg_lbl 1900870 `"1900870"', add
label define countynhg_lbl 1900890 `"1900890"', add
label define countynhg_lbl 1900910 `"1900910"', add
label define countynhg_lbl 1900930 `"1900930"', add
label define countynhg_lbl 1900950 `"1900950"', add
label define countynhg_lbl 1900970 `"1900970"', add
label define countynhg_lbl 1900990 `"1900990"', add
label define countynhg_lbl 1901010 `"1901010"', add
label define countynhg_lbl 1901030 `"1901030"', add
label define countynhg_lbl 1901050 `"1901050"', add
label define countynhg_lbl 1901070 `"1901070"', add
label define countynhg_lbl 1901090 `"1901090"', add
label define countynhg_lbl 1901110 `"1901110"', add
label define countynhg_lbl 1901130 `"1901130"', add
label define countynhg_lbl 1901150 `"1901150"', add
label define countynhg_lbl 1901170 `"1901170"', add
label define countynhg_lbl 1901190 `"1901190"', add
label define countynhg_lbl 1901210 `"1901210"', add
label define countynhg_lbl 1901230 `"1901230"', add
label define countynhg_lbl 1901250 `"1901250"', add
label define countynhg_lbl 1901270 `"1901270"', add
label define countynhg_lbl 1901290 `"1901290"', add
label define countynhg_lbl 1901310 `"1901310"', add
label define countynhg_lbl 1901330 `"1901330"', add
label define countynhg_lbl 1901350 `"1901350"', add
label define countynhg_lbl 1901370 `"1901370"', add
label define countynhg_lbl 1901390 `"1901390"', add
label define countynhg_lbl 1901410 `"1901410"', add
label define countynhg_lbl 1901430 `"1901430"', add
label define countynhg_lbl 1901450 `"1901450"', add
label define countynhg_lbl 1901470 `"1901470"', add
label define countynhg_lbl 1901490 `"1901490"', add
label define countynhg_lbl 1901510 `"1901510"', add
label define countynhg_lbl 1901530 `"1901530"', add
label define countynhg_lbl 1901550 `"1901550"', add
label define countynhg_lbl 1901570 `"1901570"', add
label define countynhg_lbl 1901590 `"1901590"', add
label define countynhg_lbl 1901610 `"1901610"', add
label define countynhg_lbl 1901630 `"1901630"', add
label define countynhg_lbl 1901650 `"1901650"', add
label define countynhg_lbl 1901670 `"1901670"', add
label define countynhg_lbl 1901690 `"1901690"', add
label define countynhg_lbl 1901710 `"1901710"', add
label define countynhg_lbl 1901730 `"1901730"', add
label define countynhg_lbl 1901750 `"1901750"', add
label define countynhg_lbl 1901770 `"1901770"', add
label define countynhg_lbl 1901790 `"1901790"', add
label define countynhg_lbl 1901810 `"1901810"', add
label define countynhg_lbl 1901830 `"1901830"', add
label define countynhg_lbl 1901850 `"1901850"', add
label define countynhg_lbl 1901870 `"1901870"', add
label define countynhg_lbl 1901890 `"1901890"', add
label define countynhg_lbl 1901910 `"1901910"', add
label define countynhg_lbl 1901930 `"1901930"', add
label define countynhg_lbl 1901950 `"1901950"', add
label define countynhg_lbl 1901970 `"1901970"', add
label define countynhg_lbl 2000010 `"2000010"', add
label define countynhg_lbl 2000030 `"2000030"', add
label define countynhg_lbl 2000035 `"2000035"', add
label define countynhg_lbl 2000050 `"2000050"', add
label define countynhg_lbl 2000070 `"2000070"', add
label define countynhg_lbl 2000090 `"2000090"', add
label define countynhg_lbl 2000110 `"2000110"', add
label define countynhg_lbl 2000130 `"2000130"', add
label define countynhg_lbl 2000135 `"2000135"', add
label define countynhg_lbl 2000150 `"2000150"', add
label define countynhg_lbl 2000170 `"2000170"', add
label define countynhg_lbl 2000190 `"2000190"', add
label define countynhg_lbl 2000210 `"2000210"', add
label define countynhg_lbl 2000230 `"2000230"', add
label define countynhg_lbl 2000250 `"2000250"', add
label define countynhg_lbl 2000270 `"2000270"', add
label define countynhg_lbl 2000290 `"2000290"', add
label define countynhg_lbl 2000310 `"2000310"', add
label define countynhg_lbl 2000330 `"2000330"', add
label define countynhg_lbl 2000350 `"2000350"', add
label define countynhg_lbl 2000370 `"2000370"', add
label define countynhg_lbl 2000375 `"2000375"', add
label define countynhg_lbl 2000390 `"2000390"', add
label define countynhg_lbl 2000410 `"2000410"', add
label define countynhg_lbl 2000430 `"2000430"', add
label define countynhg_lbl 2000450 `"2000450"', add
label define countynhg_lbl 2000470 `"2000470"', add
label define countynhg_lbl 2000490 `"2000490"', add
label define countynhg_lbl 2000510 `"2000510"', add
label define countynhg_lbl 2000530 `"2000530"', add
label define countynhg_lbl 2000550 `"2000550"', add
label define countynhg_lbl 2000555 `"2000555"', add
label define countynhg_lbl 2000570 `"2000570"', add
label define countynhg_lbl 2000590 `"2000590"', add
label define countynhg_lbl 2000610 `"2000610"', add
label define countynhg_lbl 2000630 `"2000630"', add
label define countynhg_lbl 2000650 `"2000650"', add
label define countynhg_lbl 2000670 `"2000670"', add
label define countynhg_lbl 2000690 `"2000690"', add
label define countynhg_lbl 2000710 `"2000710"', add
label define countynhg_lbl 2000730 `"2000730"', add
label define countynhg_lbl 2000750 `"2000750"', add
label define countynhg_lbl 2000770 `"2000770"', add
label define countynhg_lbl 2000790 `"2000790"', add
label define countynhg_lbl 2000810 `"2000810"', add
label define countynhg_lbl 2000830 `"2000830"', add
label define countynhg_lbl 2000833 `"2000833"', add
label define countynhg_lbl 2000850 `"2000850"', add
label define countynhg_lbl 2000870 `"2000870"', add
label define countynhg_lbl 2000890 `"2000890"', add
label define countynhg_lbl 2000910 `"2000910"', add
label define countynhg_lbl 2000915 `"2000915"', add
label define countynhg_lbl 2000930 `"2000930"', add
label define countynhg_lbl 2000950 `"2000950"', add
label define countynhg_lbl 2000970 `"2000970"', add
label define countynhg_lbl 2000990 `"2000990"', add
label define countynhg_lbl 2001010 `"2001010"', add
label define countynhg_lbl 2001030 `"2001030"', add
label define countynhg_lbl 2001050 `"2001050"', add
label define countynhg_lbl 2001070 `"2001070"', add
label define countynhg_lbl 2001090 `"2001090"', add
label define countynhg_lbl 2001110 `"2001110"', add
label define countynhg_lbl 2001130 `"2001130"', add
label define countynhg_lbl 2001150 `"2001150"', add
label define countynhg_lbl 2001170 `"2001170"', add
label define countynhg_lbl 2001190 `"2001190"', add
label define countynhg_lbl 2001210 `"2001210"', add
label define countynhg_lbl 2001230 `"2001230"', add
label define countynhg_lbl 2001250 `"2001250"', add
label define countynhg_lbl 2001270 `"2001270"', add
label define countynhg_lbl 2001290 `"2001290"', add
label define countynhg_lbl 2001310 `"2001310"', add
label define countynhg_lbl 2001330 `"2001330"', add
label define countynhg_lbl 2001350 `"2001350"', add
label define countynhg_lbl 2001370 `"2001370"', add
label define countynhg_lbl 2001390 `"2001390"', add
label define countynhg_lbl 2001410 `"2001410"', add
label define countynhg_lbl 2001430 `"2001430"', add
label define countynhg_lbl 2001450 `"2001450"', add
label define countynhg_lbl 2001470 `"2001470"', add
label define countynhg_lbl 2001490 `"2001490"', add
label define countynhg_lbl 2001510 `"2001510"', add
label define countynhg_lbl 2001530 `"2001530"', add
label define countynhg_lbl 2001550 `"2001550"', add
label define countynhg_lbl 2001570 `"2001570"', add
label define countynhg_lbl 2001590 `"2001590"', add
label define countynhg_lbl 2001610 `"2001610"', add
label define countynhg_lbl 2001630 `"2001630"', add
label define countynhg_lbl 2001650 `"2001650"', add
label define countynhg_lbl 2001670 `"2001670"', add
label define countynhg_lbl 2001690 `"2001690"', add
label define countynhg_lbl 2001710 `"2001710"', add
label define countynhg_lbl 2001730 `"2001730"', add
label define countynhg_lbl 2001735 `"2001735"', add
label define countynhg_lbl 2001750 `"2001750"', add
label define countynhg_lbl 2001770 `"2001770"', add
label define countynhg_lbl 2001790 `"2001790"', add
label define countynhg_lbl 2001810 `"2001810"', add
label define countynhg_lbl 2001830 `"2001830"', add
label define countynhg_lbl 2001850 `"2001850"', add
label define countynhg_lbl 2001870 `"2001870"', add
label define countynhg_lbl 2001890 `"2001890"', add
label define countynhg_lbl 2001910 `"2001910"', add
label define countynhg_lbl 2001930 `"2001930"', add
label define countynhg_lbl 2001950 `"2001950"', add
label define countynhg_lbl 2001970 `"2001970"', add
label define countynhg_lbl 2001990 `"2001990"', add
label define countynhg_lbl 2002010 `"2002010"', add
label define countynhg_lbl 2002030 `"2002030"', add
label define countynhg_lbl 2002050 `"2002050"', add
label define countynhg_lbl 2002070 `"2002070"', add
label define countynhg_lbl 2002090 `"2002090"', add
label define countynhg_lbl 2050015 `"2050015"', add
label define countynhg_lbl 2050035 `"2050035"', add
label define countynhg_lbl 2050055 `"2050055"', add
label define countynhg_lbl 2050075 `"2050075"', add
label define countynhg_lbl 2050095 `"2050095"', add
label define countynhg_lbl 2050115 `"2050115"', add
label define countynhg_lbl 2050135 `"2050135"', add
label define countynhg_lbl 2050155 `"2050155"', add
label define countynhg_lbl 2050175 `"2050175"', add
label define countynhg_lbl 2050195 `"2050195"', add
label define countynhg_lbl 2050215 `"2050215"', add
label define countynhg_lbl 2050255 `"2050255"', add
label define countynhg_lbl 2050275 `"2050275"', add
label define countynhg_lbl 2050295 `"2050295"', add
label define countynhg_lbl 2050315 `"2050315"', add
label define countynhg_lbl 2050335 `"2050335"', add
label define countynhg_lbl 2050355 `"2050355"', add
label define countynhg_lbl 2050395 `"2050395"', add
label define countynhg_lbl 2050435 `"2050435"', add
label define countynhg_lbl 2050455 `"2050455"', add
label define countynhg_lbl 2050475 `"2050475"', add
label define countynhg_lbl 2050495 `"2050495"', add
label define countynhg_lbl 2050515 `"2050515"', add
label define countynhg_lbl 2050535 `"2050535"', add
label define countynhg_lbl 2050555 `"2050555"', add
label define countynhg_lbl 2050575 `"2050575"', add
label define countynhg_lbl 2050595 `"2050595"', add
label define countynhg_lbl 2050615 `"2050615"', add
label define countynhg_lbl 2050635 `"2050635"', add
label define countynhg_lbl 2050655 `"2050655"', add
label define countynhg_lbl 2050695 `"2050695"', add
label define countynhg_lbl 2050715 `"2050715"', add
label define countynhg_lbl 2050735 `"2050735"', add
label define countynhg_lbl 2050755 `"2050755"', add
label define countynhg_lbl 2050795 `"2050795"', add
label define countynhg_lbl 2050815 `"2050815"', add
label define countynhg_lbl 2050835 `"2050835"', add
label define countynhg_lbl 2050855 `"2050855"', add
label define countynhg_lbl 2050875 `"2050875"', add
label define countynhg_lbl 2052090 `"2052090"', add
label define countynhg_lbl 2059999 `"2059999"', add
label define countynhg_lbl 2100010 `"2100010"', add
label define countynhg_lbl 2100030 `"2100030"', add
label define countynhg_lbl 2100050 `"2100050"', add
label define countynhg_lbl 2100070 `"2100070"', add
label define countynhg_lbl 2100090 `"2100090"', add
label define countynhg_lbl 2100110 `"2100110"', add
label define countynhg_lbl 2100130 `"2100130"', add
label define countynhg_lbl 2100150 `"2100150"', add
label define countynhg_lbl 2100170 `"2100170"', add
label define countynhg_lbl 2100190 `"2100190"', add
label define countynhg_lbl 2100210 `"2100210"', add
label define countynhg_lbl 2100230 `"2100230"', add
label define countynhg_lbl 2100250 `"2100250"', add
label define countynhg_lbl 2100270 `"2100270"', add
label define countynhg_lbl 2100290 `"2100290"', add
label define countynhg_lbl 2100310 `"2100310"', add
label define countynhg_lbl 2100330 `"2100330"', add
label define countynhg_lbl 2100350 `"2100350"', add
label define countynhg_lbl 2100370 `"2100370"', add
label define countynhg_lbl 2100390 `"2100390"', add
label define countynhg_lbl 2100410 `"2100410"', add
label define countynhg_lbl 2100430 `"2100430"', add
label define countynhg_lbl 2100450 `"2100450"', add
label define countynhg_lbl 2100470 `"2100470"', add
label define countynhg_lbl 2100490 `"2100490"', add
label define countynhg_lbl 2100510 `"2100510"', add
label define countynhg_lbl 2100530 `"2100530"', add
label define countynhg_lbl 2100550 `"2100550"', add
label define countynhg_lbl 2100570 `"2100570"', add
label define countynhg_lbl 2100590 `"2100590"', add
label define countynhg_lbl 2100610 `"2100610"', add
label define countynhg_lbl 2100630 `"2100630"', add
label define countynhg_lbl 2100650 `"2100650"', add
label define countynhg_lbl 2100670 `"2100670"', add
label define countynhg_lbl 2100690 `"2100690"', add
label define countynhg_lbl 2100710 `"2100710"', add
label define countynhg_lbl 2100730 `"2100730"', add
label define countynhg_lbl 2100750 `"2100750"', add
label define countynhg_lbl 2100770 `"2100770"', add
label define countynhg_lbl 2100790 `"2100790"', add
label define countynhg_lbl 2100810 `"2100810"', add
label define countynhg_lbl 2100830 `"2100830"', add
label define countynhg_lbl 2100850 `"2100850"', add
label define countynhg_lbl 2100870 `"2100870"', add
label define countynhg_lbl 2100890 `"2100890"', add
label define countynhg_lbl 2100910 `"2100910"', add
label define countynhg_lbl 2100930 `"2100930"', add
label define countynhg_lbl 2100950 `"2100950"', add
label define countynhg_lbl 2100970 `"2100970"', add
label define countynhg_lbl 2100990 `"2100990"', add
label define countynhg_lbl 2101010 `"2101010"', add
label define countynhg_lbl 2101030 `"2101030"', add
label define countynhg_lbl 2101050 `"2101050"', add
label define countynhg_lbl 2101070 `"2101070"', add
label define countynhg_lbl 2101090 `"2101090"', add
label define countynhg_lbl 2101110 `"2101110"', add
label define countynhg_lbl 2101130 `"2101130"', add
label define countynhg_lbl 2101150 `"2101150"', add
label define countynhg_lbl 2101155 `"2101155"', add
label define countynhg_lbl 2101170 `"2101170"', add
label define countynhg_lbl 2101190 `"2101190"', add
label define countynhg_lbl 2101210 `"2101210"', add
label define countynhg_lbl 2101230 `"2101230"', add
label define countynhg_lbl 2101250 `"2101250"', add
label define countynhg_lbl 2101270 `"2101270"', add
label define countynhg_lbl 2101290 `"2101290"', add
label define countynhg_lbl 2101310 `"2101310"', add
label define countynhg_lbl 2101330 `"2101330"', add
label define countynhg_lbl 2101350 `"2101350"', add
label define countynhg_lbl 2101370 `"2101370"', add
label define countynhg_lbl 2101390 `"2101390"', add
label define countynhg_lbl 2101410 `"2101410"', add
label define countynhg_lbl 2101430 `"2101430"', add
label define countynhg_lbl 2101450 `"2101450"', add
label define countynhg_lbl 2101470 `"2101470"', add
label define countynhg_lbl 2101490 `"2101490"', add
label define countynhg_lbl 2101510 `"2101510"', add
label define countynhg_lbl 2101530 `"2101530"', add
label define countynhg_lbl 2101550 `"2101550"', add
label define countynhg_lbl 2101570 `"2101570"', add
label define countynhg_lbl 2101590 `"2101590"', add
label define countynhg_lbl 2101610 `"2101610"', add
label define countynhg_lbl 2101630 `"2101630"', add
label define countynhg_lbl 2101650 `"2101650"', add
label define countynhg_lbl 2101670 `"2101670"', add
label define countynhg_lbl 2101690 `"2101690"', add
label define countynhg_lbl 2101710 `"2101710"', add
label define countynhg_lbl 2101730 `"2101730"', add
label define countynhg_lbl 2101750 `"2101750"', add
label define countynhg_lbl 2101770 `"2101770"', add
label define countynhg_lbl 2101790 `"2101790"', add
label define countynhg_lbl 2101810 `"2101810"', add
label define countynhg_lbl 2101830 `"2101830"', add
label define countynhg_lbl 2101850 `"2101850"', add
label define countynhg_lbl 2101870 `"2101870"', add
label define countynhg_lbl 2101890 `"2101890"', add
label define countynhg_lbl 2101910 `"2101910"', add
label define countynhg_lbl 2101930 `"2101930"', add
label define countynhg_lbl 2101950 `"2101950"', add
label define countynhg_lbl 2101970 `"2101970"', add
label define countynhg_lbl 2101990 `"2101990"', add
label define countynhg_lbl 2102010 `"2102010"', add
label define countynhg_lbl 2102030 `"2102030"', add
label define countynhg_lbl 2102050 `"2102050"', add
label define countynhg_lbl 2102070 `"2102070"', add
label define countynhg_lbl 2102090 `"2102090"', add
label define countynhg_lbl 2102110 `"2102110"', add
label define countynhg_lbl 2102130 `"2102130"', add
label define countynhg_lbl 2102150 `"2102150"', add
label define countynhg_lbl 2102170 `"2102170"', add
label define countynhg_lbl 2102190 `"2102190"', add
label define countynhg_lbl 2102210 `"2102210"', add
label define countynhg_lbl 2102230 `"2102230"', add
label define countynhg_lbl 2102250 `"2102250"', add
label define countynhg_lbl 2102270 `"2102270"', add
label define countynhg_lbl 2102290 `"2102290"', add
label define countynhg_lbl 2102310 `"2102310"', add
label define countynhg_lbl 2102330 `"2102330"', add
label define countynhg_lbl 2102350 `"2102350"', add
label define countynhg_lbl 2102370 `"2102370"', add
label define countynhg_lbl 2102390 `"2102390"', add
label define countynhg_lbl 2200010 `"2200010"', add
label define countynhg_lbl 2200030 `"2200030"', add
label define countynhg_lbl 2200050 `"2200050"', add
label define countynhg_lbl 2200070 `"2200070"', add
label define countynhg_lbl 2200090 `"2200090"', add
label define countynhg_lbl 2200110 `"2200110"', add
label define countynhg_lbl 2200130 `"2200130"', add
label define countynhg_lbl 2200150 `"2200150"', add
label define countynhg_lbl 2200170 `"2200170"', add
label define countynhg_lbl 2200190 `"2200190"', add
label define countynhg_lbl 2200210 `"2200210"', add
label define countynhg_lbl 2200230 `"2200230"', add
label define countynhg_lbl 2200235 `"2200235"', add
label define countynhg_lbl 2200250 `"2200250"', add
label define countynhg_lbl 2200270 `"2200270"', add
label define countynhg_lbl 2200290 `"2200290"', add
label define countynhg_lbl 2200310 `"2200310"', add
label define countynhg_lbl 2200330 `"2200330"', add
label define countynhg_lbl 2200350 `"2200350"', add
label define countynhg_lbl 2200370 `"2200370"', add
label define countynhg_lbl 2200390 `"2200390"', add
label define countynhg_lbl 2200410 `"2200410"', add
label define countynhg_lbl 2200430 `"2200430"', add
label define countynhg_lbl 2200450 `"2200450"', add
label define countynhg_lbl 2200470 `"2200470"', add
label define countynhg_lbl 2200490 `"2200490"', add
label define countynhg_lbl 2200510 `"2200510"', add
label define countynhg_lbl 2200530 `"2200530"', add
label define countynhg_lbl 2200550 `"2200550"', add
label define countynhg_lbl 2200570 `"2200570"', add
label define countynhg_lbl 2200590 `"2200590"', add
label define countynhg_lbl 2200610 `"2200610"', add
label define countynhg_lbl 2200630 `"2200630"', add
label define countynhg_lbl 2200650 `"2200650"', add
label define countynhg_lbl 2200670 `"2200670"', add
label define countynhg_lbl 2200690 `"2200690"', add
label define countynhg_lbl 2200710 `"2200710"', add
label define countynhg_lbl 2200730 `"2200730"', add
label define countynhg_lbl 2200750 `"2200750"', add
label define countynhg_lbl 2200770 `"2200770"', add
label define countynhg_lbl 2200790 `"2200790"', add
label define countynhg_lbl 2200810 `"2200810"', add
label define countynhg_lbl 2200830 `"2200830"', add
label define countynhg_lbl 2200850 `"2200850"', add
label define countynhg_lbl 2200870 `"2200870"', add
label define countynhg_lbl 2200890 `"2200890"', add
label define countynhg_lbl 2200910 `"2200910"', add
label define countynhg_lbl 2200930 `"2200930"', add
label define countynhg_lbl 2200950 `"2200950"', add
label define countynhg_lbl 2200970 `"2200970"', add
label define countynhg_lbl 2200990 `"2200990"', add
label define countynhg_lbl 2201010 `"2201010"', add
label define countynhg_lbl 2201030 `"2201030"', add
label define countynhg_lbl 2201050 `"2201050"', add
label define countynhg_lbl 2201070 `"2201070"', add
label define countynhg_lbl 2201090 `"2201090"', add
label define countynhg_lbl 2201110 `"2201110"', add
label define countynhg_lbl 2201130 `"2201130"', add
label define countynhg_lbl 2201150 `"2201150"', add
label define countynhg_lbl 2201170 `"2201170"', add
label define countynhg_lbl 2201190 `"2201190"', add
label define countynhg_lbl 2201210 `"2201210"', add
label define countynhg_lbl 2201230 `"2201230"', add
label define countynhg_lbl 2201250 `"2201250"', add
label define countynhg_lbl 2201270 `"2201270"', add
label define countynhg_lbl 2300010 `"2300010"', add
label define countynhg_lbl 2300030 `"2300030"', add
label define countynhg_lbl 2300050 `"2300050"', add
label define countynhg_lbl 2300070 `"2300070"', add
label define countynhg_lbl 2300090 `"2300090"', add
label define countynhg_lbl 2300110 `"2300110"', add
label define countynhg_lbl 2300130 `"2300130"', add
label define countynhg_lbl 2300150 `"2300150"', add
label define countynhg_lbl 2300170 `"2300170"', add
label define countynhg_lbl 2300190 `"2300190"', add
label define countynhg_lbl 2300210 `"2300210"', add
label define countynhg_lbl 2300230 `"2300230"', add
label define countynhg_lbl 2300250 `"2300250"', add
label define countynhg_lbl 2300270 `"2300270"', add
label define countynhg_lbl 2300290 `"2300290"', add
label define countynhg_lbl 2300310 `"2300310"', add
label define countynhg_lbl 2400010 `"2400010"', add
label define countynhg_lbl 2400030 `"2400030"', add
label define countynhg_lbl 2400050 `"2400050"', add
label define countynhg_lbl 2400090 `"2400090"', add
label define countynhg_lbl 2400110 `"2400110"', add
label define countynhg_lbl 2400130 `"2400130"', add
label define countynhg_lbl 2400150 `"2400150"', add
label define countynhg_lbl 2400170 `"2400170"', add
label define countynhg_lbl 2400190 `"2400190"', add
label define countynhg_lbl 2400210 `"2400210"', add
label define countynhg_lbl 2400230 `"2400230"', add
label define countynhg_lbl 2400250 `"2400250"', add
label define countynhg_lbl 2400270 `"2400270"', add
label define countynhg_lbl 2400290 `"2400290"', add
label define countynhg_lbl 2400310 `"2400310"', add
label define countynhg_lbl 2400330 `"2400330"', add
label define countynhg_lbl 2400350 `"2400350"', add
label define countynhg_lbl 2400370 `"2400370"', add
label define countynhg_lbl 2400390 `"2400390"', add
label define countynhg_lbl 2400410 `"2400410"', add
label define countynhg_lbl 2400430 `"2400430"', add
label define countynhg_lbl 2400450 `"2400450"', add
label define countynhg_lbl 2400470 `"2400470"', add
label define countynhg_lbl 2405100 `"2405100"', add
label define countynhg_lbl 2500010 `"2500010"', add
label define countynhg_lbl 2500030 `"2500030"', add
label define countynhg_lbl 2500050 `"2500050"', add
label define countynhg_lbl 2500070 `"2500070"', add
label define countynhg_lbl 2500090 `"2500090"', add
label define countynhg_lbl 2500110 `"2500110"', add
label define countynhg_lbl 2500130 `"2500130"', add
label define countynhg_lbl 2500150 `"2500150"', add
label define countynhg_lbl 2500170 `"2500170"', add
label define countynhg_lbl 2500190 `"2500190"', add
label define countynhg_lbl 2500210 `"2500210"', add
label define countynhg_lbl 2500230 `"2500230"', add
label define countynhg_lbl 2500250 `"2500250"', add
label define countynhg_lbl 2500270 `"2500270"', add
label define countynhg_lbl 2600010 `"2600010"', add
label define countynhg_lbl 2600030 `"2600030"', add
label define countynhg_lbl 2600050 `"2600050"', add
label define countynhg_lbl 2600070 `"2600070"', add
label define countynhg_lbl 2600090 `"2600090"', add
label define countynhg_lbl 2600110 `"2600110"', add
label define countynhg_lbl 2600130 `"2600130"', add
label define countynhg_lbl 2600150 `"2600150"', add
label define countynhg_lbl 2600170 `"2600170"', add
label define countynhg_lbl 2600190 `"2600190"', add
label define countynhg_lbl 2600210 `"2600210"', add
label define countynhg_lbl 2600230 `"2600230"', add
label define countynhg_lbl 2600250 `"2600250"', add
label define countynhg_lbl 2600270 `"2600270"', add
label define countynhg_lbl 2600290 `"2600290"', add
label define countynhg_lbl 2600310 `"2600310"', add
label define countynhg_lbl 2600330 `"2600330"', add
label define countynhg_lbl 2600350 `"2600350"', add
label define countynhg_lbl 2600370 `"2600370"', add
label define countynhg_lbl 2600390 `"2600390"', add
label define countynhg_lbl 2600410 `"2600410"', add
label define countynhg_lbl 2600430 `"2600430"', add
label define countynhg_lbl 2600450 `"2600450"', add
label define countynhg_lbl 2600470 `"2600470"', add
label define countynhg_lbl 2600490 `"2600490"', add
label define countynhg_lbl 2600510 `"2600510"', add
label define countynhg_lbl 2600530 `"2600530"', add
label define countynhg_lbl 2600550 `"2600550"', add
label define countynhg_lbl 2600570 `"2600570"', add
label define countynhg_lbl 2600590 `"2600590"', add
label define countynhg_lbl 2600610 `"2600610"', add
label define countynhg_lbl 2600630 `"2600630"', add
label define countynhg_lbl 2600650 `"2600650"', add
label define countynhg_lbl 2600670 `"2600670"', add
label define countynhg_lbl 2600690 `"2600690"', add
label define countynhg_lbl 2600710 `"2600710"', add
label define countynhg_lbl 2600730 `"2600730"', add
label define countynhg_lbl 2600735 `"2600735"', add
label define countynhg_lbl 2600750 `"2600750"', add
label define countynhg_lbl 2600770 `"2600770"', add
label define countynhg_lbl 2600790 `"2600790"', add
label define countynhg_lbl 2600810 `"2600810"', add
label define countynhg_lbl 2600830 `"2600830"', add
label define countynhg_lbl 2600850 `"2600850"', add
label define countynhg_lbl 2600870 `"2600870"', add
label define countynhg_lbl 2600890 `"2600890"', add
label define countynhg_lbl 2600910 `"2600910"', add
label define countynhg_lbl 2600930 `"2600930"', add
label define countynhg_lbl 2600950 `"2600950"', add
label define countynhg_lbl 2600970 `"2600970"', add
label define countynhg_lbl 2600990 `"2600990"', add
label define countynhg_lbl 2601010 `"2601010"', add
label define countynhg_lbl 2601015 `"2601015"', add
label define countynhg_lbl 2601030 `"2601030"', add
label define countynhg_lbl 2601050 `"2601050"', add
label define countynhg_lbl 2601070 `"2601070"', add
label define countynhg_lbl 2601090 `"2601090"', add
label define countynhg_lbl 2601095 `"2601095"', add
label define countynhg_lbl 2601110 `"2601110"', add
label define countynhg_lbl 2601130 `"2601130"', add
label define countynhg_lbl 2601150 `"2601150"', add
label define countynhg_lbl 2601170 `"2601170"', add
label define countynhg_lbl 2601190 `"2601190"', add
label define countynhg_lbl 2601210 `"2601210"', add
label define countynhg_lbl 2601230 `"2601230"', add
label define countynhg_lbl 2601250 `"2601250"', add
label define countynhg_lbl 2601270 `"2601270"', add
label define countynhg_lbl 2601290 `"2601290"', add
label define countynhg_lbl 2601310 `"2601310"', add
label define countynhg_lbl 2601330 `"2601330"', add
label define countynhg_lbl 2601350 `"2601350"', add
label define countynhg_lbl 2601370 `"2601370"', add
label define countynhg_lbl 2601390 `"2601390"', add
label define countynhg_lbl 2601410 `"2601410"', add
label define countynhg_lbl 2601430 `"2601430"', add
label define countynhg_lbl 2601450 `"2601450"', add
label define countynhg_lbl 2601470 `"2601470"', add
label define countynhg_lbl 2601490 `"2601490"', add
label define countynhg_lbl 2601510 `"2601510"', add
label define countynhg_lbl 2601530 `"2601530"', add
label define countynhg_lbl 2601550 `"2601550"', add
label define countynhg_lbl 2601570 `"2601570"', add
label define countynhg_lbl 2601590 `"2601590"', add
label define countynhg_lbl 2601610 `"2601610"', add
label define countynhg_lbl 2601630 `"2601630"', add
label define countynhg_lbl 2601650 `"2601650"', add
label define countynhg_lbl 2700010 `"2700010"', add
label define countynhg_lbl 2700030 `"2700030"', add
label define countynhg_lbl 2700050 `"2700050"', add
label define countynhg_lbl 2700070 `"2700070"', add
label define countynhg_lbl 2700090 `"2700090"', add
label define countynhg_lbl 2700110 `"2700110"', add
label define countynhg_lbl 2700130 `"2700130"', add
label define countynhg_lbl 2700135 `"2700135"', add
label define countynhg_lbl 2700150 `"2700150"', add
label define countynhg_lbl 2700155 `"2700155"', add
label define countynhg_lbl 2700170 `"2700170"', add
label define countynhg_lbl 2700190 `"2700190"', add
label define countynhg_lbl 2700210 `"2700210"', add
label define countynhg_lbl 2700230 `"2700230"', add
label define countynhg_lbl 2700250 `"2700250"', add
label define countynhg_lbl 2700270 `"2700270"', add
label define countynhg_lbl 2700290 `"2700290"', add
label define countynhg_lbl 2700310 `"2700310"', add
label define countynhg_lbl 2700330 `"2700330"', add
label define countynhg_lbl 2700350 `"2700350"', add
label define countynhg_lbl 2700370 `"2700370"', add
label define countynhg_lbl 2700390 `"2700390"', add
label define countynhg_lbl 2700410 `"2700410"', add
label define countynhg_lbl 2700430 `"2700430"', add
label define countynhg_lbl 2700450 `"2700450"', add
label define countynhg_lbl 2700470 `"2700470"', add
label define countynhg_lbl 2700490 `"2700490"', add
label define countynhg_lbl 2700510 `"2700510"', add
label define countynhg_lbl 2700530 `"2700530"', add
label define countynhg_lbl 2700550 `"2700550"', add
label define countynhg_lbl 2700570 `"2700570"', add
label define countynhg_lbl 2700590 `"2700590"', add
label define countynhg_lbl 2700610 `"2700610"', add
label define countynhg_lbl 2700630 `"2700630"', add
label define countynhg_lbl 2700650 `"2700650"', add
label define countynhg_lbl 2700670 `"2700670"', add
label define countynhg_lbl 2700690 `"2700690"', add
label define countynhg_lbl 2700710 `"2700710"', add
label define countynhg_lbl 2700730 `"2700730"', add
label define countynhg_lbl 2700735 `"2700735"', add
label define countynhg_lbl 2700750 `"2700750"', add
label define countynhg_lbl 2700770 `"2700770"', add
label define countynhg_lbl 2700790 `"2700790"', add
label define countynhg_lbl 2700810 `"2700810"', add
label define countynhg_lbl 2700830 `"2700830"', add
label define countynhg_lbl 2700850 `"2700850"', add
label define countynhg_lbl 2700870 `"2700870"', add
label define countynhg_lbl 2700877 `"2700877"', add
label define countynhg_lbl 2700890 `"2700890"', add
label define countynhg_lbl 2700910 `"2700910"', add
label define countynhg_lbl 2700930 `"2700930"', add
label define countynhg_lbl 2700950 `"2700950"', add
label define countynhg_lbl 2700955 `"2700955"', add
label define countynhg_lbl 2700970 `"2700970"', add
label define countynhg_lbl 2700990 `"2700990"', add
label define countynhg_lbl 2701010 `"2701010"', add
label define countynhg_lbl 2701030 `"2701030"', add
label define countynhg_lbl 2701050 `"2701050"', add
label define countynhg_lbl 2701070 `"2701070"', add
label define countynhg_lbl 2701090 `"2701090"', add
label define countynhg_lbl 2701110 `"2701110"', add
label define countynhg_lbl 2701115 `"2701115"', add
label define countynhg_lbl 2701130 `"2701130"', add
label define countynhg_lbl 2701135 `"2701135"', add
label define countynhg_lbl 2701150 `"2701150"', add
label define countynhg_lbl 2701170 `"2701170"', add
label define countynhg_lbl 2701175 `"2701175"', add
label define countynhg_lbl 2701190 `"2701190"', add
label define countynhg_lbl 2701210 `"2701210"', add
label define countynhg_lbl 2701230 `"2701230"', add
label define countynhg_lbl 2701250 `"2701250"', add
label define countynhg_lbl 2701270 `"2701270"', add
label define countynhg_lbl 2701290 `"2701290"', add
label define countynhg_lbl 2701310 `"2701310"', add
label define countynhg_lbl 2701330 `"2701330"', add
label define countynhg_lbl 2701350 `"2701350"', add
label define countynhg_lbl 2701370 `"2701370"', add
label define countynhg_lbl 2701390 `"2701390"', add
label define countynhg_lbl 2701410 `"2701410"', add
label define countynhg_lbl 2701430 `"2701430"', add
label define countynhg_lbl 2701450 `"2701450"', add
label define countynhg_lbl 2701470 `"2701470"', add
label define countynhg_lbl 2701490 `"2701490"', add
label define countynhg_lbl 2701510 `"2701510"', add
label define countynhg_lbl 2701530 `"2701530"', add
label define countynhg_lbl 2701535 `"2701535"', add
label define countynhg_lbl 2701550 `"2701550"', add
label define countynhg_lbl 2701570 `"2701570"', add
label define countynhg_lbl 2701590 `"2701590"', add
label define countynhg_lbl 2701610 `"2701610"', add
label define countynhg_lbl 2701630 `"2701630"', add
label define countynhg_lbl 2701650 `"2701650"', add
label define countynhg_lbl 2701670 `"2701670"', add
label define countynhg_lbl 2701690 `"2701690"', add
label define countynhg_lbl 2701710 `"2701710"', add
label define countynhg_lbl 2701730 `"2701730"', add
label define countynhg_lbl 2750015 `"2750015"', add
label define countynhg_lbl 2750035 `"2750035"', add
label define countynhg_lbl 2750055 `"2750055"', add
label define countynhg_lbl 2750075 `"2750075"', add
label define countynhg_lbl 2750095 `"2750095"', add
label define countynhg_lbl 2750115 `"2750115"', add
label define countynhg_lbl 2750135 `"2750135"', add
label define countynhg_lbl 2750155 `"2750155"', add
label define countynhg_lbl 2750175 `"2750175"', add
label define countynhg_lbl 2800010 `"2800010"', add
label define countynhg_lbl 2800030 `"2800030"', add
label define countynhg_lbl 2800050 `"2800050"', add
label define countynhg_lbl 2800070 `"2800070"', add
label define countynhg_lbl 2800090 `"2800090"', add
label define countynhg_lbl 2800110 `"2800110"', add
label define countynhg_lbl 2800130 `"2800130"', add
label define countynhg_lbl 2800150 `"2800150"', add
label define countynhg_lbl 2800170 `"2800170"', add
label define countynhg_lbl 2800190 `"2800190"', add
label define countynhg_lbl 2800210 `"2800210"', add
label define countynhg_lbl 2800230 `"2800230"', add
label define countynhg_lbl 2800250 `"2800250"', add
label define countynhg_lbl 2800270 `"2800270"', add
label define countynhg_lbl 2800290 `"2800290"', add
label define countynhg_lbl 2800310 `"2800310"', add
label define countynhg_lbl 2800330 `"2800330"', add
label define countynhg_lbl 2800350 `"2800350"', add
label define countynhg_lbl 2800370 `"2800370"', add
label define countynhg_lbl 2800390 `"2800390"', add
label define countynhg_lbl 2800410 `"2800410"', add
label define countynhg_lbl 2800430 `"2800430"', add
label define countynhg_lbl 2800450 `"2800450"', add
label define countynhg_lbl 2800470 `"2800470"', add
label define countynhg_lbl 2800490 `"2800490"', add
label define countynhg_lbl 2800510 `"2800510"', add
label define countynhg_lbl 2800530 `"2800530"', add
label define countynhg_lbl 2800550 `"2800550"', add
label define countynhg_lbl 2800570 `"2800570"', add
label define countynhg_lbl 2800590 `"2800590"', add
label define countynhg_lbl 2800610 `"2800610"', add
label define countynhg_lbl 2800630 `"2800630"', add
label define countynhg_lbl 2800650 `"2800650"', add
label define countynhg_lbl 2800670 `"2800670"', add
label define countynhg_lbl 2800690 `"2800690"', add
label define countynhg_lbl 2800710 `"2800710"', add
label define countynhg_lbl 2800730 `"2800730"', add
label define countynhg_lbl 2800750 `"2800750"', add
label define countynhg_lbl 2800770 `"2800770"', add
label define countynhg_lbl 2800790 `"2800790"', add
label define countynhg_lbl 2800810 `"2800810"', add
label define countynhg_lbl 2800830 `"2800830"', add
label define countynhg_lbl 2800850 `"2800850"', add
label define countynhg_lbl 2800870 `"2800870"', add
label define countynhg_lbl 2800890 `"2800890"', add
label define countynhg_lbl 2800910 `"2800910"', add
label define countynhg_lbl 2800930 `"2800930"', add
label define countynhg_lbl 2800950 `"2800950"', add
label define countynhg_lbl 2800970 `"2800970"', add
label define countynhg_lbl 2800990 `"2800990"', add
label define countynhg_lbl 2801010 `"2801010"', add
label define countynhg_lbl 2801030 `"2801030"', add
label define countynhg_lbl 2801050 `"2801050"', add
label define countynhg_lbl 2801070 `"2801070"', add
label define countynhg_lbl 2801090 `"2801090"', add
label define countynhg_lbl 2801110 `"2801110"', add
label define countynhg_lbl 2801130 `"2801130"', add
label define countynhg_lbl 2801150 `"2801150"', add
label define countynhg_lbl 2801170 `"2801170"', add
label define countynhg_lbl 2801190 `"2801190"', add
label define countynhg_lbl 2801210 `"2801210"', add
label define countynhg_lbl 2801230 `"2801230"', add
label define countynhg_lbl 2801250 `"2801250"', add
label define countynhg_lbl 2801270 `"2801270"', add
label define countynhg_lbl 2801290 `"2801290"', add
label define countynhg_lbl 2801310 `"2801310"', add
label define countynhg_lbl 2801315 `"2801315"', add
label define countynhg_lbl 2801330 `"2801330"', add
label define countynhg_lbl 2801350 `"2801350"', add
label define countynhg_lbl 2801370 `"2801370"', add
label define countynhg_lbl 2801390 `"2801390"', add
label define countynhg_lbl 2801410 `"2801410"', add
label define countynhg_lbl 2801430 `"2801430"', add
label define countynhg_lbl 2801450 `"2801450"', add
label define countynhg_lbl 2801470 `"2801470"', add
label define countynhg_lbl 2801490 `"2801490"', add
label define countynhg_lbl 2801510 `"2801510"', add
label define countynhg_lbl 2801530 `"2801530"', add
label define countynhg_lbl 2801550 `"2801550"', add
label define countynhg_lbl 2801570 `"2801570"', add
label define countynhg_lbl 2801590 `"2801590"', add
label define countynhg_lbl 2801610 `"2801610"', add
label define countynhg_lbl 2801630 `"2801630"', add
label define countynhg_lbl 2900010 `"2900010"', add
label define countynhg_lbl 2900030 `"2900030"', add
label define countynhg_lbl 2900050 `"2900050"', add
label define countynhg_lbl 2900070 `"2900070"', add
label define countynhg_lbl 2900090 `"2900090"', add
label define countynhg_lbl 2900110 `"2900110"', add
label define countynhg_lbl 2900130 `"2900130"', add
label define countynhg_lbl 2900150 `"2900150"', add
label define countynhg_lbl 2900170 `"2900170"', add
label define countynhg_lbl 2900190 `"2900190"', add
label define countynhg_lbl 2900210 `"2900210"', add
label define countynhg_lbl 2900230 `"2900230"', add
label define countynhg_lbl 2900250 `"2900250"', add
label define countynhg_lbl 2900270 `"2900270"', add
label define countynhg_lbl 2900290 `"2900290"', add
label define countynhg_lbl 2900310 `"2900310"', add
label define countynhg_lbl 2900330 `"2900330"', add
label define countynhg_lbl 2900350 `"2900350"', add
label define countynhg_lbl 2900370 `"2900370"', add
label define countynhg_lbl 2900390 `"2900390"', add
label define countynhg_lbl 2900410 `"2900410"', add
label define countynhg_lbl 2900430 `"2900430"', add
label define countynhg_lbl 2900450 `"2900450"', add
label define countynhg_lbl 2900470 `"2900470"', add
label define countynhg_lbl 2900490 `"2900490"', add
label define countynhg_lbl 2900510 `"2900510"', add
label define countynhg_lbl 2900530 `"2900530"', add
label define countynhg_lbl 2900550 `"2900550"', add
label define countynhg_lbl 2900570 `"2900570"', add
label define countynhg_lbl 2900590 `"2900590"', add
label define countynhg_lbl 2900610 `"2900610"', add
label define countynhg_lbl 2900630 `"2900630"', add
label define countynhg_lbl 2900650 `"2900650"', add
label define countynhg_lbl 2900655 `"2900655"', add
label define countynhg_lbl 2900670 `"2900670"', add
label define countynhg_lbl 2900690 `"2900690"', add
label define countynhg_lbl 2900710 `"2900710"', add
label define countynhg_lbl 2900730 `"2900730"', add
label define countynhg_lbl 2900750 `"2900750"', add
label define countynhg_lbl 2900770 `"2900770"', add
label define countynhg_lbl 2900790 `"2900790"', add
label define countynhg_lbl 2900810 `"2900810"', add
label define countynhg_lbl 2900830 `"2900830"', add
label define countynhg_lbl 2900850 `"2900850"', add
label define countynhg_lbl 2900870 `"2900870"', add
label define countynhg_lbl 2900890 `"2900890"', add
label define countynhg_lbl 2900910 `"2900910"', add
label define countynhg_lbl 2900930 `"2900930"', add
label define countynhg_lbl 2900950 `"2900950"', add
label define countynhg_lbl 2900970 `"2900970"', add
label define countynhg_lbl 2900990 `"2900990"', add
label define countynhg_lbl 2901010 `"2901010"', add
label define countynhg_lbl 2901030 `"2901030"', add
label define countynhg_lbl 2901050 `"2901050"', add
label define countynhg_lbl 2901070 `"2901070"', add
label define countynhg_lbl 2901090 `"2901090"', add
label define countynhg_lbl 2901110 `"2901110"', add
label define countynhg_lbl 2901130 `"2901130"', add
label define countynhg_lbl 2901150 `"2901150"', add
label define countynhg_lbl 2901170 `"2901170"', add
label define countynhg_lbl 2901190 `"2901190"', add
label define countynhg_lbl 2901210 `"2901210"', add
label define countynhg_lbl 2901230 `"2901230"', add
label define countynhg_lbl 2901250 `"2901250"', add
label define countynhg_lbl 2901270 `"2901270"', add
label define countynhg_lbl 2901290 `"2901290"', add
label define countynhg_lbl 2901310 `"2901310"', add
label define countynhg_lbl 2901330 `"2901330"', add
label define countynhg_lbl 2901350 `"2901350"', add
label define countynhg_lbl 2901370 `"2901370"', add
label define countynhg_lbl 2901390 `"2901390"', add
label define countynhg_lbl 2901410 `"2901410"', add
label define countynhg_lbl 2901430 `"2901430"', add
label define countynhg_lbl 2901450 `"2901450"', add
label define countynhg_lbl 2901470 `"2901470"', add
label define countynhg_lbl 2901490 `"2901490"', add
label define countynhg_lbl 2901510 `"2901510"', add
label define countynhg_lbl 2901530 `"2901530"', add
label define countynhg_lbl 2901550 `"2901550"', add
label define countynhg_lbl 2901570 `"2901570"', add
label define countynhg_lbl 2901590 `"2901590"', add
label define countynhg_lbl 2901610 `"2901610"', add
label define countynhg_lbl 2901630 `"2901630"', add
label define countynhg_lbl 2901650 `"2901650"', add
label define countynhg_lbl 2901670 `"2901670"', add
label define countynhg_lbl 2901690 `"2901690"', add
label define countynhg_lbl 2901710 `"2901710"', add
label define countynhg_lbl 2901730 `"2901730"', add
label define countynhg_lbl 2901750 `"2901750"', add
label define countynhg_lbl 2901770 `"2901770"', add
label define countynhg_lbl 2901790 `"2901790"', add
label define countynhg_lbl 2901810 `"2901810"', add
label define countynhg_lbl 2901830 `"2901830"', add
label define countynhg_lbl 2901850 `"2901850"', add
label define countynhg_lbl 2901860 `"2901860"', add
label define countynhg_lbl 2901870 `"2901870"', add
label define countynhg_lbl 2901890 `"2901890"', add
label define countynhg_lbl 2901950 `"2901950"', add
label define countynhg_lbl 2901970 `"2901970"', add
label define countynhg_lbl 2901990 `"2901990"', add
label define countynhg_lbl 2902010 `"2902010"', add
label define countynhg_lbl 2902030 `"2902030"', add
label define countynhg_lbl 2902050 `"2902050"', add
label define countynhg_lbl 2902070 `"2902070"', add
label define countynhg_lbl 2902090 `"2902090"', add
label define countynhg_lbl 2902110 `"2902110"', add
label define countynhg_lbl 2902130 `"2902130"', add
label define countynhg_lbl 2902150 `"2902150"', add
label define countynhg_lbl 2902170 `"2902170"', add
label define countynhg_lbl 2902190 `"2902190"', add
label define countynhg_lbl 2902210 `"2902210"', add
label define countynhg_lbl 2902230 `"2902230"', add
label define countynhg_lbl 2902250 `"2902250"', add
label define countynhg_lbl 2902270 `"2902270"', add
label define countynhg_lbl 2902290 `"2902290"', add
label define countynhg_lbl 2905100 `"2905100"', add
label define countynhg_lbl 3000010 `"3000010"', add
label define countynhg_lbl 3000030 `"3000030"', add
label define countynhg_lbl 3000050 `"3000050"', add
label define countynhg_lbl 3000070 `"3000070"', add
label define countynhg_lbl 3000090 `"3000090"', add
label define countynhg_lbl 3000110 `"3000110"', add
label define countynhg_lbl 3000130 `"3000130"', add
label define countynhg_lbl 3000150 `"3000150"', add
label define countynhg_lbl 3000170 `"3000170"', add
label define countynhg_lbl 3000190 `"3000190"', add
label define countynhg_lbl 3000210 `"3000210"', add
label define countynhg_lbl 3000230 `"3000230"', add
label define countynhg_lbl 3000250 `"3000250"', add
label define countynhg_lbl 3000270 `"3000270"', add
label define countynhg_lbl 3000290 `"3000290"', add
label define countynhg_lbl 3000310 `"3000310"', add
label define countynhg_lbl 3000330 `"3000330"', add
label define countynhg_lbl 3000350 `"3000350"', add
label define countynhg_lbl 3000370 `"3000370"', add
label define countynhg_lbl 3000390 `"3000390"', add
label define countynhg_lbl 3000410 `"3000410"', add
label define countynhg_lbl 3000430 `"3000430"', add
label define countynhg_lbl 3000450 `"3000450"', add
label define countynhg_lbl 3000470 `"3000470"', add
label define countynhg_lbl 3000490 `"3000490"', add
label define countynhg_lbl 3000510 `"3000510"', add
label define countynhg_lbl 3000530 `"3000530"', add
label define countynhg_lbl 3000550 `"3000550"', add
label define countynhg_lbl 3000570 `"3000570"', add
label define countynhg_lbl 3000590 `"3000590"', add
label define countynhg_lbl 3000610 `"3000610"', add
label define countynhg_lbl 3000630 `"3000630"', add
label define countynhg_lbl 3000650 `"3000650"', add
label define countynhg_lbl 3000670 `"3000670"', add
label define countynhg_lbl 3000690 `"3000690"', add
label define countynhg_lbl 3000710 `"3000710"', add
label define countynhg_lbl 3000730 `"3000730"', add
label define countynhg_lbl 3000750 `"3000750"', add
label define countynhg_lbl 3000770 `"3000770"', add
label define countynhg_lbl 3000790 `"3000790"', add
label define countynhg_lbl 3000810 `"3000810"', add
label define countynhg_lbl 3000830 `"3000830"', add
label define countynhg_lbl 3000850 `"3000850"', add
label define countynhg_lbl 3000870 `"3000870"', add
label define countynhg_lbl 3000890 `"3000890"', add
label define countynhg_lbl 3000910 `"3000910"', add
label define countynhg_lbl 3000930 `"3000930"', add
label define countynhg_lbl 3000950 `"3000950"', add
label define countynhg_lbl 3000970 `"3000970"', add
label define countynhg_lbl 3000990 `"3000990"', add
label define countynhg_lbl 3001010 `"3001010"', add
label define countynhg_lbl 3001030 `"3001030"', add
label define countynhg_lbl 3001050 `"3001050"', add
label define countynhg_lbl 3001070 `"3001070"', add
label define countynhg_lbl 3001090 `"3001090"', add
label define countynhg_lbl 3001110 `"3001110"', add
label define countynhg_lbl 3001130 `"3001130"', add
label define countynhg_lbl 3050015 `"3050015"', add
label define countynhg_lbl 3050035 `"3050035"', add
label define countynhg_lbl 3050055 `"3050055"', add
label define countynhg_lbl 3050075 `"3050075"', add
label define countynhg_lbl 3050095 `"3050095"', add
label define countynhg_lbl 3050115 `"3050115"', add
label define countynhg_lbl 3050135 `"3050135"', add
label define countynhg_lbl 3050155 `"3050155"', add
label define countynhg_lbl 3050175 `"3050175"', add
label define countynhg_lbl 3050195 `"3050195"', add
label define countynhg_lbl 3050215 `"3050215"', add
label define countynhg_lbl 3050235 `"3050235"', add
label define countynhg_lbl 3100010 `"3100010"', add
label define countynhg_lbl 3100030 `"3100030"', add
label define countynhg_lbl 3100050 `"3100050"', add
label define countynhg_lbl 3100070 `"3100070"', add
label define countynhg_lbl 3100075 `"3100075"', add
label define countynhg_lbl 3100090 `"3100090"', add
label define countynhg_lbl 3100110 `"3100110"', add
label define countynhg_lbl 3100130 `"3100130"', add
label define countynhg_lbl 3100150 `"3100150"', add
label define countynhg_lbl 3100170 `"3100170"', add
label define countynhg_lbl 3100190 `"3100190"', add
label define countynhg_lbl 3100210 `"3100210"', add
label define countynhg_lbl 3100230 `"3100230"', add
label define countynhg_lbl 3100250 `"3100250"', add
label define countynhg_lbl 3100270 `"3100270"', add
label define countynhg_lbl 3100290 `"3100290"', add
label define countynhg_lbl 3100310 `"3100310"', add
label define countynhg_lbl 3100330 `"3100330"', add
label define countynhg_lbl 3100350 `"3100350"', add
label define countynhg_lbl 3100370 `"3100370"', add
label define countynhg_lbl 3100390 `"3100390"', add
label define countynhg_lbl 3100410 `"3100410"', add
label define countynhg_lbl 3100430 `"3100430"', add
label define countynhg_lbl 3100450 `"3100450"', add
label define countynhg_lbl 3100470 `"3100470"', add
label define countynhg_lbl 3100490 `"3100490"', add
label define countynhg_lbl 3100510 `"3100510"', add
label define countynhg_lbl 3100530 `"3100530"', add
label define countynhg_lbl 3100550 `"3100550"', add
label define countynhg_lbl 3100570 `"3100570"', add
label define countynhg_lbl 3100590 `"3100590"', add
label define countynhg_lbl 3100610 `"3100610"', add
label define countynhg_lbl 3100630 `"3100630"', add
label define countynhg_lbl 3100650 `"3100650"', add
label define countynhg_lbl 3100670 `"3100670"', add
label define countynhg_lbl 3100690 `"3100690"', add
label define countynhg_lbl 3100710 `"3100710"', add
label define countynhg_lbl 3100730 `"3100730"', add
label define countynhg_lbl 3100750 `"3100750"', add
label define countynhg_lbl 3100770 `"3100770"', add
label define countynhg_lbl 3100790 `"3100790"', add
label define countynhg_lbl 3100810 `"3100810"', add
label define countynhg_lbl 3100830 `"3100830"', add
label define countynhg_lbl 3100835 `"3100835"', add
label define countynhg_lbl 3100850 `"3100850"', add
label define countynhg_lbl 3100870 `"3100870"', add
label define countynhg_lbl 3100890 `"3100890"', add
label define countynhg_lbl 3100910 `"3100910"', add
label define countynhg_lbl 3100930 `"3100930"', add
label define countynhg_lbl 3100935 `"3100935"', add
label define countynhg_lbl 3100950 `"3100950"', add
label define countynhg_lbl 3100970 `"3100970"', add
label define countynhg_lbl 3100990 `"3100990"', add
label define countynhg_lbl 3101010 `"3101010"', add
label define countynhg_lbl 3101030 `"3101030"', add
label define countynhg_lbl 3101050 `"3101050"', add
label define countynhg_lbl 3101070 `"3101070"', add
label define countynhg_lbl 3101090 `"3101090"', add
label define countynhg_lbl 3101095 `"3101095"', add
label define countynhg_lbl 3101110 `"3101110"', add
label define countynhg_lbl 3101130 `"3101130"', add
label define countynhg_lbl 3101150 `"3101150"', add
label define countynhg_lbl 3101170 `"3101170"', add
label define countynhg_lbl 3101190 `"3101190"', add
label define countynhg_lbl 3101210 `"3101210"', add
label define countynhg_lbl 3101215 `"3101215"', add
label define countynhg_lbl 3101230 `"3101230"', add
label define countynhg_lbl 3101250 `"3101250"', add
label define countynhg_lbl 3101270 `"3101270"', add
label define countynhg_lbl 3101290 `"3101290"', add
label define countynhg_lbl 3101310 `"3101310"', add
label define countynhg_lbl 3101330 `"3101330"', add
label define countynhg_lbl 3101350 `"3101350"', add
label define countynhg_lbl 3101370 `"3101370"', add
label define countynhg_lbl 3101390 `"3101390"', add
label define countynhg_lbl 3101410 `"3101410"', add
label define countynhg_lbl 3101430 `"3101430"', add
label define countynhg_lbl 3101450 `"3101450"', add
label define countynhg_lbl 3101470 `"3101470"', add
label define countynhg_lbl 3101490 `"3101490"', add
label define countynhg_lbl 3101510 `"3101510"', add
label define countynhg_lbl 3101530 `"3101530"', add
label define countynhg_lbl 3101550 `"3101550"', add
label define countynhg_lbl 3101570 `"3101570"', add
label define countynhg_lbl 3101590 `"3101590"', add
label define countynhg_lbl 3101610 `"3101610"', add
label define countynhg_lbl 3101630 `"3101630"', add
label define countynhg_lbl 3101650 `"3101650"', add
label define countynhg_lbl 3101670 `"3101670"', add
label define countynhg_lbl 3101690 `"3101690"', add
label define countynhg_lbl 3101710 `"3101710"', add
label define countynhg_lbl 3101730 `"3101730"', add
label define countynhg_lbl 3101733 `"3101733"', add
label define countynhg_lbl 3101750 `"3101750"', add
label define countynhg_lbl 3101770 `"3101770"', add
label define countynhg_lbl 3101790 `"3101790"', add
label define countynhg_lbl 3101810 `"3101810"', add
label define countynhg_lbl 3101830 `"3101830"', add
label define countynhg_lbl 3101850 `"3101850"', add
label define countynhg_lbl 3109015 `"3109015"', add
label define countynhg_lbl 3109035 `"3109035"', add
label define countynhg_lbl 3150015 `"3150015"', add
label define countynhg_lbl 3150035 `"3150035"', add
label define countynhg_lbl 3150055 `"3150055"', add
label define countynhg_lbl 3150095 `"3150095"', add
label define countynhg_lbl 3150115 `"3150115"', add
label define countynhg_lbl 3150135 `"3150135"', add
label define countynhg_lbl 3150155 `"3150155"', add
label define countynhg_lbl 3150175 `"3150175"', add
label define countynhg_lbl 3150195 `"3150195"', add
label define countynhg_lbl 3150215 `"3150215"', add
label define countynhg_lbl 3150235 `"3150235"', add
label define countynhg_lbl 3150255 `"3150255"', add
label define countynhg_lbl 3150275 `"3150275"', add
label define countynhg_lbl 3150315 `"3150315"', add
label define countynhg_lbl 3150335 `"3150335"', add
label define countynhg_lbl 3150355 `"3150355"', add
label define countynhg_lbl 3150375 `"3150375"', add
label define countynhg_lbl 3150395 `"3150395"', add
label define countynhg_lbl 3150415 `"3150415"', add
label define countynhg_lbl 3150455 `"3150455"', add
label define countynhg_lbl 3150475 `"3150475"', add
label define countynhg_lbl 3150515 `"3150515"', add
label define countynhg_lbl 3150535 `"3150535"', add
label define countynhg_lbl 3150575 `"3150575"', add
label define countynhg_lbl 3150595 `"3150595"', add
label define countynhg_lbl 3150615 `"3150615"', add
label define countynhg_lbl 3150635 `"3150635"', add
label define countynhg_lbl 3150655 `"3150655"', add
label define countynhg_lbl 3150675 `"3150675"', add
label define countynhg_lbl 3150695 `"3150695"', add
label define countynhg_lbl 3150715 `"3150715"', add
label define countynhg_lbl 3200010 `"3200010"', add
label define countynhg_lbl 3200030 `"3200030"', add
label define countynhg_lbl 3200050 `"3200050"', add
label define countynhg_lbl 3200070 `"3200070"', add
label define countynhg_lbl 3200090 `"3200090"', add
label define countynhg_lbl 3200110 `"3200110"', add
label define countynhg_lbl 3200130 `"3200130"', add
label define countynhg_lbl 3200150 `"3200150"', add
label define countynhg_lbl 3200170 `"3200170"', add
label define countynhg_lbl 3200190 `"3200190"', add
label define countynhg_lbl 3200210 `"3200210"', add
label define countynhg_lbl 3200230 `"3200230"', add
label define countynhg_lbl 3200250 `"3200250"', add
label define countynhg_lbl 3200270 `"3200270"', add
label define countynhg_lbl 3200275 `"3200275"', add
label define countynhg_lbl 3200290 `"3200290"', add
label define countynhg_lbl 3200310 `"3200310"', add
label define countynhg_lbl 3200330 `"3200330"', add
label define countynhg_lbl 3300010 `"3300010"', add
label define countynhg_lbl 3300030 `"3300030"', add
label define countynhg_lbl 3300050 `"3300050"', add
label define countynhg_lbl 3300070 `"3300070"', add
label define countynhg_lbl 3300090 `"3300090"', add
label define countynhg_lbl 3300110 `"3300110"', add
label define countynhg_lbl 3300130 `"3300130"', add
label define countynhg_lbl 3300150 `"3300150"', add
label define countynhg_lbl 3300170 `"3300170"', add
label define countynhg_lbl 3300190 `"3300190"', add
label define countynhg_lbl 3400010 `"3400010"', add
label define countynhg_lbl 3400030 `"3400030"', add
label define countynhg_lbl 3400050 `"3400050"', add
label define countynhg_lbl 3400070 `"3400070"', add
label define countynhg_lbl 3400090 `"3400090"', add
label define countynhg_lbl 3400110 `"3400110"', add
label define countynhg_lbl 3400130 `"3400130"', add
label define countynhg_lbl 3400150 `"3400150"', add
label define countynhg_lbl 3400170 `"3400170"', add
label define countynhg_lbl 3400190 `"3400190"', add
label define countynhg_lbl 3400210 `"3400210"', add
label define countynhg_lbl 3400230 `"3400230"', add
label define countynhg_lbl 3400250 `"3400250"', add
label define countynhg_lbl 3400270 `"3400270"', add
label define countynhg_lbl 3400290 `"3400290"', add
label define countynhg_lbl 3400310 `"3400310"', add
label define countynhg_lbl 3400330 `"3400330"', add
label define countynhg_lbl 3400350 `"3400350"', add
label define countynhg_lbl 3400370 `"3400370"', add
label define countynhg_lbl 3400390 `"3400390"', add
label define countynhg_lbl 3400410 `"3400410"', add
label define countynhg_lbl 3500010 `"3500010"', add
label define countynhg_lbl 3500030 `"3500030"', add
label define countynhg_lbl 3500050 `"3500050"', add
label define countynhg_lbl 3500070 `"3500070"', add
label define countynhg_lbl 3500090 `"3500090"', add
label define countynhg_lbl 3500110 `"3500110"', add
label define countynhg_lbl 3500130 `"3500130"', add
label define countynhg_lbl 3500150 `"3500150"', add
label define countynhg_lbl 3500170 `"3500170"', add
label define countynhg_lbl 3500190 `"3500190"', add
label define countynhg_lbl 3500210 `"3500210"', add
label define countynhg_lbl 3500230 `"3500230"', add
label define countynhg_lbl 3500250 `"3500250"', add
label define countynhg_lbl 3500270 `"3500270"', add
label define countynhg_lbl 3500290 `"3500290"', add
label define countynhg_lbl 3500310 `"3500310"', add
label define countynhg_lbl 3500330 `"3500330"', add
label define countynhg_lbl 3500350 `"3500350"', add
label define countynhg_lbl 3500370 `"3500370"', add
label define countynhg_lbl 3500390 `"3500390"', add
label define countynhg_lbl 3500410 `"3500410"', add
label define countynhg_lbl 3500430 `"3500430"', add
label define countynhg_lbl 3500450 `"3500450"', add
label define countynhg_lbl 3500470 `"3500470"', add
label define countynhg_lbl 3500490 `"3500490"', add
label define countynhg_lbl 3500510 `"3500510"', add
label define countynhg_lbl 3500530 `"3500530"', add
label define countynhg_lbl 3500550 `"3500550"', add
label define countynhg_lbl 3500570 `"3500570"', add
label define countynhg_lbl 3500590 `"3500590"', add
label define countynhg_lbl 3500610 `"3500610"', add
label define countynhg_lbl 3550015 `"3550015"', add
label define countynhg_lbl 3550035 `"3550035"', add
label define countynhg_lbl 3550055 `"3550055"', add
label define countynhg_lbl 3550075 `"3550075"', add
label define countynhg_lbl 3550095 `"3550095"', add
label define countynhg_lbl 3550115 `"3550115"', add
label define countynhg_lbl 3550135 `"3550135"', add
label define countynhg_lbl 3550155 `"3550155"', add
label define countynhg_lbl 3550175 `"3550175"', add
label define countynhg_lbl 3550195 `"3550195"', add
label define countynhg_lbl 3550215 `"3550215"', add
label define countynhg_lbl 3550235 `"3550235"', add
label define countynhg_lbl 3550255 `"3550255"', add
label define countynhg_lbl 3550275 `"3550275"', add
label define countynhg_lbl 3550295 `"3550295"', add
label define countynhg_lbl 3550315 `"3550315"', add
label define countynhg_lbl 3550335 `"3550335"', add
label define countynhg_lbl 3550355 `"3550355"', add
label define countynhg_lbl 3550375 `"3550375"', add
label define countynhg_lbl 3550395 `"3550395"', add
label define countynhg_lbl 3550415 `"3550415"', add
label define countynhg_lbl 3550435 `"3550435"', add
label define countynhg_lbl 3550455 `"3550455"', add
label define countynhg_lbl 3550475 `"3550475"', add
label define countynhg_lbl 3550495 `"3550495"', add
label define countynhg_lbl 3550515 `"3550515"', add
label define countynhg_lbl 3550535 `"3550535"', add
label define countynhg_lbl 3550555 `"3550555"', add
label define countynhg_lbl 3559999 `"3559999"', add
label define countynhg_lbl 3600010 `"3600010"', add
label define countynhg_lbl 3600030 `"3600030"', add
label define countynhg_lbl 3600050 `"3600050"', add
label define countynhg_lbl 3600070 `"3600070"', add
label define countynhg_lbl 3600090 `"3600090"', add
label define countynhg_lbl 3600110 `"3600110"', add
label define countynhg_lbl 3600130 `"3600130"', add
label define countynhg_lbl 3600150 `"3600150"', add
label define countynhg_lbl 3600170 `"3600170"', add
label define countynhg_lbl 3600190 `"3600190"', add
label define countynhg_lbl 3600210 `"3600210"', add
label define countynhg_lbl 3600230 `"3600230"', add
label define countynhg_lbl 3600250 `"3600250"', add
label define countynhg_lbl 3600270 `"3600270"', add
label define countynhg_lbl 3600290 `"3600290"', add
label define countynhg_lbl 3600310 `"3600310"', add
label define countynhg_lbl 3600330 `"3600330"', add
label define countynhg_lbl 3600350 `"3600350"', add
label define countynhg_lbl 3600370 `"3600370"', add
label define countynhg_lbl 3600390 `"3600390"', add
label define countynhg_lbl 3600410 `"3600410"', add
label define countynhg_lbl 3600430 `"3600430"', add
label define countynhg_lbl 3600450 `"3600450"', add
label define countynhg_lbl 3600470 `"3600470"', add
label define countynhg_lbl 3600490 `"3600490"', add
label define countynhg_lbl 3600510 `"3600510"', add
label define countynhg_lbl 3600530 `"3600530"', add
label define countynhg_lbl 3600550 `"3600550"', add
label define countynhg_lbl 3600570 `"3600570"', add
label define countynhg_lbl 3600590 `"3600590"', add
label define countynhg_lbl 3600610 `"3600610"', add
label define countynhg_lbl 3600630 `"3600630"', add
label define countynhg_lbl 3600650 `"3600650"', add
label define countynhg_lbl 3600670 `"3600670"', add
label define countynhg_lbl 3600690 `"3600690"', add
label define countynhg_lbl 3600710 `"3600710"', add
label define countynhg_lbl 3600730 `"3600730"', add
label define countynhg_lbl 3600750 `"3600750"', add
label define countynhg_lbl 3600770 `"3600770"', add
label define countynhg_lbl 3600790 `"3600790"', add
label define countynhg_lbl 3600810 `"3600810"', add
label define countynhg_lbl 3600830 `"3600830"', add
label define countynhg_lbl 3600850 `"3600850"', add
label define countynhg_lbl 3600870 `"3600870"', add
label define countynhg_lbl 3600890 `"3600890"', add
label define countynhg_lbl 3600910 `"3600910"', add
label define countynhg_lbl 3600930 `"3600930"', add
label define countynhg_lbl 3600950 `"3600950"', add
label define countynhg_lbl 3600970 `"3600970"', add
label define countynhg_lbl 3600990 `"3600990"', add
label define countynhg_lbl 3601010 `"3601010"', add
label define countynhg_lbl 3601030 `"3601030"', add
label define countynhg_lbl 3601050 `"3601050"', add
label define countynhg_lbl 3601070 `"3601070"', add
label define countynhg_lbl 3601090 `"3601090"', add
label define countynhg_lbl 3601110 `"3601110"', add
label define countynhg_lbl 3601130 `"3601130"', add
label define countynhg_lbl 3601150 `"3601150"', add
label define countynhg_lbl 3601170 `"3601170"', add
label define countynhg_lbl 3601190 `"3601190"', add
label define countynhg_lbl 3601210 `"3601210"', add
label define countynhg_lbl 3601230 `"3601230"', add
label define countynhg_lbl 3700010 `"3700010"', add
label define countynhg_lbl 3700030 `"3700030"', add
label define countynhg_lbl 3700050 `"3700050"', add
label define countynhg_lbl 3700070 `"3700070"', add
label define countynhg_lbl 3700090 `"3700090"', add
label define countynhg_lbl 3700110 `"3700110"', add
label define countynhg_lbl 3700130 `"3700130"', add
label define countynhg_lbl 3700150 `"3700150"', add
label define countynhg_lbl 3700170 `"3700170"', add
label define countynhg_lbl 3700190 `"3700190"', add
label define countynhg_lbl 3700210 `"3700210"', add
label define countynhg_lbl 3700230 `"3700230"', add
label define countynhg_lbl 3700250 `"3700250"', add
label define countynhg_lbl 3700270 `"3700270"', add
label define countynhg_lbl 3700290 `"3700290"', add
label define countynhg_lbl 3700310 `"3700310"', add
label define countynhg_lbl 3700330 `"3700330"', add
label define countynhg_lbl 3700350 `"3700350"', add
label define countynhg_lbl 3700370 `"3700370"', add
label define countynhg_lbl 3700390 `"3700390"', add
label define countynhg_lbl 3700410 `"3700410"', add
label define countynhg_lbl 3700430 `"3700430"', add
label define countynhg_lbl 3700450 `"3700450"', add
label define countynhg_lbl 3700470 `"3700470"', add
label define countynhg_lbl 3700490 `"3700490"', add
label define countynhg_lbl 3700510 `"3700510"', add
label define countynhg_lbl 3700530 `"3700530"', add
label define countynhg_lbl 3700550 `"3700550"', add
label define countynhg_lbl 3700570 `"3700570"', add
label define countynhg_lbl 3700590 `"3700590"', add
label define countynhg_lbl 3700610 `"3700610"', add
label define countynhg_lbl 3700630 `"3700630"', add
label define countynhg_lbl 3700650 `"3700650"', add
label define countynhg_lbl 3700670 `"3700670"', add
label define countynhg_lbl 3700690 `"3700690"', add
label define countynhg_lbl 3700710 `"3700710"', add
label define countynhg_lbl 3700730 `"3700730"', add
label define countynhg_lbl 3700750 `"3700750"', add
label define countynhg_lbl 3700770 `"3700770"', add
label define countynhg_lbl 3700790 `"3700790"', add
label define countynhg_lbl 3700810 `"3700810"', add
label define countynhg_lbl 3700830 `"3700830"', add
label define countynhg_lbl 3700850 `"3700850"', add
label define countynhg_lbl 3700870 `"3700870"', add
label define countynhg_lbl 3700890 `"3700890"', add
label define countynhg_lbl 3700910 `"3700910"', add
label define countynhg_lbl 3700930 `"3700930"', add
label define countynhg_lbl 3700950 `"3700950"', add
label define countynhg_lbl 3700970 `"3700970"', add
label define countynhg_lbl 3700990 `"3700990"', add
label define countynhg_lbl 3701010 `"3701010"', add
label define countynhg_lbl 3701030 `"3701030"', add
label define countynhg_lbl 3701050 `"3701050"', add
label define countynhg_lbl 3701070 `"3701070"', add
label define countynhg_lbl 3701090 `"3701090"', add
label define countynhg_lbl 3701110 `"3701110"', add
label define countynhg_lbl 3701130 `"3701130"', add
label define countynhg_lbl 3701150 `"3701150"', add
label define countynhg_lbl 3701170 `"3701170"', add
label define countynhg_lbl 3701190 `"3701190"', add
label define countynhg_lbl 3701210 `"3701210"', add
label define countynhg_lbl 3701230 `"3701230"', add
label define countynhg_lbl 3701250 `"3701250"', add
label define countynhg_lbl 3701270 `"3701270"', add
label define countynhg_lbl 3701290 `"3701290"', add
label define countynhg_lbl 3701310 `"3701310"', add
label define countynhg_lbl 3701330 `"3701330"', add
label define countynhg_lbl 3701350 `"3701350"', add
label define countynhg_lbl 3701370 `"3701370"', add
label define countynhg_lbl 3701390 `"3701390"', add
label define countynhg_lbl 3701410 `"3701410"', add
label define countynhg_lbl 3701430 `"3701430"', add
label define countynhg_lbl 3701450 `"3701450"', add
label define countynhg_lbl 3701470 `"3701470"', add
label define countynhg_lbl 3701490 `"3701490"', add
label define countynhg_lbl 3701510 `"3701510"', add
label define countynhg_lbl 3701530 `"3701530"', add
label define countynhg_lbl 3701550 `"3701550"', add
label define countynhg_lbl 3701570 `"3701570"', add
label define countynhg_lbl 3701590 `"3701590"', add
label define countynhg_lbl 3701610 `"3701610"', add
label define countynhg_lbl 3701630 `"3701630"', add
label define countynhg_lbl 3701650 `"3701650"', add
label define countynhg_lbl 3701670 `"3701670"', add
label define countynhg_lbl 3701690 `"3701690"', add
label define countynhg_lbl 3701710 `"3701710"', add
label define countynhg_lbl 3701730 `"3701730"', add
label define countynhg_lbl 3701750 `"3701750"', add
label define countynhg_lbl 3701770 `"3701770"', add
label define countynhg_lbl 3701790 `"3701790"', add
label define countynhg_lbl 3701810 `"3701810"', add
label define countynhg_lbl 3701830 `"3701830"', add
label define countynhg_lbl 3701850 `"3701850"', add
label define countynhg_lbl 3701870 `"3701870"', add
label define countynhg_lbl 3701890 `"3701890"', add
label define countynhg_lbl 3701910 `"3701910"', add
label define countynhg_lbl 3701930 `"3701930"', add
label define countynhg_lbl 3701950 `"3701950"', add
label define countynhg_lbl 3701970 `"3701970"', add
label define countynhg_lbl 3701990 `"3701990"', add
label define countynhg_lbl 3800010 `"3800010"', add
label define countynhg_lbl 3800030 `"3800030"', add
label define countynhg_lbl 3800050 `"3800050"', add
label define countynhg_lbl 3800070 `"3800070"', add
label define countynhg_lbl 3800090 `"3800090"', add
label define countynhg_lbl 3800110 `"3800110"', add
label define countynhg_lbl 3800130 `"3800130"', add
label define countynhg_lbl 3800150 `"3800150"', add
label define countynhg_lbl 3800170 `"3800170"', add
label define countynhg_lbl 3800190 `"3800190"', add
label define countynhg_lbl 3800210 `"3800210"', add
label define countynhg_lbl 3800230 `"3800230"', add
label define countynhg_lbl 3800250 `"3800250"', add
label define countynhg_lbl 3800270 `"3800270"', add
label define countynhg_lbl 3800290 `"3800290"', add
label define countynhg_lbl 3800310 `"3800310"', add
label define countynhg_lbl 3800330 `"3800330"', add
label define countynhg_lbl 3800350 `"3800350"', add
label define countynhg_lbl 3800370 `"3800370"', add
label define countynhg_lbl 3800390 `"3800390"', add
label define countynhg_lbl 3800410 `"3800410"', add
label define countynhg_lbl 3800430 `"3800430"', add
label define countynhg_lbl 3800450 `"3800450"', add
label define countynhg_lbl 3800470 `"3800470"', add
label define countynhg_lbl 3800490 `"3800490"', add
label define countynhg_lbl 3800510 `"3800510"', add
label define countynhg_lbl 3800530 `"3800530"', add
label define countynhg_lbl 3800550 `"3800550"', add
label define countynhg_lbl 3800570 `"3800570"', add
label define countynhg_lbl 3800590 `"3800590"', add
label define countynhg_lbl 3800610 `"3800610"', add
label define countynhg_lbl 3800630 `"3800630"', add
label define countynhg_lbl 3800650 `"3800650"', add
label define countynhg_lbl 3800670 `"3800670"', add
label define countynhg_lbl 3800690 `"3800690"', add
label define countynhg_lbl 3800710 `"3800710"', add
label define countynhg_lbl 3800730 `"3800730"', add
label define countynhg_lbl 3800750 `"3800750"', add
label define countynhg_lbl 3800770 `"3800770"', add
label define countynhg_lbl 3800790 `"3800790"', add
label define countynhg_lbl 3800810 `"3800810"', add
label define countynhg_lbl 3800830 `"3800830"', add
label define countynhg_lbl 3800850 `"3800850"', add
label define countynhg_lbl 3800870 `"3800870"', add
label define countynhg_lbl 3800890 `"3800890"', add
label define countynhg_lbl 3800910 `"3800910"', add
label define countynhg_lbl 3800930 `"3800930"', add
label define countynhg_lbl 3800950 `"3800950"', add
label define countynhg_lbl 3800970 `"3800970"', add
label define countynhg_lbl 3800990 `"3800990"', add
label define countynhg_lbl 3801010 `"3801010"', add
label define countynhg_lbl 3801030 `"3801030"', add
label define countynhg_lbl 3801050 `"3801050"', add
label define countynhg_lbl 3809055 `"3809055"', add
label define countynhg_lbl 3900010 `"3900010"', add
label define countynhg_lbl 3900030 `"3900030"', add
label define countynhg_lbl 3900050 `"3900050"', add
label define countynhg_lbl 3900070 `"3900070"', add
label define countynhg_lbl 3900090 `"3900090"', add
label define countynhg_lbl 3900110 `"3900110"', add
label define countynhg_lbl 3900130 `"3900130"', add
label define countynhg_lbl 3900150 `"3900150"', add
label define countynhg_lbl 3900170 `"3900170"', add
label define countynhg_lbl 3900190 `"3900190"', add
label define countynhg_lbl 3900210 `"3900210"', add
label define countynhg_lbl 3900230 `"3900230"', add
label define countynhg_lbl 3900250 `"3900250"', add
label define countynhg_lbl 3900270 `"3900270"', add
label define countynhg_lbl 3900290 `"3900290"', add
label define countynhg_lbl 3900310 `"3900310"', add
label define countynhg_lbl 3900330 `"3900330"', add
label define countynhg_lbl 3900350 `"3900350"', add
label define countynhg_lbl 3900370 `"3900370"', add
label define countynhg_lbl 3900390 `"3900390"', add
label define countynhg_lbl 3900410 `"3900410"', add
label define countynhg_lbl 3900430 `"3900430"', add
label define countynhg_lbl 3900450 `"3900450"', add
label define countynhg_lbl 3900470 `"3900470"', add
label define countynhg_lbl 3900490 `"3900490"', add
label define countynhg_lbl 3900510 `"3900510"', add
label define countynhg_lbl 3900530 `"3900530"', add
label define countynhg_lbl 3900550 `"3900550"', add
label define countynhg_lbl 3900570 `"3900570"', add
label define countynhg_lbl 3900590 `"3900590"', add
label define countynhg_lbl 3900610 `"3900610"', add
label define countynhg_lbl 3900630 `"3900630"', add
label define countynhg_lbl 3900650 `"3900650"', add
label define countynhg_lbl 3900670 `"3900670"', add
label define countynhg_lbl 3900690 `"3900690"', add
label define countynhg_lbl 3900710 `"3900710"', add
label define countynhg_lbl 3900730 `"3900730"', add
label define countynhg_lbl 3900750 `"3900750"', add
label define countynhg_lbl 3900770 `"3900770"', add
label define countynhg_lbl 3900790 `"3900790"', add
label define countynhg_lbl 3900810 `"3900810"', add
label define countynhg_lbl 3900830 `"3900830"', add
label define countynhg_lbl 3900850 `"3900850"', add
label define countynhg_lbl 3900870 `"3900870"', add
label define countynhg_lbl 3900890 `"3900890"', add
label define countynhg_lbl 3900910 `"3900910"', add
label define countynhg_lbl 3900930 `"3900930"', add
label define countynhg_lbl 3900950 `"3900950"', add
label define countynhg_lbl 3900970 `"3900970"', add
label define countynhg_lbl 3900990 `"3900990"', add
label define countynhg_lbl 3901010 `"3901010"', add
label define countynhg_lbl 3901030 `"3901030"', add
label define countynhg_lbl 3901050 `"3901050"', add
label define countynhg_lbl 3901070 `"3901070"', add
label define countynhg_lbl 3901090 `"3901090"', add
label define countynhg_lbl 3901110 `"3901110"', add
label define countynhg_lbl 3901130 `"3901130"', add
label define countynhg_lbl 3901150 `"3901150"', add
label define countynhg_lbl 3901170 `"3901170"', add
label define countynhg_lbl 3901190 `"3901190"', add
label define countynhg_lbl 3901210 `"3901210"', add
label define countynhg_lbl 3901230 `"3901230"', add
label define countynhg_lbl 3901250 `"3901250"', add
label define countynhg_lbl 3901270 `"3901270"', add
label define countynhg_lbl 3901290 `"3901290"', add
label define countynhg_lbl 3901310 `"3901310"', add
label define countynhg_lbl 3901330 `"3901330"', add
label define countynhg_lbl 3901350 `"3901350"', add
label define countynhg_lbl 3901370 `"3901370"', add
label define countynhg_lbl 3901390 `"3901390"', add
label define countynhg_lbl 3901410 `"3901410"', add
label define countynhg_lbl 3901430 `"3901430"', add
label define countynhg_lbl 3901450 `"3901450"', add
label define countynhg_lbl 3901470 `"3901470"', add
label define countynhg_lbl 3901490 `"3901490"', add
label define countynhg_lbl 3901510 `"3901510"', add
label define countynhg_lbl 3901530 `"3901530"', add
label define countynhg_lbl 3901550 `"3901550"', add
label define countynhg_lbl 3901570 `"3901570"', add
label define countynhg_lbl 3901590 `"3901590"', add
label define countynhg_lbl 3901610 `"3901610"', add
label define countynhg_lbl 3901630 `"3901630"', add
label define countynhg_lbl 3901650 `"3901650"', add
label define countynhg_lbl 3901670 `"3901670"', add
label define countynhg_lbl 3901690 `"3901690"', add
label define countynhg_lbl 3901710 `"3901710"', add
label define countynhg_lbl 3901730 `"3901730"', add
label define countynhg_lbl 3901750 `"3901750"', add
label define countynhg_lbl 4000010 `"4000010"', add
label define countynhg_lbl 4000030 `"4000030"', add
label define countynhg_lbl 4000050 `"4000050"', add
label define countynhg_lbl 4000070 `"4000070"', add
label define countynhg_lbl 4000090 `"4000090"', add
label define countynhg_lbl 4000110 `"4000110"', add
label define countynhg_lbl 4000130 `"4000130"', add
label define countynhg_lbl 4000150 `"4000150"', add
label define countynhg_lbl 4000170 `"4000170"', add
label define countynhg_lbl 4000190 `"4000190"', add
label define countynhg_lbl 4000210 `"4000210"', add
label define countynhg_lbl 4000230 `"4000230"', add
label define countynhg_lbl 4000250 `"4000250"', add
label define countynhg_lbl 4000270 `"4000270"', add
label define countynhg_lbl 4000290 `"4000290"', add
label define countynhg_lbl 4000310 `"4000310"', add
label define countynhg_lbl 4000330 `"4000330"', add
label define countynhg_lbl 4000350 `"4000350"', add
label define countynhg_lbl 4000370 `"4000370"', add
label define countynhg_lbl 4000390 `"4000390"', add
label define countynhg_lbl 4000410 `"4000410"', add
label define countynhg_lbl 4000430 `"4000430"', add
label define countynhg_lbl 4000450 `"4000450"', add
label define countynhg_lbl 4000470 `"4000470"', add
label define countynhg_lbl 4000490 `"4000490"', add
label define countynhg_lbl 4000510 `"4000510"', add
label define countynhg_lbl 4000530 `"4000530"', add
label define countynhg_lbl 4000550 `"4000550"', add
label define countynhg_lbl 4000570 `"4000570"', add
label define countynhg_lbl 4000590 `"4000590"', add
label define countynhg_lbl 4000610 `"4000610"', add
label define countynhg_lbl 4000630 `"4000630"', add
label define countynhg_lbl 4000650 `"4000650"', add
label define countynhg_lbl 4000670 `"4000670"', add
label define countynhg_lbl 4000690 `"4000690"', add
label define countynhg_lbl 4000710 `"4000710"', add
label define countynhg_lbl 4000730 `"4000730"', add
label define countynhg_lbl 4000750 `"4000750"', add
label define countynhg_lbl 4000770 `"4000770"', add
label define countynhg_lbl 4000790 `"4000790"', add
label define countynhg_lbl 4000810 `"4000810"', add
label define countynhg_lbl 4000830 `"4000830"', add
label define countynhg_lbl 4000850 `"4000850"', add
label define countynhg_lbl 4000870 `"4000870"', add
label define countynhg_lbl 4000890 `"4000890"', add
label define countynhg_lbl 4000910 `"4000910"', add
label define countynhg_lbl 4000930 `"4000930"', add
label define countynhg_lbl 4000950 `"4000950"', add
label define countynhg_lbl 4000970 `"4000970"', add
label define countynhg_lbl 4000990 `"4000990"', add
label define countynhg_lbl 4001010 `"4001010"', add
label define countynhg_lbl 4001030 `"4001030"', add
label define countynhg_lbl 4001050 `"4001050"', add
label define countynhg_lbl 4001070 `"4001070"', add
label define countynhg_lbl 4001090 `"4001090"', add
label define countynhg_lbl 4001110 `"4001110"', add
label define countynhg_lbl 4001130 `"4001130"', add
label define countynhg_lbl 4001150 `"4001150"', add
label define countynhg_lbl 4001170 `"4001170"', add
label define countynhg_lbl 4001190 `"4001190"', add
label define countynhg_lbl 4001210 `"4001210"', add
label define countynhg_lbl 4001230 `"4001230"', add
label define countynhg_lbl 4001250 `"4001250"', add
label define countynhg_lbl 4001270 `"4001270"', add
label define countynhg_lbl 4001290 `"4001290"', add
label define countynhg_lbl 4001310 `"4001310"', add
label define countynhg_lbl 4001330 `"4001330"', add
label define countynhg_lbl 4001350 `"4001350"', add
label define countynhg_lbl 4001370 `"4001370"', add
label define countynhg_lbl 4001390 `"4001390"', add
label define countynhg_lbl 4001410 `"4001410"', add
label define countynhg_lbl 4001430 `"4001430"', add
label define countynhg_lbl 4001450 `"4001450"', add
label define countynhg_lbl 4001470 `"4001470"', add
label define countynhg_lbl 4001490 `"4001490"', add
label define countynhg_lbl 4001510 `"4001510"', add
label define countynhg_lbl 4001530 `"4001530"', add
label define countynhg_lbl 4050015 `"4050015"', add
label define countynhg_lbl 4050035 `"4050035"', add
label define countynhg_lbl 4050055 `"4050055"', add
label define countynhg_lbl 4050075 `"4050075"', add
label define countynhg_lbl 4050095 `"4050095"', add
label define countynhg_lbl 4050155 `"4050155"', add
label define countynhg_lbl 4050175 `"4050175"', add
label define countynhg_lbl 4050195 `"4050195"', add
label define countynhg_lbl 4050215 `"4050215"', add
label define countynhg_lbl 4050235 `"4050235"', add
label define countynhg_lbl 4050255 `"4050255"', add
label define countynhg_lbl 4050275 `"4050275"', add
label define countynhg_lbl 4050295 `"4050295"', add
label define countynhg_lbl 4050315 `"4050315"', add
label define countynhg_lbl 4050335 `"4050335"', add
label define countynhg_lbl 4050355 `"4050355"', add
label define countynhg_lbl 4050375 `"4050375"', add
label define countynhg_lbl 4050395 `"4050395"', add
label define countynhg_lbl 4050415 `"4050415"', add
label define countynhg_lbl 4050435 `"4050435"', add
label define countynhg_lbl 4050455 `"4050455"', add
label define countynhg_lbl 4050475 `"4050475"', add
label define countynhg_lbl 4059015 `"4059015"', add
label define countynhg_lbl 4059035 `"4059035"', add
label define countynhg_lbl 4059055 `"4059055"', add
label define countynhg_lbl 4059075 `"4059075"', add
label define countynhg_lbl 4100010 `"4100010"', add
label define countynhg_lbl 4100030 `"4100030"', add
label define countynhg_lbl 4100050 `"4100050"', add
label define countynhg_lbl 4100070 `"4100070"', add
label define countynhg_lbl 4100090 `"4100090"', add
label define countynhg_lbl 4100110 `"4100110"', add
label define countynhg_lbl 4100130 `"4100130"', add
label define countynhg_lbl 4100150 `"4100150"', add
label define countynhg_lbl 4100170 `"4100170"', add
label define countynhg_lbl 4100190 `"4100190"', add
label define countynhg_lbl 4100210 `"4100210"', add
label define countynhg_lbl 4100230 `"4100230"', add
label define countynhg_lbl 4100250 `"4100250"', add
label define countynhg_lbl 4100270 `"4100270"', add
label define countynhg_lbl 4100290 `"4100290"', add
label define countynhg_lbl 4100310 `"4100310"', add
label define countynhg_lbl 4100330 `"4100330"', add
label define countynhg_lbl 4100350 `"4100350"', add
label define countynhg_lbl 4100370 `"4100370"', add
label define countynhg_lbl 4100390 `"4100390"', add
label define countynhg_lbl 4100410 `"4100410"', add
label define countynhg_lbl 4100430 `"4100430"', add
label define countynhg_lbl 4100450 `"4100450"', add
label define countynhg_lbl 4100470 `"4100470"', add
label define countynhg_lbl 4100490 `"4100490"', add
label define countynhg_lbl 4100510 `"4100510"', add
label define countynhg_lbl 4100530 `"4100530"', add
label define countynhg_lbl 4100550 `"4100550"', add
label define countynhg_lbl 4100570 `"4100570"', add
label define countynhg_lbl 4100590 `"4100590"', add
label define countynhg_lbl 4100595 `"4100595"', add
label define countynhg_lbl 4100610 `"4100610"', add
label define countynhg_lbl 4100630 `"4100630"', add
label define countynhg_lbl 4100650 `"4100650"', add
label define countynhg_lbl 4100670 `"4100670"', add
label define countynhg_lbl 4100690 `"4100690"', add
label define countynhg_lbl 4100710 `"4100710"', add
label define countynhg_lbl 4130035 `"4130035"', add
label define countynhg_lbl 4130075 `"4130075"', add
label define countynhg_lbl 4130115 `"4130115"', add
label define countynhg_lbl 4130135 `"4130135"', add
label define countynhg_lbl 4130155 `"4130155"', add
label define countynhg_lbl 4130175 `"4130175"', add
label define countynhg_lbl 4130195 `"4130195"', add
label define countynhg_lbl 4200010 `"4200010"', add
label define countynhg_lbl 4200030 `"4200030"', add
label define countynhg_lbl 4200050 `"4200050"', add
label define countynhg_lbl 4200070 `"4200070"', add
label define countynhg_lbl 4200090 `"4200090"', add
label define countynhg_lbl 4200110 `"4200110"', add
label define countynhg_lbl 4200130 `"4200130"', add
label define countynhg_lbl 4200150 `"4200150"', add
label define countynhg_lbl 4200170 `"4200170"', add
label define countynhg_lbl 4200190 `"4200190"', add
label define countynhg_lbl 4200210 `"4200210"', add
label define countynhg_lbl 4200230 `"4200230"', add
label define countynhg_lbl 4200250 `"4200250"', add
label define countynhg_lbl 4200270 `"4200270"', add
label define countynhg_lbl 4200290 `"4200290"', add
label define countynhg_lbl 4200310 `"4200310"', add
label define countynhg_lbl 4200330 `"4200330"', add
label define countynhg_lbl 4200350 `"4200350"', add
label define countynhg_lbl 4200370 `"4200370"', add
label define countynhg_lbl 4200390 `"4200390"', add
label define countynhg_lbl 4200410 `"4200410"', add
label define countynhg_lbl 4200430 `"4200430"', add
label define countynhg_lbl 4200450 `"4200450"', add
label define countynhg_lbl 4200470 `"4200470"', add
label define countynhg_lbl 4200490 `"4200490"', add
label define countynhg_lbl 4200510 `"4200510"', add
label define countynhg_lbl 4200530 `"4200530"', add
label define countynhg_lbl 4200550 `"4200550"', add
label define countynhg_lbl 4200570 `"4200570"', add
label define countynhg_lbl 4200590 `"4200590"', add
label define countynhg_lbl 4200610 `"4200610"', add
label define countynhg_lbl 4200630 `"4200630"', add
label define countynhg_lbl 4200650 `"4200650"', add
label define countynhg_lbl 4200670 `"4200670"', add
label define countynhg_lbl 4200690 `"4200690"', add
label define countynhg_lbl 4200710 `"4200710"', add
label define countynhg_lbl 4200730 `"4200730"', add
label define countynhg_lbl 4200750 `"4200750"', add
label define countynhg_lbl 4200770 `"4200770"', add
label define countynhg_lbl 4200790 `"4200790"', add
label define countynhg_lbl 4200810 `"4200810"', add
label define countynhg_lbl 4200830 `"4200830"', add
label define countynhg_lbl 4200850 `"4200850"', add
label define countynhg_lbl 4200870 `"4200870"', add
label define countynhg_lbl 4200890 `"4200890"', add
label define countynhg_lbl 4200910 `"4200910"', add
label define countynhg_lbl 4200930 `"4200930"', add
label define countynhg_lbl 4200950 `"4200950"', add
label define countynhg_lbl 4200970 `"4200970"', add
label define countynhg_lbl 4200990 `"4200990"', add
label define countynhg_lbl 4201010 `"4201010"', add
label define countynhg_lbl 4201030 `"4201030"', add
label define countynhg_lbl 4201050 `"4201050"', add
label define countynhg_lbl 4201070 `"4201070"', add
label define countynhg_lbl 4201090 `"4201090"', add
label define countynhg_lbl 4201110 `"4201110"', add
label define countynhg_lbl 4201130 `"4201130"', add
label define countynhg_lbl 4201150 `"4201150"', add
label define countynhg_lbl 4201170 `"4201170"', add
label define countynhg_lbl 4201190 `"4201190"', add
label define countynhg_lbl 4201210 `"4201210"', add
label define countynhg_lbl 4201230 `"4201230"', add
label define countynhg_lbl 4201250 `"4201250"', add
label define countynhg_lbl 4201270 `"4201270"', add
label define countynhg_lbl 4201290 `"4201290"', add
label define countynhg_lbl 4201310 `"4201310"', add
label define countynhg_lbl 4201330 `"4201330"', add
label define countynhg_lbl 4400010 `"4400010"', add
label define countynhg_lbl 4400030 `"4400030"', add
label define countynhg_lbl 4400050 `"4400050"', add
label define countynhg_lbl 4400070 `"4400070"', add
label define countynhg_lbl 4400090 `"4400090"', add
label define countynhg_lbl 4500010 `"4500010"', add
label define countynhg_lbl 4500030 `"4500030"', add
label define countynhg_lbl 4500050 `"4500050"', add
label define countynhg_lbl 4500070 `"4500070"', add
label define countynhg_lbl 4500090 `"4500090"', add
label define countynhg_lbl 4500110 `"4500110"', add
label define countynhg_lbl 4500130 `"4500130"', add
label define countynhg_lbl 4500150 `"4500150"', add
label define countynhg_lbl 4500170 `"4500170"', add
label define countynhg_lbl 4500190 `"4500190"', add
label define countynhg_lbl 4500210 `"4500210"', add
label define countynhg_lbl 4500230 `"4500230"', add
label define countynhg_lbl 4500250 `"4500250"', add
label define countynhg_lbl 4500270 `"4500270"', add
label define countynhg_lbl 4500290 `"4500290"', add
label define countynhg_lbl 4500310 `"4500310"', add
label define countynhg_lbl 4500330 `"4500330"', add
label define countynhg_lbl 4500350 `"4500350"', add
label define countynhg_lbl 4500370 `"4500370"', add
label define countynhg_lbl 4500390 `"4500390"', add
label define countynhg_lbl 4500410 `"4500410"', add
label define countynhg_lbl 4500430 `"4500430"', add
label define countynhg_lbl 4500450 `"4500450"', add
label define countynhg_lbl 4500470 `"4500470"', add
label define countynhg_lbl 4500490 `"4500490"', add
label define countynhg_lbl 4500510 `"4500510"', add
label define countynhg_lbl 4500530 `"4500530"', add
label define countynhg_lbl 4500550 `"4500550"', add
label define countynhg_lbl 4500570 `"4500570"', add
label define countynhg_lbl 4500590 `"4500590"', add
label define countynhg_lbl 4500610 `"4500610"', add
label define countynhg_lbl 4500630 `"4500630"', add
label define countynhg_lbl 4500650 `"4500650"', add
label define countynhg_lbl 4500670 `"4500670"', add
label define countynhg_lbl 4500690 `"4500690"', add
label define countynhg_lbl 4500710 `"4500710"', add
label define countynhg_lbl 4500730 `"4500730"', add
label define countynhg_lbl 4500750 `"4500750"', add
label define countynhg_lbl 4500770 `"4500770"', add
label define countynhg_lbl 4500790 `"4500790"', add
label define countynhg_lbl 4500810 `"4500810"', add
label define countynhg_lbl 4500830 `"4500830"', add
label define countynhg_lbl 4500850 `"4500850"', add
label define countynhg_lbl 4500870 `"4500870"', add
label define countynhg_lbl 4500890 `"4500890"', add
label define countynhg_lbl 4500910 `"4500910"', add
label define countynhg_lbl 4600010 `"4600010"', add
label define countynhg_lbl 4600030 `"4600030"', add
label define countynhg_lbl 4600050 `"4600050"', add
label define countynhg_lbl 4600070 `"4600070"', add
label define countynhg_lbl 4600090 `"4600090"', add
label define countynhg_lbl 4600110 `"4600110"', add
label define countynhg_lbl 4600130 `"4600130"', add
label define countynhg_lbl 4600150 `"4600150"', add
label define countynhg_lbl 4600170 `"4600170"', add
label define countynhg_lbl 4600190 `"4600190"', add
label define countynhg_lbl 4600210 `"4600210"', add
label define countynhg_lbl 4600230 `"4600230"', add
label define countynhg_lbl 4600250 `"4600250"', add
label define countynhg_lbl 4600270 `"4600270"', add
label define countynhg_lbl 4600290 `"4600290"', add
label define countynhg_lbl 4600310 `"4600310"', add
label define countynhg_lbl 4600330 `"4600330"', add
label define countynhg_lbl 4600350 `"4600350"', add
label define countynhg_lbl 4600370 `"4600370"', add
label define countynhg_lbl 4600390 `"4600390"', add
label define countynhg_lbl 4600410 `"4600410"', add
label define countynhg_lbl 4600430 `"4600430"', add
label define countynhg_lbl 4600450 `"4600450"', add
label define countynhg_lbl 4600470 `"4600470"', add
label define countynhg_lbl 4600490 `"4600490"', add
label define countynhg_lbl 4600510 `"4600510"', add
label define countynhg_lbl 4600530 `"4600530"', add
label define countynhg_lbl 4600550 `"4600550"', add
label define countynhg_lbl 4600570 `"4600570"', add
label define countynhg_lbl 4600590 `"4600590"', add
label define countynhg_lbl 4600610 `"4600610"', add
label define countynhg_lbl 4600630 `"4600630"', add
label define countynhg_lbl 4600650 `"4600650"', add
label define countynhg_lbl 4600670 `"4600670"', add
label define countynhg_lbl 4600690 `"4600690"', add
label define countynhg_lbl 4600710 `"4600710"', add
label define countynhg_lbl 4600730 `"4600730"', add
label define countynhg_lbl 4600750 `"4600750"', add
label define countynhg_lbl 4600770 `"4600770"', add
label define countynhg_lbl 4600790 `"4600790"', add
label define countynhg_lbl 4600810 `"4600810"', add
label define countynhg_lbl 4600830 `"4600830"', add
label define countynhg_lbl 4600850 `"4600850"', add
label define countynhg_lbl 4600870 `"4600870"', add
label define countynhg_lbl 4600890 `"4600890"', add
label define countynhg_lbl 4600910 `"4600910"', add
label define countynhg_lbl 4600930 `"4600930"', add
label define countynhg_lbl 4600950 `"4600950"', add
label define countynhg_lbl 4600970 `"4600970"', add
label define countynhg_lbl 4600990 `"4600990"', add
label define countynhg_lbl 4601010 `"4601010"', add
label define countynhg_lbl 4601030 `"4601030"', add
label define countynhg_lbl 4601050 `"4601050"', add
label define countynhg_lbl 4601070 `"4601070"', add
label define countynhg_lbl 4601090 `"4601090"', add
label define countynhg_lbl 4601110 `"4601110"', add
label define countynhg_lbl 4601113 `"4601113"', add
label define countynhg_lbl 4601130 `"4601130"', add
label define countynhg_lbl 4601150 `"4601150"', add
label define countynhg_lbl 4601170 `"4601170"', add
label define countynhg_lbl 4601175 `"4601175"', add
label define countynhg_lbl 4601190 `"4601190"', add
label define countynhg_lbl 4601210 `"4601210"', add
label define countynhg_lbl 4601230 `"4601230"', add
label define countynhg_lbl 4601250 `"4601250"', add
label define countynhg_lbl 4601270 `"4601270"', add
label define countynhg_lbl 4601290 `"4601290"', add
label define countynhg_lbl 4601310 `"4601310"', add
label define countynhg_lbl 4601330 `"4601330"', add
label define countynhg_lbl 4601350 `"4601350"', add
label define countynhg_lbl 4601370 `"4601370"', add
label define countynhg_lbl 4609015 `"4609015"', add
label define countynhg_lbl 4609075 `"4609075"', add
label define countynhg_lbl 4609095 `"4609095"', add
label define countynhg_lbl 4609115 `"4609115"', add
label define countynhg_lbl 4700010 `"4700010"', add
label define countynhg_lbl 4700030 `"4700030"', add
label define countynhg_lbl 4700050 `"4700050"', add
label define countynhg_lbl 4700070 `"4700070"', add
label define countynhg_lbl 4700090 `"4700090"', add
label define countynhg_lbl 4700110 `"4700110"', add
label define countynhg_lbl 4700130 `"4700130"', add
label define countynhg_lbl 4700150 `"4700150"', add
label define countynhg_lbl 4700170 `"4700170"', add
label define countynhg_lbl 4700190 `"4700190"', add
label define countynhg_lbl 4700210 `"4700210"', add
label define countynhg_lbl 4700230 `"4700230"', add
label define countynhg_lbl 4700250 `"4700250"', add
label define countynhg_lbl 4700270 `"4700270"', add
label define countynhg_lbl 4700290 `"4700290"', add
label define countynhg_lbl 4700310 `"4700310"', add
label define countynhg_lbl 4700330 `"4700330"', add
label define countynhg_lbl 4700350 `"4700350"', add
label define countynhg_lbl 4700370 `"4700370"', add
label define countynhg_lbl 4700390 `"4700390"', add
label define countynhg_lbl 4700410 `"4700410"', add
label define countynhg_lbl 4700430 `"4700430"', add
label define countynhg_lbl 4700450 `"4700450"', add
label define countynhg_lbl 4700470 `"4700470"', add
label define countynhg_lbl 4700490 `"4700490"', add
label define countynhg_lbl 4700510 `"4700510"', add
label define countynhg_lbl 4700530 `"4700530"', add
label define countynhg_lbl 4700550 `"4700550"', add
label define countynhg_lbl 4700570 `"4700570"', add
label define countynhg_lbl 4700590 `"4700590"', add
label define countynhg_lbl 4700610 `"4700610"', add
label define countynhg_lbl 4700630 `"4700630"', add
label define countynhg_lbl 4700650 `"4700650"', add
label define countynhg_lbl 4700670 `"4700670"', add
label define countynhg_lbl 4700690 `"4700690"', add
label define countynhg_lbl 4700710 `"4700710"', add
label define countynhg_lbl 4700730 `"4700730"', add
label define countynhg_lbl 4700750 `"4700750"', add
label define countynhg_lbl 4700770 `"4700770"', add
label define countynhg_lbl 4700790 `"4700790"', add
label define countynhg_lbl 4700810 `"4700810"', add
label define countynhg_lbl 4700830 `"4700830"', add
label define countynhg_lbl 4700850 `"4700850"', add
label define countynhg_lbl 4700870 `"4700870"', add
label define countynhg_lbl 4700875 `"4700875"', add
label define countynhg_lbl 4700890 `"4700890"', add
label define countynhg_lbl 4700910 `"4700910"', add
label define countynhg_lbl 4700930 `"4700930"', add
label define countynhg_lbl 4700950 `"4700950"', add
label define countynhg_lbl 4700970 `"4700970"', add
label define countynhg_lbl 4700990 `"4700990"', add
label define countynhg_lbl 4701010 `"4701010"', add
label define countynhg_lbl 4701030 `"4701030"', add
label define countynhg_lbl 4701050 `"4701050"', add
label define countynhg_lbl 4701070 `"4701070"', add
label define countynhg_lbl 4701090 `"4701090"', add
label define countynhg_lbl 4701110 `"4701110"', add
label define countynhg_lbl 4701130 `"4701130"', add
label define countynhg_lbl 4701150 `"4701150"', add
label define countynhg_lbl 4701170 `"4701170"', add
label define countynhg_lbl 4701190 `"4701190"', add
label define countynhg_lbl 4701210 `"4701210"', add
label define countynhg_lbl 4701230 `"4701230"', add
label define countynhg_lbl 4701250 `"4701250"', add
label define countynhg_lbl 4701270 `"4701270"', add
label define countynhg_lbl 4701290 `"4701290"', add
label define countynhg_lbl 4701310 `"4701310"', add
label define countynhg_lbl 4701330 `"4701330"', add
label define countynhg_lbl 4701350 `"4701350"', add
label define countynhg_lbl 4701370 `"4701370"', add
label define countynhg_lbl 4701390 `"4701390"', add
label define countynhg_lbl 4701410 `"4701410"', add
label define countynhg_lbl 4701430 `"4701430"', add
label define countynhg_lbl 4701450 `"4701450"', add
label define countynhg_lbl 4701470 `"4701470"', add
label define countynhg_lbl 4701490 `"4701490"', add
label define countynhg_lbl 4701510 `"4701510"', add
label define countynhg_lbl 4701530 `"4701530"', add
label define countynhg_lbl 4701550 `"4701550"', add
label define countynhg_lbl 4701570 `"4701570"', add
label define countynhg_lbl 4701590 `"4701590"', add
label define countynhg_lbl 4701610 `"4701610"', add
label define countynhg_lbl 4701630 `"4701630"', add
label define countynhg_lbl 4701650 `"4701650"', add
label define countynhg_lbl 4701670 `"4701670"', add
label define countynhg_lbl 4701690 `"4701690"', add
label define countynhg_lbl 4701710 `"4701710"', add
label define countynhg_lbl 4701730 `"4701730"', add
label define countynhg_lbl 4701750 `"4701750"', add
label define countynhg_lbl 4701770 `"4701770"', add
label define countynhg_lbl 4701790 `"4701790"', add
label define countynhg_lbl 4701810 `"4701810"', add
label define countynhg_lbl 4701830 `"4701830"', add
label define countynhg_lbl 4701850 `"4701850"', add
label define countynhg_lbl 4701870 `"4701870"', add
label define countynhg_lbl 4701890 `"4701890"', add
label define countynhg_lbl 4800010 `"4800010"', add
label define countynhg_lbl 4800030 `"4800030"', add
label define countynhg_lbl 4800050 `"4800050"', add
label define countynhg_lbl 4800070 `"4800070"', add
label define countynhg_lbl 4800090 `"4800090"', add
label define countynhg_lbl 4800110 `"4800110"', add
label define countynhg_lbl 4800130 `"4800130"', add
label define countynhg_lbl 4800150 `"4800150"', add
label define countynhg_lbl 4800170 `"4800170"', add
label define countynhg_lbl 4800190 `"4800190"', add
label define countynhg_lbl 4800210 `"4800210"', add
label define countynhg_lbl 4800230 `"4800230"', add
label define countynhg_lbl 4800250 `"4800250"', add
label define countynhg_lbl 4800270 `"4800270"', add
label define countynhg_lbl 4800290 `"4800290"', add
label define countynhg_lbl 4800295 `"4800295"', add
label define countynhg_lbl 4800310 `"4800310"', add
label define countynhg_lbl 4800330 `"4800330"', add
label define countynhg_lbl 4800350 `"4800350"', add
label define countynhg_lbl 4800370 `"4800370"', add
label define countynhg_lbl 4800390 `"4800390"', add
label define countynhg_lbl 4800410 `"4800410"', add
label define countynhg_lbl 4800430 `"4800430"', add
label define countynhg_lbl 4800450 `"4800450"', add
label define countynhg_lbl 4800470 `"4800470"', add
label define countynhg_lbl 4800490 `"4800490"', add
label define countynhg_lbl 4800493 `"4800493"', add
label define countynhg_lbl 4800510 `"4800510"', add
label define countynhg_lbl 4800530 `"4800530"', add
label define countynhg_lbl 4800550 `"4800550"', add
label define countynhg_lbl 4800570 `"4800570"', add
label define countynhg_lbl 4800590 `"4800590"', add
label define countynhg_lbl 4800610 `"4800610"', add
label define countynhg_lbl 4800630 `"4800630"', add
label define countynhg_lbl 4800650 `"4800650"', add
label define countynhg_lbl 4800655 `"4800655"', add
label define countynhg_lbl 4800670 `"4800670"', add
label define countynhg_lbl 4800690 `"4800690"', add
label define countynhg_lbl 4800710 `"4800710"', add
label define countynhg_lbl 4800730 `"4800730"', add
label define countynhg_lbl 4800750 `"4800750"', add
label define countynhg_lbl 4800770 `"4800770"', add
label define countynhg_lbl 4800790 `"4800790"', add
label define countynhg_lbl 4800810 `"4800810"', add
label define countynhg_lbl 4800830 `"4800830"', add
label define countynhg_lbl 4800850 `"4800850"', add
label define countynhg_lbl 4800870 `"4800870"', add
label define countynhg_lbl 4800890 `"4800890"', add
label define countynhg_lbl 4800910 `"4800910"', add
label define countynhg_lbl 4800930 `"4800930"', add
label define countynhg_lbl 4800950 `"4800950"', add
label define countynhg_lbl 4800970 `"4800970"', add
label define countynhg_lbl 4800990 `"4800990"', add
label define countynhg_lbl 4801010 `"4801010"', add
label define countynhg_lbl 4801030 `"4801030"', add
label define countynhg_lbl 4801050 `"4801050"', add
label define countynhg_lbl 4801070 `"4801070"', add
label define countynhg_lbl 4801090 `"4801090"', add
label define countynhg_lbl 4801110 `"4801110"', add
label define countynhg_lbl 4801130 `"4801130"', add
label define countynhg_lbl 4801135 `"4801135"', add
label define countynhg_lbl 4801150 `"4801150"', add
label define countynhg_lbl 4801155 `"4801155"', add
label define countynhg_lbl 4801170 `"4801170"', add
label define countynhg_lbl 4801190 `"4801190"', add
label define countynhg_lbl 4801210 `"4801210"', add
label define countynhg_lbl 4801230 `"4801230"', add
label define countynhg_lbl 4801250 `"4801250"', add
label define countynhg_lbl 4801270 `"4801270"', add
label define countynhg_lbl 4801290 `"4801290"', add
label define countynhg_lbl 4801310 `"4801310"', add
label define countynhg_lbl 4801330 `"4801330"', add
label define countynhg_lbl 4801350 `"4801350"', add
label define countynhg_lbl 4801370 `"4801370"', add
label define countynhg_lbl 4801390 `"4801390"', add
label define countynhg_lbl 4801410 `"4801410"', add
label define countynhg_lbl 4801415 `"4801415"', add
label define countynhg_lbl 4801430 `"4801430"', add
label define countynhg_lbl 4801450 `"4801450"', add
label define countynhg_lbl 4801470 `"4801470"', add
label define countynhg_lbl 4801490 `"4801490"', add
label define countynhg_lbl 4801510 `"4801510"', add
label define countynhg_lbl 4801530 `"4801530"', add
label define countynhg_lbl 4801550 `"4801550"', add
label define countynhg_lbl 4801570 `"4801570"', add
label define countynhg_lbl 4801590 `"4801590"', add
label define countynhg_lbl 4801610 `"4801610"', add
label define countynhg_lbl 4801630 `"4801630"', add
label define countynhg_lbl 4801650 `"4801650"', add
label define countynhg_lbl 4801670 `"4801670"', add
label define countynhg_lbl 4801690 `"4801690"', add
label define countynhg_lbl 4801710 `"4801710"', add
label define countynhg_lbl 4801730 `"4801730"', add
label define countynhg_lbl 4801750 `"4801750"', add
label define countynhg_lbl 4801770 `"4801770"', add
label define countynhg_lbl 4801790 `"4801790"', add
label define countynhg_lbl 4801810 `"4801810"', add
label define countynhg_lbl 4801830 `"4801830"', add
label define countynhg_lbl 4801850 `"4801850"', add
label define countynhg_lbl 4801870 `"4801870"', add
label define countynhg_lbl 4801890 `"4801890"', add
label define countynhg_lbl 4801910 `"4801910"', add
label define countynhg_lbl 4801930 `"4801930"', add
label define countynhg_lbl 4801950 `"4801950"', add
label define countynhg_lbl 4801970 `"4801970"', add
label define countynhg_lbl 4801990 `"4801990"', add
label define countynhg_lbl 4802010 `"4802010"', add
label define countynhg_lbl 4802030 `"4802030"', add
label define countynhg_lbl 4802050 `"4802050"', add
label define countynhg_lbl 4802070 `"4802070"', add
label define countynhg_lbl 4802090 `"4802090"', add
label define countynhg_lbl 4802110 `"4802110"', add
label define countynhg_lbl 4802130 `"4802130"', add
label define countynhg_lbl 4802150 `"4802150"', add
label define countynhg_lbl 4802170 `"4802170"', add
label define countynhg_lbl 4802190 `"4802190"', add
label define countynhg_lbl 4802210 `"4802210"', add
label define countynhg_lbl 4802230 `"4802230"', add
label define countynhg_lbl 4802250 `"4802250"', add
label define countynhg_lbl 4802270 `"4802270"', add
label define countynhg_lbl 4802290 `"4802290"', add
label define countynhg_lbl 4802310 `"4802310"', add
label define countynhg_lbl 4802330 `"4802330"', add
label define countynhg_lbl 4802350 `"4802350"', add
label define countynhg_lbl 4802370 `"4802370"', add
label define countynhg_lbl 4802390 `"4802390"', add
label define countynhg_lbl 4802410 `"4802410"', add
label define countynhg_lbl 4802430 `"4802430"', add
label define countynhg_lbl 4802450 `"4802450"', add
label define countynhg_lbl 4802470 `"4802470"', add
label define countynhg_lbl 4802490 `"4802490"', add
label define countynhg_lbl 4802510 `"4802510"', add
label define countynhg_lbl 4802530 `"4802530"', add
label define countynhg_lbl 4802550 `"4802550"', add
label define countynhg_lbl 4802570 `"4802570"', add
label define countynhg_lbl 4802590 `"4802590"', add
label define countynhg_lbl 4802610 `"4802610"', add
label define countynhg_lbl 4802630 `"4802630"', add
label define countynhg_lbl 4802650 `"4802650"', add
label define countynhg_lbl 4802670 `"4802670"', add
label define countynhg_lbl 4802690 `"4802690"', add
label define countynhg_lbl 4802710 `"4802710"', add
label define countynhg_lbl 4802730 `"4802730"', add
label define countynhg_lbl 4802750 `"4802750"', add
label define countynhg_lbl 4802770 `"4802770"', add
label define countynhg_lbl 4802790 `"4802790"', add
label define countynhg_lbl 4802810 `"4802810"', add
label define countynhg_lbl 4802830 `"4802830"', add
label define countynhg_lbl 4802850 `"4802850"', add
label define countynhg_lbl 4802870 `"4802870"', add
label define countynhg_lbl 4802890 `"4802890"', add
label define countynhg_lbl 4802910 `"4802910"', add
label define countynhg_lbl 4802930 `"4802930"', add
label define countynhg_lbl 4802950 `"4802950"', add
label define countynhg_lbl 4802970 `"4802970"', add
label define countynhg_lbl 4802990 `"4802990"', add
label define countynhg_lbl 4803010 `"4803010"', add
label define countynhg_lbl 4803030 `"4803030"', add
label define countynhg_lbl 4803050 `"4803050"', add
label define countynhg_lbl 4803070 `"4803070"', add
label define countynhg_lbl 4803090 `"4803090"', add
label define countynhg_lbl 4803110 `"4803110"', add
label define countynhg_lbl 4803130 `"4803130"', add
label define countynhg_lbl 4803150 `"4803150"', add
label define countynhg_lbl 4803170 `"4803170"', add
label define countynhg_lbl 4803190 `"4803190"', add
label define countynhg_lbl 4803210 `"4803210"', add
label define countynhg_lbl 4803230 `"4803230"', add
label define countynhg_lbl 4803250 `"4803250"', add
label define countynhg_lbl 4803270 `"4803270"', add
label define countynhg_lbl 4803290 `"4803290"', add
label define countynhg_lbl 4803310 `"4803310"', add
label define countynhg_lbl 4803330 `"4803330"', add
label define countynhg_lbl 4803350 `"4803350"', add
label define countynhg_lbl 4803370 `"4803370"', add
label define countynhg_lbl 4803390 `"4803390"', add
label define countynhg_lbl 4803410 `"4803410"', add
label define countynhg_lbl 4803430 `"4803430"', add
label define countynhg_lbl 4803450 `"4803450"', add
label define countynhg_lbl 4803470 `"4803470"', add
label define countynhg_lbl 4803490 `"4803490"', add
label define countynhg_lbl 4803510 `"4803510"', add
label define countynhg_lbl 4803530 `"4803530"', add
label define countynhg_lbl 4803550 `"4803550"', add
label define countynhg_lbl 4803570 `"4803570"', add
label define countynhg_lbl 4803590 `"4803590"', add
label define countynhg_lbl 4803610 `"4803610"', add
label define countynhg_lbl 4803630 `"4803630"', add
label define countynhg_lbl 4803650 `"4803650"', add
label define countynhg_lbl 4803670 `"4803670"', add
label define countynhg_lbl 4803690 `"4803690"', add
label define countynhg_lbl 4803710 `"4803710"', add
label define countynhg_lbl 4803730 `"4803730"', add
label define countynhg_lbl 4803750 `"4803750"', add
label define countynhg_lbl 4803770 `"4803770"', add
label define countynhg_lbl 4803790 `"4803790"', add
label define countynhg_lbl 4803810 `"4803810"', add
label define countynhg_lbl 4803830 `"4803830"', add
label define countynhg_lbl 4803850 `"4803850"', add
label define countynhg_lbl 4803870 `"4803870"', add
label define countynhg_lbl 4803890 `"4803890"', add
label define countynhg_lbl 4803910 `"4803910"', add
label define countynhg_lbl 4803930 `"4803930"', add
label define countynhg_lbl 4803950 `"4803950"', add
label define countynhg_lbl 4803970 `"4803970"', add
label define countynhg_lbl 4803990 `"4803990"', add
label define countynhg_lbl 4804010 `"4804010"', add
label define countynhg_lbl 4804030 `"4804030"', add
label define countynhg_lbl 4804050 `"4804050"', add
label define countynhg_lbl 4804070 `"4804070"', add
label define countynhg_lbl 4804090 `"4804090"', add
label define countynhg_lbl 4804110 `"4804110"', add
label define countynhg_lbl 4804130 `"4804130"', add
label define countynhg_lbl 4804150 `"4804150"', add
label define countynhg_lbl 4804170 `"4804170"', add
label define countynhg_lbl 4804190 `"4804190"', add
label define countynhg_lbl 4804210 `"4804210"', add
label define countynhg_lbl 4804230 `"4804230"', add
label define countynhg_lbl 4804250 `"4804250"', add
label define countynhg_lbl 4804270 `"4804270"', add
label define countynhg_lbl 4804290 `"4804290"', add
label define countynhg_lbl 4804310 `"4804310"', add
label define countynhg_lbl 4804330 `"4804330"', add
label define countynhg_lbl 4804350 `"4804350"', add
label define countynhg_lbl 4804370 `"4804370"', add
label define countynhg_lbl 4804390 `"4804390"', add
label define countynhg_lbl 4804410 `"4804410"', add
label define countynhg_lbl 4804430 `"4804430"', add
label define countynhg_lbl 4804450 `"4804450"', add
label define countynhg_lbl 4804470 `"4804470"', add
label define countynhg_lbl 4804490 `"4804490"', add
label define countynhg_lbl 4804510 `"4804510"', add
label define countynhg_lbl 4804530 `"4804530"', add
label define countynhg_lbl 4804550 `"4804550"', add
label define countynhg_lbl 4804570 `"4804570"', add
label define countynhg_lbl 4804590 `"4804590"', add
label define countynhg_lbl 4804610 `"4804610"', add
label define countynhg_lbl 4804630 `"4804630"', add
label define countynhg_lbl 4804650 `"4804650"', add
label define countynhg_lbl 4804670 `"4804670"', add
label define countynhg_lbl 4804690 `"4804690"', add
label define countynhg_lbl 4804710 `"4804710"', add
label define countynhg_lbl 4804730 `"4804730"', add
label define countynhg_lbl 4804750 `"4804750"', add
label define countynhg_lbl 4804770 `"4804770"', add
label define countynhg_lbl 4804790 `"4804790"', add
label define countynhg_lbl 4804810 `"4804810"', add
label define countynhg_lbl 4804830 `"4804830"', add
label define countynhg_lbl 4804850 `"4804850"', add
label define countynhg_lbl 4804870 `"4804870"', add
label define countynhg_lbl 4804890 `"4804890"', add
label define countynhg_lbl 4804910 `"4804910"', add
label define countynhg_lbl 4804930 `"4804930"', add
label define countynhg_lbl 4804950 `"4804950"', add
label define countynhg_lbl 4804970 `"4804970"', add
label define countynhg_lbl 4804990 `"4804990"', add
label define countynhg_lbl 4805010 `"4805010"', add
label define countynhg_lbl 4805030 `"4805030"', add
label define countynhg_lbl 4805050 `"4805050"', add
label define countynhg_lbl 4805070 `"4805070"', add
label define countynhg_lbl 4900010 `"4900010"', add
label define countynhg_lbl 4900030 `"4900030"', add
label define countynhg_lbl 4900050 `"4900050"', add
label define countynhg_lbl 4900070 `"4900070"', add
label define countynhg_lbl 4900090 `"4900090"', add
label define countynhg_lbl 4900110 `"4900110"', add
label define countynhg_lbl 4900130 `"4900130"', add
label define countynhg_lbl 4900150 `"4900150"', add
label define countynhg_lbl 4900170 `"4900170"', add
label define countynhg_lbl 4900190 `"4900190"', add
label define countynhg_lbl 4900210 `"4900210"', add
label define countynhg_lbl 4900230 `"4900230"', add
label define countynhg_lbl 4900250 `"4900250"', add
label define countynhg_lbl 4900270 `"4900270"', add
label define countynhg_lbl 4900290 `"4900290"', add
label define countynhg_lbl 4900310 `"4900310"', add
label define countynhg_lbl 4900330 `"4900330"', add
label define countynhg_lbl 4900350 `"4900350"', add
label define countynhg_lbl 4900370 `"4900370"', add
label define countynhg_lbl 4900390 `"4900390"', add
label define countynhg_lbl 4900410 `"4900410"', add
label define countynhg_lbl 4900430 `"4900430"', add
label define countynhg_lbl 4900450 `"4900450"', add
label define countynhg_lbl 4900470 `"4900470"', add
label define countynhg_lbl 4900490 `"4900490"', add
label define countynhg_lbl 4900510 `"4900510"', add
label define countynhg_lbl 4900530 `"4900530"', add
label define countynhg_lbl 4900550 `"4900550"', add
label define countynhg_lbl 4900570 `"4900570"', add
label define countynhg_lbl 4950015 `"4950015"', add
label define countynhg_lbl 4950035 `"4950035"', add
label define countynhg_lbl 4950055 `"4950055"', add
label define countynhg_lbl 4950075 `"4950075"', add
label define countynhg_lbl 4950095 `"4950095"', add
label define countynhg_lbl 4950115 `"4950115"', add
label define countynhg_lbl 4950155 `"4950155"', add
label define countynhg_lbl 4950215 `"4950215"', add
label define countynhg_lbl 4950235 `"4950235"', add
label define countynhg_lbl 4950255 `"4950255"', add
label define countynhg_lbl 4950275 `"4950275"', add
label define countynhg_lbl 4950295 `"4950295"', add
label define countynhg_lbl 4950315 `"4950315"', add
label define countynhg_lbl 4950335 `"4950335"', add
label define countynhg_lbl 4950375 `"4950375"', add
label define countynhg_lbl 4950395 `"4950395"', add
label define countynhg_lbl 4950415 `"4950415"', add
label define countynhg_lbl 4950435 `"4950435"', add
label define countynhg_lbl 4950495 `"4950495"', add
label define countynhg_lbl 4950515 `"4950515"', add
label define countynhg_lbl 4950535 `"4950535"', add
label define countynhg_lbl 4950555 `"4950555"', add
label define countynhg_lbl 4950575 `"4950575"', add
label define countynhg_lbl 4950595 `"4950595"', add
label define countynhg_lbl 4950605 `"4950605"', add
label define countynhg_lbl 4950615 `"4950615"', add
label define countynhg_lbl 4950635 `"4950635"', add
label define countynhg_lbl 4950675 `"4950675"', add
label define countynhg_lbl 4950715 `"4950715"', add
label define countynhg_lbl 4950735 `"4950735"', add
label define countynhg_lbl 4950755 `"4950755"', add
label define countynhg_lbl 5000010 `"5000010"', add
label define countynhg_lbl 5000030 `"5000030"', add
label define countynhg_lbl 5000050 `"5000050"', add
label define countynhg_lbl 5000070 `"5000070"', add
label define countynhg_lbl 5000090 `"5000090"', add
label define countynhg_lbl 5000110 `"5000110"', add
label define countynhg_lbl 5000130 `"5000130"', add
label define countynhg_lbl 5000150 `"5000150"', add
label define countynhg_lbl 5000170 `"5000170"', add
label define countynhg_lbl 5000190 `"5000190"', add
label define countynhg_lbl 5000210 `"5000210"', add
label define countynhg_lbl 5000230 `"5000230"', add
label define countynhg_lbl 5000250 `"5000250"', add
label define countynhg_lbl 5000270 `"5000270"', add
label define countynhg_lbl 5100010 `"5100010"', add
label define countynhg_lbl 5100030 `"5100030"', add
label define countynhg_lbl 5100035 `"5100035"', add
label define countynhg_lbl 5100050 `"5100050"', add
label define countynhg_lbl 5100070 `"5100070"', add
label define countynhg_lbl 5100090 `"5100090"', add
label define countynhg_lbl 5100110 `"5100110"', add
label define countynhg_lbl 5100130 `"5100130"', add
label define countynhg_lbl 5100150 `"5100150"', add
label define countynhg_lbl 5100155 `"5100155"', add
label define countynhg_lbl 5100170 `"5100170"', add
label define countynhg_lbl 5100190 `"5100190"', add
label define countynhg_lbl 5100195 `"5100195"', add
label define countynhg_lbl 5100210 `"5100210"', add
label define countynhg_lbl 5100215 `"5100215"', add
label define countynhg_lbl 5100230 `"5100230"', add
label define countynhg_lbl 5100233 `"5100233"', add
label define countynhg_lbl 5100237 `"5100237"', add
label define countynhg_lbl 5100250 `"5100250"', add
label define countynhg_lbl 5100270 `"5100270"', add
label define countynhg_lbl 5100290 `"5100290"', add
label define countynhg_lbl 5100293 `"5100293"', add
label define countynhg_lbl 5100297 `"5100297"', add
label define countynhg_lbl 5100310 `"5100310"', add
label define countynhg_lbl 5100330 `"5100330"', add
label define countynhg_lbl 5100350 `"5100350"', add
label define countynhg_lbl 5100360 `"5100360"', add
label define countynhg_lbl 5100370 `"5100370"', add
label define countynhg_lbl 5100410 `"5100410"', add
label define countynhg_lbl 5100430 `"5100430"', add
label define countynhg_lbl 5100435 `"5100435"', add
label define countynhg_lbl 5100450 `"5100450"', add
label define countynhg_lbl 5100470 `"5100470"', add
label define countynhg_lbl 5100490 `"5100490"', add
label define countynhg_lbl 5100510 `"5100510"', add
label define countynhg_lbl 5100530 `"5100530"', add
label define countynhg_lbl 5100533 `"5100533"', add
label define countynhg_lbl 5100550 `"5100550"', add
label define countynhg_lbl 5100570 `"5100570"', add
label define countynhg_lbl 5100590 `"5100590"', add
label define countynhg_lbl 5100610 `"5100610"', add
label define countynhg_lbl 5100615 `"5100615"', add
label define countynhg_lbl 5100630 `"5100630"', add
label define countynhg_lbl 5100650 `"5100650"', add
label define countynhg_lbl 5100670 `"5100670"', add
label define countynhg_lbl 5100690 `"5100690"', add
label define countynhg_lbl 5100710 `"5100710"', add
label define countynhg_lbl 5100715 `"5100715"', add
label define countynhg_lbl 5100730 `"5100730"', add
label define countynhg_lbl 5100750 `"5100750"', add
label define countynhg_lbl 5100770 `"5100770"', add
label define countynhg_lbl 5100775 `"5100775"', add
label define countynhg_lbl 5100790 `"5100790"', add
label define countynhg_lbl 5100810 `"5100810"', add
label define countynhg_lbl 5100830 `"5100830"', add
label define countynhg_lbl 5100833 `"5100833"', add
label define countynhg_lbl 5100837 `"5100837"', add
label define countynhg_lbl 5100850 `"5100850"', add
label define countynhg_lbl 5100853 `"5100853"', add
label define countynhg_lbl 5100857 `"5100857"', add
label define countynhg_lbl 5100870 `"5100870"', add
label define countynhg_lbl 5100890 `"5100890"', add
label define countynhg_lbl 5100910 `"5100910"', add
label define countynhg_lbl 5100930 `"5100930"', add
label define countynhg_lbl 5100935 `"5100935"', add
label define countynhg_lbl 5100950 `"5100950"', add
label define countynhg_lbl 5100953 `"5100953"', add
label define countynhg_lbl 5100957 `"5100957"', add
label define countynhg_lbl 5100970 `"5100970"', add
label define countynhg_lbl 5100990 `"5100990"', add
label define countynhg_lbl 5101010 `"5101010"', add
label define countynhg_lbl 5101030 `"5101030"', add
label define countynhg_lbl 5101050 `"5101050"', add
label define countynhg_lbl 5101053 `"5101053"', add
label define countynhg_lbl 5101057 `"5101057"', add
label define countynhg_lbl 5101070 `"5101070"', add
label define countynhg_lbl 5101090 `"5101090"', add
label define countynhg_lbl 5101110 `"5101110"', add
label define countynhg_lbl 5101130 `"5101130"', add
label define countynhg_lbl 5101133 `"5101133"', add
label define countynhg_lbl 5101135 `"5101135"', add
label define countynhg_lbl 5101137 `"5101137"', add
label define countynhg_lbl 5101150 `"5101150"', add
label define countynhg_lbl 5101155 `"5101155"', add
label define countynhg_lbl 5101170 `"5101170"', add
label define countynhg_lbl 5101175 `"5101175"', add
label define countynhg_lbl 5101190 `"5101190"', add
label define countynhg_lbl 5101193 `"5101193"', add
label define countynhg_lbl 5101197 `"5101197"', add
label define countynhg_lbl 5101210 `"5101210"', add
label define countynhg_lbl 5101215 `"5101215"', add
label define countynhg_lbl 5101230 `"5101230"', add
label define countynhg_lbl 5101250 `"5101250"', add
label define countynhg_lbl 5101270 `"5101270"', add
label define countynhg_lbl 5101275 `"5101275"', add
label define countynhg_lbl 5101290 `"5101290"', add
label define countynhg_lbl 5101310 `"5101310"', add
label define countynhg_lbl 5101330 `"5101330"', add
label define countynhg_lbl 5101350 `"5101350"', add
label define countynhg_lbl 5101355 `"5101355"', add
label define countynhg_lbl 5101370 `"5101370"', add
label define countynhg_lbl 5101390 `"5101390"', add
label define countynhg_lbl 5101410 `"5101410"', add
label define countynhg_lbl 5101415 `"5101415"', add
label define countynhg_lbl 5101430 `"5101430"', add
label define countynhg_lbl 5101433 `"5101433"', add
label define countynhg_lbl 5101437 `"5101437"', add
label define countynhg_lbl 5101450 `"5101450"', add
label define countynhg_lbl 5101455 `"5101455"', add
label define countynhg_lbl 5101470 `"5101470"', add
label define countynhg_lbl 5101490 `"5101490"', add
label define countynhg_lbl 5101510 `"5101510"', add
label define countynhg_lbl 5101530 `"5101530"', add
label define countynhg_lbl 5101550 `"5101550"', add
label define countynhg_lbl 5101553 `"5101553"', add
label define countynhg_lbl 5101555 `"5101555"', add
label define countynhg_lbl 5101557 `"5101557"', add
label define countynhg_lbl 5101570 `"5101570"', add
label define countynhg_lbl 5101590 `"5101590"', add
label define countynhg_lbl 5101593 `"5101593"', add
label define countynhg_lbl 5101597 `"5101597"', add
label define countynhg_lbl 5101610 `"5101610"', add
label define countynhg_lbl 5101630 `"5101630"', add
label define countynhg_lbl 5101650 `"5101650"', add
label define countynhg_lbl 5101670 `"5101670"', add
label define countynhg_lbl 5101690 `"5101690"', add
label define countynhg_lbl 5101710 `"5101710"', add
label define countynhg_lbl 5101730 `"5101730"', add
label define countynhg_lbl 5101750 `"5101750"', add
label define countynhg_lbl 5101770 `"5101770"', add
label define countynhg_lbl 5101790 `"5101790"', add
label define countynhg_lbl 5101810 `"5101810"', add
label define countynhg_lbl 5101830 `"5101830"', add
label define countynhg_lbl 5101835 `"5101835"', add
label define countynhg_lbl 5101850 `"5101850"', add
label define countynhg_lbl 5101853 `"5101853"', add
label define countynhg_lbl 5101855 `"5101855"', add
label define countynhg_lbl 5101857 `"5101857"', add
label define countynhg_lbl 5101870 `"5101870"', add
label define countynhg_lbl 5101890 `"5101890"', add
label define countynhg_lbl 5101910 `"5101910"', add
label define countynhg_lbl 5101913 `"5101913"', add
label define countynhg_lbl 5101917 `"5101917"', add
label define countynhg_lbl 5101930 `"5101930"', add
label define countynhg_lbl 5101933 `"5101933"', add
label define countynhg_lbl 5101937 `"5101937"', add
label define countynhg_lbl 5101950 `"5101950"', add
label define countynhg_lbl 5101953 `"5101953"', add
label define countynhg_lbl 5101957 `"5101957"', add
label define countynhg_lbl 5101970 `"5101970"', add
label define countynhg_lbl 5101990 `"5101990"', add
label define countynhg_lbl 5105100 `"5105100"', add
label define countynhg_lbl 5105200 `"5105200"', add
label define countynhg_lbl 5105300 `"5105300"', add
label define countynhg_lbl 5105400 `"5105400"', add
label define countynhg_lbl 5105600 `"5105600"', add
label define countynhg_lbl 5105900 `"5105900"', add
label define countynhg_lbl 5106300 `"5106300"', add
label define countynhg_lbl 5106500 `"5106500"', add
label define countynhg_lbl 5106600 `"5106600"', add
label define countynhg_lbl 5106700 `"5106700"', add
label define countynhg_lbl 5106800 `"5106800"', add
label define countynhg_lbl 5106855 `"5106855"', add
label define countynhg_lbl 5106900 `"5106900"', add
label define countynhg_lbl 5107000 `"5107000"', add
label define countynhg_lbl 5107100 `"5107100"', add
label define countynhg_lbl 5107300 `"5107300"', add
label define countynhg_lbl 5107400 `"5107400"', add
label define countynhg_lbl 5107500 `"5107500"', add
label define countynhg_lbl 5107600 `"5107600"', add
label define countynhg_lbl 5107700 `"5107700"', add
label define countynhg_lbl 5107805 `"5107805"', add
label define countynhg_lbl 5107900 `"5107900"', add
label define countynhg_lbl 5108000 `"5108000"', add
label define countynhg_lbl 5108300 `"5108300"', add
label define countynhg_lbl 5108400 `"5108400"', add
label define countynhg_lbl 5300010 `"5300010"', add
label define countynhg_lbl 5300030 `"5300030"', add
label define countynhg_lbl 5300050 `"5300050"', add
label define countynhg_lbl 5300055 `"5300055"', add
label define countynhg_lbl 5300070 `"5300070"', add
label define countynhg_lbl 5300090 `"5300090"', add
label define countynhg_lbl 5300110 `"5300110"', add
label define countynhg_lbl 5300130 `"5300130"', add
label define countynhg_lbl 5300150 `"5300150"', add
label define countynhg_lbl 5300170 `"5300170"', add
label define countynhg_lbl 5300190 `"5300190"', add
label define countynhg_lbl 5300210 `"5300210"', add
label define countynhg_lbl 5300230 `"5300230"', add
label define countynhg_lbl 5300250 `"5300250"', add
label define countynhg_lbl 5300270 `"5300270"', add
label define countynhg_lbl 5300290 `"5300290"', add
label define countynhg_lbl 5300310 `"5300310"', add
label define countynhg_lbl 5300330 `"5300330"', add
label define countynhg_lbl 5300350 `"5300350"', add
label define countynhg_lbl 5300370 `"5300370"', add
label define countynhg_lbl 5300390 `"5300390"', add
label define countynhg_lbl 5300410 `"5300410"', add
label define countynhg_lbl 5300430 `"5300430"', add
label define countynhg_lbl 5300450 `"5300450"', add
label define countynhg_lbl 5300470 `"5300470"', add
label define countynhg_lbl 5300490 `"5300490"', add
label define countynhg_lbl 5300510 `"5300510"', add
label define countynhg_lbl 5300530 `"5300530"', add
label define countynhg_lbl 5300550 `"5300550"', add
label define countynhg_lbl 5300570 `"5300570"', add
label define countynhg_lbl 5300590 `"5300590"', add
label define countynhg_lbl 5300610 `"5300610"', add
label define countynhg_lbl 5300630 `"5300630"', add
label define countynhg_lbl 5300650 `"5300650"', add
label define countynhg_lbl 5300670 `"5300670"', add
label define countynhg_lbl 5300690 `"5300690"', add
label define countynhg_lbl 5300710 `"5300710"', add
label define countynhg_lbl 5300730 `"5300730"', add
label define countynhg_lbl 5300750 `"5300750"', add
label define countynhg_lbl 5300770 `"5300770"', add
label define countynhg_lbl 5350015 `"5350015"', add
label define countynhg_lbl 5350035 `"5350035"', add
label define countynhg_lbl 5350055 `"5350055"', add
label define countynhg_lbl 5350075 `"5350075"', add
label define countynhg_lbl 5350095 `"5350095"', add
label define countynhg_lbl 5350115 `"5350115"', add
label define countynhg_lbl 5350135 `"5350135"', add
label define countynhg_lbl 5350155 `"5350155"', add
label define countynhg_lbl 5350175 `"5350175"', add
label define countynhg_lbl 5350195 `"5350195"', add
label define countynhg_lbl 5350215 `"5350215"', add
label define countynhg_lbl 5350235 `"5350235"', add
label define countynhg_lbl 5350255 `"5350255"', add
label define countynhg_lbl 5350275 `"5350275"', add
label define countynhg_lbl 5350295 `"5350295"', add
label define countynhg_lbl 5350315 `"5350315"', add
label define countynhg_lbl 5350355 `"5350355"', add
label define countynhg_lbl 5350375 `"5350375"', add
label define countynhg_lbl 5350395 `"5350395"', add
label define countynhg_lbl 5350415 `"5350415"', add
label define countynhg_lbl 5350435 `"5350435"', add
label define countynhg_lbl 5350455 `"5350455"', add
label define countynhg_lbl 5350475 `"5350475"', add
label define countynhg_lbl 5350495 `"5350495"', add
label define countynhg_lbl 5350515 `"5350515"', add
label define countynhg_lbl 5350535 `"5350535"', add
label define countynhg_lbl 5350555 `"5350555"', add
label define countynhg_lbl 5400010 `"5400010"', add
label define countynhg_lbl 5400030 `"5400030"', add
label define countynhg_lbl 5400050 `"5400050"', add
label define countynhg_lbl 5400070 `"5400070"', add
label define countynhg_lbl 5400090 `"5400090"', add
label define countynhg_lbl 5400110 `"5400110"', add
label define countynhg_lbl 5400130 `"5400130"', add
label define countynhg_lbl 5400150 `"5400150"', add
label define countynhg_lbl 5400170 `"5400170"', add
label define countynhg_lbl 5400190 `"5400190"', add
label define countynhg_lbl 5400210 `"5400210"', add
label define countynhg_lbl 5400230 `"5400230"', add
label define countynhg_lbl 5400250 `"5400250"', add
label define countynhg_lbl 5400270 `"5400270"', add
label define countynhg_lbl 5400290 `"5400290"', add
label define countynhg_lbl 5400310 `"5400310"', add
label define countynhg_lbl 5400330 `"5400330"', add
label define countynhg_lbl 5400350 `"5400350"', add
label define countynhg_lbl 5400370 `"5400370"', add
label define countynhg_lbl 5400390 `"5400390"', add
label define countynhg_lbl 5400410 `"5400410"', add
label define countynhg_lbl 5400430 `"5400430"', add
label define countynhg_lbl 5400450 `"5400450"', add
label define countynhg_lbl 5400470 `"5400470"', add
label define countynhg_lbl 5400490 `"5400490"', add
label define countynhg_lbl 5400510 `"5400510"', add
label define countynhg_lbl 5400530 `"5400530"', add
label define countynhg_lbl 5400550 `"5400550"', add
label define countynhg_lbl 5400570 `"5400570"', add
label define countynhg_lbl 5400590 `"5400590"', add
label define countynhg_lbl 5400610 `"5400610"', add
label define countynhg_lbl 5400630 `"5400630"', add
label define countynhg_lbl 5400650 `"5400650"', add
label define countynhg_lbl 5400670 `"5400670"', add
label define countynhg_lbl 5400690 `"5400690"', add
label define countynhg_lbl 5400710 `"5400710"', add
label define countynhg_lbl 5400730 `"5400730"', add
label define countynhg_lbl 5400750 `"5400750"', add
label define countynhg_lbl 5400770 `"5400770"', add
label define countynhg_lbl 5400790 `"5400790"', add
label define countynhg_lbl 5400810 `"5400810"', add
label define countynhg_lbl 5400830 `"5400830"', add
label define countynhg_lbl 5400850 `"5400850"', add
label define countynhg_lbl 5400870 `"5400870"', add
label define countynhg_lbl 5400890 `"5400890"', add
label define countynhg_lbl 5400910 `"5400910"', add
label define countynhg_lbl 5400930 `"5400930"', add
label define countynhg_lbl 5400950 `"5400950"', add
label define countynhg_lbl 5400970 `"5400970"', add
label define countynhg_lbl 5400990 `"5400990"', add
label define countynhg_lbl 5401010 `"5401010"', add
label define countynhg_lbl 5401030 `"5401030"', add
label define countynhg_lbl 5401050 `"5401050"', add
label define countynhg_lbl 5401070 `"5401070"', add
label define countynhg_lbl 5401090 `"5401090"', add
label define countynhg_lbl 5500010 `"5500010"', add
label define countynhg_lbl 5500030 `"5500030"', add
label define countynhg_lbl 5500035 `"5500035"', add
label define countynhg_lbl 5500050 `"5500050"', add
label define countynhg_lbl 5500070 `"5500070"', add
label define countynhg_lbl 5500090 `"5500090"', add
label define countynhg_lbl 5500110 `"5500110"', add
label define countynhg_lbl 5500130 `"5500130"', add
label define countynhg_lbl 5500150 `"5500150"', add
label define countynhg_lbl 5500170 `"5500170"', add
label define countynhg_lbl 5500190 `"5500190"', add
label define countynhg_lbl 5500210 `"5500210"', add
label define countynhg_lbl 5500230 `"5500230"', add
label define countynhg_lbl 5500235 `"5500235"', add
label define countynhg_lbl 5500250 `"5500250"', add
label define countynhg_lbl 5500270 `"5500270"', add
label define countynhg_lbl 5500290 `"5500290"', add
label define countynhg_lbl 5500310 `"5500310"', add
label define countynhg_lbl 5500330 `"5500330"', add
label define countynhg_lbl 5500350 `"5500350"', add
label define countynhg_lbl 5500370 `"5500370"', add
label define countynhg_lbl 5500390 `"5500390"', add
label define countynhg_lbl 5500410 `"5500410"', add
label define countynhg_lbl 5500430 `"5500430"', add
label define countynhg_lbl 5500450 `"5500450"', add
label define countynhg_lbl 5500470 `"5500470"', add
label define countynhg_lbl 5500490 `"5500490"', add
label define countynhg_lbl 5500510 `"5500510"', add
label define countynhg_lbl 5500530 `"5500530"', add
label define countynhg_lbl 5500550 `"5500550"', add
label define countynhg_lbl 5500570 `"5500570"', add
label define countynhg_lbl 5500590 `"5500590"', add
label define countynhg_lbl 5500610 `"5500610"', add
label define countynhg_lbl 5500630 `"5500630"', add
label define countynhg_lbl 5500650 `"5500650"', add
label define countynhg_lbl 5500670 `"5500670"', add
label define countynhg_lbl 5500675 `"5500675"', add
label define countynhg_lbl 5500690 `"5500690"', add
label define countynhg_lbl 5500710 `"5500710"', add
label define countynhg_lbl 5500730 `"5500730"', add
label define countynhg_lbl 5500750 `"5500750"', add
label define countynhg_lbl 5500770 `"5500770"', add
label define countynhg_lbl 5500790 `"5500790"', add
label define countynhg_lbl 5500810 `"5500810"', add
label define countynhg_lbl 5500830 `"5500830"', add
label define countynhg_lbl 5500850 `"5500850"', add
label define countynhg_lbl 5500870 `"5500870"', add
label define countynhg_lbl 5500890 `"5500890"', add
label define countynhg_lbl 5500910 `"5500910"', add
label define countynhg_lbl 5500930 `"5500930"', add
label define countynhg_lbl 5500950 `"5500950"', add
label define countynhg_lbl 5500970 `"5500970"', add
label define countynhg_lbl 5500990 `"5500990"', add
label define countynhg_lbl 5501010 `"5501010"', add
label define countynhg_lbl 5501030 `"5501030"', add
label define countynhg_lbl 5501050 `"5501050"', add
label define countynhg_lbl 5501070 `"5501070"', add
label define countynhg_lbl 5501090 `"5501090"', add
label define countynhg_lbl 5501110 `"5501110"', add
label define countynhg_lbl 5501130 `"5501130"', add
label define countynhg_lbl 5501150 `"5501150"', add
label define countynhg_lbl 5501170 `"5501170"', add
label define countynhg_lbl 5501190 `"5501190"', add
label define countynhg_lbl 5501210 `"5501210"', add
label define countynhg_lbl 5501230 `"5501230"', add
label define countynhg_lbl 5501250 `"5501250"', add
label define countynhg_lbl 5501270 `"5501270"', add
label define countynhg_lbl 5501290 `"5501290"', add
label define countynhg_lbl 5501310 `"5501310"', add
label define countynhg_lbl 5501330 `"5501330"', add
label define countynhg_lbl 5501350 `"5501350"', add
label define countynhg_lbl 5501370 `"5501370"', add
label define countynhg_lbl 5501390 `"5501390"', add
label define countynhg_lbl 5501410 `"5501410"', add
label define countynhg_lbl 5600010 `"5600010"', add
label define countynhg_lbl 5600030 `"5600030"', add
label define countynhg_lbl 5600050 `"5600050"', add
label define countynhg_lbl 5600070 `"5600070"', add
label define countynhg_lbl 5600090 `"5600090"', add
label define countynhg_lbl 5600110 `"5600110"', add
label define countynhg_lbl 5600130 `"5600130"', add
label define countynhg_lbl 5600150 `"5600150"', add
label define countynhg_lbl 5600170 `"5600170"', add
label define countynhg_lbl 5600190 `"5600190"', add
label define countynhg_lbl 5600210 `"5600210"', add
label define countynhg_lbl 5600230 `"5600230"', add
label define countynhg_lbl 5600250 `"5600250"', add
label define countynhg_lbl 5600270 `"5600270"', add
label define countynhg_lbl 5600290 `"5600290"', add
label define countynhg_lbl 5600310 `"5600310"', add
label define countynhg_lbl 5600330 `"5600330"', add
label define countynhg_lbl 5600350 `"5600350"', add
label define countynhg_lbl 5600370 `"5600370"', add
label define countynhg_lbl 5600390 `"5600390"', add
label define countynhg_lbl 5600410 `"5600410"', add
label define countynhg_lbl 5600430 `"5600430"', add
label define countynhg_lbl 5600450 `"5600450"', add
label define countynhg_lbl 5600470 `"5600470"', add
label define countynhg_lbl 5650015 `"5650015"', add
label define countynhg_lbl 5650035 `"5650035"', add
label define countynhg_lbl 5650055 `"5650055"', add
label define countynhg_lbl 5650095 `"5650095"', add
label define countynhg_lbl 5650115 `"5650115"', add
label define countynhg_lbl 5650135 `"5650135"', add
label define countynhg_lbl 9999999 `"9999999"', add
label values countynhg countynhg_lbl

label define urban_lbl 0 `"N/A"'
label define urban_lbl 1 `"Rural"', add
label define urban_lbl 2 `"Urban"', add
label values urban urban_lbl

label define metro_lbl 0 `"Metropolitan status indeterminable (mixed)"'
label define metro_lbl 1 `"Not in metropolitan area"', add
label define metro_lbl 2 `"In metropolitan area: In central/principal city"', add
label define metro_lbl 3 `"In metropolitan area: Not in central/principal city"', add
label define metro_lbl 4 `"In metropolitan area: Central/principal city status indeterminable (mixed)"', add
label values metro metro_lbl

label define metarea_lbl 000 `"Not identifiable or not in an MSA"'
label define metarea_lbl 004 `"Abilene, TX"', add
label define metarea_lbl 006 `"Aguadilla, PR"', add
label define metarea_lbl 008 `"Akron, OH"', add
label define metarea_lbl 012 `"Albany, GA"', add
label define metarea_lbl 016 `"Albany-Schenectady-Troy, NY"', add
label define metarea_lbl 020 `"Albuquerque, NM"', add
label define metarea_lbl 022 `"Alexandria, LA"', add
label define metarea_lbl 024 `"Allentown-Bethlehem-Easton, PA/NJ"', add
label define metarea_lbl 028 `"Altoona, PA"', add
label define metarea_lbl 032 `"Amarillo, TX"', add
label define metarea_lbl 038 `"Anchorage, AK"', add
label define metarea_lbl 040 `"Anderson, IN"', add
label define metarea_lbl 044 `"Ann Arbor, MI"', add
label define metarea_lbl 045 `"Anniston, AL"', add
label define metarea_lbl 046 `"Appleton-Oshkosh-Neenah, WI"', add
label define metarea_lbl 047 `"Arecibo, PR"', add
label define metarea_lbl 048 `"Asheville, NC"', add
label define metarea_lbl 050 `"Athens, GA"', add
label define metarea_lbl 052 `"Atlanta, GA"', add
label define metarea_lbl 056 `"Atlantic City, NJ"', add
label define metarea_lbl 058 `"Auburn-Opekika, AL"', add
label define metarea_lbl 060 `"Augusta-Aiken, GA/SC"', add
label define metarea_lbl 064 `"Austin, TX"', add
label define metarea_lbl 068 `"Bakersfield, CA"', add
label define metarea_lbl 072 `"Baltimore, MD"', add
label define metarea_lbl 073 `"Bangor, ME"', add
label define metarea_lbl 074 `"Barnstable-Yarmouth, MA"', add
label define metarea_lbl 076 `"Baton Rouge, LA"', add
label define metarea_lbl 078 `"Battle Creek, MI"', add
label define metarea_lbl 084 `"Beaumont-Port Arthur-Orange, TX"', add
label define metarea_lbl 086 `"Bellingham, WA"', add
label define metarea_lbl 087 `"Benton Harbor, MI"', add
label define metarea_lbl 088 `"Billings, MT"', add
label define metarea_lbl 092 `"Biloxi-Gulfport, MS"', add
label define metarea_lbl 096 `"Binghamton, NY"', add
label define metarea_lbl 100 `"Birmingham, AL"', add
label define metarea_lbl 102 `"Bloomington, IN"', add
label define metarea_lbl 104 `"Bloomington-Normal, IL"', add
label define metarea_lbl 108 `"Boise City, ID"', add
label define metarea_lbl 112 `"Boston, MA/NH"', add
label define metarea_lbl 114 `"Bradenton, FL"', add
label define metarea_lbl 115 `"Bremerton, WA"', add
label define metarea_lbl 116 `"Bridgeport, CT"', add
label define metarea_lbl 120 `"Brockton, MA"', add
label define metarea_lbl 124 `"Brownsville-Harlingen-San Benito, TX"', add
label define metarea_lbl 126 `"Bryan-College Station, TX"', add
label define metarea_lbl 128 `"Buffalo-Niagara Falls, NY"', add
label define metarea_lbl 130 `"Burlington, NC"', add
label define metarea_lbl 131 `"Burlington, VT"', add
label define metarea_lbl 132 `"Canton, OH"', add
label define metarea_lbl 133 `"Caguas, PR"', add
label define metarea_lbl 135 `"Casper, WY"', add
label define metarea_lbl 136 `"Cedar Rapids, IA"', add
label define metarea_lbl 140 `"Champaign-Urbana-Rantoul, IL"', add
label define metarea_lbl 144 `"Charleston-N. Charleston, SC"', add
label define metarea_lbl 148 `"Charleston, WV"', add
label define metarea_lbl 152 `"Charlotte-Gastonia-Rock Hill, NC/SC"', add
label define metarea_lbl 154 `"Charlottesville, VA"', add
label define metarea_lbl 156 `"Chattanooga, TN/GA"', add
label define metarea_lbl 158 `"Cheyenne, WY"', add
label define metarea_lbl 160 `"Chicago, IL"', add
label define metarea_lbl 162 `"Chico, CA"', add
label define metarea_lbl 164 `"Cincinnati-Hamilton, OH/KY/IN"', add
label define metarea_lbl 166 `"Clarksville- Hopkinsville, TN/KY"', add
label define metarea_lbl 168 `"Cleveland, OH"', add
label define metarea_lbl 172 `"Colorado Springs, CO"', add
label define metarea_lbl 174 `"Columbia, MO"', add
label define metarea_lbl 176 `"Columbia, SC"', add
label define metarea_lbl 180 `"Columbus, GA/AL"', add
label define metarea_lbl 184 `"Columbus, OH"', add
label define metarea_lbl 188 `"Corpus Christi, TX"', add
label define metarea_lbl 190 `"Cumberland, MD/WV"', add
label define metarea_lbl 192 `"Dallas-Fort Worth, TX"', add
label define metarea_lbl 193 `"Danbury, CT"', add
label define metarea_lbl 195 `"Danville, VA"', add
label define metarea_lbl 196 `"Davenport, IA - Rock Island-Moline, IL"', add
label define metarea_lbl 200 `"Dayton-Springfield, OH"', add
label define metarea_lbl 202 `"Daytona Beach, FL"', add
label define metarea_lbl 203 `"Decatur, AL"', add
label define metarea_lbl 204 `"Decatur, IL"', add
label define metarea_lbl 208 `"Denver-Boulder, CO"', add
label define metarea_lbl 212 `"Des Moines, IA"', add
label define metarea_lbl 216 `"Detroit, MI"', add
label define metarea_lbl 218 `"Dothan, AL"', add
label define metarea_lbl 219 `"Dover, DE"', add
label define metarea_lbl 220 `"Dubuque, IA"', add
label define metarea_lbl 224 `"Duluth-Superior, MN/WI"', add
label define metarea_lbl 228 `"Dutchess Co., NY"', add
label define metarea_lbl 229 `"Eau Claire, WI"', add
label define metarea_lbl 231 `"El Paso, TX"', add
label define metarea_lbl 232 `"Elkhart-Goshen, IN"', add
label define metarea_lbl 233 `"Elmira, NY"', add
label define metarea_lbl 234 `"Enid, OK"', add
label define metarea_lbl 236 `"Erie, PA"', add
label define metarea_lbl 240 `"Eugene-Springfield, OR"', add
label define metarea_lbl 244 `"Evansville, IN/KY"', add
label define metarea_lbl 252 `"Fargo-Morehead, ND/MN"', add
label define metarea_lbl 256 `"Fayetteville, NC"', add
label define metarea_lbl 258 `"Fayetteville-Springdale, AR"', add
label define metarea_lbl 260 `"Fitchburg-Leominster, MA"', add
label define metarea_lbl 262 `"Flagstaff, AZ/UT"', add
label define metarea_lbl 264 `"Flint, MI"', add
label define metarea_lbl 265 `"Florence, AL"', add
label define metarea_lbl 266 `"Florence, SC"', add
label define metarea_lbl 267 `"Fort Collins-Loveland, CO"', add
label define metarea_lbl 268 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
label define metarea_lbl 270 `"Fort Myers-Cape Coral, FL"', add
label define metarea_lbl 271 `"Fort Pierce, FL"', add
label define metarea_lbl 272 `"Fort Smith, AR/OK"', add
label define metarea_lbl 275 `"Fort Walton Beach, FL"', add
label define metarea_lbl 276 `"Fort Wayne, IN"', add
label define metarea_lbl 284 `"Fresno, CA"', add
label define metarea_lbl 288 `"Gadsden, AL"', add
label define metarea_lbl 290 `"Gainesville, FL"', add
label define metarea_lbl 292 `"Galveston-Texas City, TX"', add
label define metarea_lbl 297 `"Glens Falls, NY"', add
label define metarea_lbl 298 `"Goldsboro, NC"', add
label define metarea_lbl 299 `"Grand Forks, ND"', add
label define metarea_lbl 300 `"Grand Rapids, MI"', add
label define metarea_lbl 301 `"Grand Junction, CO"', add
label define metarea_lbl 304 `"Great Falls, MT"', add
label define metarea_lbl 306 `"Greeley, CO"', add
label define metarea_lbl 308 `"Green Bay, WI"', add
label define metarea_lbl 312 `"Greensboro-Winston Salem-High Point, NC"', add
label define metarea_lbl 315 `"Greenville, NC"', add
label define metarea_lbl 316 `"Greenville-Spartenburg-Anderson, SC"', add
label define metarea_lbl 318 `"Hagerstown, MD"', add
label define metarea_lbl 320 `"Hamilton-Middleton, OH"', add
label define metarea_lbl 324 `"Harrisburg-Lebanon--Carlisle, PA"', add
label define metarea_lbl 328 `"Hartford-Bristol-Middleton- New Britain, CT"', add
label define metarea_lbl 329 `"Hickory-Morganton, NC"', add
label define metarea_lbl 330 `"Hattiesburg, MS"', add
label define metarea_lbl 332 `"Honolulu, HI"', add
label define metarea_lbl 335 `"Houma-Thibodoux, LA"', add
label define metarea_lbl 336 `"Houston-Brazoria, TX"', add
label define metarea_lbl 340 `"Huntington-Ashland, WV/KY/OH"', add
label define metarea_lbl 344 `"Huntsville, AL"', add
label define metarea_lbl 348 `"Indianapolis, IN"', add
label define metarea_lbl 350 `"Iowa City, IA"', add
label define metarea_lbl 352 `"Jackson, MI"', add
label define metarea_lbl 356 `"Jackson, MS"', add
label define metarea_lbl 358 `"Jackson, TN"', add
label define metarea_lbl 359 `"Jacksonville, FL"', add
label define metarea_lbl 360 `"Jacksonville, NC"', add
label define metarea_lbl 361 `"Jamestown-Dunkirk, NY"', add
label define metarea_lbl 362 `"Janesville-Beloit, WI"', add
label define metarea_lbl 366 `"Johnson City-Kingsport--Bristol, TN/VA"', add
label define metarea_lbl 368 `"Johnstown, PA"', add
label define metarea_lbl 371 `"Joplin, MO"', add
label define metarea_lbl 372 `"Kalamazoo-Portage, MI"', add
label define metarea_lbl 374 `"Kankakee, IL"', add
label define metarea_lbl 376 `"Kansas City, MO/KS"', add
label define metarea_lbl 380 `"Kenosha, WI"', add
label define metarea_lbl 381 `"Kileen-Temple, TX"', add
label define metarea_lbl 384 `"Knoxville, TN"', add
label define metarea_lbl 385 `"Kokomo, IN"', add
label define metarea_lbl 387 `"LaCrosse, WI"', add
label define metarea_lbl 388 `"Lafayette, LA"', add
label define metarea_lbl 392 `"Lafayette-W. Lafayette, IN"', add
label define metarea_lbl 396 `"Lake Charles, LA"', add
label define metarea_lbl 398 `"Lakeland-Winterhaven, FL"', add
label define metarea_lbl 400 `"Lancaster, PA"', add
label define metarea_lbl 404 `"Lansing-E. Lansing, MI"', add
label define metarea_lbl 408 `"Laredo, TX"', add
label define metarea_lbl 410 `"Las Cruces, NM"', add
label define metarea_lbl 412 `"Las Vegas, NV"', add
label define metarea_lbl 415 `"Lawrence, KS"', add
label define metarea_lbl 420 `"Lawton, OK"', add
label define metarea_lbl 424 `"Lewiston-Auburn, ME"', add
label define metarea_lbl 428 `"Lexington-Fayette, KY"', add
label define metarea_lbl 432 `"Lima, OH"', add
label define metarea_lbl 436 `"Lincoln, NE"', add
label define metarea_lbl 440 `"Little Rock-N. Little Rock, AR"', add
label define metarea_lbl 441 `"Long Branch-Asbury Park, NJ"', add
label define metarea_lbl 442 `"Longview-Marshall, TX"', add
label define metarea_lbl 444 `"Lorain-Elyria, OH"', add
label define metarea_lbl 448 `"Los Angeles-Long Beach, CA"', add
label define metarea_lbl 452 `"Louisville, KY/IN"', add
label define metarea_lbl 460 `"Lubbock, TX"', add
label define metarea_lbl 464 `"Lynchburg, VA"', add
label define metarea_lbl 468 `"Macon-Warner Robins, GA"', add
label define metarea_lbl 472 `"Madison, WI"', add
label define metarea_lbl 476 `"Manchester, NH"', add
label define metarea_lbl 480 `"Mansfield, OH"', add
label define metarea_lbl 484 `"Mayaguez, PR"', add
label define metarea_lbl 488 `"McAllen-Edinburg-Pharr-Mission, TX"', add
label define metarea_lbl 489 `"Medford, OR"', add
label define metarea_lbl 490 `"Melbourne-Titusville-Cocoa-Palm Bay, FL"', add
label define metarea_lbl 492 `"Memphis, TN/AR/MS"', add
label define metarea_lbl 494 `"Merced, CA"', add
label define metarea_lbl 500 `"Miami-Hialeah, FL"', add
label define metarea_lbl 504 `"Midland, TX"', add
label define metarea_lbl 508 `"Milwaukee, WI"', add
label define metarea_lbl 512 `"Minneapolis-St. Paul, MN"', add
label define metarea_lbl 514 `"Missoula, MT"', add
label define metarea_lbl 516 `"Mobile, AL"', add
label define metarea_lbl 517 `"Modesto, CA"', add
label define metarea_lbl 519 `"Monmouth-Ocean, NJ"', add
label define metarea_lbl 520 `"Monroe, LA"', add
label define metarea_lbl 524 `"Montgomery, AL"', add
label define metarea_lbl 528 `"Muncie, IN"', add
label define metarea_lbl 532 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
label define metarea_lbl 533 `"Myrtle Beach, SC"', add
label define metarea_lbl 534 `"Naples, FL"', add
label define metarea_lbl 535 `"Nashua, NH"', add
label define metarea_lbl 536 `"Nashville, TN"', add
label define metarea_lbl 540 `"New Bedford, MA"', add
label define metarea_lbl 546 `"New Brunswick-Perth Amboy-Sayreville, NJ"', add
label define metarea_lbl 548 `"New Haven-Meriden, CT"', add
label define metarea_lbl 552 `"New London-Norwich, CT/RI"', add
label define metarea_lbl 556 `"New Orleans, LA"', add
label define metarea_lbl 560 `"New York, NY-Northeastern NJ"', add
label define metarea_lbl 564 `"Newark, OH"', add
label define metarea_lbl 566 `"Newburgh-Middletown, NY"', add
label define metarea_lbl 572 `"Norfolk-VA Beach--Newport News, VA"', add
label define metarea_lbl 576 `"Norwalk, CT"', add
label define metarea_lbl 579 `"Ocala, FL"', add
label define metarea_lbl 580 `"Odessa, TX"', add
label define metarea_lbl 588 `"Oklahoma City, OK"', add
label define metarea_lbl 591 `"Olympia, WA"', add
label define metarea_lbl 592 `"Omaha, NE/IA"', add
label define metarea_lbl 595 `"Orange, NY"', add
label define metarea_lbl 596 `"Orlando, FL"', add
label define metarea_lbl 599 `"Owensboro, KY"', add
label define metarea_lbl 601 `"Panama City, FL"', add
label define metarea_lbl 602 `"Parkersburg-Marietta,WV/OH"', add
label define metarea_lbl 603 `"Pascagoula-Moss Point, MS"', add
label define metarea_lbl 608 `"Pensacola, FL"', add
label define metarea_lbl 612 `"Peoria, IL"', add
label define metarea_lbl 616 `"Philadelphia, PA/NJ"', add
label define metarea_lbl 620 `"Phoenix, AZ"', add
label define metarea_lbl 628 `"Pittsburgh, PA"', add
label define metarea_lbl 632 `"Pittsfield, MA"', add
label define metarea_lbl 636 `"Ponce, PR"', add
label define metarea_lbl 640 `"Portland, ME"', add
label define metarea_lbl 644 `"Portland, OR/WA"', add
label define metarea_lbl 645 `"Portsmouth-Dover--Rochester, NH/ME"', add
label define metarea_lbl 646 `"Poughkeepsie, NY"', add
label define metarea_lbl 648 `"Providence-Fall River-Pawtucket, MA/RI"', add
label define metarea_lbl 652 `"Provo-Orem, UT"', add
label define metarea_lbl 656 `"Pueblo, CO"', add
label define metarea_lbl 658 `"Punta Gorda, FL"', add
label define metarea_lbl 660 `"Racine, WI"', add
label define metarea_lbl 664 `"Raleigh-Durham, NC"', add
label define metarea_lbl 666 `"Rapid City, SD"', add
label define metarea_lbl 668 `"Reading, PA"', add
label define metarea_lbl 669 `"Redding, CA"', add
label define metarea_lbl 672 `"Reno, NV"', add
label define metarea_lbl 674 `"Richland-Kennewick-Pasco, WA"', add
label define metarea_lbl 676 `"Richmond-Petersburg, VA"', add
label define metarea_lbl 678 `"Riverside-San Bernardino, CA"', add
label define metarea_lbl 680 `"Roanoke, VA"', add
label define metarea_lbl 682 `"Rochester, MN"', add
label define metarea_lbl 684 `"Rochester, NY"', add
label define metarea_lbl 688 `"Rockford, IL"', add
label define metarea_lbl 689 `"Rocky Mount, NC"', add
label define metarea_lbl 692 `"Sacramento, CA"', add
label define metarea_lbl 696 `"Saginaw-Bay City-Midland, MI"', add
label define metarea_lbl 698 `"St. Cloud, MN"', add
label define metarea_lbl 700 `"St. Joseph, MO"', add
label define metarea_lbl 704 `"St. Louis, MO/IL"', add
label define metarea_lbl 708 `"Salem, OR"', add
label define metarea_lbl 712 `"Salinas-Sea Side-Monterey, CA"', add
label define metarea_lbl 714 `"Salisbury-Concord, NC"', add
label define metarea_lbl 716 `"Salt Lake City-Ogden, UT"', add
label define metarea_lbl 720 `"San Angelo, TX"', add
label define metarea_lbl 724 `"San Antonio, TX"', add
label define metarea_lbl 732 `"San Diego, CA"', add
label define metarea_lbl 736 `"San Francisco-Oakland-Vallejo, CA"', add
label define metarea_lbl 740 `"San Jose, CA"', add
label define metarea_lbl 744 `"San Juan-Bayamon, PR"', add
label define metarea_lbl 746 `"San Luis Obispo-Atascad-P Robles, CA"', add
label define metarea_lbl 747 `"Santa Barbara-Santa Maria-Lompoc, CA"', add
label define metarea_lbl 748 `"Santa Cruz, CA"', add
label define metarea_lbl 749 `"Santa Fe, NM"', add
label define metarea_lbl 750 `"Santa Rosa-Petaluma, CA"', add
label define metarea_lbl 751 `"Sarasota, FL"', add
label define metarea_lbl 752 `"Savannah, GA"', add
label define metarea_lbl 756 `"Scranton-Wilkes-Barre, PA"', add
label define metarea_lbl 760 `"Seattle-Everett, WA"', add
label define metarea_lbl 761 `"Sharon, PA"', add
label define metarea_lbl 762 `"Sheboygan, WI"', add
label define metarea_lbl 764 `"Sherman-Davidson, TX"', add
label define metarea_lbl 768 `"Shreveport, LA"', add
label define metarea_lbl 772 `"Sioux City, IA/NE"', add
label define metarea_lbl 776 `"Sioux Falls, SD"', add
label define metarea_lbl 780 `"South Bend-Mishawaka, IN"', add
label define metarea_lbl 784 `"Spokane, WA"', add
label define metarea_lbl 788 `"Springfield, IL"', add
label define metarea_lbl 792 `"Springfield, MO"', add
label define metarea_lbl 800 `"Springfield-Holyoke-Chicopee, MA"', add
label define metarea_lbl 804 `"Stamford, CT"', add
label define metarea_lbl 805 `"State College, PA"', add
label define metarea_lbl 808 `"Steubenville-Weirton,OH/WV"', add
label define metarea_lbl 812 `"Stockton, CA"', add
label define metarea_lbl 814 `"Sumter, SC"', add
label define metarea_lbl 816 `"Syracuse, NY"', add
label define metarea_lbl 820 `"Tacoma, WA"', add
label define metarea_lbl 824 `"Tallahassee, FL"', add
label define metarea_lbl 828 `"Tampa-St. Petersburg-Clearwater, FL"', add
label define metarea_lbl 832 `"Terre Haute, IN"', add
label define metarea_lbl 836 `"Texarkana, TX/AR"', add
label define metarea_lbl 840 `"Toledo, OH/MI"', add
label define metarea_lbl 844 `"Topeka, KS"', add
label define metarea_lbl 848 `"Trenton, NJ"', add
label define metarea_lbl 852 `"Tucson, AZ"', add
label define metarea_lbl 856 `"Tulsa, OK"', add
label define metarea_lbl 860 `"Tuscaloosa, AL"', add
label define metarea_lbl 864 `"Tyler, TX"', add
label define metarea_lbl 868 `"Utica-Rome, NY"', add
label define metarea_lbl 873 `"Ventura-Oxnard-Simi Valley, CA"', add
label define metarea_lbl 875 `"Victoria, TX"', add
label define metarea_lbl 876 `"Vineland-Milville-Bridgetown, NJ"', add
label define metarea_lbl 878 `"Visalia-Tulare-Porterville, CA"', add
label define metarea_lbl 880 `"Waco, TX"', add
label define metarea_lbl 884 `"Washington, DC/MD/VA"', add
label define metarea_lbl 888 `"Waterbury, CT"', add
label define metarea_lbl 892 `"Waterloo-Cedar Falls, IA"', add
label define metarea_lbl 894 `"Wausau, WI"', add
label define metarea_lbl 896 `"West Palm Beach-Boca Raton-Delray Beach, FL"', add
label define metarea_lbl 900 `"Wheeling, WV/OH"', add
label define metarea_lbl 904 `"Wichita, KS"', add
label define metarea_lbl 908 `"Wichita Falls, TX"', add
label define metarea_lbl 914 `"Williamsport, PA"', add
label define metarea_lbl 916 `"Wilmington, DE/NJ/MD"', add
label define metarea_lbl 920 `"Wilmington, NC"', add
label define metarea_lbl 924 `"Worcester, MA"', add
label define metarea_lbl 926 `"Yakima, WA"', add
label define metarea_lbl 927 `"Yolo, CA"', add
label define metarea_lbl 928 `"York, PA"', add
label define metarea_lbl 932 `"Youngstown-Warren, OH/PA"', add
label define metarea_lbl 934 `"Yuba City, CA"', add
label define metarea_lbl 936 `"Yuma, AZ"', add
label values metarea metarea_lbl

label define metaread_lbl 0000 `"Not identifiable or not in an MSA"'
label define metaread_lbl 0040 `"Abilene, TX"', add
label define metaread_lbl 0060 `"Aguadilla, PR"', add
label define metaread_lbl 0080 `"Akron, OH"', add
label define metaread_lbl 0120 `"Albany, GA"', add
label define metaread_lbl 0160 `"Albany-Schenectady-Troy, NY"', add
label define metaread_lbl 0200 `"Albuquerque, NM"', add
label define metaread_lbl 0220 `"Alexandria, LA"', add
label define metaread_lbl 0240 `"Allentown-Bethlehem-Easton, PA/NJ"', add
label define metaread_lbl 0280 `"Altoona, PA"', add
label define metaread_lbl 0320 `"Amarillo, TX"', add
label define metaread_lbl 0380 `"Anchorage, AK"', add
label define metaread_lbl 0400 `"Anderson, IN"', add
label define metaread_lbl 0440 `"Ann Arbor, MI"', add
label define metaread_lbl 0450 `"Anniston, AL"', add
label define metaread_lbl 0460 `"Appleton-Oshkosh-Neenah, WI"', add
label define metaread_lbl 0470 `"Arecibo, PR"', add
label define metaread_lbl 0480 `"Asheville, NC"', add
label define metaread_lbl 0500 `"Athens, GA"', add
label define metaread_lbl 0520 `"Atlanta, GA"', add
label define metaread_lbl 0560 `"Atlantic City, NJ"', add
label define metaread_lbl 0580 `"Auburn-Opelika, AL"', add
label define metaread_lbl 0600 `"Augusta-Aiken, GA/SC"', add
label define metaread_lbl 0640 `"Austin, TX"', add
label define metaread_lbl 0680 `"Bakersfield, CA"', add
label define metaread_lbl 0720 `"Baltimore, MD"', add
label define metaread_lbl 0730 `"Bangor, ME"', add
label define metaread_lbl 0740 `"Barnstable-Yarmouth, MA"', add
label define metaread_lbl 0760 `"Baton Rouge, LA"', add
label define metaread_lbl 0780 `"Battle Creek, MI"', add
label define metaread_lbl 0840 `"Beaumont-Port Arthur-Orange, TX"', add
label define metaread_lbl 0860 `"Bellingham, WA"', add
label define metaread_lbl 0870 `"Benton Harbor, MI"', add
label define metaread_lbl 0880 `"Billings, MT"', add
label define metaread_lbl 0920 `"Biloxi-Gulfport, MS"', add
label define metaread_lbl 0960 `"Binghamton, NY"', add
label define metaread_lbl 1000 `"Birmingham, AL"', add
label define metaread_lbl 1010 `"Bismarck, ND"', add
label define metaread_lbl 1020 `"Bloomington, IN"', add
label define metaread_lbl 1040 `"Bloomington-Normal, IL"', add
label define metaread_lbl 1080 `"Boise City, ID"', add
label define metaread_lbl 1120 `"Boston, MA"', add
label define metaread_lbl 1121 `"Lawrence-Haverhill, MA/NH"', add
label define metaread_lbl 1122 `"Lowell, MA/NH"', add
label define metaread_lbl 1123 `"Salem-Gloucester, MA"', add
label define metaread_lbl 1140 `"Bradenton, FL"', add
label define metaread_lbl 1150 `"Bremerton, WA"', add
label define metaread_lbl 1160 `"Bridgeport, CT"', add
label define metaread_lbl 1200 `"Brockton, MA"', add
label define metaread_lbl 1240 `"Brownsville-Harlingen-San Benito, TX"', add
label define metaread_lbl 1260 `"Bryan-College Station, TX"', add
label define metaread_lbl 1280 `"Buffalo-Niagara Falls, NY"', add
label define metaread_lbl 1281 `"Niagara Falls, NY"', add
label define metaread_lbl 1300 `"Burlington, NC"', add
label define metaread_lbl 1310 `"Burlington, VT"', add
label define metaread_lbl 1320 `"Canton, OH"', add
label define metaread_lbl 1330 `"Caguas, PR"', add
label define metaread_lbl 1350 `"Casper, WY"', add
label define metaread_lbl 1360 `"Cedar Rapids, IA"', add
label define metaread_lbl 1400 `"Champaign-Urbana-Rantoul, IL"', add
label define metaread_lbl 1440 `"Charleston-N. Charleston, SC"', add
label define metaread_lbl 1480 `"Charleston, WV"', add
label define metaread_lbl 1520 `"Charlotte-Gastonia-Rock Hill, SC"', add
label define metaread_lbl 1521 `"Rock Hill, SC"', add
label define metaread_lbl 1540 `"Charlottesville, VA"', add
label define metaread_lbl 1560 `"Chattanooga, TN/GA"', add
label define metaread_lbl 1580 `"Cheyenne, WY"', add
label define metaread_lbl 1600 `"Chicago-Gary-Lake, IL"', add
label define metaread_lbl 1601 `"Aurora-Elgin, IL"', add
label define metaread_lbl 1602 `"Gary-Hammond-East Chicago, IN"', add
label define metaread_lbl 1603 `"Joliet, IL"', add
label define metaread_lbl 1604 `"Lake County, IL"', add
label define metaread_lbl 1620 `"Chico, CA"', add
label define metaread_lbl 1640 `"Cincinnati, OH/KY/IN"', add
label define metaread_lbl 1660 `"Clarksville-Hopkinsville, TN/KY"', add
label define metaread_lbl 1680 `"Cleveland, OH"', add
label define metaread_lbl 1720 `"Colorado Springs, CO"', add
label define metaread_lbl 1740 `"Columbia, MO"', add
label define metaread_lbl 1760 `"Columbia, SC"', add
label define metaread_lbl 1800 `"Columbus, GA/AL"', add
label define metaread_lbl 1840 `"Columbus, OH"', add
label define metaread_lbl 1880 `"Corpus Christi, TX"', add
label define metaread_lbl 1900 `"Cumberland, MD/WV"', add
label define metaread_lbl 1920 `"Dallas-Fort Worth, TX"', add
label define metaread_lbl 1921 `"Fort Worth-Arlington, TX"', add
label define metaread_lbl 1930 `"Danbury, CT"', add
label define metaread_lbl 1950 `"Danville, VA"', add
label define metaread_lbl 1960 `"Davenport, IA - Rock Island-Moline, IL"', add
label define metaread_lbl 2000 `"Dayton-Springfield, OH"', add
label define metaread_lbl 2001 `"Springfield, OH"', add
label define metaread_lbl 2020 `"Daytona Beach, FL"', add
label define metaread_lbl 2030 `"Decatur, AL"', add
label define metaread_lbl 2040 `"Decatur, IL"', add
label define metaread_lbl 2080 `"Denver-Boulder-Longmont, CO"', add
label define metaread_lbl 2081 `"Boulder-Longmont, CO"', add
label define metaread_lbl 2120 `"Des Moines, IA"', add
label define metaread_lbl 2121 `"Polk, IA"', add
label define metaread_lbl 2160 `"Detroit, MI"', add
label define metaread_lbl 2180 `"Dothan, AL"', add
label define metaread_lbl 2190 `"Dover, DE"', add
label define metaread_lbl 2200 `"Dubuque, IA"', add
label define metaread_lbl 2240 `"Duluth-Superior, MN/WI"', add
label define metaread_lbl 2281 `"Dutchess Co., NY"', add
label define metaread_lbl 2290 `"Eau Claire, WI"', add
label define metaread_lbl 2310 `"El Paso, TX"', add
label define metaread_lbl 2320 `"Elkhart-Goshen, IN"', add
label define metaread_lbl 2330 `"Elmira, NY"', add
label define metaread_lbl 2340 `"Enid, OK"', add
label define metaread_lbl 2360 `"Erie, PA"', add
label define metaread_lbl 2400 `"Eugene-Springfield, OR"', add
label define metaread_lbl 2440 `"Evansville, IN/KY"', add
label define metaread_lbl 2520 `"Fargo-Morehead, ND/MN"', add
label define metaread_lbl 2560 `"Fayetteville, NC"', add
label define metaread_lbl 2580 `"Fayetteville-Springdale, AR"', add
label define metaread_lbl 2600 `"Fitchburg-Leominster, MA"', add
label define metaread_lbl 2620 `"Flagstaff, AZ/UT"', add
label define metaread_lbl 2640 `"Flint, MI"', add
label define metaread_lbl 2650 `"Florence, AL"', add
label define metaread_lbl 2660 `"Florence, SC"', add
label define metaread_lbl 2670 `"Fort Collins-Loveland, CO"', add
label define metaread_lbl 2680 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
label define metaread_lbl 2700 `"Fort Myers-Cape Coral, FL"', add
label define metaread_lbl 2710 `"Fort Pierce, FL"', add
label define metaread_lbl 2720 `"Fort Smith, AR/OK"', add
label define metaread_lbl 2750 `"Fort Walton Beach, FL"', add
label define metaread_lbl 2760 `"Fort Wayne, IN"', add
label define metaread_lbl 2840 `"Fresno, CA"', add
label define metaread_lbl 2880 `"Gadsden, AL"', add
label define metaread_lbl 2900 `"Gainesville, FL"', add
label define metaread_lbl 2920 `"Galveston-Texas City, TX"', add
label define metaread_lbl 2970 `"Glens Falls, NY"', add
label define metaread_lbl 2980 `"Goldsboro, NC"', add
label define metaread_lbl 2990 `"Grand Forks, ND/MN"', add
label define metaread_lbl 3000 `"Grand Rapids, MI"', add
label define metaread_lbl 3010 `"Grand Junction, CO"', add
label define metaread_lbl 3040 `"Great Falls, MT"', add
label define metaread_lbl 3060 `"Greeley, CO"', add
label define metaread_lbl 3080 `"Green Bay, WI"', add
label define metaread_lbl 3120 `"Greensboro-Winston Salem-High Point, NC"', add
label define metaread_lbl 3121 `"Winston-Salem, NC"', add
label define metaread_lbl 3150 `"Greenville, NC"', add
label define metaread_lbl 3160 `"Greenville-Spartenburg-Anderson, SC"', add
label define metaread_lbl 3161 `"Anderson, SC"', add
label define metaread_lbl 3180 `"Hagerstown, MD"', add
label define metaread_lbl 3200 `"Hamilton-Middleton, OH"', add
label define metaread_lbl 3240 `"Harrisburg-Lebanon-Carlisle, PA"', add
label define metaread_lbl 3280 `"Hartford-Bristol-Middleton-New Britain, CT"', add
label define metaread_lbl 3281 `"Bristol, CT"', add
label define metaread_lbl 3282 `"Middletown, CT"', add
label define metaread_lbl 3283 `"New Britain, CT"', add
label define metaread_lbl 3290 `"Hickory-Morganton, NC"', add
label define metaread_lbl 3300 `"Hattiesburg, MS"', add
label define metaread_lbl 3320 `"Honolulu, HI"', add
label define metaread_lbl 3350 `"Houma-Thibodoux, LA"', add
label define metaread_lbl 3360 `"Houston-Brazoria, TX"', add
label define metaread_lbl 3361 `"Brazoria, TX"', add
label define metaread_lbl 3400 `"Huntington-Ashland, WV/KY/OH"', add
label define metaread_lbl 3440 `"Huntsville, AL"', add
label define metaread_lbl 3480 `"Indianapolis, IN"', add
label define metaread_lbl 3500 `"Iowa City, IA"', add
label define metaread_lbl 3520 `"Jackson, MI"', add
label define metaread_lbl 3560 `"Jackson, MS"', add
label define metaread_lbl 3580 `"Jackson, TN"', add
label define metaread_lbl 3590 `"Jacksonville, FL"', add
label define metaread_lbl 3600 `"Jacksonville, NC"', add
label define metaread_lbl 3610 `"Jamestown-Dunkirk, NY"', add
label define metaread_lbl 3620 `"Janesville-Beloit, WI"', add
label define metaread_lbl 3660 `"Johnson City-Kingsport-Bristol, TN/VA"', add
label define metaread_lbl 3680 `"Johnstown, PA"', add
label define metaread_lbl 3710 `"Joplin, MO"', add
label define metaread_lbl 3720 `"Kalamazoo-Portage, MI"', add
label define metaread_lbl 3740 `"Kankakee, IL"', add
label define metaread_lbl 3760 `"Kansas City, MO/KS"', add
label define metaread_lbl 3800 `"Kenosha, WI"', add
label define metaread_lbl 3810 `"Kileen-Temple, TX"', add
label define metaread_lbl 3840 `"Knoxville, TN"', add
label define metaread_lbl 3850 `"Kokomo, IN"', add
label define metaread_lbl 3870 `"LaCrosse, WI"', add
label define metaread_lbl 3880 `"Lafayette, LA"', add
label define metaread_lbl 3920 `"Lafayette-W. Lafayette, IN"', add
label define metaread_lbl 3960 `"Lake Charles, LA"', add
label define metaread_lbl 3980 `"Lakeland-Winterhaven, FL"', add
label define metaread_lbl 4000 `"Lancaster, PA"', add
label define metaread_lbl 4040 `"Lansing-E. Lansing, MI"', add
label define metaread_lbl 4080 `"Laredo, TX"', add
label define metaread_lbl 4100 `"Las Cruces, NM"', add
label define metaread_lbl 4120 `"Las Vegas, NV"', add
label define metaread_lbl 4150 `"Lawrence, KS"', add
label define metaread_lbl 4200 `"Lawton, OK"', add
label define metaread_lbl 4240 `"Lewiston-Auburn, ME"', add
label define metaread_lbl 4280 `"Lexington-Fayette, KY"', add
label define metaread_lbl 4320 `"Lima, OH"', add
label define metaread_lbl 4360 `"Lincoln, NE"', add
label define metaread_lbl 4400 `"Little Rock-N. Little Rock, AR"', add
label define metaread_lbl 4410 `"Long Branch-Asbury Park, NJ"', add
label define metaread_lbl 4420 `"Longview-Marshall, TX"', add
label define metaread_lbl 4440 `"Lorain-Elyria, OH"', add
label define metaread_lbl 4480 `"Los Angeles-Long Beach, CA"', add
label define metaread_lbl 4481 `"Anaheim-Santa Ana-Garden Grove, CA"', add
label define metaread_lbl 4482 `"Orange County, CA"', add
label define metaread_lbl 4520 `"Louisville, KY/IN"', add
label define metaread_lbl 4600 `"Lubbock, TX"', add
label define metaread_lbl 4640 `"Lynchburg, VA"', add
label define metaread_lbl 4680 `"Macon-Warner Robins, GA"', add
label define metaread_lbl 4720 `"Madison, WI"', add
label define metaread_lbl 4760 `"Manchester, NH"', add
label define metaread_lbl 4800 `"Mansfield, OH"', add
label define metaread_lbl 4840 `"Mayaguez, PR"', add
label define metaread_lbl 4880 `"McAllen-Edinburg-Pharr-Mission, TX"', add
label define metaread_lbl 4890 `"Medford, OR"', add
label define metaread_lbl 4900 `"Melbourne-Titusville-Cocoa-Palm Bay, FL"', add
label define metaread_lbl 4920 `"Memphis, TN/AR/MS"', add
label define metaread_lbl 4940 `"Merced, CA"', add
label define metaread_lbl 5000 `"Miami-Hialeah, FL"', add
label define metaread_lbl 5040 `"Midland, TX"', add
label define metaread_lbl 5080 `"Milwaukee, WI"', add
label define metaread_lbl 5120 `"Minneapolis-St. Paul, MN"', add
label define metaread_lbl 5140 `"Missoula, MT"', add
label define metaread_lbl 5160 `"Mobile, AL"', add
label define metaread_lbl 5170 `"Modesto, CA"', add
label define metaread_lbl 5190 `"Monmouth-Ocean, NJ"', add
label define metaread_lbl 5200 `"Monroe, LA"', add
label define metaread_lbl 5240 `"Montgomery, AL"', add
label define metaread_lbl 5280 `"Muncie, IN"', add
label define metaread_lbl 5320 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
label define metaread_lbl 5330 `"Myrtle Beach, SC"', add
label define metaread_lbl 5340 `"Naples, FL"', add
label define metaread_lbl 5350 `"Nashua, NH"', add
label define metaread_lbl 5360 `"Nashville, TN"', add
label define metaread_lbl 5400 `"New Bedford, MA"', add
label define metaread_lbl 5460 `"New Brunswick-Perth Amboy-Sayreville, NJ"', add
label define metaread_lbl 5480 `"New Haven-Meriden, CT"', add
label define metaread_lbl 5481 `"Meriden"', add
label define metaread_lbl 5482 `"New Haven, CT"', add
label define metaread_lbl 5520 `"New London-Norwich, CT/RI"', add
label define metaread_lbl 5560 `"New Orleans, LA"', add
label define metaread_lbl 5600 `"New York, NY-Northeastern NJ"', add
label define metaread_lbl 5601 `"Nassau Co, NY"', add
label define metaread_lbl 5602 `"Bergen-Passaic, NJ"', add
label define metaread_lbl 5603 `"Jersey City, NJ"', add
label define metaread_lbl 5604 `"Middlesex-Somerset-Hunterdon, NJ"', add
label define metaread_lbl 5605 `"Newark, NJ"', add
label define metaread_lbl 5640 `"Newark, OH"', add
label define metaread_lbl 5660 `"Newburgh-Middletown, NY"', add
label define metaread_lbl 5720 `"Norfolk-VA Beach-Newport News, VA"', add
label define metaread_lbl 5721 `"Newport News-Hampton"', add
label define metaread_lbl 5722 `"Norfolk- VA Beach-Portsmouth"', add
label define metaread_lbl 5760 `"Norwalk, CT"', add
label define metaread_lbl 5790 `"Ocala, FL"', add
label define metaread_lbl 5800 `"Odessa, TX"', add
label define metaread_lbl 5880 `"Oklahoma City, OK"', add
label define metaread_lbl 5910 `"Olympia, WA"', add
label define metaread_lbl 5920 `"Omaha, NE/IA"', add
label define metaread_lbl 5950 `"Orange, NY"', add
label define metaread_lbl 5960 `"Orlando, FL"', add
label define metaread_lbl 5990 `"Owensboro, KY"', add
label define metaread_lbl 6010 `"Panama City, FL"', add
label define metaread_lbl 6020 `"Parkersburg-Marietta,WV/OH"', add
label define metaread_lbl 6030 `"Pascagoula-Moss Point, MS"', add
label define metaread_lbl 6080 `"Pensacola, FL"', add
label define metaread_lbl 6120 `"Peoria, IL"', add
label define metaread_lbl 6160 `"Philadelphia, PA/NJ"', add
label define metaread_lbl 6200 `"Phoenix, AZ"', add
label define metaread_lbl 6240 `"Pine Bluff, AR"', add
label define metaread_lbl 6280 `"Pittsburgh-Beaver Valley, PA"', add
label define metaread_lbl 6281 `"Beaver County, PA"', add
label define metaread_lbl 6320 `"Pittsfield, MA"', add
label define metaread_lbl 6360 `"Ponce, PR"', add
label define metaread_lbl 6400 `"Portland, ME"', add
label define metaread_lbl 6440 `"Portland-Vancouver, OR"', add
label define metaread_lbl 6441 `"Vancouver, WA"', add
label define metaread_lbl 6450 `"Portsmouth-Dover-Rochester, NH/ME"', add
label define metaread_lbl 6460 `"Poughkeepsie, NY"', add
label define metaread_lbl 6480 `"Providence-Fall River-Pawtucket, MA/RI"', add
label define metaread_lbl 6481 `"Fall River, MA/RI"', add
label define metaread_lbl 6482 `"Pawtuckett-Woonsocket-Attleboro, RI/MA"', add
label define metaread_lbl 6520 `"Provo-Orem, UT"', add
label define metaread_lbl 6560 `"Pueblo, CO"', add
label define metaread_lbl 6580 `"Punta Gorda, FL"', add
label define metaread_lbl 6600 `"Racine, WI"', add
label define metaread_lbl 6640 `"Raleigh-Durham, NC"', add
label define metaread_lbl 6641 `"Durham, NC"', add
label define metaread_lbl 6660 `"Rapid City, SD"', add
label define metaread_lbl 6680 `"Reading, PA"', add
label define metaread_lbl 6690 `"Redding, CA"', add
label define metaread_lbl 6720 `"Reno, NV"', add
label define metaread_lbl 6740 `"Richland-Kennewick-Pasco, WA"', add
label define metaread_lbl 6760 `"Richmond-Petersburg, VA"', add
label define metaread_lbl 6761 `"Petersburg-Colonial Heights, VA"', add
label define metaread_lbl 6780 `"Riverside-San Bernardino, CA"', add
label define metaread_lbl 6781 `"San Bernardino, CA"', add
label define metaread_lbl 6800 `"Roanoke, VA"', add
label define metaread_lbl 6820 `"Rochester, MN"', add
label define metaread_lbl 6840 `"Rochester, NY"', add
label define metaread_lbl 6880 `"Rockford, IL"', add
label define metaread_lbl 6895 `"Rocky Mount, NC"', add
label define metaread_lbl 6920 `"Sacramento, CA"', add
label define metaread_lbl 6960 `"Saginaw-Bay City-Midland, MI"', add
label define metaread_lbl 6961 `"Bay City, MI"', add
label define metaread_lbl 6980 `"St. Cloud, MN"', add
label define metaread_lbl 7000 `"St. Joseph, MO"', add
label define metaread_lbl 7040 `"St. Louis, MO/IL"', add
label define metaread_lbl 7080 `"Salem, OR"', add
label define metaread_lbl 7120 `"Salinas-Sea Side-Monterey, CA"', add
label define metaread_lbl 7140 `"Salisbury-Concord, NC"', add
label define metaread_lbl 7160 `"Salt Lake City-Ogden, UT"', add
label define metaread_lbl 7161 `"Ogden"', add
label define metaread_lbl 7200 `"San Angelo, TX"', add
label define metaread_lbl 7240 `"San Antonio, TX"', add
label define metaread_lbl 7320 `"San Diego, CA"', add
label define metaread_lbl 7360 `"San Francisco-Oakland-Vallejo, CA"', add
label define metaread_lbl 7361 `"Oakland, CA"', add
label define metaread_lbl 7362 `"Vallejo-Fairfield-Napa, CA"', add
label define metaread_lbl 7400 `"San Jose, CA"', add
label define metaread_lbl 7440 `"San Juan-Bayamon, PR"', add
label define metaread_lbl 7460 `"San Luis Obispo-Atascad-P Robles, CA"', add
label define metaread_lbl 7470 `"Santa Barbara-Santa Maria-Lompoc, CA"', add
label define metaread_lbl 7480 `"Santa Cruz, CA"', add
label define metaread_lbl 7490 `"Santa Fe, NM"', add
label define metaread_lbl 7500 `"Santa Rosa-Petaluma, CA"', add
label define metaread_lbl 7510 `"Sarasota, FL"', add
label define metaread_lbl 7520 `"Savannah, GA"', add
label define metaread_lbl 7560 `"Scranton-Wilkes-Barre, PA"', add
label define metaread_lbl 7561 `"Wilkes-Barre-Hazelton, PA"', add
label define metaread_lbl 7600 `"Seattle-Everett, WA"', add
label define metaread_lbl 7610 `"Sharon, PA"', add
label define metaread_lbl 7620 `"Sheboygan, WI"', add
label define metaread_lbl 7640 `"Sherman-Denison, TX"', add
label define metaread_lbl 7680 `"Shreveport, LA"', add
label define metaread_lbl 7720 `"Sioux City, IA/NE"', add
label define metaread_lbl 7760 `"Sioux Falls, SD"', add
label define metaread_lbl 7800 `"South Bend-Mishawaka, IN"', add
label define metaread_lbl 7840 `"Spokane, WA"', add
label define metaread_lbl 7880 `"Springfield, IL"', add
label define metaread_lbl 7920 `"Springfield, MO"', add
label define metaread_lbl 8000 `"Springfield-Holyoke-Chicopee, MA"', add
label define metaread_lbl 8040 `"Stamford, CT"', add
label define metaread_lbl 8050 `"State College, PA"', add
label define metaread_lbl 8080 `"Steubenville-Weirton,OH/WV"', add
label define metaread_lbl 8120 `"Stockton, CA"', add
label define metaread_lbl 8140 `"Sumter, SC"', add
label define metaread_lbl 8160 `"Syracuse, NY"', add
label define metaread_lbl 8200 `"Tacoma, WA"', add
label define metaread_lbl 8240 `"Tallahassee, FL"', add
label define metaread_lbl 8280 `"Tampa-St. Petersburg-Clearwater, FL"', add
label define metaread_lbl 8320 `"Terre Haute, IN"', add
label define metaread_lbl 8360 `"Texarkana, TX/AR"', add
label define metaread_lbl 8400 `"Toledo, OH/MI"', add
label define metaread_lbl 8440 `"Topeka, KS"', add
label define metaread_lbl 8480 `"Trenton, NJ"', add
label define metaread_lbl 8520 `"Tucson, AZ"', add
label define metaread_lbl 8560 `"Tulsa, OK"', add
label define metaread_lbl 8600 `"Tuscaloosa, AL"', add
label define metaread_lbl 8640 `"Tyler, TX"', add
label define metaread_lbl 8680 `"Utica-Rome, NY"', add
label define metaread_lbl 8730 `"Ventura-Oxnard-Simi Valley, CA"', add
label define metaread_lbl 8750 `"Victoria, TX"', add
label define metaread_lbl 8760 `"Vineland-Milville-Bridgetown, NJ"', add
label define metaread_lbl 8780 `"Visalia-Tulare-Porterville, CA"', add
label define metaread_lbl 8800 `"Waco, TX"', add
label define metaread_lbl 8840 `"Washington, DC/MD/VA"', add
label define metaread_lbl 8880 `"Waterbury, CT"', add
label define metaread_lbl 8920 `"Waterloo-Cedar Falls, IA"', add
label define metaread_lbl 8940 `"Wausau, WI"', add
label define metaread_lbl 8960 `"West Palm Beach-Boca Raton-Delray Beach, FL"', add
label define metaread_lbl 9000 `"Wheeling, WV/OH"', add
label define metaread_lbl 9040 `"Wichita, KS"', add
label define metaread_lbl 9080 `"Wichita Falls, TX"', add
label define metaread_lbl 9140 `"Williamsport, PA"', add
label define metaread_lbl 9160 `"Wilmington, DE/NJ/MD"', add
label define metaread_lbl 9200 `"Wilmington, NC"', add
label define metaread_lbl 9240 `"Worcester, MA"', add
label define metaread_lbl 9260 `"Yakima, WA"', add
label define metaread_lbl 9270 `"Yolo, CA"', add
label define metaread_lbl 9280 `"York, PA"', add
label define metaread_lbl 9320 `"Youngstown-Warren, OH/PA"', add
label define metaread_lbl 9340 `"Yuba City, CA"', add
label define metaread_lbl 9360 `"Yuma, AZ"', add
label values metaread metaread_lbl

label define city_lbl 0000 `"Not in identifiable city (or size group)"'
label define city_lbl 0001 `"Aberdeen, SD"', add
label define city_lbl 0002 `"Aberdeen, WA"', add
label define city_lbl 0003 `"Abilene, TX"', add
label define city_lbl 0004 `"Ada, OK"', add
label define city_lbl 0005 `"Adams, MA"', add
label define city_lbl 0006 `"Adrian, MI"', add
label define city_lbl 0007 `"Abington, PA"', add
label define city_lbl 0010 `"Akron, OH"', add
label define city_lbl 0030 `"Alameda, CA"', add
label define city_lbl 0050 `"Albany, NY"', add
label define city_lbl 0051 `"Albany, GA"', add
label define city_lbl 0052 `"Albert Lea, MN"', add
label define city_lbl 0070 `"Albuquerque, NM"', add
label define city_lbl 0090 `"Alexandria, VA"', add
label define city_lbl 0091 `"Alexandria, LA"', add
label define city_lbl 0100 `"Alhambra, CA"', add
label define city_lbl 0110 `"Allegheny, PA"', add
label define city_lbl 0120 `"Aliquippa, PA"', add
label define city_lbl 0130 `"Allentown, PA"', add
label define city_lbl 0131 `"Alliance, OH"', add
label define city_lbl 0132 `"Alpena, MI"', add
label define city_lbl 0140 `"Alton, IL"', add
label define city_lbl 0150 `"Altoona, PA"', add
label define city_lbl 0160 `"Amarillo, TX"', add
label define city_lbl 0161 `"Ambridge, PA"', add
label define city_lbl 0162 `"Ames, IA"', add
label define city_lbl 0163 `"Amesbury, MA"', add
label define city_lbl 0170 `"Amsterdam, NY"', add
label define city_lbl 0171 `"Anaconda, MT"', add
label define city_lbl 0190 `"Anaheim, CA"', add
label define city_lbl 0210 `"Anchorage, AK"', add
label define city_lbl 0230 `"Anderson, IN"', add
label define city_lbl 0231 `"Anderson, SC"', add
label define city_lbl 0250 `"Andover, MA"', add
label define city_lbl 0270 `"Ann Arbor, MI"', add
label define city_lbl 0271 `"Annapolis, MD"', add
label define city_lbl 0272 `"Anniston, AL"', add
label define city_lbl 0273 `"Ansonia, CT"', add
label define city_lbl 0275 `"Antioch, CA"', add
label define city_lbl 0280 `"Appleton, WI"', add
label define city_lbl 0281 `"Ardmore, OK"', add
label define city_lbl 0282 `"Argenta, AR"', add
label define city_lbl 0283 `"Arkansas, KS"', add
label define city_lbl 0284 `"Arden-Arcade, CA"', add
label define city_lbl 0290 `"Arlington, TX"', add
label define city_lbl 0310 `"Arlington, VA"', add
label define city_lbl 0311 `"Arlington, MA"', add
label define city_lbl 0312 `"Arnold, PA"', add
label define city_lbl 0313 `"Asbury Park, NJ"', add
label define city_lbl 0330 `"Asheville, NC"', add
label define city_lbl 0331 `"Ashland, OH"', add
label define city_lbl 0340 `"Ashland, KY"', add
label define city_lbl 0341 `"Ashland, WI"', add
label define city_lbl 0342 `"Ashtabula, OH"', add
label define city_lbl 0343 `"Astoria, OR"', add
label define city_lbl 0344 `"Atchison, KS"', add
label define city_lbl 0345 `"Athens, GA"', add
label define city_lbl 0346 `"Athol, MA"', add
label define city_lbl 0347 `"Athens-Clarke County, GA"', add
label define city_lbl 0350 `"Atlanta, GA"', add
label define city_lbl 0370 `"Atlantic City, NJ"', add
label define city_lbl 0371 `"Attleboro, MA"', add
label define city_lbl 0390 `"Auburn, NY"', add
label define city_lbl 0391 `"Auburn, ME"', add
label define city_lbl 0410 `"Augusta, GA"', add
label define city_lbl 0411 `"Augusta-Richmond County, GA"', add
label define city_lbl 0430 `"Augusta, ME"', add
label define city_lbl 0450 `"Aurora, CO"', add
label define city_lbl 0470 `"Aurora, IL"', add
label define city_lbl 0490 `"Austin, TX"', add
label define city_lbl 0491 `"Austin, MN"', add
label define city_lbl 0510 `"Bakersfield, CA"', add
label define city_lbl 0530 `"Baltimore, MD"', add
label define city_lbl 0550 `"Bangor, ME"', add
label define city_lbl 0551 `"Barberton, OH"', add
label define city_lbl 0552 `"Barre, VT"', add
label define city_lbl 0553 `"Bartlesville, OK"', add
label define city_lbl 0554 `"Batavia, NY"', add
label define city_lbl 0570 `"Bath, ME"', add
label define city_lbl 0590 `"Baton Rouge, LA"', add
label define city_lbl 0610 `"Battle Creek, MI"', add
label define city_lbl 0630 `"Bay City, MI"', add
label define city_lbl 0640 `"Bayamon, PR"', add
label define city_lbl 0650 `"Bayonne, NJ"', add
label define city_lbl 0651 `"Beacon, NY"', add
label define city_lbl 0652 `"Beatrice, NE"', add
label define city_lbl 0660 `"Belleville, IL"', add
label define city_lbl 0670 `"Beaumont, TX"', add
label define city_lbl 0671 `"Beaver Falls, PA"', add
label define city_lbl 0672 `"Bedford, IN"', add
label define city_lbl 0673 `"Bellaire, OH"', add
label define city_lbl 0680 `"Bellevue, WA"', add
label define city_lbl 0690 `"Bellingham, WA"', add
label define city_lbl 0695 `"Belvedere, CA"', add
label define city_lbl 0700 `"Belleville, NJ"', add
label define city_lbl 0701 `"Bellevue, PA"', add
label define city_lbl 0702 `"Belmont, OH"', add
label define city_lbl 0703 `"Belmont, MA"', add
label define city_lbl 0704 `"Beloit, WI"', add
label define city_lbl 0705 `"Bennington, VT"', add
label define city_lbl 0706 `"Benton Harbor, MI"', add
label define city_lbl 0710 `"Berkeley, CA"', add
label define city_lbl 0711 `"Berlin, NH"', add
label define city_lbl 0712 `"Berwick, PA"', add
label define city_lbl 0720 `"Berwyn, IL"', add
label define city_lbl 0721 `"Bessemer, AL"', add
label define city_lbl 0730 `"Bethlehem, PA"', add
label define city_lbl 0740 `"Biddeford, ME"', add
label define city_lbl 0741 `"Big Spring, TX"', add
label define city_lbl 0742 `"Billings, MT"', add
label define city_lbl 0743 `"Biloxi, MS"', add
label define city_lbl 0750 `"Binghamton, NY"', add
label define city_lbl 0760 `"Beverly, MA"', add
label define city_lbl 0761 `"Beverly Hills, CA"', add
label define city_lbl 0770 `"Birmingham, AL"', add
label define city_lbl 0771 `"Birmingham, CT"', add
label define city_lbl 0772 `"Bismarck, ND"', add
label define city_lbl 0780 `"Bloomfield, NJ"', add
label define city_lbl 0790 `"Bloomington, IL"', add
label define city_lbl 0791 `"Bloomington, IN"', add
label define city_lbl 0792 `"Blue Island, IL"', add
label define city_lbl 0793 `"Bluefield, WV"', add
label define city_lbl 0794 `"Blytheville, AR"', add
label define city_lbl 0795 `"Bogalusa, LA"', add
label define city_lbl 0800 `"Boise, ID"', add
label define city_lbl 0801 `"Boone, IA"', add
label define city_lbl 0810 `"Boston, MA"', add
label define city_lbl 0811 `"Boulder, CO"', add
label define city_lbl 0812 `"Bowling Green, KY"', add
label define city_lbl 0813 `"Braddock, PA"', add
label define city_lbl 0814 `"Braden, WA"', add
label define city_lbl 0815 `"Bradford, PA"', add
label define city_lbl 0816 `"Brainerd, MN"', add
label define city_lbl 0817 `"Braintree, MA"', add
label define city_lbl 0818 `"Brawley, CA"', add
label define city_lbl 0819 `"Bremerton, WA"', add
label define city_lbl 0830 `"Bridgeport, CT"', add
label define city_lbl 0831 `"Bridgeton, NJ"', add
label define city_lbl 0832 `"Bristol, CT"', add
label define city_lbl 0833 `"Bristol, PA"', add
label define city_lbl 0834 `"Bristol, VA"', add
label define city_lbl 0835 `"Bristol, TN"', add
label define city_lbl 0837 `"Bristol, RI"', add
label define city_lbl 0850 `"Brockton, MA"', add
label define city_lbl 0851 `"Brookfield, IL"', add
label define city_lbl 0870 `"Brookline, MA"', add
label define city_lbl 0880 `"Brownsville, TX"', add
label define city_lbl 0881 `"Brownwood, TX"', add
label define city_lbl 0882 `"Brunswick, GA"', add
label define city_lbl 0883 `"Bucyrus, OH"', add
label define city_lbl 0890 `"Buffalo, NY"', add
label define city_lbl 0900 `"Burlington, IA"', add
label define city_lbl 0905 `"Burlington, VT"', add
label define city_lbl 0906 `"Burlington, NJ"', add
label define city_lbl 0907 `"Bushkill, PA"', add
label define city_lbl 0910 `"Butte, MT"', add
label define city_lbl 0911 `"Butler, PA"', add
label define city_lbl 0920 `"Burbank, CA"', add
label define city_lbl 0921 `"Burlingame, CA"', add
label define city_lbl 0926 `"Cairo, IL"', add
label define city_lbl 0927 `"Calumet City, IL"', add
label define city_lbl 0930 `"Cambridge, MA"', add
label define city_lbl 0931 `"Cambridge, OH"', add
label define city_lbl 0950 `"Camden, NJ"', add
label define city_lbl 0951 `"Campbell, OH"', add
label define city_lbl 0952 `"Canonsburg, PA"', add
label define city_lbl 0970 `"Camden, NY"', add
label define city_lbl 0990 `"Canton, OH"', add
label define city_lbl 0991 `"Canton, IL"', add
label define city_lbl 0992 `"Cape Girardeau, MO"', add
label define city_lbl 0993 `"Carbondale, PA"', add
label define city_lbl 0994 `"Carlisle, PA"', add
label define city_lbl 0995 `"Carnegie, PA"', add
label define city_lbl 0996 `"Carrick, PA"', add
label define city_lbl 0997 `"Carteret, NJ"', add
label define city_lbl 0998 `"Carthage, MO"', add
label define city_lbl 0999 `"Casper, WY"', add
label define city_lbl 1000 `"Cape Coral, FL"', add
label define city_lbl 1010 `"Cedar Rapids, IA"', add
label define city_lbl 1020 `"Central Falls, RI"', add
label define city_lbl 1021 `"Centralia, IL"', add
label define city_lbl 1023 `"Chambersburg, PA"', add
label define city_lbl 1024 `"Champaign, IL"', add
label define city_lbl 1025 `"Chanute, KS"', add
label define city_lbl 1026 `"Charleroi, PA"', add
label define city_lbl 1027 `"Chandler, AZ"', add
label define city_lbl 1030 `"Charlestown, MA"', add
label define city_lbl 1050 `"Charleston, SC"', add
label define city_lbl 1060 `"Carolina, PR"', add
label define city_lbl 1070 `"Charleston, WV"', add
label define city_lbl 1090 `"Charlotte, NC"', add
label define city_lbl 1091 `"Charlottesville, VA"', add
label define city_lbl 1110 `"Chattanooga, TN"', add
label define city_lbl 1130 `"Chelsea, MA"', add
label define city_lbl 1140 `"Cheltenham, PA"', add
label define city_lbl 1150 `"Chesapeake, VA"', add
label define city_lbl 1170 `"Chester, PA"', add
label define city_lbl 1171 `"Cheyenne, WY"', add
label define city_lbl 1190 `"Chicago, IL"', add
label define city_lbl 1191 `"Chicago Heights, IL"', add
label define city_lbl 1192 `"Chickasha, OK"', add
label define city_lbl 1210 `"Chicopee, MA"', add
label define city_lbl 1230 `"Chillicothe, OH"', add
label define city_lbl 1250 `"Chula Vista, CA"', add
label define city_lbl 1270 `"Cicero, IL"', add
label define city_lbl 1290 `"Cincinnati, OH"', add
label define city_lbl 1291 `"Clairton, PA"', add
label define city_lbl 1292 `"Claremont, NH"', add
label define city_lbl 1310 `"Clarksburg, WV"', add
label define city_lbl 1311 `"Clarksdale, MS"', add
label define city_lbl 1312 `"Cleburne, TX"', add
label define city_lbl 1330 `"Cleveland, OH"', add
label define city_lbl 1340 `"Cleveland Heights, OH"', add
label define city_lbl 1341 `"Cliffside Park, NJ"', add
label define city_lbl 1350 `"Clifton, NJ"', add
label define city_lbl 1351 `"Clinton, IN"', add
label define city_lbl 1370 `"Clinton, IA"', add
label define city_lbl 1371 `"Clinton, MA"', add
label define city_lbl 1372 `"Coatesville, PA"', add
label define city_lbl 1373 `"Coffeyville, KS"', add
label define city_lbl 1374 `"Cohoes, NY"', add
label define city_lbl 1375 `"Collingswood, NJ"', add
label define city_lbl 1390 `"Colorado Springs, CO"', add
label define city_lbl 1400 `"Cohoes, NY"', add
label define city_lbl 1410 `"Columbia, SC"', add
label define city_lbl 1411 `"Columbia, PA"', add
label define city_lbl 1412 `"Columbia, MO"', add
label define city_lbl 1420 `"Columbia City, IN"', add
label define city_lbl 1430 `"Columbus, GA"', add
label define city_lbl 1450 `"Columbus, OH"', add
label define city_lbl 1451 `"Columbus, MS"', add
label define city_lbl 1452 `"Compton, CA"', add
label define city_lbl 1470 `"Concord, CA"', add
label define city_lbl 1490 `"Concord, NH"', add
label define city_lbl 1491 `"Concord, NC"', add
label define city_lbl 1492 `"Connellsville, PA"', add
label define city_lbl 1493 `"Connersville, IN"', add
label define city_lbl 1494 `"Conshohocken, PA"', add
label define city_lbl 1495 `"Coraopolis, PA"', add
label define city_lbl 1496 `"Corning, NY"', add
label define city_lbl 1500 `"Corona, CA"', add
label define city_lbl 1510 `"Council Bluffs, IA"', add
label define city_lbl 1520 `"Corpus Christi, TX"', add
label define city_lbl 1521 `"Corsicana, TX"', add
label define city_lbl 1522 `"Cortland, NY"', add
label define city_lbl 1523 `"Coshocton, OH"', add
label define city_lbl 1530 `"Covington, KY"', add
label define city_lbl 1540 `"Costa Mesa, CA"', add
label define city_lbl 1545 `"Cranford, NJ"', add
label define city_lbl 1550 `"Cranston, RI"', add
label define city_lbl 1551 `"Crawfordsville, IN"', add
label define city_lbl 1552 `"Cripple Creek, CO"', add
label define city_lbl 1553 `"Cudahy, WI"', add
label define city_lbl 1570 `"Cumberland, MD"', add
label define city_lbl 1571 `"Cumberland, RI"', add
label define city_lbl 1572 `"Cuyahoga Falls, OH"', add
label define city_lbl 1590 `"Dallas, TX"', add
label define city_lbl 1591 `"Danbury, CT"', add
label define city_lbl 1592 `"Daly City, CA"', add
label define city_lbl 1610 `"Danvers, MA"', add
label define city_lbl 1630 `"Danville, IL"', add
label define city_lbl 1631 `"Danville, VA"', add
label define city_lbl 1650 `"Davenport, IA"', add
label define city_lbl 1670 `"Dayton, OH"', add
label define city_lbl 1671 `"Daytona Beach, FL"', add
label define city_lbl 1680 `"Dearborn, MI"', add
label define city_lbl 1690 `"Decatur, IL"', add
label define city_lbl 1691 `"Decatur, AL"', add
label define city_lbl 1692 `"Decatur, GA"', add
label define city_lbl 1693 `"Dedham, MA"', add
label define city_lbl 1694 `"Del Rio, TX"', add
label define city_lbl 1695 `"Denison, TX"', add
label define city_lbl 1710 `"Denver, CO"', add
label define city_lbl 1711 `"Derby, CT"', add
label define city_lbl 1713 `"Derry, PA"', add
label define city_lbl 1730 `"Des Moines, IA"', add
label define city_lbl 1750 `"Detroit, MI"', add
label define city_lbl 1751 `"Dickson City, PA"', add
label define city_lbl 1752 `"Dodge, KS"', add
label define city_lbl 1753 `"Donora, PA"', add
label define city_lbl 1754 `"Dormont, PA"', add
label define city_lbl 1755 `"Dothan, AL"', add
label define city_lbl 1770 `"Dorchester, MA"', add
label define city_lbl 1790 `"Dover, NH"', add
label define city_lbl 1791 `"Dover, NJ"', add
label define city_lbl 1792 `"Du Bois, PA"', add
label define city_lbl 1800 `"Downey, CA"', add
label define city_lbl 1810 `"Dubuque, IA"', add
label define city_lbl 1830 `"Duluth, MN"', add
label define city_lbl 1831 `"Dunkirk, NY"', add
label define city_lbl 1832 `"Dunmore, PA"', add
label define city_lbl 1833 `"Duquesne, PA"', add
label define city_lbl 1834 `"Dundalk, MD"', add
label define city_lbl 1850 `"Durham, NC"', add
label define city_lbl 1860 `"1860"', add
label define city_lbl 1870 `"East Chicago, IN"', add
label define city_lbl 1890 `"East Cleveland, OH"', add
label define city_lbl 1891 `"East Hartford, CT"', add
label define city_lbl 1892 `"East Liverpool, OH"', add
label define city_lbl 1893 `"East Moline, IL"', add
label define city_lbl 1910 `"East Los Angeles, CA"', add
label define city_lbl 1930 `"East Orange, NJ"', add
label define city_lbl 1931 `"East Providence, RI"', add
label define city_lbl 1940 `"East Saginaw, MI"', add
label define city_lbl 1950 `"East St. Louis, IL"', add
label define city_lbl 1951 `"East Youngstown, OH"', add
label define city_lbl 1952 `"Easthampton, MA"', add
label define city_lbl 1970 `"Easton, PA"', add
label define city_lbl 1971 `"Eau Claire, WI"', add
label define city_lbl 1972 `"Ecorse, MI"', add
label define city_lbl 1973 `"El Dorado, KS"', add
label define city_lbl 1974 `"El Dorado, AR"', add
label define city_lbl 1990 `"El Monte, CA"', add
label define city_lbl 2010 `"El Paso, TX"', add
label define city_lbl 2030 `"Elgin, IL"', add
label define city_lbl 2040 `"Elyria, OH"', add
label define city_lbl 2050 `"Elizabeth, NJ"', add
label define city_lbl 2051 `"Elizabeth City, NC"', add
label define city_lbl 2055 `"Elk Grove, CA"', add
label define city_lbl 2060 `"Elkhart, IN"', add
label define city_lbl 2061 `"Ellwood City, PA"', add
label define city_lbl 2062 `"Elmhurst, IL"', add
label define city_lbl 2070 `"Elmira, NY"', add
label define city_lbl 2071 `"Elmwood Park, IL"', add
label define city_lbl 2072 `"Elwood, IN"', add
label define city_lbl 2073 `"Emporia, KS"', add
label define city_lbl 2074 `"Endicott, NY"', add
label define city_lbl 2075 `"Enfield, CT"', add
label define city_lbl 2076 `"Englewood, NJ"', add
label define city_lbl 2080 `"Enid, OK"', add
label define city_lbl 2090 `"Erie, PA"', add
label define city_lbl 2091 `"Escanaba, MI"', add
label define city_lbl 2092 `"Euclid, OH"', add
label define city_lbl 2110 `"Escondido, CA"', add
label define city_lbl 2130 `"Eugene, OR"', add
label define city_lbl 2131 `"Eureka, CA"', add
label define city_lbl 2150 `"Evanston, IL"', add
label define city_lbl 2170 `"Evansville, IN"', add
label define city_lbl 2190 `"Everett, MA"', add
label define city_lbl 2210 `"Everett, WA"', add
label define city_lbl 2211 `"Fairfield, AL"', add
label define city_lbl 2212 `"Fairfield, CT"', add
label define city_lbl 2213 `"Fairhaven, MA"', add
label define city_lbl 2214 `"Fairmont, WV"', add
label define city_lbl 2220 `"Fargo, ND"', add
label define city_lbl 2221 `"Faribault, MN"', add
label define city_lbl 2222 `"Farrell, PA"', add
label define city_lbl 2230 `"Fall River, MA"', add
label define city_lbl 2240 `"Fayetteville, NC"', add
label define city_lbl 2241 `"Ferndale, MI"', add
label define city_lbl 2242 `"Findlay, OH"', add
label define city_lbl 2250 `"Fitchburg, MA"', add
label define city_lbl 2260 `"Fontana, CA"', add
label define city_lbl 2270 `"Flint, MI"', add
label define city_lbl 2271 `"Floral Park, NY"', add
label define city_lbl 2273 `"Florence, AL"', add
label define city_lbl 2274 `"Florence, SC"', add
label define city_lbl 2275 `"Flushing, NY"', add
label define city_lbl 2280 `"Fond du Lac, WI"', add
label define city_lbl 2281 `"Forest Park, IL"', add
label define city_lbl 2290 `"Fort Lauderdale, FL"', add
label define city_lbl 2300 `"Fort Collins, CO"', add
label define city_lbl 2301 `"Fort Dodge, IA"', add
label define city_lbl 2302 `"Fort Madison, IA"', add
label define city_lbl 2303 `"Fort Scott, KS"', add
label define city_lbl 2310 `"Fort Smith, AR"', add
label define city_lbl 2311 `"Fort Thomas, KY"', add
label define city_lbl 2330 `"Fort Wayne, IN"', add
label define city_lbl 2350 `"Fort Worth, TX"', add
label define city_lbl 2351 `"Fostoria, OH"', add
label define city_lbl 2352 `"Framingham, MA"', add
label define city_lbl 2353 `"Frankfort, IN"', add
label define city_lbl 2354 `"Frankfort, KY"', add
label define city_lbl 2355 `"Franklin, PA"', add
label define city_lbl 2356 `"Frederick, MD"', add
label define city_lbl 2357 `"Freeport, NY"', add
label define city_lbl 2358 `"Freeport, IL"', add
label define city_lbl 2359 `"Fremont, OH"', add
label define city_lbl 2360 `"Fremont, NE"', add
label define city_lbl 2370 `"Fresno, CA"', add
label define city_lbl 2390 `"Fullerton, CA"', add
label define city_lbl 2391 `"Fulton, NY"', add
label define city_lbl 2392 `"Gadsden, AL"', add
label define city_lbl 2393 `"Galena, KS"', add
label define city_lbl 2394 `"Gainesville, FL"', add
label define city_lbl 2400 `"Galesburg, IL"', add
label define city_lbl 2410 `"Galveston, TX"', add
label define city_lbl 2411 `"Gardner, MA"', add
label define city_lbl 2430 `"Garden Grove, CA"', add
label define city_lbl 2435 `"Gardena, CA"', add
label define city_lbl 2440 `"Garfield, NJ"', add
label define city_lbl 2441 `"Garfield Heights, OH"', add
label define city_lbl 2450 `"Garland, TX"', add
label define city_lbl 2470 `"Gary, IN"', add
label define city_lbl 2471 `"Gastonia, NC"', add
label define city_lbl 2472 `"Geneva, NY"', add
label define city_lbl 2473 `"Glen Cove, NY"', add
label define city_lbl 2489 `"Glendale, AZ"', add
label define city_lbl 2490 `"Glendale, CA"', add
label define city_lbl 2491 `"Glens Falls, NY"', add
label define city_lbl 2510 `"Gloucester, MA"', add
label define city_lbl 2511 `"Gloucester, NJ"', add
label define city_lbl 2512 `"Gloversville, NY"', add
label define city_lbl 2513 `"Goldsboro, NC"', add
label define city_lbl 2514 `"Goshen, IN"', add
label define city_lbl 2515 `"Grand Forks, ND"', add
label define city_lbl 2516 `"Grand Island, NE"', add
label define city_lbl 2517 `"Grand Junction, CO"', add
label define city_lbl 2520 `"Granite City, IL"', add
label define city_lbl 2530 `"Grand Rapids, MI"', add
label define city_lbl 2531 `"Grandville, MI"', add
label define city_lbl 2540 `"Great Falls, MT"', add
label define city_lbl 2541 `"Greeley, CO"', add
label define city_lbl 2550 `"Green Bay, WI"', add
label define city_lbl 2551 `"Greenfield, MA"', add
label define city_lbl 2570 `"Greensboro, NC"', add
label define city_lbl 2571 `"Greensburg, PA"', add
label define city_lbl 2572 `"Greenville, MS"', add
label define city_lbl 2573 `"Greenville, SC"', add
label define city_lbl 2574 `"Greenville, TX"', add
label define city_lbl 2575 `"Greenwich, CT"', add
label define city_lbl 2576 `"Greenwood, MS"', add
label define city_lbl 2577 `"Greenwood, SC"', add
label define city_lbl 2578 `"Griffin, GA"', add
label define city_lbl 2579 `"Grosse Pointe Park, MI"', add
label define city_lbl 2580 `"Guynabo, PR"', add
label define city_lbl 2581 `"Groton, CT"', add
label define city_lbl 2582 `"Gulfport, MS"', add
label define city_lbl 2583 `"Guthrie, OK"', add
label define city_lbl 2584 `"Hackensack, NJ"', add
label define city_lbl 2590 `"Hagerstown, MD"', add
label define city_lbl 2591 `"Hamden, CT"', add
label define city_lbl 2610 `"Hamilton, OH"', add
label define city_lbl 2630 `"Hammond, IN"', add
label define city_lbl 2650 `"Hampton, VA"', add
label define city_lbl 2670 `"Hamtramck Village, MI"', add
label define city_lbl 2680 `"Hannibal, MO"', add
label define city_lbl 2681 `"Hanover, PA"', add
label define city_lbl 2682 `"Harlingen, TX"', add
label define city_lbl 2683 `"Hanover township, Luzerne county, PA"', add
label define city_lbl 2690 `"Harrisburg, PA"', add
label define city_lbl 2691 `"Harrisburg, IL"', add
label define city_lbl 2692 `"Harrison, NJ"', add
label define city_lbl 2693 `"Harrison, PA"', add
label define city_lbl 2710 `"Hartford, CT"', add
label define city_lbl 2711 `"Harvey, IL"', add
label define city_lbl 2712 `"Hastings, NE"', add
label define city_lbl 2713 `"Hattiesburg, MS"', add
label define city_lbl 2725 `"Haverford, PA"', add
label define city_lbl 2730 `"Haverhill, MA"', add
label define city_lbl 2731 `"Hawthorne, NJ"', add
label define city_lbl 2740 `"Hayward, CA"', add
label define city_lbl 2750 `"Hazleton, PA"', add
label define city_lbl 2751 `"Helena, MT"', add
label define city_lbl 2752 `"Hempstead, NY"', add
label define city_lbl 2753 `"Henderson, KY"', add
label define city_lbl 2754 `"Herkimer, NY"', add
label define city_lbl 2755 `"Herrin, IL"', add
label define city_lbl 2756 `"Hibbing, MN"', add
label define city_lbl 2757 `"Henderson, NV"', add
label define city_lbl 2770 `"Hialeah, FL"', add
label define city_lbl 2780 `"High Point, NC"', add
label define city_lbl 2781 `"Highland Park, IL"', add
label define city_lbl 2790 `"Highland Park, MI"', add
label define city_lbl 2791 `"Hilo, HI"', add
label define city_lbl 2792 `"Hillside, NJ"', add
label define city_lbl 2810 `"Hoboken, NJ"', add
label define city_lbl 2811 `"Holland, MI"', add
label define city_lbl 2830 `"Hollywood, FL"', add
label define city_lbl 2850 `"Holyoke, MA"', add
label define city_lbl 2851 `"Homestead, PA"', add
label define city_lbl 2870 `"Honolulu, HI"', add
label define city_lbl 2871 `"Hopewell, VA"', add
label define city_lbl 2872 `"Hopkinsville, KY"', add
label define city_lbl 2873 `"Hoquiam, WA"', add
label define city_lbl 2874 `"Hornell, NY"', add
label define city_lbl 2875 `"Hot Springs, AR"', add
label define city_lbl 2890 `"Houston, TX"', add
label define city_lbl 2891 `"Hudson, NY"', add
label define city_lbl 2892 `"Huntington, IN"', add
label define city_lbl 2910 `"Huntington, WV"', add
label define city_lbl 2930 `"Huntington Beach, CA"', add
label define city_lbl 2950 `"Huntsville, AL"', add
label define city_lbl 2951 `"Huron, SD"', add
label define city_lbl 2960 `"Hutchinson, KS"', add
label define city_lbl 2961 `"Hyde Park, MA"', add
label define city_lbl 2962 `"Ilion, NY"', add
label define city_lbl 2963 `"Independence, KS"', add
label define city_lbl 2970 `"Independence, MO"', add
label define city_lbl 2990 `"Indianapolis, IN"', add
label define city_lbl 3010 `"Inglewood, CA"', add
label define city_lbl 3011 `"Iowa City, IA"', add
label define city_lbl 3012 `"Iron Mountain, MI"', add
label define city_lbl 3013 `"Ironton, OH"', add
label define city_lbl 3014 `"Ironwood, MI"', add
label define city_lbl 3015 `"Irondequoit, NY"', add
label define city_lbl 3020 `"Irvine, CA"', add
label define city_lbl 3030 `"Irving, TX"', add
label define city_lbl 3050 `"Irvington, NJ"', add
label define city_lbl 3051 `"Ishpeming, MI"', add
label define city_lbl 3052 `"Ithaca, NY"', add
label define city_lbl 3070 `"Jackson, MI"', add
label define city_lbl 3071 `"Jackson, MN"', add
label define city_lbl 3090 `"Jackson, MS"', add
label define city_lbl 3091 `"Jackson, TN"', add
label define city_lbl 3110 `"Jacksonville, FL"', add
label define city_lbl 3111 `"Jacksonville, IL"', add
label define city_lbl 3130 `"Jamestown, NY"', add
label define city_lbl 3131 `"Janesville, WI"', add
label define city_lbl 3132 `"Jeannette, PA"', add
label define city_lbl 3133 `"Jefferson City, MO"', add
label define city_lbl 3134 `"Jeffersonville, IN"', add
label define city_lbl 3150 `"Jersey City, NJ"', add
label define city_lbl 3151 `"Johnson City, NY"', add
label define city_lbl 3160 `"Johnson City, TN"', add
label define city_lbl 3161 `"Johnstown, NY"', add
label define city_lbl 3170 `"Johnstown, PA"', add
label define city_lbl 3190 `"Joliet, IL"', add
label define city_lbl 3191 `"Jonesboro, AR"', add
label define city_lbl 3210 `"Joplin, MO"', add
label define city_lbl 3230 `"Kalamazoo, MI"', add
label define city_lbl 3231 `"Kankakee, IL"', add
label define city_lbl 3250 `"Kansas City, KS"', add
label define city_lbl 3260 `"Kansas City, MO"', add
label define city_lbl 3270 `"Kearny, NJ"', add
label define city_lbl 3271 `"Keene, NH"', add
label define city_lbl 3272 `"Kenmore, NY"', add
label define city_lbl 3273 `"Kenmore, OH"', add
label define city_lbl 3290 `"Kenosha, WI"', add
label define city_lbl 3291 `"Keokuk, IA"', add
label define city_lbl 3292 `"Kewanee, IL"', add
label define city_lbl 3293 `"Key West, FL"', add
label define city_lbl 3294 `"Kingsport, TN"', add
label define city_lbl 3310 `"Kingston, NY"', add
label define city_lbl 3311 `"Kingston, PA"', add
label define city_lbl 3312 `"Kinston, NC"', add
label define city_lbl 3313 `"Klamath Falls, OR"', add
label define city_lbl 3330 `"Knoxville, TN"', add
label define city_lbl 3350 `"Kokomo, IN"', add
label define city_lbl 3370 `"La Crosse, WI"', add
label define city_lbl 3380 `"Lafayette, IN"', add
label define city_lbl 3390 `"Lafayette, LA"', add
label define city_lbl 3391 `"La Grange, IL"', add
label define city_lbl 3392 `"La Grange, GA"', add
label define city_lbl 3393 `"La Porte, IN"', add
label define city_lbl 3394 `"La Salle, IL"', add
label define city_lbl 3395 `"Lackawanna, NY"', add
label define city_lbl 3396 `"Laconia, NH"', add
label define city_lbl 3400 `"Lake Charles, LA"', add
label define city_lbl 3405 `"Lakeland, FL"', add
label define city_lbl 3410 `"Lakewood, CO"', add
label define city_lbl 3430 `"Lakewood, OH"', add
label define city_lbl 3440 `"Lancaster, CA"', add
label define city_lbl 3450 `"Lancaster, PA"', add
label define city_lbl 3451 `"Lancaster, OH"', add
label define city_lbl 3470 `"Lansing, MI"', add
label define city_lbl 3471 `"Lansingburgh, NY"', add
label define city_lbl 3480 `"Laredo, TX"', add
label define city_lbl 3481 `"Latrobe, PA"', add
label define city_lbl 3482 `"Laurel, MS"', add
label define city_lbl 3490 `"Las Vegas, NV"', add
label define city_lbl 3510 `"Lawrence, MA"', add
label define city_lbl 3511 `"Lawrence, KS"', add
label define city_lbl 3512 `"Lawton, OK"', add
label define city_lbl 3513 `"Leadville, CO"', add
label define city_lbl 3520 `"Leavenworth, KS"', add
label define city_lbl 3521 `"Lebanon, PA"', add
label define city_lbl 3522 `"Leominster, MA"', add
label define city_lbl 3530 `"Lehigh, PA"', add
label define city_lbl 3540 `"Lebanon, PA"', add
label define city_lbl 3550 `"Lewiston, ME"', add
label define city_lbl 3551 `"Lewistown, PA"', add
label define city_lbl 3560 `"Lewisville, TX"', add
label define city_lbl 3570 `"Lexington, KY"', add
label define city_lbl 3590 `"Lexington-Fayette, KY"', add
label define city_lbl 3610 `"Lima, OH"', add
label define city_lbl 3630 `"Lincoln, NE"', add
label define city_lbl 3631 `"Lincoln, IL"', add
label define city_lbl 3632 `"Lincoln Park, MI"', add
label define city_lbl 3633 `"Lincoln, RI"', add
label define city_lbl 3634 `"Linden, NJ"', add
label define city_lbl 3635 `"Little Falls, NY"', add
label define city_lbl 3638 `"Lodi, NJ"', add
label define city_lbl 3639 `"Logansport, IN"', add
label define city_lbl 3650 `"Little Rock, AR"', add
label define city_lbl 3670 `"Livonia, MI"', add
label define city_lbl 3680 `"Lockport, NY"', add
label define city_lbl 3690 `"Long Beach, CA"', add
label define city_lbl 3691 `"Long Branch, NJ"', add
label define city_lbl 3692 `"Long Island City, NY"', add
label define city_lbl 3693 `"Longview, WA"', add
label define city_lbl 3710 `"Lorain, OH"', add
label define city_lbl 3730 `"Los Angeles, CA"', add
label define city_lbl 3750 `"Louisville, KY"', add
label define city_lbl 3765 `"Lower Merion, PA"', add
label define city_lbl 3770 `"Lowell, MA"', add
label define city_lbl 3771 `"Lubbock, TX"', add
label define city_lbl 3772 `"Lynbrook, NY"', add
label define city_lbl 3790 `"Lynchburg, VA"', add
label define city_lbl 3800 `"Lyndhurst, NJ"', add
label define city_lbl 3810 `"Lynn, MA"', add
label define city_lbl 3830 `"Macon, GA"', add
label define city_lbl 3850 `"Madison, IN"', add
label define city_lbl 3870 `"Madison, WI"', add
label define city_lbl 3871 `"Mahanoy City, PA"', add
label define city_lbl 3890 `"Malden, MA"', add
label define city_lbl 3891 `"Mamaroneck, NY"', add
label define city_lbl 3910 `"Manchester, NH"', add
label define city_lbl 3911 `"Manchester, CT"', add
label define city_lbl 3912 `"Manhattan, KS"', add
label define city_lbl 3913 `"Manistee, MI"', add
label define city_lbl 3914 `"Manitowoc, WI"', add
label define city_lbl 3915 `"Mankato, MN"', add
label define city_lbl 3929 `"Maplewood, NJ"', add
label define city_lbl 3930 `"Mansfield, OH"', add
label define city_lbl 3931 `"Maplewood, MO"', add
label define city_lbl 3932 `"Marietta, OH"', add
label define city_lbl 3933 `"Marinette, WI"', add
label define city_lbl 3934 `"Marion, IN"', add
label define city_lbl 3940 `"Maywood, IL"', add
label define city_lbl 3950 `"Marion, OH"', add
label define city_lbl 3951 `"Marlborough, MA"', add
label define city_lbl 3952 `"Marquette, MI"', add
label define city_lbl 3953 `"Marshall, TX"', add
label define city_lbl 3954 `"Marshalltown, IA"', add
label define city_lbl 3955 `"Martins Ferry, OH"', add
label define city_lbl 3956 `"Martinsburg, WV"', add
label define city_lbl 3957 `"Mason City, IA"', add
label define city_lbl 3958 `"Massena, NY"', add
label define city_lbl 3959 `"Massillon, OH"', add
label define city_lbl 3960 `"McAllen, TX"', add
label define city_lbl 3961 `"Mattoon, IL"', add
label define city_lbl 3962 `"Mcalester, OK"', add
label define city_lbl 3963 `"Mccomb, MS"', add
label define city_lbl 3964 `"Mckees Rocks, PA"', add
label define city_lbl 3970 `"McKeesport, PA"', add
label define city_lbl 3971 `"Meadville, PA"', add
label define city_lbl 3990 `"Medford, MA"', add
label define city_lbl 3991 `"Medford, OR"', add
label define city_lbl 3992 `"Melrose, MA"', add
label define city_lbl 3993 `"Melrose Park, IL"', add
label define city_lbl 4010 `"Memphis, TN"', add
label define city_lbl 4011 `"Menominee, MI"', add
label define city_lbl 4030 `"Meriden, CT"', add
label define city_lbl 4040 `"Meridian, MS"', add
label define city_lbl 4041 `"Methuen, MA"', add
label define city_lbl 4050 `"Mesa, AZ"', add
label define city_lbl 4070 `"Mesquite, TX"', add
label define city_lbl 4090 `"Metairie, LA"', add
label define city_lbl 4110 `"Miami, FL"', add
label define city_lbl 4120 `"Michigan City, IN"', add
label define city_lbl 4121 `"Middlesboro, KY"', add
label define city_lbl 4122 `"Middletown, CT"', add
label define city_lbl 4123 `"Middletown, NY"', add
label define city_lbl 4124 `"Middletown, OH"', add
label define city_lbl 4125 `"Milford, CT"', add
label define city_lbl 4126 `"Milford, MA"', add
label define city_lbl 4127 `"Millville, NJ"', add
label define city_lbl 4128 `"Milton, MA"', add
label define city_lbl 4130 `"Milwaukee, WI"', add
label define city_lbl 4150 `"Minneapolis, MN"', add
label define city_lbl 4151 `"Minot, ND"', add
label define city_lbl 4160 `"Mishawaka, IN"', add
label define city_lbl 4161 `"Missoula, MT"', add
label define city_lbl 4162 `"Mitchell, SD"', add
label define city_lbl 4163 `"Moberly, MO"', add
label define city_lbl 4170 `"Mobile, AL"', add
label define city_lbl 4190 `"Modesto, CA"', add
label define city_lbl 4210 `"Moline, IL"', add
label define city_lbl 4211 `"Monessen, PA"', add
label define city_lbl 4212 `"Monroe, MI"', add
label define city_lbl 4213 `"Monroe, LA"', add
label define city_lbl 4214 `"Monrovia, CA"', add
label define city_lbl 4230 `"Montclair, NJ"', add
label define city_lbl 4250 `"Montgomery, AL"', add
label define city_lbl 4251 `"Morgantown, WV"', add
label define city_lbl 4252 `"Morristown, NJ"', add
label define city_lbl 4253 `"Moundsville, WV"', add
label define city_lbl 4254 `"Mount Arlington, NJ"', add
label define city_lbl 4255 `"Mount Carmel, PA"', add
label define city_lbl 4256 `"Mount Clemens, MI"', add
label define city_lbl 4260 `"Mount Lebanon, PA"', add
label define city_lbl 4270 `"Moreno Valley, CA"', add
label define city_lbl 4290 `"Mount Vernon, NY"', add
label define city_lbl 4291 `"Mount Vernon, IL"', add
label define city_lbl 4310 `"Muncie, IN"', add
label define city_lbl 4311 `"Munhall, PA"', add
label define city_lbl 4312 `"Murphysboro, IL"', add
label define city_lbl 4313 `"Muscatine, IA"', add
label define city_lbl 4330 `"Muskegon, MI"', add
label define city_lbl 4331 `"Muskegon Heights, MI"', add
label define city_lbl 4350 `"Muskogee, OK"', add
label define city_lbl 4351 `"Nanticoke, PA"', add
label define city_lbl 4370 `"Nantucket, MA"', add
label define city_lbl 4390 `"Nashua, NH"', add
label define city_lbl 4410 `"Nashville-Davidson, TN"', add
label define city_lbl 4411 `"Nashville, TN"', add
label define city_lbl 4413 `"Natchez, MS"', add
label define city_lbl 4414 `"Natick, MA"', add
label define city_lbl 4415 `"Naugatuck, CT"', add
label define city_lbl 4416 `"Needham, MA"', add
label define city_lbl 4420 `"Neptune, NJ"', add
label define city_lbl 4430 `"New Albany, IN"', add
label define city_lbl 4450 `"New Bedford, MA"', add
label define city_lbl 4451 `"New Bern, NC"', add
label define city_lbl 4452 `"New Brighton, NY"', add
label define city_lbl 4470 `"New Britain, CT"', add
label define city_lbl 4490 `"New Brunswick, NJ"', add
label define city_lbl 4510 `"New Castle, PA"', add
label define city_lbl 4511 `"New Castle, IN"', add
label define city_lbl 4530 `"New Haven, CT"', add
label define city_lbl 4550 `"New London, CT"', add
label define city_lbl 4570 `"New Orleans, LA"', add
label define city_lbl 4571 `"New Philadelphia, OH"', add
label define city_lbl 4590 `"New Rochelle, NY"', add
label define city_lbl 4610 `"New York, NY"', add
label define city_lbl 4611 `"Brooklyn (only in census years before 1900)"', add
label define city_lbl 4630 `"Newark, NJ"', add
label define city_lbl 4650 `"Newark, OH"', add
label define city_lbl 4670 `"Newburgh, NY"', add
label define city_lbl 4690 `"Newburyport, MA"', add
label define city_lbl 4710 `"Newport, KY"', add
label define city_lbl 4730 `"Newport, RI"', add
label define city_lbl 4750 `"Newport News, VA"', add
label define city_lbl 4770 `"Newton, MA"', add
label define city_lbl 4771 `"Newton, IA"', add
label define city_lbl 4772 `"Newton, KS"', add
label define city_lbl 4790 `"Niagara Falls, NY"', add
label define city_lbl 4791 `"Niles, MI"', add
label define city_lbl 4792 `"Niles, OH"', add
label define city_lbl 4810 `"Norfolk, VA"', add
label define city_lbl 4811 `"Norfolk, NE"', add
label define city_lbl 4820 `"North Las Vegas, NV"', add
label define city_lbl 4830 `"Norristown Borough, PA"', add
label define city_lbl 4831 `"North Adams, MA"', add
label define city_lbl 4832 `"North Attleborough, MA"', add
label define city_lbl 4833 `"North Bennington, VT"', add
label define city_lbl 4834 `"North Braddock, PA"', add
label define city_lbl 4835 `"North Branford, CT"', add
label define city_lbl 4836 `"North Haven, CT"', add
label define city_lbl 4837 `"North Little Rock, AR"', add
label define city_lbl 4838 `"North Platte, NE"', add
label define city_lbl 4839 `"North Providence, RI"', add
label define city_lbl 4840 `"Northampton, MA"', add
label define city_lbl 4841 `"North Tonawanda, NY"', add
label define city_lbl 4842 `"North Yakima, WA"', add
label define city_lbl 4843 `"Northbridge, MA"', add
label define city_lbl 4845 `"North Bergen, NJ"', add
label define city_lbl 4850 `"North Providence, RI"', add
label define city_lbl 4860 `"Norwalk, CA"', add
label define city_lbl 4870 `"Norwalk, CT"', add
label define city_lbl 4890 `"Norwich, CT"', add
label define city_lbl 4900 `"Norwood, OH"', add
label define city_lbl 4901 `"Norwood, MA"', add
label define city_lbl 4902 `"Nutley, NJ"', add
label define city_lbl 4905 `"Oak Park, IL"', add
label define city_lbl 4910 `"Oak Park Village, IL"', add
label define city_lbl 4930 `"Oakland, CA"', add
label define city_lbl 4950 `"Oceanside, CA"', add
label define city_lbl 4970 `"Ogden, UT"', add
label define city_lbl 4971 `"Ogdensburg, NY"', add
label define city_lbl 4972 `"Oil City, PA"', add
label define city_lbl 4990 `"Oklahoma City, OK"', add
label define city_lbl 4991 `"Okmulgee, OK"', add
label define city_lbl 4992 `"Old Bennington, VT"', add
label define city_lbl 4993 `"Old Forge, PA"', add
label define city_lbl 4994 `"Olean, NY"', add
label define city_lbl 4995 `"Olympia, WA"', add
label define city_lbl 4996 `"Olyphant, PA"', add
label define city_lbl 5010 `"Omaha, NE"', add
label define city_lbl 5011 `"Oneida, NY"', add
label define city_lbl 5012 `"Oneonta, NY"', add
label define city_lbl 5030 `"Ontario, CA"', add
label define city_lbl 5040 `"Orange, CA"', add
label define city_lbl 5050 `"Orange, NJ"', add
label define city_lbl 5051 `"Orange, CT"', add
label define city_lbl 5070 `"Orlando, FL"', add
label define city_lbl 5090 `"Oshkosh, WI"', add
label define city_lbl 5091 `"Oskaloosa, IA"', add
label define city_lbl 5092 `"Ossining, NY"', add
label define city_lbl 5110 `"Oswego, NY"', add
label define city_lbl 5111 `"Ottawa, IL"', add
label define city_lbl 5112 `"Ottumwa, IA"', add
label define city_lbl 5113 `"Owensboro, KY"', add
label define city_lbl 5114 `"Owosso, MI"', add
label define city_lbl 5116 `"Painesville, OH"', add
label define city_lbl 5117 `"Palestine, TX"', add
label define city_lbl 5118 `"Palo Alto, CA"', add
label define city_lbl 5119 `"Pampa, TX"', add
label define city_lbl 5121 `"Paris, TX"', add
label define city_lbl 5122 `"Park Ridge, IL"', add
label define city_lbl 5123 `"Parkersburg, WV"', add
label define city_lbl 5124 `"Parma, OH"', add
label define city_lbl 5125 `"Parsons, KS"', add
label define city_lbl 5130 `"Oxnard, CA"', add
label define city_lbl 5140 `"Palmdale, CA"', add
label define city_lbl 5150 `"Pasadena, CA"', add
label define city_lbl 5170 `"Pasadena, TX"', add
label define city_lbl 5180 `"Paducah, KY"', add
label define city_lbl 5190 `"Passaic, NJ"', add
label define city_lbl 5210 `"Paterson, NJ"', add
label define city_lbl 5230 `"Pawtucket, RI"', add
label define city_lbl 5231 `"Peabody, MA"', add
label define city_lbl 5232 `"Peekskill, NY"', add
label define city_lbl 5233 `"Pekin, IL"', add
label define city_lbl 5240 `"Pembroke Pines, FL"', add
label define city_lbl 5250 `"Pensacola, FL"', add
label define city_lbl 5255 `"Pensauken, NJ"', add
label define city_lbl 5269 `"Peoria, AZ"', add
label define city_lbl 5270 `"Peoria, IL"', add
label define city_lbl 5271 `"Peoria Heights, IL"', add
label define city_lbl 5290 `"Perth Amboy, NJ"', add
label define city_lbl 5291 `"Peru, IN"', add
label define city_lbl 5310 `"Petersburg, VA"', add
label define city_lbl 5311 `"Phenix City, AL"', add
label define city_lbl 5330 `"Philadelphia, PA"', add
label define city_lbl 5331 `"Kensington"', add
label define city_lbl 5332 `"Moyamensing"', add
label define city_lbl 5333 `"Northern Liberties"', add
label define city_lbl 5334 `"Southwark"', add
label define city_lbl 5335 `"Spring Garden"', add
label define city_lbl 5341 `"Phillipsburg, NJ"', add
label define city_lbl 5350 `"Phoenix, AZ"', add
label define city_lbl 5351 `"Phoenixville, PA"', add
label define city_lbl 5352 `"Pine Bluff, AR"', add
label define city_lbl 5353 `"Piqua, OH"', add
label define city_lbl 5354 `"Pittsburg, KS"', add
label define city_lbl 5370 `"Pittsburgh, PA"', add
label define city_lbl 5390 `"Pittsfield, MA"', add
label define city_lbl 5391 `"Pittston, PA"', add
label define city_lbl 5409 `"Plains, PA"', add
label define city_lbl 5410 `"Plainfield, NJ"', add
label define city_lbl 5411 `"Plattsburg, NY"', add
label define city_lbl 5412 `"Pleasantville, NJ"', add
label define city_lbl 5413 `"Plymouth, PA"', add
label define city_lbl 5414 `"Plymouth, MA"', add
label define city_lbl 5415 `"Pocatello, ID"', add
label define city_lbl 5430 `"Plano, TX"', add
label define city_lbl 5450 `"Pomona, CA"', add
label define city_lbl 5451 `"Ponca City, OK"', add
label define city_lbl 5460 `"Ponce, PR"', add
label define city_lbl 5470 `"Pontiac, MI"', add
label define city_lbl 5471 `"Port Angeles, WA"', add
label define city_lbl 5480 `"Port Arthur, TX"', add
label define city_lbl 5481 `"Port Chester, NY"', add
label define city_lbl 5490 `"Port Huron, MI"', add
label define city_lbl 5491 `"Port Jervis, NY"', add
label define city_lbl 5500 `"Port St. Lucie, FL"', add
label define city_lbl 5510 `"Portland, ME"', add
label define city_lbl 5511 `"Portland, IL"', add
label define city_lbl 5530 `"Portland, OR"', add
label define city_lbl 5550 `"Portsmouth, NH"', add
label define city_lbl 5570 `"Portsmouth, OH"', add
label define city_lbl 5590 `"Portsmouth, VA"', add
label define city_lbl 5591 `"Pottstown, PA"', add
label define city_lbl 5610 `"Pottsville, PA"', add
label define city_lbl 5630 `"Poughkeepsie, NY"', add
label define city_lbl 5650 `"Providence, RI"', add
label define city_lbl 5660 `"Provo, UT"', add
label define city_lbl 5670 `"Pueblo, CO"', add
label define city_lbl 5671 `"Punxsutawney, PA"', add
label define city_lbl 5690 `"Quincy, IL"', add
label define city_lbl 5710 `"Quincy, MA"', add
label define city_lbl 5730 `"Racine, WI"', add
label define city_lbl 5731 `"Rahway, NJ"', add
label define city_lbl 5750 `"Raleigh, NC"', add
label define city_lbl 5751 `"Ranger, TX"', add
label define city_lbl 5752 `"Rapid City, SD"', add
label define city_lbl 5770 `"Rancho Cucamonga, CA"', add
label define city_lbl 5790 `"Reading, PA"', add
label define city_lbl 5791 `"Red Bank, NJ"', add
label define city_lbl 5792 `"Redlands, CA"', add
label define city_lbl 5810 `"Reno, NV"', add
label define city_lbl 5811 `"Rensselaer, NY"', add
label define city_lbl 5830 `"Revere, MA"', add
label define city_lbl 5850 `"Richmond, IN"', add
label define city_lbl 5870 `"Richmond, VA"', add
label define city_lbl 5871 `"Richmond, CA"', add
label define city_lbl 5872 `"Ridgefield Park, NJ"', add
label define city_lbl 5873 `"Ridgewood, NJ"', add
label define city_lbl 5874 `"River Rouge, MI"', add
label define city_lbl 5890 `"Riverside, CA"', add
label define city_lbl 5910 `"Roanoke, VA"', add
label define city_lbl 5930 `"Rochester, NY"', add
label define city_lbl 5931 `"Rochester, NH"', add
label define city_lbl 5932 `"Rochester, MN"', add
label define city_lbl 5933 `"Rock Hill, SC"', add
label define city_lbl 5950 `"Rock Island, IL"', add
label define city_lbl 5970 `"Rockford, IL"', add
label define city_lbl 5971 `"Rockland, ME"', add
label define city_lbl 5972 `"Rockton, IL"', add
label define city_lbl 5973 `"Rockville Centre, NY"', add
label define city_lbl 5974 `"Rocky Mount, NC"', add
label define city_lbl 5990 `"Rome, NY"', add
label define city_lbl 5991 `"Rome, GA"', add
label define city_lbl 5992 `"Roosevelt, NJ"', add
label define city_lbl 5993 `"Roselle, NJ"', add
label define city_lbl 5994 `"Roswell, NM"', add
label define city_lbl 5995 `"Roseville, CA"', add
label define city_lbl 6010 `"Roxbury, MA"', add
label define city_lbl 6011 `"Royal Oak, MI"', add
label define city_lbl 6012 `"Rumford Falls, ME"', add
label define city_lbl 6013 `"Rutherford, NJ"', add
label define city_lbl 6014 `"Rutland, VT"', add
label define city_lbl 6030 `"Sacramento, CA"', add
label define city_lbl 6050 `"Saginaw, MI"', add
label define city_lbl 6070 `"Saint Joseph, MO"', add
label define city_lbl 6090 `"Saint Louis, MO"', add
label define city_lbl 6110 `"Saint Paul, MN"', add
label define city_lbl 6130 `"Saint Petersburg, FL"', add
label define city_lbl 6150 `"Salem, MA"', add
label define city_lbl 6170 `"Salem, OR"', add
label define city_lbl 6171 `"Salem, OH"', add
label define city_lbl 6172 `"Salina, KS"', add
label define city_lbl 6190 `"Salinas, CA"', add
label define city_lbl 6191 `"Salisbury, NC"', add
label define city_lbl 6192 `"Salisbury, MD"', add
label define city_lbl 6210 `"Salt Lake City, UT"', add
label define city_lbl 6211 `"San Angelo, TX"', add
label define city_lbl 6220 `"San Angelo, TX"', add
label define city_lbl 6230 `"San Antonio, TX"', add
label define city_lbl 6231 `"San Benito, TX"', add
label define city_lbl 6250 `"San Bernardino, CA"', add
label define city_lbl 6260 `"San Buenaventura (Ventura), CA"', add
label define city_lbl 6270 `"San Diego, CA"', add
label define city_lbl 6280 `"Sandusky, OH"', add
label define city_lbl 6281 `"Sanford, FL"', add
label define city_lbl 6282 `"Sanford, ME"', add
label define city_lbl 6290 `"San Francisco, CA"', add
label define city_lbl 6300 `"San Juan, PR"', add
label define city_lbl 6310 `"San Jose, CA"', add
label define city_lbl 6311 `"San Leandro, CA"', add
label define city_lbl 6312 `"San Mateo, CA"', add
label define city_lbl 6320 `"Santa Barbara, CA"', add
label define city_lbl 6321 `"Santa Cruz, CA"', add
label define city_lbl 6322 `"Santa Fe, NM"', add
label define city_lbl 6330 `"Santa Ana, CA"', add
label define city_lbl 6335 `"Santa Clara, CA"', add
label define city_lbl 6340 `"Santa Clarita, CA"', add
label define city_lbl 6350 `"Santa Rosa, CA"', add
label define city_lbl 6351 `"Sapulpa, OK"', add
label define city_lbl 6352 `"Saratoga Springs, NY"', add
label define city_lbl 6353 `"Saugus, MA"', add
label define city_lbl 6354 `"Sault Ste. Marie, MI"', add
label define city_lbl 6360 `"Santa Monica, CA"', add
label define city_lbl 6370 `"Savannah, GA"', add
label define city_lbl 6390 `"Schenectedy, NY"', add
label define city_lbl 6410 `"Scranton, PA"', add
label define city_lbl 6430 `"Seattle, WA"', add
label define city_lbl 6431 `"Sedalia, MO"', add
label define city_lbl 6432 `"Selma, AL"', add
label define city_lbl 6433 `"Seminole, OK"', add
label define city_lbl 6434 `"Shaker Heights, OH"', add
label define city_lbl 6435 `"Shamokin, PA"', add
label define city_lbl 6437 `"Sharpsville, PA"', add
label define city_lbl 6438 `"Shawnee, OK"', add
label define city_lbl 6440 `"Sharon, PA"', add
label define city_lbl 6450 `"Sheboygan, WI"', add
label define city_lbl 6451 `"Shelby, NC"', add
label define city_lbl 6452 `"Shelbyville, IN"', add
label define city_lbl 6453 `"Shelton, CT"', add
label define city_lbl 6470 `"Shenandoah Borough, PA"', add
label define city_lbl 6471 `"Sherman, TX"', add
label define city_lbl 6472 `"Shorewood, WI"', add
label define city_lbl 6490 `"Shreveport, LA"', add
label define city_lbl 6500 `"Simi Valley, CA"', add
label define city_lbl 6510 `"Sioux City, IA"', add
label define city_lbl 6530 `"Sioux Falls, SD"', add
label define city_lbl 6550 `"Smithfield, RI (1850)"', add
label define city_lbl 6570 `"Somerville, MA"', add
label define city_lbl 6590 `"South Bend, IN"', add
label define city_lbl 6591 `"South Bethlehem, PA"', add
label define city_lbl 6592 `"South Boise, ID"', add
label define city_lbl 6593 `"South Gate, CA"', add
label define city_lbl 6594 `"South Milwaukee, WI"', add
label define city_lbl 6595 `"South Norwalk, CT"', add
label define city_lbl 6610 `"South Omaha, NE"', add
label define city_lbl 6611 `"South Orange, NJ"', add
label define city_lbl 6612 `"South Pasadena, CA"', add
label define city_lbl 6613 `"South Pittsburgh, PA"', add
label define city_lbl 6614 `"South Portland, ME"', add
label define city_lbl 6615 `"South River, NJ"', add
label define city_lbl 6616 `"South St. Paul, MN"', add
label define city_lbl 6617 `"Southbridge, MA"', add
label define city_lbl 6620 `"Spartanburg, SC"', add
label define city_lbl 6630 `"Spokane, WA"', add
label define city_lbl 6640 `"Spring Valley, NV"', add
label define city_lbl 6650 `"Springfield, IL"', add
label define city_lbl 6670 `"Springfield, MA"', add
label define city_lbl 6690 `"Springfield, MO"', add
label define city_lbl 6691 `"St. Augustine, FL"', add
label define city_lbl 6692 `"St. Charles, MO"', add
label define city_lbl 6693 `"St. Cloud, MN"', add
label define city_lbl 6710 `"Springfield, OH"', add
label define city_lbl 6730 `"Stamford, CT"', add
label define city_lbl 6731 `"Statesville, NC"', add
label define city_lbl 6732 `"Staunton, VA"', add
label define city_lbl 6733 `"Steelton, PA"', add
label define city_lbl 6734 `"Sterling, IL"', add
label define city_lbl 6750 `"Sterling Heights, MI"', add
label define city_lbl 6770 `"Steubenville, OH"', add
label define city_lbl 6771 `"Stevens Point, WI"', add
label define city_lbl 6772 `"Stillwater, MN"', add
label define city_lbl 6789 `"Stowe, PA"', add
label define city_lbl 6790 `"Stockton, CA"', add
label define city_lbl 6791 `"Stoneham, MA"', add
label define city_lbl 6792 `"Stonington, CT"', add
label define city_lbl 6793 `"Stratford, CT"', add
label define city_lbl 6794 `"Streator, IL"', add
label define city_lbl 6795 `"Struthers, OH"', add
label define city_lbl 6796 `"Suffolk, VA"', add
label define city_lbl 6797 `"Summit, NJ"', add
label define city_lbl 6798 `"Sumter, SC"', add
label define city_lbl 6799 `"Sunbury, PA"', add
label define city_lbl 6810 `"Sunnyvale, CA"', add
label define city_lbl 6830 `"Superior, WI"', add
label define city_lbl 6831 `"Swampscott, MA"', add
label define city_lbl 6832 `"Sweetwater, TX"', add
label define city_lbl 6833 `"Swissvale, PA"', add
label define city_lbl 6850 `"Syracuse, NY"', add
label define city_lbl 6870 `"Tacoma, WA"', add
label define city_lbl 6871 `"Tallahassee, FL"', add
label define city_lbl 6872 `"Tamaqua, PA"', add
label define city_lbl 6890 `"Tampa, FL"', add
label define city_lbl 6910 `"Taunton, MA"', add
label define city_lbl 6911 `"Taylor, PA"', add
label define city_lbl 6912 `"Temple, TX"', add
label define city_lbl 6913 `"Teaneck, NJ"', add
label define city_lbl 6930 `"Tempe, AZ"', add
label define city_lbl 6950 `"Terre Haute, IN"', add
label define city_lbl 6951 `"Texarkana, TX"', add
label define city_lbl 6952 `"Thomasville, GA"', add
label define city_lbl 6953 `"Thomasville, NC"', add
label define city_lbl 6954 `"Tiffin, OH"', add
label define city_lbl 6960 `"Thousand Oaks, CA"', add
label define city_lbl 6970 `"Toledo, OH"', add
label define city_lbl 6971 `"Tonawanda, NY"', add
label define city_lbl 6990 `"Topeka, KS"', add
label define city_lbl 6991 `"Torrington, CT"', add
label define city_lbl 6992 `"Traverse City, MI"', add
label define city_lbl 7000 `"Torrance, CA"', add
label define city_lbl 7010 `"Trenton, NJ"', add
label define city_lbl 7011 `"Trinidad, CO"', add
label define city_lbl 7030 `"Troy, NY"', add
label define city_lbl 7050 `"Tucson, AZ"', add
label define city_lbl 7070 `"Tulsa, OK"', add
label define city_lbl 7071 `"Turtle Creek, PA"', add
label define city_lbl 7072 `"Tuscaloosa, AL"', add
label define city_lbl 7073 `"Two Rivers, WI"', add
label define city_lbl 7074 `"Tyler, TX"', add
label define city_lbl 7079 `"Union, NJ"', add
label define city_lbl 7080 `"Union City, NJ"', add
label define city_lbl 7081 `"Uniontown, PA"', add
label define city_lbl 7082 `"University City, MO"', add
label define city_lbl 7083 `"Urbana, IL"', add
label define city_lbl 7084 `"Upper Darby, PA"', add
label define city_lbl 7090 `"Utica, NY"', add
label define city_lbl 7091 `"Valdosta, GA"', add
label define city_lbl 7092 `"Vallejo, CA"', add
label define city_lbl 7093 `"Valley Stream, NY"', add
label define city_lbl 7100 `"Vancouver, WA"', add
label define city_lbl 7110 `"Vallejo, CA"', add
label define city_lbl 7111 `"Vandergrift, PA"', add
label define city_lbl 7112 `"Venice, CA"', add
label define city_lbl 7120 `"Vicksburg, MS"', add
label define city_lbl 7121 `"Vincennes, IN"', add
label define city_lbl 7122 `"Virginia, MN"', add
label define city_lbl 7123 `"Virginia City, NV"', add
label define city_lbl 7130 `"Virginia Beach, VA"', add
label define city_lbl 7140 `"Visalia, CA"', add
label define city_lbl 7150 `"Waco, TX"', add
label define city_lbl 7151 `"Wakefield, MA"', add
label define city_lbl 7152 `"Walla Walla, WA"', add
label define city_lbl 7153 `"Wallingford, CT"', add
label define city_lbl 7170 `"Waltham, MA"', add
label define city_lbl 7180 `"Warren, MI"', add
label define city_lbl 7190 `"Warren, OH"', add
label define city_lbl 7191 `"Warren, PA"', add
label define city_lbl 7210 `"Warwick Town, RI"', add
label define city_lbl 7230 `"Washington, DC"', add
label define city_lbl 7231 `"Georgetown, DC"', add
label define city_lbl 7241 `"Washington, PA"', add
label define city_lbl 7242 `"Washington, VA"', add
label define city_lbl 7250 `"Waterbury, CT"', add
label define city_lbl 7270 `"Waterloo, IA"', add
label define city_lbl 7290 `"Waterloo, NY"', add
label define city_lbl 7310 `"Watertown, NY"', add
label define city_lbl 7311 `"Watertown, WI"', add
label define city_lbl 7312 `"Watertown, SD"', add
label define city_lbl 7313 `"Watertown, MA"', add
label define city_lbl 7314 `"Waterville, ME"', add
label define city_lbl 7315 `"Watervliet, NY"', add
label define city_lbl 7316 `"Waukegan, IL"', add
label define city_lbl 7317 `"Waukesha, WI"', add
label define city_lbl 7318 `"Wausau, WI"', add
label define city_lbl 7319 `"Wauwatosa, WI"', add
label define city_lbl 7320 `"West Covina, CA"', add
label define city_lbl 7321 `"Waycross, GA"', add
label define city_lbl 7322 `"Waynesboro, PA"', add
label define city_lbl 7323 `"Webb City, MO"', add
label define city_lbl 7324 `"Webster Groves, MO"', add
label define city_lbl 7325 `"Webster, MA"', add
label define city_lbl 7326 `"Wellesley, MA"', add
label define city_lbl 7327 `"Wenatchee, WA"', add
label define city_lbl 7328 `"Weehawken, NJ"', add
label define city_lbl 7329 `"West Bay City, MI"', add
label define city_lbl 7330 `"West Hoboken, NJ"', add
label define city_lbl 7331 `"West Bethlehem, PA"', add
label define city_lbl 7332 `"West Chester, PA"', add
label define city_lbl 7333 `"West Frankfort, IL"', add
label define city_lbl 7334 `"West Hartford, CT"', add
label define city_lbl 7335 `"West Haven, CT"', add
label define city_lbl 7340 `"West Allis, WI"', add
label define city_lbl 7350 `"West New York, NJ"', add
label define city_lbl 7351 `"West Orange, NJ"', add
label define city_lbl 7352 `"West Palm Beach, FL"', add
label define city_lbl 7353 `"West Springfield, MA"', add
label define city_lbl 7370 `"West Troy, NY"', add
label define city_lbl 7371 `"West Warwick, RI"', add
label define city_lbl 7372 `"Westbrook, ME"', add
label define city_lbl 7373 `"Westerly, RI"', add
label define city_lbl 7374 `"Westfield, MA"', add
label define city_lbl 7375 `"Westfield, NJ"', add
label define city_lbl 7376 `"Wewoka, OK"', add
label define city_lbl 7377 `"Weymouth, MA"', add
label define city_lbl 7390 `"Wheeling, WV"', add
label define city_lbl 7400 `"White Plains, NY"', add
label define city_lbl 7401 `"Whiting, IN"', add
label define city_lbl 7402 `"Whittier, CA"', add
label define city_lbl 7410 `"Wichita, KS"', add
label define city_lbl 7430 `"Wichita Falls, TX"', add
label define city_lbl 7450 `"Wilkes-Barre, PA"', add
label define city_lbl 7451 `"Wilkinsburg, PA"', add
label define city_lbl 7460 `"Wilkinsburg, PA"', add
label define city_lbl 7470 `"Williamsport, PA"', add
label define city_lbl 7471 `"Willimantic, CT"', add
label define city_lbl 7472 `"Wilmette, IL"', add
label define city_lbl 7490 `"Wilmington, DE"', add
label define city_lbl 7510 `"Wilmington, NC"', add
label define city_lbl 7511 `"Wilson, NC"', add
label define city_lbl 7512 `"Winchester, VA"', add
label define city_lbl 7513 `"Winchester, MA"', add
label define city_lbl 7514 `"Windham, CT"', add
label define city_lbl 7515 `"Winnetka, IL"', add
label define city_lbl 7516 `"Winona, MN"', add
label define city_lbl 7530 `"Winston-Salem, NC"', add
label define city_lbl 7531 `"Winthrop, MA"', add
label define city_lbl 7532 `"Woburn, MA"', add
label define city_lbl 7533 `"Woodlawn, PA"', add
label define city_lbl 7534 `"Woodmont, CT"', add
label define city_lbl 7535 `"Woodbridge, NJ"', add
label define city_lbl 7550 `"Woonsocket, RI"', add
label define city_lbl 7551 `"Wooster, OH"', add
label define city_lbl 7570 `"Worcester, MA"', add
label define city_lbl 7571 `"Wyandotte, MI"', add
label define city_lbl 7572 `"Xenia, OH"', add
label define city_lbl 7573 `"Yakima, WA"', add
label define city_lbl 7590 `"Yonkers, NY"', add
label define city_lbl 7610 `"York, PA"', add
label define city_lbl 7630 `"Youngstown, OH"', add
label define city_lbl 7631 `"Ypsilanti, MI"', add
label define city_lbl 7650 `"Zanesville, OH"', add
label values city city_lbl

label define sizepl_lbl 00 `"Not identifiable"'
label define sizepl_lbl 01 `"Under 1,000, or unincorporated"', add
label define sizepl_lbl 02 `"1,000 - 2,499"', add
label define sizepl_lbl 03 `"2,500 - 3,999"', add
label define sizepl_lbl 04 `"4,000 - 4,999"', add
label define sizepl_lbl 05 `"5,000 - 9,999"', add
label define sizepl_lbl 06 `"10,000 - 24,999"', add
label define sizepl_lbl 07 `"25,000 - 49,999"', add
label define sizepl_lbl 08 `"50,000 - 74,999"', add
label define sizepl_lbl 09 `"75,000 - 99,999"', add
label define sizepl_lbl 10 `"100,000 - 199,999"', add
label define sizepl_lbl 20 `"200,000 - 299,999"', add
label define sizepl_lbl 30 `"300,000 - 399,999"', add
label define sizepl_lbl 40 `"400,000 - 499,999"', add
label define sizepl_lbl 50 `"500,000 - 599,999"', add
label define sizepl_lbl 60 `"600,000 - 749,999"', add
label define sizepl_lbl 70 `"750,000 - 999,999"', add
label define sizepl_lbl 80 `"1,000,000 - 1,999,999"', add
label define sizepl_lbl 90 `"2,000,000+"', add
label values sizepl sizepl_lbl

label define sea_lbl 001 `"1"'
label define sea_lbl 002 `"2"', add
label define sea_lbl 003 `"3"', add
label define sea_lbl 004 `"4"', add
label define sea_lbl 005 `"5"', add
label define sea_lbl 007 `"7"', add
label define sea_lbl 008 `"8"', add
label define sea_lbl 009 `"9"', add
label define sea_lbl 010 `"10"', add
label define sea_lbl 011 `"11"', add
label define sea_lbl 013 `"13"', add
label define sea_lbl 014 `"14"', add
label define sea_lbl 015 `"15"', add
label define sea_lbl 016 `"16"', add
label define sea_lbl 017 `"17"', add
label define sea_lbl 018 `"18"', add
label define sea_lbl 019 `"19"', add
label define sea_lbl 020 `"20"', add
label define sea_lbl 021 `"21"', add
label define sea_lbl 022 `"22"', add
label define sea_lbl 023 `"23"', add
label define sea_lbl 024 `"24"', add
label define sea_lbl 025 `"25"', add
label define sea_lbl 026 `"26"', add
label define sea_lbl 027 `"27"', add
label define sea_lbl 029 `"29"', add
label define sea_lbl 030 `"30"', add
label define sea_lbl 031 `"31"', add
label define sea_lbl 032 `"32"', add
label define sea_lbl 033 `"33"', add
label define sea_lbl 034 `"34"', add
label define sea_lbl 035 `"35"', add
label define sea_lbl 036 `"36"', add
label define sea_lbl 037 `"37"', add
label define sea_lbl 038 `"38"', add
label define sea_lbl 039 `"39"', add
label define sea_lbl 040 `"40"', add
label define sea_lbl 041 `"41"', add
label define sea_lbl 042 `"42"', add
label define sea_lbl 043 `"43"', add
label define sea_lbl 044 `"44"', add
label define sea_lbl 045 `"45"', add
label define sea_lbl 046 `"46"', add
label define sea_lbl 047 `"47"', add
label define sea_lbl 048 `"48"', add
label define sea_lbl 050 `"50"', add
label define sea_lbl 051 `"51"', add
label define sea_lbl 052 `"52"', add
label define sea_lbl 053 `"53"', add
label define sea_lbl 054 `"54"', add
label define sea_lbl 055 `"55"', add
label define sea_lbl 056 `"56"', add
label define sea_lbl 057 `"57"', add
label define sea_lbl 058 `"58"', add
label define sea_lbl 059 `"59"', add
label define sea_lbl 060 `"60"', add
label define sea_lbl 061 `"61"', add
label define sea_lbl 062 `"62"', add
label define sea_lbl 063 `"63"', add
label define sea_lbl 064 `"64"', add
label define sea_lbl 065 `"65"', add
label define sea_lbl 066 `"66"', add
label define sea_lbl 067 `"67"', add
label define sea_lbl 068 `"68"', add
label define sea_lbl 069 `"69"', add
label define sea_lbl 070 `"70"', add
label define sea_lbl 071 `"71"', add
label define sea_lbl 072 `"72"', add
label define sea_lbl 073 `"73"', add
label define sea_lbl 074 `"74"', add
label define sea_lbl 075 `"75"', add
label define sea_lbl 076 `"76"', add
label define sea_lbl 077 `"77"', add
label define sea_lbl 079 `"79"', add
label define sea_lbl 080 `"80"', add
label define sea_lbl 081 `"81"', add
label define sea_lbl 083 `"83"', add
label define sea_lbl 084 `"84"', add
label define sea_lbl 085 `"85"', add
label define sea_lbl 086 `"86"', add
label define sea_lbl 087 `"87"', add
label define sea_lbl 088 `"88"', add
label define sea_lbl 089 `"89"', add
label define sea_lbl 090 `"90"', add
label define sea_lbl 091 `"91"', add
label define sea_lbl 092 `"92"', add
label define sea_lbl 093 `"93"', add
label define sea_lbl 094 `"94"', add
label define sea_lbl 095 `"95"', add
label define sea_lbl 096 `"96"', add
label define sea_lbl 097 `"97"', add
label define sea_lbl 098 `"98"', add
label define sea_lbl 099 `"99"', add
label define sea_lbl 100 `"100"', add
label define sea_lbl 101 `"101"', add
label define sea_lbl 102 `"102"', add
label define sea_lbl 103 `"103"', add
label define sea_lbl 104 `"104"', add
label define sea_lbl 105 `"105"', add
label define sea_lbl 106 `"106"', add
label define sea_lbl 107 `"107"', add
label define sea_lbl 108 `"108"', add
label define sea_lbl 109 `"109"', add
label define sea_lbl 110 `"110"', add
label define sea_lbl 111 `"111"', add
label define sea_lbl 112 `"112"', add
label define sea_lbl 113 `"113"', add
label define sea_lbl 114 `"114"', add
label define sea_lbl 115 `"115"', add
label define sea_lbl 116 `"116"', add
label define sea_lbl 117 `"117"', add
label define sea_lbl 118 `"118"', add
label define sea_lbl 119 `"119"', add
label define sea_lbl 120 `"120"', add
label define sea_lbl 121 `"121"', add
label define sea_lbl 122 `"122"', add
label define sea_lbl 123 `"123"', add
label define sea_lbl 124 `"124"', add
label define sea_lbl 125 `"125"', add
label define sea_lbl 126 `"126"', add
label define sea_lbl 127 `"127"', add
label define sea_lbl 128 `"128"', add
label define sea_lbl 129 `"129"', add
label define sea_lbl 130 `"130"', add
label define sea_lbl 131 `"131"', add
label define sea_lbl 132 `"132"', add
label define sea_lbl 133 `"133"', add
label define sea_lbl 135 `"135"', add
label define sea_lbl 136 `"136"', add
label define sea_lbl 137 `"137"', add
label define sea_lbl 138 `"138"', add
label define sea_lbl 139 `"139"', add
label define sea_lbl 140 `"140"', add
label define sea_lbl 141 `"141"', add
label define sea_lbl 142 `"142"', add
label define sea_lbl 143 `"143"', add
label define sea_lbl 145 `"145"', add
label define sea_lbl 146 `"146"', add
label define sea_lbl 147 `"147"', add
label define sea_lbl 148 `"148"', add
label define sea_lbl 149 `"149"', add
label define sea_lbl 150 `"150"', add
label define sea_lbl 151 `"151"', add
label define sea_lbl 152 `"152"', add
label define sea_lbl 153 `"153"', add
label define sea_lbl 154 `"154"', add
label define sea_lbl 155 `"155"', add
label define sea_lbl 156 `"156"', add
label define sea_lbl 157 `"157"', add
label define sea_lbl 158 `"158"', add
label define sea_lbl 159 `"159"', add
label define sea_lbl 160 `"160"', add
label define sea_lbl 162 `"162"', add
label define sea_lbl 163 `"163"', add
label define sea_lbl 164 `"164"', add
label define sea_lbl 165 `"165"', add
label define sea_lbl 166 `"166"', add
label define sea_lbl 167 `"167"', add
label define sea_lbl 168 `"168"', add
label define sea_lbl 169 `"169"', add
label define sea_lbl 170 `"170"', add
label define sea_lbl 171 `"171"', add
label define sea_lbl 172 `"172"', add
label define sea_lbl 173 `"173"', add
label define sea_lbl 175 `"175"', add
label define sea_lbl 176 `"176"', add
label define sea_lbl 177 `"177"', add
label define sea_lbl 178 `"178"', add
label define sea_lbl 179 `"179"', add
label define sea_lbl 180 `"180"', add
label define sea_lbl 181 `"181"', add
label define sea_lbl 182 `"182"', add
label define sea_lbl 183 `"183"', add
label define sea_lbl 184 `"184"', add
label define sea_lbl 185 `"185"', add
label define sea_lbl 186 `"186"', add
label define sea_lbl 187 `"187"', add
label define sea_lbl 188 `"188"', add
label define sea_lbl 189 `"189"', add
label define sea_lbl 190 `"190"', add
label define sea_lbl 191 `"191"', add
label define sea_lbl 192 `"192"', add
label define sea_lbl 193 `"193"', add
label define sea_lbl 194 `"194"', add
label define sea_lbl 195 `"195"', add
label define sea_lbl 196 `"196"', add
label define sea_lbl 197 `"197"', add
label define sea_lbl 198 `"198"', add
label define sea_lbl 199 `"199"', add
label define sea_lbl 200 `"200"', add
label define sea_lbl 201 `"201"', add
label define sea_lbl 202 `"202"', add
label define sea_lbl 203 `"203"', add
label define sea_lbl 204 `"204"', add
label define sea_lbl 205 `"205"', add
label define sea_lbl 206 `"206"', add
label define sea_lbl 207 `"207"', add
label define sea_lbl 208 `"208"', add
label define sea_lbl 209 `"209"', add
label define sea_lbl 210 `"210"', add
label define sea_lbl 211 `"211"', add
label define sea_lbl 212 `"212"', add
label define sea_lbl 213 `"213"', add
label define sea_lbl 214 `"214"', add
label define sea_lbl 215 `"215"', add
label define sea_lbl 216 `"216"', add
label define sea_lbl 217 `"217"', add
label define sea_lbl 218 `"218"', add
label define sea_lbl 219 `"219"', add
label define sea_lbl 220 `"220"', add
label define sea_lbl 221 `"221"', add
label define sea_lbl 222 `"222"', add
label define sea_lbl 223 `"223"', add
label define sea_lbl 224 `"224"', add
label define sea_lbl 225 `"225"', add
label define sea_lbl 226 `"226"', add
label define sea_lbl 227 `"227"', add
label define sea_lbl 228 `"228"', add
label define sea_lbl 229 `"229"', add
label define sea_lbl 230 `"230"', add
label define sea_lbl 231 `"231"', add
label define sea_lbl 232 `"232"', add
label define sea_lbl 233 `"233"', add
label define sea_lbl 234 `"234"', add
label define sea_lbl 235 `"235"', add
label define sea_lbl 236 `"236"', add
label define sea_lbl 237 `"237"', add
label define sea_lbl 238 `"238"', add
label define sea_lbl 239 `"239"', add
label define sea_lbl 240 `"240"', add
label define sea_lbl 242 `"242"', add
label define sea_lbl 243 `"243"', add
label define sea_lbl 244 `"244"', add
label define sea_lbl 245 `"245"', add
label define sea_lbl 246 `"246"', add
label define sea_lbl 247 `"247"', add
label define sea_lbl 248 `"248"', add
label define sea_lbl 249 `"249"', add
label define sea_lbl 251 `"251"', add
label define sea_lbl 253 `"253"', add
label define sea_lbl 254 `"254"', add
label define sea_lbl 256 `"256"', add
label define sea_lbl 257 `"257"', add
label define sea_lbl 258 `"258"', add
label define sea_lbl 259 `"259"', add
label define sea_lbl 260 `"260"', add
label define sea_lbl 261 `"261"', add
label define sea_lbl 262 `"262"', add
label define sea_lbl 263 `"263"', add
label define sea_lbl 264 `"264"', add
label define sea_lbl 265 `"265"', add
label define sea_lbl 266 `"266"', add
label define sea_lbl 267 `"267"', add
label define sea_lbl 268 `"268"', add
label define sea_lbl 269 `"269"', add
label define sea_lbl 270 `"270"', add
label define sea_lbl 271 `"271"', add
label define sea_lbl 273 `"273"', add
label define sea_lbl 274 `"274"', add
label define sea_lbl 275 `"275"', add
label define sea_lbl 276 `"276"', add
label define sea_lbl 277 `"277"', add
label define sea_lbl 278 `"278"', add
label define sea_lbl 279 `"279"', add
label define sea_lbl 280 `"280"', add
label define sea_lbl 281 `"281"', add
label define sea_lbl 282 `"282"', add
label define sea_lbl 283 `"283"', add
label define sea_lbl 284 `"284"', add
label define sea_lbl 285 `"285"', add
label define sea_lbl 286 `"286"', add
label define sea_lbl 287 `"287"', add
label define sea_lbl 288 `"288"', add
label define sea_lbl 289 `"289"', add
label define sea_lbl 290 `"290"', add
label define sea_lbl 291 `"291"', add
label define sea_lbl 292 `"292"', add
label define sea_lbl 293 `"293"', add
label define sea_lbl 294 `"294"', add
label define sea_lbl 295 `"295"', add
label define sea_lbl 296 `"296"', add
label define sea_lbl 298 `"298"', add
label define sea_lbl 299 `"299"', add
label define sea_lbl 300 `"300"', add
label define sea_lbl 301 `"301"', add
label define sea_lbl 302 `"302"', add
label define sea_lbl 303 `"303"', add
label define sea_lbl 304 `"304"', add
label define sea_lbl 305 `"305"', add
label define sea_lbl 306 `"306"', add
label define sea_lbl 307 `"307"', add
label define sea_lbl 308 `"308"', add
label define sea_lbl 309 `"309"', add
label define sea_lbl 310 `"310"', add
label define sea_lbl 311 `"311"', add
label define sea_lbl 314 `"314"', add
label define sea_lbl 315 `"315"', add
label define sea_lbl 317 `"317"', add
label define sea_lbl 318 `"318"', add
label define sea_lbl 319 `"319"', add
label define sea_lbl 320 `"320"', add
label define sea_lbl 321 `"321"', add
label define sea_lbl 322 `"322"', add
label define sea_lbl 323 `"323"', add
label define sea_lbl 324 `"324"', add
label define sea_lbl 325 `"325"', add
label define sea_lbl 326 `"326"', add
label define sea_lbl 327 `"327"', add
label define sea_lbl 328 `"328"', add
label define sea_lbl 329 `"329"', add
label define sea_lbl 330 `"330"', add
label define sea_lbl 331 `"331"', add
label define sea_lbl 332 `"332"', add
label define sea_lbl 333 `"333"', add
label define sea_lbl 334 `"334"', add
label define sea_lbl 335 `"335"', add
label define sea_lbl 336 `"336"', add
label define sea_lbl 337 `"337"', add
label define sea_lbl 339 `"339"', add
label define sea_lbl 340 `"340"', add
label define sea_lbl 341 `"341"', add
label define sea_lbl 342 `"342"', add
label define sea_lbl 343 `"343"', add
label define sea_lbl 344 `"344"', add
label define sea_lbl 345 `"345"', add
label define sea_lbl 346 `"346"', add
label define sea_lbl 347 `"347"', add
label define sea_lbl 348 `"348"', add
label define sea_lbl 349 `"349"', add
label define sea_lbl 350 `"350"', add
label define sea_lbl 352 `"352"', add
label define sea_lbl 353 `"353"', add
label define sea_lbl 354 `"354"', add
label define sea_lbl 355 `"355"', add
label define sea_lbl 356 `"356"', add
label define sea_lbl 357 `"357"', add
label define sea_lbl 358 `"358"', add
label define sea_lbl 360 `"360"', add
label define sea_lbl 361 `"361"', add
label define sea_lbl 362 `"362"', add
label define sea_lbl 363 `"363"', add
label define sea_lbl 364 `"364"', add
label define sea_lbl 365 `"365"', add
label define sea_lbl 366 `"366"', add
label define sea_lbl 367 `"367"', add
label define sea_lbl 368 `"368"', add
label define sea_lbl 369 `"369"', add
label define sea_lbl 370 `"370"', add
label define sea_lbl 371 `"371"', add
label define sea_lbl 372 `"372"', add
label define sea_lbl 373 `"373"', add
label define sea_lbl 374 `"374"', add
label define sea_lbl 375 `"375"', add
label define sea_lbl 376 `"376"', add
label define sea_lbl 377 `"377"', add
label define sea_lbl 378 `"378"', add
label define sea_lbl 379 `"379"', add
label define sea_lbl 380 `"380"', add
label define sea_lbl 381 `"381"', add
label define sea_lbl 382 `"382"', add
label define sea_lbl 384 `"384"', add
label define sea_lbl 385 `"385"', add
label define sea_lbl 386 `"386"', add
label define sea_lbl 387 `"387"', add
label define sea_lbl 388 `"388"', add
label define sea_lbl 389 `"389"', add
label define sea_lbl 390 `"390"', add
label define sea_lbl 391 `"391"', add
label define sea_lbl 392 `"392"', add
label define sea_lbl 393 `"393"', add
label define sea_lbl 394 `"394"', add
label define sea_lbl 395 `"395"', add
label define sea_lbl 396 `"396"', add
label define sea_lbl 398 `"398"', add
label define sea_lbl 402 `"402"', add
label define sea_lbl 403 `"403"', add
label define sea_lbl 404 `"404"', add
label define sea_lbl 405 `"405"', add
label define sea_lbl 406 `"406"', add
label define sea_lbl 407 `"407"', add
label define sea_lbl 408 `"408"', add
label define sea_lbl 409 `"409"', add
label define sea_lbl 410 `"410"', add
label define sea_lbl 411 `"411"', add
label define sea_lbl 412 `"412"', add
label define sea_lbl 413 `"413"', add
label define sea_lbl 414 `"414"', add
label define sea_lbl 415 `"415"', add
label define sea_lbl 416 `"416"', add
label define sea_lbl 418 `"418"', add
label define sea_lbl 420 `"420"', add
label define sea_lbl 421 `"421"', add
label define sea_lbl 422 `"422"', add
label define sea_lbl 425 `"425"', add
label define sea_lbl 426 `"426"', add
label define sea_lbl 427 `"427"', add
label define sea_lbl 428 `"428"', add
label define sea_lbl 429 `"429"', add
label define sea_lbl 430 `"430"', add
label define sea_lbl 431 `"431"', add
label define sea_lbl 432 `"432"', add
label define sea_lbl 433 `"433"', add
label define sea_lbl 434 `"434"', add
label define sea_lbl 435 `"435"', add
label define sea_lbl 436 `"436"', add
label define sea_lbl 437 `"437"', add
label define sea_lbl 438 `"438"', add
label define sea_lbl 439 `"439"', add
label define sea_lbl 440 `"440"', add
label define sea_lbl 441 `"441"', add
label define sea_lbl 442 `"442"', add
label define sea_lbl 443 `"443"', add
label define sea_lbl 444 `"444"', add
label define sea_lbl 445 `"445"', add
label define sea_lbl 446 `"446"', add
label define sea_lbl 447 `"447"', add
label define sea_lbl 448 `"448"', add
label define sea_lbl 449 `"449"', add
label define sea_lbl 450 `"450"', add
label define sea_lbl 451 `"451"', add
label define sea_lbl 452 `"452"', add
label define sea_lbl 453 `"453"', add
label define sea_lbl 454 `"454"', add
label define sea_lbl 455 `"455"', add
label define sea_lbl 456 `"456"', add
label define sea_lbl 457 `"457"', add
label define sea_lbl 458 `"458"', add
label define sea_lbl 459 `"459"', add
label define sea_lbl 460 `"460"', add
label define sea_lbl 461 `"461"', add
label define sea_lbl 462 `"462"', add
label define sea_lbl 464 `"464"', add
label define sea_lbl 465 `"465"', add
label define sea_lbl 466 `"466"', add
label define sea_lbl 467 `"467"', add
label define sea_lbl 468 `"468"', add
label define sea_lbl 469 `"469"', add
label define sea_lbl 470 `"470"', add
label define sea_lbl 471 `"471"', add
label define sea_lbl 473 `"473"', add
label define sea_lbl 474 `"474"', add
label define sea_lbl 475 `"475"', add
label define sea_lbl 476 `"476"', add
label define sea_lbl 477 `"477"', add
label define sea_lbl 478 `"478"', add
label define sea_lbl 479 `"479"', add
label define sea_lbl 480 `"480"', add
label define sea_lbl 481 `"481"', add
label define sea_lbl 482 `"482"', add
label define sea_lbl 483 `"483"', add
label define sea_lbl 484 `"484"', add
label define sea_lbl 485 `"485"', add
label define sea_lbl 487 `"487"', add
label define sea_lbl 488 `"488"', add
label define sea_lbl 489 `"489"', add
label define sea_lbl 490 `"490"', add
label define sea_lbl 491 `"491"', add
label define sea_lbl 492 `"492"', add
label define sea_lbl 493 `"493"', add
label define sea_lbl 494 `"494"', add
label define sea_lbl 495 `"495"', add
label define sea_lbl 496 `"496"', add
label define sea_lbl 497 `"497"', add
label define sea_lbl 498 `"498"', add
label define sea_lbl 500 `"500"', add
label define sea_lbl 501 `"501"', add
label define sea_lbl 502 `"502"', add
label define sea_lbl 990 `"990"', add
label define sea_lbl 991 `"991"', add
label define sea_lbl 992 `"992"', add
label define sea_lbl 999 `"999"', add
label values sea sea_lbl

label define mdstatus_lbl 1 `"Not Metropolitan District"'
label define mdstatus_lbl 2 `"Central City"', add
label define mdstatus_lbl 3 `"Urbanized Fringe"', add
label define mdstatus_lbl 4 `"Metropolitan Fringe"', add
label define mdstatus_lbl 9 `"Not Classified"', add
label values mdstatus mdstatus_lbl

label define metdist_lbl 0000 `"Not in a Metropolitan District"'
label define metdist_lbl 0080 `"Akron, OH"', add
label define metdist_lbl 0160 `"Albany-Troy, NY"', add
label define metdist_lbl 0180 `"Schenectady, NY"', add
label define metdist_lbl 0240 `"Allentown-Bethlehem, PA"', add
label define metdist_lbl 0280 `"Altoona, PA"', add
label define metdist_lbl 0320 `"Amarillo, TX"', add
label define metdist_lbl 0480 `"Asheville, NC"', add
label define metdist_lbl 0520 `"Atlanta, GA"', add
label define metdist_lbl 0560 `"Atlantic City, NJ"', add
label define metdist_lbl 0600 `"Augusta, GA"', add
label define metdist_lbl 0640 `"Austin, TX"', add
label define metdist_lbl 0720 `"Baltimore, MD"', add
label define metdist_lbl 0840 `"Beaumont, TX"', add
label define metdist_lbl 0850 `"Port Arthur, TX"', add
label define metdist_lbl 0960 `"Binghamton, NY"', add
label define metdist_lbl 1000 `"Birmingham, AL"', add
label define metdist_lbl 1120 `"Boston, MA"', add
label define metdist_lbl 1121 `"Lawrence-Haverhill, MA"', add
label define metdist_lbl 1122 `"Lowell, MA"', add
label define metdist_lbl 1160 `"Bridgeport, CT"', add
label define metdist_lbl 1200 `"Brockton, MA"', add
label define metdist_lbl 1280 `"Buffalo, NY"', add
label define metdist_lbl 1281 `"Niagara Falls, NY"', add
label define metdist_lbl 1320 `"Canton, OH"', add
label define metdist_lbl 1360 `"Cedar Rapids, IA"', add
label define metdist_lbl 1440 `"Charleston, SC"', add
label define metdist_lbl 1480 `"Charleston, WV"', add
label define metdist_lbl 1520 `"Charlotte, NC"', add
label define metdist_lbl 1560 `"Chattanooga, TN"', add
label define metdist_lbl 1600 `"Chicago, IL/IN"', add
label define metdist_lbl 1640 `"Cincinnati, OH/KY"', add
label define metdist_lbl 1680 `"Cleveland, OH"', add
label define metdist_lbl 1760 `"Columbia, SC"', add
label define metdist_lbl 1800 `"Columbus, GA"', add
label define metdist_lbl 1840 `"Columbus, OH"', add
label define metdist_lbl 1880 `"Corpus Christi, TX"', add
label define metdist_lbl 1920 `"Dallas, TX"', add
label define metdist_lbl 1921 `"Fort Worth, TX"', add
label define metdist_lbl 1960 `"Davenport-Rock Island-Moline, IA/IL"', add
label define metdist_lbl 2000 `"Dayton, OH"', add
label define metdist_lbl 2001 `"Springfield, OH"', add
label define metdist_lbl 2040 `"Decatur, IL"', add
label define metdist_lbl 2080 `"Denver, CO"', add
label define metdist_lbl 2120 `"Des Moines, IA"', add
label define metdist_lbl 2160 `"Detroit, MI"', add
label define metdist_lbl 2161 `"Pontiac, MI"', add
label define metdist_lbl 2240 `"Duluth-Superior, MN/WI"', add
label define metdist_lbl 2310 `"El Paso, TX"', add
label define metdist_lbl 2360 `"Erie, PA"', add
label define metdist_lbl 2440 `"Evansville, ID"', add
label define metdist_lbl 2640 `"Flint, MI"', add
label define metdist_lbl 2760 `"Fort Wayne, ID"', add
label define metdist_lbl 2840 `"Fresno, CA"', add
label define metdist_lbl 2920 `"Galveston, TX"', add
label define metdist_lbl 3000 `"Grand Rapids, MI"', add
label define metdist_lbl 3120 `"Greensboro, NC"', add
label define metdist_lbl 3121 `"Winston-Salem, NC"', add
label define metdist_lbl 3200 `"Hamilton, OH"', add
label define metdist_lbl 3240 `"Harrisburg, PA"', add
label define metdist_lbl 3280 `"Hartford, CT"', add
label define metdist_lbl 3283 `"New Britain, CT"', add
label define metdist_lbl 3360 `"Houston, TX"', add
label define metdist_lbl 3400 `"Huntington-Ashland, KY/OH"', add
label define metdist_lbl 3480 `"Indianapolis, ID"', add
label define metdist_lbl 3520 `"Jackson, MI"', add
label define metdist_lbl 3560 `"Jackson, MS"', add
label define metdist_lbl 3590 `"Jacksonville, FL"', add
label define metdist_lbl 3680 `"Johnstown, PA"', add
label define metdist_lbl 3720 `"Kalamazoo, MI"', add
label define metdist_lbl 3760 `"Kansas City, MO/KS"', add
label define metdist_lbl 3800 `"Kenosha, WI"', add
label define metdist_lbl 3840 `"Knoxville, TN"', add
label define metdist_lbl 4000 `"Lancaster, PA"', add
label define metdist_lbl 4040 `"Lansing, MI"', add
label define metdist_lbl 4360 `"Lincoln, NE"', add
label define metdist_lbl 4400 `"Little Rock, AR"', add
label define metdist_lbl 4480 `"Los Angeles, CA"', add
label define metdist_lbl 4520 `"Louisville, KY"', add
label define metdist_lbl 4680 `"Macon, GA"', add
label define metdist_lbl 4720 `"Madison, WI"', add
label define metdist_lbl 4760 `"Manchester, NH"', add
label define metdist_lbl 4920 `"Memphis, TN"', add
label define metdist_lbl 5000 `"Miami, FL"', add
label define metdist_lbl 5080 `"Milwaukee, WI"', add
label define metdist_lbl 5120 `"Minneapolis-St. Paul, MN"', add
label define metdist_lbl 5160 `"Mobile, AL"', add
label define metdist_lbl 5240 `"Montgomery, AL"', add
label define metdist_lbl 5320 `"Muskegon, MI"', add
label define metdist_lbl 5360 `"Nashville, TN"', add
label define metdist_lbl 5400 `"New Bedford, MA"', add
label define metdist_lbl 5480 `"New Haven, CT"', add
label define metdist_lbl 5560 `"New Orleans, LA"', add
label define metdist_lbl 5600 `"New York, NY-Northeastern NJ"', add
label define metdist_lbl 5720 `"Norfolk-Portsmouth, VA"', add
label define metdist_lbl 5880 `"Oklahoma City, OK"', add
label define metdist_lbl 5920 `"Omaha, NE/IA"', add
label define metdist_lbl 6120 `"Peoria, IL"', add
label define metdist_lbl 6160 `"Philadelphia, PA/NJ"', add
label define metdist_lbl 6200 `"Phoenix, AZ"', add
label define metdist_lbl 6280 `"Pittsburgh, PA"', add
label define metdist_lbl 6400 `"Portland, ME"', add
label define metdist_lbl 6440 `"Portland, OR/WA"', add
label define metdist_lbl 6480 `"Providence, RI/MA"', add
label define metdist_lbl 6481 `"Fall River, MA"', add
label define metdist_lbl 6560 `"Pueblo, CO"', add
label define metdist_lbl 6600 `"Racine, WI"', add
label define metdist_lbl 6641 `"Durham, NC"', add
label define metdist_lbl 6680 `"Reading, PA"', add
label define metdist_lbl 6760 `"Richmond, VA"', add
label define metdist_lbl 6781 `"San Bernardino, CA"', add
label define metdist_lbl 6800 `"Roanoke, VA"', add
label define metdist_lbl 6840 `"Rochester, NY"', add
label define metdist_lbl 6880 `"Rockford, IL"', add
label define metdist_lbl 6920 `"Sacramento, CA"', add
label define metdist_lbl 6960 `"Saginaw, MI"', add
label define metdist_lbl 6961 `"Bay City, MI"', add
label define metdist_lbl 7000 `"St. Joseph, MO"', add
label define metdist_lbl 7040 `"St. Louis, MO/IL"', add
label define metdist_lbl 7060 `"St. Petersburg, FL"', add
label define metdist_lbl 7160 `"Salt Lake City, UT"', add
label define metdist_lbl 7240 `"San Antonio, TX"', add
label define metdist_lbl 7320 `"San Diego, CA"', add
label define metdist_lbl 7360 `"San Francisco-Oakland, CA"', add
label define metdist_lbl 7400 `"San Jose, CA"', add
label define metdist_lbl 7520 `"Savannah, GA"', add
label define metdist_lbl 7560 `"Scranton, PA"', add
label define metdist_lbl 7561 `"Wilkes-Barre, PA"', add
label define metdist_lbl 7600 `"Seattle, WA"', add
label define metdist_lbl 7680 `"Shreveport, LA"', add
label define metdist_lbl 7720 `"Sioux City, IA"', add
label define metdist_lbl 7800 `"South Bend, ID"', add
label define metdist_lbl 7840 `"Spokane, WA"', add
label define metdist_lbl 7880 `"Springfield, IL"', add
label define metdist_lbl 7920 `"Springfield, MO"', add
label define metdist_lbl 8000 `"Springfield-Holyoke, MA"', add
label define metdist_lbl 8120 `"Stockton, CA"', add
label define metdist_lbl 8160 `"Syracuse, NY"', add
label define metdist_lbl 8200 `"Tacoma, WA"', add
label define metdist_lbl 8280 `"Tampa, FL"', add
label define metdist_lbl 8300 `"St. Petersburg, FL"', add
label define metdist_lbl 8320 `"Terre Haute, ID"', add
label define metdist_lbl 8400 `"Toledo, OH"', add
label define metdist_lbl 8440 `"Topeka, KS"', add
label define metdist_lbl 8480 `"Trenton, NJ"', add
label define metdist_lbl 8560 `"Tulsa, OK"', add
label define metdist_lbl 8680 `"Utica, NY"', add
label define metdist_lbl 8800 `"Waco, TX"', add
label define metdist_lbl 8840 `"Washington, DC/MD/VA"', add
label define metdist_lbl 8880 `"Waterbury, CT"', add
label define metdist_lbl 8920 `"Waterloo, IA"', add
label define metdist_lbl 9000 `"Wheeling, WV"', add
label define metdist_lbl 9040 `"Wichita, KS"', add
label define metdist_lbl 9160 `"Wilmington, DE"', add
label define metdist_lbl 9240 `"Worcester, MA"', add
label define metdist_lbl 9280 `"York, PA"', add
label define metdist_lbl 9320 `"Youngstown, OH"', add
label define metdist_lbl 9999 `"Not classified"', add
label values metdist metdist_lbl

label define urbarea_lbl 0000 `"N/A (household does not reside in an urbanized area)"'
label define urbarea_lbl 0080 `"Akron, OH"', add
label define urbarea_lbl 0160 `"Albany-Schenectady-Troy, NY"', add
label define urbarea_lbl 0180 `"Schenectady, NY"', add
label define urbarea_lbl 0240 `"Allentown-Bethlehem-Easton, PA/NJ"', add
label define urbarea_lbl 0280 `"Altoona, PA"', add
label define urbarea_lbl 0380 `"Anchorage, AK"', add
label define urbarea_lbl 0440 `"Ann Arbor, MI"', add
label define urbarea_lbl 0480 `"Asheville, NC"', add
label define urbarea_lbl 0520 `"Atlanta, GA"', add
label define urbarea_lbl 0560 `"Atlantic City, NJ"', add
label define urbarea_lbl 0600 `"Augusta-Aiken, GA/SC"', add
label define urbarea_lbl 0640 `"Austin, TX"', add
label define urbarea_lbl 0680 `"Bakersfield, CA"', add
label define urbarea_lbl 0720 `"Baltimore, MD"', add
label define urbarea_lbl 0760 `"Baton Rouge, LA"', add
label define urbarea_lbl 0840 `"Beaumont-Port Arthur-Orange,TX"', add
label define urbarea_lbl 0850 `"Port Arthur, TX"', add
label define urbarea_lbl 0960 `"Binghamton, NY"', add
label define urbarea_lbl 1000 `"Birmingham, AL"', add
label define urbarea_lbl 1120 `"Boston, MA"', add
label define urbarea_lbl 1121 `"Lawrence-Haverhill, MA/NH"', add
label define urbarea_lbl 1122 `"Lowell, MA/NH"', add
label define urbarea_lbl 1200 `"Brockton, MA"', add
label define urbarea_lbl 1160 `"Bridgeport, CT"', add
label define urbarea_lbl 1280 `"Buffalo, NY"', add
label define urbarea_lbl 1281 `"Niagara Falls, NY"', add
label define urbarea_lbl 1320 `"Canton, OH"', add
label define urbarea_lbl 1360 `"Cedar Rapids, IA"', add
label define urbarea_lbl 1440 `"Charleston-N.Charleston,SC"', add
label define urbarea_lbl 1480 `"Charleston, WV"', add
label define urbarea_lbl 1520 `"Charlotte-Gastonia-Rock Hill, SC"', add
label define urbarea_lbl 1560 `"Chattanooga, TN/GA"', add
label define urbarea_lbl 1600 `"Chicago-Gary-Lake, IL"', add
label define urbarea_lbl 1640 `"Cincinnati, OH/KY/IN"', add
label define urbarea_lbl 1680 `"Cleveland, OH"', add
label define urbarea_lbl 1760 `"Columbia, SC"', add
label define urbarea_lbl 1800 `"Columbus, GA/AL"', add
label define urbarea_lbl 1840 `"Columbus, OH"', add
label define urbarea_lbl 1920 `"Dallas-Fort Worth, TX"', add
label define urbarea_lbl 1921 `"Fort Worth-Arlington, TX"', add
label define urbarea_lbl 1960 `"Davenport, IA - Rock Island-Moline, IL"', add
label define urbarea_lbl 2000 `"Dayton-Springfield, OH"', add
label define urbarea_lbl 2001 `"Springfield, OH"', add
label define urbarea_lbl 2040 `"Decatur, IL"', add
label define urbarea_lbl 2080 `"Denver-Boulder-Longmont, CO"', add
label define urbarea_lbl 2120 `"Des Moines, IA"', add
label define urbarea_lbl 2160 `"Detroit, MI"', add
label define urbarea_lbl 2161 `"Pontiac, MI"', add
label define urbarea_lbl 2240 `"Duluth-Superior, MN/WI"', add
label define urbarea_lbl 2310 `"El Paso, TX"', add
label define urbarea_lbl 2360 `"Erie, PA"', add
label define urbarea_lbl 2440 `"Evansville, IN/KY"', add
label define urbarea_lbl 2540 `"Montgomery, AL"', add
label define urbarea_lbl 2640 `"Flint, MI"', add
label define urbarea_lbl 2680 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
label define urbarea_lbl 2760 `"Fort Wayne, IN"', add
label define urbarea_lbl 2840 `"Fresno, CA"', add
label define urbarea_lbl 2920 `"Galveston-Texas City, TX"', add
label define urbarea_lbl 3000 `"Grand Rapids, MI"', add
label define urbarea_lbl 3120 `"Greensboro-Winston Salem-High Point, NC"', add
label define urbarea_lbl 3121 `"Winston-Salem, NC"', add
label define urbarea_lbl 3200 `"Hamilton-Middleton, OH"', add
label define urbarea_lbl 3240 `"Harrisburg-Lebanon-Carlisle, PA"', add
label define urbarea_lbl 3280 `"Hartford-Bristol-Middleton, CT"', add
label define urbarea_lbl 3283 `"New Britain, CT"', add
label define urbarea_lbl 3360 `"Houston-Brazoria, TX"', add
label define urbarea_lbl 3400 `"Huntington-Ashland, WV/KY/OH"', add
label define urbarea_lbl 3480 `"Indianapolis, IN"', add
label define urbarea_lbl 3520 `"Jackson, MI"', add
label define urbarea_lbl 3590 `"Jacksonville, FL"', add
label define urbarea_lbl 3680 `"Johnstown, PA"', add
label define urbarea_lbl 3720 `"Kalamazoo-Portage, MI"', add
label define urbarea_lbl 3760 `"Kansas City, MO/KS"', add
label define urbarea_lbl 3800 `"Kenosha, WI"', add
label define urbarea_lbl 3840 `"Knoxville, TN"', add
label define urbarea_lbl 4000 `"Lancaster, PA"', add
label define urbarea_lbl 4040 `"Lansing-E. Lansing, MI"', add
label define urbarea_lbl 4120 `"Las Vegas, NV"', add
label define urbarea_lbl 4280 `"Lexington-Fayette, KY"', add
label define urbarea_lbl 4360 `"Lincoln, NE"', add
label define urbarea_lbl 4400 `"Little Rock-N. Little Rock, AR"', add
label define urbarea_lbl 4480 `"Los Angeles-Long Beach, CA"', add
label define urbarea_lbl 4520 `"Louisville, KY/IN"', add
label define urbarea_lbl 4680 `"Macon-Warner Robins, GA"', add
label define urbarea_lbl 4720 `"Madison, WI"', add
label define urbarea_lbl 4760 `"Manchester, NH"', add
label define urbarea_lbl 4920 `"Memphis, TN/AR/MS"', add
label define urbarea_lbl 5000 `"Miami-Hialeah, FL"', add
label define urbarea_lbl 5080 `"Milwaukee, WI"', add
label define urbarea_lbl 5120 `"Minneapolis-St. Paul, MN"', add
label define urbarea_lbl 5160 `"Mobile, AL"', add
label define urbarea_lbl 5320 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
label define urbarea_lbl 5360 `"Nashville, TN"', add
label define urbarea_lbl 5400 `"New Bedford, MA"', add
label define urbarea_lbl 5480 `"New Haven-Meriden, CT"', add
label define urbarea_lbl 5560 `"New Orleans, LA"', add
label define urbarea_lbl 5600 `"New York, NY-Northeastern NJ"', add
label define urbarea_lbl 5720 `"Norfolk-VA Beach-Newport News, VA"', add
label define urbarea_lbl 5880 `"Oklahoma City, OK"', add
label define urbarea_lbl 5920 `"Omaha, NE/IA"', add
label define urbarea_lbl 5960 `"Orlando, FL"', add
label define urbarea_lbl 6120 `"Peoria, IL"', add
label define urbarea_lbl 6160 `"Philadelphia, PA/NJ"', add
label define urbarea_lbl 6200 `"Phoenix, AZ"', add
label define urbarea_lbl 6280 `"Pittsburgh-Beaver Valley, PA"', add
label define urbarea_lbl 6400 `"Portland, ME"', add
label define urbarea_lbl 6440 `"Portland-Vancouver, OR"', add
label define urbarea_lbl 6480 `"Providence-Fall River-Pawtucket, MA/RI"', add
label define urbarea_lbl 6481 `"Fall River, MA/RI"', add
label define urbarea_lbl 6560 `"Pueblo, CO"', add
label define urbarea_lbl 6600 `"Racine, WI"', add
label define urbarea_lbl 6641 `"Durham, NC"', add
label define urbarea_lbl 6680 `"Reading, PA"', add
label define urbarea_lbl 6760 `"Richmond-Petersburg, VA"', add
label define urbarea_lbl 6780 `"Riverside-San Bernadino, CA"', add
label define urbarea_lbl 6800 `"Roanoke, VA"', add
label define urbarea_lbl 6840 `"Rochester, NY"', add
label define urbarea_lbl 6880 `"Rockford, IL"', add
label define urbarea_lbl 6920 `"Sacramento, CA"', add
label define urbarea_lbl 6960 `"Saginaw-Bay City-Midland, MI"', add
label define urbarea_lbl 7000 `"St. Joseph, MO"', add
label define urbarea_lbl 7040 `"St. Louis, MO/IL"', add
label define urbarea_lbl 7160 `"Salt Lake City-Ogden, UT"', add
label define urbarea_lbl 7240 `"San Antonio, TX"', add
label define urbarea_lbl 7320 `"San Diego, CA"', add
label define urbarea_lbl 7360 `"San Francisco-Oakland-Vallejo, CA"', add
label define urbarea_lbl 7400 `"San Jose, CA"', add
label define urbarea_lbl 7520 `"Savannah, GA"', add
label define urbarea_lbl 7560 `"Scranton-Wilkes-Barre, PA"', add
label define urbarea_lbl 7561 `"Wilkes-Barre-Hazelton, PA"', add
label define urbarea_lbl 7600 `"Seattle-Everett, WA"', add
label define urbarea_lbl 7680 `"Shreveport, LA"', add
label define urbarea_lbl 7720 `"Sioux City, IA/NE"', add
label define urbarea_lbl 7800 `"South Bend-Mishawaka, IN"', add
label define urbarea_lbl 7840 `"Spokane, WA"', add
label define urbarea_lbl 7880 `"Springfield, IL"', add
label define urbarea_lbl 7920 `"Springfield, MO"', add
label define urbarea_lbl 8000 `"Springfield-Holyoke-Chicopee, MA"', add
label define urbarea_lbl 8160 `"Syracuse, NY"', add
label define urbarea_lbl 8200 `"Tacoma, WA"', add
label define urbarea_lbl 8280 `"Tampa-St. Petersburg-Clearwater, FL"', add
label define urbarea_lbl 8281 `"Tampa, FL"', add
label define urbarea_lbl 8282 `"St. Petersberg, FL"', add
label define urbarea_lbl 8320 `"Terre Haute, IN"', add
label define urbarea_lbl 8400 `"Toledo, OH/MI"', add
label define urbarea_lbl 8440 `"Topeka, KS"', add
label define urbarea_lbl 8480 `"Trenton, NJ"', add
label define urbarea_lbl 8520 `"Tucson, AZ"', add
label define urbarea_lbl 8560 `"Tulsa, OK"', add
label define urbarea_lbl 8680 `"Utica-Rome, NY"', add
label define urbarea_lbl 8730 `"Ventura-Oxnard-Simi Valley, CA"', add
label define urbarea_lbl 8800 `"Waco, TX"', add
label define urbarea_lbl 8840 `"Washington, DC/MD/VA"', add
label define urbarea_lbl 8880 `"Waterbury, CT"', add
label define urbarea_lbl 9000 `"Wheeling, WV/OH"', add
label define urbarea_lbl 9040 `"Wichita, KS"', add
label define urbarea_lbl 9160 `"Wilmington, DE/NJ/MD"', add
label define urbarea_lbl 9240 `"Worcester, MA"', add
label define urbarea_lbl 9280 `"York, PA"', add
label define urbarea_lbl 9320 `"Youngstown-Warren, OH/PA"', add
label values urbarea urbarea_lbl

label define cntry_lbl 630 `"Puerto Rico"'
label define cntry_lbl 840 `"United States"', add
label values cntry cntry_lbl

label define gq_lbl 0 `"Vacant unit"'
label define gq_lbl 1 `"Households under 1970 definition"', add
label define gq_lbl 2 `"Additional households under 1990 definition"', add
label define gq_lbl 3 `"Group quarters--Institutions"', add
label define gq_lbl 4 `"Other group quarters"', add
label define gq_lbl 5 `"Additional households under 2000 definition"', add
label define gq_lbl 6 `"Fragment"', add
label values gq gq_lbl

label define gqtype_lbl 0 `"NA (non-group quarters households)"'
label define gqtype_lbl 1 `"Institution (1990, 2000, ACS/PRCS)"', add
label define gqtype_lbl 2 `"Correctional institutions"', add
label define gqtype_lbl 3 `"Mental institutions"', add
label define gqtype_lbl 4 `"Institutions for the elderly, handicapped, and poor"', add
label define gqtype_lbl 5 `"Non-institutional GQ"', add
label define gqtype_lbl 6 `"Military"', add
label define gqtype_lbl 7 `"College dormitory"', add
label define gqtype_lbl 8 `"Rooming house"', add
label define gqtype_lbl 9 `"Other non-institutional GQ and unknown"', add
label values gqtype gqtype_lbl

label define gqtyped_lbl 000 `"NA (non-group quarters households)"'
label define gqtyped_lbl 010 `"Family group, someone related to head"', add
label define gqtyped_lbl 020 `"Unrelated individuals, no one related to head"', add
label define gqtyped_lbl 100 `"Institution (1990, 2000, ACS/PRCS)"', add
label define gqtyped_lbl 200 `"Correctional institution"', add
label define gqtyped_lbl 210 `"Federal/state correctional"', add
label define gqtyped_lbl 211 `"Prison"', add
label define gqtyped_lbl 212 `"Penitentiary"', add
label define gqtyped_lbl 213 `"Military prison"', add
label define gqtyped_lbl 220 `"Local correctional"', add
label define gqtyped_lbl 221 `"Jail"', add
label define gqtyped_lbl 230 `"School juvenile delinquents"', add
label define gqtyped_lbl 240 `"Reformatory"', add
label define gqtyped_lbl 250 `"Camp or chain gang"', add
label define gqtyped_lbl 260 `"House of correction"', add
label define gqtyped_lbl 300 `"Mental institutions"', add
label define gqtyped_lbl 400 `"Institutions for the elderly, handicapped, and poor"', add
label define gqtyped_lbl 410 `"Homes for elderly"', add
label define gqtyped_lbl 411 `"Aged, dependent home"', add
label define gqtyped_lbl 412 `"Nursing/convalescent home"', add
label define gqtyped_lbl 413 `"Old soldiers' home"', add
label define gqtyped_lbl 420 `"Other Instits (Not Aged)"', add
label define gqtyped_lbl 421 `"Other Institution nec"', add
label define gqtyped_lbl 430 `"Homes neglected/depend children"', add
label define gqtyped_lbl 431 `"Orphan school"', add
label define gqtyped_lbl 432 `"Orphans' home, asylum"', add
label define gqtyped_lbl 440 `"Other instits for children"', add
label define gqtyped_lbl 441 `"Children's home, asylum"', add
label define gqtyped_lbl 450 `"Homes physically handicapped"', add
label define gqtyped_lbl 451 `"Deaf, blind school"', add
label define gqtyped_lbl 452 `"Deaf, blind, epilepsy"', add
label define gqtyped_lbl 460 `"Mentally handicapped home"', add
label define gqtyped_lbl 461 `"School for feeblemind"', add
label define gqtyped_lbl 470 `"TB and chronic disease hospital"', add
label define gqtyped_lbl 471 `"Chronic hospitals"', add
label define gqtyped_lbl 472 `"Sanatoria"', add
label define gqtyped_lbl 480 `"Poor houses and farms"', add
label define gqtyped_lbl 481 `"Poor house, almshouse"', add
label define gqtyped_lbl 482 `"Poor farm, workhouse"', add
label define gqtyped_lbl 491 `"Maternity homes for unmarried mothers"', add
label define gqtyped_lbl 492 `"Homes for widows, single, fallen women"', add
label define gqtyped_lbl 493 `"Detention homes"', add
label define gqtyped_lbl 494 `"Misc asylums"', add
label define gqtyped_lbl 495 `"Home, other dependent"', add
label define gqtyped_lbl 496 `"Institution combination or unknown"', add
label define gqtyped_lbl 500 `"Non-institutional group quarters"', add
label define gqtyped_lbl 501 `"Family formerly in institutional group quarters"', add
label define gqtyped_lbl 502 `"Unrelated individual residing with family formerly in institutional group quarters"', add
label define gqtyped_lbl 600 `"Military"', add
label define gqtyped_lbl 601 `"U.S. army installation"', add
label define gqtyped_lbl 602 `"Navy, marine installation"', add
label define gqtyped_lbl 603 `"Navy ships"', add
label define gqtyped_lbl 604 `"Air service"', add
label define gqtyped_lbl 700 `"College dormitory"', add
label define gqtyped_lbl 701 `"Military service academies"', add
label define gqtyped_lbl 800 `"Rooming house"', add
label define gqtyped_lbl 801 `"Hotel"', add
label define gqtyped_lbl 802 `"House, lodging apartments"', add
label define gqtyped_lbl 803 `"YMCA, YWCA"', add
label define gqtyped_lbl 804 `"Club"', add
label define gqtyped_lbl 900 `"Other Non-Instit GQ"', add
label define gqtyped_lbl 901 `"Other Non-Instit GQ"', add
label define gqtyped_lbl 910 `"Schools"', add
label define gqtyped_lbl 911 `"Boarding schools"', add
label define gqtyped_lbl 912 `"Academy, institute"', add
label define gqtyped_lbl 913 `"Industrial training"', add
label define gqtyped_lbl 914 `"Indian school"', add
label define gqtyped_lbl 920 `"Hospitals"', add
label define gqtyped_lbl 921 `"Hospital, charity"', add
label define gqtyped_lbl 922 `"Infirmary"', add
label define gqtyped_lbl 923 `"Maternity hospital"', add
label define gqtyped_lbl 924 `"Children's hospital"', add
label define gqtyped_lbl 931 `"Church, Abbey"', add
label define gqtyped_lbl 932 `"Convent"', add
label define gqtyped_lbl 933 `"Monastery"', add
label define gqtyped_lbl 934 `"Mission"', add
label define gqtyped_lbl 935 `"Seminary"', add
label define gqtyped_lbl 936 `"Religious commune"', add
label define gqtyped_lbl 937 `"Other religious"', add
label define gqtyped_lbl 940 `"Work sites"', add
label define gqtyped_lbl 941 `"Construction, except rr"', add
label define gqtyped_lbl 942 `"Lumber"', add
label define gqtyped_lbl 943 `"Mining"', add
label define gqtyped_lbl 944 `"Railroad"', add
label define gqtyped_lbl 945 `"Farms, ranches"', add
label define gqtyped_lbl 946 `"Ships, boats"', add
label define gqtyped_lbl 947 `"Other industrial"', add
label define gqtyped_lbl 948 `"Other worksites"', add
label define gqtyped_lbl 950 `"Nurses home, dorm"', add
label define gqtyped_lbl 955 `"Passenger ships"', add
label define gqtyped_lbl 960 `"Other group quarters"', add
label define gqtyped_lbl 997 `"Unknown"', add
label define gqtyped_lbl 999 `"Fragment (boarders and lodgers, 1900)"', add
label values gqtyped gqtyped_lbl

label define gqfunds_lbl 00 `"N/A"'
label define gqfunds_lbl 11 `"Federal support"', add
label define gqfunds_lbl 12 `"Federal and State"', add
label define gqfunds_lbl 13 `"State support"', add
label define gqfunds_lbl 14 `"Local support"', add
label define gqfunds_lbl 15 `"State and Local"', add
label define gqfunds_lbl 16 `"Government, not specified"', add
label define gqfunds_lbl 21 `"Private, Nonprofit"', add
label define gqfunds_lbl 22 `"Private, Commercial"', add
label define gqfunds_lbl 23 `"Religious"', add
label define gqfunds_lbl 24 `"Ethnic, fraternal"', add
label define gqfunds_lbl 25 `"Private, unknown"', add
label define gqfunds_lbl 99 `"Fragment or Unknown"', add
label values gqfunds gqfunds_lbl

label define farm_lbl 0 `"N/A"'
label define farm_lbl 1 `"Non-Farm"', add
label define farm_lbl 2 `"Farm"', add
label values farm farm_lbl

label define nfams_lbl 00 `"0 families (vacant unit)"'
label define nfams_lbl 01 `"1 family or N/A"', add
label define nfams_lbl 02 `"2 families"', add
label define nfams_lbl 03 `"3"', add
label define nfams_lbl 04 `"4"', add
label define nfams_lbl 05 `"5"', add
label define nfams_lbl 06 `"6"', add
label define nfams_lbl 07 `"7"', add
label define nfams_lbl 08 `"8"', add
label define nfams_lbl 09 `"9"', add
label define nfams_lbl 10 `"10"', add
label define nfams_lbl 11 `"11"', add
label define nfams_lbl 12 `"12"', add
label define nfams_lbl 13 `"13"', add
label define nfams_lbl 14 `"14"', add
label define nfams_lbl 15 `"15"', add
label define nfams_lbl 16 `"16"', add
label define nfams_lbl 17 `"17"', add
label define nfams_lbl 18 `"18"', add
label define nfams_lbl 19 `"19"', add
label define nfams_lbl 20 `"20"', add
label define nfams_lbl 21 `"21"', add
label define nfams_lbl 22 `"22"', add
label define nfams_lbl 23 `"23"', add
label define nfams_lbl 24 `"24"', add
label define nfams_lbl 25 `"25"', add
label define nfams_lbl 26 `"26"', add
label define nfams_lbl 27 `"27"', add
label define nfams_lbl 28 `"28"', add
label define nfams_lbl 29 `"29"', add
label define nfams_lbl 30 `"30"', add
label values nfams nfams_lbl

label define ncouples_lbl 0 `"0 couples or N/A"'
label define ncouples_lbl 1 `"1"', add
label define ncouples_lbl 2 `"2"', add
label define ncouples_lbl 3 `"3"', add
label define ncouples_lbl 4 `"4"', add
label define ncouples_lbl 5 `"5"', add
label define ncouples_lbl 6 `"6"', add
label define ncouples_lbl 7 `"7"', add
label define ncouples_lbl 8 `"8"', add
label define ncouples_lbl 9 `"9"', add
label values ncouples ncouples_lbl

label define nmothers_lbl 0 `"0 mothers or N/A"'
label define nmothers_lbl 1 `"1"', add
label define nmothers_lbl 2 `"2"', add
label define nmothers_lbl 3 `"3"', add
label define nmothers_lbl 4 `"4"', add
label define nmothers_lbl 5 `"5"', add
label define nmothers_lbl 6 `"6"', add
label define nmothers_lbl 7 `"7"', add
label define nmothers_lbl 8 `"8"', add
label values nmothers nmothers_lbl

label define nfathers_lbl 0 `"0 fathers or N/A"'
label define nfathers_lbl 1 `"1"', add
label define nfathers_lbl 2 `"2"', add
label define nfathers_lbl 3 `"3"', add
label define nfathers_lbl 4 `"4"', add
label define nfathers_lbl 5 `"5"', add
label define nfathers_lbl 6 `"6"', add
label values nfathers nfathers_lbl

label define split_lbl 0 `"Person was not in a large group quarters that was split apart"'
label define split_lbl 1 `"Person was in a large group quarters that was split apart"', add
label values split split_lbl

label define famunit_lbl 01 `"1st family in household or group quarters"'
label define famunit_lbl 02 `"2nd family in household or group quarters"', add
label define famunit_lbl 03 `"3rd"', add
label define famunit_lbl 04 `"4th"', add
label define famunit_lbl 05 `"5th"', add
label define famunit_lbl 06 `"6th"', add
label define famunit_lbl 07 `"7th"', add
label define famunit_lbl 08 `"8th"', add
label define famunit_lbl 09 `"9th"', add
label define famunit_lbl 10 `"10th"', add
label define famunit_lbl 11 `"11th"', add
label define famunit_lbl 12 `"12th"', add
label define famunit_lbl 13 `"13th"', add
label define famunit_lbl 14 `"14th"', add
label define famunit_lbl 15 `"15th"', add
label define famunit_lbl 16 `"16th"', add
label define famunit_lbl 17 `"17th"', add
label define famunit_lbl 18 `"18th"', add
label define famunit_lbl 19 `"19th"', add
label define famunit_lbl 20 `"20th"', add
label define famunit_lbl 21 `"21th"', add
label define famunit_lbl 22 `"22th"', add
label define famunit_lbl 23 `"23th"', add
label define famunit_lbl 24 `"24th"', add
label define famunit_lbl 25 `"25th"', add
label define famunit_lbl 26 `"26th"', add
label define famunit_lbl 27 `"27th"', add
label define famunit_lbl 28 `"28th"', add
label define famunit_lbl 29 `"29th"', add
label define famunit_lbl 30 `"30th"', add
label values famunit famunit_lbl

label define famsize_lbl 01 `"1 family member present"'
label define famsize_lbl 02 `"2 family members present"', add
label define famsize_lbl 03 `"3"', add
label define famsize_lbl 04 `"4"', add
label define famsize_lbl 05 `"5"', add
label define famsize_lbl 06 `"6"', add
label define famsize_lbl 07 `"7"', add
label define famsize_lbl 08 `"8"', add
label define famsize_lbl 09 `"9"', add
label define famsize_lbl 10 `"10"', add
label define famsize_lbl 11 `"11"', add
label define famsize_lbl 12 `"12"', add
label define famsize_lbl 13 `"13"', add
label define famsize_lbl 14 `"14"', add
label define famsize_lbl 15 `"15"', add
label define famsize_lbl 16 `"16"', add
label define famsize_lbl 17 `"17"', add
label define famsize_lbl 18 `"18"', add
label define famsize_lbl 19 `"19"', add
label define famsize_lbl 20 `"20"', add
label define famsize_lbl 21 `"21"', add
label define famsize_lbl 22 `"22"', add
label define famsize_lbl 23 `"23"', add
label define famsize_lbl 24 `"24"', add
label define famsize_lbl 25 `"25"', add
label define famsize_lbl 26 `"26"', add
label define famsize_lbl 27 `"27"', add
label define famsize_lbl 28 `"28"', add
label define famsize_lbl 29 `"29"', add
label values famsize famsize_lbl

label define stepmom_lbl 0 `"No stepmother present"'
label define stepmom_lbl 1 `"Improbable age difference"', add
label define stepmom_lbl 2 `"Spouse of father"', add
label define stepmom_lbl 3 `"Identified stepmother"', add
label define stepmom_lbl 4 `"No surviving children"', add
label define stepmom_lbl 5 `"Identified as adopted"', add
label define stepmom_lbl 6 `"Birthplace/marriage duration mismatch"', add
label define stepmom_lbl 7 `"Number of children born/children surviving check"', add
label values stepmom stepmom_lbl

label define momrule_hist_lbl 0 `"No mother link"'
label define momrule_hist_lbl 1 `"Unambiguous mother link"', add
label define momrule_hist_lbl 2 `"Daughter/grandchild link"', add
label define momrule_hist_lbl 3 `"Preceding female (no intervening person)"', add
label define momrule_hist_lbl 4 `"Preceding female (surname similarity)"', add
label define momrule_hist_lbl 5 `"Daughter/grandchild (child surviving status)"', add
label define momrule_hist_lbl 6 `"Preceding female (child surviving status)"', add
label define momrule_hist_lbl 7 `"Spouse of father becomes stepmother"', add
label values momrule_hist momrule_hist_lbl

label define steppop_lbl 0 `"No stepfather present"'
label define steppop_lbl 1 `"Improbable age difference"', add
label define steppop_lbl 2 `"Spouse of mother"', add
label define steppop_lbl 3 `"Identified stepfather"', add
label define steppop_lbl 5 `"Identified as adopted"', add
label define steppop_lbl 6 `"Birthplace/marriage duration mismatch"', add
label define steppop_lbl 7 `"Surname difference -- male child or never-married female"', add
label values steppop steppop_lbl

label define poprule_hist_lbl 0 `"No father link"'
label define poprule_hist_lbl 1 `"Unambiguous father link"', add
label define poprule_hist_lbl 2 `"Son/granchild link"', add
label define poprule_hist_lbl 3 `"Preceding male (no intervening person)"', add
label define poprule_hist_lbl 4 `"Preceding male (surname similarity)"', add
label define poprule_hist_lbl 7 `"Husband of mother becomes stepfather"', add
label values poprule_hist poprule_hist_lbl

label define sprule_hist_lbl 0 `"No spouse link"'
label define sprule_hist_lbl 1 `"Wife follows husband"', add
label define sprule_hist_lbl 2 `"Wife precedes husband"', add
label define sprule_hist_lbl 3 `"Non-adjacent links -- consistent relationship to head/age differences"', add
label define sprule_hist_lbl 4 `"Adjacent links (wife follows husband -- no age, other relative conflicts)"', add
label define sprule_hist_lbl 5 `"Adjacent links (wife precedes husband -- no age, other relative conflicts)"', add
label define sprule_hist_lbl 6 `"Non-adjacent links -- no age, other relative conflicts"', add
label define sprule_hist_lbl 7 `"Previously allocated marital status -- no age, other relative conflicts"', add
label values sprule_hist sprule_hist_lbl

label define nchild_lbl 0 `"0 children present"'
label define nchild_lbl 1 `"1 child present"', add
label define nchild_lbl 2 `"2"', add
label define nchild_lbl 3 `"3"', add
label define nchild_lbl 4 `"4"', add
label define nchild_lbl 5 `"5"', add
label define nchild_lbl 6 `"6"', add
label define nchild_lbl 7 `"7"', add
label define nchild_lbl 8 `"8"', add
label define nchild_lbl 9 `"9+"', add
label values nchild nchild_lbl

label define nchlt5_lbl 0 `"No children under age 5"'
label define nchlt5_lbl 1 `"1 child under age 5"', add
label define nchlt5_lbl 2 `"2"', add
label define nchlt5_lbl 3 `"3"', add
label define nchlt5_lbl 4 `"4"', add
label define nchlt5_lbl 5 `"5"', add
label define nchlt5_lbl 6 `"6"', add
label define nchlt5_lbl 7 `"7"', add
label define nchlt5_lbl 8 `"8"', add
label define nchlt5_lbl 9 `"9+"', add
label values nchlt5 nchlt5_lbl

label define nsibs_lbl 0 `"0 siblings"'
label define nsibs_lbl 1 `"1 sibling"', add
label define nsibs_lbl 2 `"2 siblings"', add
label define nsibs_lbl 3 `"3 siblings"', add
label define nsibs_lbl 4 `"4 siblings"', add
label define nsibs_lbl 5 `"5 siblings"', add
label define nsibs_lbl 6 `"6 siblings"', add
label define nsibs_lbl 7 `"7 siblings"', add
label define nsibs_lbl 8 `"8 siblings"', add
label define nsibs_lbl 9 `"9 or more siblings"', add
label values nsibs nsibs_lbl

label define eldch_lbl 00 `"Less than 1 year old"'
label define eldch_lbl 01 `"1"', add
label define eldch_lbl 02 `"2"', add
label define eldch_lbl 03 `"3"', add
label define eldch_lbl 04 `"4"', add
label define eldch_lbl 05 `"5"', add
label define eldch_lbl 06 `"6"', add
label define eldch_lbl 07 `"7"', add
label define eldch_lbl 08 `"8"', add
label define eldch_lbl 09 `"9"', add
label define eldch_lbl 10 `"10"', add
label define eldch_lbl 11 `"11"', add
label define eldch_lbl 12 `"12"', add
label define eldch_lbl 13 `"13"', add
label define eldch_lbl 14 `"14"', add
label define eldch_lbl 15 `"15"', add
label define eldch_lbl 16 `"16"', add
label define eldch_lbl 17 `"17"', add
label define eldch_lbl 18 `"18"', add
label define eldch_lbl 19 `"19"', add
label define eldch_lbl 20 `"20"', add
label define eldch_lbl 21 `"21"', add
label define eldch_lbl 22 `"22"', add
label define eldch_lbl 23 `"23"', add
label define eldch_lbl 24 `"24"', add
label define eldch_lbl 25 `"25"', add
label define eldch_lbl 26 `"26"', add
label define eldch_lbl 27 `"27"', add
label define eldch_lbl 28 `"28"', add
label define eldch_lbl 29 `"29"', add
label define eldch_lbl 30 `"30"', add
label define eldch_lbl 31 `"31"', add
label define eldch_lbl 32 `"32"', add
label define eldch_lbl 33 `"33"', add
label define eldch_lbl 34 `"34"', add
label define eldch_lbl 35 `"35"', add
label define eldch_lbl 36 `"36"', add
label define eldch_lbl 37 `"37"', add
label define eldch_lbl 38 `"38"', add
label define eldch_lbl 39 `"39"', add
label define eldch_lbl 40 `"40"', add
label define eldch_lbl 41 `"41"', add
label define eldch_lbl 42 `"42"', add
label define eldch_lbl 43 `"43"', add
label define eldch_lbl 44 `"44"', add
label define eldch_lbl 45 `"45"', add
label define eldch_lbl 46 `"46"', add
label define eldch_lbl 47 `"47"', add
label define eldch_lbl 48 `"48"', add
label define eldch_lbl 49 `"49"', add
label define eldch_lbl 50 `"50"', add
label define eldch_lbl 51 `"51"', add
label define eldch_lbl 52 `"52"', add
label define eldch_lbl 53 `"53"', add
label define eldch_lbl 54 `"54"', add
label define eldch_lbl 55 `"55"', add
label define eldch_lbl 56 `"56"', add
label define eldch_lbl 57 `"57"', add
label define eldch_lbl 58 `"58"', add
label define eldch_lbl 59 `"59"', add
label define eldch_lbl 60 `"60"', add
label define eldch_lbl 61 `"61"', add
label define eldch_lbl 62 `"62"', add
label define eldch_lbl 63 `"63"', add
label define eldch_lbl 64 `"64"', add
label define eldch_lbl 65 `"65"', add
label define eldch_lbl 66 `"66"', add
label define eldch_lbl 67 `"67"', add
label define eldch_lbl 68 `"68"', add
label define eldch_lbl 69 `"69"', add
label define eldch_lbl 70 `"70"', add
label define eldch_lbl 71 `"71"', add
label define eldch_lbl 72 `"72"', add
label define eldch_lbl 73 `"73"', add
label define eldch_lbl 74 `"74"', add
label define eldch_lbl 75 `"75"', add
label define eldch_lbl 76 `"76"', add
label define eldch_lbl 77 `"77"', add
label define eldch_lbl 78 `"78"', add
label define eldch_lbl 79 `"79"', add
label define eldch_lbl 80 `"80"', add
label define eldch_lbl 81 `"81"', add
label define eldch_lbl 82 `"82"', add
label define eldch_lbl 83 `"83"', add
label define eldch_lbl 84 `"84"', add
label define eldch_lbl 85 `"85"', add
label define eldch_lbl 86 `"86"', add
label define eldch_lbl 87 `"87"', add
label define eldch_lbl 88 `"88"', add
label define eldch_lbl 89 `"89"', add
label define eldch_lbl 90 `"90"', add
label define eldch_lbl 91 `"91"', add
label define eldch_lbl 92 `"92"', add
label define eldch_lbl 93 `"93"', add
label define eldch_lbl 94 `"94"', add
label define eldch_lbl 95 `"95"', add
label define eldch_lbl 96 `"96"', add
label define eldch_lbl 97 `"97"', add
label define eldch_lbl 98 `"98"', add
label define eldch_lbl 99 `"N/A"', add
label values eldch eldch_lbl

label define yngch_lbl 00 `"Less than 1 year old"'
label define yngch_lbl 01 `"1"', add
label define yngch_lbl 02 `"2"', add
label define yngch_lbl 03 `"3"', add
label define yngch_lbl 04 `"4"', add
label define yngch_lbl 05 `"5"', add
label define yngch_lbl 06 `"6"', add
label define yngch_lbl 07 `"7"', add
label define yngch_lbl 08 `"8"', add
label define yngch_lbl 09 `"9"', add
label define yngch_lbl 10 `"10"', add
label define yngch_lbl 11 `"11"', add
label define yngch_lbl 12 `"12"', add
label define yngch_lbl 13 `"13"', add
label define yngch_lbl 14 `"14"', add
label define yngch_lbl 15 `"15"', add
label define yngch_lbl 16 `"16"', add
label define yngch_lbl 17 `"17"', add
label define yngch_lbl 18 `"18"', add
label define yngch_lbl 19 `"19"', add
label define yngch_lbl 20 `"20"', add
label define yngch_lbl 21 `"21"', add
label define yngch_lbl 22 `"22"', add
label define yngch_lbl 23 `"23"', add
label define yngch_lbl 24 `"24"', add
label define yngch_lbl 25 `"25"', add
label define yngch_lbl 26 `"26"', add
label define yngch_lbl 27 `"27"', add
label define yngch_lbl 28 `"28"', add
label define yngch_lbl 29 `"29"', add
label define yngch_lbl 30 `"30"', add
label define yngch_lbl 31 `"31"', add
label define yngch_lbl 32 `"32"', add
label define yngch_lbl 33 `"33"', add
label define yngch_lbl 34 `"34"', add
label define yngch_lbl 35 `"35"', add
label define yngch_lbl 36 `"36"', add
label define yngch_lbl 37 `"37"', add
label define yngch_lbl 38 `"38"', add
label define yngch_lbl 39 `"39"', add
label define yngch_lbl 40 `"40"', add
label define yngch_lbl 41 `"41"', add
label define yngch_lbl 42 `"42"', add
label define yngch_lbl 43 `"43"', add
label define yngch_lbl 44 `"44"', add
label define yngch_lbl 45 `"45"', add
label define yngch_lbl 46 `"46"', add
label define yngch_lbl 47 `"47"', add
label define yngch_lbl 48 `"48"', add
label define yngch_lbl 49 `"49"', add
label define yngch_lbl 50 `"50"', add
label define yngch_lbl 51 `"51"', add
label define yngch_lbl 52 `"52"', add
label define yngch_lbl 53 `"53"', add
label define yngch_lbl 54 `"54"', add
label define yngch_lbl 55 `"55"', add
label define yngch_lbl 56 `"56"', add
label define yngch_lbl 57 `"57"', add
label define yngch_lbl 58 `"58"', add
label define yngch_lbl 59 `"59"', add
label define yngch_lbl 60 `"60"', add
label define yngch_lbl 61 `"61"', add
label define yngch_lbl 62 `"62"', add
label define yngch_lbl 63 `"63"', add
label define yngch_lbl 64 `"64"', add
label define yngch_lbl 65 `"65"', add
label define yngch_lbl 66 `"66"', add
label define yngch_lbl 67 `"67"', add
label define yngch_lbl 68 `"68"', add
label define yngch_lbl 69 `"69"', add
label define yngch_lbl 70 `"70"', add
label define yngch_lbl 71 `"71"', add
label define yngch_lbl 72 `"72"', add
label define yngch_lbl 73 `"73"', add
label define yngch_lbl 74 `"74"', add
label define yngch_lbl 75 `"75"', add
label define yngch_lbl 76 `"76"', add
label define yngch_lbl 77 `"77"', add
label define yngch_lbl 78 `"78"', add
label define yngch_lbl 79 `"79"', add
label define yngch_lbl 80 `"80"', add
label define yngch_lbl 81 `"81"', add
label define yngch_lbl 82 `"82"', add
label define yngch_lbl 83 `"83"', add
label define yngch_lbl 84 `"84"', add
label define yngch_lbl 85 `"85"', add
label define yngch_lbl 86 `"86"', add
label define yngch_lbl 87 `"87"', add
label define yngch_lbl 88 `"88"', add
label define yngch_lbl 89 `"89"', add
label define yngch_lbl 90 `"90"', add
label define yngch_lbl 91 `"91"', add
label define yngch_lbl 92 `"92"', add
label define yngch_lbl 93 `"93"', add
label define yngch_lbl 94 `"94"', add
label define yngch_lbl 95 `"95"', add
label define yngch_lbl 96 `"96"', add
label define yngch_lbl 97 `"97"', add
label define yngch_lbl 98 `"98"', add
label define yngch_lbl 99 `"N/A"', add
label values yngch yngch_lbl

label define relate_lbl 01 `"Head/Householder"'
label define relate_lbl 02 `"Spouse"', add
label define relate_lbl 03 `"Child"', add
label define relate_lbl 04 `"Child-in-law"', add
label define relate_lbl 05 `"Parent"', add
label define relate_lbl 06 `"Parent-in-Law"', add
label define relate_lbl 07 `"Sibling"', add
label define relate_lbl 08 `"Sibling-in-Law"', add
label define relate_lbl 09 `"Grandchild"', add
label define relate_lbl 10 `"Other relatives"', add
label define relate_lbl 11 `"Partner, friend, visitor"', add
label define relate_lbl 12 `"Other non-relatives"', add
label define relate_lbl 13 `"Institutional inmates"', add
label values relate relate_lbl

label define related_lbl 0101 `"Head/Householder"'
label define related_lbl 0201 `"Spouse"', add
label define related_lbl 0202 `"2nd/3rd Wife (Polygamous)"', add
label define related_lbl 0301 `"Child"', add
label define related_lbl 0302 `"Adopted Child"', add
label define related_lbl 0303 `"Stepchild"', add
label define related_lbl 0304 `"Adopted, n.s."', add
label define related_lbl 0401 `"Child-in-law"', add
label define related_lbl 0402 `"Step Child-in-law"', add
label define related_lbl 0501 `"Parent"', add
label define related_lbl 0502 `"Stepparent"', add
label define related_lbl 0601 `"Parent-in-Law"', add
label define related_lbl 0602 `"Stepparent-in-law"', add
label define related_lbl 0701 `"Sibling"', add
label define related_lbl 0702 `"Step/Half/Adopted Sibling"', add
label define related_lbl 0801 `"Sibling-in-Law"', add
label define related_lbl 0802 `"Step/Half Sibling-in-law"', add
label define related_lbl 0901 `"Grandchild"', add
label define related_lbl 0902 `"Adopted Grandchild"', add
label define related_lbl 0903 `"Step Grandchild"', add
label define related_lbl 0904 `"Grandchild-in-law"', add
label define related_lbl 1000 `"Other relatives:"', add
label define related_lbl 1001 `"Other Relatives"', add
label define related_lbl 1011 `"Grandparent"', add
label define related_lbl 1012 `"Step Grandparent"', add
label define related_lbl 1013 `"Grandparent-in-law"', add
label define related_lbl 1021 `"Aunt or Uncle"', add
label define related_lbl 1022 `"Aunt,Uncle-in-law"', add
label define related_lbl 1031 `"Nephew, Niece"', add
label define related_lbl 1032 `"Neph/Niece-in-law"', add
label define related_lbl 1033 `"Step/Adopted Nephew/Niece"', add
label define related_lbl 1034 `"Grand Niece/Nephew"', add
label define related_lbl 1041 `"Cousin"', add
label define related_lbl 1042 `"Cousin-in-law"', add
label define related_lbl 1051 `"Great Grandchild"', add
label define related_lbl 1061 `"Other relatives, nec"', add
label define related_lbl 1100 `"Partner, Friend, Visitor"', add
label define related_lbl 1110 `"Partner/friend"', add
label define related_lbl 1111 `"Friend"', add
label define related_lbl 1112 `"Partner"', add
label define related_lbl 1113 `"Partner/roommate"', add
label define related_lbl 1114 `"Unmarried Partner"', add
label define related_lbl 1115 `"Housemate/Roomate"', add
label define related_lbl 1120 `"Relative of partner"', add
label define related_lbl 1130 `"Concubine/Mistress"', add
label define related_lbl 1131 `"Visitor"', add
label define related_lbl 1132 `"Companion and family of companion"', add
label define related_lbl 1139 `"Allocated partner/friend/visitor"', add
label define related_lbl 1200 `"Other non-relatives"', add
label define related_lbl 1201 `"Roomers/boarders/lodgers"', add
label define related_lbl 1202 `"Boarders"', add
label define related_lbl 1203 `"Lodgers"', add
label define related_lbl 1204 `"Roomer"', add
label define related_lbl 1205 `"Tenant"', add
label define related_lbl 1206 `"Foster child"', add
label define related_lbl 1210 `"Employees:"', add
label define related_lbl 1211 `"Servant"', add
label define related_lbl 1212 `"Housekeeper"', add
label define related_lbl 1213 `"Maid"', add
label define related_lbl 1214 `"Cook"', add
label define related_lbl 1215 `"Nurse"', add
label define related_lbl 1216 `"Other probable domestic employee"', add
label define related_lbl 1217 `"Other employee"', add
label define related_lbl 1219 `"Relative of employee"', add
label define related_lbl 1221 `"Military"', add
label define related_lbl 1222 `"Students"', add
label define related_lbl 1223 `"Members of religious orders"', add
label define related_lbl 1230 `"Other non-relatives"', add
label define related_lbl 1239 `"Allocated other non-relative"', add
label define related_lbl 1240 `"Roomers/boarders/lodgers and foster children"', add
label define related_lbl 1241 `"Roomers/boarders/lodgers"', add
label define related_lbl 1242 `"Foster children"', add
label define related_lbl 1250 `"Employees"', add
label define related_lbl 1251 `"Domestic employees"', add
label define related_lbl 1252 `"Non-domestic employees"', add
label define related_lbl 1253 `"Relative of employee"', add
label define related_lbl 1260 `"Other non-relatives (1990 includes employees)"', add
label define related_lbl 1270 `"Non-inmate 1990"', add
label define related_lbl 1281 `"Head of group quarters"', add
label define related_lbl 1282 `"Employees of group quarters"', add
label define related_lbl 1283 `"Relative of head, staff, or employee group quarters"', add
label define related_lbl 1284 `"Other non-inmate 1940-1959"', add
label define related_lbl 1291 `"Military"', add
label define related_lbl 1292 `"College dormitories"', add
label define related_lbl 1293 `"Residents of rooming houses"', add
label define related_lbl 1294 `"Other non-inmate 1980 (includes employees and non-inmates in"', add
label define related_lbl 1295 `"Other non-inmates 1960-1970 (includes employees)"', add
label define related_lbl 1296 `"Non-inmates in institutions"', add
label define related_lbl 1301 `"Institutional inmates"', add
label define related_lbl 9996 `"Unclassifiable"', add
label define related_lbl 9997 `"Unknown"', add
label define related_lbl 9998 `"Illegible"', add
label define related_lbl 9999 `"Missing"', add
label values related related_lbl

label define imprel_lbl 00 `"Not applicable (oversamples in 1900-1910)"'
label define imprel_lbl 01 `"Head/Householder"', add
label define imprel_lbl 02 `"Spouse"', add
label define imprel_lbl 03 `"Child"', add
label define imprel_lbl 04 `"Child-in-law"', add
label define imprel_lbl 05 `"Parent"', add
label define imprel_lbl 06 `"Parent-in-Law"', add
label define imprel_lbl 07 `"Sibling"', add
label define imprel_lbl 08 `"Sibling-in-Law"', add
label define imprel_lbl 09 `"Grandchild"', add
label define imprel_lbl 10 `"Other relatives"', add
label define imprel_lbl 11 `"Partner, friend, visitor"', add
label define imprel_lbl 12 `"Other non-relatives"', add
label define imprel_lbl 13 `"Institutional inmates"', add
label values imprel imprel_lbl

label define sex_lbl 1 `"Male"'
label define sex_lbl 2 `"Female"', add
label values sex sex_lbl

label define age_lbl 000 `"Less than 1 year old"'
label define age_lbl 001 `"1"', add
label define age_lbl 002 `"2"', add
label define age_lbl 003 `"3"', add
label define age_lbl 004 `"4"', add
label define age_lbl 005 `"5"', add
label define age_lbl 006 `"6"', add
label define age_lbl 007 `"7"', add
label define age_lbl 008 `"8"', add
label define age_lbl 009 `"9"', add
label define age_lbl 010 `"10"', add
label define age_lbl 011 `"11"', add
label define age_lbl 012 `"12"', add
label define age_lbl 013 `"13"', add
label define age_lbl 014 `"14"', add
label define age_lbl 015 `"15"', add
label define age_lbl 016 `"16"', add
label define age_lbl 017 `"17"', add
label define age_lbl 018 `"18"', add
label define age_lbl 019 `"19"', add
label define age_lbl 020 `"20"', add
label define age_lbl 021 `"21"', add
label define age_lbl 022 `"22"', add
label define age_lbl 023 `"23"', add
label define age_lbl 024 `"24"', add
label define age_lbl 025 `"25"', add
label define age_lbl 026 `"26"', add
label define age_lbl 027 `"27"', add
label define age_lbl 028 `"28"', add
label define age_lbl 029 `"29"', add
label define age_lbl 030 `"30"', add
label define age_lbl 031 `"31"', add
label define age_lbl 032 `"32"', add
label define age_lbl 033 `"33"', add
label define age_lbl 034 `"34"', add
label define age_lbl 035 `"35"', add
label define age_lbl 036 `"36"', add
label define age_lbl 037 `"37"', add
label define age_lbl 038 `"38"', add
label define age_lbl 039 `"39"', add
label define age_lbl 040 `"40"', add
label define age_lbl 041 `"41"', add
label define age_lbl 042 `"42"', add
label define age_lbl 043 `"43"', add
label define age_lbl 044 `"44"', add
label define age_lbl 045 `"45"', add
label define age_lbl 046 `"46"', add
label define age_lbl 047 `"47"', add
label define age_lbl 048 `"48"', add
label define age_lbl 049 `"49"', add
label define age_lbl 050 `"50"', add
label define age_lbl 051 `"51"', add
label define age_lbl 052 `"52"', add
label define age_lbl 053 `"53"', add
label define age_lbl 054 `"54"', add
label define age_lbl 055 `"55"', add
label define age_lbl 056 `"56"', add
label define age_lbl 057 `"57"', add
label define age_lbl 058 `"58"', add
label define age_lbl 059 `"59"', add
label define age_lbl 060 `"60"', add
label define age_lbl 061 `"61"', add
label define age_lbl 062 `"62"', add
label define age_lbl 063 `"63"', add
label define age_lbl 064 `"64"', add
label define age_lbl 065 `"65"', add
label define age_lbl 066 `"66"', add
label define age_lbl 067 `"67"', add
label define age_lbl 068 `"68"', add
label define age_lbl 069 `"69"', add
label define age_lbl 070 `"70"', add
label define age_lbl 071 `"71"', add
label define age_lbl 072 `"72"', add
label define age_lbl 073 `"73"', add
label define age_lbl 074 `"74"', add
label define age_lbl 075 `"75"', add
label define age_lbl 076 `"76"', add
label define age_lbl 077 `"77"', add
label define age_lbl 078 `"78"', add
label define age_lbl 079 `"79"', add
label define age_lbl 080 `"80"', add
label define age_lbl 081 `"81"', add
label define age_lbl 082 `"82"', add
label define age_lbl 083 `"83"', add
label define age_lbl 084 `"84"', add
label define age_lbl 085 `"85"', add
label define age_lbl 086 `"86"', add
label define age_lbl 087 `"87"', add
label define age_lbl 088 `"88"', add
label define age_lbl 089 `"89"', add
label define age_lbl 090 `"90 (90+ in 1980 and 1990)"', add
label define age_lbl 091 `"91"', add
label define age_lbl 092 `"92"', add
label define age_lbl 093 `"93"', add
label define age_lbl 094 `"94"', add
label define age_lbl 095 `"95"', add
label define age_lbl 096 `"96"', add
label define age_lbl 097 `"97"', add
label define age_lbl 098 `"98"', add
label define age_lbl 099 `"99"', add
label define age_lbl 100 `"100 (100+ in 1960-1970)"', add
label define age_lbl 101 `"101"', add
label define age_lbl 102 `"102"', add
label define age_lbl 103 `"103"', add
label define age_lbl 104 `"104"', add
label define age_lbl 105 `"105"', add
label define age_lbl 106 `"106"', add
label define age_lbl 107 `"107"', add
label define age_lbl 108 `"108"', add
label define age_lbl 109 `"109"', add
label define age_lbl 110 `"110"', add
label define age_lbl 111 `"111"', add
label define age_lbl 112 `"112 (112+ in the 1980 internal data)"', add
label define age_lbl 113 `"113"', add
label define age_lbl 114 `"114"', add
label define age_lbl 115 `"115 (115+ in the 1990 internal data)"', add
label define age_lbl 116 `"116"', add
label define age_lbl 117 `"117"', add
label define age_lbl 118 `"118"', add
label define age_lbl 119 `"119"', add
label define age_lbl 120 `"120"', add
label define age_lbl 121 `"121"', add
label define age_lbl 122 `"122"', add
label define age_lbl 123 `"123"', add
label define age_lbl 124 `"124"', add
label define age_lbl 125 `"125"', add
label define age_lbl 126 `"126"', add
label define age_lbl 129 `"129"', add
label define age_lbl 130 `"130"', add
label define age_lbl 135 `"135"', add
label values age age_lbl

label define agemonth_lbl 00 `"0 months old"'
label define agemonth_lbl 01 `"1 month old"', add
label define agemonth_lbl 02 `"2"', add
label define agemonth_lbl 03 `"3"', add
label define agemonth_lbl 04 `"4"', add
label define agemonth_lbl 05 `"5"', add
label define agemonth_lbl 06 `"6"', add
label define agemonth_lbl 07 `"7"', add
label define agemonth_lbl 08 `"8"', add
label define agemonth_lbl 09 `"9"', add
label define agemonth_lbl 10 `"10"', add
label define agemonth_lbl 11 `"11"', add
label define agemonth_lbl 12 `"12"', add
label define agemonth_lbl 98 `"Unknown/illegible"', add
label define agemonth_lbl 99 `"N/A or blank"', add
label values agemonth agemonth_lbl

label define race_lbl 1 `"White"'
label define race_lbl 2 `"Black/African American/Negro"', add
label define race_lbl 3 `"American Indian or Alaska Native"', add
label define race_lbl 4 `"Chinese"', add
label define race_lbl 5 `"Japanese"', add
label define race_lbl 6 `"Other Asian or Pacific Islander"', add
label define race_lbl 7 `"Other race, nec"', add
label define race_lbl 8 `"Two major races"', add
label define race_lbl 9 `"Three or more major races"', add
label values race race_lbl

label define raced_lbl 100 `"White"'
label define raced_lbl 110 `"Spanish write_in"', add
label define raced_lbl 120 `"Blank (white) (1850)"', add
label define raced_lbl 130 `"Portuguese"', add
label define raced_lbl 140 `"Mexican (1930)"', add
label define raced_lbl 150 `"Puerto Rican (1910 Hawaii)"', add
label define raced_lbl 200 `"Black/African American/Negro"', add
label define raced_lbl 210 `"Mulatto"', add
label define raced_lbl 300 `"American Indian/Alaska Native"', add
label define raced_lbl 302 `"Apache"', add
label define raced_lbl 303 `"Blackfoot"', add
label define raced_lbl 304 `"Cherokee"', add
label define raced_lbl 305 `"Cheyenne"', add
label define raced_lbl 306 `"Chickasaw"', add
label define raced_lbl 307 `"Chippewa"', add
label define raced_lbl 308 `"Choctaw"', add
label define raced_lbl 309 `"Comanche"', add
label define raced_lbl 310 `"Creek"', add
label define raced_lbl 311 `"Crow"', add
label define raced_lbl 312 `"Iroquois"', add
label define raced_lbl 313 `"Kiowa"', add
label define raced_lbl 314 `"Lumbee"', add
label define raced_lbl 315 `"Navajo"', add
label define raced_lbl 316 `"Osage"', add
label define raced_lbl 317 `"Paiute"', add
label define raced_lbl 318 `"Pima"', add
label define raced_lbl 319 `"Potawatomi"', add
label define raced_lbl 320 `"Pueblo"', add
label define raced_lbl 321 `"Seminole"', add
label define raced_lbl 322 `"Shoshone"', add
label define raced_lbl 323 `"Sioux"', add
label define raced_lbl 324 `"Tlingit (Tlingit_Haida, 2000/ACS)"', add
label define raced_lbl 325 `"Tohono O Odham"', add
label define raced_lbl 326 `"All other tribes (1990)"', add
label define raced_lbl 328 `"Hopi"', add
label define raced_lbl 329 `"Central American Indian"', add
label define raced_lbl 330 `"Spanish American Indian"', add
label define raced_lbl 350 `"Delaware"', add
label define raced_lbl 351 `"Latin American Indian"', add
label define raced_lbl 352 `"Puget Sound Salish"', add
label define raced_lbl 353 `"Yakama"', add
label define raced_lbl 354 `"Yaqui"', add
label define raced_lbl 355 `"Colville"', add
label define raced_lbl 356 `"Houma"', add
label define raced_lbl 357 `"Menominee"', add
label define raced_lbl 358 `"Yuman"', add
label define raced_lbl 359 `"South American Indian"', add
label define raced_lbl 360 `"Mexican American Indian"', add
label define raced_lbl 361 `"Other Amer. Indian tribe (2000,ACS)"', add
label define raced_lbl 362 `"2+ Amer. Indian tribes (2000,ACS)"', add
label define raced_lbl 370 `"Alaskan Athabaskan"', add
label define raced_lbl 371 `"Aleut"', add
label define raced_lbl 372 `"Eskimo"', add
label define raced_lbl 373 `"Alaskan mixed"', add
label define raced_lbl 374 `"Inupiat"', add
label define raced_lbl 375 `"Yup'ik"', add
label define raced_lbl 379 `"Other Alaska Native tribe(s) (2000,ACS)"', add
label define raced_lbl 398 `"Both Am. Ind. and Alaska Native (2000,ACS)"', add
label define raced_lbl 399 `"Tribe not specified"', add
label define raced_lbl 400 `"Chinese"', add
label define raced_lbl 410 `"Taiwanese"', add
label define raced_lbl 420 `"Chinese and Taiwanese"', add
label define raced_lbl 500 `"Japanese"', add
label define raced_lbl 600 `"Filipino"', add
label define raced_lbl 610 `"Asian Indian (Hindu 1920_1940)"', add
label define raced_lbl 620 `"Korean"', add
label define raced_lbl 630 `"Hawaiian"', add
label define raced_lbl 631 `"Hawaiian and Asian (1900,1920)"', add
label define raced_lbl 632 `"Hawaiian and European (1900,1920)"', add
label define raced_lbl 634 `"Hawaiian mixed"', add
label define raced_lbl 640 `"Vietnamese"', add
label define raced_lbl 641 `"Bhutanese"', add
label define raced_lbl 642 `"Mongolian"', add
label define raced_lbl 643 `"Nepalese"', add
label define raced_lbl 650 `"Other Asian or Pacific Islander (1920,1980)"', add
label define raced_lbl 651 `"Asian only (CPS)"', add
label define raced_lbl 652 `"Pacific Islander only (CPS)"', add
label define raced_lbl 653 `"Asian or Pacific Islander, n.s. (1990 Internal Census files)"', add
label define raced_lbl 660 `"Cambodian"', add
label define raced_lbl 661 `"Hmong"', add
label define raced_lbl 662 `"Laotian"', add
label define raced_lbl 663 `"Thai"', add
label define raced_lbl 664 `"Bangladeshi"', add
label define raced_lbl 665 `"Burmese"', add
label define raced_lbl 666 `"Indonesian"', add
label define raced_lbl 667 `"Malaysian"', add
label define raced_lbl 668 `"Okinawan"', add
label define raced_lbl 669 `"Pakistani"', add
label define raced_lbl 670 `"Sri Lankan"', add
label define raced_lbl 671 `"Other Asian, n.e.c."', add
label define raced_lbl 672 `"Asian, not specified"', add
label define raced_lbl 673 `"Chinese and Japanese"', add
label define raced_lbl 674 `"Chinese and Filipino"', add
label define raced_lbl 675 `"Chinese and Vietnamese"', add
label define raced_lbl 676 `"Chinese and Asian write_in"', add
label define raced_lbl 677 `"Japanese and Filipino"', add
label define raced_lbl 678 `"Asian Indian and Asian write_in"', add
label define raced_lbl 679 `"Other Asian race combinations"', add
label define raced_lbl 680 `"Samoan"', add
label define raced_lbl 681 `"Tahitian"', add
label define raced_lbl 682 `"Tongan"', add
label define raced_lbl 683 `"Other Polynesian (1990)"', add
label define raced_lbl 684 `"1+ other Polynesian races (2000,ACS)"', add
label define raced_lbl 685 `"Guamanian/Chamorro"', add
label define raced_lbl 686 `"Northern Mariana Islander"', add
label define raced_lbl 687 `"Palauan"', add
label define raced_lbl 688 `"Other Micronesian (1990)"', add
label define raced_lbl 689 `"1+ other Micronesian races (2000,ACS)"', add
label define raced_lbl 690 `"Fijian"', add
label define raced_lbl 691 `"Other Melanesian (1990)"', add
label define raced_lbl 692 `"1+ other Melanesian races (2000,ACS)"', add
label define raced_lbl 698 `"2+ PI races from 2+ PI regions"', add
label define raced_lbl 699 `"Pacific Islander, n.s."', add
label define raced_lbl 700 `"Other race, n.e.c."', add
label define raced_lbl 801 `"White and Black"', add
label define raced_lbl 802 `"White and AIAN"', add
label define raced_lbl 810 `"White and Asian"', add
label define raced_lbl 811 `"White and Chinese"', add
label define raced_lbl 812 `"White and Japanese"', add
label define raced_lbl 813 `"White and Filipino"', add
label define raced_lbl 814 `"White and Asian Indian"', add
label define raced_lbl 815 `"White and Korean"', add
label define raced_lbl 816 `"White and Vietnamese"', add
label define raced_lbl 817 `"White and Asian write_in"', add
label define raced_lbl 818 `"White and other Asian race(s)"', add
label define raced_lbl 819 `"White and two or more Asian groups"', add
label define raced_lbl 820 `"White and PI"', add
label define raced_lbl 821 `"White and Native Hawaiian"', add
label define raced_lbl 822 `"White and Samoan"', add
label define raced_lbl 823 `"White and Guamanian"', add
label define raced_lbl 824 `"White and PI write_in"', add
label define raced_lbl 825 `"White and other PI race(s)"', add
label define raced_lbl 826 `"White and other race write_in"', add
label define raced_lbl 827 `"White and other race, n.e.c."', add
label define raced_lbl 830 `"Black and AIAN"', add
label define raced_lbl 831 `"Black and Asian"', add
label define raced_lbl 832 `"Black and Chinese"', add
label define raced_lbl 833 `"Black and Japanese"', add
label define raced_lbl 834 `"Black and Filipino"', add
label define raced_lbl 835 `"Black and Asian Indian"', add
label define raced_lbl 836 `"Black and Korean"', add
label define raced_lbl 837 `"Black and Asian write_in"', add
label define raced_lbl 838 `"Black and other Asian race(s)"', add
label define raced_lbl 840 `"Black and PI"', add
label define raced_lbl 841 `"Black and PI write_in"', add
label define raced_lbl 842 `"Black and other PI race(s)"', add
label define raced_lbl 845 `"Black and other race write_in"', add
label define raced_lbl 850 `"AIAN and Asian"', add
label define raced_lbl 851 `"AIAN and Filipino (2000 1%)"', add
label define raced_lbl 852 `"AIAN and Asian Indian"', add
label define raced_lbl 853 `"AIAN and Asian write_in (2000 1%)"', add
label define raced_lbl 854 `"AIAN and other Asian race(s)"', add
label define raced_lbl 855 `"AIAN and PI"', add
label define raced_lbl 856 `"AIAN and other race write_in"', add
label define raced_lbl 860 `"Asian and PI"', add
label define raced_lbl 861 `"Chinese and Hawaiian"', add
label define raced_lbl 862 `"Chinese, Filipino, Hawaiian (2000 1%)"', add
label define raced_lbl 863 `"Japanese and Hawaiian (2000 1%)"', add
label define raced_lbl 864 `"Filipino and Hawaiian"', add
label define raced_lbl 865 `"Filipino and PI write_in"', add
label define raced_lbl 866 `"Asian Indian and PI write_in (2000 1%)"', add
label define raced_lbl 867 `"Asian write_in and PI write_in"', add
label define raced_lbl 868 `"Other Asian race(s) and PI race(s)"', add
label define raced_lbl 869 `"Japanese and Korean (ACS)"', add
label define raced_lbl 880 `"Asian and other race write_in"', add
label define raced_lbl 881 `"Chinese and other race write_in"', add
label define raced_lbl 882 `"Japanese and other race write_in"', add
label define raced_lbl 883 `"Filipino and other race write_in"', add
label define raced_lbl 884 `"Asian Indian and other race write_in"', add
label define raced_lbl 885 `"Asian write_in and other race write_in"', add
label define raced_lbl 886 `"Other Asian race(s) and other race write_in"', add
label define raced_lbl 887 `"Chinese and Korean"', add
label define raced_lbl 890 `"PI and other race write_in:"', add
label define raced_lbl 891 `"PI write_in and other race write_in"', add
label define raced_lbl 892 `"Other PI race(s) and other race write_in"', add
label define raced_lbl 893 `"Native Hawaiian or PI other race(s)"', add
label define raced_lbl 899 `"API and other race write_in"', add
label define raced_lbl 901 `"White, Black, AIAN"', add
label define raced_lbl 902 `"White, Black, Asian"', add
label define raced_lbl 903 `"White, Black, PI"', add
label define raced_lbl 904 `"White, Black, other race write_in"', add
label define raced_lbl 905 `"White, AIAN, Asian"', add
label define raced_lbl 906 `"White, AIAN, PI"', add
label define raced_lbl 907 `"White, AIAN, other race write_in"', add
label define raced_lbl 910 `"White, Asian, PI"', add
label define raced_lbl 911 `"White, Chinese, Hawaiian"', add
label define raced_lbl 912 `"White, Chinese, Filipino, Hawaiian (2000 1%)"', add
label define raced_lbl 913 `"White, Japanese, Hawaiian (2000 1%)"', add
label define raced_lbl 914 `"White, Filipino, Hawaiian"', add
label define raced_lbl 915 `"Other White, Asian race(s), PI race(s)"', add
label define raced_lbl 916 `"White, AIAN and Filipino"', add
label define raced_lbl 917 `"White, Black, and Filipino"', add
label define raced_lbl 920 `"White, Asian, other race write_in"', add
label define raced_lbl 921 `"White, Filipino, other race write_in (2000 1%)"', add
label define raced_lbl 922 `"White, Asian write_in, other race write_in (2000 1%)"', add
label define raced_lbl 923 `"Other White, Asian race(s), other race write_in (2000 1%)"', add
label define raced_lbl 925 `"White, PI, other race write_in"', add
label define raced_lbl 930 `"Black, AIAN, Asian"', add
label define raced_lbl 931 `"Black, AIAN, PI"', add
label define raced_lbl 932 `"Black, AIAN, other race write_in"', add
label define raced_lbl 933 `"Black, Asian, PI"', add
label define raced_lbl 934 `"Black, Asian, other race write_in"', add
label define raced_lbl 935 `"Black, PI, other race write_in"', add
label define raced_lbl 940 `"AIAN, Asian, PI"', add
label define raced_lbl 941 `"AIAN, Asian, other race write_in"', add
label define raced_lbl 942 `"AIAN, PI, other race write_in"', add
label define raced_lbl 943 `"Asian, PI, other race write_in"', add
label define raced_lbl 944 `"Asian (Chinese, Japanese, Korean, Vietnamese); and Native Hawaiian or PI; and Other"', add
label define raced_lbl 949 `"2 or 3 races (CPS)"', add
label define raced_lbl 950 `"White, Black, AIAN, Asian"', add
label define raced_lbl 951 `"White, Black, AIAN, PI"', add
label define raced_lbl 952 `"White, Black, AIAN, other race write_in"', add
label define raced_lbl 953 `"White, Black, Asian, PI"', add
label define raced_lbl 954 `"White, Black, Asian, other race write_in"', add
label define raced_lbl 955 `"White, Black, PI, other race write_in"', add
label define raced_lbl 960 `"White, AIAN, Asian, PI"', add
label define raced_lbl 961 `"White, AIAN, Asian, other race write_in"', add
label define raced_lbl 962 `"White, AIAN, PI, other race write_in"', add
label define raced_lbl 963 `"White, Asian, PI, other race write_in"', add
label define raced_lbl 964 `"White, Chinese, Japanese, Native Hawaiian"', add
label define raced_lbl 970 `"Black, AIAN, Asian, PI"', add
label define raced_lbl 971 `"Black, AIAN, Asian, other race write_in"', add
label define raced_lbl 972 `"Black, AIAN, PI, other race write_in"', add
label define raced_lbl 973 `"Black, Asian, PI, other race write_in"', add
label define raced_lbl 974 `"AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 975 `"AIAN, Asian, PI, Hawaiian other race write_in"', add
label define raced_lbl 976 `"Two specified Asian  (Chinese and other Asian, Chinese and Japanese, Japanese and other Asian, Korean and other Asian); Native Hawaiian/PI; and Other Race"', add
label define raced_lbl 980 `"White, Black, AIAN, Asian, PI"', add
label define raced_lbl 981 `"White, Black, AIAN, Asian, other race write_in"', add
label define raced_lbl 982 `"White, Black, AIAN, PI, other race write_in"', add
label define raced_lbl 983 `"White, Black, Asian, PI, other race write_in"', add
label define raced_lbl 984 `"White, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 985 `"Black, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 986 `"Black, AIAN, Asian, PI, Hawaiian, other race write_in"', add
label define raced_lbl 989 `"4 or 5 races (CPS)"', add
label define raced_lbl 990 `"White, Black, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 991 `"White race; Some other race; Black or African American race and/or American Indian and Alaska Native race and/or Asian groups and/or Native Hawaiian and Other Pacific Islander groups"', add
label define raced_lbl 996 `"2+ races, n.e.c. (CPS)"', add
label values raced raced_lbl

label define hispan_lbl 0 `"Not Hispanic"'
label define hispan_lbl 1 `"Mexican"', add
label define hispan_lbl 2 `"Puerto Rican"', add
label define hispan_lbl 3 `"Cuban"', add
label define hispan_lbl 4 `"Other"', add
label define hispan_lbl 9 `"Not Reported"', add
label values hispan hispan_lbl

label define hispand_lbl 000 `"Not Hispanic"'
label define hispand_lbl 100 `"Mexican"', add
label define hispand_lbl 102 `"Mexican American"', add
label define hispand_lbl 103 `"Mexicano/Mexicana"', add
label define hispand_lbl 104 `"Chicano/Chicana"', add
label define hispand_lbl 105 `"La Raza"', add
label define hispand_lbl 106 `"Mexican American Indian"', add
label define hispand_lbl 107 `"Mexico"', add
label define hispand_lbl 200 `"Puerto Rican"', add
label define hispand_lbl 300 `"Cuban"', add
label define hispand_lbl 401 `"Central American Indian"', add
label define hispand_lbl 402 `"Canal Zone"', add
label define hispand_lbl 411 `"Costa Rican"', add
label define hispand_lbl 412 `"Guatemalan"', add
label define hispand_lbl 413 `"Honduran"', add
label define hispand_lbl 414 `"Nicaraguan"', add
label define hispand_lbl 415 `"Panamanian"', add
label define hispand_lbl 416 `"Salvadoran"', add
label define hispand_lbl 417 `"Central American, n.e.c."', add
label define hispand_lbl 420 `"Argentinean"', add
label define hispand_lbl 421 `"Bolivian"', add
label define hispand_lbl 422 `"Chilean"', add
label define hispand_lbl 423 `"Colombian"', add
label define hispand_lbl 424 `"Ecuadorian"', add
label define hispand_lbl 425 `"Paraguayan"', add
label define hispand_lbl 426 `"Peruvian"', add
label define hispand_lbl 427 `"Uruguayan"', add
label define hispand_lbl 428 `"Venezuelan"', add
label define hispand_lbl 429 `"South American Indian"', add
label define hispand_lbl 430 `"Criollo"', add
label define hispand_lbl 431 `"South American, n.e.c."', add
label define hispand_lbl 450 `"Spaniard"', add
label define hispand_lbl 451 `"Andalusian"', add
label define hispand_lbl 452 `"Asturian"', add
label define hispand_lbl 453 `"Castillian"', add
label define hispand_lbl 454 `"Catalonian"', add
label define hispand_lbl 455 `"Balearic Islander"', add
label define hispand_lbl 456 `"Gallego"', add
label define hispand_lbl 457 `"Valencian"', add
label define hispand_lbl 458 `"Canarian"', add
label define hispand_lbl 459 `"Spanish Basque"', add
label define hispand_lbl 460 `"Dominican"', add
label define hispand_lbl 465 `"Latin American"', add
label define hispand_lbl 470 `"Hispanic"', add
label define hispand_lbl 480 `"Spanish"', add
label define hispand_lbl 490 `"Californio"', add
label define hispand_lbl 491 `"Tejano"', add
label define hispand_lbl 492 `"Nuevo Mexicano"', add
label define hispand_lbl 493 `"Spanish American"', add
label define hispand_lbl 494 `"Spanish American Indian"', add
label define hispand_lbl 495 `"Meso American Indian"', add
label define hispand_lbl 496 `"Mestizo"', add
label define hispand_lbl 498 `"Other, n.s."', add
label define hispand_lbl 499 `"Other, n.e.c."', add
label define hispand_lbl 900 `"Not Reported"', add
label values hispand hispand_lbl

label define bpl_lbl 001 `"Alabama"'
label define bpl_lbl 002 `"Alaska"', add
label define bpl_lbl 004 `"Arizona"', add
label define bpl_lbl 005 `"Arkansas"', add
label define bpl_lbl 006 `"California"', add
label define bpl_lbl 008 `"Colorado"', add
label define bpl_lbl 009 `"Connecticut"', add
label define bpl_lbl 010 `"Delaware"', add
label define bpl_lbl 011 `"District of Columbia"', add
label define bpl_lbl 012 `"Florida"', add
label define bpl_lbl 013 `"Georgia"', add
label define bpl_lbl 015 `"Hawaii"', add
label define bpl_lbl 016 `"Idaho"', add
label define bpl_lbl 017 `"Illinois"', add
label define bpl_lbl 018 `"Indiana"', add
label define bpl_lbl 019 `"Iowa"', add
label define bpl_lbl 020 `"Kansas"', add
label define bpl_lbl 021 `"Kentucky"', add
label define bpl_lbl 022 `"Louisiana"', add
label define bpl_lbl 023 `"Maine"', add
label define bpl_lbl 024 `"Maryland"', add
label define bpl_lbl 025 `"Massachusetts"', add
label define bpl_lbl 026 `"Michigan"', add
label define bpl_lbl 027 `"Minnesota"', add
label define bpl_lbl 028 `"Mississippi"', add
label define bpl_lbl 029 `"Missouri"', add
label define bpl_lbl 030 `"Montana"', add
label define bpl_lbl 031 `"Nebraska"', add
label define bpl_lbl 032 `"Nevada"', add
label define bpl_lbl 033 `"New Hampshire"', add
label define bpl_lbl 034 `"New Jersey"', add
label define bpl_lbl 035 `"New Mexico"', add
label define bpl_lbl 036 `"New York"', add
label define bpl_lbl 037 `"North Carolina"', add
label define bpl_lbl 038 `"North Dakota"', add
label define bpl_lbl 039 `"Ohio"', add
label define bpl_lbl 040 `"Oklahoma"', add
label define bpl_lbl 041 `"Oregon"', add
label define bpl_lbl 042 `"Pennsylvania"', add
label define bpl_lbl 044 `"Rhode Island"', add
label define bpl_lbl 045 `"South Carolina"', add
label define bpl_lbl 046 `"South Dakota"', add
label define bpl_lbl 047 `"Tennessee"', add
label define bpl_lbl 048 `"Texas"', add
label define bpl_lbl 049 `"Utah"', add
label define bpl_lbl 050 `"Vermont"', add
label define bpl_lbl 051 `"Virginia"', add
label define bpl_lbl 053 `"Washington"', add
label define bpl_lbl 054 `"West Virginia"', add
label define bpl_lbl 055 `"Wisconsin"', add
label define bpl_lbl 056 `"Wyoming"', add
label define bpl_lbl 090 `"Native American"', add
label define bpl_lbl 099 `"United States, ns"', add
label define bpl_lbl 100 `"American Samoa"', add
label define bpl_lbl 105 `"Guam"', add
label define bpl_lbl 110 `"Puerto Rico"', add
label define bpl_lbl 115 `"U.S. Virgin Islands"', add
label define bpl_lbl 120 `"Other US Possessions"', add
label define bpl_lbl 150 `"Canada"', add
label define bpl_lbl 155 `"St. Pierre and Miquelon"', add
label define bpl_lbl 160 `"Atlantic Islands"', add
label define bpl_lbl 199 `"North America, ns"', add
label define bpl_lbl 200 `"Mexico"', add
label define bpl_lbl 210 `"Central America"', add
label define bpl_lbl 250 `"Cuba"', add
label define bpl_lbl 260 `"West Indies"', add
label define bpl_lbl 299 `"Americas, n.s."', add
label define bpl_lbl 300 `"SOUTH AMERICA"', add
label define bpl_lbl 400 `"Denmark"', add
label define bpl_lbl 401 `"Finland"', add
label define bpl_lbl 402 `"Iceland"', add
label define bpl_lbl 403 `"Lapland, n.s."', add
label define bpl_lbl 404 `"Norway"', add
label define bpl_lbl 405 `"Sweden"', add
label define bpl_lbl 410 `"England"', add
label define bpl_lbl 411 `"Scotland"', add
label define bpl_lbl 412 `"Wales"', add
label define bpl_lbl 413 `"United Kingdom, ns"', add
label define bpl_lbl 414 `"Ireland"', add
label define bpl_lbl 419 `"Northern Europe, ns"', add
label define bpl_lbl 420 `"Belgium"', add
label define bpl_lbl 421 `"France"', add
label define bpl_lbl 422 `"Liechtenstein"', add
label define bpl_lbl 423 `"Luxembourg"', add
label define bpl_lbl 424 `"Monaco"', add
label define bpl_lbl 425 `"Netherlands"', add
label define bpl_lbl 426 `"Switzerland"', add
label define bpl_lbl 429 `"Western Europe, ns"', add
label define bpl_lbl 430 `"Albania"', add
label define bpl_lbl 431 `"Andorra"', add
label define bpl_lbl 432 `"Gibraltar"', add
label define bpl_lbl 433 `"Greece"', add
label define bpl_lbl 434 `"Italy"', add
label define bpl_lbl 435 `"Malta"', add
label define bpl_lbl 436 `"Portugal"', add
label define bpl_lbl 437 `"San Marino"', add
label define bpl_lbl 438 `"Spain"', add
label define bpl_lbl 439 `"Vatican City"', add
label define bpl_lbl 440 `"Southern Europe, ns"', add
label define bpl_lbl 450 `"Austria"', add
label define bpl_lbl 451 `"Bulgaria"', add
label define bpl_lbl 452 `"Czechoslovakia"', add
label define bpl_lbl 453 `"Germany"', add
label define bpl_lbl 454 `"Hungary"', add
label define bpl_lbl 455 `"Poland"', add
label define bpl_lbl 456 `"Romania"', add
label define bpl_lbl 457 `"Yugoslavia"', add
label define bpl_lbl 458 `"Central Europe, ns"', add
label define bpl_lbl 459 `"Eastern Europe, ns"', add
label define bpl_lbl 460 `"Estonia"', add
label define bpl_lbl 461 `"Latvia"', add
label define bpl_lbl 462 `"Lithuania"', add
label define bpl_lbl 463 `"Baltic States, ns"', add
label define bpl_lbl 465 `"Other USSR/Russia"', add
label define bpl_lbl 499 `"Europe, ns"', add
label define bpl_lbl 500 `"China"', add
label define bpl_lbl 501 `"Japan"', add
label define bpl_lbl 502 `"Korea"', add
label define bpl_lbl 509 `"East Asia, ns"', add
label define bpl_lbl 510 `"Brunei"', add
label define bpl_lbl 511 `"Cambodia (Kampuchea)"', add
label define bpl_lbl 512 `"Indonesia"', add
label define bpl_lbl 513 `"Laos"', add
label define bpl_lbl 514 `"Malaysia"', add
label define bpl_lbl 515 `"Philippines"', add
label define bpl_lbl 516 `"Singapore"', add
label define bpl_lbl 517 `"Thailand"', add
label define bpl_lbl 518 `"Vietnam"', add
label define bpl_lbl 519 `"Southeast Asia, ns"', add
label define bpl_lbl 520 `"Afghanistan"', add
label define bpl_lbl 521 `"India"', add
label define bpl_lbl 522 `"Iran"', add
label define bpl_lbl 523 `"Maldives"', add
label define bpl_lbl 524 `"Nepal"', add
label define bpl_lbl 530 `"Bahrain"', add
label define bpl_lbl 531 `"Cyprus"', add
label define bpl_lbl 532 `"Iraq"', add
label define bpl_lbl 533 `"Iraq/Saudi Arabia"', add
label define bpl_lbl 534 `"Israel/Palestine"', add
label define bpl_lbl 535 `"Jordan"', add
label define bpl_lbl 536 `"Kuwait"', add
label define bpl_lbl 537 `"Lebanon"', add
label define bpl_lbl 538 `"Oman"', add
label define bpl_lbl 539 `"Qatar"', add
label define bpl_lbl 540 `"Saudi Arabia"', add
label define bpl_lbl 541 `"Syria"', add
label define bpl_lbl 542 `"Turkey"', add
label define bpl_lbl 543 `"United Arab Emirates"', add
label define bpl_lbl 544 `"Yemen Arab Republic (North)"', add
label define bpl_lbl 545 `"Yemen, PDR (South)"', add
label define bpl_lbl 546 `"Persian Gulf States, n.s."', add
label define bpl_lbl 547 `"Middle East, ns"', add
label define bpl_lbl 548 `"Southwest Asia, nec/ns"', add
label define bpl_lbl 549 `"Asia Minor, ns"', add
label define bpl_lbl 550 `"South Asia, nec"', add
label define bpl_lbl 599 `"Asia, nec/ns"', add
label define bpl_lbl 600 `"AFRICA"', add
label define bpl_lbl 700 `"Australia and New Zealand"', add
label define bpl_lbl 710 `"Pacific Islands"', add
label define bpl_lbl 800 `"Antarctica, ns/nec"', add
label define bpl_lbl 900 `"Abroad (unknown) or at sea"', add
label define bpl_lbl 950 `"Other n.e.c."', add
label define bpl_lbl 999 `"Missing/blank"', add
label values bpl bpl_lbl

label define bpld_lbl 00100 `"Alabama"'
label define bpld_lbl 00200 `"Alaska"', add
label define bpld_lbl 00400 `"Arizona"', add
label define bpld_lbl 00500 `"Arkansas"', add
label define bpld_lbl 00600 `"California"', add
label define bpld_lbl 00800 `"Colorado"', add
label define bpld_lbl 00900 `"Connecticut"', add
label define bpld_lbl 01000 `"Delaware"', add
label define bpld_lbl 01100 `"District of Columbia"', add
label define bpld_lbl 01200 `"Florida"', add
label define bpld_lbl 01300 `"Georgia"', add
label define bpld_lbl 01500 `"Hawaii"', add
label define bpld_lbl 01600 `"Idaho"', add
label define bpld_lbl 01610 `"Idaho Territory"', add
label define bpld_lbl 01700 `"Illinois"', add
label define bpld_lbl 01800 `"Indiana"', add
label define bpld_lbl 01900 `"Iowa"', add
label define bpld_lbl 02000 `"Kansas"', add
label define bpld_lbl 02100 `"Kentucky"', add
label define bpld_lbl 02200 `"Louisiana"', add
label define bpld_lbl 02300 `"Maine"', add
label define bpld_lbl 02400 `"Maryland"', add
label define bpld_lbl 02500 `"Massachusetts"', add
label define bpld_lbl 02600 `"Michigan"', add
label define bpld_lbl 02700 `"Minnesota"', add
label define bpld_lbl 02800 `"Mississippi"', add
label define bpld_lbl 02900 `"Missouri"', add
label define bpld_lbl 03000 `"Montana"', add
label define bpld_lbl 03100 `"Nebraska"', add
label define bpld_lbl 03200 `"Nevada"', add
label define bpld_lbl 03300 `"New Hampshire"', add
label define bpld_lbl 03400 `"New Jersey"', add
label define bpld_lbl 03500 `"New Mexico"', add
label define bpld_lbl 03510 `"New Mexico Territory"', add
label define bpld_lbl 03600 `"New York"', add
label define bpld_lbl 03700 `"North Carolina"', add
label define bpld_lbl 03800 `"North Dakota"', add
label define bpld_lbl 03900 `"Ohio"', add
label define bpld_lbl 04000 `"Oklahoma"', add
label define bpld_lbl 04010 `"Indian Territory"', add
label define bpld_lbl 04100 `"Oregon"', add
label define bpld_lbl 04200 `"Pennsylvania"', add
label define bpld_lbl 04400 `"Rhode Island"', add
label define bpld_lbl 04500 `"South Carolina"', add
label define bpld_lbl 04600 `"South Dakota"', add
label define bpld_lbl 04610 `"Dakota Territory"', add
label define bpld_lbl 04700 `"Tennessee"', add
label define bpld_lbl 04800 `"Texas"', add
label define bpld_lbl 04900 `"Utah"', add
label define bpld_lbl 04910 `"Utah Territory"', add
label define bpld_lbl 05000 `"Vermont"', add
label define bpld_lbl 05100 `"Virginia"', add
label define bpld_lbl 05300 `"Washington"', add
label define bpld_lbl 05400 `"West Virginia"', add
label define bpld_lbl 05500 `"Wisconsin"', add
label define bpld_lbl 05600 `"Wyoming"', add
label define bpld_lbl 05610 `"Wyoming Territory"', add
label define bpld_lbl 09000 `"Native American"', add
label define bpld_lbl 09900 `"United States, ns"', add
label define bpld_lbl 10000 `"American Samoa"', add
label define bpld_lbl 10010 `"Samoa, 1940-1950"', add
label define bpld_lbl 10500 `"Guam"', add
label define bpld_lbl 11000 `"Puerto Rico"', add
label define bpld_lbl 11500 `"U.S. Virgin Islands"', add
label define bpld_lbl 11510 `"St. Croix"', add
label define bpld_lbl 11520 `"St. John"', add
label define bpld_lbl 11530 `"St. Thomas"', add
label define bpld_lbl 12000 `"Other US Possessions:"', add
label define bpld_lbl 12010 `"Johnston Atoll"', add
label define bpld_lbl 12020 `"Midway Islands"', add
label define bpld_lbl 12030 `"Wake Island"', add
label define bpld_lbl 12040 `"Other US Caribbean Islands"', add
label define bpld_lbl 12041 `"Navassa Island"', add
label define bpld_lbl 12050 `"Other US Pacific Islands"', add
label define bpld_lbl 12051 `"Baker Island"', add
label define bpld_lbl 12052 `"Howland Island"', add
label define bpld_lbl 12053 `"Jarvis Island"', add
label define bpld_lbl 12054 `"Kingman Reef"', add
label define bpld_lbl 12055 `"Palmyra Atoll"', add
label define bpld_lbl 12056 `"Canton and Enderbury Island"', add
label define bpld_lbl 12090 `"US outlying areas, ns"', add
label define bpld_lbl 12091 `"US possessions, ns"', add
label define bpld_lbl 12092 `"US territory, ns"', add
label define bpld_lbl 15000 `"Canada"', add
label define bpld_lbl 15010 `"English Canada"', add
label define bpld_lbl 15011 `"British Columbia"', add
label define bpld_lbl 15013 `"Alberta"', add
label define bpld_lbl 15015 `"Saskatchewan"', add
label define bpld_lbl 15017 `"Northwest"', add
label define bpld_lbl 15019 `"Ruperts Land"', add
label define bpld_lbl 15020 `"Manitoba"', add
label define bpld_lbl 15021 `"Red River"', add
label define bpld_lbl 15030 `"Ontario/Upper Canada"', add
label define bpld_lbl 15031 `"Upper Canada"', add
label define bpld_lbl 15032 `"Canada West"', add
label define bpld_lbl 15040 `"New Brunswick"', add
label define bpld_lbl 15050 `"Nova Scotia"', add
label define bpld_lbl 15051 `"Cape Breton"', add
label define bpld_lbl 15052 `"Halifax"', add
label define bpld_lbl 15060 `"Prince Edward Island"', add
label define bpld_lbl 15070 `"Newfoundland"', add
label define bpld_lbl 15080 `"French Canada"', add
label define bpld_lbl 15081 `"Quebec"', add
label define bpld_lbl 15082 `"Lower Canada"', add
label define bpld_lbl 15083 `"Canada East"', add
label define bpld_lbl 15500 `"St. Pierre and Miquelon"', add
label define bpld_lbl 16000 `"Atlantic Islands"', add
label define bpld_lbl 16010 `"Bermuda"', add
label define bpld_lbl 16020 `"Cape Verde"', add
label define bpld_lbl 16030 `"Falkland Islands"', add
label define bpld_lbl 16040 `"Greenland"', add
label define bpld_lbl 16050 `"St. Helena and Ascension"', add
label define bpld_lbl 16060 `"Canary Islands"', add
label define bpld_lbl 19900 `"North America, ns"', add
label define bpld_lbl 20000 `"Mexico"', add
label define bpld_lbl 21000 `"Central America"', add
label define bpld_lbl 21010 `"Belize/British Honduras"', add
label define bpld_lbl 21020 `"Costa Rica"', add
label define bpld_lbl 21030 `"El Salvador"', add
label define bpld_lbl 21040 `"Guatemala"', add
label define bpld_lbl 21050 `"Honduras"', add
label define bpld_lbl 21060 `"Nicaragua"', add
label define bpld_lbl 21070 `"Panama"', add
label define bpld_lbl 21071 `"Canal Zone"', add
label define bpld_lbl 21090 `"Central America, ns"', add
label define bpld_lbl 25000 `"Cuba"', add
label define bpld_lbl 26000 `"West Indies"', add
label define bpld_lbl 26010 `"Dominican Republic"', add
label define bpld_lbl 26020 `"Haiti"', add
label define bpld_lbl 26030 `"Jamaica"', add
label define bpld_lbl 26040 `"British West Indies"', add
label define bpld_lbl 26041 `"Anguilla"', add
label define bpld_lbl 26042 `"Antigua-Barbuda"', add
label define bpld_lbl 26043 `"Bahamas"', add
label define bpld_lbl 26044 `"Barbados"', add
label define bpld_lbl 26045 `"British Virgin Islands"', add
label define bpld_lbl 26046 `"Anegada"', add
label define bpld_lbl 26047 `"Cooper"', add
label define bpld_lbl 26048 `"Jost Van Dyke"', add
label define bpld_lbl 26049 `"Peter"', add
label define bpld_lbl 26050 `"Tortola"', add
label define bpld_lbl 26051 `"Virgin Gorda"', add
label define bpld_lbl 26052 `"Br. Virgin Islands, ns"', add
label define bpld_lbl 26053 `"Cayman Islands"', add
label define bpld_lbl 26054 `"Dominica"', add
label define bpld_lbl 26055 `"Grenada"', add
label define bpld_lbl 26056 `"Montserrat"', add
label define bpld_lbl 26057 `"St. Kitts-Nevis"', add
label define bpld_lbl 26058 `"St. Lucia"', add
label define bpld_lbl 26059 `"St. Vincent"', add
label define bpld_lbl 26060 `"Trinidad and Tobago"', add
label define bpld_lbl 26061 `"Turks and Caicos"', add
label define bpld_lbl 26069 `"Br. Virgin Islands, ns"', add
label define bpld_lbl 26070 `"Other West Indies"', add
label define bpld_lbl 26071 `"Aruba"', add
label define bpld_lbl 26072 `"Netherlands Antilles"', add
label define bpld_lbl 26073 `"Bonaire"', add
label define bpld_lbl 26074 `"Curacao"', add
label define bpld_lbl 26075 `"Dutch St. Maarten"', add
label define bpld_lbl 26076 `"Saba"', add
label define bpld_lbl 26077 `"St. Eustatius"', add
label define bpld_lbl 26079 `"Dutch Caribbean, ns"', add
label define bpld_lbl 26080 `"French St. Maarten"', add
label define bpld_lbl 26081 `"Guadeloupe"', add
label define bpld_lbl 26082 `"Martinique"', add
label define bpld_lbl 26083 `"St. Barthelemy"', add
label define bpld_lbl 26089 `"French Caribbean, ns"', add
label define bpld_lbl 26090 `"Antilles, ns"', add
label define bpld_lbl 26091 `"Caribbean, ns"', add
label define bpld_lbl 26092 `"Latin America, ns"', add
label define bpld_lbl 26093 `"Leeward Islands, ns"', add
label define bpld_lbl 26094 `"West Indies, ns"', add
label define bpld_lbl 26095 `"Windward Islands, ns"', add
label define bpld_lbl 29900 `"Americas, ns"', add
label define bpld_lbl 30000 `"South America"', add
label define bpld_lbl 30005 `"Argentina"', add
label define bpld_lbl 30010 `"Bolivia"', add
label define bpld_lbl 30015 `"Brazil"', add
label define bpld_lbl 30020 `"Chile"', add
label define bpld_lbl 30025 `"Colombia"', add
label define bpld_lbl 30030 `"Ecuador"', add
label define bpld_lbl 30035 `"French Guiana"', add
label define bpld_lbl 30040 `"Guyana/British Guiana"', add
label define bpld_lbl 30045 `"Paraguay"', add
label define bpld_lbl 30050 `"Peru"', add
label define bpld_lbl 30055 `"Suriname"', add
label define bpld_lbl 30060 `"Uruguay"', add
label define bpld_lbl 30065 `"Venezuela"', add
label define bpld_lbl 30090 `"South America, ns"', add
label define bpld_lbl 30091 `"South and Central America, n.s."', add
label define bpld_lbl 40000 `"Denmark"', add
label define bpld_lbl 40010 `"Faeroe Islands"', add
label define bpld_lbl 40100 `"Finland"', add
label define bpld_lbl 40200 `"Iceland"', add
label define bpld_lbl 40300 `"Lapland, ns"', add
label define bpld_lbl 40400 `"Norway"', add
label define bpld_lbl 40410 `"Svalbard and Jan Meyen"', add
label define bpld_lbl 40411 `"Svalbard"', add
label define bpld_lbl 40412 `"Jan Meyen"', add
label define bpld_lbl 40500 `"Sweden"', add
label define bpld_lbl 41000 `"England"', add
label define bpld_lbl 41010 `"Channel Islands"', add
label define bpld_lbl 41011 `"Guernsey"', add
label define bpld_lbl 41012 `"Jersey"', add
label define bpld_lbl 41020 `"Isle of Man"', add
label define bpld_lbl 41100 `"Scotland"', add
label define bpld_lbl 41200 `"Wales"', add
label define bpld_lbl 41300 `"United Kingdom, ns"', add
label define bpld_lbl 41400 `"Ireland"', add
label define bpld_lbl 41410 `"Northern Ireland"', add
label define bpld_lbl 41900 `"Northern Europe, ns"', add
label define bpld_lbl 42000 `"Belgium"', add
label define bpld_lbl 42100 `"France"', add
label define bpld_lbl 42110 `"Alsace-Lorraine"', add
label define bpld_lbl 42111 `"Alsace"', add
label define bpld_lbl 42112 `"Lorraine"', add
label define bpld_lbl 42200 `"Liechtenstein"', add
label define bpld_lbl 42300 `"Luxembourg"', add
label define bpld_lbl 42400 `"Monaco"', add
label define bpld_lbl 42500 `"Netherlands"', add
label define bpld_lbl 42600 `"Switzerland"', add
label define bpld_lbl 42900 `"Western Europe, ns"', add
label define bpld_lbl 43000 `"Albania"', add
label define bpld_lbl 43100 `"Andorra"', add
label define bpld_lbl 43200 `"Gibraltar"', add
label define bpld_lbl 43300 `"Greece"', add
label define bpld_lbl 43310 `"Dodecanese Islands"', add
label define bpld_lbl 43320 `"Turkey Greece"', add
label define bpld_lbl 43330 `"Macedonia"', add
label define bpld_lbl 43400 `"Italy"', add
label define bpld_lbl 43500 `"Malta"', add
label define bpld_lbl 43600 `"Portugal"', add
label define bpld_lbl 43610 `"Azores"', add
label define bpld_lbl 43620 `"Madeira Islands"', add
label define bpld_lbl 43630 `"Cape Verde Islands"', add
label define bpld_lbl 43640 `"St. Miguel"', add
label define bpld_lbl 43700 `"San Marino"', add
label define bpld_lbl 43800 `"Spain"', add
label define bpld_lbl 43900 `"Vatican City"', add
label define bpld_lbl 44000 `"Southern Europe, ns"', add
label define bpld_lbl 45000 `"Austria"', add
label define bpld_lbl 45010 `"Austria-Hungary"', add
label define bpld_lbl 45020 `"Austria-Graz"', add
label define bpld_lbl 45030 `"Austria-Linz"', add
label define bpld_lbl 45040 `"Austria-Salzburg"', add
label define bpld_lbl 45050 `"Austria-Tyrol"', add
label define bpld_lbl 45060 `"Austria-Vienna"', add
label define bpld_lbl 45070 `"Austria-Kaernsten"', add
label define bpld_lbl 45080 `"Austria-Neustadt"', add
label define bpld_lbl 45100 `"Bulgaria"', add
label define bpld_lbl 45200 `"Czechoslovakia"', add
label define bpld_lbl 45210 `"Bohemia"', add
label define bpld_lbl 45211 `"Bohemia-Moravia"', add
label define bpld_lbl 45212 `"Slovakia"', add
label define bpld_lbl 45213 `"Czech Republic"', add
label define bpld_lbl 45300 `"Germany"', add
label define bpld_lbl 45301 `"Berlin"', add
label define bpld_lbl 45302 `"West Berlin"', add
label define bpld_lbl 45303 `"East Berlin"', add
label define bpld_lbl 45310 `"West Germany"', add
label define bpld_lbl 45311 `"Baden"', add
label define bpld_lbl 45312 `"Bavaria"', add
label define bpld_lbl 45313 `"Braunschweig"', add
label define bpld_lbl 45314 `"Bremen"', add
label define bpld_lbl 45315 `"Hamburg"', add
label define bpld_lbl 45316 `"Hanover"', add
label define bpld_lbl 45317 `"Hessen"', add
label define bpld_lbl 45318 `"Hesse-Nassau"', add
label define bpld_lbl 45319 `"Lippe"', add
label define bpld_lbl 45320 `"Lubeck"', add
label define bpld_lbl 45321 `"Oldenburg"', add
label define bpld_lbl 45322 `"Rheinland"', add
label define bpld_lbl 45323 `"Schaumburg-Lippe"', add
label define bpld_lbl 45324 `"Schleswig"', add
label define bpld_lbl 45325 `"Sigmaringen"', add
label define bpld_lbl 45326 `"Schwarzburg"', add
label define bpld_lbl 45327 `"Westphalia"', add
label define bpld_lbl 45328 `"Wurttemberg"', add
label define bpld_lbl 45329 `"Waldeck"', add
label define bpld_lbl 45330 `"Wittenberg"', add
label define bpld_lbl 45331 `"Frankfurt"', add
label define bpld_lbl 45332 `"Saarland"', add
label define bpld_lbl 45333 `"Nordrhein-Westfalen"', add
label define bpld_lbl 45340 `"East Germany"', add
label define bpld_lbl 45341 `"Anhalt"', add
label define bpld_lbl 45342 `"Brandenburg"', add
label define bpld_lbl 45344 `"Kingdom of Saxony"', add
label define bpld_lbl 45345 `"Mecklenburg"', add
label define bpld_lbl 45346 `"Saxony"', add
label define bpld_lbl 45347 `"Thuringian States"', add
label define bpld_lbl 45348 `"Sachsen-Meiningen"', add
label define bpld_lbl 45349 `"Sachsen-Weimar-Eisenach"', add
label define bpld_lbl 45350 `"Probable Saxony"', add
label define bpld_lbl 45351 `"Schwerin"', add
label define bpld_lbl 45352 `"Strelitz"', add
label define bpld_lbl 45353 `"Probably Thuringian States"', add
label define bpld_lbl 45360 `"Prussia, nec"', add
label define bpld_lbl 45361 `"Hohenzollern"', add
label define bpld_lbl 45362 `"Niedersachsen"', add
label define bpld_lbl 45400 `"Hungary"', add
label define bpld_lbl 45500 `"Poland"', add
label define bpld_lbl 45510 `"Austrian Poland"', add
label define bpld_lbl 45511 `"Galicia"', add
label define bpld_lbl 45520 `"German Poland"', add
label define bpld_lbl 45521 `"East Prussia"', add
label define bpld_lbl 45522 `"Pomerania"', add
label define bpld_lbl 45523 `"Posen"', add
label define bpld_lbl 45524 `"Prussian Poland"', add
label define bpld_lbl 45525 `"Silesia"', add
label define bpld_lbl 45526 `"West Prussia"', add
label define bpld_lbl 45530 `"Russian Poland"', add
label define bpld_lbl 45600 `"Romania"', add
label define bpld_lbl 45610 `"Transylvania"', add
label define bpld_lbl 45700 `"Yugoslavia"', add
label define bpld_lbl 45710 `"Croatia"', add
label define bpld_lbl 45720 `"Montenegro"', add
label define bpld_lbl 45730 `"Serbia"', add
label define bpld_lbl 45740 `"Bosnia"', add
label define bpld_lbl 45750 `"Dalmatia"', add
label define bpld_lbl 45760 `"Slovonia"', add
label define bpld_lbl 45770 `"Carniola"', add
label define bpld_lbl 45780 `"Slovenia"', add
label define bpld_lbl 45790 `"Kosovo"', add
label define bpld_lbl 45800 `"Central Europe, ns"', add
label define bpld_lbl 45900 `"Eastern Europe, ns"', add
label define bpld_lbl 46000 `"Estonia"', add
label define bpld_lbl 46100 `"Latvia"', add
label define bpld_lbl 46200 `"Lithuania"', add
label define bpld_lbl 46300 `"Baltic States, ns"', add
label define bpld_lbl 46500 `"Other USSR/Russia"', add
label define bpld_lbl 46510 `"Byelorussia"', add
label define bpld_lbl 46520 `"Moldavia"', add
label define bpld_lbl 46521 `"Bessarabia"', add
label define bpld_lbl 46530 `"Ukraine"', add
label define bpld_lbl 46540 `"Armenia"', add
label define bpld_lbl 46541 `"Azerbaijan"', add
label define bpld_lbl 46542 `"Republic of Georgia"', add
label define bpld_lbl 46543 `"Kazakhstan"', add
label define bpld_lbl 46544 `"Kirghizia"', add
label define bpld_lbl 46545 `"Tadzhik"', add
label define bpld_lbl 46546 `"Turkmenistan"', add
label define bpld_lbl 46547 `"Uzbekistan"', add
label define bpld_lbl 46548 `"Siberia"', add
label define bpld_lbl 46590 `"USSR, ns"', add
label define bpld_lbl 49900 `"Europe, ns."', add
label define bpld_lbl 50000 `"China"', add
label define bpld_lbl 50010 `"Hong Kong"', add
label define bpld_lbl 50020 `"Macau"', add
label define bpld_lbl 50030 `"Mongolia"', add
label define bpld_lbl 50040 `"Taiwan"', add
label define bpld_lbl 50100 `"Japan"', add
label define bpld_lbl 50200 `"Korea"', add
label define bpld_lbl 50210 `"North Korea"', add
label define bpld_lbl 50220 `"South Korea"', add
label define bpld_lbl 50900 `"East Asia, ns"', add
label define bpld_lbl 51000 `"Brunei"', add
label define bpld_lbl 51100 `"Cambodia (Kampuchea)"', add
label define bpld_lbl 51200 `"Indonesia"', add
label define bpld_lbl 51210 `"East Indies"', add
label define bpld_lbl 51220 `"East Timor"', add
label define bpld_lbl 51300 `"Laos"', add
label define bpld_lbl 51400 `"Malaysia"', add
label define bpld_lbl 51500 `"Philippines"', add
label define bpld_lbl 51600 `"Singapore"', add
label define bpld_lbl 51700 `"Thailand"', add
label define bpld_lbl 51800 `"Vietnam"', add
label define bpld_lbl 51900 `"Southeast Asia, ns"', add
label define bpld_lbl 51910 `"Indochina, ns"', add
label define bpld_lbl 52000 `"Afghanistan"', add
label define bpld_lbl 52100 `"India"', add
label define bpld_lbl 52110 `"Bangladesh"', add
label define bpld_lbl 52120 `"Bhutan"', add
label define bpld_lbl 52130 `"Burma (Myanmar)"', add
label define bpld_lbl 52140 `"Pakistan"', add
label define bpld_lbl 52150 `"Sri Lanka (Ceylon)"', add
label define bpld_lbl 52200 `"Iran"', add
label define bpld_lbl 52300 `"Maldives"', add
label define bpld_lbl 52400 `"Nepal"', add
label define bpld_lbl 53000 `"Bahrain"', add
label define bpld_lbl 53100 `"Cyprus"', add
label define bpld_lbl 53200 `"Iraq"', add
label define bpld_lbl 53210 `"Mesopotamia"', add
label define bpld_lbl 53300 `"Iraq/Saudi Arabia"', add
label define bpld_lbl 53400 `"Israel/Palestine"', add
label define bpld_lbl 53410 `"Gaza Strip"', add
label define bpld_lbl 53420 `"Palestine"', add
label define bpld_lbl 53430 `"West Bank"', add
label define bpld_lbl 53440 `"Israel"', add
label define bpld_lbl 53500 `"Jordan"', add
label define bpld_lbl 53600 `"Kuwait"', add
label define bpld_lbl 53700 `"Lebanon"', add
label define bpld_lbl 53800 `"Oman"', add
label define bpld_lbl 53900 `"Qatar"', add
label define bpld_lbl 54000 `"Saudi Arabia"', add
label define bpld_lbl 54100 `"Syria"', add
label define bpld_lbl 54200 `"Turkey"', add
label define bpld_lbl 54210 `"European Turkey"', add
label define bpld_lbl 54220 `"Asian Turkey"', add
label define bpld_lbl 54300 `"United Arab Emirates"', add
label define bpld_lbl 54400 `"Yemen Arab Republic (North)"', add
label define bpld_lbl 54500 `"Yemen, PDR (South)"', add
label define bpld_lbl 54600 `"Persian Gulf States, ns"', add
label define bpld_lbl 54700 `"Middle East, ns"', add
label define bpld_lbl 54800 `"Southwest Asia, nec/ns"', add
label define bpld_lbl 54900 `"Asia Minor, ns"', add
label define bpld_lbl 55000 `"South Asia, nec"', add
label define bpld_lbl 59900 `"Asia, nec/ns"', add
label define bpld_lbl 60000 `"Africa"', add
label define bpld_lbl 60010 `"Northern Africa"', add
label define bpld_lbl 60011 `"Algeria"', add
label define bpld_lbl 60012 `"Egypt/United Arab Rep."', add
label define bpld_lbl 60013 `"Libya"', add
label define bpld_lbl 60014 `"Morocco"', add
label define bpld_lbl 60015 `"Sudan"', add
label define bpld_lbl 60016 `"Tunisia"', add
label define bpld_lbl 60017 `"Western Sahara"', add
label define bpld_lbl 60019 `"North Africa, ns"', add
label define bpld_lbl 60020 `"Benin"', add
label define bpld_lbl 60021 `"Burkina Faso"', add
label define bpld_lbl 60022 `"Gambia"', add
label define bpld_lbl 60023 `"Ghana"', add
label define bpld_lbl 60024 `"Guinea"', add
label define bpld_lbl 60025 `"Guinea-Bissau"', add
label define bpld_lbl 60026 `"Ivory Coast"', add
label define bpld_lbl 60027 `"Liberia"', add
label define bpld_lbl 60028 `"Mali"', add
label define bpld_lbl 60029 `"Mauritania"', add
label define bpld_lbl 60030 `"Niger"', add
label define bpld_lbl 60031 `"Nigeria"', add
label define bpld_lbl 60032 `"Senegal"', add
label define bpld_lbl 60033 `"Sierra Leone"', add
label define bpld_lbl 60034 `"Togo"', add
label define bpld_lbl 60038 `"Western Africa, ns"', add
label define bpld_lbl 60039 `"French West Africa, ns"', add
label define bpld_lbl 60040 `"British Indian Ocean Territory"', add
label define bpld_lbl 60041 `"Burundi"', add
label define bpld_lbl 60042 `"Comoros"', add
label define bpld_lbl 60043 `"Djibouti"', add
label define bpld_lbl 60044 `"Ethiopia"', add
label define bpld_lbl 60045 `"Kenya"', add
label define bpld_lbl 60046 `"Madagascar"', add
label define bpld_lbl 60047 `"Malawi"', add
label define bpld_lbl 60048 `"Mauritius"', add
label define bpld_lbl 60049 `"Mozambique"', add
label define bpld_lbl 60050 `"Reunion"', add
label define bpld_lbl 60051 `"Rwanda"', add
label define bpld_lbl 60052 `"Seychelles"', add
label define bpld_lbl 60053 `"Somalia"', add
label define bpld_lbl 60054 `"Tanzania"', add
label define bpld_lbl 60055 `"Uganda"', add
label define bpld_lbl 60056 `"Zambia"', add
label define bpld_lbl 60057 `"Zimbabwe"', add
label define bpld_lbl 60058 `"Bassas de India"', add
label define bpld_lbl 60059 `"Europa"', add
label define bpld_lbl 60060 `"Gloriosos"', add
label define bpld_lbl 60061 `"Juan de Nova"', add
label define bpld_lbl 60062 `"Mayotte"', add
label define bpld_lbl 60063 `"Tromelin"', add
label define bpld_lbl 60064 `"Eastern Africa, nec/ns"', add
label define bpld_lbl 60065 `"Eritrea"', add
label define bpld_lbl 60066 `"South Sudan"', add
label define bpld_lbl 60070 `"Central Africa"', add
label define bpld_lbl 60071 `"Angola"', add
label define bpld_lbl 60072 `"Cameroon"', add
label define bpld_lbl 60073 `"Central African Republic"', add
label define bpld_lbl 60074 `"Chad"', add
label define bpld_lbl 60075 `"Congo"', add
label define bpld_lbl 60076 `"Equatorial Guinea"', add
label define bpld_lbl 60077 `"Gabon"', add
label define bpld_lbl 60078 `"Sao Tome and Principe"', add
label define bpld_lbl 60079 `"Zaire"', add
label define bpld_lbl 60080 `"Central Africa, ns"', add
label define bpld_lbl 60081 `"Equatorial Africa, ns"', add
label define bpld_lbl 60082 `"French Equatorial Africa, ns"', add
label define bpld_lbl 60090 `"Southern Africa"', add
label define bpld_lbl 60091 `"Botswana"', add
label define bpld_lbl 60092 `"Lesotho"', add
label define bpld_lbl 60093 `"Namibia"', add
label define bpld_lbl 60094 `"South Africa (Union of)"', add
label define bpld_lbl 60095 `"Swaziland"', add
label define bpld_lbl 60096 `"Southern Africa, ns"', add
label define bpld_lbl 60099 `"Africa, ns/nec"', add
label define bpld_lbl 70000 `"Australia and New Zealand"', add
label define bpld_lbl 70010 `"Australia"', add
label define bpld_lbl 70011 `"Ashmore and Cartier Islands"', add
label define bpld_lbl 70012 `"Coral Sea Islands Territory"', add
label define bpld_lbl 70013 `"Christmas Island"', add
label define bpld_lbl 70014 `"Cocos Islands"', add
label define bpld_lbl 70020 `"New Zealand"', add
label define bpld_lbl 71000 `"Pacific Islands"', add
label define bpld_lbl 71010 `"New Caledonia"', add
label define bpld_lbl 71012 `"Papua New Guinea"', add
label define bpld_lbl 71013 `"Solomon Islands"', add
label define bpld_lbl 71014 `"Vanuatu (New Hebrides)"', add
label define bpld_lbl 71015 `"Fiji"', add
label define bpld_lbl 71016 `"Melanesia, ns"', add
label define bpld_lbl 71017 `"Norfolk Islands"', add
label define bpld_lbl 71018 `"Niue"', add
label define bpld_lbl 71020 `"Cook Islands"', add
label define bpld_lbl 71022 `"French Polynesia"', add
label define bpld_lbl 71023 `"Tonga"', add
label define bpld_lbl 71024 `"Wallis and Futuna Islands"', add
label define bpld_lbl 71025 `"Western Samoa"', add
label define bpld_lbl 71026 `"Pitcairn Island"', add
label define bpld_lbl 71027 `"Tokelau"', add
label define bpld_lbl 71028 `"Tuvalu"', add
label define bpld_lbl 71029 `"Polynesia, ns"', add
label define bpld_lbl 71032 `"Kiribati"', add
label define bpld_lbl 71033 `"Canton and Enderbury"', add
label define bpld_lbl 71034 `"Nauru"', add
label define bpld_lbl 71039 `"Micronesia, ns"', add
label define bpld_lbl 71040 `"US Pacific Trust Territories"', add
label define bpld_lbl 71041 `"Marshall Islands"', add
label define bpld_lbl 71042 `"Micronesia"', add
label define bpld_lbl 71043 `"Kosrae"', add
label define bpld_lbl 71044 `"Pohnpei"', add
label define bpld_lbl 71045 `"Truk"', add
label define bpld_lbl 71046 `"Yap"', add
label define bpld_lbl 71047 `"Northern Mariana Islands"', add
label define bpld_lbl 71048 `"Palau"', add
label define bpld_lbl 71049 `"Pacific Trust Terr, ns"', add
label define bpld_lbl 71050 `"Clipperton Island"', add
label define bpld_lbl 71090 `"Oceania, ns/nec"', add
label define bpld_lbl 80000 `"Antarctica, ns/nec"', add
label define bpld_lbl 80010 `"Bouvet Islands"', add
label define bpld_lbl 80020 `"British Antarctic Terr."', add
label define bpld_lbl 80030 `"Dronning Maud Land"', add
label define bpld_lbl 80040 `"French Southern and Antarctic Lands"', add
label define bpld_lbl 80050 `"Heard and McDonald Islands"', add
label define bpld_lbl 90000 `"Abroad (unknown) or at sea"', add
label define bpld_lbl 90010 `"Abroad, ns"', add
label define bpld_lbl 90011 `"Abroad (US citizen)"', add
label define bpld_lbl 90020 `"At sea"', add
label define bpld_lbl 90021 `"At sea (US citizen)"', add
label define bpld_lbl 90022 `"At sea or abroad (U.S. citizen)"', add
label define bpld_lbl 95000 `"Other n.e.c."', add
label define bpld_lbl 99900 `"Missing/blank"', add
label values bpld bpld_lbl

label define nativity_lbl 0 `"N/A"'
label define nativity_lbl 1 `"Native born, and both parents native born"', add
label define nativity_lbl 2 `"Native born, and father foreign, mother native"', add
label define nativity_lbl 3 `"Native born, and mother foreign, father native"', add
label define nativity_lbl 4 `"Native born, and both parents foreign"', add
label define nativity_lbl 5 `"Foreign born"', add
label values nativity nativity_lbl

label define citizen_lbl 0 `"N/A"'
label define citizen_lbl 1 `"Born abroad of American parents"', add
label define citizen_lbl 2 `"Naturalized citizen"', add
label define citizen_lbl 3 `"Not a citizen"', add
label define citizen_lbl 4 `"Not a citizen, but has received first papers"', add
label define citizen_lbl 5 `"Foreign born, citizenship status not reported"', add
label define citizen_lbl 8 `"Illegible"', add
label define citizen_lbl 9 `"Missing/blank"', add
label values citizen citizen_lbl

label define hisprule_lbl 0 `"Not assigned as Hispanic"'
label define hisprule_lbl 1 `"Birthplace is Hispanic"', add
label define hisprule_lbl 2 `"Parental birthplace is Hispanic"', add
label define hisprule_lbl 3 `"Grandparental birthplace is Hispanic"', add
label define hisprule_lbl 4 `"Spouse is Hispanic"', add
label define hisprule_lbl 5 `"Related HH head is Hispanic"', add
label define hisprule_lbl 6 `"Spanish surname"', add
label define hisprule_lbl 7 `"Spouse has Spanish surname"', add
label define hisprule_lbl 8 `"Related HH head has Spanish surname"', add
label values hisprule hisprule_lbl

label define racesing_lbl 1 `"White"'
label define racesing_lbl 2 `"Black"', add
label define racesing_lbl 3 `"American Indian/Alaska Native"', add
label define racesing_lbl 4 `"Asian and/or Pacific Islander"', add
label define racesing_lbl 5 `"Other race, non-Hispanic"', add
label values racesing racesing_lbl

label define racesingd_lbl 10 `"White"'
label define racesingd_lbl 12 `""Other race", Hispanic"', add
label define racesingd_lbl 20 `"Black"', add
label define racesingd_lbl 21 `"Mulatto"', add
label define racesingd_lbl 30 `"AI (American Indian)"', add
label define racesingd_lbl 31 `"AN (Alaskan Native)"', add
label define racesingd_lbl 32 `"AI/AN (American Indian/Alaskan Native)"', add
label define racesingd_lbl 40 `"Asian Indian"', add
label define racesingd_lbl 41 `"Chinese"', add
label define racesingd_lbl 42 `"Filipino"', add
label define racesingd_lbl 43 `"Japanese"', add
label define racesingd_lbl 44 `"Korean"', add
label define racesingd_lbl 45 `"Asian"', add
label define racesingd_lbl 46 `"Hawaiian"', add
label define racesingd_lbl 47 `"PI (Pacific Islander)"', add
label define racesingd_lbl 48 `"Asian and PI (Pacific Islander)"', add
label define racesingd_lbl 50 `"Other race, non-Hispanic"', add
label define racesingd_lbl 51 `"Other race"', add
label values racesingd racesingd_lbl

label define school_lbl 0 `"N/A"'
label define school_lbl 1 `"No, not in school"', add
label define school_lbl 2 `"Yes, in school"', add
label define school_lbl 8 `"Unknown"', add
label define school_lbl 9 `"Missing"', add
label values school school_lbl

label define lit_lbl 0 `"N/A"'
label define lit_lbl 1 `"No, illiterate (cannot read or write)"', add
label define lit_lbl 2 `"Cannot read, can write"', add
label define lit_lbl 3 `"Cannot write, can read"', add
label define lit_lbl 4 `"Yes, literate (reads and writes)"', add
label define lit_lbl 9 `"Unknown, illegible or blank"', add
label values lit lit_lbl

label define labforce_lbl 0 `"N/A"'
label define labforce_lbl 1 `"No, not in the labor force"', add
label define labforce_lbl 2 `"Yes, in the labor force"', add
label values labforce labforce_lbl

label define occ_lbl 0000 `"0000"'
label define occ_lbl 0001 `"0001"', add
label define occ_lbl 0002 `"0002"', add
label define occ_lbl 0003 `"0003"', add
label define occ_lbl 0004 `"0004"', add
label define occ_lbl 0005 `"0005"', add
label define occ_lbl 0006 `"0006"', add
label define occ_lbl 0007 `"0007"', add
label define occ_lbl 0008 `"0008"', add
label define occ_lbl 0009 `"0009"', add
label define occ_lbl 0010 `"0010"', add
label define occ_lbl 0011 `"0011"', add
label define occ_lbl 0012 `"0012"', add
label define occ_lbl 0013 `"0013"', add
label define occ_lbl 0014 `"0014"', add
label define occ_lbl 0015 `"0015"', add
label define occ_lbl 0016 `"0016"', add
label define occ_lbl 0017 `"0017"', add
label define occ_lbl 0018 `"0018"', add
label define occ_lbl 0019 `"0019"', add
label define occ_lbl 0020 `"0020"', add
label define occ_lbl 0021 `"0021"', add
label define occ_lbl 0022 `"0022"', add
label define occ_lbl 0023 `"0023"', add
label define occ_lbl 0024 `"0024"', add
label define occ_lbl 0025 `"0025"', add
label define occ_lbl 0026 `"0026"', add
label define occ_lbl 0027 `"0027"', add
label define occ_lbl 0028 `"0028"', add
label define occ_lbl 0029 `"0029"', add
label define occ_lbl 0030 `"0030"', add
label define occ_lbl 0031 `"0031"', add
label define occ_lbl 0032 `"0032"', add
label define occ_lbl 0033 `"0033"', add
label define occ_lbl 0034 `"0034"', add
label define occ_lbl 0035 `"0035"', add
label define occ_lbl 0036 `"0036"', add
label define occ_lbl 0037 `"0037"', add
label define occ_lbl 0038 `"0038"', add
label define occ_lbl 0039 `"0039"', add
label define occ_lbl 0040 `"0040"', add
label define occ_lbl 0041 `"0041"', add
label define occ_lbl 0042 `"0042"', add
label define occ_lbl 0043 `"0043"', add
label define occ_lbl 0044 `"0044"', add
label define occ_lbl 0045 `"0045"', add
label define occ_lbl 0046 `"0046"', add
label define occ_lbl 0047 `"0047"', add
label define occ_lbl 0048 `"0048"', add
label define occ_lbl 0049 `"0049"', add
label define occ_lbl 0050 `"0050"', add
label define occ_lbl 0051 `"0051"', add
label define occ_lbl 0052 `"0052"', add
label define occ_lbl 0053 `"0053"', add
label define occ_lbl 0054 `"0054"', add
label define occ_lbl 0055 `"0055"', add
label define occ_lbl 0056 `"0056"', add
label define occ_lbl 0057 `"0057"', add
label define occ_lbl 0058 `"0058"', add
label define occ_lbl 0059 `"0059"', add
label define occ_lbl 0060 `"0060"', add
label define occ_lbl 0061 `"0061"', add
label define occ_lbl 0062 `"0062"', add
label define occ_lbl 0063 `"0063"', add
label define occ_lbl 0064 `"0064"', add
label define occ_lbl 0065 `"0065"', add
label define occ_lbl 0066 `"0066"', add
label define occ_lbl 0067 `"0067"', add
label define occ_lbl 0068 `"0068"', add
label define occ_lbl 0069 `"0069"', add
label define occ_lbl 0070 `"0070"', add
label define occ_lbl 0071 `"0071"', add
label define occ_lbl 0072 `"0072"', add
label define occ_lbl 0073 `"0073"', add
label define occ_lbl 0074 `"0074"', add
label define occ_lbl 0075 `"0075"', add
label define occ_lbl 0076 `"0076"', add
label define occ_lbl 0077 `"0077"', add
label define occ_lbl 0078 `"0078"', add
label define occ_lbl 0079 `"0079"', add
label define occ_lbl 0080 `"0080"', add
label define occ_lbl 0081 `"0081"', add
label define occ_lbl 0082 `"0082"', add
label define occ_lbl 0083 `"0083"', add
label define occ_lbl 0084 `"0084"', add
label define occ_lbl 0085 `"0085"', add
label define occ_lbl 0086 `"0086"', add
label define occ_lbl 0087 `"0087"', add
label define occ_lbl 0088 `"0088"', add
label define occ_lbl 0089 `"0089"', add
label define occ_lbl 0090 `"0090"', add
label define occ_lbl 0091 `"0091"', add
label define occ_lbl 0092 `"0092"', add
label define occ_lbl 0093 `"0093"', add
label define occ_lbl 0094 `"0094"', add
label define occ_lbl 0095 `"0095"', add
label define occ_lbl 0096 `"0096"', add
label define occ_lbl 0097 `"0097"', add
label define occ_lbl 0098 `"0098"', add
label define occ_lbl 0099 `"0099"', add
label define occ_lbl 0100 `"0100"', add
label define occ_lbl 0101 `"0101"', add
label define occ_lbl 0102 `"0102"', add
label define occ_lbl 0103 `"0103"', add
label define occ_lbl 0104 `"0104"', add
label define occ_lbl 0105 `"0105"', add
label define occ_lbl 0106 `"0106"', add
label define occ_lbl 0107 `"0107"', add
label define occ_lbl 0108 `"0108"', add
label define occ_lbl 0109 `"0109"', add
label define occ_lbl 0110 `"0110"', add
label define occ_lbl 0111 `"0111"', add
label define occ_lbl 0112 `"0112"', add
label define occ_lbl 0113 `"0113"', add
label define occ_lbl 0114 `"0114"', add
label define occ_lbl 0115 `"0115"', add
label define occ_lbl 0116 `"0116"', add
label define occ_lbl 0117 `"0117"', add
label define occ_lbl 0118 `"0118"', add
label define occ_lbl 0119 `"0119"', add
label define occ_lbl 0120 `"0120"', add
label define occ_lbl 0121 `"0121"', add
label define occ_lbl 0122 `"0122"', add
label define occ_lbl 0123 `"0123"', add
label define occ_lbl 0124 `"0124"', add
label define occ_lbl 0125 `"0125"', add
label define occ_lbl 0126 `"0126"', add
label define occ_lbl 0127 `"0127"', add
label define occ_lbl 0128 `"0128"', add
label define occ_lbl 0129 `"0129"', add
label define occ_lbl 0130 `"0130"', add
label define occ_lbl 0131 `"0131"', add
label define occ_lbl 0132 `"0132"', add
label define occ_lbl 0133 `"0133"', add
label define occ_lbl 0134 `"0134"', add
label define occ_lbl 0135 `"0135"', add
label define occ_lbl 0136 `"0136"', add
label define occ_lbl 0137 `"0137"', add
label define occ_lbl 0138 `"0138"', add
label define occ_lbl 0139 `"0139"', add
label define occ_lbl 0140 `"0140"', add
label define occ_lbl 0141 `"0141"', add
label define occ_lbl 0142 `"0142"', add
label define occ_lbl 0143 `"0143"', add
label define occ_lbl 0144 `"0144"', add
label define occ_lbl 0145 `"0145"', add
label define occ_lbl 0146 `"0146"', add
label define occ_lbl 0147 `"0147"', add
label define occ_lbl 0148 `"0148"', add
label define occ_lbl 0149 `"0149"', add
label define occ_lbl 0150 `"0150"', add
label define occ_lbl 0151 `"0151"', add
label define occ_lbl 0152 `"0152"', add
label define occ_lbl 0153 `"0153"', add
label define occ_lbl 0154 `"0154"', add
label define occ_lbl 0155 `"0155"', add
label define occ_lbl 0156 `"0156"', add
label define occ_lbl 0157 `"0157"', add
label define occ_lbl 0158 `"0158"', add
label define occ_lbl 0159 `"0159"', add
label define occ_lbl 0160 `"0160"', add
label define occ_lbl 0161 `"0161"', add
label define occ_lbl 0162 `"0162"', add
label define occ_lbl 0163 `"0163"', add
label define occ_lbl 0164 `"0164"', add
label define occ_lbl 0165 `"0165"', add
label define occ_lbl 0166 `"0166"', add
label define occ_lbl 0167 `"0167"', add
label define occ_lbl 0168 `"0168"', add
label define occ_lbl 0169 `"0169"', add
label define occ_lbl 0170 `"0170"', add
label define occ_lbl 0171 `"0171"', add
label define occ_lbl 0172 `"0172"', add
label define occ_lbl 0173 `"0173"', add
label define occ_lbl 0174 `"0174"', add
label define occ_lbl 0175 `"0175"', add
label define occ_lbl 0176 `"0176"', add
label define occ_lbl 0177 `"0177"', add
label define occ_lbl 0178 `"0178"', add
label define occ_lbl 0179 `"0179"', add
label define occ_lbl 0180 `"0180"', add
label define occ_lbl 0181 `"0181"', add
label define occ_lbl 0182 `"0182"', add
label define occ_lbl 0183 `"0183"', add
label define occ_lbl 0184 `"0184"', add
label define occ_lbl 0185 `"0185"', add
label define occ_lbl 0186 `"0186"', add
label define occ_lbl 0187 `"0187"', add
label define occ_lbl 0188 `"0188"', add
label define occ_lbl 0189 `"0189"', add
label define occ_lbl 0190 `"0190"', add
label define occ_lbl 0191 `"0191"', add
label define occ_lbl 0192 `"0192"', add
label define occ_lbl 0193 `"0193"', add
label define occ_lbl 0194 `"0194"', add
label define occ_lbl 0195 `"0195"', add
label define occ_lbl 0196 `"0196"', add
label define occ_lbl 0197 `"0197"', add
label define occ_lbl 0198 `"0198"', add
label define occ_lbl 0199 `"0199"', add
label define occ_lbl 0200 `"0200"', add
label define occ_lbl 0201 `"0201"', add
label define occ_lbl 0202 `"0202"', add
label define occ_lbl 0203 `"0203"', add
label define occ_lbl 0204 `"0204"', add
label define occ_lbl 0205 `"0205"', add
label define occ_lbl 0206 `"0206"', add
label define occ_lbl 0207 `"0207"', add
label define occ_lbl 0208 `"0208"', add
label define occ_lbl 0209 `"0209"', add
label define occ_lbl 0210 `"0210"', add
label define occ_lbl 0211 `"0211"', add
label define occ_lbl 0212 `"0212"', add
label define occ_lbl 0213 `"0213"', add
label define occ_lbl 0214 `"0214"', add
label define occ_lbl 0215 `"0215"', add
label define occ_lbl 0216 `"0216"', add
label define occ_lbl 0217 `"0217"', add
label define occ_lbl 0218 `"0218"', add
label define occ_lbl 0219 `"0219"', add
label define occ_lbl 0220 `"0220"', add
label define occ_lbl 0221 `"0221"', add
label define occ_lbl 0222 `"0222"', add
label define occ_lbl 0223 `"0223"', add
label define occ_lbl 0224 `"0224"', add
label define occ_lbl 0225 `"0225"', add
label define occ_lbl 0226 `"0226"', add
label define occ_lbl 0227 `"0227"', add
label define occ_lbl 0228 `"0228"', add
label define occ_lbl 0229 `"0229"', add
label define occ_lbl 0230 `"0230"', add
label define occ_lbl 0231 `"0231"', add
label define occ_lbl 0232 `"0232"', add
label define occ_lbl 0233 `"0233"', add
label define occ_lbl 0234 `"0234"', add
label define occ_lbl 0235 `"0235"', add
label define occ_lbl 0236 `"0236"', add
label define occ_lbl 0237 `"0237"', add
label define occ_lbl 0238 `"0238"', add
label define occ_lbl 0239 `"0239"', add
label define occ_lbl 0240 `"0240"', add
label define occ_lbl 0241 `"0241"', add
label define occ_lbl 0242 `"0242"', add
label define occ_lbl 0243 `"0243"', add
label define occ_lbl 0244 `"0244"', add
label define occ_lbl 0245 `"0245"', add
label define occ_lbl 0246 `"0246"', add
label define occ_lbl 0247 `"0247"', add
label define occ_lbl 0248 `"0248"', add
label define occ_lbl 0249 `"0249"', add
label define occ_lbl 0250 `"0250"', add
label define occ_lbl 0251 `"0251"', add
label define occ_lbl 0252 `"0252"', add
label define occ_lbl 0253 `"0253"', add
label define occ_lbl 0254 `"0254"', add
label define occ_lbl 0255 `"0255"', add
label define occ_lbl 0256 `"0256"', add
label define occ_lbl 0257 `"0257"', add
label define occ_lbl 0258 `"0258"', add
label define occ_lbl 0259 `"0259"', add
label define occ_lbl 0260 `"0260"', add
label define occ_lbl 0261 `"0261"', add
label define occ_lbl 0262 `"0262"', add
label define occ_lbl 0263 `"0263"', add
label define occ_lbl 0264 `"0264"', add
label define occ_lbl 0265 `"0265"', add
label define occ_lbl 0266 `"0266"', add
label define occ_lbl 0267 `"0267"', add
label define occ_lbl 0268 `"0268"', add
label define occ_lbl 0269 `"0269"', add
label define occ_lbl 0270 `"0270"', add
label define occ_lbl 0271 `"0271"', add
label define occ_lbl 0272 `"0272"', add
label define occ_lbl 0273 `"0273"', add
label define occ_lbl 0274 `"0274"', add
label define occ_lbl 0275 `"0275"', add
label define occ_lbl 0276 `"0276"', add
label define occ_lbl 0277 `"0277"', add
label define occ_lbl 0278 `"0278"', add
label define occ_lbl 0279 `"0279"', add
label define occ_lbl 0280 `"0280"', add
label define occ_lbl 0281 `"0281"', add
label define occ_lbl 0282 `"0282"', add
label define occ_lbl 0283 `"0283"', add
label define occ_lbl 0284 `"0284"', add
label define occ_lbl 0285 `"0285"', add
label define occ_lbl 0286 `"0286"', add
label define occ_lbl 0287 `"0287"', add
label define occ_lbl 0288 `"0288"', add
label define occ_lbl 0289 `"0289"', add
label define occ_lbl 0290 `"0290"', add
label define occ_lbl 0291 `"0291"', add
label define occ_lbl 0292 `"0292"', add
label define occ_lbl 0293 `"0293"', add
label define occ_lbl 0294 `"0294"', add
label define occ_lbl 0295 `"0295"', add
label define occ_lbl 0296 `"0296"', add
label define occ_lbl 0297 `"0297"', add
label define occ_lbl 0298 `"0298"', add
label define occ_lbl 0299 `"0299"', add
label define occ_lbl 0300 `"0300"', add
label define occ_lbl 0301 `"0301"', add
label define occ_lbl 0302 `"0302"', add
label define occ_lbl 0303 `"0303"', add
label define occ_lbl 0304 `"0304"', add
label define occ_lbl 0305 `"0305"', add
label define occ_lbl 0306 `"0306"', add
label define occ_lbl 0307 `"0307"', add
label define occ_lbl 0308 `"0308"', add
label define occ_lbl 0309 `"0309"', add
label define occ_lbl 0310 `"0310"', add
label define occ_lbl 0311 `"0311"', add
label define occ_lbl 0312 `"0312"', add
label define occ_lbl 0313 `"0313"', add
label define occ_lbl 0314 `"0314"', add
label define occ_lbl 0315 `"0315"', add
label define occ_lbl 0316 `"0316"', add
label define occ_lbl 0317 `"0317"', add
label define occ_lbl 0318 `"0318"', add
label define occ_lbl 0319 `"0319"', add
label define occ_lbl 0320 `"0320"', add
label define occ_lbl 0321 `"0321"', add
label define occ_lbl 0322 `"0322"', add
label define occ_lbl 0323 `"0323"', add
label define occ_lbl 0324 `"0324"', add
label define occ_lbl 0325 `"0325"', add
label define occ_lbl 0326 `"0326"', add
label define occ_lbl 0327 `"0327"', add
label define occ_lbl 0328 `"0328"', add
label define occ_lbl 0329 `"0329"', add
label define occ_lbl 0330 `"0330"', add
label define occ_lbl 0331 `"0331"', add
label define occ_lbl 0332 `"0332"', add
label define occ_lbl 0333 `"0333"', add
label define occ_lbl 0334 `"0334"', add
label define occ_lbl 0335 `"0335"', add
label define occ_lbl 0336 `"0336"', add
label define occ_lbl 0337 `"0337"', add
label define occ_lbl 0338 `"0338"', add
label define occ_lbl 0339 `"0339"', add
label define occ_lbl 0340 `"0340"', add
label define occ_lbl 0341 `"0341"', add
label define occ_lbl 0342 `"0342"', add
label define occ_lbl 0343 `"0343"', add
label define occ_lbl 0344 `"0344"', add
label define occ_lbl 0345 `"0345"', add
label define occ_lbl 0346 `"0346"', add
label define occ_lbl 0347 `"0347"', add
label define occ_lbl 0348 `"0348"', add
label define occ_lbl 0349 `"0349"', add
label define occ_lbl 0350 `"0350"', add
label define occ_lbl 0351 `"0351"', add
label define occ_lbl 0352 `"0352"', add
label define occ_lbl 0353 `"0353"', add
label define occ_lbl 0354 `"0354"', add
label define occ_lbl 0355 `"0355"', add
label define occ_lbl 0356 `"0356"', add
label define occ_lbl 0357 `"0357"', add
label define occ_lbl 0358 `"0358"', add
label define occ_lbl 0359 `"0359"', add
label define occ_lbl 0360 `"0360"', add
label define occ_lbl 0361 `"0361"', add
label define occ_lbl 0362 `"0362"', add
label define occ_lbl 0363 `"0363"', add
label define occ_lbl 0364 `"0364"', add
label define occ_lbl 0365 `"0365"', add
label define occ_lbl 0366 `"0366"', add
label define occ_lbl 0367 `"0367"', add
label define occ_lbl 0368 `"0368"', add
label define occ_lbl 0369 `"0369"', add
label define occ_lbl 0370 `"0370"', add
label define occ_lbl 0371 `"0371"', add
label define occ_lbl 0372 `"0372"', add
label define occ_lbl 0373 `"0373"', add
label define occ_lbl 0374 `"0374"', add
label define occ_lbl 0375 `"0375"', add
label define occ_lbl 0376 `"0376"', add
label define occ_lbl 0377 `"0377"', add
label define occ_lbl 0378 `"0378"', add
label define occ_lbl 0379 `"0379"', add
label define occ_lbl 0380 `"0380"', add
label define occ_lbl 0381 `"0381"', add
label define occ_lbl 0382 `"0382"', add
label define occ_lbl 0383 `"0383"', add
label define occ_lbl 0384 `"0384"', add
label define occ_lbl 0385 `"0385"', add
label define occ_lbl 0386 `"0386"', add
label define occ_lbl 0387 `"0387"', add
label define occ_lbl 0388 `"0388"', add
label define occ_lbl 0389 `"0389"', add
label define occ_lbl 0390 `"0390"', add
label define occ_lbl 0391 `"0391"', add
label define occ_lbl 0392 `"0392"', add
label define occ_lbl 0393 `"0393"', add
label define occ_lbl 0394 `"0394"', add
label define occ_lbl 0395 `"0395"', add
label define occ_lbl 0396 `"0396"', add
label define occ_lbl 0397 `"0397"', add
label define occ_lbl 0398 `"0398"', add
label define occ_lbl 0399 `"0399"', add
label define occ_lbl 0400 `"0400"', add
label define occ_lbl 0401 `"0401"', add
label define occ_lbl 0402 `"0402"', add
label define occ_lbl 0403 `"0403"', add
label define occ_lbl 0404 `"0404"', add
label define occ_lbl 0405 `"0405"', add
label define occ_lbl 0406 `"0406"', add
label define occ_lbl 0407 `"0407"', add
label define occ_lbl 0408 `"0408"', add
label define occ_lbl 0409 `"0409"', add
label define occ_lbl 0410 `"0410"', add
label define occ_lbl 0411 `"0411"', add
label define occ_lbl 0412 `"0412"', add
label define occ_lbl 0413 `"0413"', add
label define occ_lbl 0414 `"0414"', add
label define occ_lbl 0415 `"0415"', add
label define occ_lbl 0416 `"0416"', add
label define occ_lbl 0417 `"0417"', add
label define occ_lbl 0418 `"0418"', add
label define occ_lbl 0419 `"0419"', add
label define occ_lbl 0420 `"0420"', add
label define occ_lbl 0421 `"0421"', add
label define occ_lbl 0422 `"0422"', add
label define occ_lbl 0423 `"0423"', add
label define occ_lbl 0424 `"0424"', add
label define occ_lbl 0425 `"0425"', add
label define occ_lbl 0426 `"0426"', add
label define occ_lbl 0427 `"0427"', add
label define occ_lbl 0428 `"0428"', add
label define occ_lbl 0429 `"0429"', add
label define occ_lbl 0430 `"0430"', add
label define occ_lbl 0431 `"0431"', add
label define occ_lbl 0432 `"0432"', add
label define occ_lbl 0433 `"0433"', add
label define occ_lbl 0434 `"0434"', add
label define occ_lbl 0435 `"0435"', add
label define occ_lbl 0436 `"0436"', add
label define occ_lbl 0437 `"0437"', add
label define occ_lbl 0438 `"0438"', add
label define occ_lbl 0439 `"0439"', add
label define occ_lbl 0440 `"0440"', add
label define occ_lbl 0441 `"0441"', add
label define occ_lbl 0442 `"0442"', add
label define occ_lbl 0443 `"0443"', add
label define occ_lbl 0444 `"0444"', add
label define occ_lbl 0445 `"0445"', add
label define occ_lbl 0446 `"0446"', add
label define occ_lbl 0447 `"0447"', add
label define occ_lbl 0448 `"0448"', add
label define occ_lbl 0449 `"0449"', add
label define occ_lbl 0450 `"0450"', add
label define occ_lbl 0451 `"0451"', add
label define occ_lbl 0452 `"0452"', add
label define occ_lbl 0453 `"0453"', add
label define occ_lbl 0454 `"0454"', add
label define occ_lbl 0455 `"0455"', add
label define occ_lbl 0456 `"0456"', add
label define occ_lbl 0457 `"0457"', add
label define occ_lbl 0458 `"0458"', add
label define occ_lbl 0459 `"0459"', add
label define occ_lbl 0460 `"0460"', add
label define occ_lbl 0461 `"0461"', add
label define occ_lbl 0462 `"0462"', add
label define occ_lbl 0463 `"0463"', add
label define occ_lbl 0464 `"0464"', add
label define occ_lbl 0465 `"0465"', add
label define occ_lbl 0466 `"0466"', add
label define occ_lbl 0467 `"0467"', add
label define occ_lbl 0468 `"0468"', add
label define occ_lbl 0469 `"0469"', add
label define occ_lbl 0470 `"0470"', add
label define occ_lbl 0471 `"0471"', add
label define occ_lbl 0472 `"0472"', add
label define occ_lbl 0473 `"0473"', add
label define occ_lbl 0474 `"0474"', add
label define occ_lbl 0475 `"0475"', add
label define occ_lbl 0476 `"0476"', add
label define occ_lbl 0477 `"0477"', add
label define occ_lbl 0478 `"0478"', add
label define occ_lbl 0479 `"0479"', add
label define occ_lbl 0480 `"0480"', add
label define occ_lbl 0481 `"0481"', add
label define occ_lbl 0482 `"0482"', add
label define occ_lbl 0483 `"0483"', add
label define occ_lbl 0484 `"0484"', add
label define occ_lbl 0485 `"0485"', add
label define occ_lbl 0486 `"0486"', add
label define occ_lbl 0487 `"0487"', add
label define occ_lbl 0488 `"0488"', add
label define occ_lbl 0489 `"0489"', add
label define occ_lbl 0490 `"0490"', add
label define occ_lbl 0491 `"0491"', add
label define occ_lbl 0492 `"0492"', add
label define occ_lbl 0493 `"0493"', add
label define occ_lbl 0494 `"0494"', add
label define occ_lbl 0495 `"0495"', add
label define occ_lbl 0496 `"0496"', add
label define occ_lbl 0497 `"0497"', add
label define occ_lbl 0498 `"0498"', add
label define occ_lbl 0499 `"0499"', add
label define occ_lbl 0500 `"0500"', add
label define occ_lbl 0501 `"0501"', add
label define occ_lbl 0502 `"0502"', add
label define occ_lbl 0503 `"0503"', add
label define occ_lbl 0504 `"0504"', add
label define occ_lbl 0505 `"0505"', add
label define occ_lbl 0506 `"0506"', add
label define occ_lbl 0507 `"0507"', add
label define occ_lbl 0508 `"0508"', add
label define occ_lbl 0509 `"0509"', add
label define occ_lbl 0510 `"0510"', add
label define occ_lbl 0511 `"0511"', add
label define occ_lbl 0512 `"0512"', add
label define occ_lbl 0513 `"0513"', add
label define occ_lbl 0514 `"0514"', add
label define occ_lbl 0515 `"0515"', add
label define occ_lbl 0516 `"0516"', add
label define occ_lbl 0517 `"0517"', add
label define occ_lbl 0518 `"0518"', add
label define occ_lbl 0519 `"0519"', add
label define occ_lbl 0520 `"0520"', add
label define occ_lbl 0521 `"0521"', add
label define occ_lbl 0522 `"0522"', add
label define occ_lbl 0523 `"0523"', add
label define occ_lbl 0524 `"0524"', add
label define occ_lbl 0525 `"0525"', add
label define occ_lbl 0526 `"0526"', add
label define occ_lbl 0527 `"0527"', add
label define occ_lbl 0528 `"0528"', add
label define occ_lbl 0529 `"0529"', add
label define occ_lbl 0530 `"0530"', add
label define occ_lbl 0531 `"0531"', add
label define occ_lbl 0532 `"0532"', add
label define occ_lbl 0533 `"0533"', add
label define occ_lbl 0534 `"0534"', add
label define occ_lbl 0535 `"0535"', add
label define occ_lbl 0536 `"0536"', add
label define occ_lbl 0537 `"0537"', add
label define occ_lbl 0538 `"0538"', add
label define occ_lbl 0539 `"0539"', add
label define occ_lbl 0540 `"0540"', add
label define occ_lbl 0541 `"0541"', add
label define occ_lbl 0542 `"0542"', add
label define occ_lbl 0543 `"0543"', add
label define occ_lbl 0544 `"0544"', add
label define occ_lbl 0545 `"0545"', add
label define occ_lbl 0546 `"0546"', add
label define occ_lbl 0547 `"0547"', add
label define occ_lbl 0548 `"0548"', add
label define occ_lbl 0549 `"0549"', add
label define occ_lbl 0550 `"0550"', add
label define occ_lbl 0551 `"0551"', add
label define occ_lbl 0552 `"0552"', add
label define occ_lbl 0553 `"0553"', add
label define occ_lbl 0554 `"0554"', add
label define occ_lbl 0555 `"0555"', add
label define occ_lbl 0556 `"0556"', add
label define occ_lbl 0557 `"0557"', add
label define occ_lbl 0558 `"0558"', add
label define occ_lbl 0559 `"0559"', add
label define occ_lbl 0560 `"0560"', add
label define occ_lbl 0561 `"0561"', add
label define occ_lbl 0562 `"0562"', add
label define occ_lbl 0563 `"0563"', add
label define occ_lbl 0564 `"0564"', add
label define occ_lbl 0565 `"0565"', add
label define occ_lbl 0566 `"0566"', add
label define occ_lbl 0567 `"0567"', add
label define occ_lbl 0568 `"0568"', add
label define occ_lbl 0569 `"0569"', add
label define occ_lbl 0570 `"0570"', add
label define occ_lbl 0571 `"0571"', add
label define occ_lbl 0572 `"0572"', add
label define occ_lbl 0573 `"0573"', add
label define occ_lbl 0574 `"0574"', add
label define occ_lbl 0575 `"0575"', add
label define occ_lbl 0576 `"0576"', add
label define occ_lbl 0577 `"0577"', add
label define occ_lbl 0578 `"0578"', add
label define occ_lbl 0579 `"0579"', add
label define occ_lbl 0580 `"0580"', add
label define occ_lbl 0581 `"0581"', add
label define occ_lbl 0582 `"0582"', add
label define occ_lbl 0583 `"0583"', add
label define occ_lbl 0584 `"0584"', add
label define occ_lbl 0585 `"0585"', add
label define occ_lbl 0586 `"0586"', add
label define occ_lbl 0587 `"0587"', add
label define occ_lbl 0588 `"0588"', add
label define occ_lbl 0589 `"0589"', add
label define occ_lbl 0590 `"0590"', add
label define occ_lbl 0591 `"0591"', add
label define occ_lbl 0592 `"0592"', add
label define occ_lbl 0593 `"0593"', add
label define occ_lbl 0594 `"0594"', add
label define occ_lbl 0595 `"0595"', add
label define occ_lbl 0596 `"0596"', add
label define occ_lbl 0597 `"0597"', add
label define occ_lbl 0598 `"0598"', add
label define occ_lbl 0599 `"0599"', add
label define occ_lbl 0600 `"0600"', add
label define occ_lbl 0601 `"0601"', add
label define occ_lbl 0602 `"0602"', add
label define occ_lbl 0603 `"0603"', add
label define occ_lbl 0604 `"0604"', add
label define occ_lbl 0605 `"0605"', add
label define occ_lbl 0606 `"0606"', add
label define occ_lbl 0607 `"0607"', add
label define occ_lbl 0608 `"0608"', add
label define occ_lbl 0609 `"0609"', add
label define occ_lbl 0610 `"0610"', add
label define occ_lbl 0611 `"0611"', add
label define occ_lbl 0612 `"0612"', add
label define occ_lbl 0613 `"0613"', add
label define occ_lbl 0614 `"0614"', add
label define occ_lbl 0615 `"0615"', add
label define occ_lbl 0616 `"0616"', add
label define occ_lbl 0617 `"0617"', add
label define occ_lbl 0618 `"0618"', add
label define occ_lbl 0619 `"0619"', add
label define occ_lbl 0620 `"0620"', add
label define occ_lbl 0621 `"0621"', add
label define occ_lbl 0622 `"0622"', add
label define occ_lbl 0623 `"0623"', add
label define occ_lbl 0624 `"0624"', add
label define occ_lbl 0625 `"0625"', add
label define occ_lbl 0626 `"0626"', add
label define occ_lbl 0627 `"0627"', add
label define occ_lbl 0628 `"0628"', add
label define occ_lbl 0629 `"0629"', add
label define occ_lbl 0630 `"0630"', add
label define occ_lbl 0631 `"0631"', add
label define occ_lbl 0632 `"0632"', add
label define occ_lbl 0633 `"0633"', add
label define occ_lbl 0634 `"0634"', add
label define occ_lbl 0635 `"0635"', add
label define occ_lbl 0636 `"0636"', add
label define occ_lbl 0637 `"0637"', add
label define occ_lbl 0638 `"0638"', add
label define occ_lbl 0639 `"0639"', add
label define occ_lbl 0640 `"0640"', add
label define occ_lbl 0641 `"0641"', add
label define occ_lbl 0642 `"0642"', add
label define occ_lbl 0643 `"0643"', add
label define occ_lbl 0644 `"0644"', add
label define occ_lbl 0645 `"0645"', add
label define occ_lbl 0646 `"0646"', add
label define occ_lbl 0647 `"0647"', add
label define occ_lbl 0648 `"0648"', add
label define occ_lbl 0649 `"0649"', add
label define occ_lbl 0650 `"0650"', add
label define occ_lbl 0651 `"0651"', add
label define occ_lbl 0652 `"0652"', add
label define occ_lbl 0653 `"0653"', add
label define occ_lbl 0654 `"0654"', add
label define occ_lbl 0655 `"0655"', add
label define occ_lbl 0656 `"0656"', add
label define occ_lbl 0657 `"0657"', add
label define occ_lbl 0658 `"0658"', add
label define occ_lbl 0659 `"0659"', add
label define occ_lbl 0660 `"0660"', add
label define occ_lbl 0661 `"0661"', add
label define occ_lbl 0662 `"0662"', add
label define occ_lbl 0663 `"0663"', add
label define occ_lbl 0664 `"0664"', add
label define occ_lbl 0665 `"0665"', add
label define occ_lbl 0666 `"0666"', add
label define occ_lbl 0667 `"0667"', add
label define occ_lbl 0668 `"0668"', add
label define occ_lbl 0669 `"0669"', add
label define occ_lbl 0670 `"0670"', add
label define occ_lbl 0671 `"0671"', add
label define occ_lbl 0672 `"0672"', add
label define occ_lbl 0673 `"0673"', add
label define occ_lbl 0674 `"0674"', add
label define occ_lbl 0675 `"0675"', add
label define occ_lbl 0676 `"0676"', add
label define occ_lbl 0677 `"0677"', add
label define occ_lbl 0678 `"0678"', add
label define occ_lbl 0679 `"0679"', add
label define occ_lbl 0680 `"0680"', add
label define occ_lbl 0681 `"0681"', add
label define occ_lbl 0682 `"0682"', add
label define occ_lbl 0683 `"0683"', add
label define occ_lbl 0684 `"0684"', add
label define occ_lbl 0685 `"0685"', add
label define occ_lbl 0686 `"0686"', add
label define occ_lbl 0687 `"0687"', add
label define occ_lbl 0688 `"0688"', add
label define occ_lbl 0689 `"0689"', add
label define occ_lbl 0690 `"0690"', add
label define occ_lbl 0691 `"0691"', add
label define occ_lbl 0692 `"0692"', add
label define occ_lbl 0693 `"0693"', add
label define occ_lbl 0694 `"0694"', add
label define occ_lbl 0695 `"0695"', add
label define occ_lbl 0696 `"0696"', add
label define occ_lbl 0697 `"0697"', add
label define occ_lbl 0698 `"0698"', add
label define occ_lbl 0699 `"0699"', add
label define occ_lbl 0700 `"0700"', add
label define occ_lbl 0701 `"0701"', add
label define occ_lbl 0702 `"0702"', add
label define occ_lbl 0703 `"0703"', add
label define occ_lbl 0704 `"0704"', add
label define occ_lbl 0705 `"0705"', add
label define occ_lbl 0706 `"0706"', add
label define occ_lbl 0707 `"0707"', add
label define occ_lbl 0708 `"0708"', add
label define occ_lbl 0709 `"0709"', add
label define occ_lbl 0710 `"0710"', add
label define occ_lbl 0711 `"0711"', add
label define occ_lbl 0712 `"0712"', add
label define occ_lbl 0713 `"0713"', add
label define occ_lbl 0714 `"0714"', add
label define occ_lbl 0715 `"0715"', add
label define occ_lbl 0716 `"0716"', add
label define occ_lbl 0717 `"0717"', add
label define occ_lbl 0718 `"0718"', add
label define occ_lbl 0719 `"0719"', add
label define occ_lbl 0720 `"0720"', add
label define occ_lbl 0721 `"0721"', add
label define occ_lbl 0722 `"0722"', add
label define occ_lbl 0723 `"0723"', add
label define occ_lbl 0724 `"0724"', add
label define occ_lbl 0725 `"0725"', add
label define occ_lbl 0726 `"0726"', add
label define occ_lbl 0727 `"0727"', add
label define occ_lbl 0728 `"0728"', add
label define occ_lbl 0729 `"0729"', add
label define occ_lbl 0730 `"0730"', add
label define occ_lbl 0731 `"0731"', add
label define occ_lbl 0732 `"0732"', add
label define occ_lbl 0733 `"0733"', add
label define occ_lbl 0734 `"0734"', add
label define occ_lbl 0735 `"0735"', add
label define occ_lbl 0736 `"0736"', add
label define occ_lbl 0737 `"0737"', add
label define occ_lbl 0738 `"0738"', add
label define occ_lbl 0739 `"0739"', add
label define occ_lbl 0740 `"0740"', add
label define occ_lbl 0741 `"0741"', add
label define occ_lbl 0742 `"0742"', add
label define occ_lbl 0743 `"0743"', add
label define occ_lbl 0744 `"0744"', add
label define occ_lbl 0745 `"0745"', add
label define occ_lbl 0746 `"0746"', add
label define occ_lbl 0747 `"0747"', add
label define occ_lbl 0748 `"0748"', add
label define occ_lbl 0749 `"0749"', add
label define occ_lbl 0750 `"0750"', add
label define occ_lbl 0751 `"0751"', add
label define occ_lbl 0752 `"0752"', add
label define occ_lbl 0753 `"0753"', add
label define occ_lbl 0754 `"0754"', add
label define occ_lbl 0755 `"0755"', add
label define occ_lbl 0756 `"0756"', add
label define occ_lbl 0757 `"0757"', add
label define occ_lbl 0758 `"0758"', add
label define occ_lbl 0759 `"0759"', add
label define occ_lbl 0760 `"0760"', add
label define occ_lbl 0761 `"0761"', add
label define occ_lbl 0762 `"0762"', add
label define occ_lbl 0763 `"0763"', add
label define occ_lbl 0764 `"0764"', add
label define occ_lbl 0765 `"0765"', add
label define occ_lbl 0766 `"0766"', add
label define occ_lbl 0767 `"0767"', add
label define occ_lbl 0768 `"0768"', add
label define occ_lbl 0769 `"0769"', add
label define occ_lbl 0770 `"0770"', add
label define occ_lbl 0771 `"0771"', add
label define occ_lbl 0772 `"0772"', add
label define occ_lbl 0773 `"0773"', add
label define occ_lbl 0774 `"0774"', add
label define occ_lbl 0775 `"0775"', add
label define occ_lbl 0776 `"0776"', add
label define occ_lbl 0777 `"0777"', add
label define occ_lbl 0778 `"0778"', add
label define occ_lbl 0779 `"0779"', add
label define occ_lbl 0780 `"0780"', add
label define occ_lbl 0781 `"0781"', add
label define occ_lbl 0782 `"0782"', add
label define occ_lbl 0783 `"0783"', add
label define occ_lbl 0784 `"0784"', add
label define occ_lbl 0785 `"0785"', add
label define occ_lbl 0786 `"0786"', add
label define occ_lbl 0787 `"0787"', add
label define occ_lbl 0788 `"0788"', add
label define occ_lbl 0789 `"0789"', add
label define occ_lbl 0790 `"0790"', add
label define occ_lbl 0791 `"0791"', add
label define occ_lbl 0792 `"0792"', add
label define occ_lbl 0793 `"0793"', add
label define occ_lbl 0794 `"0794"', add
label define occ_lbl 0795 `"0795"', add
label define occ_lbl 0796 `"0796"', add
label define occ_lbl 0797 `"0797"', add
label define occ_lbl 0798 `"0798"', add
label define occ_lbl 0799 `"0799"', add
label define occ_lbl 0800 `"0800"', add
label define occ_lbl 0801 `"0801"', add
label define occ_lbl 0802 `"0802"', add
label define occ_lbl 0803 `"0803"', add
label define occ_lbl 0804 `"0804"', add
label define occ_lbl 0805 `"0805"', add
label define occ_lbl 0806 `"0806"', add
label define occ_lbl 0807 `"0807"', add
label define occ_lbl 0808 `"0808"', add
label define occ_lbl 0809 `"0809"', add
label define occ_lbl 0810 `"0810"', add
label define occ_lbl 0811 `"0811"', add
label define occ_lbl 0812 `"0812"', add
label define occ_lbl 0813 `"0813"', add
label define occ_lbl 0814 `"0814"', add
label define occ_lbl 0815 `"0815"', add
label define occ_lbl 0816 `"0816"', add
label define occ_lbl 0817 `"0817"', add
label define occ_lbl 0818 `"0818"', add
label define occ_lbl 0819 `"0819"', add
label define occ_lbl 0820 `"0820"', add
label define occ_lbl 0821 `"0821"', add
label define occ_lbl 0822 `"0822"', add
label define occ_lbl 0823 `"0823"', add
label define occ_lbl 0824 `"0824"', add
label define occ_lbl 0825 `"0825"', add
label define occ_lbl 0826 `"0826"', add
label define occ_lbl 0827 `"0827"', add
label define occ_lbl 0828 `"0828"', add
label define occ_lbl 0829 `"0829"', add
label define occ_lbl 0830 `"0830"', add
label define occ_lbl 0831 `"0831"', add
label define occ_lbl 0832 `"0832"', add
label define occ_lbl 0833 `"0833"', add
label define occ_lbl 0834 `"0834"', add
label define occ_lbl 0835 `"0835"', add
label define occ_lbl 0836 `"0836"', add
label define occ_lbl 0837 `"0837"', add
label define occ_lbl 0838 `"0838"', add
label define occ_lbl 0839 `"0839"', add
label define occ_lbl 0840 `"0840"', add
label define occ_lbl 0841 `"0841"', add
label define occ_lbl 0842 `"0842"', add
label define occ_lbl 0843 `"0843"', add
label define occ_lbl 0844 `"0844"', add
label define occ_lbl 0845 `"0845"', add
label define occ_lbl 0846 `"0846"', add
label define occ_lbl 0847 `"0847"', add
label define occ_lbl 0848 `"0848"', add
label define occ_lbl 0849 `"0849"', add
label define occ_lbl 0850 `"0850"', add
label define occ_lbl 0851 `"0851"', add
label define occ_lbl 0852 `"0852"', add
label define occ_lbl 0853 `"0853"', add
label define occ_lbl 0854 `"0854"', add
label define occ_lbl 0855 `"0855"', add
label define occ_lbl 0856 `"0856"', add
label define occ_lbl 0857 `"0857"', add
label define occ_lbl 0858 `"0858"', add
label define occ_lbl 0859 `"0859"', add
label define occ_lbl 0860 `"0860"', add
label define occ_lbl 0861 `"0861"', add
label define occ_lbl 0862 `"0862"', add
label define occ_lbl 0863 `"0863"', add
label define occ_lbl 0864 `"0864"', add
label define occ_lbl 0865 `"0865"', add
label define occ_lbl 0866 `"0866"', add
label define occ_lbl 0867 `"0867"', add
label define occ_lbl 0868 `"0868"', add
label define occ_lbl 0869 `"0869"', add
label define occ_lbl 0870 `"0870"', add
label define occ_lbl 0871 `"0871"', add
label define occ_lbl 0872 `"0872"', add
label define occ_lbl 0873 `"0873"', add
label define occ_lbl 0874 `"0874"', add
label define occ_lbl 0875 `"0875"', add
label define occ_lbl 0876 `"0876"', add
label define occ_lbl 0877 `"0877"', add
label define occ_lbl 0878 `"0878"', add
label define occ_lbl 0879 `"0879"', add
label define occ_lbl 0880 `"0880"', add
label define occ_lbl 0881 `"0881"', add
label define occ_lbl 0882 `"0882"', add
label define occ_lbl 0883 `"0883"', add
label define occ_lbl 0884 `"0884"', add
label define occ_lbl 0885 `"0885"', add
label define occ_lbl 0886 `"0886"', add
label define occ_lbl 0887 `"0887"', add
label define occ_lbl 0888 `"0888"', add
label define occ_lbl 0889 `"0889"', add
label define occ_lbl 0890 `"0890"', add
label define occ_lbl 0891 `"0891"', add
label define occ_lbl 0892 `"0892"', add
label define occ_lbl 0893 `"0893"', add
label define occ_lbl 0894 `"0894"', add
label define occ_lbl 0895 `"0895"', add
label define occ_lbl 0896 `"0896"', add
label define occ_lbl 0897 `"0897"', add
label define occ_lbl 0898 `"0898"', add
label define occ_lbl 0899 `"0899"', add
label define occ_lbl 0900 `"0900"', add
label define occ_lbl 0901 `"0901"', add
label define occ_lbl 0902 `"0902"', add
label define occ_lbl 0903 `"0903"', add
label define occ_lbl 0904 `"0904"', add
label define occ_lbl 0905 `"0905"', add
label define occ_lbl 0906 `"0906"', add
label define occ_lbl 0907 `"0907"', add
label define occ_lbl 0908 `"0908"', add
label define occ_lbl 0909 `"0909"', add
label define occ_lbl 0910 `"0910"', add
label define occ_lbl 0911 `"0911"', add
label define occ_lbl 0912 `"0912"', add
label define occ_lbl 0913 `"0913"', add
label define occ_lbl 0914 `"0914"', add
label define occ_lbl 0915 `"0915"', add
label define occ_lbl 0916 `"0916"', add
label define occ_lbl 0917 `"0917"', add
label define occ_lbl 0918 `"0918"', add
label define occ_lbl 0919 `"0919"', add
label define occ_lbl 0920 `"0920"', add
label define occ_lbl 0921 `"0921"', add
label define occ_lbl 0922 `"0922"', add
label define occ_lbl 0923 `"0923"', add
label define occ_lbl 0924 `"0924"', add
label define occ_lbl 0925 `"0925"', add
label define occ_lbl 0926 `"0926"', add
label define occ_lbl 0927 `"0927"', add
label define occ_lbl 0928 `"0928"', add
label define occ_lbl 0929 `"0929"', add
label define occ_lbl 0930 `"0930"', add
label define occ_lbl 0931 `"0931"', add
label define occ_lbl 0932 `"0932"', add
label define occ_lbl 0933 `"0933"', add
label define occ_lbl 0934 `"0934"', add
label define occ_lbl 0935 `"0935"', add
label define occ_lbl 0936 `"0936"', add
label define occ_lbl 0937 `"0937"', add
label define occ_lbl 0938 `"0938"', add
label define occ_lbl 0939 `"0939"', add
label define occ_lbl 0940 `"0940"', add
label define occ_lbl 0941 `"0941"', add
label define occ_lbl 0942 `"0942"', add
label define occ_lbl 0943 `"0943"', add
label define occ_lbl 0944 `"0944"', add
label define occ_lbl 0945 `"0945"', add
label define occ_lbl 0946 `"0946"', add
label define occ_lbl 0947 `"0947"', add
label define occ_lbl 0948 `"0948"', add
label define occ_lbl 0949 `"0949"', add
label define occ_lbl 0950 `"0950"', add
label define occ_lbl 0951 `"0951"', add
label define occ_lbl 0952 `"0952"', add
label define occ_lbl 0953 `"0953"', add
label define occ_lbl 0954 `"0954"', add
label define occ_lbl 0955 `"0955"', add
label define occ_lbl 0956 `"0956"', add
label define occ_lbl 0957 `"0957"', add
label define occ_lbl 0958 `"0958"', add
label define occ_lbl 0959 `"0959"', add
label define occ_lbl 0960 `"0960"', add
label define occ_lbl 0961 `"0961"', add
label define occ_lbl 0962 `"0962"', add
label define occ_lbl 0963 `"0963"', add
label define occ_lbl 0964 `"0964"', add
label define occ_lbl 0965 `"0965"', add
label define occ_lbl 0966 `"0966"', add
label define occ_lbl 0967 `"0967"', add
label define occ_lbl 0968 `"0968"', add
label define occ_lbl 0969 `"0969"', add
label define occ_lbl 0970 `"0970"', add
label define occ_lbl 0971 `"0971"', add
label define occ_lbl 0972 `"0972"', add
label define occ_lbl 0973 `"0973"', add
label define occ_lbl 0974 `"0974"', add
label define occ_lbl 0975 `"0975"', add
label define occ_lbl 0976 `"0976"', add
label define occ_lbl 0977 `"0977"', add
label define occ_lbl 0978 `"0978"', add
label define occ_lbl 0979 `"0979"', add
label define occ_lbl 0980 `"0980"', add
label define occ_lbl 0981 `"0981"', add
label define occ_lbl 0982 `"0982"', add
label define occ_lbl 0983 `"0983"', add
label define occ_lbl 0984 `"0984"', add
label define occ_lbl 0985 `"0985"', add
label define occ_lbl 0986 `"0986"', add
label define occ_lbl 0987 `"0987"', add
label define occ_lbl 0988 `"0988"', add
label define occ_lbl 0989 `"0989"', add
label define occ_lbl 0990 `"0990"', add
label define occ_lbl 0991 `"0991"', add
label define occ_lbl 0992 `"0992"', add
label define occ_lbl 0993 `"0993"', add
label define occ_lbl 0994 `"0994"', add
label define occ_lbl 0995 `"0995"', add
label define occ_lbl 0996 `"0996"', add
label define occ_lbl 0997 `"0997"', add
label define occ_lbl 0998 `"0998"', add
label define occ_lbl 0999 `"0999"', add
label values occ occ_lbl

label define occ1950_lbl 000 `"Accountants and auditors"'
label define occ1950_lbl 001 `"Actors and actresses"', add
label define occ1950_lbl 002 `"Airplane pilots and navigators"', add
label define occ1950_lbl 003 `"Architects"', add
label define occ1950_lbl 004 `"Artists and art teachers"', add
label define occ1950_lbl 005 `"Athletes"', add
label define occ1950_lbl 006 `"Authors"', add
label define occ1950_lbl 007 `"Chemists"', add
label define occ1950_lbl 008 `"Chiropractors"', add
label define occ1950_lbl 009 `"Clergymen"', add
label define occ1950_lbl 010 `"College presidents and deans"', add
label define occ1950_lbl 012 `"Agricultural sciences-Professors and instructors"', add
label define occ1950_lbl 013 `"Biological sciences-Professors and instructors"', add
label define occ1950_lbl 014 `"Chemistry-Professors and instructors"', add
label define occ1950_lbl 015 `"Economics-Professors and instructors"', add
label define occ1950_lbl 016 `"Engineering-Professors and instructors"', add
label define occ1950_lbl 017 `"Geology and geophysics-Professors and instructors"', add
label define occ1950_lbl 018 `"Mathematics-Professors and instructors"', add
label define occ1950_lbl 019 `"Medical Sciences-Professors and instructors"', add
label define occ1950_lbl 023 `"Physics-Professors and instructors"', add
label define occ1950_lbl 024 `"Psychology-Professors and instructors"', add
label define occ1950_lbl 025 `"Statistics-Professors and instructors"', add
label define occ1950_lbl 026 `"Natural science (nec)-Professors and instructors"', add
label define occ1950_lbl 027 `"Social sciences (nec)-Professors and instructors"', add
label define occ1950_lbl 028 `"Non-scientific subjects-Professors and instructors"', add
label define occ1950_lbl 029 `"Subject not specified-Professors and instructors"', add
label define occ1950_lbl 031 `"Dancers and dancing teachers"', add
label define occ1950_lbl 032 `"Dentists"', add
label define occ1950_lbl 033 `"Designers"', add
label define occ1950_lbl 034 `"Dietitians and nutritionists"', add
label define occ1950_lbl 035 `"Draftsmen"', add
label define occ1950_lbl 036 `"Editors and reporters"', add
label define occ1950_lbl 041 `"Aeronautical-Engineers"', add
label define occ1950_lbl 042 `"Chemical-Engineers"', add
label define occ1950_lbl 043 `"Civil-Engineers"', add
label define occ1950_lbl 044 `"Electrical-Engineers"', add
label define occ1950_lbl 045 `"Industrial-Engineers"', add
label define occ1950_lbl 046 `"Mechanical-Engineers"', add
label define occ1950_lbl 047 `"Metallurgical, metallurgists-Engineers"', add
label define occ1950_lbl 048 `"Mining-Engineers"', add
label define occ1950_lbl 049 `"Engineers (nec)"', add
label define occ1950_lbl 051 `"Entertainers (nec)"', add
label define occ1950_lbl 052 `"Farm and home management advisors"', add
label define occ1950_lbl 053 `"Foresters and conservationists"', add
label define occ1950_lbl 054 `"Funeral directors and embalmers"', add
label define occ1950_lbl 055 `"Lawyers and judges"', add
label define occ1950_lbl 056 `"Librarians"', add
label define occ1950_lbl 057 `"Musicians and music teachers"', add
label define occ1950_lbl 058 `"Nurses, professional"', add
label define occ1950_lbl 059 `"Nurses, student professional"', add
label define occ1950_lbl 061 `"Agricultural scientists"', add
label define occ1950_lbl 062 `"Biological scientists"', add
label define occ1950_lbl 063 `"Geologists and geophysicists"', add
label define occ1950_lbl 067 `"Mathematicians"', add
label define occ1950_lbl 068 `"Physicists"', add
label define occ1950_lbl 069 `"Misc. natural scientists"', add
label define occ1950_lbl 070 `"Optometrists"', add
label define occ1950_lbl 071 `"Osteopaths"', add
label define occ1950_lbl 072 `"Personnel and labor relations workers"', add
label define occ1950_lbl 073 `"Pharmacists"', add
label define occ1950_lbl 074 `"Photographers"', add
label define occ1950_lbl 075 `"Physicians and surgeons"', add
label define occ1950_lbl 076 `"Radio operators"', add
label define occ1950_lbl 077 `"Recreation and group workers"', add
label define occ1950_lbl 078 `"Religious workers"', add
label define occ1950_lbl 079 `"Social and welfare workers, except group"', add
label define occ1950_lbl 081 `"Economists"', add
label define occ1950_lbl 082 `"Psychologists"', add
label define occ1950_lbl 083 `"Statisticians and actuaries"', add
label define occ1950_lbl 084 `"Misc social scientists"', add
label define occ1950_lbl 091 `"Sports instructors and officials"', add
label define occ1950_lbl 092 `"Surveyors"', add
label define occ1950_lbl 093 `"Teachers (n.e.c.)"', add
label define occ1950_lbl 094 `"Medical and dental-technicians"', add
label define occ1950_lbl 095 `"Testing-technicians"', add
label define occ1950_lbl 096 `"Technicians (nec)"', add
label define occ1950_lbl 097 `"Therapists and healers (nec)"', add
label define occ1950_lbl 098 `"Veterinarians"', add
label define occ1950_lbl 099 `"Professional, technical and kindred workers (nec)"', add
label define occ1950_lbl 100 `"Farmers (owners and tenants)"', add
label define occ1950_lbl 123 `"Farm managers"', add
label define occ1950_lbl 200 `"Buyers and dept heads, store"', add
label define occ1950_lbl 201 `"Buyers and shippers, farm products"', add
label define occ1950_lbl 203 `"Conductors, railroad"', add
label define occ1950_lbl 204 `"Credit men"', add
label define occ1950_lbl 205 `"Floormen and floor managers, store"', add
label define occ1950_lbl 210 `"Inspectors, public administration"', add
label define occ1950_lbl 230 `"Managers and superintendants, building"', add
label define occ1950_lbl 240 `"Officers, pilots, pursers and engineers, ship"', add
label define occ1950_lbl 250 `"Officials and administratators (nec), public administration"', add
label define occ1950_lbl 260 `"Officials, lodge, society, union, etc."', add
label define occ1950_lbl 270 `"Postmasters"', add
label define occ1950_lbl 280 `"Purchasing agents and buyers (nec)"', add
label define occ1950_lbl 290 `"Managers, officials, and proprietors (nec)"', add
label define occ1950_lbl 300 `"Agents (nec)"', add
label define occ1950_lbl 301 `"Attendants and assistants, library"', add
label define occ1950_lbl 302 `"Attendants, physicians and dentists office"', add
label define occ1950_lbl 304 `"Baggagemen, transportation"', add
label define occ1950_lbl 305 `"Bank tellers"', add
label define occ1950_lbl 310 `"Bookkeepers"', add
label define occ1950_lbl 320 `"Cashiers"', add
label define occ1950_lbl 321 `"Collectors, bill and account"', add
label define occ1950_lbl 322 `"Dispatchers and starters, vehicle"', add
label define occ1950_lbl 325 `"Express messengers and railway mail clerks"', add
label define occ1950_lbl 335 `"Mail carriers"', add
label define occ1950_lbl 340 `"Messengers and office boys"', add
label define occ1950_lbl 341 `"Office machine operators"', add
label define occ1950_lbl 342 `"Shipping and receiving clerks"', add
label define occ1950_lbl 350 `"Stenographers, typists, and secretaries"', add
label define occ1950_lbl 360 `"Telegraph messengers"', add
label define occ1950_lbl 365 `"Telegraph operators"', add
label define occ1950_lbl 370 `"Telephone operators"', add
label define occ1950_lbl 380 `"Ticket, station, and express agents"', add
label define occ1950_lbl 390 `"Clerical and kindred workers (n.e.c.)"', add
label define occ1950_lbl 400 `"Advertising agents and salesmen"', add
label define occ1950_lbl 410 `"Auctioneers"', add
label define occ1950_lbl 420 `"Demonstrators"', add
label define occ1950_lbl 430 `"Hucksters and peddlers"', add
label define occ1950_lbl 450 `"Insurance agents and brokers"', add
label define occ1950_lbl 460 `"Newsboys"', add
label define occ1950_lbl 470 `"Real estate agents and brokers"', add
label define occ1950_lbl 480 `"Stock and bond salesmen"', add
label define occ1950_lbl 490 `"Salesmen and sales clerks (nec)"', add
label define occ1950_lbl 500 `"Bakers"', add
label define occ1950_lbl 501 `"Blacksmiths"', add
label define occ1950_lbl 502 `"Bookbinders"', add
label define occ1950_lbl 503 `"Boilermakers"', add
label define occ1950_lbl 504 `"Brickmasons,stonemasons, and tile setters"', add
label define occ1950_lbl 505 `"Cabinetmakers"', add
label define occ1950_lbl 510 `"Carpenters"', add
label define occ1950_lbl 511 `"Cement and concrete finishers"', add
label define occ1950_lbl 512 `"Compositors and typesetters"', add
label define occ1950_lbl 513 `"Cranemen,derrickmen, and hoistmen"', add
label define occ1950_lbl 514 `"Decorators and window dressers"', add
label define occ1950_lbl 515 `"Electricians"', add
label define occ1950_lbl 520 `"Electrotypers and stereotypers"', add
label define occ1950_lbl 521 `"Engravers, except photoengravers"', add
label define occ1950_lbl 522 `"Excavating, grading, and road machinery operators"', add
label define occ1950_lbl 523 `"Foremen (nec)"', add
label define occ1950_lbl 524 `"Forgemen and hammermen"', add
label define occ1950_lbl 525 `"Furriers"', add
label define occ1950_lbl 530 `"Glaziers"', add
label define occ1950_lbl 531 `"Heat treaters, annealers, temperers"', add
label define occ1950_lbl 532 `"Inspectors, scalers, and graders log and lumber"', add
label define occ1950_lbl 533 `"Inspectors (nec)"', add
label define occ1950_lbl 534 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define occ1950_lbl 535 `"Job setters, metal"', add
label define occ1950_lbl 540 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define occ1950_lbl 541 `"Locomotive engineers"', add
label define occ1950_lbl 542 `"Locomotive firemen"', add
label define occ1950_lbl 543 `"Loom fixers"', add
label define occ1950_lbl 544 `"Machinists"', add
label define occ1950_lbl 545 `"Airplane-mechanics and repairmen"', add
label define occ1950_lbl 550 `"Automobile-mechanics and repairmen"', add
label define occ1950_lbl 551 `"Office machine-mechanics and repairmen"', add
label define occ1950_lbl 552 `"Radio and television-mechanics and repairmen"', add
label define occ1950_lbl 553 `"Railroad and car shop-mechanics and repairmen"', add
label define occ1950_lbl 554 `"Mechanics and repairmen (nec)"', add
label define occ1950_lbl 555 `"Millers, grain, flour, feed, etc"', add
label define occ1950_lbl 560 `"Millwrights"', add
label define occ1950_lbl 561 `"Molders, metal"', add
label define occ1950_lbl 562 `"Motion picture projectionists"', add
label define occ1950_lbl 563 `"Opticians and lens grinders and polishers"', add
label define occ1950_lbl 564 `"Painters, construction and maintenance"', add
label define occ1950_lbl 565 `"Paperhangers"', add
label define occ1950_lbl 570 `"Pattern and model makers, except paper"', add
label define occ1950_lbl 571 `"Photoengravers and lithographers"', add
label define occ1950_lbl 572 `"Piano and organ tuners and repairmen"', add
label define occ1950_lbl 573 `"Plasterers"', add
label define occ1950_lbl 574 `"Plumbers and pipe fitters"', add
label define occ1950_lbl 575 `"Pressmen and plate printers, printing"', add
label define occ1950_lbl 580 `"Rollers and roll hands, metal"', add
label define occ1950_lbl 581 `"Roofers and slaters"', add
label define occ1950_lbl 582 `"Shoemakers and repairers, except factory"', add
label define occ1950_lbl 583 `"Stationary engineers"', add
label define occ1950_lbl 584 `"Stone cutters and stone carvers"', add
label define occ1950_lbl 585 `"Structural metal workers"', add
label define occ1950_lbl 590 `"Tailors and tailoresses"', add
label define occ1950_lbl 591 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ1950_lbl 592 `"Tool makers, and die makers and setters"', add
label define occ1950_lbl 593 `"Upholsterers"', add
label define occ1950_lbl 594 `"Craftsmen and kindred workers (nec)"', add
label define occ1950_lbl 595 `"Members of the armed services"', add
label define occ1950_lbl 600 `"Auto mechanics apprentice"', add
label define occ1950_lbl 601 `"Bricklayers and masons apprentice"', add
label define occ1950_lbl 602 `"Carpenters apprentice"', add
label define occ1950_lbl 603 `"Electricians apprentice"', add
label define occ1950_lbl 604 `"Machinists and toolmakers apprentice"', add
label define occ1950_lbl 605 `"Mechanics, except auto apprentice"', add
label define occ1950_lbl 610 `"Plumbers and pipe fitters apprentice"', add
label define occ1950_lbl 611 `"Apprentices, building trades (nec)"', add
label define occ1950_lbl 612 `"Apprentices, metalworking trades (nec)"', add
label define occ1950_lbl 613 `"Apprentices, printing  trades"', add
label define occ1950_lbl 614 `"Apprentices, other specified trades"', add
label define occ1950_lbl 615 `"Apprentices, trade not specified"', add
label define occ1950_lbl 620 `"Asbestos and insulation workers"', add
label define occ1950_lbl 621 `"Attendants, auto service and parking"', add
label define occ1950_lbl 622 `"Blasters and powdermen"', add
label define occ1950_lbl 623 `"Boatmen, canalmen, and lock keepers"', add
label define occ1950_lbl 624 `"Brakemen, railroad"', add
label define occ1950_lbl 625 `"Bus drivers"', add
label define occ1950_lbl 630 `"Chainmen, rodmen, and axmen, surveying"', add
label define occ1950_lbl 631 `"Conductors, bus and street railway"', add
label define occ1950_lbl 632 `"Deliverymen and routemen"', add
label define occ1950_lbl 633 `"Dressmakers and seamstresses, except factory"', add
label define occ1950_lbl 634 `"Dyers"', add
label define occ1950_lbl 635 `"Filers, grinders, and polishers, metal"', add
label define occ1950_lbl 640 `"Fruit, nut, and vegetable graders, and packers, except facto"', add
label define occ1950_lbl 641 `"Furnacemen, smeltermen and pourers"', add
label define occ1950_lbl 642 `"Heaters, metal"', add
label define occ1950_lbl 643 `"Laundry and dry cleaning Operatives"', add
label define occ1950_lbl 644 `"Meat cutters, except slaughter and packing house"', add
label define occ1950_lbl 645 `"Milliners"', add
label define occ1950_lbl 650 `"Mine operatives and laborers"', add
label define occ1950_lbl 660 `"Motormen, mine, factory, logging camp, etc"', add
label define occ1950_lbl 661 `"Motormen, street, subway, and elevated railway"', add
label define occ1950_lbl 662 `"Oilers and greaser, except auto"', add
label define occ1950_lbl 670 `"Painters, except construction or maintenance"', add
label define occ1950_lbl 671 `"Photographic process workers"', add
label define occ1950_lbl 672 `"Power station operators"', add
label define occ1950_lbl 673 `"Sailors and deck hands"', add
label define occ1950_lbl 674 `"Sawyers"', add
label define occ1950_lbl 675 `"Spinners, textile"', add
label define occ1950_lbl 680 `"Stationary firemen"', add
label define occ1950_lbl 681 `"Switchmen, railroad"', add
label define occ1950_lbl 682 `"Taxicab drivers and chauffeurs"', add
label define occ1950_lbl 683 `"Truck and tractor drivers"', add
label define occ1950_lbl 684 `"Weavers, textile"', add
label define occ1950_lbl 685 `"Welders and flame cutters"', add
label define occ1950_lbl 690 `"Operative and kindred workers (nec)"', add
label define occ1950_lbl 700 `"Housekeepers, private household"', add
label define occ1950_lbl 710 `"Laundresses, private household"', add
label define occ1950_lbl 720 `"Private household workers (nec)"', add
label define occ1950_lbl 730 `"Attendants, hospital and other institution"', add
label define occ1950_lbl 731 `"Attendants, professional and personal service (nec)"', add
label define occ1950_lbl 732 `"Attendants, recreation and amusement"', add
label define occ1950_lbl 740 `"Barbers, beauticians, and manicurists"', add
label define occ1950_lbl 750 `"Bartenders"', add
label define occ1950_lbl 751 `"Bootblacks"', add
label define occ1950_lbl 752 `"Boarding and lodging house keepers"', add
label define occ1950_lbl 753 `"Charwomen and cleaners"', add
label define occ1950_lbl 754 `"Cooks, except private household"', add
label define occ1950_lbl 760 `"Counter and fountain workers"', add
label define occ1950_lbl 761 `"Elevator operators"', add
label define occ1950_lbl 762 `"Firemen, fire protection"', add
label define occ1950_lbl 763 `"Guards, watchmen, and doorkeepers"', add
label define occ1950_lbl 764 `"Housekeepers and stewards, except private household"', add
label define occ1950_lbl 770 `"Janitors and sextons"', add
label define occ1950_lbl 771 `"Marshals and constables"', add
label define occ1950_lbl 772 `"Midwives"', add
label define occ1950_lbl 773 `"Policemen and detectives"', add
label define occ1950_lbl 780 `"Porters"', add
label define occ1950_lbl 781 `"Practical nurses"', add
label define occ1950_lbl 782 `"Sheriffs and bailiffs"', add
label define occ1950_lbl 783 `"Ushers, recreation and amusement"', add
label define occ1950_lbl 784 `"Waiters and waitresses"', add
label define occ1950_lbl 785 `"Watchmen (crossing) and bridge tenders"', add
label define occ1950_lbl 790 `"Service workers, except private household (nec)"', add
label define occ1950_lbl 810 `"Farm foremen"', add
label define occ1950_lbl 820 `"Farm laborers, wage workers"', add
label define occ1950_lbl 830 `"Farm laborers, unpaid family workers"', add
label define occ1950_lbl 840 `"Farm service laborers, self-employed"', add
label define occ1950_lbl 910 `"Fishermen and oystermen"', add
label define occ1950_lbl 920 `"Garage laborers and car washers and greasers"', add
label define occ1950_lbl 930 `"Gardeners, except farm and groundskeepers"', add
label define occ1950_lbl 940 `"Longshoremen and stevedores"', add
label define occ1950_lbl 950 `"Lumbermen, raftsmen, and woodchoppers"', add
label define occ1950_lbl 960 `"Teamsters"', add
label define occ1950_lbl 970 `"Laborers (nec)"', add
label define occ1950_lbl 979 `"Not yet classified"', add
label define occ1950_lbl 980 `"Keeps house/housekeeping at home/housewife"', add
label define occ1950_lbl 981 `"Imputed keeping house (1850-1900)"', add
label define occ1950_lbl 982 `"Helping at home/helps parents/housework"', add
label define occ1950_lbl 983 `"At school/student"', add
label define occ1950_lbl 984 `"Retired"', add
label define occ1950_lbl 985 `"Unemployed/without occupation"', add
label define occ1950_lbl 986 `"Invalid/disabled w/ no occupation reported"', add
label define occ1950_lbl 987 `"Inmate"', add
label define occ1950_lbl 990 `"New Worker"', add
label define occ1950_lbl 991 `"Gentleman/lady/at leisure"', add
label define occ1950_lbl 995 `"Other non-occupation"', add
label define occ1950_lbl 997 `"Occupation missing/unknown"', add
label define occ1950_lbl 999 `"N/A (blank)"', add
label values occ1950 occ1950_lbl

label define ind1950_lbl 000 `"N/A or none reported"'
label define ind1950_lbl 105 `"Agriculture"', add
label define ind1950_lbl 116 `"Forestry"', add
label define ind1950_lbl 126 `"Fisheries"', add
label define ind1950_lbl 206 `"Metal mining"', add
label define ind1950_lbl 216 `"Coal mining"', add
label define ind1950_lbl 226 `"Crude petroleum and natural gas extraction"', add
label define ind1950_lbl 236 `"Nonmettalic  mining and quarrying, except fuel"', add
label define ind1950_lbl 239 `"Mining, not specified"', add
label define ind1950_lbl 246 `"Construction"', add
label define ind1950_lbl 306 `"Logging"', add
label define ind1950_lbl 307 `"Sawmills, planing mills, and mill work"', add
label define ind1950_lbl 308 `"Misc wood products"', add
label define ind1950_lbl 309 `"Furniture and fixtures"', add
label define ind1950_lbl 316 `"Glass and glass products"', add
label define ind1950_lbl 317 `"Cement, concrete, gypsum and plaster products"', add
label define ind1950_lbl 318 `"Structural clay products"', add
label define ind1950_lbl 319 `"Pottery and related prods"', add
label define ind1950_lbl 326 `"Misc nonmetallic mineral and stone products"', add
label define ind1950_lbl 336 `"Blast furnaces, steel works, and rolling mills"', add
label define ind1950_lbl 337 `"Other primary iron and steel industries"', add
label define ind1950_lbl 338 `"Primary nonferrous industries"', add
label define ind1950_lbl 346 `"Fabricated steel products"', add
label define ind1950_lbl 347 `"Fabricated nonferrous metal products"', add
label define ind1950_lbl 348 `"Not specified metal industries"', add
label define ind1950_lbl 356 `"Agricultural machinery and tractors"', add
label define ind1950_lbl 357 `"Office and store machines"', add
label define ind1950_lbl 358 `"Misc machinery"', add
label define ind1950_lbl 367 `"Electrical machinery, equipment and supplies"', add
label define ind1950_lbl 376 `"Motor vehicles and motor vehicle equipment"', add
label define ind1950_lbl 377 `"Aircraft and parts"', add
label define ind1950_lbl 378 `"Ship and boat building and repairing"', add
label define ind1950_lbl 379 `"Railroad and misc transportation equipment"', add
label define ind1950_lbl 386 `"Professional equipment"', add
label define ind1950_lbl 387 `"Photographic equipment and supplies"', add
label define ind1950_lbl 388 `"Watches, clocks, and clockwork-operated devices"', add
label define ind1950_lbl 399 `"Misc manufacturing industries"', add
label define ind1950_lbl 406 `"Meat products"', add
label define ind1950_lbl 407 `"Dairy products"', add
label define ind1950_lbl 408 `"Canning and preserving fruits, vegetables, and seafoods"', add
label define ind1950_lbl 409 `"Grain-mill products"', add
label define ind1950_lbl 416 `"Bakery products"', add
label define ind1950_lbl 417 `"Confectionery and related products"', add
label define ind1950_lbl 418 `"Beverage industries"', add
label define ind1950_lbl 419 `"Misc food preparations and kindred products"', add
label define ind1950_lbl 426 `"Not specified food industries"', add
label define ind1950_lbl 429 `"Tobacco manufactures"', add
label define ind1950_lbl 436 `"Knitting mills"', add
label define ind1950_lbl 437 `"Dyeing and finishing textiles, except knit goods"', add
label define ind1950_lbl 438 `"Carpets, rugs, and other floor coverings"', add
label define ind1950_lbl 439 `"Yarn, thread, and fabric"', add
label define ind1950_lbl 446 `"Misc textile mill products"', add
label define ind1950_lbl 448 `"Apparel and accessories"', add
label define ind1950_lbl 449 `"Misc fabricated textile products"', add
label define ind1950_lbl 456 `"Pulp, paper, and paper-board mills"', add
label define ind1950_lbl 457 `"Paperboard containers and boxes"', add
label define ind1950_lbl 458 `"Misc paper and pulp products"', add
label define ind1950_lbl 459 `"Printing, publishing, and allied industries"', add
label define ind1950_lbl 466 `"Synthetic fibers"', add
label define ind1950_lbl 467 `"Drugs and medicines"', add
label define ind1950_lbl 468 `"Paints, varnishes, and related products"', add
label define ind1950_lbl 469 `"Misc chemicals and allied products"', add
label define ind1950_lbl 476 `"Petroleum refining"', add
label define ind1950_lbl 477 `"Misc petroleum and coal products"', add
label define ind1950_lbl 478 `"Rubber products"', add
label define ind1950_lbl 487 `"Leather: tanned, curried, and finished"', add
label define ind1950_lbl 488 `"Footwear, except rubber"', add
label define ind1950_lbl 489 `"Leather products, except footwear"', add
label define ind1950_lbl 499 `"Not specified manufacturing industries"', add
label define ind1950_lbl 506 `"Railroads and railway"', add
label define ind1950_lbl 516 `"Street railways and bus lines"', add
label define ind1950_lbl 526 `"Trucking service"', add
label define ind1950_lbl 527 `"Warehousing and storage"', add
label define ind1950_lbl 536 `"Taxicab service"', add
label define ind1950_lbl 546 `"Water transportation"', add
label define ind1950_lbl 556 `"Air transportation"', add
label define ind1950_lbl 567 `"Petroleum and gasoline pipe lines"', add
label define ind1950_lbl 568 `"Services incidental to transportation"', add
label define ind1950_lbl 578 `"Telephone"', add
label define ind1950_lbl 579 `"Telegraph"', add
label define ind1950_lbl 586 `"Electric light and power"', add
label define ind1950_lbl 587 `"Gas and steam supply systems"', add
label define ind1950_lbl 588 `"Electric-gas utilities"', add
label define ind1950_lbl 596 `"Water supply"', add
label define ind1950_lbl 597 `"Sanitary services"', add
label define ind1950_lbl 598 `"Other and not specified utilities"', add
label define ind1950_lbl 606 `"Motor vehicles and equipment"', add
label define ind1950_lbl 607 `"Drugs, chemicals, and allied products"', add
label define ind1950_lbl 608 `"Dry goods apparel"', add
label define ind1950_lbl 609 `"Food and related products"', add
label define ind1950_lbl 616 `"Electrical goods, hardware, and plumbing equipment"', add
label define ind1950_lbl 617 `"Machinery, equipment, and supplies"', add
label define ind1950_lbl 618 `"Petroleum products"', add
label define ind1950_lbl 619 `"Farm prods--raw materials"', add
label define ind1950_lbl 626 `"Misc wholesale trade"', add
label define ind1950_lbl 627 `"Not specified wholesale trade"', add
label define ind1950_lbl 636 `"Food stores, except dairy"', add
label define ind1950_lbl 637 `"Dairy prods stores and milk retailing"', add
label define ind1950_lbl 646 `"General merchandise"', add
label define ind1950_lbl 647 `"Five and ten cent stores"', add
label define ind1950_lbl 656 `"Apparel and accessories stores, except shoe"', add
label define ind1950_lbl 657 `"Shoe stores"', add
label define ind1950_lbl 658 `"Furniture and house furnishings stores"', add
label define ind1950_lbl 659 `"Household appliance and radio stores"', add
label define ind1950_lbl 667 `"Motor vehicles and accessories retailing"', add
label define ind1950_lbl 668 `"Gasoline service stations"', add
label define ind1950_lbl 669 `"Drug stores"', add
label define ind1950_lbl 679 `"Eating and drinking  places"', add
label define ind1950_lbl 686 `"Hardware and farm implement stores"', add
label define ind1950_lbl 687 `"Lumber and building material retailing"', add
label define ind1950_lbl 688 `"Liquor stores"', add
label define ind1950_lbl 689 `"Retail florists"', add
label define ind1950_lbl 696 `"Jewelry stores"', add
label define ind1950_lbl 697 `"Fuel and ice retailing"', add
label define ind1950_lbl 698 `"Misc retail stores"', add
label define ind1950_lbl 699 `"Not specified retail trade"', add
label define ind1950_lbl 716 `"Banking and credit"', add
label define ind1950_lbl 726 `"Security and commodity brokerage and invest companies"', add
label define ind1950_lbl 736 `"Insurance"', add
label define ind1950_lbl 746 `"Real estate"', add
label define ind1950_lbl 756 `"Real estate-insurance-law  offices"', add
label define ind1950_lbl 806 `"Advertising"', add
label define ind1950_lbl 807 `"Accounting, auditing, and bookkeeping services"', add
label define ind1950_lbl 808 `"Misc business services"', add
label define ind1950_lbl 816 `"Auto repair services and garages"', add
label define ind1950_lbl 817 `"Misc repair services"', add
label define ind1950_lbl 826 `"Private households"', add
label define ind1950_lbl 836 `"Hotels and lodging places"', add
label define ind1950_lbl 846 `"Laundering, cleaning, and dyeing"', add
label define ind1950_lbl 847 `"Dressmaking shops"', add
label define ind1950_lbl 848 `"Shoe repair shops"', add
label define ind1950_lbl 849 `"Misc personal services"', add
label define ind1950_lbl 856 `"Radio broadcasting and television"', add
label define ind1950_lbl 857 `"Theaters and motion pictures"', add
label define ind1950_lbl 858 `"Bowling alleys, and billiard and pool parlors"', add
label define ind1950_lbl 859 `"Misc entertainment and recreation services"', add
label define ind1950_lbl 868 `"Medical and other health services, except hospitals"', add
label define ind1950_lbl 869 `"Hospitals"', add
label define ind1950_lbl 879 `"Legal services"', add
label define ind1950_lbl 888 `"Educational services"', add
label define ind1950_lbl 896 `"Welfare and religious services"', add
label define ind1950_lbl 897 `"Nonprofit membership organizs."', add
label define ind1950_lbl 898 `"Engineering and architectural services"', add
label define ind1950_lbl 899 `"Misc professional and related"', add
label define ind1950_lbl 906 `"Postal service"', add
label define ind1950_lbl 916 `"Federal public administration"', add
label define ind1950_lbl 926 `"State public administration"', add
label define ind1950_lbl 936 `"Local public administration"', add
label define ind1950_lbl 946 `"Public Administration, level not specified"', add
label define ind1950_lbl 976 `"Common or general laborer"', add
label define ind1950_lbl 979 `"Not yet specified"', add
label define ind1950_lbl 980 `"Unpaid domestic work"', add
label define ind1950_lbl 982 `"Housework at home"', add
label define ind1950_lbl 983 `"School response (students, etc.)"', add
label define ind1950_lbl 984 `"Retired"', add
label define ind1950_lbl 986 `"Sick/disabled"', add
label define ind1950_lbl 987 `"Institution response"', add
label define ind1950_lbl 991 `"Lady/Man of leisure"', add
label define ind1950_lbl 995 `"Non-industrial response"', add
label define ind1950_lbl 997 `"Nonclassifiable"', add
label define ind1950_lbl 998 `"Industry not reported"', add
label define ind1950_lbl 999 `"Blank or blank equivalent"', add
label values ind1950 ind1950_lbl

label define occscore_lbl 00 `"00"'
label define occscore_lbl 03 `"03"', add
label define occscore_lbl 04 `"04"', add
label define occscore_lbl 05 `"05"', add
label define occscore_lbl 06 `"06"', add
label define occscore_lbl 07 `"07"', add
label define occscore_lbl 08 `"08"', add
label define occscore_lbl 09 `"09"', add
label define occscore_lbl 10 `"10"', add
label define occscore_lbl 11 `"11"', add
label define occscore_lbl 12 `"12"', add
label define occscore_lbl 13 `"13"', add
label define occscore_lbl 14 `"14"', add
label define occscore_lbl 15 `"15"', add
label define occscore_lbl 16 `"16"', add
label define occscore_lbl 17 `"17"', add
label define occscore_lbl 18 `"18"', add
label define occscore_lbl 19 `"19"', add
label define occscore_lbl 20 `"20"', add
label define occscore_lbl 21 `"21"', add
label define occscore_lbl 22 `"22"', add
label define occscore_lbl 23 `"23"', add
label define occscore_lbl 24 `"24"', add
label define occscore_lbl 25 `"25"', add
label define occscore_lbl 26 `"26"', add
label define occscore_lbl 27 `"27"', add
label define occscore_lbl 28 `"28"', add
label define occscore_lbl 29 `"29"', add
label define occscore_lbl 30 `"30"', add
label define occscore_lbl 31 `"31"', add
label define occscore_lbl 32 `"32"', add
label define occscore_lbl 33 `"33"', add
label define occscore_lbl 34 `"34"', add
label define occscore_lbl 35 `"35"', add
label define occscore_lbl 36 `"36"', add
label define occscore_lbl 37 `"37"', add
label define occscore_lbl 38 `"38"', add
label define occscore_lbl 39 `"39"', add
label define occscore_lbl 40 `"40"', add
label define occscore_lbl 41 `"41"', add
label define occscore_lbl 42 `"42"', add
label define occscore_lbl 43 `"43"', add
label define occscore_lbl 44 `"44"', add
label define occscore_lbl 45 `"45"', add
label define occscore_lbl 46 `"46"', add
label define occscore_lbl 47 `"47"', add
label define occscore_lbl 48 `"48"', add
label define occscore_lbl 49 `"49"', add
label define occscore_lbl 50 `"50"', add
label define occscore_lbl 52 `"52"', add
label define occscore_lbl 54 `"54"', add
label define occscore_lbl 58 `"58"', add
label define occscore_lbl 60 `"60"', add
label define occscore_lbl 61 `"61"', add
label define occscore_lbl 62 `"62"', add
label define occscore_lbl 63 `"63"', add
label define occscore_lbl 79 `"79"', add
label define occscore_lbl 80 `"80"', add
label values occscore occscore_lbl

label define sei_lbl 78 `"78"'
label define sei_lbl 60 `"60"', add
label define sei_lbl 79 `"79"', add
label define sei_lbl 90 `"90"', add
label define sei_lbl 67 `"67"', add
label define sei_lbl 52 `"52"', add
label define sei_lbl 76 `"76"', add
label define sei_lbl 75 `"75"', add
label define sei_lbl 84 `"84"', add
label define sei_lbl 45 `"45"', add
label define sei_lbl 96 `"96"', add
label define sei_lbl 73 `"73"', add
label define sei_lbl 39 `"39"', add
label define sei_lbl 82 `"82"', add
label define sei_lbl 87 `"87"', add
label define sei_lbl 86 `"86"', add
label define sei_lbl 85 `"85"', add
label define sei_lbl 31 `"31"', add
label define sei_lbl 83 `"83"', add
label define sei_lbl 48 `"48"', add
label define sei_lbl 59 `"59"', add
label define sei_lbl 93 `"93"', add
label define sei_lbl 46 `"46"', add
label define sei_lbl 51 `"51"', add
label define sei_lbl 80 `"80"', add
label define sei_lbl 50 `"50"', add
label define sei_lbl 92 `"92"', add
label define sei_lbl 69 `"69"', add
label define sei_lbl 56 `"56"', add
label define sei_lbl 64 `"64"', add
label define sei_lbl 81 `"81"', add
label define sei_lbl 72 `"72"', add
label define sei_lbl 53 `"53"', add
label define sei_lbl 62 `"62"', add
label define sei_lbl 58 `"58"', add
label define sei_lbl 65 `"65"', add
label define sei_lbl 14 `"14"', add
label define sei_lbl 36 `"36"', add
label define sei_lbl 33 `"33"', add
label define sei_lbl 74 `"74"', add
label define sei_lbl 63 `"63"', add
label define sei_lbl 32 `"32"', add
label define sei_lbl 54 `"54"', add
label define sei_lbl 66 `"66"', add
label define sei_lbl 77 `"77"', add
label define sei_lbl 68 `"68"', add
label define sei_lbl 44 `"44"', add
label define sei_lbl 38 `"38"', add
label define sei_lbl 25 `"25"', add
label define sei_lbl 40 `"40"', add
label define sei_lbl 28 `"28"', add
label define sei_lbl 22 `"22"', add
label define sei_lbl 61 `"61"', add
label define sei_lbl 47 `"47"', add
label define sei_lbl 35 `"35"', add
label define sei_lbl 08 `"8"', add
label define sei_lbl 27 `"27"', add
label define sei_lbl 16 `"16"', add
label define sei_lbl 23 `"23"', add
label define sei_lbl 19 `"19"', add
label define sei_lbl 21 `"21"', add
label define sei_lbl 55 `"55"', add
label define sei_lbl 24 `"24"', add
label define sei_lbl 49 `"49"', add
label define sei_lbl 26 `"26"', add
label define sei_lbl 41 `"41"', add
label define sei_lbl 10 `"10"', add
label define sei_lbl 12 `"12"', add
label define sei_lbl 43 `"43"', add
label define sei_lbl 34 `"34"', add
label define sei_lbl 15 `"15"', add
label define sei_lbl 18 `"18"', add
label define sei_lbl 37 `"37"', add
label define sei_lbl 29 `"29"', add
label define sei_lbl 11 `"11"', add
label define sei_lbl 42 `"42"', add
label define sei_lbl 30 `"30"', add
label define sei_lbl 03 `"3"', add
label define sei_lbl 05 `"5"', add
label define sei_lbl 17 `"17"', add
label define sei_lbl 06 `"6"', add
label define sei_lbl 07 `"7"', add
label define sei_lbl 13 `"13"', add
label define sei_lbl 09 `"9"', add
label define sei_lbl 04 `"4"', add
label define sei_lbl 20 `"20"', add
label define sei_lbl 00 `"0"', add
label values sei sei_lbl

label define presgl_lbl 000 `"N/A"'
label define presgl_lbl 093 `"Bootblacks"', add
label define presgl_lbl 122 `"Teamsters"', add
label define presgl_lbl 124 `"Charwomen and cleaners"', add
label define presgl_lbl 141 `"Attendants, professional and personal service (n.e.c.)"', add
label define presgl_lbl 147 `"Attendants, recreation and amusement"', add
label define presgl_lbl 149 `"Ushers, recreation and amusement"', add
label define presgl_lbl 153 `"Counter and fountain workers"', add
label define presgl_lbl 154 `"Newsboys"', add
label define presgl_lbl 161 `"Janitors and sextons"', add
label define presgl_lbl 163 `"Garage laborers and car washers and greasers"', add
label define presgl_lbl 175 `"Laborers (n.e.c.)"', add
label define presgl_lbl 176 `"Laundressses, private household"', add
label define presgl_lbl 182 `"Laundry and dry cleaning operatives"', add
label define presgl_lbl 183 `"Hucksters and peddlers"', add
label define presgl_lbl 184 `"Farm laborers, wage workers"', add
label define presgl_lbl 187 `"Filers, grinders, and polishers, metal"', add
label define presgl_lbl 189 `"Private household workers (n.e.c.)"', add
label define presgl_lbl 191 `"Messengers and office boys"', add
label define presgl_lbl 199 `"Bartenders"', add
label define presgl_lbl 202 `"Porters"', add
label define presgl_lbl 203 `"Waiters and waitresses"', add
label define presgl_lbl 209 `"Elevator operators"', add
label define presgl_lbl 215 `"Fruit, nut, and vegetable graders, and packers, except factory"', add
label define presgl_lbl 216 `"Attendants, auto service and parking"', add
label define presgl_lbl 219 `"Guards, watchmen, and doorkeepers"', add
label define presgl_lbl 220 `"Taxicab drivers and chauffers"', add
label define presgl_lbl 221 `"Boarding and lodging house keepers"', add
label define presgl_lbl 225 `"Gardeners, except farm, and groundskeepers"', add
label define presgl_lbl 232 `"Baggagemen, transportation"', add
label define presgl_lbl 233 `"Midwives"', add
label define presgl_lbl 235 `"Watchmen (crossing) and bridge tenders"', add
label define presgl_lbl 242 `"Oilers and greaser, except auto"', add
label define presgl_lbl 243 `"Paperhangers"', add
label define presgl_lbl 244 `"Longshoremen and stevedores"', add
label define presgl_lbl 249 `"Spinners, textile"', add
label define presgl_lbl 250 `"Dyers"', add
label define presgl_lbl 252 `"Millers, grain, flour, feed, etc."', add
label define presgl_lbl 255 `"Glaziers"', add
label define presgl_lbl 259 `"Collectors, bill and account"', add
label define presgl_lbl 263 `"Mine operatives and laborers"', add
label define presgl_lbl 264 `"Cooks, except private household"', add
label define presgl_lbl 268 `"Farm service laborers, self-employed"', add
label define presgl_lbl 272 `"Motormen, mine, factory, logging camp, etc."', add
label define presgl_lbl 274 `"Floormen and floor managers, store"', add
label define presgl_lbl 277 `"Sawyers"', add
label define presgl_lbl 280 `"Conductors, bus and street railway"', add
label define presgl_lbl 283 `"Demonstrators"', add
label define presgl_lbl 284 `"Asbestos and insulation workers"', add
label define presgl_lbl 290 `"Painters, except construction or maintenance"', add
label define presgl_lbl 292 `"Shipping and receiving clerks"', add
label define presgl_lbl 298 `"Telegraph messengers"', add
label define presgl_lbl 302 `"Fishermen and oystermen"', add
label define presgl_lbl 303 `"Upholsterers"', add
label define presgl_lbl 304 `"Loom fixers"', add
label define presgl_lbl 307 `"Boilermakers"', add
label define presgl_lbl 309 `"Cashiers"', add
label define presgl_lbl 312 `"Roofers and slaters"', add
label define presgl_lbl 313 `"Bookbinders"', add
label define presgl_lbl 316 `"Cement and concrete finishers"', add
label define presgl_lbl 317 `"Dressmakers and seamstresses, except factory"', add
label define presgl_lbl 319 `"Auctioneers"', add
label define presgl_lbl 320 `"Piano and organ tuners and repairmen"', add
label define presgl_lbl 321 `"Blasters and powdermen"', add
label define presgl_lbl 324 `"Bus drivers"', add
label define presgl_lbl 325 `"Stationary firemen"', add
label define presgl_lbl 326 `"Excavating, grading, and road machinery operators"', add
label define presgl_lbl 328 `"Salesmen and sales clerks (n.e.c.)"', add
label define presgl_lbl 329 `"Furnacemen, smeltermen and pourers"', add
label define presgl_lbl 332 `"Plasterers"', add
label define presgl_lbl 334 `"Milliners"', add
label define presgl_lbl 335 `"Dispatchers and starters, vehicle"', add
label define presgl_lbl 337 `"Sailors and deck hands"', add
label define presgl_lbl 339 `"Mechanics and repairmen, office machine"', add
label define presgl_lbl 342 `"Entertainers (n.e.c.)"', add
label define presgl_lbl 347 `"Furriers"', add
label define presgl_lbl 350 `"Mechanics and repairmen, radio and television"', add
label define presgl_lbl 354 `"Ticket, station, and express agents"', add
label define presgl_lbl 355 `"Clerical and kindred workers (n.e.c.)"', add
label define presgl_lbl 357 `"Brickmasons, stonemasons, and tile setters"', add
label define presgl_lbl 359 `"Photographic process workers"', add
label define presgl_lbl 360 `"Rollers and roll hands, metal"', add
label define presgl_lbl 362 `"Locomotive firemen"', add
label define presgl_lbl 363 `"Attendants, hospital and other institution"', add
label define presgl_lbl 364 `"Housekeepers and stewards, except private household"', add
label define presgl_lbl 367 `"Therapists and healers (n.e.c.)"', add
label define presgl_lbl 368 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define presgl_lbl 372 `"Mechanics and repairmen, railroad and car shop"', add
label define presgl_lbl 373 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define presgl_lbl 374 `"Decorators and window dressers"', add
label define presgl_lbl 376 `"Dancers and dancing teachers"', add
label define presgl_lbl 380 `"Compositors and typesetters"', add
label define presgl_lbl 383 `"Managers and superintendents, building"', add
label define presgl_lbl 386 `"Cabinetmakers"', add
label define presgl_lbl 388 `"Cranemen, derrickmen, and hoistmen"', add
label define presgl_lbl 391 `"Molders, metal"', add
label define presgl_lbl 392 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define presgl_lbl 394 `"Chainmen, rodmen, and axmen, surveying"', add
label define presgl_lbl 399 `"Carpenters"', add
label define presgl_lbl 401 `"Welders and flame cutters"', add
label define presgl_lbl 402 `"Pressmen and plate printers, printing"', add
label define presgl_lbl 403 `"Millwrights"', add
label define presgl_lbl 404 `"Telephone operators"', add
label define presgl_lbl 405 `"Photographers"', add
label define presgl_lbl 406 `"Inspectors, public administration"', add
label define presgl_lbl 407 `"Farmers (owners and tenants)"', add
label define presgl_lbl 408 `"Apprentice auto mechanics"', add
label define presgl_lbl 409 `"Buyers and shippers, farm products"', add
label define presgl_lbl 412 `"Engravers, except photoengravers"', add
label define presgl_lbl 413 `"Attendants and assistants, library"', add
label define presgl_lbl 419 `"Practical nurses"', add
label define presgl_lbl 420 `"Tool makers, and die makers and setters"', add
label define presgl_lbl 422 `"Advertising agents and salesmen"', add
label define presgl_lbl 423 `"Mail carriers"', add
label define presgl_lbl 425 `"Agents (n.e.c.)"', add
label define presgl_lbl 428 `"Radio operators"', add
label define presgl_lbl 435 `"Telegraph operators"', add
label define presgl_lbl 437 `"Farm managers"', add
label define presgl_lbl 438 `"Firemen, fire protection"', add
label define presgl_lbl 440 `"Real estate agents and brokers"', add
label define presgl_lbl 445 `"Stenographers, typists, and secretaries"', add
label define presgl_lbl 449 `"Office machine operators"', add
label define presgl_lbl 451 `"Nurses, student professional"', add
label define presgl_lbl 453 `"Foremen (n.e.c.)"', add
label define presgl_lbl 458 `"Marshals and constables"', add
label define presgl_lbl 460 `"Musicians and music teachers"', add
label define presgl_lbl 466 `"Craftsmen and kindred workers (n.e.c.)"', add
label define presgl_lbl 469 `"Insurance agents and brokers"', add
label define presgl_lbl 470 `"Technicians, testing"', add
label define presgl_lbl 476 `"Bookkeepers"', add
label define presgl_lbl 478 `"Attendants, physician's and dentist's office"', add
label define presgl_lbl 479 `"Purchasing agents and buyers (n.e.c.)"', add
label define presgl_lbl 482 `"Mechanics and repairmen, airplane"', add
label define presgl_lbl 483 `"Officials, lodge, society, union, etc."', add
label define presgl_lbl 486 `"Recreation and group workers"', add
label define presgl_lbl 488 `"Credit men"', add
label define presgl_lbl 492 `"Electricians"', add
label define presgl_lbl 495 `"Bank tellers"', add
label define presgl_lbl 500 `"Buyers and department heads, store"', add
label define presgl_lbl 502 `"Technicians (n.e.c.)"', add
label define presgl_lbl 503 `"Managers, officials, and proprietors (n.e.c.)"', add
label define presgl_lbl 506 `"Professional, technical and kindred workers (n.e.c.)"', add
label define presgl_lbl 508 `"Locomotive engineers"', add
label define presgl_lbl 514 `"Athletes"', add
label define presgl_lbl 521 `"Dieticians and nutritionists"', add
label define presgl_lbl 522 `"Funeral directors and embalmers"', add
label define presgl_lbl 524 `"Social and welfare workers, except group"', add
label define presgl_lbl 525 `"Editors and reporters"', add
label define presgl_lbl 532 `"Sports instructors and officials"', add
label define presgl_lbl 533 `"Surveyors"', add
label define presgl_lbl 539 `"Farm and home management advisors"', add
label define presgl_lbl 544 `"Engineers, industrial"', add
label define presgl_lbl 546 `"Librarians"', add
label define presgl_lbl 550 `"Actors and actresses"', add
label define presgl_lbl 554 `"Statisticians and actuaries"', add
label define presgl_lbl 555 `"Religious workers"', add
label define presgl_lbl 558 `"Engineers, metallurgical, metallurgists"', add
label define presgl_lbl 560 `"Personnel and labor relations workers"', add
label define presgl_lbl 561 `"Draftsmen"', add
label define presgl_lbl 562 `"Artists and art teachers"', add
label define presgl_lbl 567 `"Accountants and auditors"', add
label define presgl_lbl 568 `"Economists"', add
label define presgl_lbl 581 `"Postmasters"', add
label define presgl_lbl 582 `"Designers"', add
label define presgl_lbl 596 `"Teachers (n.e.c.)"', add
label define presgl_lbl 597 `"Veterinarians"', add
label define presgl_lbl 598 `"Authors"', add
label define presgl_lbl 599 `"Officers, pilots, pursers and engineers, ship"', add
label define presgl_lbl 600 `"Chiropractors"', add
label define presgl_lbl 604 `"Engineers (n.e.c.)"', add
label define presgl_lbl 606 `"Officials and administrators (n.e.c.), public administration"', add
label define presgl_lbl 607 `"Pharmacists"', add
label define presgl_lbl 610 `"Technicians, medical and dental"', add
label define presgl_lbl 615 `"Nurses, professional"', add
label define presgl_lbl 616 `"Engineers, mining"', add
label define presgl_lbl 619 `"Osteopaths"', add
label define presgl_lbl 620 `"Optometrists"', add
label define presgl_lbl 623 `"Engineers, mechanical"', add
label define presgl_lbl 650 `"Mathematicians"', add
label define presgl_lbl 656 `"Miscellaneous social scientists"', add
label define presgl_lbl 672 `"Geologists and geophysicists"', add
label define presgl_lbl 673 `"Engineers, chemical"', add
label define presgl_lbl 677 `"Biological scientists"', add
label define presgl_lbl 678 `"Engineers, civil"', add
label define presgl_lbl 681 `"Miscellaneous natural scientists"', add
label define presgl_lbl 688 `"Chemists"', add
label define presgl_lbl 690 `"Clergymen"', add
label define presgl_lbl 694 `"Engineers, electrical"', add
label define presgl_lbl 701 `"Airplane pilots and navigators"', add
label define presgl_lbl 705 `"Architects"', add
label define presgl_lbl 711 `"Engineers, aeronautical"', add
label define presgl_lbl 714 `"Psychologists"', add
label define presgl_lbl 736 `"Dentists"', add
label define presgl_lbl 738 `"Physicists"', add
label define presgl_lbl 757 `"Lawyers and judges"', add
label define presgl_lbl 783 `"College presidents and deans"', add
label define presgl_lbl 815 `"Physicians and surgeons"', add
label values presgl presgl_lbl

label define erscor50_lbl 0000 `"0"'
label define erscor50_lbl 0001 `"0.1"', add
label define erscor50_lbl 0002 `"0.2"', add
label define erscor50_lbl 0003 `"0.3"', add
label define erscor50_lbl 0004 `"0.4"', add
label define erscor50_lbl 0005 `"0.5"', add
label define erscor50_lbl 0006 `"0.6"', add
label define erscor50_lbl 0007 `"0.7"', add
label define erscor50_lbl 0008 `"0.8"', add
label define erscor50_lbl 0009 `"0.9"', add
label define erscor50_lbl 0010 `"1"', add
label define erscor50_lbl 0011 `"1.1"', add
label define erscor50_lbl 0012 `"1.2"', add
label define erscor50_lbl 0013 `"1.3"', add
label define erscor50_lbl 0014 `"1.4"', add
label define erscor50_lbl 0015 `"1.5"', add
label define erscor50_lbl 0016 `"1.6"', add
label define erscor50_lbl 0017 `"1.7"', add
label define erscor50_lbl 0018 `"1.8"', add
label define erscor50_lbl 0019 `"1.9"', add
label define erscor50_lbl 0020 `"2"', add
label define erscor50_lbl 0021 `"2.1"', add
label define erscor50_lbl 0022 `"2.2"', add
label define erscor50_lbl 0023 `"2.3"', add
label define erscor50_lbl 0024 `"2.4"', add
label define erscor50_lbl 0025 `"2.5"', add
label define erscor50_lbl 0026 `"2.6"', add
label define erscor50_lbl 0027 `"2.7"', add
label define erscor50_lbl 0028 `"2.8"', add
label define erscor50_lbl 0029 `"2.9"', add
label define erscor50_lbl 0030 `"3"', add
label define erscor50_lbl 0031 `"3.1"', add
label define erscor50_lbl 0032 `"3.2"', add
label define erscor50_lbl 0033 `"3.3"', add
label define erscor50_lbl 0034 `"3.4"', add
label define erscor50_lbl 0035 `"3.5"', add
label define erscor50_lbl 0036 `"3.6"', add
label define erscor50_lbl 0037 `"3.7"', add
label define erscor50_lbl 0038 `"3.8"', add
label define erscor50_lbl 0039 `"3.9"', add
label define erscor50_lbl 0040 `"4"', add
label define erscor50_lbl 0041 `"4.1"', add
label define erscor50_lbl 0042 `"4.2"', add
label define erscor50_lbl 0043 `"4.3"', add
label define erscor50_lbl 0044 `"4.4"', add
label define erscor50_lbl 0045 `"4.5"', add
label define erscor50_lbl 0046 `"4.6"', add
label define erscor50_lbl 0047 `"4.7"', add
label define erscor50_lbl 0048 `"4.8"', add
label define erscor50_lbl 0049 `"4.9"', add
label define erscor50_lbl 0050 `"5"', add
label define erscor50_lbl 0051 `"5.1"', add
label define erscor50_lbl 0052 `"5.2"', add
label define erscor50_lbl 0053 `"5.3"', add
label define erscor50_lbl 0054 `"5.4"', add
label define erscor50_lbl 0055 `"5.5"', add
label define erscor50_lbl 0056 `"5.6"', add
label define erscor50_lbl 0057 `"5.7"', add
label define erscor50_lbl 0058 `"5.8"', add
label define erscor50_lbl 0059 `"5.9"', add
label define erscor50_lbl 0060 `"6"', add
label define erscor50_lbl 0061 `"6.1"', add
label define erscor50_lbl 0062 `"6.2"', add
label define erscor50_lbl 0063 `"6.3"', add
label define erscor50_lbl 0064 `"6.4"', add
label define erscor50_lbl 0065 `"6.5"', add
label define erscor50_lbl 0066 `"6.6"', add
label define erscor50_lbl 0067 `"6.7"', add
label define erscor50_lbl 0068 `"6.8"', add
label define erscor50_lbl 0069 `"6.9"', add
label define erscor50_lbl 0070 `"7"', add
label define erscor50_lbl 0071 `"7.1"', add
label define erscor50_lbl 0072 `"7.2"', add
label define erscor50_lbl 0073 `"7.3"', add
label define erscor50_lbl 0074 `"7.4"', add
label define erscor50_lbl 0075 `"7.5"', add
label define erscor50_lbl 0076 `"7.6"', add
label define erscor50_lbl 0077 `"7.7"', add
label define erscor50_lbl 0078 `"7.8"', add
label define erscor50_lbl 0079 `"7.9"', add
label define erscor50_lbl 0080 `"8"', add
label define erscor50_lbl 0081 `"8.1"', add
label define erscor50_lbl 0082 `"8.2"', add
label define erscor50_lbl 0083 `"8.3"', add
label define erscor50_lbl 0084 `"8.4"', add
label define erscor50_lbl 0085 `"8.5"', add
label define erscor50_lbl 0086 `"8.6"', add
label define erscor50_lbl 0087 `"8.7"', add
label define erscor50_lbl 0088 `"8.8"', add
label define erscor50_lbl 0089 `"8.9"', add
label define erscor50_lbl 0090 `"9"', add
label define erscor50_lbl 0091 `"9.1"', add
label define erscor50_lbl 0092 `"9.2"', add
label define erscor50_lbl 0093 `"9.3"', add
label define erscor50_lbl 0094 `"9.4"', add
label define erscor50_lbl 0095 `"9.5"', add
label define erscor50_lbl 0096 `"9.6"', add
label define erscor50_lbl 0097 `"9.7"', add
label define erscor50_lbl 0098 `"9.8"', add
label define erscor50_lbl 0099 `"9.9"', add
label define erscor50_lbl 0100 `"10"', add
label define erscor50_lbl 0101 `"10.1"', add
label define erscor50_lbl 0102 `"10.2"', add
label define erscor50_lbl 0103 `"10.3"', add
label define erscor50_lbl 0104 `"10.4"', add
label define erscor50_lbl 0105 `"10.5"', add
label define erscor50_lbl 0106 `"10.6"', add
label define erscor50_lbl 0107 `"10.7"', add
label define erscor50_lbl 0108 `"10.8"', add
label define erscor50_lbl 0109 `"10.9"', add
label define erscor50_lbl 0110 `"11"', add
label define erscor50_lbl 0111 `"11.1"', add
label define erscor50_lbl 0112 `"11.2"', add
label define erscor50_lbl 0113 `"11.3"', add
label define erscor50_lbl 0114 `"11.4"', add
label define erscor50_lbl 0115 `"11.5"', add
label define erscor50_lbl 0116 `"11.6"', add
label define erscor50_lbl 0117 `"11.7"', add
label define erscor50_lbl 0118 `"11.8"', add
label define erscor50_lbl 0119 `"11.9"', add
label define erscor50_lbl 0120 `"12"', add
label define erscor50_lbl 0121 `"12.1"', add
label define erscor50_lbl 0122 `"12.2"', add
label define erscor50_lbl 0123 `"12.3"', add
label define erscor50_lbl 0124 `"12.4"', add
label define erscor50_lbl 0125 `"12.5"', add
label define erscor50_lbl 0126 `"12.6"', add
label define erscor50_lbl 0127 `"12.7"', add
label define erscor50_lbl 0128 `"12.8"', add
label define erscor50_lbl 0129 `"12.9"', add
label define erscor50_lbl 0130 `"13"', add
label define erscor50_lbl 0131 `"13.1"', add
label define erscor50_lbl 0132 `"13.2"', add
label define erscor50_lbl 0133 `"13.3"', add
label define erscor50_lbl 0134 `"13.4"', add
label define erscor50_lbl 0135 `"13.5"', add
label define erscor50_lbl 0136 `"13.6"', add
label define erscor50_lbl 0137 `"13.7"', add
label define erscor50_lbl 0138 `"13.8"', add
label define erscor50_lbl 0139 `"13.9"', add
label define erscor50_lbl 0140 `"14"', add
label define erscor50_lbl 0141 `"14.1"', add
label define erscor50_lbl 0142 `"14.2"', add
label define erscor50_lbl 0143 `"14.3"', add
label define erscor50_lbl 0144 `"14.4"', add
label define erscor50_lbl 0145 `"14.5"', add
label define erscor50_lbl 0146 `"14.6"', add
label define erscor50_lbl 0147 `"14.7"', add
label define erscor50_lbl 0148 `"14.8"', add
label define erscor50_lbl 0149 `"14.9"', add
label define erscor50_lbl 0150 `"15"', add
label define erscor50_lbl 0151 `"15.1"', add
label define erscor50_lbl 0152 `"15.2"', add
label define erscor50_lbl 0153 `"15.3"', add
label define erscor50_lbl 0154 `"15.4"', add
label define erscor50_lbl 0155 `"15.5"', add
label define erscor50_lbl 0156 `"15.6"', add
label define erscor50_lbl 0157 `"15.7"', add
label define erscor50_lbl 0158 `"15.8"', add
label define erscor50_lbl 0159 `"15.9"', add
label define erscor50_lbl 0160 `"16"', add
label define erscor50_lbl 0161 `"16.1"', add
label define erscor50_lbl 0162 `"16.2"', add
label define erscor50_lbl 0163 `"16.3"', add
label define erscor50_lbl 0164 `"16.4"', add
label define erscor50_lbl 0165 `"16.5"', add
label define erscor50_lbl 0166 `"16.6"', add
label define erscor50_lbl 0167 `"16.7"', add
label define erscor50_lbl 0168 `"16.8"', add
label define erscor50_lbl 0169 `"16.9"', add
label define erscor50_lbl 0170 `"17"', add
label define erscor50_lbl 0171 `"17.1"', add
label define erscor50_lbl 0172 `"17.2"', add
label define erscor50_lbl 0173 `"17.3"', add
label define erscor50_lbl 0174 `"17.4"', add
label define erscor50_lbl 0175 `"17.5"', add
label define erscor50_lbl 0176 `"17.6"', add
label define erscor50_lbl 0177 `"17.7"', add
label define erscor50_lbl 0178 `"17.8"', add
label define erscor50_lbl 0179 `"17.9"', add
label define erscor50_lbl 0180 `"18"', add
label define erscor50_lbl 0181 `"18.1"', add
label define erscor50_lbl 0182 `"18.2"', add
label define erscor50_lbl 0183 `"18.3"', add
label define erscor50_lbl 0184 `"18.4"', add
label define erscor50_lbl 0185 `"18.5"', add
label define erscor50_lbl 0186 `"18.6"', add
label define erscor50_lbl 0187 `"18.7"', add
label define erscor50_lbl 0188 `"18.8"', add
label define erscor50_lbl 0189 `"18.9"', add
label define erscor50_lbl 0190 `"19"', add
label define erscor50_lbl 0191 `"19.1"', add
label define erscor50_lbl 0192 `"19.2"', add
label define erscor50_lbl 0193 `"19.3"', add
label define erscor50_lbl 0194 `"19.4"', add
label define erscor50_lbl 0195 `"19.5"', add
label define erscor50_lbl 0196 `"19.6"', add
label define erscor50_lbl 0197 `"19.7"', add
label define erscor50_lbl 0198 `"19.8"', add
label define erscor50_lbl 0199 `"19.9"', add
label define erscor50_lbl 0200 `"20"', add
label define erscor50_lbl 0201 `"20.1"', add
label define erscor50_lbl 0202 `"20.2"', add
label define erscor50_lbl 0203 `"20.3"', add
label define erscor50_lbl 0204 `"20.4"', add
label define erscor50_lbl 0205 `"20.5"', add
label define erscor50_lbl 0206 `"20.6"', add
label define erscor50_lbl 0207 `"20.7"', add
label define erscor50_lbl 0208 `"20.8"', add
label define erscor50_lbl 0209 `"20.9"', add
label define erscor50_lbl 0210 `"21"', add
label define erscor50_lbl 0211 `"21.1"', add
label define erscor50_lbl 0212 `"21.2"', add
label define erscor50_lbl 0213 `"21.3"', add
label define erscor50_lbl 0214 `"21.4"', add
label define erscor50_lbl 0215 `"21.5"', add
label define erscor50_lbl 0216 `"21.6"', add
label define erscor50_lbl 0217 `"21.7"', add
label define erscor50_lbl 0218 `"21.8"', add
label define erscor50_lbl 0219 `"21.9"', add
label define erscor50_lbl 0220 `"22"', add
label define erscor50_lbl 0221 `"22.1"', add
label define erscor50_lbl 0222 `"22.2"', add
label define erscor50_lbl 0223 `"22.3"', add
label define erscor50_lbl 0224 `"22.4"', add
label define erscor50_lbl 0225 `"22.5"', add
label define erscor50_lbl 0226 `"22.6"', add
label define erscor50_lbl 0227 `"22.7"', add
label define erscor50_lbl 0228 `"22.8"', add
label define erscor50_lbl 0229 `"22.9"', add
label define erscor50_lbl 0230 `"23"', add
label define erscor50_lbl 0231 `"23.1"', add
label define erscor50_lbl 0232 `"23.2"', add
label define erscor50_lbl 0233 `"23.3"', add
label define erscor50_lbl 0234 `"23.4"', add
label define erscor50_lbl 0235 `"23.5"', add
label define erscor50_lbl 0236 `"23.6"', add
label define erscor50_lbl 0237 `"23.7"', add
label define erscor50_lbl 0238 `"23.8"', add
label define erscor50_lbl 0239 `"23.9"', add
label define erscor50_lbl 0240 `"24"', add
label define erscor50_lbl 0241 `"24.1"', add
label define erscor50_lbl 0242 `"24.2"', add
label define erscor50_lbl 0243 `"24.3"', add
label define erscor50_lbl 0244 `"24.4"', add
label define erscor50_lbl 0245 `"24.5"', add
label define erscor50_lbl 0246 `"24.6"', add
label define erscor50_lbl 0247 `"24.7"', add
label define erscor50_lbl 0248 `"24.8"', add
label define erscor50_lbl 0249 `"24.9"', add
label define erscor50_lbl 0250 `"25"', add
label define erscor50_lbl 0251 `"25.1"', add
label define erscor50_lbl 0252 `"25.2"', add
label define erscor50_lbl 0253 `"25.3"', add
label define erscor50_lbl 0254 `"25.4"', add
label define erscor50_lbl 0255 `"25.5"', add
label define erscor50_lbl 0256 `"25.6"', add
label define erscor50_lbl 0257 `"25.7"', add
label define erscor50_lbl 0258 `"25.8"', add
label define erscor50_lbl 0259 `"25.9"', add
label define erscor50_lbl 0260 `"26"', add
label define erscor50_lbl 0261 `"26.1"', add
label define erscor50_lbl 0262 `"26.2"', add
label define erscor50_lbl 0263 `"26.3"', add
label define erscor50_lbl 0264 `"26.4"', add
label define erscor50_lbl 0265 `"26.5"', add
label define erscor50_lbl 0266 `"26.6"', add
label define erscor50_lbl 0267 `"26.7"', add
label define erscor50_lbl 0268 `"26.8"', add
label define erscor50_lbl 0269 `"26.9"', add
label define erscor50_lbl 0270 `"27"', add
label define erscor50_lbl 0271 `"27.1"', add
label define erscor50_lbl 0272 `"27.2"', add
label define erscor50_lbl 0273 `"27.3"', add
label define erscor50_lbl 0274 `"27.4"', add
label define erscor50_lbl 0275 `"27.5"', add
label define erscor50_lbl 0276 `"27.6"', add
label define erscor50_lbl 0277 `"27.7"', add
label define erscor50_lbl 0278 `"27.8"', add
label define erscor50_lbl 0279 `"27.9"', add
label define erscor50_lbl 0280 `"28"', add
label define erscor50_lbl 0281 `"28.1"', add
label define erscor50_lbl 0282 `"28.2"', add
label define erscor50_lbl 0283 `"28.3"', add
label define erscor50_lbl 0284 `"28.4"', add
label define erscor50_lbl 0285 `"28.5"', add
label define erscor50_lbl 0286 `"28.6"', add
label define erscor50_lbl 0287 `"28.7"', add
label define erscor50_lbl 0288 `"28.8"', add
label define erscor50_lbl 0289 `"28.9"', add
label define erscor50_lbl 0290 `"29"', add
label define erscor50_lbl 0291 `"29.1"', add
label define erscor50_lbl 0292 `"29.2"', add
label define erscor50_lbl 0293 `"29.3"', add
label define erscor50_lbl 0294 `"29.4"', add
label define erscor50_lbl 0295 `"29.5"', add
label define erscor50_lbl 0296 `"29.6"', add
label define erscor50_lbl 0297 `"29.7"', add
label define erscor50_lbl 0298 `"29.8"', add
label define erscor50_lbl 0299 `"29.9"', add
label define erscor50_lbl 0300 `"30"', add
label define erscor50_lbl 0301 `"30.1"', add
label define erscor50_lbl 0302 `"30.2"', add
label define erscor50_lbl 0303 `"30.3"', add
label define erscor50_lbl 0304 `"30.4"', add
label define erscor50_lbl 0305 `"30.5"', add
label define erscor50_lbl 0306 `"30.6"', add
label define erscor50_lbl 0307 `"30.7"', add
label define erscor50_lbl 0308 `"30.8"', add
label define erscor50_lbl 0309 `"30.9"', add
label define erscor50_lbl 0310 `"31"', add
label define erscor50_lbl 0311 `"31.1"', add
label define erscor50_lbl 0312 `"31.2"', add
label define erscor50_lbl 0313 `"31.3"', add
label define erscor50_lbl 0314 `"31.4"', add
label define erscor50_lbl 0315 `"31.5"', add
label define erscor50_lbl 0316 `"31.6"', add
label define erscor50_lbl 0317 `"31.7"', add
label define erscor50_lbl 0318 `"31.8"', add
label define erscor50_lbl 0319 `"31.9"', add
label define erscor50_lbl 0320 `"32"', add
label define erscor50_lbl 0321 `"32.1"', add
label define erscor50_lbl 0322 `"32.2"', add
label define erscor50_lbl 0323 `"32.3"', add
label define erscor50_lbl 0324 `"32.4"', add
label define erscor50_lbl 0325 `"32.5"', add
label define erscor50_lbl 0326 `"32.6"', add
label define erscor50_lbl 0327 `"32.7"', add
label define erscor50_lbl 0328 `"32.8"', add
label define erscor50_lbl 0329 `"32.9"', add
label define erscor50_lbl 0330 `"33"', add
label define erscor50_lbl 0331 `"33.1"', add
label define erscor50_lbl 0332 `"33.2"', add
label define erscor50_lbl 0333 `"33.3"', add
label define erscor50_lbl 0334 `"33.4"', add
label define erscor50_lbl 0335 `"33.5"', add
label define erscor50_lbl 0336 `"33.6"', add
label define erscor50_lbl 0337 `"33.7"', add
label define erscor50_lbl 0338 `"33.8"', add
label define erscor50_lbl 0339 `"33.9"', add
label define erscor50_lbl 0340 `"34"', add
label define erscor50_lbl 0341 `"34.1"', add
label define erscor50_lbl 0342 `"34.2"', add
label define erscor50_lbl 0343 `"34.3"', add
label define erscor50_lbl 0344 `"34.4"', add
label define erscor50_lbl 0345 `"34.5"', add
label define erscor50_lbl 0346 `"34.6"', add
label define erscor50_lbl 0347 `"34.7"', add
label define erscor50_lbl 0348 `"34.8"', add
label define erscor50_lbl 0349 `"34.9"', add
label define erscor50_lbl 0350 `"35"', add
label define erscor50_lbl 0351 `"35.1"', add
label define erscor50_lbl 0352 `"35.2"', add
label define erscor50_lbl 0353 `"35.3"', add
label define erscor50_lbl 0354 `"35.4"', add
label define erscor50_lbl 0355 `"35.5"', add
label define erscor50_lbl 0356 `"35.6"', add
label define erscor50_lbl 0357 `"35.7"', add
label define erscor50_lbl 0358 `"35.8"', add
label define erscor50_lbl 0359 `"35.9"', add
label define erscor50_lbl 0360 `"36"', add
label define erscor50_lbl 0361 `"36.1"', add
label define erscor50_lbl 0362 `"36.2"', add
label define erscor50_lbl 0363 `"36.3"', add
label define erscor50_lbl 0364 `"36.4"', add
label define erscor50_lbl 0365 `"36.5"', add
label define erscor50_lbl 0366 `"36.6"', add
label define erscor50_lbl 0367 `"36.7"', add
label define erscor50_lbl 0368 `"36.8"', add
label define erscor50_lbl 0369 `"36.9"', add
label define erscor50_lbl 0370 `"37"', add
label define erscor50_lbl 0371 `"37.1"', add
label define erscor50_lbl 0372 `"37.2"', add
label define erscor50_lbl 0373 `"37.3"', add
label define erscor50_lbl 0374 `"37.4"', add
label define erscor50_lbl 0375 `"37.5"', add
label define erscor50_lbl 0376 `"37.6"', add
label define erscor50_lbl 0377 `"37.7"', add
label define erscor50_lbl 0378 `"37.8"', add
label define erscor50_lbl 0379 `"37.9"', add
label define erscor50_lbl 0380 `"38"', add
label define erscor50_lbl 0381 `"38.1"', add
label define erscor50_lbl 0382 `"38.2"', add
label define erscor50_lbl 0383 `"38.3"', add
label define erscor50_lbl 0384 `"38.4"', add
label define erscor50_lbl 0385 `"38.5"', add
label define erscor50_lbl 0386 `"38.6"', add
label define erscor50_lbl 0387 `"38.7"', add
label define erscor50_lbl 0388 `"38.8"', add
label define erscor50_lbl 0389 `"38.9"', add
label define erscor50_lbl 0390 `"39"', add
label define erscor50_lbl 0391 `"39.1"', add
label define erscor50_lbl 0392 `"39.2"', add
label define erscor50_lbl 0393 `"39.3"', add
label define erscor50_lbl 0394 `"39.4"', add
label define erscor50_lbl 0395 `"39.5"', add
label define erscor50_lbl 0396 `"39.6"', add
label define erscor50_lbl 0397 `"39.7"', add
label define erscor50_lbl 0398 `"39.8"', add
label define erscor50_lbl 0399 `"39.9"', add
label define erscor50_lbl 0400 `"40"', add
label define erscor50_lbl 0401 `"40.1"', add
label define erscor50_lbl 0402 `"40.2"', add
label define erscor50_lbl 0403 `"40.3"', add
label define erscor50_lbl 0404 `"40.4"', add
label define erscor50_lbl 0405 `"40.5"', add
label define erscor50_lbl 0406 `"40.6"', add
label define erscor50_lbl 0407 `"40.7"', add
label define erscor50_lbl 0408 `"40.8"', add
label define erscor50_lbl 0409 `"40.9"', add
label define erscor50_lbl 0410 `"41"', add
label define erscor50_lbl 0411 `"41.1"', add
label define erscor50_lbl 0412 `"41.2"', add
label define erscor50_lbl 0413 `"41.3"', add
label define erscor50_lbl 0414 `"41.4"', add
label define erscor50_lbl 0415 `"41.5"', add
label define erscor50_lbl 0416 `"41.6"', add
label define erscor50_lbl 0417 `"41.7"', add
label define erscor50_lbl 0418 `"41.8"', add
label define erscor50_lbl 0419 `"41.9"', add
label define erscor50_lbl 0420 `"42"', add
label define erscor50_lbl 0421 `"42.1"', add
label define erscor50_lbl 0422 `"42.2"', add
label define erscor50_lbl 0423 `"42.3"', add
label define erscor50_lbl 0424 `"42.4"', add
label define erscor50_lbl 0425 `"42.5"', add
label define erscor50_lbl 0426 `"42.6"', add
label define erscor50_lbl 0427 `"42.7"', add
label define erscor50_lbl 0428 `"42.8"', add
label define erscor50_lbl 0429 `"42.9"', add
label define erscor50_lbl 0430 `"43"', add
label define erscor50_lbl 0431 `"43.1"', add
label define erscor50_lbl 0432 `"43.2"', add
label define erscor50_lbl 0433 `"43.3"', add
label define erscor50_lbl 0434 `"43.4"', add
label define erscor50_lbl 0435 `"43.5"', add
label define erscor50_lbl 0436 `"43.6"', add
label define erscor50_lbl 0437 `"43.7"', add
label define erscor50_lbl 0438 `"43.8"', add
label define erscor50_lbl 0439 `"43.9"', add
label define erscor50_lbl 0440 `"44"', add
label define erscor50_lbl 0441 `"44.1"', add
label define erscor50_lbl 0442 `"44.2"', add
label define erscor50_lbl 0443 `"44.3"', add
label define erscor50_lbl 0444 `"44.4"', add
label define erscor50_lbl 0445 `"44.5"', add
label define erscor50_lbl 0446 `"44.6"', add
label define erscor50_lbl 0447 `"44.7"', add
label define erscor50_lbl 0448 `"44.8"', add
label define erscor50_lbl 0449 `"44.9"', add
label define erscor50_lbl 0450 `"45"', add
label define erscor50_lbl 0451 `"45.1"', add
label define erscor50_lbl 0452 `"45.2"', add
label define erscor50_lbl 0453 `"45.3"', add
label define erscor50_lbl 0454 `"45.4"', add
label define erscor50_lbl 0455 `"45.5"', add
label define erscor50_lbl 0456 `"45.6"', add
label define erscor50_lbl 0457 `"45.7"', add
label define erscor50_lbl 0458 `"45.8"', add
label define erscor50_lbl 0459 `"45.9"', add
label define erscor50_lbl 0460 `"46"', add
label define erscor50_lbl 0461 `"46.1"', add
label define erscor50_lbl 0462 `"46.2"', add
label define erscor50_lbl 0463 `"46.3"', add
label define erscor50_lbl 0464 `"46.4"', add
label define erscor50_lbl 0465 `"46.5"', add
label define erscor50_lbl 0466 `"46.6"', add
label define erscor50_lbl 0467 `"46.7"', add
label define erscor50_lbl 0468 `"46.8"', add
label define erscor50_lbl 0469 `"46.9"', add
label define erscor50_lbl 0470 `"47"', add
label define erscor50_lbl 0471 `"47.1"', add
label define erscor50_lbl 0472 `"47.2"', add
label define erscor50_lbl 0473 `"47.3"', add
label define erscor50_lbl 0474 `"47.4"', add
label define erscor50_lbl 0475 `"47.5"', add
label define erscor50_lbl 0476 `"47.6"', add
label define erscor50_lbl 0477 `"47.7"', add
label define erscor50_lbl 0478 `"47.8"', add
label define erscor50_lbl 0479 `"47.9"', add
label define erscor50_lbl 0480 `"48"', add
label define erscor50_lbl 0481 `"48.1"', add
label define erscor50_lbl 0482 `"48.2"', add
label define erscor50_lbl 0483 `"48.3"', add
label define erscor50_lbl 0484 `"48.4"', add
label define erscor50_lbl 0485 `"48.5"', add
label define erscor50_lbl 0486 `"48.6"', add
label define erscor50_lbl 0487 `"48.7"', add
label define erscor50_lbl 0488 `"48.8"', add
label define erscor50_lbl 0489 `"48.9"', add
label define erscor50_lbl 0490 `"49"', add
label define erscor50_lbl 0491 `"49.1"', add
label define erscor50_lbl 0492 `"49.2"', add
label define erscor50_lbl 0493 `"49.3"', add
label define erscor50_lbl 0494 `"49.4"', add
label define erscor50_lbl 0495 `"49.5"', add
label define erscor50_lbl 0496 `"49.6"', add
label define erscor50_lbl 0497 `"49.7"', add
label define erscor50_lbl 0498 `"49.8"', add
label define erscor50_lbl 0499 `"49.9"', add
label define erscor50_lbl 0500 `"50"', add
label define erscor50_lbl 0501 `"50.1"', add
label define erscor50_lbl 0502 `"50.2"', add
label define erscor50_lbl 0503 `"50.3"', add
label define erscor50_lbl 0504 `"50.4"', add
label define erscor50_lbl 0505 `"50.5"', add
label define erscor50_lbl 0506 `"50.6"', add
label define erscor50_lbl 0507 `"50.7"', add
label define erscor50_lbl 0508 `"50.8"', add
label define erscor50_lbl 0509 `"50.9"', add
label define erscor50_lbl 0510 `"51"', add
label define erscor50_lbl 0511 `"51.1"', add
label define erscor50_lbl 0512 `"51.2"', add
label define erscor50_lbl 0513 `"51.3"', add
label define erscor50_lbl 0514 `"51.4"', add
label define erscor50_lbl 0515 `"51.5"', add
label define erscor50_lbl 0516 `"51.6"', add
label define erscor50_lbl 0517 `"51.7"', add
label define erscor50_lbl 0518 `"51.8"', add
label define erscor50_lbl 0519 `"51.9"', add
label define erscor50_lbl 0520 `"52"', add
label define erscor50_lbl 0521 `"52.1"', add
label define erscor50_lbl 0522 `"52.2"', add
label define erscor50_lbl 0523 `"52.3"', add
label define erscor50_lbl 0524 `"52.4"', add
label define erscor50_lbl 0525 `"52.5"', add
label define erscor50_lbl 0526 `"52.6"', add
label define erscor50_lbl 0527 `"52.7"', add
label define erscor50_lbl 0528 `"52.8"', add
label define erscor50_lbl 0529 `"52.9"', add
label define erscor50_lbl 0530 `"53"', add
label define erscor50_lbl 0531 `"53.1"', add
label define erscor50_lbl 0532 `"53.2"', add
label define erscor50_lbl 0533 `"53.3"', add
label define erscor50_lbl 0534 `"53.4"', add
label define erscor50_lbl 0535 `"53.5"', add
label define erscor50_lbl 0536 `"53.6"', add
label define erscor50_lbl 0537 `"53.7"', add
label define erscor50_lbl 0538 `"53.8"', add
label define erscor50_lbl 0539 `"53.9"', add
label define erscor50_lbl 0540 `"54"', add
label define erscor50_lbl 0541 `"54.1"', add
label define erscor50_lbl 0542 `"54.2"', add
label define erscor50_lbl 0543 `"54.3"', add
label define erscor50_lbl 0544 `"54.4"', add
label define erscor50_lbl 0545 `"54.5"', add
label define erscor50_lbl 0546 `"54.6"', add
label define erscor50_lbl 0547 `"54.7"', add
label define erscor50_lbl 0548 `"54.8"', add
label define erscor50_lbl 0549 `"54.9"', add
label define erscor50_lbl 0550 `"55"', add
label define erscor50_lbl 0551 `"55.1"', add
label define erscor50_lbl 0552 `"55.2"', add
label define erscor50_lbl 0553 `"55.3"', add
label define erscor50_lbl 0554 `"55.4"', add
label define erscor50_lbl 0555 `"55.5"', add
label define erscor50_lbl 0556 `"55.6"', add
label define erscor50_lbl 0557 `"55.7"', add
label define erscor50_lbl 0558 `"55.8"', add
label define erscor50_lbl 0559 `"55.9"', add
label define erscor50_lbl 0560 `"56"', add
label define erscor50_lbl 0561 `"56.1"', add
label define erscor50_lbl 0562 `"56.2"', add
label define erscor50_lbl 0563 `"56.3"', add
label define erscor50_lbl 0564 `"56.4"', add
label define erscor50_lbl 0565 `"56.5"', add
label define erscor50_lbl 0566 `"56.6"', add
label define erscor50_lbl 0567 `"56.7"', add
label define erscor50_lbl 0568 `"56.8"', add
label define erscor50_lbl 0569 `"56.9"', add
label define erscor50_lbl 0570 `"57"', add
label define erscor50_lbl 0571 `"57.1"', add
label define erscor50_lbl 0572 `"57.2"', add
label define erscor50_lbl 0573 `"57.3"', add
label define erscor50_lbl 0574 `"57.4"', add
label define erscor50_lbl 0575 `"57.5"', add
label define erscor50_lbl 0576 `"57.6"', add
label define erscor50_lbl 0577 `"57.7"', add
label define erscor50_lbl 0578 `"57.8"', add
label define erscor50_lbl 0579 `"57.9"', add
label define erscor50_lbl 0580 `"58"', add
label define erscor50_lbl 0581 `"58.1"', add
label define erscor50_lbl 0582 `"58.2"', add
label define erscor50_lbl 0583 `"58.3"', add
label define erscor50_lbl 0584 `"58.4"', add
label define erscor50_lbl 0585 `"58.5"', add
label define erscor50_lbl 0586 `"58.6"', add
label define erscor50_lbl 0587 `"58.7"', add
label define erscor50_lbl 0588 `"58.8"', add
label define erscor50_lbl 0589 `"58.9"', add
label define erscor50_lbl 0590 `"59"', add
label define erscor50_lbl 0591 `"59.1"', add
label define erscor50_lbl 0592 `"59.2"', add
label define erscor50_lbl 0593 `"59.3"', add
label define erscor50_lbl 0594 `"59.4"', add
label define erscor50_lbl 0595 `"59.5"', add
label define erscor50_lbl 0596 `"59.6"', add
label define erscor50_lbl 0597 `"59.7"', add
label define erscor50_lbl 0598 `"59.8"', add
label define erscor50_lbl 0599 `"59.9"', add
label define erscor50_lbl 0600 `"60"', add
label define erscor50_lbl 0601 `"60.1"', add
label define erscor50_lbl 0602 `"60.2"', add
label define erscor50_lbl 0603 `"60.3"', add
label define erscor50_lbl 0604 `"60.4"', add
label define erscor50_lbl 0605 `"60.5"', add
label define erscor50_lbl 0606 `"60.6"', add
label define erscor50_lbl 0607 `"60.7"', add
label define erscor50_lbl 0608 `"60.8"', add
label define erscor50_lbl 0609 `"60.9"', add
label define erscor50_lbl 0610 `"61"', add
label define erscor50_lbl 0611 `"61.1"', add
label define erscor50_lbl 0612 `"61.2"', add
label define erscor50_lbl 0613 `"61.3"', add
label define erscor50_lbl 0614 `"61.4"', add
label define erscor50_lbl 0615 `"61.5"', add
label define erscor50_lbl 0616 `"61.6"', add
label define erscor50_lbl 0617 `"61.7"', add
label define erscor50_lbl 0618 `"61.8"', add
label define erscor50_lbl 0619 `"61.9"', add
label define erscor50_lbl 0620 `"62"', add
label define erscor50_lbl 0621 `"62.1"', add
label define erscor50_lbl 0622 `"62.2"', add
label define erscor50_lbl 0623 `"62.3"', add
label define erscor50_lbl 0624 `"62.4"', add
label define erscor50_lbl 0625 `"62.5"', add
label define erscor50_lbl 0626 `"62.6"', add
label define erscor50_lbl 0627 `"62.7"', add
label define erscor50_lbl 0628 `"62.8"', add
label define erscor50_lbl 0629 `"62.9"', add
label define erscor50_lbl 0630 `"63"', add
label define erscor50_lbl 0631 `"63.1"', add
label define erscor50_lbl 0632 `"63.2"', add
label define erscor50_lbl 0633 `"63.3"', add
label define erscor50_lbl 0634 `"63.4"', add
label define erscor50_lbl 0635 `"63.5"', add
label define erscor50_lbl 0636 `"63.6"', add
label define erscor50_lbl 0637 `"63.7"', add
label define erscor50_lbl 0638 `"63.8"', add
label define erscor50_lbl 0639 `"63.9"', add
label define erscor50_lbl 0640 `"64"', add
label define erscor50_lbl 0641 `"64.1"', add
label define erscor50_lbl 0642 `"64.2"', add
label define erscor50_lbl 0643 `"64.3"', add
label define erscor50_lbl 0644 `"64.4"', add
label define erscor50_lbl 0645 `"64.5"', add
label define erscor50_lbl 0646 `"64.6"', add
label define erscor50_lbl 0647 `"64.7"', add
label define erscor50_lbl 0648 `"64.8"', add
label define erscor50_lbl 0649 `"64.9"', add
label define erscor50_lbl 0650 `"65"', add
label define erscor50_lbl 0651 `"65.1"', add
label define erscor50_lbl 0652 `"65.2"', add
label define erscor50_lbl 0653 `"65.3"', add
label define erscor50_lbl 0654 `"65.4"', add
label define erscor50_lbl 0655 `"65.5"', add
label define erscor50_lbl 0656 `"65.6"', add
label define erscor50_lbl 0657 `"65.7"', add
label define erscor50_lbl 0658 `"65.8"', add
label define erscor50_lbl 0659 `"65.9"', add
label define erscor50_lbl 0660 `"66"', add
label define erscor50_lbl 0661 `"66.1"', add
label define erscor50_lbl 0662 `"66.2"', add
label define erscor50_lbl 0663 `"66.3"', add
label define erscor50_lbl 0664 `"66.4"', add
label define erscor50_lbl 0665 `"66.5"', add
label define erscor50_lbl 0666 `"66.6"', add
label define erscor50_lbl 0667 `"66.7"', add
label define erscor50_lbl 0668 `"66.8"', add
label define erscor50_lbl 0669 `"66.9"', add
label define erscor50_lbl 0670 `"67"', add
label define erscor50_lbl 0671 `"67.1"', add
label define erscor50_lbl 0672 `"67.2"', add
label define erscor50_lbl 0673 `"67.3"', add
label define erscor50_lbl 0674 `"67.4"', add
label define erscor50_lbl 0675 `"67.5"', add
label define erscor50_lbl 0676 `"67.6"', add
label define erscor50_lbl 0677 `"67.7"', add
label define erscor50_lbl 0678 `"67.8"', add
label define erscor50_lbl 0679 `"67.9"', add
label define erscor50_lbl 0680 `"68"', add
label define erscor50_lbl 0681 `"68.1"', add
label define erscor50_lbl 0682 `"68.2"', add
label define erscor50_lbl 0683 `"68.3"', add
label define erscor50_lbl 0684 `"68.4"', add
label define erscor50_lbl 0685 `"68.5"', add
label define erscor50_lbl 0686 `"68.6"', add
label define erscor50_lbl 0687 `"68.7"', add
label define erscor50_lbl 0688 `"68.8"', add
label define erscor50_lbl 0689 `"68.9"', add
label define erscor50_lbl 0690 `"69"', add
label define erscor50_lbl 0691 `"69.1"', add
label define erscor50_lbl 0692 `"69.2"', add
label define erscor50_lbl 0693 `"69.3"', add
label define erscor50_lbl 0694 `"69.4"', add
label define erscor50_lbl 0695 `"69.5"', add
label define erscor50_lbl 0696 `"69.6"', add
label define erscor50_lbl 0697 `"69.7"', add
label define erscor50_lbl 0698 `"69.8"', add
label define erscor50_lbl 0699 `"69.9"', add
label define erscor50_lbl 0700 `"70"', add
label define erscor50_lbl 0701 `"70.1"', add
label define erscor50_lbl 0702 `"70.2"', add
label define erscor50_lbl 0703 `"70.3"', add
label define erscor50_lbl 0704 `"70.4"', add
label define erscor50_lbl 0705 `"70.5"', add
label define erscor50_lbl 0706 `"70.6"', add
label define erscor50_lbl 0707 `"70.7"', add
label define erscor50_lbl 0708 `"70.8"', add
label define erscor50_lbl 0709 `"70.9"', add
label define erscor50_lbl 0710 `"71"', add
label define erscor50_lbl 0711 `"71.1"', add
label define erscor50_lbl 0712 `"71.2"', add
label define erscor50_lbl 0713 `"71.3"', add
label define erscor50_lbl 0714 `"71.4"', add
label define erscor50_lbl 0715 `"71.5"', add
label define erscor50_lbl 0716 `"71.6"', add
label define erscor50_lbl 0717 `"71.7"', add
label define erscor50_lbl 0718 `"71.8"', add
label define erscor50_lbl 0719 `"71.9"', add
label define erscor50_lbl 0720 `"72"', add
label define erscor50_lbl 0721 `"72.1"', add
label define erscor50_lbl 0722 `"72.2"', add
label define erscor50_lbl 0723 `"72.3"', add
label define erscor50_lbl 0724 `"72.4"', add
label define erscor50_lbl 0725 `"72.5"', add
label define erscor50_lbl 0726 `"72.6"', add
label define erscor50_lbl 0727 `"72.7"', add
label define erscor50_lbl 0728 `"72.8"', add
label define erscor50_lbl 0729 `"72.9"', add
label define erscor50_lbl 0730 `"73"', add
label define erscor50_lbl 0731 `"73.1"', add
label define erscor50_lbl 0732 `"73.2"', add
label define erscor50_lbl 0733 `"73.3"', add
label define erscor50_lbl 0734 `"73.4"', add
label define erscor50_lbl 0735 `"73.5"', add
label define erscor50_lbl 0736 `"73.6"', add
label define erscor50_lbl 0737 `"73.7"', add
label define erscor50_lbl 0738 `"73.8"', add
label define erscor50_lbl 0739 `"73.9"', add
label define erscor50_lbl 0740 `"74"', add
label define erscor50_lbl 0741 `"74.1"', add
label define erscor50_lbl 0742 `"74.2"', add
label define erscor50_lbl 0743 `"74.3"', add
label define erscor50_lbl 0744 `"74.4"', add
label define erscor50_lbl 0745 `"74.5"', add
label define erscor50_lbl 0746 `"74.6"', add
label define erscor50_lbl 0747 `"74.7"', add
label define erscor50_lbl 0748 `"74.8"', add
label define erscor50_lbl 0749 `"74.9"', add
label define erscor50_lbl 0750 `"75"', add
label define erscor50_lbl 0751 `"75.1"', add
label define erscor50_lbl 0752 `"75.2"', add
label define erscor50_lbl 0753 `"75.3"', add
label define erscor50_lbl 0754 `"75.4"', add
label define erscor50_lbl 0755 `"75.5"', add
label define erscor50_lbl 0756 `"75.6"', add
label define erscor50_lbl 0757 `"75.7"', add
label define erscor50_lbl 0758 `"75.8"', add
label define erscor50_lbl 0759 `"75.9"', add
label define erscor50_lbl 0760 `"76"', add
label define erscor50_lbl 0761 `"76.1"', add
label define erscor50_lbl 0762 `"76.2"', add
label define erscor50_lbl 0763 `"76.3"', add
label define erscor50_lbl 0764 `"76.4"', add
label define erscor50_lbl 0765 `"76.5"', add
label define erscor50_lbl 0766 `"76.6"', add
label define erscor50_lbl 0767 `"76.7"', add
label define erscor50_lbl 0768 `"76.8"', add
label define erscor50_lbl 0769 `"76.9"', add
label define erscor50_lbl 0770 `"77"', add
label define erscor50_lbl 0771 `"77.1"', add
label define erscor50_lbl 0772 `"77.2"', add
label define erscor50_lbl 0773 `"77.3"', add
label define erscor50_lbl 0774 `"77.4"', add
label define erscor50_lbl 0775 `"77.5"', add
label define erscor50_lbl 0776 `"77.6"', add
label define erscor50_lbl 0777 `"77.7"', add
label define erscor50_lbl 0778 `"77.8"', add
label define erscor50_lbl 0779 `"77.9"', add
label define erscor50_lbl 0780 `"78"', add
label define erscor50_lbl 0781 `"78.1"', add
label define erscor50_lbl 0782 `"78.2"', add
label define erscor50_lbl 0783 `"78.3"', add
label define erscor50_lbl 0784 `"78.4"', add
label define erscor50_lbl 0785 `"78.5"', add
label define erscor50_lbl 0786 `"78.6"', add
label define erscor50_lbl 0787 `"78.7"', add
label define erscor50_lbl 0788 `"78.8"', add
label define erscor50_lbl 0789 `"78.9"', add
label define erscor50_lbl 0790 `"79"', add
label define erscor50_lbl 0791 `"79.1"', add
label define erscor50_lbl 0792 `"79.2"', add
label define erscor50_lbl 0793 `"79.3"', add
label define erscor50_lbl 0794 `"79.4"', add
label define erscor50_lbl 0795 `"79.5"', add
label define erscor50_lbl 0796 `"79.6"', add
label define erscor50_lbl 0797 `"79.7"', add
label define erscor50_lbl 0798 `"79.8"', add
label define erscor50_lbl 0799 `"79.9"', add
label define erscor50_lbl 0800 `"80"', add
label define erscor50_lbl 0801 `"80.1"', add
label define erscor50_lbl 0802 `"80.2"', add
label define erscor50_lbl 0803 `"80.3"', add
label define erscor50_lbl 0804 `"80.4"', add
label define erscor50_lbl 0805 `"80.5"', add
label define erscor50_lbl 0806 `"80.6"', add
label define erscor50_lbl 0807 `"80.7"', add
label define erscor50_lbl 0808 `"80.8"', add
label define erscor50_lbl 0809 `"80.9"', add
label define erscor50_lbl 0810 `"81"', add
label define erscor50_lbl 0811 `"81.1"', add
label define erscor50_lbl 0812 `"81.2"', add
label define erscor50_lbl 0813 `"81.3"', add
label define erscor50_lbl 0814 `"81.4"', add
label define erscor50_lbl 0815 `"81.5"', add
label define erscor50_lbl 0816 `"81.6"', add
label define erscor50_lbl 0817 `"81.7"', add
label define erscor50_lbl 0818 `"81.8"', add
label define erscor50_lbl 0819 `"81.9"', add
label define erscor50_lbl 0820 `"82"', add
label define erscor50_lbl 0821 `"82.1"', add
label define erscor50_lbl 0822 `"82.2"', add
label define erscor50_lbl 0823 `"82.3"', add
label define erscor50_lbl 0824 `"82.4"', add
label define erscor50_lbl 0825 `"82.5"', add
label define erscor50_lbl 0826 `"82.6"', add
label define erscor50_lbl 0827 `"82.7"', add
label define erscor50_lbl 0828 `"82.8"', add
label define erscor50_lbl 0829 `"82.9"', add
label define erscor50_lbl 0830 `"83"', add
label define erscor50_lbl 0831 `"83.1"', add
label define erscor50_lbl 0832 `"83.2"', add
label define erscor50_lbl 0833 `"83.3"', add
label define erscor50_lbl 0834 `"83.4"', add
label define erscor50_lbl 0835 `"83.5"', add
label define erscor50_lbl 0836 `"83.6"', add
label define erscor50_lbl 0837 `"83.7"', add
label define erscor50_lbl 0838 `"83.8"', add
label define erscor50_lbl 0839 `"83.9"', add
label define erscor50_lbl 0840 `"84"', add
label define erscor50_lbl 0841 `"84.1"', add
label define erscor50_lbl 0842 `"84.2"', add
label define erscor50_lbl 0843 `"84.3"', add
label define erscor50_lbl 0844 `"84.4"', add
label define erscor50_lbl 0845 `"84.5"', add
label define erscor50_lbl 0846 `"84.6"', add
label define erscor50_lbl 0847 `"84.7"', add
label define erscor50_lbl 0848 `"84.8"', add
label define erscor50_lbl 0849 `"84.9"', add
label define erscor50_lbl 0850 `"85"', add
label define erscor50_lbl 0851 `"85.1"', add
label define erscor50_lbl 0852 `"85.2"', add
label define erscor50_lbl 0853 `"85.3"', add
label define erscor50_lbl 0854 `"85.4"', add
label define erscor50_lbl 0855 `"85.5"', add
label define erscor50_lbl 0856 `"85.6"', add
label define erscor50_lbl 0857 `"85.7"', add
label define erscor50_lbl 0858 `"85.8"', add
label define erscor50_lbl 0859 `"85.9"', add
label define erscor50_lbl 0860 `"86"', add
label define erscor50_lbl 0861 `"86.1"', add
label define erscor50_lbl 0862 `"86.2"', add
label define erscor50_lbl 0863 `"86.3"', add
label define erscor50_lbl 0864 `"86.4"', add
label define erscor50_lbl 0865 `"86.5"', add
label define erscor50_lbl 0866 `"86.6"', add
label define erscor50_lbl 0867 `"86.7"', add
label define erscor50_lbl 0868 `"86.8"', add
label define erscor50_lbl 0869 `"86.9"', add
label define erscor50_lbl 0870 `"87"', add
label define erscor50_lbl 0871 `"87.1"', add
label define erscor50_lbl 0872 `"87.2"', add
label define erscor50_lbl 0873 `"87.3"', add
label define erscor50_lbl 0874 `"87.4"', add
label define erscor50_lbl 0875 `"87.5"', add
label define erscor50_lbl 0876 `"87.6"', add
label define erscor50_lbl 0877 `"87.7"', add
label define erscor50_lbl 0878 `"87.8"', add
label define erscor50_lbl 0879 `"87.9"', add
label define erscor50_lbl 0880 `"88"', add
label define erscor50_lbl 0881 `"88.1"', add
label define erscor50_lbl 0882 `"88.2"', add
label define erscor50_lbl 0883 `"88.3"', add
label define erscor50_lbl 0884 `"88.4"', add
label define erscor50_lbl 0885 `"88.5"', add
label define erscor50_lbl 0886 `"88.6"', add
label define erscor50_lbl 0887 `"88.7"', add
label define erscor50_lbl 0888 `"88.8"', add
label define erscor50_lbl 0889 `"88.9"', add
label define erscor50_lbl 0890 `"89"', add
label define erscor50_lbl 0891 `"89.1"', add
label define erscor50_lbl 0892 `"89.2"', add
label define erscor50_lbl 0893 `"89.3"', add
label define erscor50_lbl 0894 `"89.4"', add
label define erscor50_lbl 0895 `"89.5"', add
label define erscor50_lbl 0896 `"89.6"', add
label define erscor50_lbl 0897 `"89.7"', add
label define erscor50_lbl 0898 `"89.8"', add
label define erscor50_lbl 0899 `"89.9"', add
label define erscor50_lbl 0900 `"90"', add
label define erscor50_lbl 0901 `"90.1"', add
label define erscor50_lbl 0902 `"90.2"', add
label define erscor50_lbl 0903 `"90.3"', add
label define erscor50_lbl 0904 `"90.4"', add
label define erscor50_lbl 0905 `"90.5"', add
label define erscor50_lbl 0906 `"90.6"', add
label define erscor50_lbl 0907 `"90.7"', add
label define erscor50_lbl 0908 `"90.8"', add
label define erscor50_lbl 0909 `"90.9"', add
label define erscor50_lbl 0910 `"91"', add
label define erscor50_lbl 0911 `"91.1"', add
label define erscor50_lbl 0912 `"91.2"', add
label define erscor50_lbl 0913 `"91.3"', add
label define erscor50_lbl 0914 `"91.4"', add
label define erscor50_lbl 0915 `"91.5"', add
label define erscor50_lbl 0916 `"91.6"', add
label define erscor50_lbl 0917 `"91.7"', add
label define erscor50_lbl 0918 `"91.8"', add
label define erscor50_lbl 0919 `"91.9"', add
label define erscor50_lbl 0920 `"92"', add
label define erscor50_lbl 0921 `"92.1"', add
label define erscor50_lbl 0922 `"92.2"', add
label define erscor50_lbl 0923 `"92.3"', add
label define erscor50_lbl 0924 `"92.4"', add
label define erscor50_lbl 0925 `"92.5"', add
label define erscor50_lbl 0926 `"92.6"', add
label define erscor50_lbl 0927 `"92.7"', add
label define erscor50_lbl 0928 `"92.8"', add
label define erscor50_lbl 0929 `"92.9"', add
label define erscor50_lbl 0930 `"93"', add
label define erscor50_lbl 0931 `"93.1"', add
label define erscor50_lbl 0932 `"93.2"', add
label define erscor50_lbl 0933 `"93.3"', add
label define erscor50_lbl 0934 `"93.4"', add
label define erscor50_lbl 0935 `"93.5"', add
label define erscor50_lbl 0936 `"93.6"', add
label define erscor50_lbl 0937 `"93.7"', add
label define erscor50_lbl 0938 `"93.8"', add
label define erscor50_lbl 0939 `"93.9"', add
label define erscor50_lbl 0940 `"94"', add
label define erscor50_lbl 0941 `"94.1"', add
label define erscor50_lbl 0942 `"94.2"', add
label define erscor50_lbl 0943 `"94.3"', add
label define erscor50_lbl 0944 `"94.4"', add
label define erscor50_lbl 0945 `"94.5"', add
label define erscor50_lbl 0946 `"94.6"', add
label define erscor50_lbl 0947 `"94.7"', add
label define erscor50_lbl 0948 `"94.8"', add
label define erscor50_lbl 0949 `"94.9"', add
label define erscor50_lbl 0950 `"95"', add
label define erscor50_lbl 0951 `"95.1"', add
label define erscor50_lbl 0952 `"95.2"', add
label define erscor50_lbl 0953 `"95.3"', add
label define erscor50_lbl 0954 `"95.4"', add
label define erscor50_lbl 0955 `"95.5"', add
label define erscor50_lbl 0956 `"95.6"', add
label define erscor50_lbl 0957 `"95.7"', add
label define erscor50_lbl 0958 `"95.8"', add
label define erscor50_lbl 0959 `"95.9"', add
label define erscor50_lbl 0960 `"96"', add
label define erscor50_lbl 0961 `"96.1"', add
label define erscor50_lbl 0962 `"96.2"', add
label define erscor50_lbl 0963 `"96.3"', add
label define erscor50_lbl 0964 `"96.4"', add
label define erscor50_lbl 0965 `"96.5"', add
label define erscor50_lbl 0966 `"96.6"', add
label define erscor50_lbl 0967 `"96.7"', add
label define erscor50_lbl 0968 `"96.8"', add
label define erscor50_lbl 0969 `"96.9"', add
label define erscor50_lbl 0970 `"97"', add
label define erscor50_lbl 0971 `"97.1"', add
label define erscor50_lbl 0972 `"97.2"', add
label define erscor50_lbl 0973 `"97.3"', add
label define erscor50_lbl 0974 `"97.4"', add
label define erscor50_lbl 0975 `"97.5"', add
label define erscor50_lbl 0976 `"97.6"', add
label define erscor50_lbl 0977 `"97.7"', add
label define erscor50_lbl 0978 `"97.8"', add
label define erscor50_lbl 0979 `"97.9"', add
label define erscor50_lbl 0980 `"98"', add
label define erscor50_lbl 0981 `"98.1"', add
label define erscor50_lbl 0982 `"98.2"', add
label define erscor50_lbl 0983 `"98.3"', add
label define erscor50_lbl 0984 `"98.4"', add
label define erscor50_lbl 0985 `"98.5"', add
label define erscor50_lbl 0986 `"98.6"', add
label define erscor50_lbl 0987 `"98.7"', add
label define erscor50_lbl 0988 `"98.8"', add
label define erscor50_lbl 0989 `"98.9"', add
label define erscor50_lbl 0990 `"99"', add
label define erscor50_lbl 0991 `"99.1"', add
label define erscor50_lbl 0992 `"99.2"', add
label define erscor50_lbl 0993 `"99.3"', add
label define erscor50_lbl 0994 `"99.4"', add
label define erscor50_lbl 0995 `"99.5"', add
label define erscor50_lbl 0996 `"99.6"', add
label define erscor50_lbl 0997 `"99.7"', add
label define erscor50_lbl 0998 `"99.8"', add
label define erscor50_lbl 0999 `"99.9"', add
label define erscor50_lbl 1000 `"100"', add
label define erscor50_lbl 9999 `"N/A"', add
label values erscor50 erscor50_lbl

label define edscor50_lbl 0000 `"0"'
label define edscor50_lbl 0001 `"0.1"', add
label define edscor50_lbl 0002 `"0.2"', add
label define edscor50_lbl 0003 `"0.3"', add
label define edscor50_lbl 0004 `"0.4"', add
label define edscor50_lbl 0005 `"0.5"', add
label define edscor50_lbl 0006 `"0.6"', add
label define edscor50_lbl 0007 `"0.7"', add
label define edscor50_lbl 0008 `"0.8"', add
label define edscor50_lbl 0009 `"0.9"', add
label define edscor50_lbl 0010 `"1"', add
label define edscor50_lbl 0011 `"1.1"', add
label define edscor50_lbl 0012 `"1.2"', add
label define edscor50_lbl 0013 `"1.3"', add
label define edscor50_lbl 0014 `"1.4"', add
label define edscor50_lbl 0015 `"1.5"', add
label define edscor50_lbl 0016 `"1.6"', add
label define edscor50_lbl 0017 `"1.7"', add
label define edscor50_lbl 0018 `"1.8"', add
label define edscor50_lbl 0019 `"1.9"', add
label define edscor50_lbl 0020 `"2"', add
label define edscor50_lbl 0021 `"2.1"', add
label define edscor50_lbl 0022 `"2.2"', add
label define edscor50_lbl 0023 `"2.3"', add
label define edscor50_lbl 0024 `"2.4"', add
label define edscor50_lbl 0025 `"2.5"', add
label define edscor50_lbl 0026 `"2.6"', add
label define edscor50_lbl 0027 `"2.7"', add
label define edscor50_lbl 0028 `"2.8"', add
label define edscor50_lbl 0029 `"2.9"', add
label define edscor50_lbl 0030 `"3"', add
label define edscor50_lbl 0031 `"3.1"', add
label define edscor50_lbl 0032 `"3.2"', add
label define edscor50_lbl 0033 `"3.3"', add
label define edscor50_lbl 0034 `"3.4"', add
label define edscor50_lbl 0035 `"3.5"', add
label define edscor50_lbl 0036 `"3.6"', add
label define edscor50_lbl 0037 `"3.7"', add
label define edscor50_lbl 0038 `"3.8"', add
label define edscor50_lbl 0039 `"3.9"', add
label define edscor50_lbl 0040 `"4"', add
label define edscor50_lbl 0041 `"4.1"', add
label define edscor50_lbl 0042 `"4.2"', add
label define edscor50_lbl 0043 `"4.3"', add
label define edscor50_lbl 0044 `"4.4"', add
label define edscor50_lbl 0045 `"4.5"', add
label define edscor50_lbl 0046 `"4.6"', add
label define edscor50_lbl 0047 `"4.7"', add
label define edscor50_lbl 0048 `"4.8"', add
label define edscor50_lbl 0049 `"4.9"', add
label define edscor50_lbl 0050 `"5"', add
label define edscor50_lbl 0051 `"5.1"', add
label define edscor50_lbl 0052 `"5.2"', add
label define edscor50_lbl 0053 `"5.3"', add
label define edscor50_lbl 0054 `"5.4"', add
label define edscor50_lbl 0055 `"5.5"', add
label define edscor50_lbl 0056 `"5.6"', add
label define edscor50_lbl 0057 `"5.7"', add
label define edscor50_lbl 0058 `"5.8"', add
label define edscor50_lbl 0059 `"5.9"', add
label define edscor50_lbl 0060 `"6"', add
label define edscor50_lbl 0061 `"6.1"', add
label define edscor50_lbl 0062 `"6.2"', add
label define edscor50_lbl 0063 `"6.3"', add
label define edscor50_lbl 0064 `"6.4"', add
label define edscor50_lbl 0065 `"6.5"', add
label define edscor50_lbl 0066 `"6.6"', add
label define edscor50_lbl 0067 `"6.7"', add
label define edscor50_lbl 0068 `"6.8"', add
label define edscor50_lbl 0069 `"6.9"', add
label define edscor50_lbl 0070 `"7"', add
label define edscor50_lbl 0071 `"7.1"', add
label define edscor50_lbl 0072 `"7.2"', add
label define edscor50_lbl 0073 `"7.3"', add
label define edscor50_lbl 0074 `"7.4"', add
label define edscor50_lbl 0075 `"7.5"', add
label define edscor50_lbl 0076 `"7.6"', add
label define edscor50_lbl 0077 `"7.7"', add
label define edscor50_lbl 0078 `"7.8"', add
label define edscor50_lbl 0079 `"7.9"', add
label define edscor50_lbl 0080 `"8"', add
label define edscor50_lbl 0081 `"8.1"', add
label define edscor50_lbl 0082 `"8.2"', add
label define edscor50_lbl 0083 `"8.3"', add
label define edscor50_lbl 0084 `"8.4"', add
label define edscor50_lbl 0085 `"8.5"', add
label define edscor50_lbl 0086 `"8.6"', add
label define edscor50_lbl 0087 `"8.7"', add
label define edscor50_lbl 0088 `"8.8"', add
label define edscor50_lbl 0089 `"8.9"', add
label define edscor50_lbl 0090 `"9"', add
label define edscor50_lbl 0091 `"9.1"', add
label define edscor50_lbl 0092 `"9.2"', add
label define edscor50_lbl 0093 `"9.3"', add
label define edscor50_lbl 0094 `"9.4"', add
label define edscor50_lbl 0095 `"9.5"', add
label define edscor50_lbl 0096 `"9.6"', add
label define edscor50_lbl 0097 `"9.7"', add
label define edscor50_lbl 0098 `"9.8"', add
label define edscor50_lbl 0099 `"9.9"', add
label define edscor50_lbl 0100 `"10"', add
label define edscor50_lbl 0101 `"10.1"', add
label define edscor50_lbl 0102 `"10.2"', add
label define edscor50_lbl 0103 `"10.3"', add
label define edscor50_lbl 0104 `"10.4"', add
label define edscor50_lbl 0105 `"10.5"', add
label define edscor50_lbl 0106 `"10.6"', add
label define edscor50_lbl 0107 `"10.7"', add
label define edscor50_lbl 0108 `"10.8"', add
label define edscor50_lbl 0109 `"10.9"', add
label define edscor50_lbl 0110 `"11"', add
label define edscor50_lbl 0111 `"11.1"', add
label define edscor50_lbl 0112 `"11.2"', add
label define edscor50_lbl 0113 `"11.3"', add
label define edscor50_lbl 0114 `"11.4"', add
label define edscor50_lbl 0115 `"11.5"', add
label define edscor50_lbl 0116 `"11.6"', add
label define edscor50_lbl 0117 `"11.7"', add
label define edscor50_lbl 0118 `"11.8"', add
label define edscor50_lbl 0119 `"11.9"', add
label define edscor50_lbl 0120 `"12"', add
label define edscor50_lbl 0121 `"12.1"', add
label define edscor50_lbl 0122 `"12.2"', add
label define edscor50_lbl 0123 `"12.3"', add
label define edscor50_lbl 0124 `"12.4"', add
label define edscor50_lbl 0125 `"12.5"', add
label define edscor50_lbl 0126 `"12.6"', add
label define edscor50_lbl 0127 `"12.7"', add
label define edscor50_lbl 0128 `"12.8"', add
label define edscor50_lbl 0129 `"12.9"', add
label define edscor50_lbl 0130 `"13"', add
label define edscor50_lbl 0131 `"13.1"', add
label define edscor50_lbl 0132 `"13.2"', add
label define edscor50_lbl 0133 `"13.3"', add
label define edscor50_lbl 0134 `"13.4"', add
label define edscor50_lbl 0135 `"13.5"', add
label define edscor50_lbl 0136 `"13.6"', add
label define edscor50_lbl 0137 `"13.7"', add
label define edscor50_lbl 0138 `"13.8"', add
label define edscor50_lbl 0139 `"13.9"', add
label define edscor50_lbl 0140 `"14"', add
label define edscor50_lbl 0141 `"14.1"', add
label define edscor50_lbl 0142 `"14.2"', add
label define edscor50_lbl 0143 `"14.3"', add
label define edscor50_lbl 0144 `"14.4"', add
label define edscor50_lbl 0145 `"14.5"', add
label define edscor50_lbl 0146 `"14.6"', add
label define edscor50_lbl 0147 `"14.7"', add
label define edscor50_lbl 0148 `"14.8"', add
label define edscor50_lbl 0149 `"14.9"', add
label define edscor50_lbl 0150 `"15"', add
label define edscor50_lbl 0151 `"15.1"', add
label define edscor50_lbl 0152 `"15.2"', add
label define edscor50_lbl 0153 `"15.3"', add
label define edscor50_lbl 0154 `"15.4"', add
label define edscor50_lbl 0155 `"15.5"', add
label define edscor50_lbl 0156 `"15.6"', add
label define edscor50_lbl 0157 `"15.7"', add
label define edscor50_lbl 0158 `"15.8"', add
label define edscor50_lbl 0159 `"15.9"', add
label define edscor50_lbl 0160 `"16"', add
label define edscor50_lbl 0161 `"16.1"', add
label define edscor50_lbl 0162 `"16.2"', add
label define edscor50_lbl 0163 `"16.3"', add
label define edscor50_lbl 0164 `"16.4"', add
label define edscor50_lbl 0165 `"16.5"', add
label define edscor50_lbl 0166 `"16.6"', add
label define edscor50_lbl 0167 `"16.7"', add
label define edscor50_lbl 0168 `"16.8"', add
label define edscor50_lbl 0169 `"16.9"', add
label define edscor50_lbl 0170 `"17"', add
label define edscor50_lbl 0171 `"17.1"', add
label define edscor50_lbl 0172 `"17.2"', add
label define edscor50_lbl 0173 `"17.3"', add
label define edscor50_lbl 0174 `"17.4"', add
label define edscor50_lbl 0175 `"17.5"', add
label define edscor50_lbl 0176 `"17.6"', add
label define edscor50_lbl 0177 `"17.7"', add
label define edscor50_lbl 0178 `"17.8"', add
label define edscor50_lbl 0179 `"17.9"', add
label define edscor50_lbl 0180 `"18"', add
label define edscor50_lbl 0181 `"18.1"', add
label define edscor50_lbl 0182 `"18.2"', add
label define edscor50_lbl 0183 `"18.3"', add
label define edscor50_lbl 0184 `"18.4"', add
label define edscor50_lbl 0185 `"18.5"', add
label define edscor50_lbl 0186 `"18.6"', add
label define edscor50_lbl 0187 `"18.7"', add
label define edscor50_lbl 0188 `"18.8"', add
label define edscor50_lbl 0189 `"18.9"', add
label define edscor50_lbl 0190 `"19"', add
label define edscor50_lbl 0191 `"19.1"', add
label define edscor50_lbl 0192 `"19.2"', add
label define edscor50_lbl 0193 `"19.3"', add
label define edscor50_lbl 0194 `"19.4"', add
label define edscor50_lbl 0195 `"19.5"', add
label define edscor50_lbl 0196 `"19.6"', add
label define edscor50_lbl 0197 `"19.7"', add
label define edscor50_lbl 0198 `"19.8"', add
label define edscor50_lbl 0199 `"19.9"', add
label define edscor50_lbl 0200 `"20"', add
label define edscor50_lbl 0201 `"20.1"', add
label define edscor50_lbl 0202 `"20.2"', add
label define edscor50_lbl 0203 `"20.3"', add
label define edscor50_lbl 0204 `"20.4"', add
label define edscor50_lbl 0205 `"20.5"', add
label define edscor50_lbl 0206 `"20.6"', add
label define edscor50_lbl 0207 `"20.7"', add
label define edscor50_lbl 0208 `"20.8"', add
label define edscor50_lbl 0209 `"20.9"', add
label define edscor50_lbl 0210 `"21"', add
label define edscor50_lbl 0211 `"21.1"', add
label define edscor50_lbl 0212 `"21.2"', add
label define edscor50_lbl 0213 `"21.3"', add
label define edscor50_lbl 0214 `"21.4"', add
label define edscor50_lbl 0215 `"21.5"', add
label define edscor50_lbl 0216 `"21.6"', add
label define edscor50_lbl 0217 `"21.7"', add
label define edscor50_lbl 0218 `"21.8"', add
label define edscor50_lbl 0219 `"21.9"', add
label define edscor50_lbl 0220 `"22"', add
label define edscor50_lbl 0221 `"22.1"', add
label define edscor50_lbl 0222 `"22.2"', add
label define edscor50_lbl 0223 `"22.3"', add
label define edscor50_lbl 0224 `"22.4"', add
label define edscor50_lbl 0225 `"22.5"', add
label define edscor50_lbl 0226 `"22.6"', add
label define edscor50_lbl 0227 `"22.7"', add
label define edscor50_lbl 0228 `"22.8"', add
label define edscor50_lbl 0229 `"22.9"', add
label define edscor50_lbl 0230 `"23"', add
label define edscor50_lbl 0231 `"23.1"', add
label define edscor50_lbl 0232 `"23.2"', add
label define edscor50_lbl 0233 `"23.3"', add
label define edscor50_lbl 0234 `"23.4"', add
label define edscor50_lbl 0235 `"23.5"', add
label define edscor50_lbl 0236 `"23.6"', add
label define edscor50_lbl 0237 `"23.7"', add
label define edscor50_lbl 0238 `"23.8"', add
label define edscor50_lbl 0239 `"23.9"', add
label define edscor50_lbl 0240 `"24"', add
label define edscor50_lbl 0241 `"24.1"', add
label define edscor50_lbl 0242 `"24.2"', add
label define edscor50_lbl 0243 `"24.3"', add
label define edscor50_lbl 0244 `"24.4"', add
label define edscor50_lbl 0245 `"24.5"', add
label define edscor50_lbl 0246 `"24.6"', add
label define edscor50_lbl 0247 `"24.7"', add
label define edscor50_lbl 0248 `"24.8"', add
label define edscor50_lbl 0249 `"24.9"', add
label define edscor50_lbl 0250 `"25"', add
label define edscor50_lbl 0251 `"25.1"', add
label define edscor50_lbl 0252 `"25.2"', add
label define edscor50_lbl 0253 `"25.3"', add
label define edscor50_lbl 0254 `"25.4"', add
label define edscor50_lbl 0255 `"25.5"', add
label define edscor50_lbl 0256 `"25.6"', add
label define edscor50_lbl 0257 `"25.7"', add
label define edscor50_lbl 0258 `"25.8"', add
label define edscor50_lbl 0259 `"25.9"', add
label define edscor50_lbl 0260 `"26"', add
label define edscor50_lbl 0261 `"26.1"', add
label define edscor50_lbl 0262 `"26.2"', add
label define edscor50_lbl 0263 `"26.3"', add
label define edscor50_lbl 0264 `"26.4"', add
label define edscor50_lbl 0265 `"26.5"', add
label define edscor50_lbl 0266 `"26.6"', add
label define edscor50_lbl 0267 `"26.7"', add
label define edscor50_lbl 0268 `"26.8"', add
label define edscor50_lbl 0269 `"26.9"', add
label define edscor50_lbl 0270 `"27"', add
label define edscor50_lbl 0271 `"27.1"', add
label define edscor50_lbl 0272 `"27.2"', add
label define edscor50_lbl 0273 `"27.3"', add
label define edscor50_lbl 0274 `"27.4"', add
label define edscor50_lbl 0275 `"27.5"', add
label define edscor50_lbl 0276 `"27.6"', add
label define edscor50_lbl 0277 `"27.7"', add
label define edscor50_lbl 0278 `"27.8"', add
label define edscor50_lbl 0279 `"27.9"', add
label define edscor50_lbl 0280 `"28"', add
label define edscor50_lbl 0281 `"28.1"', add
label define edscor50_lbl 0282 `"28.2"', add
label define edscor50_lbl 0283 `"28.3"', add
label define edscor50_lbl 0284 `"28.4"', add
label define edscor50_lbl 0285 `"28.5"', add
label define edscor50_lbl 0286 `"28.6"', add
label define edscor50_lbl 0287 `"28.7"', add
label define edscor50_lbl 0288 `"28.8"', add
label define edscor50_lbl 0289 `"28.9"', add
label define edscor50_lbl 0290 `"29"', add
label define edscor50_lbl 0291 `"29.1"', add
label define edscor50_lbl 0292 `"29.2"', add
label define edscor50_lbl 0293 `"29.3"', add
label define edscor50_lbl 0294 `"29.4"', add
label define edscor50_lbl 0295 `"29.5"', add
label define edscor50_lbl 0296 `"29.6"', add
label define edscor50_lbl 0297 `"29.7"', add
label define edscor50_lbl 0298 `"29.8"', add
label define edscor50_lbl 0299 `"29.9"', add
label define edscor50_lbl 0300 `"30"', add
label define edscor50_lbl 0301 `"30.1"', add
label define edscor50_lbl 0302 `"30.2"', add
label define edscor50_lbl 0303 `"30.3"', add
label define edscor50_lbl 0304 `"30.4"', add
label define edscor50_lbl 0305 `"30.5"', add
label define edscor50_lbl 0306 `"30.6"', add
label define edscor50_lbl 0307 `"30.7"', add
label define edscor50_lbl 0308 `"30.8"', add
label define edscor50_lbl 0309 `"30.9"', add
label define edscor50_lbl 0310 `"31"', add
label define edscor50_lbl 0311 `"31.1"', add
label define edscor50_lbl 0312 `"31.2"', add
label define edscor50_lbl 0313 `"31.3"', add
label define edscor50_lbl 0314 `"31.4"', add
label define edscor50_lbl 0315 `"31.5"', add
label define edscor50_lbl 0316 `"31.6"', add
label define edscor50_lbl 0317 `"31.7"', add
label define edscor50_lbl 0318 `"31.8"', add
label define edscor50_lbl 0319 `"31.9"', add
label define edscor50_lbl 0320 `"32"', add
label define edscor50_lbl 0321 `"32.1"', add
label define edscor50_lbl 0322 `"32.2"', add
label define edscor50_lbl 0323 `"32.3"', add
label define edscor50_lbl 0324 `"32.4"', add
label define edscor50_lbl 0325 `"32.5"', add
label define edscor50_lbl 0326 `"32.6"', add
label define edscor50_lbl 0327 `"32.7"', add
label define edscor50_lbl 0328 `"32.8"', add
label define edscor50_lbl 0329 `"32.9"', add
label define edscor50_lbl 0330 `"33"', add
label define edscor50_lbl 0331 `"33.1"', add
label define edscor50_lbl 0332 `"33.2"', add
label define edscor50_lbl 0333 `"33.3"', add
label define edscor50_lbl 0334 `"33.4"', add
label define edscor50_lbl 0335 `"33.5"', add
label define edscor50_lbl 0336 `"33.6"', add
label define edscor50_lbl 0337 `"33.7"', add
label define edscor50_lbl 0338 `"33.8"', add
label define edscor50_lbl 0339 `"33.9"', add
label define edscor50_lbl 0340 `"34"', add
label define edscor50_lbl 0341 `"34.1"', add
label define edscor50_lbl 0342 `"34.2"', add
label define edscor50_lbl 0343 `"34.3"', add
label define edscor50_lbl 0344 `"34.4"', add
label define edscor50_lbl 0345 `"34.5"', add
label define edscor50_lbl 0346 `"34.6"', add
label define edscor50_lbl 0347 `"34.7"', add
label define edscor50_lbl 0348 `"34.8"', add
label define edscor50_lbl 0349 `"34.9"', add
label define edscor50_lbl 0350 `"35"', add
label define edscor50_lbl 0351 `"35.1"', add
label define edscor50_lbl 0352 `"35.2"', add
label define edscor50_lbl 0353 `"35.3"', add
label define edscor50_lbl 0354 `"35.4"', add
label define edscor50_lbl 0355 `"35.5"', add
label define edscor50_lbl 0356 `"35.6"', add
label define edscor50_lbl 0357 `"35.7"', add
label define edscor50_lbl 0358 `"35.8"', add
label define edscor50_lbl 0359 `"35.9"', add
label define edscor50_lbl 0360 `"36"', add
label define edscor50_lbl 0361 `"36.1"', add
label define edscor50_lbl 0362 `"36.2"', add
label define edscor50_lbl 0363 `"36.3"', add
label define edscor50_lbl 0364 `"36.4"', add
label define edscor50_lbl 0365 `"36.5"', add
label define edscor50_lbl 0366 `"36.6"', add
label define edscor50_lbl 0367 `"36.7"', add
label define edscor50_lbl 0368 `"36.8"', add
label define edscor50_lbl 0369 `"36.9"', add
label define edscor50_lbl 0370 `"37"', add
label define edscor50_lbl 0371 `"37.1"', add
label define edscor50_lbl 0372 `"37.2"', add
label define edscor50_lbl 0373 `"37.3"', add
label define edscor50_lbl 0374 `"37.4"', add
label define edscor50_lbl 0375 `"37.5"', add
label define edscor50_lbl 0376 `"37.6"', add
label define edscor50_lbl 0377 `"37.7"', add
label define edscor50_lbl 0378 `"37.8"', add
label define edscor50_lbl 0379 `"37.9"', add
label define edscor50_lbl 0380 `"38"', add
label define edscor50_lbl 0381 `"38.1"', add
label define edscor50_lbl 0382 `"38.2"', add
label define edscor50_lbl 0383 `"38.3"', add
label define edscor50_lbl 0384 `"38.4"', add
label define edscor50_lbl 0385 `"38.5"', add
label define edscor50_lbl 0386 `"38.6"', add
label define edscor50_lbl 0387 `"38.7"', add
label define edscor50_lbl 0388 `"38.8"', add
label define edscor50_lbl 0389 `"38.9"', add
label define edscor50_lbl 0390 `"39"', add
label define edscor50_lbl 0391 `"39.1"', add
label define edscor50_lbl 0392 `"39.2"', add
label define edscor50_lbl 0393 `"39.3"', add
label define edscor50_lbl 0394 `"39.4"', add
label define edscor50_lbl 0395 `"39.5"', add
label define edscor50_lbl 0396 `"39.6"', add
label define edscor50_lbl 0397 `"39.7"', add
label define edscor50_lbl 0398 `"39.8"', add
label define edscor50_lbl 0399 `"39.9"', add
label define edscor50_lbl 0400 `"40"', add
label define edscor50_lbl 0401 `"40.1"', add
label define edscor50_lbl 0402 `"40.2"', add
label define edscor50_lbl 0403 `"40.3"', add
label define edscor50_lbl 0404 `"40.4"', add
label define edscor50_lbl 0405 `"40.5"', add
label define edscor50_lbl 0406 `"40.6"', add
label define edscor50_lbl 0407 `"40.7"', add
label define edscor50_lbl 0408 `"40.8"', add
label define edscor50_lbl 0409 `"40.9"', add
label define edscor50_lbl 0410 `"41"', add
label define edscor50_lbl 0411 `"41.1"', add
label define edscor50_lbl 0412 `"41.2"', add
label define edscor50_lbl 0413 `"41.3"', add
label define edscor50_lbl 0414 `"41.4"', add
label define edscor50_lbl 0415 `"41.5"', add
label define edscor50_lbl 0416 `"41.6"', add
label define edscor50_lbl 0417 `"41.7"', add
label define edscor50_lbl 0418 `"41.8"', add
label define edscor50_lbl 0419 `"41.9"', add
label define edscor50_lbl 0420 `"42"', add
label define edscor50_lbl 0421 `"42.1"', add
label define edscor50_lbl 0422 `"42.2"', add
label define edscor50_lbl 0423 `"42.3"', add
label define edscor50_lbl 0424 `"42.4"', add
label define edscor50_lbl 0425 `"42.5"', add
label define edscor50_lbl 0426 `"42.6"', add
label define edscor50_lbl 0427 `"42.7"', add
label define edscor50_lbl 0428 `"42.8"', add
label define edscor50_lbl 0429 `"42.9"', add
label define edscor50_lbl 0430 `"43"', add
label define edscor50_lbl 0431 `"43.1"', add
label define edscor50_lbl 0432 `"43.2"', add
label define edscor50_lbl 0433 `"43.3"', add
label define edscor50_lbl 0434 `"43.4"', add
label define edscor50_lbl 0435 `"43.5"', add
label define edscor50_lbl 0436 `"43.6"', add
label define edscor50_lbl 0437 `"43.7"', add
label define edscor50_lbl 0438 `"43.8"', add
label define edscor50_lbl 0439 `"43.9"', add
label define edscor50_lbl 0440 `"44"', add
label define edscor50_lbl 0441 `"44.1"', add
label define edscor50_lbl 0442 `"44.2"', add
label define edscor50_lbl 0443 `"44.3"', add
label define edscor50_lbl 0444 `"44.4"', add
label define edscor50_lbl 0445 `"44.5"', add
label define edscor50_lbl 0446 `"44.6"', add
label define edscor50_lbl 0447 `"44.7"', add
label define edscor50_lbl 0448 `"44.8"', add
label define edscor50_lbl 0449 `"44.9"', add
label define edscor50_lbl 0450 `"45"', add
label define edscor50_lbl 0451 `"45.1"', add
label define edscor50_lbl 0452 `"45.2"', add
label define edscor50_lbl 0453 `"45.3"', add
label define edscor50_lbl 0454 `"45.4"', add
label define edscor50_lbl 0455 `"45.5"', add
label define edscor50_lbl 0456 `"45.6"', add
label define edscor50_lbl 0457 `"45.7"', add
label define edscor50_lbl 0458 `"45.8"', add
label define edscor50_lbl 0459 `"45.9"', add
label define edscor50_lbl 0460 `"46"', add
label define edscor50_lbl 0461 `"46.1"', add
label define edscor50_lbl 0462 `"46.2"', add
label define edscor50_lbl 0463 `"46.3"', add
label define edscor50_lbl 0464 `"46.4"', add
label define edscor50_lbl 0465 `"46.5"', add
label define edscor50_lbl 0466 `"46.6"', add
label define edscor50_lbl 0467 `"46.7"', add
label define edscor50_lbl 0468 `"46.8"', add
label define edscor50_lbl 0469 `"46.9"', add
label define edscor50_lbl 0470 `"47"', add
label define edscor50_lbl 0471 `"47.1"', add
label define edscor50_lbl 0472 `"47.2"', add
label define edscor50_lbl 0473 `"47.3"', add
label define edscor50_lbl 0474 `"47.4"', add
label define edscor50_lbl 0475 `"47.5"', add
label define edscor50_lbl 0476 `"47.6"', add
label define edscor50_lbl 0477 `"47.7"', add
label define edscor50_lbl 0478 `"47.8"', add
label define edscor50_lbl 0479 `"47.9"', add
label define edscor50_lbl 0480 `"48"', add
label define edscor50_lbl 0481 `"48.1"', add
label define edscor50_lbl 0482 `"48.2"', add
label define edscor50_lbl 0483 `"48.3"', add
label define edscor50_lbl 0484 `"48.4"', add
label define edscor50_lbl 0485 `"48.5"', add
label define edscor50_lbl 0486 `"48.6"', add
label define edscor50_lbl 0487 `"48.7"', add
label define edscor50_lbl 0488 `"48.8"', add
label define edscor50_lbl 0489 `"48.9"', add
label define edscor50_lbl 0490 `"49"', add
label define edscor50_lbl 0491 `"49.1"', add
label define edscor50_lbl 0492 `"49.2"', add
label define edscor50_lbl 0493 `"49.3"', add
label define edscor50_lbl 0494 `"49.4"', add
label define edscor50_lbl 0495 `"49.5"', add
label define edscor50_lbl 0496 `"49.6"', add
label define edscor50_lbl 0497 `"49.7"', add
label define edscor50_lbl 0498 `"49.8"', add
label define edscor50_lbl 0499 `"49.9"', add
label define edscor50_lbl 0500 `"50"', add
label define edscor50_lbl 0501 `"50.1"', add
label define edscor50_lbl 0502 `"50.2"', add
label define edscor50_lbl 0503 `"50.3"', add
label define edscor50_lbl 0504 `"50.4"', add
label define edscor50_lbl 0505 `"50.5"', add
label define edscor50_lbl 0506 `"50.6"', add
label define edscor50_lbl 0507 `"50.7"', add
label define edscor50_lbl 0508 `"50.8"', add
label define edscor50_lbl 0509 `"50.9"', add
label define edscor50_lbl 0510 `"51"', add
label define edscor50_lbl 0511 `"51.1"', add
label define edscor50_lbl 0512 `"51.2"', add
label define edscor50_lbl 0513 `"51.3"', add
label define edscor50_lbl 0514 `"51.4"', add
label define edscor50_lbl 0515 `"51.5"', add
label define edscor50_lbl 0516 `"51.6"', add
label define edscor50_lbl 0517 `"51.7"', add
label define edscor50_lbl 0518 `"51.8"', add
label define edscor50_lbl 0519 `"51.9"', add
label define edscor50_lbl 0520 `"52"', add
label define edscor50_lbl 0521 `"52.1"', add
label define edscor50_lbl 0522 `"52.2"', add
label define edscor50_lbl 0523 `"52.3"', add
label define edscor50_lbl 0524 `"52.4"', add
label define edscor50_lbl 0525 `"52.5"', add
label define edscor50_lbl 0526 `"52.6"', add
label define edscor50_lbl 0527 `"52.7"', add
label define edscor50_lbl 0528 `"52.8"', add
label define edscor50_lbl 0529 `"52.9"', add
label define edscor50_lbl 0530 `"53"', add
label define edscor50_lbl 0531 `"53.1"', add
label define edscor50_lbl 0532 `"53.2"', add
label define edscor50_lbl 0533 `"53.3"', add
label define edscor50_lbl 0534 `"53.4"', add
label define edscor50_lbl 0535 `"53.5"', add
label define edscor50_lbl 0536 `"53.6"', add
label define edscor50_lbl 0537 `"53.7"', add
label define edscor50_lbl 0538 `"53.8"', add
label define edscor50_lbl 0539 `"53.9"', add
label define edscor50_lbl 0540 `"54"', add
label define edscor50_lbl 0541 `"54.1"', add
label define edscor50_lbl 0542 `"54.2"', add
label define edscor50_lbl 0543 `"54.3"', add
label define edscor50_lbl 0544 `"54.4"', add
label define edscor50_lbl 0545 `"54.5"', add
label define edscor50_lbl 0546 `"54.6"', add
label define edscor50_lbl 0547 `"54.7"', add
label define edscor50_lbl 0548 `"54.8"', add
label define edscor50_lbl 0549 `"54.9"', add
label define edscor50_lbl 0550 `"55"', add
label define edscor50_lbl 0551 `"55.1"', add
label define edscor50_lbl 0552 `"55.2"', add
label define edscor50_lbl 0553 `"55.3"', add
label define edscor50_lbl 0554 `"55.4"', add
label define edscor50_lbl 0555 `"55.5"', add
label define edscor50_lbl 0556 `"55.6"', add
label define edscor50_lbl 0557 `"55.7"', add
label define edscor50_lbl 0558 `"55.8"', add
label define edscor50_lbl 0559 `"55.9"', add
label define edscor50_lbl 0560 `"56"', add
label define edscor50_lbl 0561 `"56.1"', add
label define edscor50_lbl 0562 `"56.2"', add
label define edscor50_lbl 0563 `"56.3"', add
label define edscor50_lbl 0564 `"56.4"', add
label define edscor50_lbl 0565 `"56.5"', add
label define edscor50_lbl 0566 `"56.6"', add
label define edscor50_lbl 0567 `"56.7"', add
label define edscor50_lbl 0568 `"56.8"', add
label define edscor50_lbl 0569 `"56.9"', add
label define edscor50_lbl 0570 `"57"', add
label define edscor50_lbl 0571 `"57.1"', add
label define edscor50_lbl 0572 `"57.2"', add
label define edscor50_lbl 0573 `"57.3"', add
label define edscor50_lbl 0574 `"57.4"', add
label define edscor50_lbl 0575 `"57.5"', add
label define edscor50_lbl 0576 `"57.6"', add
label define edscor50_lbl 0577 `"57.7"', add
label define edscor50_lbl 0578 `"57.8"', add
label define edscor50_lbl 0579 `"57.9"', add
label define edscor50_lbl 0580 `"58"', add
label define edscor50_lbl 0581 `"58.1"', add
label define edscor50_lbl 0582 `"58.2"', add
label define edscor50_lbl 0583 `"58.3"', add
label define edscor50_lbl 0584 `"58.4"', add
label define edscor50_lbl 0585 `"58.5"', add
label define edscor50_lbl 0586 `"58.6"', add
label define edscor50_lbl 0587 `"58.7"', add
label define edscor50_lbl 0588 `"58.8"', add
label define edscor50_lbl 0589 `"58.9"', add
label define edscor50_lbl 0590 `"59"', add
label define edscor50_lbl 0591 `"59.1"', add
label define edscor50_lbl 0592 `"59.2"', add
label define edscor50_lbl 0593 `"59.3"', add
label define edscor50_lbl 0594 `"59.4"', add
label define edscor50_lbl 0595 `"59.5"', add
label define edscor50_lbl 0596 `"59.6"', add
label define edscor50_lbl 0597 `"59.7"', add
label define edscor50_lbl 0598 `"59.8"', add
label define edscor50_lbl 0599 `"59.9"', add
label define edscor50_lbl 0600 `"60"', add
label define edscor50_lbl 0601 `"60.1"', add
label define edscor50_lbl 0602 `"60.2"', add
label define edscor50_lbl 0603 `"60.3"', add
label define edscor50_lbl 0604 `"60.4"', add
label define edscor50_lbl 0605 `"60.5"', add
label define edscor50_lbl 0606 `"60.6"', add
label define edscor50_lbl 0607 `"60.7"', add
label define edscor50_lbl 0608 `"60.8"', add
label define edscor50_lbl 0609 `"60.9"', add
label define edscor50_lbl 0610 `"61"', add
label define edscor50_lbl 0611 `"61.1"', add
label define edscor50_lbl 0612 `"61.2"', add
label define edscor50_lbl 0613 `"61.3"', add
label define edscor50_lbl 0614 `"61.4"', add
label define edscor50_lbl 0615 `"61.5"', add
label define edscor50_lbl 0616 `"61.6"', add
label define edscor50_lbl 0617 `"61.7"', add
label define edscor50_lbl 0618 `"61.8"', add
label define edscor50_lbl 0619 `"61.9"', add
label define edscor50_lbl 0620 `"62"', add
label define edscor50_lbl 0621 `"62.1"', add
label define edscor50_lbl 0622 `"62.2"', add
label define edscor50_lbl 0623 `"62.3"', add
label define edscor50_lbl 0624 `"62.4"', add
label define edscor50_lbl 0625 `"62.5"', add
label define edscor50_lbl 0626 `"62.6"', add
label define edscor50_lbl 0627 `"62.7"', add
label define edscor50_lbl 0628 `"62.8"', add
label define edscor50_lbl 0629 `"62.9"', add
label define edscor50_lbl 0630 `"63"', add
label define edscor50_lbl 0631 `"63.1"', add
label define edscor50_lbl 0632 `"63.2"', add
label define edscor50_lbl 0633 `"63.3"', add
label define edscor50_lbl 0634 `"63.4"', add
label define edscor50_lbl 0635 `"63.5"', add
label define edscor50_lbl 0636 `"63.6"', add
label define edscor50_lbl 0637 `"63.7"', add
label define edscor50_lbl 0638 `"63.8"', add
label define edscor50_lbl 0639 `"63.9"', add
label define edscor50_lbl 0640 `"64"', add
label define edscor50_lbl 0641 `"64.1"', add
label define edscor50_lbl 0642 `"64.2"', add
label define edscor50_lbl 0643 `"64.3"', add
label define edscor50_lbl 0644 `"64.4"', add
label define edscor50_lbl 0645 `"64.5"', add
label define edscor50_lbl 0646 `"64.6"', add
label define edscor50_lbl 0647 `"64.7"', add
label define edscor50_lbl 0648 `"64.8"', add
label define edscor50_lbl 0649 `"64.9"', add
label define edscor50_lbl 0650 `"65"', add
label define edscor50_lbl 0651 `"65.1"', add
label define edscor50_lbl 0652 `"65.2"', add
label define edscor50_lbl 0653 `"65.3"', add
label define edscor50_lbl 0654 `"65.4"', add
label define edscor50_lbl 0655 `"65.5"', add
label define edscor50_lbl 0656 `"65.6"', add
label define edscor50_lbl 0657 `"65.7"', add
label define edscor50_lbl 0658 `"65.8"', add
label define edscor50_lbl 0659 `"65.9"', add
label define edscor50_lbl 0660 `"66"', add
label define edscor50_lbl 0661 `"66.1"', add
label define edscor50_lbl 0662 `"66.2"', add
label define edscor50_lbl 0663 `"66.3"', add
label define edscor50_lbl 0664 `"66.4"', add
label define edscor50_lbl 0665 `"66.5"', add
label define edscor50_lbl 0666 `"66.6"', add
label define edscor50_lbl 0667 `"66.7"', add
label define edscor50_lbl 0668 `"66.8"', add
label define edscor50_lbl 0669 `"66.9"', add
label define edscor50_lbl 0670 `"67"', add
label define edscor50_lbl 0671 `"67.1"', add
label define edscor50_lbl 0672 `"67.2"', add
label define edscor50_lbl 0673 `"67.3"', add
label define edscor50_lbl 0674 `"67.4"', add
label define edscor50_lbl 0675 `"67.5"', add
label define edscor50_lbl 0676 `"67.6"', add
label define edscor50_lbl 0677 `"67.7"', add
label define edscor50_lbl 0678 `"67.8"', add
label define edscor50_lbl 0679 `"67.9"', add
label define edscor50_lbl 0680 `"68"', add
label define edscor50_lbl 0681 `"68.1"', add
label define edscor50_lbl 0682 `"68.2"', add
label define edscor50_lbl 0683 `"68.3"', add
label define edscor50_lbl 0684 `"68.4"', add
label define edscor50_lbl 0685 `"68.5"', add
label define edscor50_lbl 0686 `"68.6"', add
label define edscor50_lbl 0687 `"68.7"', add
label define edscor50_lbl 0688 `"68.8"', add
label define edscor50_lbl 0689 `"68.9"', add
label define edscor50_lbl 0690 `"69"', add
label define edscor50_lbl 0691 `"69.1"', add
label define edscor50_lbl 0692 `"69.2"', add
label define edscor50_lbl 0693 `"69.3"', add
label define edscor50_lbl 0694 `"69.4"', add
label define edscor50_lbl 0695 `"69.5"', add
label define edscor50_lbl 0696 `"69.6"', add
label define edscor50_lbl 0697 `"69.7"', add
label define edscor50_lbl 0698 `"69.8"', add
label define edscor50_lbl 0699 `"69.9"', add
label define edscor50_lbl 0700 `"70"', add
label define edscor50_lbl 0701 `"70.1"', add
label define edscor50_lbl 0702 `"70.2"', add
label define edscor50_lbl 0703 `"70.3"', add
label define edscor50_lbl 0704 `"70.4"', add
label define edscor50_lbl 0705 `"70.5"', add
label define edscor50_lbl 0706 `"70.6"', add
label define edscor50_lbl 0707 `"70.7"', add
label define edscor50_lbl 0708 `"70.8"', add
label define edscor50_lbl 0709 `"70.9"', add
label define edscor50_lbl 0710 `"71"', add
label define edscor50_lbl 0711 `"71.1"', add
label define edscor50_lbl 0712 `"71.2"', add
label define edscor50_lbl 0713 `"71.3"', add
label define edscor50_lbl 0714 `"71.4"', add
label define edscor50_lbl 0715 `"71.5"', add
label define edscor50_lbl 0716 `"71.6"', add
label define edscor50_lbl 0717 `"71.7"', add
label define edscor50_lbl 0718 `"71.8"', add
label define edscor50_lbl 0719 `"71.9"', add
label define edscor50_lbl 0720 `"72"', add
label define edscor50_lbl 0721 `"72.1"', add
label define edscor50_lbl 0722 `"72.2"', add
label define edscor50_lbl 0723 `"72.3"', add
label define edscor50_lbl 0724 `"72.4"', add
label define edscor50_lbl 0725 `"72.5"', add
label define edscor50_lbl 0726 `"72.6"', add
label define edscor50_lbl 0727 `"72.7"', add
label define edscor50_lbl 0728 `"72.8"', add
label define edscor50_lbl 0729 `"72.9"', add
label define edscor50_lbl 0730 `"73"', add
label define edscor50_lbl 0731 `"73.1"', add
label define edscor50_lbl 0732 `"73.2"', add
label define edscor50_lbl 0733 `"73.3"', add
label define edscor50_lbl 0734 `"73.4"', add
label define edscor50_lbl 0735 `"73.5"', add
label define edscor50_lbl 0736 `"73.6"', add
label define edscor50_lbl 0737 `"73.7"', add
label define edscor50_lbl 0738 `"73.8"', add
label define edscor50_lbl 0739 `"73.9"', add
label define edscor50_lbl 0740 `"74"', add
label define edscor50_lbl 0741 `"74.1"', add
label define edscor50_lbl 0742 `"74.2"', add
label define edscor50_lbl 0743 `"74.3"', add
label define edscor50_lbl 0744 `"74.4"', add
label define edscor50_lbl 0745 `"74.5"', add
label define edscor50_lbl 0746 `"74.6"', add
label define edscor50_lbl 0747 `"74.7"', add
label define edscor50_lbl 0748 `"74.8"', add
label define edscor50_lbl 0749 `"74.9"', add
label define edscor50_lbl 0750 `"75"', add
label define edscor50_lbl 0751 `"75.1"', add
label define edscor50_lbl 0752 `"75.2"', add
label define edscor50_lbl 0753 `"75.3"', add
label define edscor50_lbl 0754 `"75.4"', add
label define edscor50_lbl 0755 `"75.5"', add
label define edscor50_lbl 0756 `"75.6"', add
label define edscor50_lbl 0757 `"75.7"', add
label define edscor50_lbl 0758 `"75.8"', add
label define edscor50_lbl 0759 `"75.9"', add
label define edscor50_lbl 0760 `"76"', add
label define edscor50_lbl 0761 `"76.1"', add
label define edscor50_lbl 0762 `"76.2"', add
label define edscor50_lbl 0763 `"76.3"', add
label define edscor50_lbl 0764 `"76.4"', add
label define edscor50_lbl 0765 `"76.5"', add
label define edscor50_lbl 0766 `"76.6"', add
label define edscor50_lbl 0767 `"76.7"', add
label define edscor50_lbl 0768 `"76.8"', add
label define edscor50_lbl 0769 `"76.9"', add
label define edscor50_lbl 0770 `"77"', add
label define edscor50_lbl 0771 `"77.1"', add
label define edscor50_lbl 0772 `"77.2"', add
label define edscor50_lbl 0773 `"77.3"', add
label define edscor50_lbl 0774 `"77.4"', add
label define edscor50_lbl 0775 `"77.5"', add
label define edscor50_lbl 0776 `"77.6"', add
label define edscor50_lbl 0777 `"77.7"', add
label define edscor50_lbl 0778 `"77.8"', add
label define edscor50_lbl 0779 `"77.9"', add
label define edscor50_lbl 0780 `"78"', add
label define edscor50_lbl 0781 `"78.1"', add
label define edscor50_lbl 0782 `"78.2"', add
label define edscor50_lbl 0783 `"78.3"', add
label define edscor50_lbl 0784 `"78.4"', add
label define edscor50_lbl 0785 `"78.5"', add
label define edscor50_lbl 0786 `"78.6"', add
label define edscor50_lbl 0787 `"78.7"', add
label define edscor50_lbl 0788 `"78.8"', add
label define edscor50_lbl 0789 `"78.9"', add
label define edscor50_lbl 0790 `"79"', add
label define edscor50_lbl 0791 `"79.1"', add
label define edscor50_lbl 0792 `"79.2"', add
label define edscor50_lbl 0793 `"79.3"', add
label define edscor50_lbl 0794 `"79.4"', add
label define edscor50_lbl 0795 `"79.5"', add
label define edscor50_lbl 0796 `"79.6"', add
label define edscor50_lbl 0797 `"79.7"', add
label define edscor50_lbl 0798 `"79.8"', add
label define edscor50_lbl 0799 `"79.9"', add
label define edscor50_lbl 0800 `"80"', add
label define edscor50_lbl 0801 `"80.1"', add
label define edscor50_lbl 0802 `"80.2"', add
label define edscor50_lbl 0803 `"80.3"', add
label define edscor50_lbl 0804 `"80.4"', add
label define edscor50_lbl 0805 `"80.5"', add
label define edscor50_lbl 0806 `"80.6"', add
label define edscor50_lbl 0807 `"80.7"', add
label define edscor50_lbl 0808 `"80.8"', add
label define edscor50_lbl 0809 `"80.9"', add
label define edscor50_lbl 0810 `"81"', add
label define edscor50_lbl 0811 `"81.1"', add
label define edscor50_lbl 0812 `"81.2"', add
label define edscor50_lbl 0813 `"81.3"', add
label define edscor50_lbl 0814 `"81.4"', add
label define edscor50_lbl 0815 `"81.5"', add
label define edscor50_lbl 0816 `"81.6"', add
label define edscor50_lbl 0817 `"81.7"', add
label define edscor50_lbl 0818 `"81.8"', add
label define edscor50_lbl 0819 `"81.9"', add
label define edscor50_lbl 0820 `"82"', add
label define edscor50_lbl 0821 `"82.1"', add
label define edscor50_lbl 0822 `"82.2"', add
label define edscor50_lbl 0823 `"82.3"', add
label define edscor50_lbl 0824 `"82.4"', add
label define edscor50_lbl 0825 `"82.5"', add
label define edscor50_lbl 0826 `"82.6"', add
label define edscor50_lbl 0827 `"82.7"', add
label define edscor50_lbl 0828 `"82.8"', add
label define edscor50_lbl 0829 `"82.9"', add
label define edscor50_lbl 0830 `"83"', add
label define edscor50_lbl 0831 `"83.1"', add
label define edscor50_lbl 0832 `"83.2"', add
label define edscor50_lbl 0833 `"83.3"', add
label define edscor50_lbl 0834 `"83.4"', add
label define edscor50_lbl 0835 `"83.5"', add
label define edscor50_lbl 0836 `"83.6"', add
label define edscor50_lbl 0837 `"83.7"', add
label define edscor50_lbl 0838 `"83.8"', add
label define edscor50_lbl 0839 `"83.9"', add
label define edscor50_lbl 0840 `"84"', add
label define edscor50_lbl 0841 `"84.1"', add
label define edscor50_lbl 0842 `"84.2"', add
label define edscor50_lbl 0843 `"84.3"', add
label define edscor50_lbl 0844 `"84.4"', add
label define edscor50_lbl 0845 `"84.5"', add
label define edscor50_lbl 0846 `"84.6"', add
label define edscor50_lbl 0847 `"84.7"', add
label define edscor50_lbl 0848 `"84.8"', add
label define edscor50_lbl 0849 `"84.9"', add
label define edscor50_lbl 0850 `"85"', add
label define edscor50_lbl 0851 `"85.1"', add
label define edscor50_lbl 0852 `"85.2"', add
label define edscor50_lbl 0853 `"85.3"', add
label define edscor50_lbl 0854 `"85.4"', add
label define edscor50_lbl 0855 `"85.5"', add
label define edscor50_lbl 0856 `"85.6"', add
label define edscor50_lbl 0857 `"85.7"', add
label define edscor50_lbl 0858 `"85.8"', add
label define edscor50_lbl 0859 `"85.9"', add
label define edscor50_lbl 0860 `"86"', add
label define edscor50_lbl 0861 `"86.1"', add
label define edscor50_lbl 0862 `"86.2"', add
label define edscor50_lbl 0863 `"86.3"', add
label define edscor50_lbl 0864 `"86.4"', add
label define edscor50_lbl 0865 `"86.5"', add
label define edscor50_lbl 0866 `"86.6"', add
label define edscor50_lbl 0867 `"86.7"', add
label define edscor50_lbl 0868 `"86.8"', add
label define edscor50_lbl 0869 `"86.9"', add
label define edscor50_lbl 0870 `"87"', add
label define edscor50_lbl 0871 `"87.1"', add
label define edscor50_lbl 0872 `"87.2"', add
label define edscor50_lbl 0873 `"87.3"', add
label define edscor50_lbl 0874 `"87.4"', add
label define edscor50_lbl 0875 `"87.5"', add
label define edscor50_lbl 0876 `"87.6"', add
label define edscor50_lbl 0877 `"87.7"', add
label define edscor50_lbl 0878 `"87.8"', add
label define edscor50_lbl 0879 `"87.9"', add
label define edscor50_lbl 0880 `"88"', add
label define edscor50_lbl 0881 `"88.1"', add
label define edscor50_lbl 0882 `"88.2"', add
label define edscor50_lbl 0883 `"88.3"', add
label define edscor50_lbl 0884 `"88.4"', add
label define edscor50_lbl 0885 `"88.5"', add
label define edscor50_lbl 0886 `"88.6"', add
label define edscor50_lbl 0887 `"88.7"', add
label define edscor50_lbl 0888 `"88.8"', add
label define edscor50_lbl 0889 `"88.9"', add
label define edscor50_lbl 0890 `"89"', add
label define edscor50_lbl 0891 `"89.1"', add
label define edscor50_lbl 0892 `"89.2"', add
label define edscor50_lbl 0893 `"89.3"', add
label define edscor50_lbl 0894 `"89.4"', add
label define edscor50_lbl 0895 `"89.5"', add
label define edscor50_lbl 0896 `"89.6"', add
label define edscor50_lbl 0897 `"89.7"', add
label define edscor50_lbl 0898 `"89.8"', add
label define edscor50_lbl 0899 `"89.9"', add
label define edscor50_lbl 0900 `"90"', add
label define edscor50_lbl 0901 `"90.1"', add
label define edscor50_lbl 0902 `"90.2"', add
label define edscor50_lbl 0903 `"90.3"', add
label define edscor50_lbl 0904 `"90.4"', add
label define edscor50_lbl 0905 `"90.5"', add
label define edscor50_lbl 0906 `"90.6"', add
label define edscor50_lbl 0907 `"90.7"', add
label define edscor50_lbl 0908 `"90.8"', add
label define edscor50_lbl 0909 `"90.9"', add
label define edscor50_lbl 0910 `"91"', add
label define edscor50_lbl 0911 `"91.1"', add
label define edscor50_lbl 0912 `"91.2"', add
label define edscor50_lbl 0913 `"91.3"', add
label define edscor50_lbl 0914 `"91.4"', add
label define edscor50_lbl 0915 `"91.5"', add
label define edscor50_lbl 0916 `"91.6"', add
label define edscor50_lbl 0917 `"91.7"', add
label define edscor50_lbl 0918 `"91.8"', add
label define edscor50_lbl 0919 `"91.9"', add
label define edscor50_lbl 0920 `"92"', add
label define edscor50_lbl 0921 `"92.1"', add
label define edscor50_lbl 0922 `"92.2"', add
label define edscor50_lbl 0923 `"92.3"', add
label define edscor50_lbl 0924 `"92.4"', add
label define edscor50_lbl 0925 `"92.5"', add
label define edscor50_lbl 0926 `"92.6"', add
label define edscor50_lbl 0927 `"92.7"', add
label define edscor50_lbl 0928 `"92.8"', add
label define edscor50_lbl 0929 `"92.9"', add
label define edscor50_lbl 0930 `"93"', add
label define edscor50_lbl 0931 `"93.1"', add
label define edscor50_lbl 0932 `"93.2"', add
label define edscor50_lbl 0933 `"93.3"', add
label define edscor50_lbl 0934 `"93.4"', add
label define edscor50_lbl 0935 `"93.5"', add
label define edscor50_lbl 0936 `"93.6"', add
label define edscor50_lbl 0937 `"93.7"', add
label define edscor50_lbl 0938 `"93.8"', add
label define edscor50_lbl 0939 `"93.9"', add
label define edscor50_lbl 0940 `"94"', add
label define edscor50_lbl 0941 `"94.1"', add
label define edscor50_lbl 0942 `"94.2"', add
label define edscor50_lbl 0943 `"94.3"', add
label define edscor50_lbl 0944 `"94.4"', add
label define edscor50_lbl 0945 `"94.5"', add
label define edscor50_lbl 0946 `"94.6"', add
label define edscor50_lbl 0947 `"94.7"', add
label define edscor50_lbl 0948 `"94.8"', add
label define edscor50_lbl 0949 `"94.9"', add
label define edscor50_lbl 0950 `"95"', add
label define edscor50_lbl 0951 `"95.1"', add
label define edscor50_lbl 0952 `"95.2"', add
label define edscor50_lbl 0953 `"95.3"', add
label define edscor50_lbl 0954 `"95.4"', add
label define edscor50_lbl 0955 `"95.5"', add
label define edscor50_lbl 0956 `"95.6"', add
label define edscor50_lbl 0957 `"95.7"', add
label define edscor50_lbl 0958 `"95.8"', add
label define edscor50_lbl 0959 `"95.9"', add
label define edscor50_lbl 0960 `"96"', add
label define edscor50_lbl 0961 `"96.1"', add
label define edscor50_lbl 0962 `"96.2"', add
label define edscor50_lbl 0963 `"96.3"', add
label define edscor50_lbl 0964 `"96.4"', add
label define edscor50_lbl 0965 `"96.5"', add
label define edscor50_lbl 0966 `"96.6"', add
label define edscor50_lbl 0967 `"96.7"', add
label define edscor50_lbl 0968 `"96.8"', add
label define edscor50_lbl 0969 `"96.9"', add
label define edscor50_lbl 0970 `"97"', add
label define edscor50_lbl 0971 `"97.1"', add
label define edscor50_lbl 0972 `"97.2"', add
label define edscor50_lbl 0973 `"97.3"', add
label define edscor50_lbl 0974 `"97.4"', add
label define edscor50_lbl 0975 `"97.5"', add
label define edscor50_lbl 0976 `"97.6"', add
label define edscor50_lbl 0977 `"97.7"', add
label define edscor50_lbl 0978 `"97.8"', add
label define edscor50_lbl 0979 `"97.9"', add
label define edscor50_lbl 0980 `"98"', add
label define edscor50_lbl 0981 `"98.1"', add
label define edscor50_lbl 0982 `"98.2"', add
label define edscor50_lbl 0983 `"98.3"', add
label define edscor50_lbl 0984 `"98.4"', add
label define edscor50_lbl 0985 `"98.5"', add
label define edscor50_lbl 0986 `"98.6"', add
label define edscor50_lbl 0987 `"98.7"', add
label define edscor50_lbl 0988 `"98.8"', add
label define edscor50_lbl 0989 `"98.9"', add
label define edscor50_lbl 0990 `"99"', add
label define edscor50_lbl 0991 `"99.1"', add
label define edscor50_lbl 0992 `"99.2"', add
label define edscor50_lbl 0993 `"99.3"', add
label define edscor50_lbl 0994 `"99.4"', add
label define edscor50_lbl 0995 `"99.5"', add
label define edscor50_lbl 0996 `"99.6"', add
label define edscor50_lbl 0997 `"99.7"', add
label define edscor50_lbl 0998 `"99.8"', add
label define edscor50_lbl 0999 `"99.9"', add
label define edscor50_lbl 1000 `"100"', add
label define edscor50_lbl 9999 `"N/A"', add
label values edscor50 edscor50_lbl

label define npboss50_lbl 0000 `"0000"'
label define npboss50_lbl 0001 `"0001"', add
label define npboss50_lbl 0002 `"0002"', add
label define npboss50_lbl 0003 `"0003"', add
label define npboss50_lbl 0004 `"0004"', add
label define npboss50_lbl 0005 `"0005"', add
label define npboss50_lbl 0006 `"0006"', add
label define npboss50_lbl 0007 `"0007"', add
label define npboss50_lbl 0008 `"0008"', add
label define npboss50_lbl 0009 `"0009"', add
label define npboss50_lbl 0010 `"0010"', add
label define npboss50_lbl 0011 `"0011"', add
label define npboss50_lbl 0012 `"0012"', add
label define npboss50_lbl 0013 `"0013"', add
label define npboss50_lbl 0014 `"0014"', add
label define npboss50_lbl 0015 `"0015"', add
label define npboss50_lbl 0016 `"0016"', add
label define npboss50_lbl 0017 `"0017"', add
label define npboss50_lbl 0018 `"0018"', add
label define npboss50_lbl 0019 `"0019"', add
label define npboss50_lbl 0020 `"0020"', add
label define npboss50_lbl 0021 `"0021"', add
label define npboss50_lbl 0022 `"0022"', add
label define npboss50_lbl 0023 `"0023"', add
label define npboss50_lbl 0024 `"0024"', add
label define npboss50_lbl 0025 `"0025"', add
label define npboss50_lbl 0026 `"0026"', add
label define npboss50_lbl 0027 `"0027"', add
label define npboss50_lbl 0028 `"0028"', add
label define npboss50_lbl 0029 `"0029"', add
label define npboss50_lbl 0030 `"0030"', add
label define npboss50_lbl 0031 `"0031"', add
label define npboss50_lbl 0032 `"0032"', add
label define npboss50_lbl 0033 `"0033"', add
label define npboss50_lbl 0034 `"0034"', add
label define npboss50_lbl 0035 `"0035"', add
label define npboss50_lbl 0036 `"0036"', add
label define npboss50_lbl 0037 `"0037"', add
label define npboss50_lbl 0038 `"0038"', add
label define npboss50_lbl 0039 `"0039"', add
label define npboss50_lbl 0040 `"0040"', add
label define npboss50_lbl 0041 `"0041"', add
label define npboss50_lbl 0042 `"0042"', add
label define npboss50_lbl 0043 `"0043"', add
label define npboss50_lbl 0044 `"0044"', add
label define npboss50_lbl 0045 `"0045"', add
label define npboss50_lbl 0046 `"0046"', add
label define npboss50_lbl 0047 `"0047"', add
label define npboss50_lbl 0048 `"0048"', add
label define npboss50_lbl 0049 `"0049"', add
label define npboss50_lbl 0050 `"0050"', add
label define npboss50_lbl 0051 `"0051"', add
label define npboss50_lbl 0052 `"0052"', add
label define npboss50_lbl 0053 `"0053"', add
label define npboss50_lbl 0054 `"0054"', add
label define npboss50_lbl 0055 `"0055"', add
label define npboss50_lbl 0056 `"0056"', add
label define npboss50_lbl 0057 `"0057"', add
label define npboss50_lbl 0058 `"0058"', add
label define npboss50_lbl 0059 `"0059"', add
label define npboss50_lbl 0060 `"0060"', add
label define npboss50_lbl 0061 `"0061"', add
label define npboss50_lbl 0062 `"0062"', add
label define npboss50_lbl 0063 `"0063"', add
label define npboss50_lbl 0064 `"0064"', add
label define npboss50_lbl 0065 `"0065"', add
label define npboss50_lbl 0066 `"0066"', add
label define npboss50_lbl 0067 `"0067"', add
label define npboss50_lbl 0068 `"0068"', add
label define npboss50_lbl 0069 `"0069"', add
label define npboss50_lbl 0070 `"0070"', add
label define npboss50_lbl 0071 `"0071"', add
label define npboss50_lbl 0072 `"0072"', add
label define npboss50_lbl 0073 `"0073"', add
label define npboss50_lbl 0074 `"0074"', add
label define npboss50_lbl 0075 `"0075"', add
label define npboss50_lbl 0076 `"0076"', add
label define npboss50_lbl 0077 `"0077"', add
label define npboss50_lbl 0078 `"0078"', add
label define npboss50_lbl 0079 `"0079"', add
label define npboss50_lbl 0080 `"0080"', add
label define npboss50_lbl 0081 `"0081"', add
label define npboss50_lbl 0082 `"0082"', add
label define npboss50_lbl 0083 `"0083"', add
label define npboss50_lbl 0084 `"0084"', add
label define npboss50_lbl 0085 `"0085"', add
label define npboss50_lbl 0086 `"0086"', add
label define npboss50_lbl 0087 `"0087"', add
label define npboss50_lbl 0088 `"0088"', add
label define npboss50_lbl 0089 `"0089"', add
label define npboss50_lbl 0090 `"0090"', add
label define npboss50_lbl 0091 `"0091"', add
label define npboss50_lbl 0092 `"0092"', add
label define npboss50_lbl 0093 `"0093"', add
label define npboss50_lbl 0094 `"0094"', add
label define npboss50_lbl 0095 `"0095"', add
label define npboss50_lbl 0096 `"0096"', add
label define npboss50_lbl 0097 `"0097"', add
label define npboss50_lbl 0098 `"0098"', add
label define npboss50_lbl 0099 `"0099"', add
label define npboss50_lbl 0100 `"0100"', add
label define npboss50_lbl 0101 `"0101"', add
label define npboss50_lbl 0102 `"0102"', add
label define npboss50_lbl 0103 `"0103"', add
label define npboss50_lbl 0104 `"0104"', add
label define npboss50_lbl 0105 `"0105"', add
label define npboss50_lbl 0106 `"0106"', add
label define npboss50_lbl 0107 `"0107"', add
label define npboss50_lbl 0108 `"0108"', add
label define npboss50_lbl 0109 `"0109"', add
label define npboss50_lbl 0110 `"0110"', add
label define npboss50_lbl 0111 `"0111"', add
label define npboss50_lbl 0112 `"0112"', add
label define npboss50_lbl 0113 `"0113"', add
label define npboss50_lbl 0114 `"0114"', add
label define npboss50_lbl 0115 `"0115"', add
label define npboss50_lbl 0116 `"0116"', add
label define npboss50_lbl 0117 `"0117"', add
label define npboss50_lbl 0118 `"0118"', add
label define npboss50_lbl 0119 `"0119"', add
label define npboss50_lbl 0120 `"0120"', add
label define npboss50_lbl 0121 `"0121"', add
label define npboss50_lbl 0122 `"0122"', add
label define npboss50_lbl 0123 `"0123"', add
label define npboss50_lbl 0124 `"0124"', add
label define npboss50_lbl 0125 `"0125"', add
label define npboss50_lbl 0126 `"0126"', add
label define npboss50_lbl 0127 `"0127"', add
label define npboss50_lbl 0128 `"0128"', add
label define npboss50_lbl 0129 `"0129"', add
label define npboss50_lbl 0130 `"0130"', add
label define npboss50_lbl 0131 `"0131"', add
label define npboss50_lbl 0132 `"0132"', add
label define npboss50_lbl 0133 `"0133"', add
label define npboss50_lbl 0134 `"0134"', add
label define npboss50_lbl 0135 `"0135"', add
label define npboss50_lbl 0136 `"0136"', add
label define npboss50_lbl 0137 `"0137"', add
label define npboss50_lbl 0138 `"0138"', add
label define npboss50_lbl 0139 `"0139"', add
label define npboss50_lbl 0140 `"0140"', add
label define npboss50_lbl 0141 `"0141"', add
label define npboss50_lbl 0142 `"0142"', add
label define npboss50_lbl 0143 `"0143"', add
label define npboss50_lbl 0144 `"0144"', add
label define npboss50_lbl 0145 `"0145"', add
label define npboss50_lbl 0146 `"0146"', add
label define npboss50_lbl 0147 `"0147"', add
label define npboss50_lbl 0148 `"0148"', add
label define npboss50_lbl 0149 `"0149"', add
label define npboss50_lbl 0150 `"0150"', add
label define npboss50_lbl 0151 `"0151"', add
label define npboss50_lbl 0152 `"0152"', add
label define npboss50_lbl 0153 `"0153"', add
label define npboss50_lbl 0154 `"0154"', add
label define npboss50_lbl 0155 `"0155"', add
label define npboss50_lbl 0156 `"0156"', add
label define npboss50_lbl 0157 `"0157"', add
label define npboss50_lbl 0158 `"0158"', add
label define npboss50_lbl 0159 `"0159"', add
label define npboss50_lbl 0160 `"0160"', add
label define npboss50_lbl 0161 `"0161"', add
label define npboss50_lbl 0162 `"0162"', add
label define npboss50_lbl 0163 `"0163"', add
label define npboss50_lbl 0164 `"0164"', add
label define npboss50_lbl 0165 `"0165"', add
label define npboss50_lbl 0166 `"0166"', add
label define npboss50_lbl 0167 `"0167"', add
label define npboss50_lbl 0168 `"0168"', add
label define npboss50_lbl 0169 `"0169"', add
label define npboss50_lbl 0170 `"0170"', add
label define npboss50_lbl 0171 `"0171"', add
label define npboss50_lbl 0172 `"0172"', add
label define npboss50_lbl 0173 `"0173"', add
label define npboss50_lbl 0174 `"0174"', add
label define npboss50_lbl 0175 `"0175"', add
label define npboss50_lbl 0176 `"0176"', add
label define npboss50_lbl 0177 `"0177"', add
label define npboss50_lbl 0178 `"0178"', add
label define npboss50_lbl 0179 `"0179"', add
label define npboss50_lbl 0180 `"0180"', add
label define npboss50_lbl 0181 `"0181"', add
label define npboss50_lbl 0182 `"0182"', add
label define npboss50_lbl 0183 `"0183"', add
label define npboss50_lbl 0184 `"0184"', add
label define npboss50_lbl 0185 `"0185"', add
label define npboss50_lbl 0186 `"0186"', add
label define npboss50_lbl 0187 `"0187"', add
label define npboss50_lbl 0188 `"0188"', add
label define npboss50_lbl 0189 `"0189"', add
label define npboss50_lbl 0190 `"0190"', add
label define npboss50_lbl 0191 `"0191"', add
label define npboss50_lbl 0192 `"0192"', add
label define npboss50_lbl 0193 `"0193"', add
label define npboss50_lbl 0194 `"0194"', add
label define npboss50_lbl 0195 `"0195"', add
label define npboss50_lbl 0196 `"0196"', add
label define npboss50_lbl 0197 `"0197"', add
label define npboss50_lbl 0198 `"0198"', add
label define npboss50_lbl 0199 `"0199"', add
label define npboss50_lbl 0200 `"0200"', add
label define npboss50_lbl 0201 `"0201"', add
label define npboss50_lbl 0202 `"0202"', add
label define npboss50_lbl 0203 `"0203"', add
label define npboss50_lbl 0204 `"0204"', add
label define npboss50_lbl 0205 `"0205"', add
label define npboss50_lbl 0206 `"0206"', add
label define npboss50_lbl 0207 `"0207"', add
label define npboss50_lbl 0208 `"0208"', add
label define npboss50_lbl 0209 `"0209"', add
label define npboss50_lbl 0210 `"0210"', add
label define npboss50_lbl 0211 `"0211"', add
label define npboss50_lbl 0212 `"0212"', add
label define npboss50_lbl 0213 `"0213"', add
label define npboss50_lbl 0214 `"0214"', add
label define npboss50_lbl 0215 `"0215"', add
label define npboss50_lbl 0216 `"0216"', add
label define npboss50_lbl 0217 `"0217"', add
label define npboss50_lbl 0218 `"0218"', add
label define npboss50_lbl 0219 `"0219"', add
label define npboss50_lbl 0220 `"0220"', add
label define npboss50_lbl 0221 `"0221"', add
label define npboss50_lbl 0222 `"0222"', add
label define npboss50_lbl 0223 `"0223"', add
label define npboss50_lbl 0224 `"0224"', add
label define npboss50_lbl 0225 `"0225"', add
label define npboss50_lbl 0226 `"0226"', add
label define npboss50_lbl 0227 `"0227"', add
label define npboss50_lbl 0228 `"0228"', add
label define npboss50_lbl 0229 `"0229"', add
label define npboss50_lbl 0230 `"0230"', add
label define npboss50_lbl 0231 `"0231"', add
label define npboss50_lbl 0232 `"0232"', add
label define npboss50_lbl 0233 `"0233"', add
label define npboss50_lbl 0234 `"0234"', add
label define npboss50_lbl 0235 `"0235"', add
label define npboss50_lbl 0236 `"0236"', add
label define npboss50_lbl 0237 `"0237"', add
label define npboss50_lbl 0238 `"0238"', add
label define npboss50_lbl 0239 `"0239"', add
label define npboss50_lbl 0240 `"0240"', add
label define npboss50_lbl 0241 `"0241"', add
label define npboss50_lbl 0242 `"0242"', add
label define npboss50_lbl 0243 `"0243"', add
label define npboss50_lbl 0244 `"0244"', add
label define npboss50_lbl 0245 `"0245"', add
label define npboss50_lbl 0246 `"0246"', add
label define npboss50_lbl 0247 `"0247"', add
label define npboss50_lbl 0248 `"0248"', add
label define npboss50_lbl 0249 `"0249"', add
label define npboss50_lbl 0250 `"0250"', add
label define npboss50_lbl 0251 `"0251"', add
label define npboss50_lbl 0252 `"0252"', add
label define npboss50_lbl 0253 `"0253"', add
label define npboss50_lbl 0254 `"0254"', add
label define npboss50_lbl 0255 `"0255"', add
label define npboss50_lbl 0256 `"0256"', add
label define npboss50_lbl 0257 `"0257"', add
label define npboss50_lbl 0258 `"0258"', add
label define npboss50_lbl 0259 `"0259"', add
label define npboss50_lbl 0260 `"0260"', add
label define npboss50_lbl 0261 `"0261"', add
label define npboss50_lbl 0262 `"0262"', add
label define npboss50_lbl 0263 `"0263"', add
label define npboss50_lbl 0264 `"0264"', add
label define npboss50_lbl 0265 `"0265"', add
label define npboss50_lbl 0266 `"0266"', add
label define npboss50_lbl 0267 `"0267"', add
label define npboss50_lbl 0268 `"0268"', add
label define npboss50_lbl 0269 `"0269"', add
label define npboss50_lbl 0270 `"0270"', add
label define npboss50_lbl 0271 `"0271"', add
label define npboss50_lbl 0272 `"0272"', add
label define npboss50_lbl 0273 `"0273"', add
label define npboss50_lbl 0274 `"0274"', add
label define npboss50_lbl 0275 `"0275"', add
label define npboss50_lbl 0276 `"0276"', add
label define npboss50_lbl 0277 `"0277"', add
label define npboss50_lbl 0278 `"0278"', add
label define npboss50_lbl 0279 `"0279"', add
label define npboss50_lbl 0280 `"0280"', add
label define npboss50_lbl 0281 `"0281"', add
label define npboss50_lbl 0282 `"0282"', add
label define npboss50_lbl 0283 `"0283"', add
label define npboss50_lbl 0284 `"0284"', add
label define npboss50_lbl 0285 `"0285"', add
label define npboss50_lbl 0286 `"0286"', add
label define npboss50_lbl 0287 `"0287"', add
label define npboss50_lbl 0288 `"0288"', add
label define npboss50_lbl 0289 `"0289"', add
label define npboss50_lbl 0290 `"0290"', add
label define npboss50_lbl 0291 `"0291"', add
label define npboss50_lbl 0292 `"0292"', add
label define npboss50_lbl 0293 `"0293"', add
label define npboss50_lbl 0294 `"0294"', add
label define npboss50_lbl 0295 `"0295"', add
label define npboss50_lbl 0296 `"0296"', add
label define npboss50_lbl 0297 `"0297"', add
label define npboss50_lbl 0298 `"0298"', add
label define npboss50_lbl 0299 `"0299"', add
label define npboss50_lbl 0300 `"0300"', add
label define npboss50_lbl 0301 `"0301"', add
label define npboss50_lbl 0302 `"0302"', add
label define npboss50_lbl 0303 `"0303"', add
label define npboss50_lbl 0304 `"0304"', add
label define npboss50_lbl 0305 `"0305"', add
label define npboss50_lbl 0306 `"0306"', add
label define npboss50_lbl 0307 `"0307"', add
label define npboss50_lbl 0308 `"0308"', add
label define npboss50_lbl 0309 `"0309"', add
label define npboss50_lbl 0310 `"0310"', add
label define npboss50_lbl 0311 `"0311"', add
label define npboss50_lbl 0312 `"0312"', add
label define npboss50_lbl 0313 `"0313"', add
label define npboss50_lbl 0314 `"0314"', add
label define npboss50_lbl 0315 `"0315"', add
label define npboss50_lbl 0316 `"0316"', add
label define npboss50_lbl 0317 `"0317"', add
label define npboss50_lbl 0318 `"0318"', add
label define npboss50_lbl 0319 `"0319"', add
label define npboss50_lbl 0320 `"0320"', add
label define npboss50_lbl 0321 `"0321"', add
label define npboss50_lbl 0322 `"0322"', add
label define npboss50_lbl 0323 `"0323"', add
label define npboss50_lbl 0324 `"0324"', add
label define npboss50_lbl 0325 `"0325"', add
label define npboss50_lbl 0326 `"0326"', add
label define npboss50_lbl 0327 `"0327"', add
label define npboss50_lbl 0328 `"0328"', add
label define npboss50_lbl 0329 `"0329"', add
label define npboss50_lbl 0330 `"0330"', add
label define npboss50_lbl 0331 `"0331"', add
label define npboss50_lbl 0332 `"0332"', add
label define npboss50_lbl 0333 `"0333"', add
label define npboss50_lbl 0334 `"0334"', add
label define npboss50_lbl 0335 `"0335"', add
label define npboss50_lbl 0336 `"0336"', add
label define npboss50_lbl 0337 `"0337"', add
label define npboss50_lbl 0338 `"0338"', add
label define npboss50_lbl 0339 `"0339"', add
label define npboss50_lbl 0340 `"0340"', add
label define npboss50_lbl 0341 `"0341"', add
label define npboss50_lbl 0342 `"0342"', add
label define npboss50_lbl 0343 `"0343"', add
label define npboss50_lbl 0344 `"0344"', add
label define npboss50_lbl 0345 `"0345"', add
label define npboss50_lbl 0346 `"0346"', add
label define npboss50_lbl 0347 `"0347"', add
label define npboss50_lbl 0348 `"0348"', add
label define npboss50_lbl 0349 `"0349"', add
label define npboss50_lbl 0350 `"0350"', add
label define npboss50_lbl 0351 `"0351"', add
label define npboss50_lbl 0352 `"0352"', add
label define npboss50_lbl 0353 `"0353"', add
label define npboss50_lbl 0354 `"0354"', add
label define npboss50_lbl 0355 `"0355"', add
label define npboss50_lbl 0356 `"0356"', add
label define npboss50_lbl 0357 `"0357"', add
label define npboss50_lbl 0358 `"0358"', add
label define npboss50_lbl 0359 `"0359"', add
label define npboss50_lbl 0360 `"0360"', add
label define npboss50_lbl 0361 `"0361"', add
label define npboss50_lbl 0362 `"0362"', add
label define npboss50_lbl 0363 `"0363"', add
label define npboss50_lbl 0364 `"0364"', add
label define npboss50_lbl 0365 `"0365"', add
label define npboss50_lbl 0366 `"0366"', add
label define npboss50_lbl 0367 `"0367"', add
label define npboss50_lbl 0368 `"0368"', add
label define npboss50_lbl 0369 `"0369"', add
label define npboss50_lbl 0370 `"0370"', add
label define npboss50_lbl 0371 `"0371"', add
label define npboss50_lbl 0372 `"0372"', add
label define npboss50_lbl 0373 `"0373"', add
label define npboss50_lbl 0374 `"0374"', add
label define npboss50_lbl 0375 `"0375"', add
label define npboss50_lbl 0376 `"0376"', add
label define npboss50_lbl 0377 `"0377"', add
label define npboss50_lbl 0378 `"0378"', add
label define npboss50_lbl 0379 `"0379"', add
label define npboss50_lbl 0380 `"0380"', add
label define npboss50_lbl 0381 `"0381"', add
label define npboss50_lbl 0382 `"0382"', add
label define npboss50_lbl 0383 `"0383"', add
label define npboss50_lbl 0384 `"0384"', add
label define npboss50_lbl 0385 `"0385"', add
label define npboss50_lbl 0386 `"0386"', add
label define npboss50_lbl 0387 `"0387"', add
label define npboss50_lbl 0388 `"0388"', add
label define npboss50_lbl 0389 `"0389"', add
label define npboss50_lbl 0390 `"0390"', add
label define npboss50_lbl 0391 `"0391"', add
label define npboss50_lbl 0392 `"0392"', add
label define npboss50_lbl 0393 `"0393"', add
label define npboss50_lbl 0394 `"0394"', add
label define npboss50_lbl 0395 `"0395"', add
label define npboss50_lbl 0396 `"0396"', add
label define npboss50_lbl 0397 `"0397"', add
label define npboss50_lbl 0398 `"0398"', add
label define npboss50_lbl 0399 `"0399"', add
label define npboss50_lbl 0400 `"0400"', add
label define npboss50_lbl 0401 `"0401"', add
label define npboss50_lbl 0402 `"0402"', add
label define npboss50_lbl 0403 `"0403"', add
label define npboss50_lbl 0404 `"0404"', add
label define npboss50_lbl 0405 `"0405"', add
label define npboss50_lbl 0406 `"0406"', add
label define npboss50_lbl 0407 `"0407"', add
label define npboss50_lbl 0408 `"0408"', add
label define npboss50_lbl 0409 `"0409"', add
label define npboss50_lbl 0410 `"0410"', add
label define npboss50_lbl 0411 `"0411"', add
label define npboss50_lbl 0412 `"0412"', add
label define npboss50_lbl 0413 `"0413"', add
label define npboss50_lbl 0414 `"0414"', add
label define npboss50_lbl 0415 `"0415"', add
label define npboss50_lbl 0416 `"0416"', add
label define npboss50_lbl 0417 `"0417"', add
label define npboss50_lbl 0418 `"0418"', add
label define npboss50_lbl 0419 `"0419"', add
label define npboss50_lbl 0420 `"0420"', add
label define npboss50_lbl 0421 `"0421"', add
label define npboss50_lbl 0422 `"0422"', add
label define npboss50_lbl 0423 `"0423"', add
label define npboss50_lbl 0424 `"0424"', add
label define npboss50_lbl 0425 `"0425"', add
label define npboss50_lbl 0426 `"0426"', add
label define npboss50_lbl 0427 `"0427"', add
label define npboss50_lbl 0428 `"0428"', add
label define npboss50_lbl 0429 `"0429"', add
label define npboss50_lbl 0430 `"0430"', add
label define npboss50_lbl 0431 `"0431"', add
label define npboss50_lbl 0432 `"0432"', add
label define npboss50_lbl 0433 `"0433"', add
label define npboss50_lbl 0434 `"0434"', add
label define npboss50_lbl 0435 `"0435"', add
label define npboss50_lbl 0436 `"0436"', add
label define npboss50_lbl 0437 `"0437"', add
label define npboss50_lbl 0438 `"0438"', add
label define npboss50_lbl 0439 `"0439"', add
label define npboss50_lbl 0440 `"0440"', add
label define npboss50_lbl 0441 `"0441"', add
label define npboss50_lbl 0442 `"0442"', add
label define npboss50_lbl 0443 `"0443"', add
label define npboss50_lbl 0444 `"0444"', add
label define npboss50_lbl 0445 `"0445"', add
label define npboss50_lbl 0446 `"0446"', add
label define npboss50_lbl 0447 `"0447"', add
label define npboss50_lbl 0448 `"0448"', add
label define npboss50_lbl 0449 `"0449"', add
label define npboss50_lbl 0450 `"0450"', add
label define npboss50_lbl 0451 `"0451"', add
label define npboss50_lbl 0452 `"0452"', add
label define npboss50_lbl 0453 `"0453"', add
label define npboss50_lbl 0454 `"0454"', add
label define npboss50_lbl 0455 `"0455"', add
label define npboss50_lbl 0456 `"0456"', add
label define npboss50_lbl 0457 `"0457"', add
label define npboss50_lbl 0458 `"0458"', add
label define npboss50_lbl 0459 `"0459"', add
label define npboss50_lbl 0460 `"0460"', add
label define npboss50_lbl 0461 `"0461"', add
label define npboss50_lbl 0462 `"0462"', add
label define npboss50_lbl 0463 `"0463"', add
label define npboss50_lbl 0464 `"0464"', add
label define npboss50_lbl 0465 `"0465"', add
label define npboss50_lbl 0466 `"0466"', add
label define npboss50_lbl 0467 `"0467"', add
label define npboss50_lbl 0468 `"0468"', add
label define npboss50_lbl 0469 `"0469"', add
label define npboss50_lbl 0470 `"0470"', add
label define npboss50_lbl 0471 `"0471"', add
label define npboss50_lbl 0472 `"0472"', add
label define npboss50_lbl 0473 `"0473"', add
label define npboss50_lbl 0474 `"0474"', add
label define npboss50_lbl 0475 `"0475"', add
label define npboss50_lbl 0476 `"0476"', add
label define npboss50_lbl 0477 `"0477"', add
label define npboss50_lbl 0478 `"0478"', add
label define npboss50_lbl 0479 `"0479"', add
label define npboss50_lbl 0480 `"0480"', add
label define npboss50_lbl 0481 `"0481"', add
label define npboss50_lbl 0482 `"0482"', add
label define npboss50_lbl 0483 `"0483"', add
label define npboss50_lbl 0484 `"0484"', add
label define npboss50_lbl 0485 `"0485"', add
label define npboss50_lbl 0486 `"0486"', add
label define npboss50_lbl 0487 `"0487"', add
label define npboss50_lbl 0488 `"0488"', add
label define npboss50_lbl 0489 `"0489"', add
label define npboss50_lbl 0490 `"0490"', add
label define npboss50_lbl 0491 `"0491"', add
label define npboss50_lbl 0492 `"0492"', add
label define npboss50_lbl 0493 `"0493"', add
label define npboss50_lbl 0494 `"0494"', add
label define npboss50_lbl 0495 `"0495"', add
label define npboss50_lbl 0496 `"0496"', add
label define npboss50_lbl 0497 `"0497"', add
label define npboss50_lbl 0498 `"0498"', add
label define npboss50_lbl 0499 `"0499"', add
label define npboss50_lbl 0500 `"0500"', add
label define npboss50_lbl 0501 `"0501"', add
label define npboss50_lbl 0502 `"0502"', add
label define npboss50_lbl 0503 `"0503"', add
label define npboss50_lbl 0504 `"0504"', add
label define npboss50_lbl 0505 `"0505"', add
label define npboss50_lbl 0506 `"0506"', add
label define npboss50_lbl 0507 `"0507"', add
label define npboss50_lbl 0508 `"0508"', add
label define npboss50_lbl 0509 `"0509"', add
label define npboss50_lbl 0510 `"0510"', add
label define npboss50_lbl 0511 `"0511"', add
label define npboss50_lbl 0512 `"0512"', add
label define npboss50_lbl 0513 `"0513"', add
label define npboss50_lbl 0514 `"0514"', add
label define npboss50_lbl 0515 `"0515"', add
label define npboss50_lbl 0516 `"0516"', add
label define npboss50_lbl 0517 `"0517"', add
label define npboss50_lbl 0518 `"0518"', add
label define npboss50_lbl 0519 `"0519"', add
label define npboss50_lbl 0520 `"0520"', add
label define npboss50_lbl 0521 `"0521"', add
label define npboss50_lbl 0522 `"0522"', add
label define npboss50_lbl 0523 `"0523"', add
label define npboss50_lbl 0524 `"0524"', add
label define npboss50_lbl 0525 `"0525"', add
label define npboss50_lbl 0526 `"0526"', add
label define npboss50_lbl 0527 `"0527"', add
label define npboss50_lbl 0528 `"0528"', add
label define npboss50_lbl 0529 `"0529"', add
label define npboss50_lbl 0530 `"0530"', add
label define npboss50_lbl 0531 `"0531"', add
label define npboss50_lbl 0532 `"0532"', add
label define npboss50_lbl 0533 `"0533"', add
label define npboss50_lbl 0534 `"0534"', add
label define npboss50_lbl 0535 `"0535"', add
label define npboss50_lbl 0536 `"0536"', add
label define npboss50_lbl 0537 `"0537"', add
label define npboss50_lbl 0538 `"0538"', add
label define npboss50_lbl 0539 `"0539"', add
label define npboss50_lbl 0540 `"0540"', add
label define npboss50_lbl 0541 `"0541"', add
label define npboss50_lbl 0542 `"0542"', add
label define npboss50_lbl 0543 `"0543"', add
label define npboss50_lbl 0544 `"0544"', add
label define npboss50_lbl 0545 `"0545"', add
label define npboss50_lbl 0546 `"0546"', add
label define npboss50_lbl 0547 `"0547"', add
label define npboss50_lbl 0548 `"0548"', add
label define npboss50_lbl 0549 `"0549"', add
label define npboss50_lbl 0550 `"0550"', add
label define npboss50_lbl 0551 `"0551"', add
label define npboss50_lbl 0552 `"0552"', add
label define npboss50_lbl 0553 `"0553"', add
label define npboss50_lbl 0554 `"0554"', add
label define npboss50_lbl 0555 `"0555"', add
label define npboss50_lbl 0556 `"0556"', add
label define npboss50_lbl 0557 `"0557"', add
label define npboss50_lbl 0558 `"0558"', add
label define npboss50_lbl 0559 `"0559"', add
label define npboss50_lbl 0560 `"0560"', add
label define npboss50_lbl 0561 `"0561"', add
label define npboss50_lbl 0562 `"0562"', add
label define npboss50_lbl 0563 `"0563"', add
label define npboss50_lbl 0564 `"0564"', add
label define npboss50_lbl 0565 `"0565"', add
label define npboss50_lbl 0566 `"0566"', add
label define npboss50_lbl 0567 `"0567"', add
label define npboss50_lbl 0568 `"0568"', add
label define npboss50_lbl 0569 `"0569"', add
label define npboss50_lbl 0570 `"0570"', add
label define npboss50_lbl 0571 `"0571"', add
label define npboss50_lbl 0572 `"0572"', add
label define npboss50_lbl 0573 `"0573"', add
label define npboss50_lbl 0574 `"0574"', add
label define npboss50_lbl 0575 `"0575"', add
label define npboss50_lbl 0576 `"0576"', add
label define npboss50_lbl 0577 `"0577"', add
label define npboss50_lbl 0578 `"0578"', add
label define npboss50_lbl 0579 `"0579"', add
label define npboss50_lbl 0580 `"0580"', add
label define npboss50_lbl 0581 `"0581"', add
label define npboss50_lbl 0582 `"0582"', add
label define npboss50_lbl 0583 `"0583"', add
label define npboss50_lbl 0584 `"0584"', add
label define npboss50_lbl 0585 `"0585"', add
label define npboss50_lbl 0586 `"0586"', add
label define npboss50_lbl 0587 `"0587"', add
label define npboss50_lbl 0588 `"0588"', add
label define npboss50_lbl 0589 `"0589"', add
label define npboss50_lbl 0590 `"0590"', add
label define npboss50_lbl 0591 `"0591"', add
label define npboss50_lbl 0592 `"0592"', add
label define npboss50_lbl 0593 `"0593"', add
label define npboss50_lbl 0594 `"0594"', add
label define npboss50_lbl 0595 `"0595"', add
label define npboss50_lbl 0596 `"0596"', add
label define npboss50_lbl 0597 `"0597"', add
label define npboss50_lbl 0598 `"0598"', add
label define npboss50_lbl 0599 `"0599"', add
label define npboss50_lbl 0600 `"0600"', add
label define npboss50_lbl 0601 `"0601"', add
label define npboss50_lbl 0602 `"0602"', add
label define npboss50_lbl 0603 `"0603"', add
label define npboss50_lbl 0604 `"0604"', add
label define npboss50_lbl 0605 `"0605"', add
label define npboss50_lbl 0606 `"0606"', add
label define npboss50_lbl 0607 `"0607"', add
label define npboss50_lbl 0608 `"0608"', add
label define npboss50_lbl 0609 `"0609"', add
label define npboss50_lbl 0610 `"0610"', add
label define npboss50_lbl 0611 `"0611"', add
label define npboss50_lbl 0612 `"0612"', add
label define npboss50_lbl 0613 `"0613"', add
label define npboss50_lbl 0614 `"0614"', add
label define npboss50_lbl 0615 `"0615"', add
label define npboss50_lbl 0616 `"0616"', add
label define npboss50_lbl 0617 `"0617"', add
label define npboss50_lbl 0618 `"0618"', add
label define npboss50_lbl 0619 `"0619"', add
label define npboss50_lbl 0620 `"0620"', add
label define npboss50_lbl 0621 `"0621"', add
label define npboss50_lbl 0622 `"0622"', add
label define npboss50_lbl 0623 `"0623"', add
label define npboss50_lbl 0624 `"0624"', add
label define npboss50_lbl 0625 `"0625"', add
label define npboss50_lbl 0626 `"0626"', add
label define npboss50_lbl 0627 `"0627"', add
label define npboss50_lbl 0628 `"0628"', add
label define npboss50_lbl 0629 `"0629"', add
label define npboss50_lbl 0630 `"0630"', add
label define npboss50_lbl 0631 `"0631"', add
label define npboss50_lbl 0632 `"0632"', add
label define npboss50_lbl 0633 `"0633"', add
label define npboss50_lbl 0634 `"0634"', add
label define npboss50_lbl 0635 `"0635"', add
label define npboss50_lbl 0636 `"0636"', add
label define npboss50_lbl 0637 `"0637"', add
label define npboss50_lbl 0638 `"0638"', add
label define npboss50_lbl 0639 `"0639"', add
label define npboss50_lbl 0640 `"0640"', add
label define npboss50_lbl 0641 `"0641"', add
label define npboss50_lbl 0642 `"0642"', add
label define npboss50_lbl 0643 `"0643"', add
label define npboss50_lbl 0644 `"0644"', add
label define npboss50_lbl 0645 `"0645"', add
label define npboss50_lbl 0646 `"0646"', add
label define npboss50_lbl 0647 `"0647"', add
label define npboss50_lbl 0648 `"0648"', add
label define npboss50_lbl 0649 `"0649"', add
label define npboss50_lbl 0650 `"0650"', add
label define npboss50_lbl 0651 `"0651"', add
label define npboss50_lbl 0652 `"0652"', add
label define npboss50_lbl 0653 `"0653"', add
label define npboss50_lbl 0654 `"0654"', add
label define npboss50_lbl 0655 `"0655"', add
label define npboss50_lbl 0656 `"0656"', add
label define npboss50_lbl 0657 `"0657"', add
label define npboss50_lbl 0658 `"0658"', add
label define npboss50_lbl 0659 `"0659"', add
label define npboss50_lbl 0660 `"0660"', add
label define npboss50_lbl 0661 `"0661"', add
label define npboss50_lbl 0662 `"0662"', add
label define npboss50_lbl 0663 `"0663"', add
label define npboss50_lbl 0664 `"0664"', add
label define npboss50_lbl 0665 `"0665"', add
label define npboss50_lbl 0666 `"0666"', add
label define npboss50_lbl 0667 `"0667"', add
label define npboss50_lbl 0668 `"0668"', add
label define npboss50_lbl 0669 `"0669"', add
label define npboss50_lbl 0670 `"0670"', add
label define npboss50_lbl 0671 `"0671"', add
label define npboss50_lbl 0672 `"0672"', add
label define npboss50_lbl 0673 `"0673"', add
label define npboss50_lbl 0674 `"0674"', add
label define npboss50_lbl 0675 `"0675"', add
label define npboss50_lbl 0676 `"0676"', add
label define npboss50_lbl 0677 `"0677"', add
label define npboss50_lbl 0678 `"0678"', add
label define npboss50_lbl 0679 `"0679"', add
label define npboss50_lbl 0680 `"0680"', add
label define npboss50_lbl 0681 `"0681"', add
label define npboss50_lbl 0682 `"0682"', add
label define npboss50_lbl 0683 `"0683"', add
label define npboss50_lbl 0684 `"0684"', add
label define npboss50_lbl 0685 `"0685"', add
label define npboss50_lbl 0686 `"0686"', add
label define npboss50_lbl 0687 `"0687"', add
label define npboss50_lbl 0688 `"0688"', add
label define npboss50_lbl 0689 `"0689"', add
label define npboss50_lbl 0690 `"0690"', add
label define npboss50_lbl 0691 `"0691"', add
label define npboss50_lbl 0692 `"0692"', add
label define npboss50_lbl 0693 `"0693"', add
label define npboss50_lbl 0694 `"0694"', add
label define npboss50_lbl 0695 `"0695"', add
label define npboss50_lbl 0696 `"0696"', add
label define npboss50_lbl 0697 `"0697"', add
label define npboss50_lbl 0698 `"0698"', add
label define npboss50_lbl 0699 `"0699"', add
label define npboss50_lbl 0700 `"0700"', add
label define npboss50_lbl 0701 `"0701"', add
label define npboss50_lbl 0702 `"0702"', add
label define npboss50_lbl 0703 `"0703"', add
label define npboss50_lbl 0704 `"0704"', add
label define npboss50_lbl 0705 `"0705"', add
label define npboss50_lbl 0706 `"0706"', add
label define npboss50_lbl 0707 `"0707"', add
label define npboss50_lbl 0708 `"0708"', add
label define npboss50_lbl 0709 `"0709"', add
label define npboss50_lbl 0710 `"0710"', add
label define npboss50_lbl 0711 `"0711"', add
label define npboss50_lbl 0712 `"0712"', add
label define npboss50_lbl 0713 `"0713"', add
label define npboss50_lbl 0714 `"0714"', add
label define npboss50_lbl 0715 `"0715"', add
label define npboss50_lbl 0716 `"0716"', add
label define npboss50_lbl 0717 `"0717"', add
label define npboss50_lbl 0718 `"0718"', add
label define npboss50_lbl 0719 `"0719"', add
label define npboss50_lbl 0720 `"0720"', add
label define npboss50_lbl 0721 `"0721"', add
label define npboss50_lbl 0722 `"0722"', add
label define npboss50_lbl 0723 `"0723"', add
label define npboss50_lbl 0724 `"0724"', add
label define npboss50_lbl 0725 `"0725"', add
label define npboss50_lbl 0726 `"0726"', add
label define npboss50_lbl 0727 `"0727"', add
label define npboss50_lbl 0728 `"0728"', add
label define npboss50_lbl 0729 `"0729"', add
label define npboss50_lbl 0730 `"0730"', add
label define npboss50_lbl 0731 `"0731"', add
label define npboss50_lbl 0732 `"0732"', add
label define npboss50_lbl 0733 `"0733"', add
label define npboss50_lbl 0734 `"0734"', add
label define npboss50_lbl 0735 `"0735"', add
label define npboss50_lbl 0736 `"0736"', add
label define npboss50_lbl 0737 `"0737"', add
label define npboss50_lbl 0738 `"0738"', add
label define npboss50_lbl 0739 `"0739"', add
label define npboss50_lbl 0740 `"0740"', add
label define npboss50_lbl 0741 `"0741"', add
label define npboss50_lbl 0742 `"0742"', add
label define npboss50_lbl 0743 `"0743"', add
label define npboss50_lbl 0744 `"0744"', add
label define npboss50_lbl 0745 `"0745"', add
label define npboss50_lbl 0746 `"0746"', add
label define npboss50_lbl 0747 `"0747"', add
label define npboss50_lbl 0748 `"0748"', add
label define npboss50_lbl 0749 `"0749"', add
label define npboss50_lbl 0750 `"0750"', add
label define npboss50_lbl 0751 `"0751"', add
label define npboss50_lbl 0752 `"0752"', add
label define npboss50_lbl 0753 `"0753"', add
label define npboss50_lbl 0754 `"0754"', add
label define npboss50_lbl 0755 `"0755"', add
label define npboss50_lbl 0756 `"0756"', add
label define npboss50_lbl 0757 `"0757"', add
label define npboss50_lbl 0758 `"0758"', add
label define npboss50_lbl 0759 `"0759"', add
label define npboss50_lbl 0760 `"0760"', add
label define npboss50_lbl 0761 `"0761"', add
label define npboss50_lbl 0762 `"0762"', add
label define npboss50_lbl 0763 `"0763"', add
label define npboss50_lbl 0764 `"0764"', add
label define npboss50_lbl 0765 `"0765"', add
label define npboss50_lbl 0766 `"0766"', add
label define npboss50_lbl 0767 `"0767"', add
label define npboss50_lbl 0768 `"0768"', add
label define npboss50_lbl 0769 `"0769"', add
label define npboss50_lbl 0770 `"0770"', add
label define npboss50_lbl 0771 `"0771"', add
label define npboss50_lbl 0772 `"0772"', add
label define npboss50_lbl 0773 `"0773"', add
label define npboss50_lbl 0774 `"0774"', add
label define npboss50_lbl 0775 `"0775"', add
label define npboss50_lbl 0776 `"0776"', add
label define npboss50_lbl 0777 `"0777"', add
label define npboss50_lbl 0778 `"0778"', add
label define npboss50_lbl 0779 `"0779"', add
label define npboss50_lbl 0780 `"0780"', add
label define npboss50_lbl 0781 `"0781"', add
label define npboss50_lbl 0782 `"0782"', add
label define npboss50_lbl 0783 `"0783"', add
label define npboss50_lbl 0784 `"0784"', add
label define npboss50_lbl 0785 `"0785"', add
label define npboss50_lbl 0786 `"0786"', add
label define npboss50_lbl 0787 `"0787"', add
label define npboss50_lbl 0788 `"0788"', add
label define npboss50_lbl 0789 `"0789"', add
label define npboss50_lbl 0790 `"0790"', add
label define npboss50_lbl 0791 `"0791"', add
label define npboss50_lbl 0792 `"0792"', add
label define npboss50_lbl 0793 `"0793"', add
label define npboss50_lbl 0794 `"0794"', add
label define npboss50_lbl 0795 `"0795"', add
label define npboss50_lbl 0796 `"0796"', add
label define npboss50_lbl 0797 `"0797"', add
label define npboss50_lbl 0798 `"0798"', add
label define npboss50_lbl 0799 `"0799"', add
label define npboss50_lbl 0800 `"0800"', add
label define npboss50_lbl 0801 `"0801"', add
label define npboss50_lbl 0802 `"0802"', add
label define npboss50_lbl 0803 `"0803"', add
label define npboss50_lbl 0804 `"0804"', add
label define npboss50_lbl 0805 `"0805"', add
label define npboss50_lbl 0806 `"0806"', add
label define npboss50_lbl 0807 `"0807"', add
label define npboss50_lbl 0808 `"0808"', add
label define npboss50_lbl 0809 `"0809"', add
label define npboss50_lbl 0810 `"0810"', add
label define npboss50_lbl 0811 `"0811"', add
label define npboss50_lbl 0812 `"0812"', add
label define npboss50_lbl 0813 `"0813"', add
label define npboss50_lbl 0814 `"0814"', add
label define npboss50_lbl 0815 `"0815"', add
label define npboss50_lbl 0816 `"0816"', add
label define npboss50_lbl 0817 `"0817"', add
label define npboss50_lbl 0818 `"0818"', add
label define npboss50_lbl 0819 `"0819"', add
label define npboss50_lbl 0820 `"0820"', add
label define npboss50_lbl 0821 `"0821"', add
label define npboss50_lbl 0822 `"0822"', add
label define npboss50_lbl 0823 `"0823"', add
label define npboss50_lbl 0824 `"0824"', add
label define npboss50_lbl 0825 `"0825"', add
label define npboss50_lbl 0826 `"0826"', add
label define npboss50_lbl 0827 `"0827"', add
label define npboss50_lbl 0828 `"0828"', add
label define npboss50_lbl 0829 `"0829"', add
label define npboss50_lbl 0830 `"0830"', add
label define npboss50_lbl 0831 `"0831"', add
label define npboss50_lbl 0832 `"0832"', add
label define npboss50_lbl 0833 `"0833"', add
label define npboss50_lbl 0834 `"0834"', add
label define npboss50_lbl 0835 `"0835"', add
label define npboss50_lbl 0836 `"0836"', add
label define npboss50_lbl 0837 `"0837"', add
label define npboss50_lbl 0838 `"0838"', add
label define npboss50_lbl 0839 `"0839"', add
label define npboss50_lbl 0840 `"0840"', add
label define npboss50_lbl 0841 `"0841"', add
label define npboss50_lbl 0842 `"0842"', add
label define npboss50_lbl 0843 `"0843"', add
label define npboss50_lbl 0844 `"0844"', add
label define npboss50_lbl 0845 `"0845"', add
label define npboss50_lbl 0846 `"0846"', add
label define npboss50_lbl 0847 `"0847"', add
label define npboss50_lbl 0848 `"0848"', add
label define npboss50_lbl 0849 `"0849"', add
label define npboss50_lbl 0850 `"0850"', add
label define npboss50_lbl 0851 `"0851"', add
label define npboss50_lbl 0852 `"0852"', add
label define npboss50_lbl 0853 `"0853"', add
label define npboss50_lbl 0854 `"0854"', add
label define npboss50_lbl 0855 `"0855"', add
label define npboss50_lbl 0856 `"0856"', add
label define npboss50_lbl 0857 `"0857"', add
label define npboss50_lbl 0858 `"0858"', add
label define npboss50_lbl 0859 `"0859"', add
label define npboss50_lbl 0860 `"0860"', add
label define npboss50_lbl 0861 `"0861"', add
label define npboss50_lbl 0862 `"0862"', add
label define npboss50_lbl 0863 `"0863"', add
label define npboss50_lbl 0864 `"0864"', add
label define npboss50_lbl 0865 `"0865"', add
label define npboss50_lbl 0866 `"0866"', add
label define npboss50_lbl 0867 `"0867"', add
label define npboss50_lbl 0868 `"0868"', add
label define npboss50_lbl 0869 `"0869"', add
label define npboss50_lbl 0870 `"0870"', add
label define npboss50_lbl 0871 `"0871"', add
label define npboss50_lbl 0872 `"0872"', add
label define npboss50_lbl 0873 `"0873"', add
label define npboss50_lbl 0874 `"0874"', add
label define npboss50_lbl 0875 `"0875"', add
label define npboss50_lbl 0876 `"0876"', add
label define npboss50_lbl 0877 `"0877"', add
label define npboss50_lbl 0878 `"0878"', add
label define npboss50_lbl 0879 `"0879"', add
label define npboss50_lbl 0880 `"0880"', add
label define npboss50_lbl 0881 `"0881"', add
label define npboss50_lbl 0882 `"0882"', add
label define npboss50_lbl 0883 `"0883"', add
label define npboss50_lbl 0884 `"0884"', add
label define npboss50_lbl 0885 `"0885"', add
label define npboss50_lbl 0886 `"0886"', add
label define npboss50_lbl 0887 `"0887"', add
label define npboss50_lbl 0888 `"0888"', add
label define npboss50_lbl 0889 `"0889"', add
label define npboss50_lbl 0890 `"0890"', add
label define npboss50_lbl 0891 `"0891"', add
label define npboss50_lbl 0892 `"0892"', add
label define npboss50_lbl 0893 `"0893"', add
label define npboss50_lbl 0894 `"0894"', add
label define npboss50_lbl 0895 `"0895"', add
label define npboss50_lbl 0896 `"0896"', add
label define npboss50_lbl 0897 `"0897"', add
label define npboss50_lbl 0898 `"0898"', add
label define npboss50_lbl 0899 `"0899"', add
label define npboss50_lbl 0900 `"0900"', add
label define npboss50_lbl 0901 `"0901"', add
label define npboss50_lbl 0902 `"0902"', add
label define npboss50_lbl 0903 `"0903"', add
label define npboss50_lbl 0904 `"0904"', add
label define npboss50_lbl 0905 `"0905"', add
label define npboss50_lbl 0906 `"0906"', add
label define npboss50_lbl 0907 `"0907"', add
label define npboss50_lbl 0908 `"0908"', add
label define npboss50_lbl 0909 `"0909"', add
label define npboss50_lbl 0910 `"0910"', add
label define npboss50_lbl 0911 `"0911"', add
label define npboss50_lbl 0912 `"0912"', add
label define npboss50_lbl 0913 `"0913"', add
label define npboss50_lbl 0914 `"0914"', add
label define npboss50_lbl 0915 `"0915"', add
label define npboss50_lbl 0916 `"0916"', add
label define npboss50_lbl 0917 `"0917"', add
label define npboss50_lbl 0918 `"0918"', add
label define npboss50_lbl 0919 `"0919"', add
label define npboss50_lbl 0920 `"0920"', add
label define npboss50_lbl 0921 `"0921"', add
label define npboss50_lbl 0922 `"0922"', add
label define npboss50_lbl 0923 `"0923"', add
label define npboss50_lbl 0924 `"0924"', add
label define npboss50_lbl 0925 `"0925"', add
label define npboss50_lbl 0926 `"0926"', add
label define npboss50_lbl 0927 `"0927"', add
label define npboss50_lbl 0928 `"0928"', add
label define npboss50_lbl 0929 `"0929"', add
label define npboss50_lbl 0930 `"0930"', add
label define npboss50_lbl 0931 `"0931"', add
label define npboss50_lbl 0932 `"0932"', add
label define npboss50_lbl 0933 `"0933"', add
label define npboss50_lbl 0934 `"0934"', add
label define npboss50_lbl 0935 `"0935"', add
label define npboss50_lbl 0936 `"0936"', add
label define npboss50_lbl 0937 `"0937"', add
label define npboss50_lbl 0938 `"0938"', add
label define npboss50_lbl 0939 `"0939"', add
label define npboss50_lbl 0940 `"0940"', add
label define npboss50_lbl 0941 `"0941"', add
label define npboss50_lbl 0942 `"0942"', add
label define npboss50_lbl 0943 `"0943"', add
label define npboss50_lbl 0944 `"0944"', add
label define npboss50_lbl 0945 `"0945"', add
label define npboss50_lbl 0946 `"0946"', add
label define npboss50_lbl 0947 `"0947"', add
label define npboss50_lbl 0948 `"0948"', add
label define npboss50_lbl 0949 `"0949"', add
label define npboss50_lbl 0950 `"0950"', add
label define npboss50_lbl 0951 `"0951"', add
label define npboss50_lbl 0952 `"0952"', add
label define npboss50_lbl 0953 `"0953"', add
label define npboss50_lbl 0954 `"0954"', add
label define npboss50_lbl 0955 `"0955"', add
label define npboss50_lbl 0956 `"0956"', add
label define npboss50_lbl 0957 `"0957"', add
label define npboss50_lbl 0958 `"0958"', add
label define npboss50_lbl 0959 `"0959"', add
label define npboss50_lbl 0960 `"0960"', add
label define npboss50_lbl 0961 `"0961"', add
label define npboss50_lbl 0962 `"0962"', add
label define npboss50_lbl 0963 `"0963"', add
label define npboss50_lbl 0964 `"0964"', add
label define npboss50_lbl 0965 `"0965"', add
label define npboss50_lbl 0966 `"0966"', add
label define npboss50_lbl 0967 `"0967"', add
label define npboss50_lbl 0968 `"0968"', add
label define npboss50_lbl 0969 `"0969"', add
label define npboss50_lbl 0970 `"0970"', add
label define npboss50_lbl 0971 `"0971"', add
label define npboss50_lbl 0972 `"0972"', add
label define npboss50_lbl 0973 `"0973"', add
label define npboss50_lbl 0974 `"0974"', add
label define npboss50_lbl 0975 `"0975"', add
label define npboss50_lbl 0976 `"0976"', add
label define npboss50_lbl 0977 `"0977"', add
label define npboss50_lbl 0978 `"0978"', add
label define npboss50_lbl 0979 `"0979"', add
label define npboss50_lbl 0980 `"0980"', add
label define npboss50_lbl 0981 `"0981"', add
label define npboss50_lbl 0982 `"0982"', add
label define npboss50_lbl 0983 `"0983"', add
label define npboss50_lbl 0984 `"0984"', add
label define npboss50_lbl 0985 `"0985"', add
label define npboss50_lbl 0986 `"0986"', add
label define npboss50_lbl 0987 `"0987"', add
label define npboss50_lbl 0988 `"0988"', add
label define npboss50_lbl 0989 `"0989"', add
label define npboss50_lbl 0990 `"0990"', add
label define npboss50_lbl 0991 `"0991"', add
label define npboss50_lbl 0992 `"0992"', add
label define npboss50_lbl 0993 `"0993"', add
label define npboss50_lbl 0994 `"0994"', add
label define npboss50_lbl 0995 `"0995"', add
label define npboss50_lbl 0996 `"0996"', add
label define npboss50_lbl 0997 `"0997"', add
label define npboss50_lbl 0998 `"0998"', add
label define npboss50_lbl 0999 `"0999"', add
label define npboss50_lbl 1000 `"1000"', add
label define npboss50_lbl 9999 `"N/A"', add
label values npboss50 npboss50_lbl

label define versionhist_lbl 01 `"Public release version 1"'
label define versionhist_lbl 02 `"Public release version 2"', add
label values versionhist versionhist_lbl

label define sursim_lbl 00 `"N/A (sampled at the individual level)"'
label define sursim_lbl 01 `"1st surname in household"', add
label define sursim_lbl 02 `"2"', add
label define sursim_lbl 03 `"3"', add
label define sursim_lbl 04 `"4"', add
label define sursim_lbl 05 `"5"', add
label define sursim_lbl 06 `"6"', add
label define sursim_lbl 07 `"7"', add
label define sursim_lbl 08 `"8"', add
label define sursim_lbl 09 `"9"', add
label define sursim_lbl 10 `"10"', add
label define sursim_lbl 11 `"11"', add
label define sursim_lbl 12 `"12"', add
label define sursim_lbl 13 `"13"', add
label define sursim_lbl 14 `"14"', add
label define sursim_lbl 15 `"15"', add
label define sursim_lbl 16 `"16"', add
label define sursim_lbl 17 `"17"', add
label define sursim_lbl 18 `"18"', add
label define sursim_lbl 19 `"19"', add
label define sursim_lbl 20 `"20"', add
label define sursim_lbl 21 `"21"', add
label define sursim_lbl 22 `"22"', add
label define sursim_lbl 23 `"23"', add
label define sursim_lbl 24 `"24"', add
label define sursim_lbl 25 `"25"', add
label define sursim_lbl 26 `"26"', add
label define sursim_lbl 27 `"27"', add
label define sursim_lbl 28 `"28"', add
label define sursim_lbl 29 `"29"', add
label define sursim_lbl 30 `"30"', add
label define sursim_lbl 99 `"Unknown"', add
label values sursim sursim_lbl

label define vote_lbl 0 `"N/A or not (able to vote)"'
label define vote_lbl 2 `"Yes, denied the right to vote"', add
label values vote vote_lbl

compress

save "$repfolder/data/analysis/1870_100pct", replace
