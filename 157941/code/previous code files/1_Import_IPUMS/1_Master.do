cd "$repfolder/data/analysis"

do "$repfolder/code/1_Import_IPUMS/Import_1850_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1860_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1870_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1880_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1900_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1910_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1920_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1930_ipums_version.do"
do "$repfolder/code/1_Import_IPUMS/Import_1940_ipums_version.do"

/*
do "$repfolder/code/1_Import_IPUMS/Import_1850.do"
do "$repfolder/code/1_Import_IPUMS/Import_1860.do"
do "$repfolder/code/1_Import_IPUMS/Import_1870.do"
do "$repfolder/code/1_Import_IPUMS/Import_1880.do"
do "$repfolder/code/1_Import_IPUMS/Import_1900.do"
do "$repfolder/code/1_Import_IPUMS/Import_1910.do"
do "$repfolder/code/1_Import_IPUMS/Import_1920.do"
do "$repfolder/code/1_Import_IPUMS/Import_1930.do"
do "$repfolder/code/1_Import_IPUMS/Import_1940.do"
*/