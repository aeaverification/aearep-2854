cd "$repfolder/data/analysis"

do "$repfolder/code/2_Create_Files/2.1_Recategorize_1850/RecatProbit1850.do"
do "$repfolder/code/2_Create_Files/2.2_Create_Wealth_Scores/2.2_Master.do"
do "$repfolder/code/2_Create_Files/2.3_Create_Occ_Ranks/2.3_Master.do"
do "$repfolder/code/2_Create_Files/2.4_Create_Main_Files/2.4_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5_Master.do"
