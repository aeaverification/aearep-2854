cd "$repfolder/data/analysis"

//The code in this folder essentially repeats that in 2.4. The comments in each file mark cases where there is a difference from the benchmark.

do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.1_Alternate_Methods/2.5.1_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.2_Impute/2.5.2_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/2.5.3_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.4_Forward/2.5.4_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.5_Second/2.5.5_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.6_Recent/2.5.6_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.7_Scores/2.5.7_Master.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.8_English/2.5.8_Master.do"
