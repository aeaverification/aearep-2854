cd "$repfolder/data/analysis"

do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/CreateRecatProbit_1850_1870.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/Create_1880_1900.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/Create_1900_1920.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/Create_1910_1930.do"
do "$repfolder/code/2_Create_Files/2.5_Create_Robustness_Files/2.5.3_20Yr/Create_1920_1940.do"
