cd "$repfolder/data/analysis"

use  "$repfolder/data/analysis/1910_100pct", clear
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=44 & age<=64 & race==1 & (region<30 | region>=40)

preserve
use "$repfolder/data/confidential/clp_linkage_crosswalks/crosswalk_1880_1910", clear
rename histid_1880 histid_80
rename histid_1910 histid_10
replace histid_80=upper(histid_80)
replace histid_10=upper(histid_10)
keep if abe_exact_conservative==1 & abe_nysiis_conservative==1
keep histid*
merge 1:1 histid* using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1880_1910"
keep if _merge==3
drop _merge
tempfile crosswalk
save `crosswalk'
restore

gen histid_10=upper(histid)
merge 1:1 histid_10 using `crosswalk', keep(1 3)
gen linked=(histid_80~="")
drop _merge histid

preserve
use  "$repfolder/data/analysis/1880_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_80=upper(histid)
drop histid
keep if sex==1
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_80
}
tempfile census80
save `census80'
restore
rename age age_10
merge m:1 histid_80 using `census80', keep(3)

		//Require detailed birthplace match
		drop if bpld~=bpld_80
		
		//Require parents' birthplace match
		drop if fbpl~=fbpl_80
		drop if mbpl~=mbpl_80
	
		//Drop those who go from married to never married
		drop if marst_80<=5 & marst==6

		//Require race match	
		drop if race~=race_80
	
		//Drop those who "forget" how to read
		drop if lit_80==4 & lit==1
	
keep histid*
compress
save crosswalk_1880_1910_IntPlus, replace
