cd "$repfolder/data/analysis"

do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/DistributionsFarmFam_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/RankGraphsRecatProbit_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/DissimilarityControlRecatProbit_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/OccupationalUpgradingGraphsMidpoint_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/DeltaRankRegressionsRecatProbit_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/DeltaRankRegressionsRecatProbitMidpoint_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/DeltaRankRegressionsRecatProbitUpper_Impute.do"
do "$repfolder/code/4_Robustness_Analysis/4.2_Impute/GraphUnconditionalConditionalResults_Impute.do"
