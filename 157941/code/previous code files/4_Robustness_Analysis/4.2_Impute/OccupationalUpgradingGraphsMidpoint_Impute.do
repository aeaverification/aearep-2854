cd "$repfolder/data/analysis"

///////////////////////////////

use FileRecatProbit_1850_1880_Impute, clear
merge 1:1 histid_50 using RescoredRecatProbit_1850, keep(1 3)
gen rescored_50=(_merge==3)
drop _merge
replace unskill_50=0 if rescored_50==1

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_80-avg_rank_midpoint_50
rename foreign_50 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_50
	gen native_craft=native*craft_50
	gen native_operative=native*operative_50
	gen native_unskill=native*unskill_50
	gen native_rescored=native*rescored_50
	gen foreign_wc=foreign*wc_50
	gen foreign_farmer=foreign*farmer_50
	gen foreign_craft=foreign*craft_50
	gen foreign_operative=foreign*operative_50
	gen foreign_unskill=foreign*unskill_50
	gen foreign_rescored=foreign*rescored_50
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_50##c.age_50##c.age_50##c.age_50 [aw=1/link_prob_midpoint_50], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6,angle(0) glwidth(0)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)") ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants"))
	graph export "$repfolder/results/FigureD6a.pdf", replace
		
///////////////////////////////

use File_1870_1900_Impute, clear
merge 1:1 histid_70 using Rescored_1870, keep(1 3)
gen rescored_70=(_merge==3)
drop _merge
replace unskill_70=0 if rescored_70==1

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen delta_rank_midpoint=avg_rank_impute_midpoint_00-avg_rank_midpoint_70
rename foreign_00 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_70
	gen native_craft=native*craft_70
	gen native_operative=native*operative_70
	gen native_unskill=native*unskill_70
	gen native_rescored=native*rescored_70
	gen foreign_wc=foreign*wc_70
	gen foreign_farmer=foreign*farmer_70
	gen foreign_craft=foreign*craft_70
	gen foreign_operative=foreign*operative_70
	gen foreign_unskill=foreign*unskill_70
	gen foreign_rescored=foreign*rescored_70
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_70##c.age_70##c.age_70##c.age_70 [aw=1/link_prob_midpoint_00], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6,angle(0) glwidth(0)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)") ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants"))
	graph export "$repfolder/results/FigureD6b.pdf", replace

////////////////////////////

use File_1880_1910_Impute, clear
merge 1:1 histid_80 using Rescored_1880, keep(1 3)
gen rescored_80=(_merge==3)
drop _merge
replace unskill_80=0 if rescored_80==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen delta_rank_midpoint=avg_rank_impute_midpoint_10-avg_rank_midpoint_80
rename foreign_10 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_80
	gen native_craft=native*craft_80
	gen native_operative=native*operative_80
	gen native_unskill=native*unskill_80
	gen native_rescored=native*rescored_80
	gen foreign_wc=foreign*wc_80
	gen foreign_farmer=foreign*farmer_80
	gen foreign_craft=foreign*craft_80
	gen foreign_operative=foreign*operative_80
	gen foreign_unskill=foreign*unskill_80
	gen foreign_rescored=foreign*rescored_80
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_80##c.age_80##c.age_80##c.age_80 [aw=1/link_prob_midpoint_10], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6,angle(0) glwidth(0)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)") ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants"))
	graph export "$repfolder/results/FigureD6c.pdf", replace
	
////////////////////////////

use File_1900_1930_Impute, clear
merge 1:1 histid_00 using RescoredImpute_1900, keep(1 3)
gen rescored_00=(_merge==3)
drop _merge
replace unskill_00=0 if rescored_00==1

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen delta_rank_midpoint=avg_rank_impute_midpoint_30-avg_rank_impute_midpoint_00
rename foreign_30 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_00
	gen native_craft=native*craft_00
	gen native_operative=native*operative_00
	gen native_unskill=native*unskill_00
	gen native_rescored=native*rescored_00
	gen foreign_wc=foreign*wc_00
	gen foreign_farmer=foreign*farmer_00
	gen foreign_craft=foreign*craft_00
	gen foreign_operative=foreign*operative_00
	gen foreign_unskill=foreign*unskill_00
	gen foreign_rescored=foreign*rescored_00
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_00##c.age_00##c.age_00##c.age_00 [aw=1/link_prob_midpoint_30], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6,angle(0) glwidth(0)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)") ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants"))
	graph export "$repfolder/results/FigureD6d.pdf", replace
	
////////////////////////////

use File_1910_1940_Impute, clear
merge 1:1 histid_10 using RescoredImpute_1910, keep(1 3)
gen rescored_10=(_merge==3)
drop _merge
replace unskill_10=0 if rescored_10==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen delta_rank_midpoint=avg_rank_midpoint_40-avg_rank_impute_midpoint_10
rename foreign_10 foreign
label var foreign "Immigrant"

	gen native=1-foreign
	gen native_farmer=native*farmer_10
	gen native_craft=native*craft_10
	gen native_operative=native*operative_10
	gen native_unskill=native*unskill_10
	gen native_rescored=native*rescored_10
	gen foreign_wc=foreign*wc_10
	gen foreign_farmer=foreign*farmer_10
	gen foreign_craft=foreign*craft_10
	gen foreign_operative=foreign*operative_10
	gen foreign_unskill=foreign*unskill_10
	gen foreign_rescored=foreign*rescored_10
	
	gen main_wc=foreign_wc
	gen main_farmer=foreign_farmer
	gen main_craft=foreign_craft
	gen main_operative=foreign_operative
	gen main_unskill=foreign_unskill
	gen main_rescored=foreign_rescored
	
	label var main_wc "White Collar"
	label var main_farmer "Farmer"
	label var main_craft "Craft"
	label var main_operative "Operative"
	label var main_unskill "Unskill"
		label var main_rescored "Farm Family"
	
	reg delta_rank_midpoint native_* main_* c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto foreign
	
	replace main_farmer=native_farmer
	replace main_craft=native_craft
	replace main_operative=native_operative
	replace main_unskill=native_unskill
	replace main_rescored=native_rescored
	
	reg delta_rank_midpoint foreign_* main_farmer-main_rescored c.age_10##c.age_10##c.age_10##c.age_10 [aw=1/link_prob_midpoint_10], robust
	est sto native
	
	coefplot ///
		(native, mcolor(black) ciopts(color(black))) ///
		(foreign, mcolor(gray) msymbol(S) ciopts(color(gray))) ///
		, vertical keep(main*) order(main_wc main_farmer main_craft main_operative main_unskill main_rescored) ///
		graphregion(color(white)) yline(0, lcolor(gray)) ///
		ylabel(-.2(.2).6,angle(0) glwidth(0)) ///
		ytitle("Average {&Delta} Rank (vs Native White Collar)") ///
		legend(region(lcolor(white)) order(2 "Natives" 4 "Immigrants"))
	graph export "$repfolder/results/FigureD6e.pdf", replace
	