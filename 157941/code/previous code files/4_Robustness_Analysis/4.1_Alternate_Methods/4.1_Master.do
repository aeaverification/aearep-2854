cd "$repfolder/data/analysis"

do "$repfolder/code/4_Robustness_Analysis/4.1_Alternate_Methods/4.1.1_ABEE/4.1.1_Master.do"
do "$repfolder/code/4_Robustness_Analysis/4.1_Alternate_Methods/4.1.2_ABEN/4.1.2_Master.do"
do "$repfolder/code/4_Robustness_Analysis/4.1_Alternate_Methods/4.1.3_Int/4.1.3_Master.do"
do "$repfolder/code/4_Robustness_Analysis/4.1_Alternate_Methods/4.1.4_IntPlus/4.1.4_Master.do"
