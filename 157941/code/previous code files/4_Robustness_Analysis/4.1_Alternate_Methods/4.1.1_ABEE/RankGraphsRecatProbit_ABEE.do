cd "$repfolder/data/analysis"

use FileRecatProbit_1850_1880_ABEE, clear

_pctile link_prob_50, percentiles(0.5)
replace link_prob_50=r(r1) if link_prob_50<r(r1)

reg avg_rank_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if avg_rank_50~=. & avg_rank_80~=. [aw=1/link_prob_50]
	local b1=_b[foreign_50]
	local s1=_se[foreign_50]
reg avg_rank_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_50~=. & avg_rank_80~=. [aw=1/link_prob_50]
	local b2=_b[foreign_50]
	local s2=_se[foreign_50]
	
capture postclose output
postfile output b1 s1 b2 s2 year1 year2 span rank using RankGapsRecatProbit_ABEE, replace
post output (`b1') (`s1') (`b2') (`s2') (1850) (1880) (1) (1)
	
_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

reg avg_rank_midpoint_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if avg_rank_midpoint_50~=. & avg_rank_midpoint_80~=. [aw=1/link_prob_midpoint_50]
	local b1=_b[foreign_50]
	local s1=_se[foreign_50]
reg avg_rank_midpoint_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_midpoint_50~=. & avg_rank_midpoint_80~=. [aw=1/link_prob_midpoint_50]
	local b2=_b[foreign_50]
	local s2=_se[foreign_50]
	
post output (`b1') (`s1') (`b2') (`s2') (1850) (1880) (1) (2)
	
_pctile link_prob_upper_50, percentiles(0.5)
replace link_prob_upper_50=r(r1) if link_prob_upper_50<r(r1)

reg avg_rank_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if avg_rank_upper_50~=. & avg_rank_upper_80~=. [aw=1/link_prob_upper_50]
	local b1=_b[foreign_50]
	local s1=_se[foreign_50]
reg avg_rank_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_upper_50~=. & avg_rank_upper_80~=. [aw=1/link_prob_upper_50]
	local b2=_b[foreign_50]
	local s2=_se[foreign_50]
	
post output (`b1') (`s1') (`b2') (`s2') (1850) (1880) (1) (3)

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

use File_1870_1900_ABEE, clear

_pctile link_prob_00, percentiles(0.5)
replace link_prob_00=r(r1) if link_prob_00<r(r1)

reg avg_rank_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if avg_rank_70~=. & avg_rank_00~=. [aw=1/link_prob_00]
	local b1=_b[foreign_00]
	local s1=_se[foreign_00]
reg avg_rank_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_70~=. & avg_rank_00~=. [aw=1/link_prob_00]
	local b2=_b[foreign_00]
	local s2=_se[foreign_00]
	
post output (`b1') (`s1') (`b2') (`s2') (1870) (1900) (2) (1)
	
_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

reg avg_rank_midpoint_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if avg_rank_midpoint_70~=. & avg_rank_midpoint_00~=. [aw=1/link_prob_midpoint_00]
	local b1=_b[foreign_00]
	local s1=_se[foreign_00]
reg avg_rank_midpoint_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_midpoint_70~=. & avg_rank_midpoint_00~=. [aw=1/link_prob_midpoint_00]
	local b2=_b[foreign_00]
	local s2=_se[foreign_00]
	
post output (`b1') (`s1') (`b2') (`s2') (1870) (1900) (2) (2)
	
_pctile link_prob_upper_00, percentiles(0.5)
replace link_prob_upper_00=r(r1) if link_prob_upper_00<r(r1)

reg avg_rank_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if avg_rank_upper_70~=. & avg_rank_upper_00~=. [aw=1/link_prob_upper_00]
	local b1=_b[foreign_00]
	local s1=_se[foreign_00]
reg avg_rank_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_upper_70~=. & avg_rank_upper_00~=. [aw=1/link_prob_upper_00]
	local b2=_b[foreign_00]
	local s2=_se[foreign_00]
	
post output (`b1') (`s1') (`b2') (`s2') (1870) (1900) (2) (3)

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

use File_1880_1910_ABEE, clear

_pctile link_prob_10, percentiles(0.5)
replace link_prob_10=r(r1) if link_prob_10<r(r1)

reg avg_rank_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_80~=. & avg_rank_10~=. [aw=1/link_prob_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_80~=. & avg_rank_10~=. [aw=1/link_prob_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1880) (1910) (3) (1)
	
_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

reg avg_rank_midpoint_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_midpoint_80~=. & avg_rank_midpoint_10~=. [aw=1/link_prob_midpoint_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_midpoint_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_midpoint_80~=. & avg_rank_midpoint_10~=. [aw=1/link_prob_midpoint_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1880) (1910) (3) (2)
	
_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)

reg avg_rank_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_upper_80~=. & avg_rank_upper_10~=. [aw=1/link_prob_upper_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_upper_80~=. & avg_rank_upper_10~=. [aw=1/link_prob_upper_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1880) (1910) (3) (3)

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

use File_1900_1930_ABEE, clear

_pctile link_prob_30, percentiles(0.5)
replace link_prob_30=r(r1) if link_prob_30<r(r1)

reg avg_rank_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_00~=. & avg_rank_30~=. [aw=1/link_prob_30]
	local b1=_b[foreign_30]
	local s1=_se[foreign_30]
reg avg_rank_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if avg_rank_00~=. & avg_rank_30~=. [aw=1/link_prob_30]
	local b2=_b[foreign_30]
	local s2=_se[foreign_30]
	
post output (`b1') (`s1') (`b2') (`s2') (1900) (1930) (4) (1)
	
_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

reg avg_rank_midpoint_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_midpoint_00~=. & avg_rank_midpoint_30~=. [aw=1/link_prob_midpoint_30]
	local b1=_b[foreign_30]
	local s1=_se[foreign_30]
reg avg_rank_midpoint_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if avg_rank_midpoint_00~=. & avg_rank_midpoint_30~=. [aw=1/link_prob_midpoint_30]
	local b2=_b[foreign_30]
	local s2=_se[foreign_30]
	
post output (`b1') (`s1') (`b2') (`s2') (1900) (1930) (4) (2)
	
_pctile link_prob_upper_30, percentiles(0.5)
replace link_prob_upper_30=r(r1) if link_prob_upper_30<r(r1)

reg avg_rank_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_upper_00~=. & avg_rank_upper_30~=. [aw=1/link_prob_upper_30]
	local b1=_b[foreign_30]
	local s1=_se[foreign_30]
reg avg_rank_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if avg_rank_upper_00~=. & avg_rank_upper_30~=. [aw=1/link_prob_upper_30]
	local b2=_b[foreign_30]
	local s2=_se[foreign_30]
	
post output (`b1') (`s1') (`b2') (`s2') (1900) (1930) (4) (3)

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

use File_1910_1940_ABEE, clear

_pctile link_prob_10, percentiles(0.5)
replace link_prob_10=r(r1) if link_prob_10<r(r1)

reg avg_rank_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_10~=. & avg_rank_40~=. [aw=1/link_prob_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if avg_rank_10~=. & avg_rank_40~=. [aw=1/link_prob_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1910) (1940) (5) (1)
	
_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

reg avg_rank_midpoint_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_midpoint_10~=. & avg_rank_midpoint_40~=. [aw=1/link_prob_midpoint_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_midpoint_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if avg_rank_midpoint_10~=. & avg_rank_midpoint_40~=. [aw=1/link_prob_midpoint_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1910) (1940) (5) (2)
	
_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)

reg avg_rank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_upper_10~=. & avg_rank_upper_40~=. [aw=1/link_prob_upper_10]
	local b1=_b[foreign_10]
	local s1=_se[foreign_10]
reg avg_rank_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if avg_rank_upper_10~=. & avg_rank_upper_40~=. [aw=1/link_prob_upper_10]
	local b2=_b[foreign_10]
	local s2=_se[foreign_10]
	
post output (`b1') (`s1') (`b2') (`s2') (1910) (1940) (5) (3)
postclose output

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

use RankGapsRecatProbit_ABEE, clear

twoway ///
	connected b1 year1 if rank==1, lcolor(black) mcolor(black) ///
|| ///
	connected b1 year1 if rank==2, lcolor(black) mcolor(black) lpattern(-) msymbol(D) ///
|| ///
	connected b1 year1 if rank==3, lcolor(black) mcolor(black) lpattern(_) msymbol(T) ///
, graphregion(color(white)) ///
		ylabel(,angle(0) glwidth(0) labsize(medium)) xlabel(, labsize(medium)) ///
		xtitle("Year", size(medium)) ytitle("Coefficient on Foreign", size(medium)) yscale(titlegap(*-25)) ///
		legend(region(lcolor(white)) order(1 "Farm Labor" 2 "Midpoint" 3 "Farmer") size(medium) subtitle("Ranking of Farm Family")) ///
		yline(0, lcolor(gray) lwidth(thin))
graph export "$repfolder/results/FigureC2.pdf", replace
