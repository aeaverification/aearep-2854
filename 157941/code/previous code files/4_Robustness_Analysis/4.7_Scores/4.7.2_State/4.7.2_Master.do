cd "$repfolder/data/analysis"

do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/DistributionsFarmFam_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/RankGraphsRecatProbit_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/OccupationalUpgradingGraphsMidpoint_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/DeltaRankRegressionsRecatProbit_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/DeltaRankRegressionsRecatProbitMidpoint_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/DeltaRankRegressionsRecatProbitUpper_State.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.2_State/GraphUnconditionalConditionalResults_State.do"
