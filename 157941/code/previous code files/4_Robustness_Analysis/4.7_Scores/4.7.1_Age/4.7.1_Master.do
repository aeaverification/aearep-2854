cd "$repfolder/data/analysis"

do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/DistributionsFarmFam_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/RankGraphsRecatProbit_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/OccupationalUpgradingGraphsMidpoint_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/DeltaRankRegressionsRecatProbit_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/DeltaRankRegressionsRecatProbitMidpoint_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/DeltaRankRegressionsRecatProbitUpper_Age.do"
do "$repfolder/code/4_Robustness_Analysis/4.7_Scores/4.7.1_Age/GraphUnconditionalConditionalResults_Age.do"
