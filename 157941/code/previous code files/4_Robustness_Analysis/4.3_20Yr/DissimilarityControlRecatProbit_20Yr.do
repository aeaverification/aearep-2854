cd "$repfolder/data/analysis"

/////////////////////////////////

use FileRecatProbit_1850_1870.dta, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

reg wc_50 foreign c.age_50##c.age_50##c.age_50##c.age_50 if wc_50~=. & wc_70~=. [aw=1/link_prob_midpoint_50]
	local b1=_b[foreign]
	est sto wc
reg farmer_50 foreign c.age_50##c.age_50##c.age_50##c.age_50 if farmer_50~=. & farmer_70~=. [aw=1/link_prob_midpoint_50]
	local b2=_b[foreign]
	est sto farmer
reg craft_50 foreign c.age_50##c.age_50##c.age_50##c.age_50 if craft_50~=. & craft_70~=. [aw=1/link_prob_midpoint_50]
	local b3=_b[foreign]
	est sto craft
reg operative_50 foreign c.age_50##c.age_50##c.age_50##c.age_50 if operative_50~=. & operative_70~=. [aw=1/link_prob_midpoint_50]
	local b4=_b[foreign]
	est sto operative
reg unskill_50 foreign c.age_50##c.age_50##c.age_50##c.age_50 if unskill_50~=. & unskill_70~=. [aw=1/link_prob_midpoint_50]
	local b5=_b[foreign]
	global n50: di %7.0fc e(N)
	est sto unskill
	
global d50=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d50

reg wc_70 foreign c.age_70##c.age_70##c.age_70##c.age_70 if wc_50~=. & wc_70~=. [aw=1/link_prob_midpoint_50]
	local b1=_b[foreign]
	est sto wc
reg farmer_70 foreign c.age_70##c.age_70##c.age_70##c.age_70 if farmer_50~=. & farmer_70~=. [aw=1/link_prob_midpoint_50]
	local b2=_b[foreign]
	est sto farmer
reg craft_70 foreign c.age_70##c.age_70##c.age_70##c.age_70 if craft_50~=. & craft_70~=. [aw=1/link_prob_midpoint_50]
	local b3=_b[foreign]
	est sto craft
reg operative_70 foreign c.age_70##c.age_70##c.age_70##c.age_70 if operative_50~=. & operative_70~=. [aw=1/link_prob_midpoint_50]
	local b4=_b[foreign]
	est sto operative
reg unskill_70 foreign c.age_70##c.age_70##c.age_70##c.age_70 if unskill_50~=. & unskill_70~=. [aw=1/link_prob_midpoint_50]
	local b5=_b[foreign]
	global n70: di %7.0fc e(N)
	est sto unskill
	
global d70=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d70

capture postclose outfile
postfile outfile span diss0 diss1 year0 year1 using DissimilarityRecatProbit_20Yr, replace
post outfile (1) ($d50) ($d70) (1850) (1870)

/////////////////////////////////

use File_1880_1900.dta, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

reg wc_80 foreign c.age_80##c.age_80##c.age_80##c.age_80 if wc_80~=. & wc_00~=. [aw=1/link_prob_midpoint_00]
	local b1=_b[foreign]
	est sto wc
reg farmer_80 foreign c.age_80##c.age_80##c.age_80##c.age_80 if farmer_80~=. & farmer_00~=. [aw=1/link_prob_midpoint_00]
	local b2=_b[foreign]
	est sto farmer
reg craft_80 foreign c.age_80##c.age_80##c.age_80##c.age_80 if craft_80~=. & craft_00~=. [aw=1/link_prob_midpoint_00]
	local b3=_b[foreign]
	est sto craft
reg operative_80 foreign c.age_80##c.age_80##c.age_80##c.age_80 if operative_80~=. & operative_00~=. [aw=1/link_prob_midpoint_00]
	local b4=_b[foreign]
	est sto operative
reg unskill_80 foreign c.age_80##c.age_80##c.age_80##c.age_80 if unskill_80~=. & unskill_00~=. [aw=1/link_prob_midpoint_00]
	local b5=_b[foreign]
	global n80: di %7.0fc e(N)
	est sto unskill
	
global d80=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d80

reg wc_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if wc_80~=. & wc_00~=. [aw=1/link_prob_midpoint_00]
	local b1=_b[foreign]
	est sto wc
reg farmer_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if farmer_80~=. & farmer_00~=. [aw=1/link_prob_midpoint_00]
	local b2=_b[foreign]
	est sto farmer
reg craft_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if craft_80~=. & craft_00~=. [aw=1/link_prob_midpoint_00]
	local b3=_b[foreign]
	est sto craft
reg operative_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if operative_80~=. & operative_00~=. [aw=1/link_prob_midpoint_00]
	local b4=_b[foreign]
	est sto operative
reg unskill_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if unskill_80~=. & unskill_00~=. [aw=1/link_prob_midpoint_00]
	local b5=_b[foreign]
	global n00: di %7.0fc e(N)
	est sto unskill
	
global d00=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d00

post outfile (2) ($d80) ($d00) (1880) (1900)

/////////////////////////////////

use File_1900_1920.dta, clear

_pctile link_prob_midpoint_20, percentiles(0.5)
replace link_prob_midpoint_20=r(r1) if link_prob_midpoint_20<r(r1)

reg wc_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if wc_00~=. & wc_20~=. [aw=1/link_prob_midpoint_20]
	local b1=_b[foreign]
	est sto wc
reg farmer_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if farmer_00~=. & farmer_20~=. [aw=1/link_prob_midpoint_20]
	local b2=_b[foreign]
	est sto farmer
reg craft_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if craft_00~=. & craft_20~=. [aw=1/link_prob_midpoint_20]
	local b3=_b[foreign]
	est sto craft
reg operative_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if operative_00~=. & operative_20~=. [aw=1/link_prob_midpoint_20]
	local b4=_b[foreign]
	est sto operative
reg unskill_00 foreign c.age_00##c.age_00##c.age_00##c.age_00 if unskill_00~=. & unskill_20~=. [aw=1/link_prob_midpoint_20]
	local b5=_b[foreign]
	global n00: di %7.0fc e(N)
	est sto unskill
	
global d00=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d00

reg wc_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if wc_00~=. & wc_20~=. [aw=1/link_prob_midpoint_20]
	local b1=_b[foreign]
	est sto wc
reg farmer_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if farmer_00~=. & farmer_20~=. [aw=1/link_prob_midpoint_20]
	local b2=_b[foreign]
	est sto farmer
reg craft_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if craft_00~=. & craft_20~=. [aw=1/link_prob_midpoint_20]
	local b3=_b[foreign]
	est sto craft
reg operative_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if operative_00~=. & operative_20~=. [aw=1/link_prob_midpoint_20]
	local b4=_b[foreign]
	est sto operative
reg unskill_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if unskill_00~=. & unskill_20~=. [aw=1/link_prob_midpoint_20]
	local b5=_b[foreign]
	global n20: di %7.0fc e(N)
	est sto unskill
	
global d20=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d20

post outfile (3) ($d00) ($d20) (1900) (1920)
	
/////////////////////////////////

use File_1910_1930.dta, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

reg wc_10 foreign c.age_10##c.age_10##c.age_10##c.age_10 if wc_10~=. & wc_30~=. [aw=1/link_prob_midpoint_30]
	local b1=_b[foreign]
	est sto wc
reg farmer_10 foreign c.age_10##c.age_10##c.age_10##c.age_10 if farmer_10~=. & farmer_30~=. [aw=1/link_prob_midpoint_30]
	local b2=_b[foreign]
	est sto farmer
reg craft_10 foreign c.age_10##c.age_10##c.age_10##c.age_10 if craft_10~=. & craft_30~=. [aw=1/link_prob_midpoint_30]
	local b3=_b[foreign]
	est sto craft
reg operative_10 foreign c.age_10##c.age_10##c.age_10##c.age_10 if operative_10~=. & operative_30~=. [aw=1/link_prob_midpoint_30]
	local b4=_b[foreign]
	est sto operative
reg unskill_10 foreign c.age_10##c.age_10##c.age_10##c.age_10 if unskill_10~=. & unskill_30~=. [aw=1/link_prob_midpoint_30]
	local b5=_b[foreign]
	global n10: di %7.0fc e(N)
	est sto unskill
	
global d10=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d10

reg wc_30 foreign c.age_30##c.age_30##c.age_30##c.age_30 if wc_10~=. & wc_30~=. [aw=1/link_prob_midpoint_30]
	local b1=_b[foreign]
	est sto wc
reg farmer_30 foreign c.age_30##c.age_30##c.age_30##c.age_30 if farmer_10~=. & farmer_30~=. [aw=1/link_prob_midpoint_30]
	local b2=_b[foreign]
	est sto farmer
reg craft_30 foreign c.age_30##c.age_30##c.age_30##c.age_30 if craft_10~=. & craft_30~=. [aw=1/link_prob_midpoint_30]
	local b3=_b[foreign]
	est sto craft
reg operative_30 foreign c.age_30##c.age_30##c.age_30##c.age_30 if operative_10~=. & operative_30~=. [aw=1/link_prob_midpoint_30]
	local b4=_b[foreign]
	est sto operative
reg unskill_30 foreign c.age_30##c.age_30##c.age_30##c.age_30 if unskill_10~=. & unskill_30~=. [aw=1/link_prob_midpoint_30]
	local b5=_b[foreign]
	global n30: di %7.0fc e(N)
	est sto unskill
	
global d30=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d30

post outfile (4) ($d10) ($d30) (1910) (1930)

/////////////////////////////////

use File_1920_1940.dta, clear

_pctile link_prob_midpoint_20, percentiles(0.5)
replace link_prob_midpoint_20=r(r1) if link_prob_midpoint_20<r(r1)

reg wc_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if wc_20~=. & wc_40~=. [aw=1/link_prob_midpoint_20]
	local b1=_b[foreign]
	est sto wc
reg farmer_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if farmer_20~=. & farmer_40~=. [aw=1/link_prob_midpoint_20]
	local b2=_b[foreign]
	est sto farmer
reg craft_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if craft_20~=. & craft_40~=. [aw=1/link_prob_midpoint_20]
	local b3=_b[foreign]
	est sto craft
reg operative_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if operative_20~=. & operative_40~=. [aw=1/link_prob_midpoint_20]
	local b4=_b[foreign]
	est sto operative
reg unskill_20 foreign c.age_20##c.age_20##c.age_20##c.age_20 if unskill_20~=. & unskill_40~=. [aw=1/link_prob_midpoint_20]
	local b5=_b[foreign]
	global n20: di %7.0fc e(N)
	est sto unskill
	
global d20=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d20

reg wc_40 foreign c.age_40##c.age_40##c.age_40##c.age_40 if wc_20~=. & wc_40~=. [aw=1/link_prob_midpoint_20]
	local b1=_b[foreign]
	est sto wc
reg farmer_40 foreign c.age_40##c.age_40##c.age_40##c.age_40 if farmer_20~=. & farmer_40~=. [aw=1/link_prob_midpoint_20]
	local b2=_b[foreign]
	est sto farmer
reg craft_40 foreign c.age_40##c.age_40##c.age_40##c.age_40 if craft_20~=. & craft_40~=. [aw=1/link_prob_midpoint_20]
	local b3=_b[foreign]
	est sto craft
reg operative_40 foreign c.age_40##c.age_40##c.age_40##c.age_40 if operative_20~=. & operative_40~=. [aw=1/link_prob_midpoint_20]
	local b4=_b[foreign]
	est sto operative
reg unskill_40 foreign c.age_40##c.age_40##c.age_40##c.age_40 if unskill_20~=. & unskill_40~=. [aw=1/link_prob_midpoint_20]
	local b5=_b[foreign]
	global n40: di %7.0fc e(N)
	est sto unskill
	
global d40=0.5*(abs(`b1')+abs(`b2')+abs(`b3')+abs(`b4')+abs(`b5'))
di $d40

post outfile (5) ($d20) ($d40) (1920) (1940)
postclose outfile

//////////////////////////
use DissimilarityRecatProbit_20Yr, clear
reshape long diss year, i(span) j(final)

twoway ///
	line diss year if span==1, lcolor(gray) lpattern(-) ///
|| ///
	line diss year if span==2, lcolor(gray) lpattern(-) ///
|| ///
	line diss year if span==3, lcolor(gray) lpattern(-) ///
|| ///
	line diss year if span==4, lcolor(gray) lpattern(-) ///
|| ///
	line diss year if span==5, lcolor(gray) lpattern(-) ///
|| ///
	connected diss year if final==0, lcolor(black) mcolor(black) lwidth(thick) ///
|| ///
	connected diss year if final==1, lcolor(black) lpattern(_) mcolor(black) lwidth(thick) ///
, graphregion(color(white)) ///
		ylabel(,angle(0) glwidth(0) labsize(medium)) xlabel(, labsize(medium)) ///
		xtitle("Year", size(medium)) ytitle("Dissimilarity Index", size(medium)) ///
		legend(region(lcolor(white)) order(6 "Initial" 7 "Final") size(medium))
graph export "$repfolder/results/FigureF3.pdf", replace
		

