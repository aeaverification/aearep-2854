global repfolder "SET FOLDER" //Must be updated for each system

ssc install texdoc
ssc install coefplot

cd "$repfolder/data/analysis"

global simple=1 //Under this setting, probits to compute linkage probabilities for reweighting will not be run, and will instead be merged in from provided files. Set to 0 instead to run the probits. Note that doing this will overwrite the provided crosswalks, so if you wish to retain them, they should be moved or renamed.

do "$repfolder/code/1_Create_Files/1_Master.do"
do "$repfolder/code/2_Main_Analysis/2_Master.do"
do "$repfolder/code/3_Robustness_Analysis/3_Master.do"
