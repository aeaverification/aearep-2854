cd "$repfolder/data/analysis"

import excel using "$repfolder/data/confidential/hsus_data/Aa6-8.xls", cellrange(A7:C217) clear
rename A year
rename C pop
drop B
destring year, replace
tempfile pop
save `pop'

import excel using "$repfolder/data/confidential/hsus_data/Ad106-120.xls", cellrange(A7:AA186) clear
drop B D G I K M O Q T W Z
foreach x of varlist A-AA {
	destring `x', force replace
}
preserve
keep A C
rename A year
rename C migr
drop if year==.
merge 1:1 year using `pop'
drop if _merge==2
drop _merge

gen share=(migr/pop)/1000

twoway ///
	line share year if year<=1930, lcolor(black) ///
,graphregion(color(white)) ///
	xtitle("Year") ylabel(,angle(0) glwidth(0)) ///
	legend(off) ytitle("Immigrants as Share of Population") ///
	xlabel(1810(10)1940, angle(25)) xscale(range(1810 1940))
graph export "$repfolder/results/Figure1.pdf", replace
