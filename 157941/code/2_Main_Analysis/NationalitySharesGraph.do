cd "$repfolder/data/analysis"

use InitialNationalityDistributionRecatProbit1850, clear

merge 1:1 bpl using InitialNationalityDistribution1870
drop _merge

merge 1:1 bpl using InitialNationalityDistribution1880
drop _merge

merge 1:1 bpl using InitialNationalityDistribution1900
drop _merge

merge 1:1 bpl using InitialNationalityDistribution1910
drop _merge

reshape long frac, i(bpl) j(year)

replace bpl=410 if bpl==411 | bpl==412
collapse (sum) frac, by(year bpl)

twoway ///
	connected frac year if bpl==405, lcolor(black) mcolor(black) /// Sweden
|| ///
	connected frac year if bpl==410, lcolor(black) mcolor(black) lpattern(-) msymbol(S) /// UK 
|| ///
	connected frac year if bpl==414, lcolor(black) mcolor(black) lpattern(-.) msymbol(D) /// Ireland
|| ///
	connected frac year if bpl==434, lcolor(black) mcolor(black) lpattern(_) msymbol(T) /// Italy 
|| ///
	connected frac year if bpl==450, lcolor(black) mcolor(black) lpattern(_.) msymbol(+) /// Austria
|| ///
	connected frac year if bpl==453, lcolor(gray) mcolor(gray) msymbol(X) /// Germany 
|| ///
	connected frac year if bpl==465, lcolor(gray) mcolor(gray) lpattern(-) msymbol(Oh) /// Russia
, graphregion(color(white)) ///
		ylabel(,angle(0) glwidth(0)) ///
		xtitle("Initial Year") ytitle("Share of Foreign-Born") ///
		legend(region(lcolor(white)) order(1 "Sweden" 2 "UK" 3 "Ireland" 4 "Italy" 5 "Austria" 6 "Germany" 7 "Russia"))
graph export "$repfolder/results/Figure3.pdf", replace
