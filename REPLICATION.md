# [AEJApp-2021-0008.R2] [Working Their Way Up? US Immigrants' Changing Labor Market Assimilation in the Age of Mass Migration] Validation and Replication results

> Some useful links:
> - [Official Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code)
> - [Step by step guidance](https://aeadataeditor.github.io/aea-de-guidance/) 
> - [Template README](https://social-science-data-editors.github.io/template_README/)

## SUMMARY

Thank you for your replication archive. This report follows up on the previous report, after we worked with authors to get a full run completed. We were still unable to run some of the logistic regressions, due to some fine-tuning that is both data- and possibly  platform-dependent. We were able to replicate all tables and figures using the author-provided IPUMS USA extracts and (pre-computed) logit_probabilities files. The probit data preparation programs were not run. We encountered many bugs in the code that are described in detail in the replication steps. 

**Conditional on making the requested changes to the openICPSR deposit prior to publication, the replication package is accepted.**

In assessing compliance with our [Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code), we have identified the following issues, which we ask you to address:

### New Action Items

- [REQUIRED] Please amend README to contain complete requirements. 
  - SAS version and missing Stata package

### Previously

#### Unresolved

> [WE SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended), in order to improve findability of your data and code supplement. 

- Not done.

> [WE SUGGESTED] We suggest you update the openICPSR metadata fields marked as (suggested), in order to improve findability of your data and code supplement. 

- Not done.

#### Resolved

> [WE REQUESTED] Please provide debugged code, addressing the issues identified in this report.
  - Make it clear in the README when the logit probabilities are recomputed - ideally, a global switch.

- Done. An updated version of code is provided.

> [WE REQUESTED] Please add clarifying language to README regarding download of external files, fragility of IPUMS files (due to revisions).
  - If not already done, please also note that the IPUMS files that replicators might obtain from IPUMS as of Oct 2022 will likely not work "as-is" and will require adjustment (due to changes in coding by IPUMS). If possible, note where adjustments might need to be made.

- Done. IPUMS files are noted in the README.

> [WE REQUESTED] Please amend README to contain complete requirements. 

- Done. SAS version is included in README.

> [We REQUESTED] Please provide debugged code, addressing the issues identified in this report.

- Done. An updated version of code is provided.

> [WE REQUESTED] Please add data citations to the **article**. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Done.

> [WE REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. The author now provides a link to the download the data and states it requires a subscription. 

> [WE REQUESTED] Please add the DOI to the citations.

- Done. The DOI is now included in the citation.

> [WE REQUESTED] Please review the title of the openICPSR deposit as per our guidelines (below).

- Done. The title now conforms to guidance. 

> [WE SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

- Done. 


> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.

> [NOTE] Since July 1, 2021, we will publish replication packages as soon as all requested changes to the deposit have been made. Please process any requested changes as soon as possible.

## General

- [x] Data Availability and Provenance Statements
  - [x] Statement about Rights
  - [ ] License for Data
  - [x] Details on each Data Source
- [x] Dataset list
- [x] Computational requirements
  - [x] Software Requirements
  - [ ] Controlled Randomness
  - [x] Memory and Runtime Requirements
- [x] Description of programs/code
  - [ ] (Optional, but recommended) License for Code
- [x] Instructions to Replicators
  - [ ] Details
- [x] List of tables and programs
- [x] References

> [WE SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

- Done. 

## Data description

### Data Sources

#### Census Linkage Project

- The datasets can be downloaded from the link provided in the references section of the manuscript. 
- Data cited in README and the References section of the manuscript. Each dataset is cited separately.

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Santiago Pérez, and Myera Rashid (2020). Census Linking Project: Version 2.0 [Machine-readable database]. http://censuslinkingproject.org (accessed December 16, 2021).

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Myera Rashid, and Santiago Pérez (2022a). Census Linking Project: 1850-1880 Crosswalk. [Machine-readable database]. doi:10.7910/DVN/ZEBJRO, Harvard Dataverse, V2.

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Myera Rashid, and Santiago Pérez (2022b). Census Linking Project: 1870-1900 Crosswalk. [Machine-readable database]. doi:10.7910/DVN/8WMSZ7, Harvard Dataverse, V2.

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Myera Rashid, and Santiago Pérez (2022c). Census Linking Project: 1880-1910 Crosswalk. [Machine-readable database]. doi:10.7910/DVN/ISGRMS, Harvard Dataverse, V2.

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Myera Rashid, and Santiago Pérez (2022d). Census Linking Project: 1900-1930 Crosswalk. [Machine-readable database]. doi:10.7910/DVN/ERFWIU, Harvard Dataverse, V2.

> Abramitzky, Ran, Leah Boustan, Katherine Eriksson, Myera Rashid, and Santiago Pérez (2022e). Census Linking Project: 1910-1940 Crosswalk. [Machine-readable database]. doi:10.7910/DVN/WRGXP0, Harvard Dataverse, V2.

> [WE REQUESTED] Please add data citations to the **article**. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

When citing in the manuscript, it is fine to consolidate this as you have into a single citation, but the README should list the DOI for each of the files.

- Done. The data citation was added to the main references section. The README now lists citations with the DOI for each file. 

> [WE NOTED] Just to be clear: the Census Linking Project files are *not* confidential. This might warrant a short clarification in the README.

- Done. This data is no longer referred to as confidential in the README. 

#### Historical Statistics of the United States

- The dataset is restricted and not provided in the replication materials.
- In particular, replicators should download Aa6-8.xls from Haines and Sutch (2006) and Ad106-120.xls from Barde, Carter, and Sutch (2006) from https://hsus.cambridge.org/HSUSWeb and place them in this folder; however, this requires a subscription. 
- The Haines data can be accessed from the following link by searching "Population 1790-2000" and selecting the file with series id "Aa6-8":
"https://hsus.cambridge.org/HSUSWeb/search/searchmod.do".
- The Barde data can be accessed at the link the following link by searching "Immigrants by country of last residence-Europe 1820-1997" and selecting the file with series id "Ad106-120":"https://hsus.cambridge.org/HSUSWeb/search/searchmod.do".
- Data is cited.

> Barde, Robert, Susan B. Carter, and Richard Sutch (2006). “Table Ad106-120: Immigrants, by country of last residence—Europe, 1820-1997.” In Historical Statistics of the United States. Susan B. Carter, Scott Sigmund Gardner, Michael R. Haines, Alan L. Olmstead, Richard Sutch, and Gavin Wright (eds.). Cambridge: Cambridge University Press, pp. 1.560-1.563. doi:10.1017/ISBN-9780511132971.Ad90-221.

> Haines, Michael R. and Richard Sutch (2006). “Table Aa6-8: Population: 1790-2000 [Annual Estimates].” In Historical Statistics of the United States. Susan B. Carter, Scott Sigmund Gardner, Michael R. Haines, Alan L. Olmstead, Richard Sutch, and Gavin Wright (eds.). Cambridge: Cambridge University Press, pp. 1.28-1.30. doi:10.1017/ISBN-9780511132971.Aa1-109.

> [WE REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. The author now provides a link to the download the data and states it requires a subscription. 

> [WE REQUESTED] Please add the DOI to the citations.

- Done. The DOI is now included in the citation. 

#### IPUMS USA Ruggles et al. 2020

- The dataset is restricted (full-count) and not provided in the replication materials.
- Access conditions are not described, an IPUMS account is required.
- The dataset can be downloaded by the replicators from IPUMS using the provided codebook.
  - However, each dataset has around 100 variables. We strongly urge you to publish the JSON version of the extract specification, and/or to reduce the data extract to only those variables used.
- Data is cited.

> Ruggles, Steven, Sarah Flood, Ronald Goeken, Josiah Grover, Erin Meyer, Jose Pacas, and Matthew Sobek (2020). IPUMS USA: Version 10.0 [Machine-readable database]. Minneapolis: University of Minnesota.

> [WE REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

Given the large number of variables (around 100 variables per dataset), please do review whether all variables are needed, to facilitate future replicators ability to download smaller data extracts from IPUMS.

- Done. The README has been updated to include that the data requires an account to download.

#### Preston and Haines (1991)

- The raw dataset is not provided, the authors instead provide a modified analysis dataset.
- The authors do not provide the code to produce the analysis file from the raw dataset or the raw dataset.
- The author should provide a specific data citation on top of the citation of the book.

> Preston, Samuel H. and Michael R. Haines (1991). Fatal Years: Child Mortality in Late Nineteenth Century America. Princeton: Princeton University Press.

> [WE REQUESTED] Please add **data** citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Done. The data citations has been added to the references section of the manuscript. 

> [WE REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. The README has been updated to include that data were manually entered from the physical copy of the Preston and Haines (1991) book.

#### Minnesota Population Center (MPC)

- The raw dataset is not provided, the authors instead provide a modified analysis dataset.
- Confidential IPUMS-Ancestry.com census data obtained from MPC
- Agreement between Vanderbilt University and MPC. These data can be obtained by application to the Minnesota Population Center
- Data is cited: 

> Ruggles, Steven, Sarah Flood, Ronald Goeken, Josiah Grover, Erin Meyer, Jose Pacas, and Matthew Sobek (2020b). IPUMS USA: Version 10.0 [Machine-readable database]. Minneapolis: University of Minnesota.

> [WE REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Done. A data citation has been added to the README and the manuscript.

> [WE REQUESTED] The access description here is commingled with processing instructions. Since these files are the most restrictive to obtain, the access description should be moved to the start of the README (section "Data sources"). It should be very clear from the README that replicators cannot proceed through the entire process without obtaining restricted data that might take weeks or months to obtain.

- Done. The README begins by describing these data files. The author states that they are difficult to obtain and lists the sections that can be replicated without obtaining them. 

### Analysis Data Files

- [ ] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [X] Analysis data files mentioned, provided. File names listed below.

```
./data/raw/PH_inc_occ1950.dta
./data/raw/crosswalk_1850_1870.dta
./data/raw/crosswalk_1850_1880.dta
./data/raw/crosswalk_1870_1900.dta
./data/raw/crosswalk_1880_1900.dta
./data/raw/crosswalk_1880_1910.dta
./data/raw/crosswalk_1900_1920.dta
./data/raw/crosswalk_1900_1930.dta
./data/raw/crosswalk_1910_1930.dta
./data/raw/crosswalk_1910_1940.dta
./data/raw/crosswalk_1920_1940.dta
./data/raw/UnclassifiedOccModes1900.dta
./data/raw/UnclassifiedOccModes1910.dta
./data/raw/UnclassifiedOccModes1920.dta
./data/raw/UnclassifiedOccModes 1930.dta
```

## Data deposit

### Requirements 

- [X] README is in TXT, MD, PDF format
- [X] openICPSR deposit has no ZIP files
- [x] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [X] Authors (with affiliations) are listed in the same order as on the paper

> [WE REQUESTED] Please review the title of the openICPSR deposit as per our guidelines (below).

- Done. The title now conforms to guidance. 

> Detailed guidance is at [https://aeadataeditor.github.io/aea-de-guidance/](https://aeadataeditor.github.io/aea-de-guidance/). 


### Deposit Metadata

- [X] JEL Classification (required)
- [X] Manuscript Number (required)
- [ ] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [ ] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [ ] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

- [NOTE] openICPSR metadata is sufficient.

> [WE SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended), in order to improve findability of your data and code supplement. 

> [WE SUGGESTED] We suggest you update the openICPSR metadata fields marked as (suggested), in order to improve findability of your data and code supplement. 

- Not done.

For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

## Data checks

- Data can be read using the software indicated by the author.
- Datasets are in archive-ready formats.
- The do files have variable labels.
- There is no PII concern.

## Code description

There are 512 files Stata and sas files, including a "master.do".

- All tables and figures map to code. The authors provided mapping for main figures and Appendix A and B in README. 
- The code in 4_Robustness_Analysis creates the results in Appendices C, D, F, G, H, I, J, and K. There are a large number of files, so the authors do not provide mapping for those figures, but it is analogous to the mapping provided for Appendix A and B. 

- [X] The replication package contains a "main" or "master" file(s) which calls all other auxiliary programs.


| Data Preparation Program | Dataset created |
|--------------------------|-----------------|
| 0_Crosswalks_Creation    | crosswalks      |

| Figure      | Program                                    | Line Number |
|-------------|--------------------------------------------|-------------|
| Figure 1    | ImmDescr.do                                | 33          |
| Figure 2a   | OccupationTrends.do                        | 426         |
| Figure 2b   | OccupationTrends.do                        | 438         |
| Figure 3    | NationalitySharesGraph.do                  | 40          |
| Figure 4a   | DistributionsFarmFam.do                    | 49          |
| Figure 4b   | DistributionsFarmFam.do                    | 63          |
| Figure 4c   | DistributionsFarmFam.do                    | 148         |
| Figure 4d   | DistributionsFarmFam.do                    | 162         |
| Figure 4e   | DistributionsFarmFam.do                    | 247         |
| Figure 4f   | DistributionsFarmFam.do                    | 261         |
| Figure 4g   | DistributionsFarmFam.do                    | 346         |
| Figure 4h   | DistributionsFarmFam.do                    | 360         |
| Figure 4i   | DistributionsFarmFam.do                    | 445         |
| Figure 4j   | DistributionsFarmFam.do                    | 459         |
| Figure 5    | RankGraphsRecatProbit.do                   | 229         |
| Figure 6    | DissimilarityControlRecatProbit.do         | 545         |
| Figure 7    | GraphUnconditionalConditionalResults.do    | 24          |
| Figure 8    | GraphUnconditionalConditionalResults.do    | 37          |
| Figure 9a   | OccupationalUpgradingGraphsMidpoint.do     | 65          |
| Figure 9b   | OccupationalUpgradingGraphsMidpoint.do     | 213         |
| Figure 9c   | OccupationalUpgradingGraphsMidpoint.do     | 494         |
| Figure 9d   | OccupationalUpgradingGraphsMidpoint.do     | 775         |
| Figure 9e   | OccupationalUpgradingGraphsMidpoint.do     | 1058        |
| Figure 10   | DeltaRankRegressionsRecatProbitMidpoint.do | 661         |
| Figure 11   | DeltaRankRegressionsRecatProbitMidpoint.do | 622         |
| Figure 12   | DeltaRankRegressionsRecatProbitMidpoint.do | 696         |
| Figure A.1a | OccupationalUpgradingGraphs.do             | 65          |
| Figure A.1b | OccupationalUpgradingGraphs.do             | 129         |
| Figure A.1c | OccupationalUpgradingGraphs.do             | 193         |
| Figure A.1d | OccupationalUpgradingGraphs.do             | 257         |
| Figure A.1e | OccupationalUpgradingGraphs.do             | 321         |
| Figure A.2a | OccupationalUpgadingGraphsUpper.do         | 65          |
| Figure A.2b | OccupationalUpgadingGraphsUpper.do         | 129         |
| Figure A.2c | OccupationalUpgadingGraphsUpper.do         | 193         |
| Figure A.2d | OccupationalUpgadingGraphsUpper.do         | 257         |
| Figure A.2e | OccupationalUpgadingGraphsUpper.do         | 321         |
| Figure A.3a | DeltaRankRegressionsRecatProbit.do         | 661         |
| Figure A.3b | DeltaRankRegressionsRecatProbitUpper.do    | 661         |
| Figure A.4a | DeltaRankRegressionsRecatProbit.do         | 622         |
| Figure A.4b | DeltaRankRegressionsRecatProbitUpper.do    | 622         |
| Figure A.5a | DeltaRankRegressionsRecatProbit.do         | 696         |
| Figure A.5b | DeltaRankRegressionsRecatProbitUpper.do    | 696         |
| Figure A.5c | DeltaRankRegressionsRecatProbitUpper.do    | 4           |
| Figure A.6a | OccupationalUpgradingGraphsMidpoint.do     | 149         |
| Figure A.6b | OccupationalUpgradingGraphsMidpoint.do     | 430         |
| Figure A.6c | OccupationalUpgradingGraphsMidpoint.do     | 711         |
| Figure A.6d | OccupationalUpgradingGraphsMidpoint.do     | 994         |
| Figure A.6e | OccupationalUpgradingGraphsMidpoint.do     | 1277        |
| Figure B.1a | ObservablesLinkageGraphs.do                | 44          |
| Figure B.1b | ObservablesLinkageGraphs.do                | 101         |
| Figure B.1c | ObservablesLinkageGraphs.do                | 158         |
| Figure B.1d | ObservablesLinkageGraphs.do                | 215         |
| Figure B.1e | ObservablesLinkageGraphs.do                | 272         |
| Figure B.2a | ObservablesLinkageGraphs.do                | 53          |
| Figure B.2b | ObservablesLinkageGraphs.do                | 110         |
| Figure B.2c | ObservablesLinkageGraphs.do                | 167         |
| Figure B.2d | ObservablesLinkageGraphs.do                | 224         |
| Figure B.2e | ObservablesLinkageGraphs.do                | 281         |
| appendix figures |                                       |             |

| Table #    | Program                                    | Line Number |
|------------|--------------------------------------------|-------------|
| Table A.1  | SummaryStatsTable.do                       | 286         |
| Table A.2  | DissimilarityControlRecatProbit.do         | 89          |
| Table A.3  | ResultsRecatProbitMidpoint.do              | 24          |
| Table A.4a | ResultsRecatProbit.do                      | 24          |
| Table A.4b | ResultsRecatProbitMidpoint.do              | 338         |
| Table A.4c | ResultsRecatProbitUpper.do                 | 24          |
| Table A.5a | DeltaRankRegressionsRecatProbit.do         | 4           |
| Table A.5b | DeltaRankRegressionsRecatProbitMidpoint.do | 4           |
| Table A.5c | DeltaRankRegressionsRecatProbitUpper.do    | 4           |
| Table B.1  | ObservablesLinkageGraphs.do                | 272         |
| Table B.2  | ObservablesLinkageGraphs.do                | 281         |


## Stated Requirements

- [ ] No requirements specified
- [X] Software Requirements specified as follows:
  - Stata 17/MP
- [x] Computational Requirements specified as follows:
  - Late 2020 iMac with a 3.6 GHz 10-core Intel Core i9 processor with 40 GB memory
- [X] Time Requirements specified as follows:
  - 1-2 days

- [x] Requirements are complete.

> Author's stated requirements:

- For construction of crosswalks: "We executed the provided code at various times in 2019, 2020, and 2021 on Vanderbilt University’s Advanced Computing Cluster for Research and Education (ACCRE) using Stata-SE 14 and various versions of SAS. These scripts can take a good deal of time to run, often several days each (though many can often be run simultaneously) depending on the capabilities of the system in question."

## Missing Requirements

- [X] Software Requirements 
  - [x] Stata
    - missing packages: coefplot
  - [x] SAS
    - [x] Version (this can be obtained from the log files)

> [REQUIRED] Please amend README to contain complete requirements. 

- Mostly done. SAS version is still not included in README and a Stata package is missing.


## Computing Environment of the Replicator

- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Stata/MP 17
- R-4.2.0

## Replication steps

1. Downloaded code from openICPSR deposit. 
2. Downloaded all necessary data per README instructions. 
   - Deviation: We used the JSON files provided to us by the IPUMS staff based on your extracts. Created txt/JSON files with extract definitions for the IPUMS USA data and ran the program `ipums_extract.R` as per the IPUMS API documentation to obtain IPUMS data. (see copy in the appendix) 
   - Note: **when attempting to submit the 1930 extract, the variable "language" was not found in the sample**, so it was removed from the extract definition file.
3. Added the config.do generating system information.
4. Ran master file per README. Encountered error: `no observations r(2000)`. Error sourced to `CreateOccRanks_1870.do`. 
5. We thought perhaps the error came from the files in the folder "1_Import_IPUMS" incorrectly reading in the ipums 1870 dataset. Downloaded the .dta version of the 1870 dataset from IPUMS USA and unzipped it. Created program `Compress_1870.do`. Ran master file again, substituting `Import_1870.do` with `Compress_1870.do`. Received the same error for the subsequent IPUMS dataset. 
6. Attempted another method. Downloaded the Stata command file that corresponds to the ipums 1870 dataset from the IPUMS USA website. Substituted `Import_1870.do` with that program (`Import_1870_ipums_version.do`) to read in the .dat file. Ran master file. Received the same error for the subsequent dataset. 
7. Used method in step 6 for all IPUMS datasets in all years. Ran master file with the new command files. Received error: 
> convergence not achieved r(430);
- unable to debug. error sourced to `./code/2_Create_Files/2.5_Create_Robustness_Files/2.5.1_Alternate_Methods/2.5.1.3_Int/Create_1880_1910_Int.do`
8. New IPUMS DTA files provided by authors, and code was updated to accommodate them. Downloaded the DTA files from Box link. Created and ran program `unzip_bz2_ipums_files.R` to unzip the dta files.
9. Ran master file. Received the same error (`convergence not achieved r(430)`). 
10. To ensure that there was no overlap between data from previous runs, the deposit was downloaded again into a new folder with the new data provided by the authors, and the master file was run. Received the same error: 
> `convergence not achieved r(430)`
11. Attempted another replication run using the same IPUMS DTA files provided by author from last attempt, as well as the linkage_probabilities provided by the author (probit runs were skipped). 
  - Received error: `file //rschfs1x/userRS/F-J/ik273_RS/Documents/Workspace/aearep-2854/157941-3/data/confidential/hsus_data/Aa6-8.xls not found`
  - Error sourced to `code/2_Main_Analysis/ImmDescr.do`
12. Changed `TableAa6-8.xls` to `Aa6-8.xls` and `TableAd106-120.xls` to `Ad106-120.xls`.
13. Ran master file, starting at `2_Master.do`. Received error: 
> `import excel using "$repfolder/data/confidential/hsus_data/Aa6-8.xls", cellrange(A7:C217) clear`
> `invalid row range in cellrange() option lower right row is after data area in worksheet r(198);`
- Error due to the format of `Aa6-8.xls` and `Ad106-120.xls`
14. Redownloaded the two data files from https://hsus.cambridge.org/HSUSWeb. When downloading the data, used the option labeled "Highlight Columns / Rows". The new data files are now in the same format as the author's files.
15. Reran master file, starting at `2_Master.do`. Recieved error that the stata package "coefplot" is missing. Added package to config file.
16. Reran master file, starting at `2_Master.do`. No errors encountered. Code finished running successfully.

> [REQUIRED] Please provide debugged code, addressing the issues identified in this report.

## Findings

### Data preparation programs

- Encountered many errors with data preparation programs. See replication steps.
- After obtaining IPUMS DTA files and linkage_probabilities provided by author, programs ran successfully. Probit runs were skipped.

### Tables

- All tables successfully replicated.

| Table #    | Program                                    | Line Number | Reproduced? |
|------------|--------------------------------------------|-------------|-------------|
| Table A.1  | SummaryStatsTable.do                       | 286         | yes         |
| Table A.2  | DissimilarityControlRecatProbit.do         | 89          | yes         |
| Table A.3  | ResultsRecatProbitMidpoint.do              | 24          | yes         |
| Table A.4a | ResultsRecatProbit.do                      | 24          | yes         |
| Table A.4b | ResultsRecatProbitMidpoint.do              | 338         | yes         |
| Table A.4c | ResultsRecatProbitUpper.do                 | 24          | yes         |
| Table A.5a | DeltaRankRegressionsRecatProbit.do         | 4           | yes         |
| Table A.5b | DeltaRankRegressionsRecatProbitMidpoint.do | 4           | yes         |
| Table A.5c | DeltaRankRegressionsRecatProbitUpper.do    | 4           | yes         |
| Table B.1  | ObservablesLinkageGraphs.do                | 272         | yes         |
| Table B.2  | ObservablesLinkageGraphs.do                | 281         | yes         |

### Figures

- All figures (including appendix figures) were successfully replicated.

| Figure               | Program                                    | Line Number | Reproduced? |
|----------------------|--------------------------------------------|-------------|-------------|
| Figure 1             | ImmDescr.do                                | 33          | yes         |
| Figure 2a            | OccupationTrends.do                        | 426         | yes         |
| Figure 2b            | OccupationTrends.do                        | 438         | yes         |
| Figure 3             | NationalitySharesGraph.do                  | 40          | yes         |
| Figure 4a            | DistributionsFarmFam.do                    | 49          | yes         |
| Figure 4b            | DistributionsFarmFam.do                    | 63          | yes         |
| Figure 4c            | DistributionsFarmFam.do                    | 148         | yes         |
| Figure 4d            | DistributionsFarmFam.do                    | 162         | yes         |
| Figure 4e            | DistributionsFarmFam.do                    | 247         | yes         |
| Figure 4f            | DistributionsFarmFam.do                    | 261         | yes         |
| Figure 4g            | DistributionsFarmFam.do                    | 346         | yes         |
| Figure 4h            | DistributionsFarmFam.do                    | 360         | yes         |
| Figure 4i            | DistributionsFarmFam.do                    | 445         | yes         |
| Figure 4j            | DistributionsFarmFam.do                    | 459         | yes         |
| Figure 5             | RankGraphsRecatProbit.do                   | 229         | yes         |
| Figure 6             | DissimilarityControlRecatProbit.do         | 545         | yes         |
| Figure 7             | GraphUnconditionalConditionalResults.do    | 24          | yes         |
| Figure 8             | GraphUnconditionalConditionalResults.do    | 37          | yes         |
| Figure 9a            | OccupationalUpgradingGraphsMidpoint.do     | 65          | yes         |
| Figure 9b            | OccupationalUpgradingGraphsMidpoint.do     | 213         | yes         |
| Figure 9c            | OccupationalUpgradingGraphsMidpoint.do     | 494         | yes         |
| Figure 9d            | OccupationalUpgradingGraphsMidpoint.do     | 775         | yes         |
| Figure 9e            | OccupationalUpgradingGraphsMidpoint.do     | 1058        | yes         |
| Figure 10            | DeltaRankRegressionsRecatProbitMidpoint.do | 661         | yes         |
| Figure 11            | DeltaRankRegressionsRecatProbitMidpoint.do | 622         | yes         |
| Figure 12            | DeltaRankRegressionsRecatProbitMidpoint.do | 696         | yes         |
| Figure A.1a          | OccupationalUpgradingGraphs.do             | 65          | yes         |
| Figure A.1b          | OccupationalUpgradingGraphs.do             | 129         | yes         |
| Figure A.1c          | OccupationalUpgradingGraphs.do             | 193         | yes         |
| Figure A.1d          | OccupationalUpgradingGraphs.do             | 257         | yes         |
| Figure A.1e          | OccupationalUpgradingGraphs.do             | 321         | yes         |
| Figure A.2a          | OccupationalUpgadingGraphsUpper.do         | 65          | yes         |
| Figure A.2b          | OccupationalUpgadingGraphsUpper.do         | 129         | yes         |
| Figure A.2c          | OccupationalUpgadingGraphsUpper.do         | 193         | yes         |
| Figure A.2d          | OccupationalUpgadingGraphsUpper.do         | 257         | yes         |
| Figure A.2e          | OccupationalUpgadingGraphsUpper.do         | 321         | yes         |
| Figure A.3a          | DeltaRankRegressionsRecatProbit.do         | 661         | yes         |
| Figure A.3b          | DeltaRankRegressionsRecatProbitUpper.do    | 661         | yes         |
| Figure A.4a          | DeltaRankRegressionsRecatProbit.do         | 622         | yes         |
| Figure A.4b          | DeltaRankRegressionsRecatProbitUpper.do    | 622         | yes         |
| Figure A.5a          | DeltaRankRegressionsRecatProbit.do         | 696         | yes         |
| Figure A.5b          | DeltaRankRegressionsRecatProbitUpper.do    | 696         | yes         |
| Figure A.6a          | OccupationalUpgradingGraphsMidpoint.do     | 149         | yes         |
| Figure A.6b          | OccupationalUpgradingGraphsMidpoint.do     | 430         | yes         |
| Figure A.6c          | OccupationalUpgradingGraphsMidpoint.do     | 711         | yes         |
| Figure A.6d          | OccupationalUpgradingGraphsMidpoint.do     | 994         | yes         |
| Figure A.6e          | OccupationalUpgradingGraphsMidpoint.do     | 1277        | yes         |
| Figure B.1a          | ObservablesLinkageGraphs.do                | 44          | yes         |
| Figure B.1b          | ObservablesLinkageGraphs.do                | 101         | yes         |
| Figure B.1c          | ObservablesLinkageGraphs.do                | 158         | yes         |
| Figure B.1d          | ObservablesLinkageGraphs.do                | 215         | yes         |
| Figure B.1e          | ObservablesLinkageGraphs.do                | 272         | yes         |
| Figure B.2a          | ObservablesLinkageGraphs.do                | 53          | yes         |
| Figure B.2b          | ObservablesLinkageGraphs.do                | 110         | yes         |
| Figure B.2c          | ObservablesLinkageGraphs.do                | 167         | yes         |
| Figure B.2d          | ObservablesLinkageGraphs.do                | 224         | yes         |
| Figure B.2e          | ObservablesLinkageGraphs.do                | 281         | yes         |
| appendix figures C-K |                                            |             | yes         |


## Classification

> Full reproduction can include a small number of apparently insignificant changes in the numbers in the table. Full reproduction also applies when changes to the programs needed to be made, but were successfully implemented.
>
> Partial reproduction means that a significant number (>25%) of programs and/or numbers are different.
>
> Note that if some data is confidential and not available, then a partial reproduction applies. This should be noted in the Reasons.
>
> Note that when all data is confidential, it is unlikely that this exercise should have been attempted.
>
> Failure to reproduce: only a small number of programs ran successfully, or only a small number of numbers were successfully generated (<25%). This also applies when all data is restricted-access and none of the **main** tables/figures are run.

- [ ] full reproduction
- [X] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility

- [ ] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [X] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 

---

## Appendix

### Function to download IPUMS data via API

```{r, eval=FALSE}
# function to extract data
extract_from_ipums <- function(year) {
  setwd(extract_defs)
  extract_name <- paste0("usa_extract_",year,".txt") # name extract
  
  extract_definition <- define_extract_from_json(extract_name)
  submitted_extract <- submit_extract(extract_definition)
  submitted_extract <- get_extract_info(submitted_extract)
  downloadable_extract <- wait_for_extract(submitted_extract, max_delay_seconds = 150)
  ddi_file <- download_extract(downloadable_extract, download_dir = extractbase, overwrite=TRUE)
  
  # unzip dat.gz file
  base_filename <- file_path_sans_ext(basename(ddi_file))
  R.utils::gunzip(file.path(paste0(extractbase, "/", base_filename, ".dat.gz")))
  
  # rename file
  setwd(extractbase)
  file.rename(from = paste0(base_filename, ".dat"), to = paste0(year, "_raw.dat")) # rename dat file
  file.rename(from = paste0(base_filename, ".xml"), to = paste0(year, "_raw.xml")) # rename xml file
  
  print(paste("extract completed for", year, sep=" "))
}
```