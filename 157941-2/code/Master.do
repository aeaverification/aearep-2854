include "config.do"
global repfolder "U:/Documents/Workspace/aearep-2854/157941-2" //Must be updated for each system

ssc install texdoc

cd "$repfolder/data/analysis"

do "$repfolder/code/1_Create_Files/1_Master.do"
do "$repfolder/code/2_Main_Analysis/2_Master.do"
do "$repfolder/code/3_Robustness_Analysis/3_Master.do"
