cd "$repfolder/data/analysis"

/////////////////////////////

use FileRecatProbit_1850_1880_Impute, clear
merge 1:1 histid_50 using RescoredRecatProbit_1850, keep(1 3)
gen rescored_50=(_merge==3)
drop _merge
replace unskill_50=0 if rescored_50==1

merge 1:1 histid_80 using Rescored_1880, keep(1 3)
gen rescored_80=(_merge==3)
drop _merge
replace unskill_80=0 if rescored_80==1

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen occ_cat_50=1 if wc_50==1
replace occ_cat_50=2 if farmer_50==1
replace occ_cat_50=3 if craft_50==1
replace occ_cat_50=4 if operative_50==1
replace occ_cat_50=5 if unskill_50==1
replace occ_cat_50=6 if rescored_50==1

gen occ_cat_80=1 if wc_80==1
replace occ_cat_80=2 if farmer_80==1
replace occ_cat_80=3 if craft_80==1
replace occ_cat_80=4 if operative_80==1
replace occ_cat_80=5 if unskill_80==1
replace occ_cat_80=6 if rescored_80==1

gen p=1
collapse (sum) p [pw=1/link_prob_midpoint_50], by(occ_cat_50 occ_cat_80 foreign)
drop if occ_cat_50==. | occ_cat_80==.

preserve
collapse (sum) p, by(occ_cat_50 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_50) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1a.pdf", replace
	
restore
collapse (sum) p, by(occ_cat_80 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_80) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1b.pdf", replace

use FileRecatProbit_1850_1880_Impute, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen count=1
keep if occ1950_50<=970 & occ1950_80<=970
collapse (sum) count [pw=1/link_prob_midpoint_50], by(occ1950_50 foreign_50)
fillin occ1950_50 foreign_50
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_50)
gen share=count/total
keep occ1950_50 share foreign_50
rename foreign_50 foreign
rename occ1950_50 occ1950
rename share share1850
compress
save InitialDistributionRecatProbit1850_Impute, replace

use FileRecatProbit_1850_1880_Impute, clear

_pctile link_prob_midpoint_50, percentiles(0.5)
replace link_prob_midpoint_50=r(r1) if link_prob_midpoint_50<r(r1)

gen count=1
keep if occ1950_50<=970 & occ1950_80<=970
collapse (sum) count [pw=1/link_prob_midpoint_50], by(bpl_50)
keep if bpl_50>=100
egen total=total(count)
gen frac=count/total
keep bpl_50 frac
rename bpl_50 bpl
rename frac frac1850
compress
save InitialNationalityDistributionRecatProbit1850_Impute, replace

/////////////////////////////

use File_1870_1900_Impute, clear
merge 1:1 histid_70 using Rescored_1870, keep(1 3)
gen rescored_70=(_merge==3)
drop _merge
replace unskill_70=0 if rescored_70==1

merge 1:1 histid_00 using RescoredImpute_1900, keep(1 3)
gen rescored_00=(_merge==3)
drop _merge
replace unskill_00=0 if rescored_00==1

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen occ_cat_70=1 if wc_70==1
replace occ_cat_70=2 if farmer_70==1
replace occ_cat_70=3 if craft_70==1
replace occ_cat_70=4 if operative_70==1
replace occ_cat_70=5 if unskill_70==1
replace occ_cat_70=6 if rescored_70==1

gen occ_cat_00=1 if wc_00==1
replace occ_cat_00=2 if farmer_00==1
replace occ_cat_00=3 if craft_00==1
replace occ_cat_00=4 if operative_00==1
replace occ_cat_00=5 if unskill_00==1
replace occ_cat_00=6 if rescored_00==1

gen p=1
collapse (sum) p [pw=1/link_prob_midpoint_00], by(occ_cat_70 occ_cat_00 foreign)
drop if occ_cat_70==. | occ_cat_00==.

preserve
collapse (sum) p, by(occ_cat_70 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_70) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1c.pdf", replace
	
restore
collapse (sum) p, by(occ_cat_00 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_00) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1d.pdf", replace

use File_1870_1900_Impute, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen count=1
keep if occ1950_70<=970 & occ1950_impute_00<=970
collapse (sum) count [pw=1/link_prob_midpoint_00], by(occ1950_70 foreign_00)
fillin occ1950_70 foreign_00
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_00)
gen share=count/total
keep occ1950_70 share foreign_00
rename foreign_00 foreign
rename occ1950_70 occ1950
rename share share1870
compress
save InitialDistribution1870_Impute, replace

use File_1870_1900_Impute, clear

_pctile link_prob_midpoint_00, percentiles(0.5)
replace link_prob_midpoint_00=r(r1) if link_prob_midpoint_00<r(r1)

gen count=1
keep if occ1950_70<=970 & occ1950_impute_00<=970
collapse (sum) count [pw=1/link_prob_midpoint_00], by(bpl_00)
keep if bpl_00>=100
egen total=total(count)
gen frac=count/total
keep bpl_00 frac
rename bpl_00 bpl
rename frac frac1870
compress
save InitialNationalityDistribution1870_Impute, replace

/////////////////////////////

use File_1880_1910_Impute, clear
merge 1:1 histid_80 using Rescored_1880, keep(1 3)
gen rescored_80=(_merge==3)
drop _merge
replace unskill_80=0 if rescored_80==1

merge 1:1 histid_10 using RescoredImpute_1910, keep(1 3)
gen rescored_10=(_merge==3)
drop _merge
replace unskill_10=0 if rescored_10==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen occ_cat_80=1 if wc_80==1
replace occ_cat_80=2 if farmer_80==1
replace occ_cat_80=3 if craft_80==1
replace occ_cat_80=4 if operative_80==1
replace occ_cat_80=5 if unskill_80==1
replace occ_cat_80=6 if rescored_80==1

gen occ_cat_10=1 if wc_10==1
replace occ_cat_10=2 if farmer_10==1
replace occ_cat_10=3 if craft_10==1
replace occ_cat_10=4 if operative_10==1
replace occ_cat_10=5 if unskill_10==1
replace occ_cat_10=6 if rescored_10==1

gen p=1
collapse (sum) p [pw=1/link_prob_midpoint_10], by(occ_cat_80 occ_cat_10 foreign)
drop if occ_cat_80==. | occ_cat_10==.

preserve
collapse (sum) p, by(occ_cat_80 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_80) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1e.pdf", replace
	
restore
collapse (sum) p, by(occ_cat_10 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_10) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1f.pdf", replace

use File_1880_1910_Impute, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_80<=970 & occ1950_impute_10<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(occ1950_80 foreign_10)
fillin occ1950_80 foreign_10
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_10)
gen share=count/total
keep occ1950_80 share foreign_10
rename foreign_10 foreign
rename occ1950_80 occ1950
rename share share1880
compress
save InitialDistribution1880_Impute, replace

use File_1880_1910_Impute, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_80<=970 & occ1950_impute_10<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(bpl_10)
keep if bpl_10>=100
egen total=total(count)
gen frac=count/total
keep bpl_10 frac
rename bpl_10 bpl
rename frac frac1880
compress
save InitialNationalityDistribution1880_Impute, replace

/////////////////////////////

use File_1900_1930_Impute, clear
merge 1:1 histid_00 using RescoredImpute_1900, keep(1 3)
gen rescored_00=(_merge==3)
drop _merge
replace unskill_00=0 if rescored_00==1

merge 1:1 histid_30 using RescoredImpute_1930, keep(1 3)
gen rescored_30=(_merge==3)
drop _merge
replace unskill_30=0 if rescored_30==1

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen occ_cat_00=1 if wc_00==1
replace occ_cat_00=2 if farmer_00==1
replace occ_cat_00=3 if craft_00==1
replace occ_cat_00=4 if operative_00==1
replace occ_cat_00=5 if unskill_00==1
replace occ_cat_00=6 if rescored_00==1

gen occ_cat_30=1 if wc_30==1
replace occ_cat_30=2 if farmer_30==1
replace occ_cat_30=3 if craft_30==1
replace occ_cat_30=4 if operative_30==1
replace occ_cat_30=5 if unskill_30==1
replace occ_cat_30=6 if rescored_30==1

gen p=1
collapse (sum) p [pw=1/link_prob_midpoint_30], by(occ_cat_00 occ_cat_30 foreign)
drop if occ_cat_00==. | occ_cat_30==.

preserve
collapse (sum) p, by(occ_cat_00 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_00) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1g.pdf", replace
	
restore
collapse (sum) p, by(occ_cat_30 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_30) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1h.pdf", replace

use File_1900_1930_Impute, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen count=1
keep if occ1950_impute_00<=970 & occ1950_impute_30<=970
collapse (sum) count [pw=1/link_prob_midpoint_30], by(occ1950_impute_00 foreign_30)
fillin occ1950_impute_00 foreign_30
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_30)
gen share=count/total
keep occ1950_impute_00 share foreign_30
rename foreign_30 foreign
rename occ1950_impute_00 occ1950
rename share share1900
compress
save InitialDistribution1900_Impute, replace

use File_1900_1930_Impute, clear

_pctile link_prob_midpoint_30, percentiles(0.5)
replace link_prob_midpoint_30=r(r1) if link_prob_midpoint_30<r(r1)

gen count=1
keep if occ1950_impute_00<=970 & occ1950_impute_30<=970
collapse (sum) count [pw=1/link_prob_midpoint_30], by(bpl_30)
keep if bpl_30>=100
egen total=total(count)
gen frac=count/total
keep bpl_30 frac
rename bpl_30 bpl
rename frac frac1900
compress
save InitialNationalityDistribution1900_Impute, replace

/////////////////////////////

use File_1910_1940_Impute, clear
merge 1:1 histid_10 using RescoredImpute_1910, keep(1 3)
gen rescored_10=(_merge==3)
drop _merge
replace unskill_10=0 if rescored_10==1

merge 1:1 histid_40 using Rescored_1940, keep(1 3)
gen rescored_40=(_merge==3)
drop _merge
replace unskill_40=0 if rescored_40==1

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen occ_cat_10=1 if wc_10==1
replace occ_cat_10=2 if farmer_10==1
replace occ_cat_10=3 if craft_10==1
replace occ_cat_10=4 if operative_10==1
replace occ_cat_10=5 if unskill_10==1
replace occ_cat_10=6 if rescored_10==1

gen occ_cat_40=1 if wc_40==1
replace occ_cat_40=2 if farmer_40==1
replace occ_cat_40=3 if craft_40==1
replace occ_cat_40=4 if operative_40==1
replace occ_cat_40=5 if unskill_40==1
replace occ_cat_40=6 if rescored_40==1

gen p=1
collapse (sum) p [pw=1/link_prob_midpoint_10], by(occ_cat_10 occ_cat_40 foreign)
drop if occ_cat_10==. | occ_cat_40==.

preserve
collapse (sum) p, by(occ_cat_10 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_10) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1i.pdf", replace
	
restore
collapse (sum) p, by(occ_cat_40 foreign)
egen total=total(p), by(foreign)
gen frac=p/total
drop p total
reshape wide frac, i(occ_cat_40) j(foreign)

label def occ_cats 1 "White Collar" 2 "Farmer" 3 "Craft" 4 "Operative" 5 "Unskilled" 6 "Farm Family"
label values occ_cat occ_cats

graph bar frac0 frac1, over(occ_cat, label(labsize(medium))) graphregion(color(white)) ylabel(0(.1).5, angle(0) glwidth(0) labsize(medium)) yscale(range(0 .55)) ///
	bar(1, color(black)) bar(2, color(gs12)) legend(region(lcolor(white)) size(medium) order(1 "Natives" 2 "Immigrants"))
graph export "$repfolder/results/FigureD1j.pdf", replace

use File_1910_1940_Impute, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_impute_10<=970 & occ1950_40<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(occ1950_impute_10 foreign_10)
fillin occ1950_impute_10 foreign_10
drop _fillin
replace count=0 if count==.
egen total=total(count), by(foreign_10)
gen share=count/total
keep occ1950_impute_10 share foreign_10
rename foreign_10 foreign
rename occ1950_impute_10 occ1950
rename share share1910
compress
save InitialDistribution1910_Impute, replace

use File_1910_1940_Impute, clear

_pctile link_prob_midpoint_10, percentiles(0.5)
replace link_prob_midpoint_10=r(r1) if link_prob_midpoint_10<r(r1)

gen count=1
keep if occ1950_impute_10<=970 & occ1950_40<=970
collapse (sum) count [pw=1/link_prob_midpoint_10], by(bpl_10)
keep if bpl_10>=100
egen total=total(count)
gen frac=count/total
keep bpl_10 frac
rename bpl_10 bpl
rename frac frac1910
compress
save InitialNationalityDistribution1910_Impute, replace
