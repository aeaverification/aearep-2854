cd "$repfolder/data/analysis"

use UnconditionalConditionalResults_IntPlus, clear
append using UnconditionalConditionalResultsMidpoint_IntPlus
append using UnconditionalConditionalResultsUpper_IntPlus

gen year=1850 if span==1
replace year=1870 if span==2
replace year=1880 if span==3
replace year=1900 if span==4
replace year=1910 if span==5

twoway ///
	connected b year if conditional==0 & farmfam==0, lcolor(black) mcolor(black) ///
|| ///
	connected b year if conditional==0 & farmfam==1, lcolor(black) mcolor(black) lpattern(-) msymbol(D) ///
|| ///
	connected b year if conditional==0 & farmfam==2, lcolor(black) mcolor(black) lpattern(_) msymbol(T) ///
, graphregion(color(white)) ///
		ylabel(,angle(0) glwidth(0) labsize(medium)) xlabel(, labsize(medium)) ///
		xtitle("Initial Year", size(medium)) ytitle("Coefficient on Foreign", size(medium)) ///
		legend(region(lcolor(white)) order(1 "Farm Labor" 2 "Midpoint" 3 "Farmer") size(medium) subtitle("Ranking of Farm Family")) ///
		yline(0, lcolor(gray) lwidth(thin))
graph export "$repfolder/results/FigureC31.pdf", replace
		
twoway ///
	connected b year if conditional==1 & farmfam==0, lcolor(black) mcolor(black) ///
|| ///
	connected b year if conditional==1 & farmfam==1, lcolor(black) mcolor(black) lpattern(-) msymbol(D) ///
|| ///
	connected b year if conditional==1 & farmfam==2, lcolor(black) mcolor(black) lpattern(_) msymbol(T) ///
, graphregion(color(white)) ///
		ylabel(,angle(0) glwidth(0) labsize(medium)) xlabel(, labsize(medium)) ///
		xtitle("Initial Year", size(medium)) ytitle("Coefficient on Foreign", size(medium)) ///
		legend(region(lcolor(white)) order(1 "Farm Labor" 2 "Midpoint" 3 "Farmer") size(medium) subtitle("Ranking of Farm Family")) ///
		yline(0, lcolor(gray) lwidth(thin))
graph export "$repfolder/results/FigureC32.pdf", replace
