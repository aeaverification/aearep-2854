cd "$repfolder/data/analysis"

do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/DistributionsFarmFam_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/RankGraphsRecatProbit_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/OccupationalUpgradingGraphsMidpoint_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/DeltaRankRegressionsRecatProbit_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/DeltaRankRegressionsRecatProbitMidpoint_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/DeltaRankRegressionsRecatProbitUpper_State.do"
do "$repfolder/code/3_Robustness_Analysis/3.7_Scores/3.7.2_State/GraphUnconditionalConditionalResults_State.do"
