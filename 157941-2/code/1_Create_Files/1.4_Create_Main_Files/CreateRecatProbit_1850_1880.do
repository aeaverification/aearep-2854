cd "$repfolder/data/analysis"

//Load the 1850 census
use  "$repfolder/data/confidential/ipums_data/1850_100pct", clear
replace occ1950=820 if occ1950==830

//Recategorize farm family
gen histid_50=upper(histid)
merge 1:1 histid_50 using RecatProbit1850, keepusing(occ1950) update replace
drop _merge
drop histid_50

//Limit the sample to non-southern white men aged 18-30 in the first year of the sample
keep if sex==1 & age>=18 & age<=30 & race==1 & (region<30 | region>=40)

gen histid_50=upper(histid)

//Merge with linkage crosswalk
merge 1:1 histid_50 using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1850_1880", keep(1 3)
gen linked=(histid_80~="")
drop _merge histid

merge 1:1 histid_50 using OccRanksRecatProbit_1850, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

//Limit the sample to nationalities that we attempted to link
keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

//Export linkage statistics
count if foreign==0
	global all_n=r(N)
count if linked==1 & foreign==0
	global link_n=r(N)

count if foreign==1
	global all_f=r(N)
count if linked==1 & foreign==1
	global link_f=r(N)
	
capture texdoc close
texdoc init "$repfolder/results/TableB1", replace force

tex \begin{threeparttable}
tex \begin{tabular}{lrrrr}
tex \hline
tex \addlinespace[2pt]
tex & \multicolumn{2}{c}{Natives} & \multicolumn{2}{c}{Immigrants} \\
tex \cmidrule(lr){2-3} \cmidrule(lr){4-5}
tex & \multicolumn{1}{c}{(1)} & \multicolumn{1}{c}{(2)} & \multicolumn{1}{c}{(3)} & \multicolumn{1}{c}{(4)} \\
tex \emph{Link} & \multicolumn{1}{c}{Start} & \multicolumn{1}{c}{Linked} & \multicolumn{1}{c}{Start} & \multicolumn{1}{c}{Linked} \\
tex \hline
tex \addlinespace[2pt]

global fullflinked_n: di %4.3f $link_n / $all_n
global fullflinked_f: di %4.3f $link_f / $all_f

global all_n: di %10.0fc $all_n
global link_n: di %10.0fc $link_n
global all_f: di %10.0fc $all_f
global searched_f: di %10.0fc $searched_f
global link_f: di %10.0fc $link_f

tex 1850--1880 & $all_n & $link_n & $all_f & $link_f \\
tex & & ($fullflinked_n) & & ($fullflinked_f) \\
tex \addlinespace[2pt]

texdoc close
	
foreach x of numlist 404 405 410 411 414 421 425 426 434 450 453 455 465 {
	tab bpl if bpl==`x'
	count if bpl==`x'
		local all=r(N)
		global all`x'=r(N)
	count if linked==1 & bpl==`x'
		global link`x'=r(N)
		local link=r(N)
	di `link' / `all'
}

tab bpl if bpl>=410 & bpl<=413
count if bpl>=410 & bpl<=413
	global alluk=r(N)
	local alluk=r(N)
count if linked==1 & bpl>=410 & bpl<=413
	global linkuk=r(N)
	local link=r(N)
di `link' / `alluk'

capture texdoc close
texdoc init "$repfolder/results/TableB2", replace force

tex \begin{threeparttable}
tex \begin{tabular}{llrrrrrrrrrrrr}
tex \hline
tex \addlinespace[2pt]
tex & & \multicolumn{1}{c}{(1)} & \multicolumn{1}{c}{(2)} & \multicolumn{1}{c}{(3)} & \multicolumn{1}{c}{(4)} & \multicolumn{1}{c}{(5)} & \multicolumn{1}{c}{(6)} & \multicolumn{1}{c}{(7)} & \multicolumn{1}{c}{(8)} & \multicolumn{1}{c}{(9)} & \multicolumn{1}{c}{(10)} & \multicolumn{1}{c}{(11)} & \multicolumn{1}{c}{(12)} \\
tex & & \multicolumn{1}{c}{Norway} & \multicolumn{1}{c}{Sweden} & \multicolumn{1}{c}{UK} & \multicolumn{1}{c}{Ireland} & \multicolumn{1}{c}{France} & \multicolumn{1}{c}{Netherlands} & \multicolumn{1}{c}{Switzerland} & \multicolumn{1}{c}{Italy} & \multicolumn{1}{c}{Austria} & \multicolumn{1}{c}{Germany} & \multicolumn{1}{c}{Poland} & \multicolumn{1}{c}{Russia} \\
tex \hline
tex \addlinespace[2pt]

global flinked_all404: di %4.3f $link404 / $all404
global all404: di %9.0fc $all404
global link404: di %9.0fc $link404

global flinked_all405: di %4.3f $link405 / $all405
global all405: di %9.0fc $all405
global link405: di %9.0fc $link405

global flinked_alluk: di %4.3f $linkuk / $alluk
global alluk: di %9.0fc $alluk
global linkuk: di %9.0fc $linkuk

global flinked_all414: di %4.3f $link414 / $all414
global all414: di %9.0fc $all414
global link414: di %9.0fc $link414

global flinked_all421: di %4.3f $link421 / $all421
global all421: di %9.0fc $all421
global link421: di %9.0fc $link421

global flinked_all425: di %4.3f $link425 / $all425
global all425: di %9.0fc $all425
global link425: di %9.0fc $link425

global flinked_all426: di %4.3f $link426 / $all426
global all426: di %9.0fc $all426
global link426: di %9.0fc $link426

global flinked_all434: di %4.3f $link434 / $all434
global all434: di %9.0fc $all434
global link434: di %9.0fc $link434

global flinked_all450: di %4.3f $link450 / $all450
global all450: di %9.0fc $all450
global link450: di %9.0fc $link450

global flinked_all453: di %4.3f $link453 / $all453
global all453: di %9.0fc $all453
global link453: di %9.0fc $link453

global flinked_all455: di %4.3f $link455 / $all455
global all455: di %9.0fc $all455
global link455: di %9.0fc $link455

global flinked_all465: di %4.3f $link465 / $all465
global all465: di %9.0fc $all465
global link465: di %9.0fc $link465

tex 1850--1880 & Start & $all404 & $all405 & $alluk & $all414 & $all421 & $all425 & $all426 & $all434 & $all450 & $all453 & $all455 & $all465 \\
tex & Linked & $link404 & $link405 & $linkuk & $link414 & $link421 & $link425 & $link426 & $link434 & $link450 & $link453 & $link455 & $link465 \\
tex & Link Share & ($flinked_all404) & ($flinked_all405) & ($flinked_alluk) & ($flinked_all414) & ($flinked_all421) & ($flinked_all425) & ($flinked_all426) & ($flinked_all434) & ($flinked_all450) & ($flinked_all453) & ($flinked_all455) & ($flinked_all465) \\
tex \addlinespace[2pt]

texdoc close

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850) if occ1950<=970

gen urban_clean=(urban==2) if urban~=.
gen farm_clean=(farm~=1)
gen lit_clean=(lit==4)
gen lit_young=(age<20)

gen realprop_clean=realprop if realprop<999998

gen head=(relate==1)

//Output data to create graphs of representativeness of linked data on observables
preserve
collapse age urban_clean farm_clean lit_clean wc farmer craft operative unskill numperhh realprop_clean proprank proprank_age proprank_state phrank head proprank_midpoint proprank_upper phrank_midpoint phrank_upper, by(foreign)
gen linked=0
compress
save ObservablesLinkageRecatProbit_1850_1880, replace
restore, preserve
keep if linked==1
collapse age urban_clean farm_clean lit_clean wc farmer craft operative unskill numperhh realprop_clean proprank proprank_age proprank_state phrank head proprank_midpoint proprank_upper phrank_midpoint phrank_upper, by(foreign)
gen linked=1
append using ObservablesLinkageRecatProbit_1850_1880
compress
save ObservablesLinkageRecatProbit_1850_1880, replace
restore

//Create linkage weights, farm family equivalent to farm labor
gen link_prob=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob=prob if link_prob==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob=prob if link_prob==. & prob~=.
	drop prob
	
//Create linkage weights, midpoint ranking
gen link_prob_midpoint=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
	drop prob

//Create linkage weights, farm family equivalent to farmers
gen link_prob_upper=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob_upper=prob if link_prob_upper==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob_upper=prob if link_prob_upper==. & prob~=.
	drop prob

//Keep only linked individuals
keep if linked==1
	
order *, alpha
order histid*, first
foreach x of varlist age-yngch {
	rename `x' `x'_50
}

//Merge in 1880 data
preserve
use  "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_80=upper(histid)
drop histid
keep if sex==1 & (region<30 | region>40)
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_80
}
tempfile census80
save `census80'
restore
merge 1:1 histid_80 using `census80', keep(3)
drop _merge

merge 1:1 histid_80 using OccRanks_1880, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_state=(proprank_state+phrank)/2

foreach x of varlist lntotprop proprank lntotprop_age proprank_age lntotprop_state proprank_state inc1900 l_inc1900 phrank avg_rank avg_rank_age avg_rank_state *midpoint *upper {
	rename `x' `x'_80
}

//Create variables
gen wc_80=(occ1950_80<100 | (occ1950_80>=200 & occ1950_80<500)) if occ1950_80<=970
gen farmer_80=(occ1950_80>=100 & occ1950_80<200) if occ1950_80<=970
gen craft_80=(occ1950_80>=500 & occ1950_80<600) if occ1950_80<=970
gen operative_80=(occ1950_80>=600 & occ1950_80<700) if occ1950_80<=970
gen unskill_80=(occ1950_80>=700 & occ1950_80<=970 & occ1950_80~=850) if occ1950_80<=970

gen urban_clean_80=(urban_80==2) if urban_80~=.
gen farm_clean_80=(farm_80~=1)
gen lit_clean_80=(lit_80==4)

gen head_80=(relate_80==1)

gen marst_clean_80=(marst_80==1 | marst_80==2)

gen ffor_80=1-(fbpl_80<100)

compress

//Save
save FileRecatProbit_1850_1880, replace
