cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1930_100pct", clear
keep if sex==1
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=44 & age<=64 & race==1 & (region<30 | region>=40)

preserve
use "$repfolder/data/confidential/clp_linkage_crosswalks/crosswalk_1900_1930", clear
rename histid_1900 histid_00
rename histid_1930 histid_30
replace histid_00=upper(histid_00)
replace histid_30=upper(histid_30)
keep if abe_exact_conservative==1 & abe_nysiis_conservative==1
keep histid*
merge 1:1 histid* using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1900_1930"
keep if _merge==3
drop _merge
tempfile crosswalk
save `crosswalk'
restore

gen histid_30=upper(histid)
merge 1:1 histid_30 using `crosswalk', keep(1 3)
gen linked=(histid_00~="")
drop _merge histid

preserve
use  "$repfolder/data/confidential/ipums_data/1900_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_00=upper(histid)
drop histid
keep if sex==1
order *, alpha
order histid, first
foreach x of varlist age-yrimmig {
	rename `x' `x'_00
}
tempfile census00
save `census00'
restore
rename age age_30
merge m:1 histid_00 using `census00', keep(3)
drop _merge

	//Require detailed birthplace match
	drop if bpld~=bpld_00
	
	//Require that parents' birthplaces match
	drop if fbpl~=fbpl_00
	drop if mbpl~=mbpl_00
	
	//Drop those who go from married to never married
	drop if marst_00<=5 & marst==6
	
	//Drop those who "forget" how to speak English
	drop if speakeng_00==2 & speakeng==1
	
	//Require racial match
	drop if race~=race_00
	
	//Drop those who "forget" how to read
	drop if lit_00==4 & lit==1
	
	//Drop those whose arrival years are more than 4 years apart
	drop if abs(yrimmig-yrimmig_00)>4 & bpl_00>100
	
keep histid*
compress
save crosswalk_1900_1930_IntPlus, replace
