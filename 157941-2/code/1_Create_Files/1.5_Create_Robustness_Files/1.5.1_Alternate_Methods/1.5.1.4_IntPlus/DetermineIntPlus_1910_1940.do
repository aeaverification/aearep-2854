cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1910_100pct", clear
replace occ1950=820 if occ1950==830

keep if sex==1 & age>=18 & age<=30 & race==1 & (region<30 | region>=40)

preserve
use "$repfolder/data/confidential/clp_linkage_crosswalks/crosswalk_1910_1940", clear
rename histid_1910 histid_10
rename histid_1940 histid_40
replace histid_10=upper(histid_10)
replace histid_40=upper(histid_40)
keep if abe_exact_conservative==1 & abe_nysiis_conservative==1
keep histid*
merge 1:1 histid* using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1910_1940"
keep if _merge==3
drop _merge
tempfile crosswalk
save `crosswalk'
restore

gen histid_10=upper(histid)
merge 1:1 histid_10 using `crosswalk', keep(1 3)
gen linked=(histid_40~="")
drop _merge histid

keep if linked==1

preserve
use  "$repfolder/data/confidential/ipums_data/1940_100pct_male", clear
replace occ1950=820 if occ1950==830

gen histid_40=upper(histid)
drop histid
keep if sex==1 & (region<30 | region>40)
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_40
}
tempfile census40
save `census40'
restore
merge 1:1 histid_40 using `census40', keep(3)
drop _merge

	//Require detailed birthplace match
	drop if bpld~=bpld_40
	
	//Drop those who go from married to never married
	drop if marst_40<=5 & marst==6
	
	//Require racial match
	drop if race~=race_40
	
keep histid*
compress
save crosswalk_1910_1940_IntPlus, replace
