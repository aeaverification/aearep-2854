cd "$repfolder/data/analysis"

do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/DetermineIntPlus_1850_1880.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/DetermineIntPlus_1870_1900.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/DetermineIntPlus_1880_1910.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/DetermineIntPlus_1900_1930.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/DetermineIntPlus_1910_1940.do"

do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/CreateRecatProbit_1850_1880_IntPlus.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/Create_1870_1900_IntPlus.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/Create_1880_1910_IntPlus.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/Create_1900_1930_IntPlus.do"
do "$repfolder/code/1_Create_Files/1.5_Create_Robustness_Files/1.5.1_Alternate_Methods/1.5.1.4_IntPlus/Create_1910_1940_IntPlus.do"
