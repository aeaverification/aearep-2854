cd "$repfolder/data/analysis"

use  "$repfolder/data/confidential/ipums_data/1850_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_50=upper(histid)
merge 1:1 histid_50 using RecatProbit1850, keepusing(occ1950) update replace
drop _merge
drop histid_50

keep if sex==1 & age>=18 & age<=30 & race==1 & (region<30 | region>=40)

gen histid_50=upper(histid)
merge 1:1 histid_50 using "$repfolder/data/raw/linkage_crosswalks/crosswalk_1850_1880", keep(1 3)
gen linked=(histid_80~="")
drop _merge histid

merge 1:1 histid_50 using OccRanksRecatProbit_1850, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_age_midpoint=(proprank_age_midpoint+phrank_midpoint)/2
gen avg_rank_age_upper=(proprank_age_upper+phrank_upper)/2
gen avg_rank_state=(proprank_state+phrank)/2
gen avg_rank_state_midpoint=(proprank_state_midpoint+phrank_midpoint)/2
gen avg_rank_state_upper=(proprank_state_upper+phrank_upper)/2

rename lntotprop lntotprop_orig
rename lntotprop_midpoint lntotprop_orig_midpoint
rename lntotprop_upper lntotprop_orig_upper
rename proprank proprank_orig
rename proprank_midpoint proprank_orig_midpoint
rename proprank_upper proprank_orig_upper
rename avg_rank avg_rank_orig
rename avg_rank_midpoint avg_rank_orig_midpoint
rename avg_rank_upper avg_rank_orig_upper
rename lntotprop_age lntotprop
rename lntotprop_age_midpoint lntotprop_midpoint
rename lntotprop_age_upper lntotprop_upper
rename proprank_age proprank
rename proprank_age_midpoint proprank_midpoint
rename proprank_age_upper proprank_upper
rename avg_rank_age avg_rank
rename avg_rank_age_midpoint avg_rank_midpoint
rename avg_rank_age_upper avg_rank_upper

keep if bpld<9000 | (bpld>=15000 & bpld<16000) | bpld==20000 | bpld==25000 | ///
	(bpld>=40000 & bpld<40600) | (bpld>=41000 & bpld<41500) | (bpld>=42000 & bpld<42700) ///
	| (bpld>=43000 & bpld<43900) | (bpld>=45000 & bpld<45800) | (bpld>=46000 & bpld<46300) ///
	| (bpld>=46500 & bpld<46600) | (bpld>=50000 & bpld<50300) | (bpld>=51000 & bpld<51900) ///
	| (bpld>=52000 & bpld<54600) | (bpld>=70000 & bpld<70100)
gen foreign=1-(bpld<9000)

keep if foreign==0 | (bpl>=400 & bpl<=465 & bpl~=403 & bpl~=413)

gen wc=(occ1950<100 | (occ1950>=200 & occ1950<500)) if occ1950<=970
gen farmer=(occ1950>=100 & occ1950<200) if occ1950<=970
gen craft=(occ1950>=500 & occ1950<600) if occ1950<=970
gen operative=(occ1950>=600 & occ1950<700) if occ1950<=970
gen unskill=(occ1950>=700 & occ1950<=970 & occ1950~=850) if occ1950<=970

gen urban_clean=(urban==2) if urban~=.
gen farm_clean=(farm~=1)
gen lit_clean=(lit==4)
gen lit_young=(age<20)

gen realprop_clean=realprop if realprop<999998

gen head=(relate==1)

gen link_prob=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob=prob if link_prob==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob=prob if link_prob==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank##c.proprank##c.proprank##c.proprank c.phrank##c.phrank##c.phrank##c.phrank wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob=prob if link_prob==. & prob~=.
	drop prob
	
gen link_prob_midpoint=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint##c.proprank_midpoint c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint##c.phrank_midpoint wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob_midpoint=prob if link_prob_midpoint==. & prob~=.
	drop prob
	
gen link_prob_upper=.
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl<90
	predict prob if e(sample)
	replace link_prob_upper=prob if link_prob_upper==. & prob~=.
	drop prob

foreach x of numlist 410 411 414 421 453 {
	tab bpl if bpl==`x'
	probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.statefip if bpl==`x'
		predict prob if e(sample)
		replace link_prob_upper=prob if link_prob_upper==. & prob~=.
		drop prob
}
probit linked c.age##c.age##c.age##c.age urban_clean farm_clean lit_clean lit_young c.proprank_upper##c.proprank_upper##c.proprank_upper##c.proprank_upper c.phrank_upper##c.phrank_upper##c.phrank_upper##c.phrank_upper wc unskill craft operative c.realprop_clean##c.realprop_clean##c.realprop_clean##c.realprop_clean c.numperhh##c.numperhh##c.numperhh##c.numperhh head i.bpl i.statefip if bpl>90 & bpl~=410 & bpl~=411 & bpl~=414 & bpl~=421 & bpl~=453 & (bpl<410 | bpl>412)
	predict prob if e(sample)
	replace link_prob_upper=prob if link_prob_upper==. & prob~=.
	drop prob

keep if linked==1
	
order *, alpha
order histid*, first
foreach x of varlist age-yngch {
	rename `x' `x'_50
}

preserve
use  "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

gen histid_80=upper(histid)
drop histid
keep if sex==1 & (region<30 | region>40)
order *, alpha
order histid, first
foreach x of varlist age-yngch {
	rename `x' `x'_80
}
tempfile census80
save `census80'
restore
merge 1:1 histid_80 using `census80', keep(3)
drop _merge

merge 1:1 histid_80 using OccRanks_1880, keep(1 3)
drop _merge
gen avg_rank=(proprank+phrank)/2
gen avg_rank_midpoint=(proprank_midpoint+phrank_midpoint)/2
gen avg_rank_upper=(proprank_upper+phrank_upper)/2
gen avg_rank_age=(proprank_age+phrank)/2
gen avg_rank_age_midpoint=(proprank_age_midpoint+phrank_midpoint)/2
gen avg_rank_age_upper=(proprank_age_upper+phrank_upper)/2
gen avg_rank_state=(proprank_state+phrank)/2
gen avg_rank_state_midpoint=(proprank_state_midpoint+phrank_midpoint)/2
gen avg_rank_state_upper=(proprank_state_upper+phrank_upper)/2

rename lntotprop lntotprop_orig
rename lntotprop_midpoint lntotprop_orig_midpoint
rename lntotprop_upper lntotprop_orig_upper
rename proprank proprank_orig
rename proprank_midpoint proprank_orig_midpoint
rename proprank_upper proprank_orig_upper
rename avg_rank avg_rank_orig
rename avg_rank_midpoint avg_rank_orig_midpoint
rename avg_rank_upper avg_rank_orig_upper
rename lntotprop_age lntotprop
rename lntotprop_age_midpoint lntotprop_midpoint
rename lntotprop_age_upper lntotprop_upper
rename proprank_age proprank
rename proprank_age_midpoint proprank_midpoint
rename proprank_age_upper proprank_upper
rename avg_rank_age avg_rank
rename avg_rank_age_midpoint avg_rank_midpoint
rename avg_rank_age_upper avg_rank_upper

foreach x of varlist lntotprop proprank lntotprop_orig proprank_orig lntotprop_state proprank_state inc1900 l_inc1900 phrank avg_rank avg_rank_orig avg_rank_state *midpoint *upper {
	rename `x' `x'_80
}

gen wc_80=(occ1950_80<100 | (occ1950_80>=200 & occ1950_80<500)) if occ1950_80<=970
gen farmer_80=(occ1950_80>=100 & occ1950_80<200) if occ1950_80<=970
gen craft_80=(occ1950_80>=500 & occ1950_80<600) if occ1950_80<=970
gen operative_80=(occ1950_80>=600 & occ1950_80<700) if occ1950_80<=970
gen unskill_80=(occ1950_80>=700 & occ1950_80<=970 & occ1950_80~=850) if occ1950_80<=970

gen urban_clean_80=(urban_80==2) if urban_80~=.
gen farm_clean_80=(farm_80~=1)
gen lit_clean_80=(lit_80==4)

gen head_80=(relate_80==1)

gen marst_clean_80=(marst_80==1 | marst_80==2)

gen ffor_80=1-(fbpl_80<100)

compress

save FileRecatProbit_1850_1880_Age, replace
