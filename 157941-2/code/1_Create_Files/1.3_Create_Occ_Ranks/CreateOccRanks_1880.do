cd "$repfolder/code/1_Create_Files/1.3_Create_Occ_Ranks"

//Load the necessary .ado files into memory
capture MergeWealthScores
capture MergeWealthScoresAgeCells
capture MergeWealthScoresState

cd "$repfolder/data/analysis"

//Load the 1880 census
use "$repfolder/data/confidential/ipums_data/1880_100pct", clear
replace occ1950=820 if occ1950==830

//Occscore calculations not used in final analysis
su occscore if occ1950==820
local flab_occscore=r(mean)
su occscore if occ1950==100
local farmer_occscore=r(mean)
replace occscore=`flab_occscore' if occ1950==820

//Determine who is a farm family member
preserve
keep if relate==1
gen isfarm=(occ1950>=100 & occ1950<200)
collapse (max) isfarm, by(serial)
tempfile occhead
save `occhead'
restore
merge m:1 serial using `occhead'
drop _merge

//Limit the sample to non-southern white men aged 18-64 with an occupation
keep if race==1 & age>=18 & age<=64 & sex==1 & (region<30 | region>40) & occ1950<=970

keep histid region age stateicp occ1950 ind1950 isfarm relate occscore

gen south=(stateicp==11 | (stateicp>=40 & stateicp<=56) | stateicp==98)

//Merge in wealth scores
MergeWealthScores, occ1950(occ1950) file_narrow(OccWealth70_Narrow) file_broad(OccWealth70_Broad) gen(totprop_final_70) cutoff(100) region(south)

	//Assign midpoint or upper scores to farm family
	su totprop_final_70 if occ1950==100
	local farmer=r(mean)
	su totprop_final_70 if occ1950==820
	local flab=r(mean)
	gen totprop_final_70_midpoint=totprop_final_70
	replace totprop_final_70_midpoint=(`farmer'+`flab')/2 if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	gen totprop_final_70_upper=totprop_final_70
	replace totprop_final_70_upper=`farmer' if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	
gen lntotprop=log(totprop_final_70)
gen lntotprop_midpoint=log(totprop_final_70_midpoint)
gen lntotprop_upper=log(totprop_final_70_upper)
drop totprop_final_70*

//Rank wealth scores
egen proprank=rank(lntotprop)
su proprank
replace proprank=proprank/r(max)
egen proprank_midpoint=rank(lntotprop_midpoint)
su proprank_midpoint
replace proprank_midpoint=proprank_midpoint/r(max)
egen proprank_upper=rank(lntotprop_upper)
su proprank_upper
replace proprank_upper=proprank_upper/r(max)

//Merge in wealth scores varying by age, assign midpoint or upper scores to farm family, and rank
gen young=(age<=30)
MergeWealthScoresAgeCells, occ1950(occ1950) file_narrow(OccWealth70AgeCells_Narrow) file_broad(OccWealth70AgeCells_Broad) gen(totprop_final_70) cutoff(100) region(south) age(young)
	preserve
	keep occ1950 young totprop_final_70 south
	duplicates drop
	keep if occ1950==100 | occ1950==820
	collapse (mean) totprop_final_70, by(young south)
	rename totprop_final_70 totprop_final_70_midpoint_fill
	tempfile midpoint
	save `midpoint'
	restore
	merge m:1 young south using `midpoint'
	drop _merge
	gen totprop_final_70_midpoint=totprop_final_70
	replace totprop_final_70_midpoint=totprop_final_70_midpoint_fill if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	
	preserve
	keep occ1950 young totprop_final_70 south
	duplicates drop
	keep if occ1950==100
	rename totprop_final_70 totprop_final_70_upper_fill
	tempfile upper
	save `upper'
	restore
	merge m:1 young south using `upper'
	drop _merge
	gen totprop_final_70_upper=totprop_final_70
	replace totprop_final_70_upper=totprop_final_70_upper_fill if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	
gen lntotprop_age=log(totprop_final_70)
gen lntotprop_age_midpoint=log(totprop_final_70_midpoint)
gen lntotprop_age_upper=log(totprop_final_70_upper)
drop totprop_final_70* young
egen proprank_age=rank(lntotprop_age)
su proprank_age
replace proprank_age=proprank_age/r(max)
egen proprank_age_midpoint=rank(lntotprop_age_midpoint)
su proprank_age_midpoint
replace proprank_age_midpoint=proprank_age_midpoint/r(max)
egen proprank_age_upper=rank(lntotprop_age_upper)
su proprank_age_upper
replace proprank_age_upper=proprank_age_upper/r(max)

//Merge in wealth scores varying by state, assign midpoint or upper scores to farm family, and rank
MergeWealthScoresState, occ1950(occ1950) file_narrow(OccWealth70State_Narrow) file_broad(OccWealth70State_Broad) gen(totprop_final_70) cutoff(100) stateicp(stateicp)
	preserve
	keep occ1950 totprop_final_70 stateicp
	duplicates drop
	keep if occ1950==100 | occ1950==820
	collapse (mean) totprop_final_70, by(stateicp)
	rename totprop_final_70 totprop_final_70_midpoint_fill
	tempfile midpoint
	save `midpoint'
	restore
	merge m:1 stateicp using `midpoint'
	drop _merge
	gen totprop_final_70_midpoint=totprop_final_70
	replace totprop_final_70_midpoint=totprop_final_70_midpoint_fill if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	
	preserve
	keep occ1950 totprop_final_70 stateicp
	duplicates drop
	keep if occ1950==100
	collapse (mean) totprop_final_70, by(stateicp)
	rename totprop_final_70 totprop_final_70_upper_fill
	tempfile upper
	save `upper'
	restore
	merge m:1 stateicp using `upper'
	drop _merge
	gen totprop_final_70_upper=totprop_final_70
	replace totprop_final_70_upper=totprop_final_70_upper_fill if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900

gen lntotprop_state=log(totprop_final_70)
gen lntotprop_state_midpoint=log(totprop_final_70_midpoint)
gen lntotprop_state_upper=log(totprop_final_70_upper)
drop totprop_final_70* stateicp
egen proprank_state=rank(lntotprop_state)
su proprank_state
replace proprank_state=proprank_state/r(max)
egen proprank_state_midpoint=rank(lntotprop_state_midpoint)
su proprank_state_midpoint
replace proprank_state_midpoint=proprank_state_midpoint/r(max)
egen proprank_state_upper=rank(lntotprop_state_upper)
su proprank_state_upper
replace proprank_state_upper=proprank_state_upper/r(max)

//Merge in 1900 occupational scores, create midpoint and upper scores, and rank
merge m:1 occ1950 using PH_inc_occ1950.dta, keepusing(inc1900)
drop if _merge==2
do "$repfolder/code/1_Create_Files/1.3_Create_Occ_Ranks/Fill_in_scores.do"
drop missing
drop _merge
	su inc1900 if occ1950==100
	local farmer=r(mean)
	su inc1900 if occ1950==820
	local flab=r(mean)
	gen inc1900_midpoint=inc1900
	replace inc1900_midpoint=(`farmer'+`flab')/2 if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	gen inc1900_upper=inc1900
	replace inc1900_upper=`farmer' if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
gen l_inc1900=log(inc1900)
egen phrank=rank(l_inc1900)
su phrank
replace phrank=phrank/r(max)
gen l_inc1900_midpoint=log(inc1900_midpoint)
egen phrank_midpoint=rank(l_inc1900_midpoint)
su phrank_midpoint
replace phrank_midpoint=phrank_midpoint/r(max)
gen l_inc1900_upper=log(inc1900_upper)
egen phrank_upper=rank(l_inc1900_upper)
su phrank_upper
replace phrank_upper=phrank_upper/r(max)

		su occscore if occ1950==100
	local farmer=r(mean)
	su occscore if occ1950==820
	local flab=r(mean)
	gen occscore_midpoint=occscore
	replace occscore_midpoint=(`farmer'+`flab')/2 if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
	gen occscore_upper=occscore
	replace occscore_upper=`farmer' if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
gen l_occscore=log(occscore)
egen occrank=rank(l_occscore)
su occrank
replace occrank=occrank/r(max)
gen l_occscore_midpoint=log(occscore_midpoint)
egen occrank_midpoint=rank(l_occscore_midpoint)
su occrank_midpoint
replace occrank_midpoint=occrank_midpoint/r(max)
gen l_occscore_upper=log(occscore_upper)
egen occrank_upper=rank(l_occscore_upper)
su occrank_upper
replace occrank_upper=occrank_upper/r(max)

//Mark farm family in 1880
preserve
keep if isfarm==1 & relate>=2 & relate<12 & occ1950>=800 & occ1950<900
keep histid
rename histid histid_80
replace histid_80=upper(histid_80)
compress
save Rescored_1880, replace
restore

//Export
rename histid histid_80
replace histid_80=upper(histid_80)
keep histid_80 *prop* *inc* *rank* l_occscore*
compress
save OccRanks_1880, replace
