//cd 

clear
insheet Modes1900.csv, comma names
keep histid* occ1950
rename occ1950 occ1950_fill
replace histid=upper(histid)
compress
save UnclassifiedOccModes1900, replace

clear
insheet Modes1910.csv, comma names
keep histid* occ1950
rename occ1950 occ1950_fill
replace histid=upper(histid)
compress
save UnclassifiedOccModes1910, replace

clear
insheet Modes1920.csv, comma names
keep histid* occ1950
rename occ1950 occ1950_fill
replace histid=upper(histid)
compress
save UnclassifiedOccModes1920, replace

clear
insheet Modes1930.csv, comma names
keep histid* occ1950
rename occ1950 occ1950_fill
replace histid=upper(histid)
compress
save UnclassifiedOccModes1930, replace
