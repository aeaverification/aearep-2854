libname IPUMS "1850_3-1";

proc format cntlin=IPUMS.us1850c_usa_f;
run;

proc sql;
	create table IPUMS.cens1850 as select
	a.rectype as rectype,
	a.year as year,
	a.datanum as datanum,
	a.serial as serial,
	a.numprec as numprec,
	a.subsamp as subsamp,
	a.dwsize as dwsize,
	a.region as region,
	a.stateicp as stateicp,
	a.statefip as statefip,
	a.sea as sea,
	a.metro as metro,
	a.metarea as metarea,
	a.metdist as metdist,
	a.city as city,
	a.citypop as citypop,
	a.sizepl as sizepl,
	a.urban as urban,
	a.urbarea as urbarea,
	a.gq as gq,
	a.gqtype as gqtype,
	a.gqfunds as gqfunds,
	a.farm as farm,
	a.pageno as pageno,
	a.nfams as nfams,
	a.ncouples as ncouples,
	a.nmothers as nmothers,
	a.nfathers as nfathers,
	a.nengpop as nengpop,
	a.urbpop as urbpop,
	a.hhtype as hhtype,
	a.cntry as cntry,
	a.headloc as headloc,
	a.nhgisjoin as nhgisjoin,
	a.yrstcounty as yrstcounty,
	a.stcounty as stcounty,
	a.appal as appal,
	a.county as county,
	a.hhwt as hhwt,
	a.stdcity as stdcity,
	a.gqstr as gqstr,
	a.dwelling as dwelling,
	a.mdstatus as mdstatus,
	a.ward as ward,
	a.reel as reel,
	a.numperhh as numperhh,
	a.line as line,
	a.enumdist as enumdist,
	a.split as split,
	a.splithid as splithid,
	a.splitnum as splitnum,
	a.us1850c_0010 as us1850c_0010,
	a.us1850c_0011 as us1850c_0011,
	a.us1850c_0012 as us1850c_0012,
	a.us1850c_0013 as us1850c_0013,
	a.us1850c_0014 as us1850c_0014,
	a.us1850c_0015 as us1850c_0015,
	a.us1850c_0016 as us1850c_0016,
	a.us1850c_0017 as us1850c_0017,
	a.us1850c_0018 as us1850c_0018,
	a.us1850c_0019 as us1850c_0019,
	a.us1850c_0020 as us1850c_0020,
	a.us1850c_0021 as us1850c_0021,
	a.us1850c_0022 as us1850c_0022,
	a.us1850c_0023 as us1850c_0023,
	a.us1850c_0024 as us1850c_0024,
	a.us1850c_0025 as us1850c_0025,
	a.us1850c_0026 as us1850c_0026,
	a.us1850c_0027 as us1850c_0027,
	a.us1850c_0028 as us1850c_0028,
	a.us1850c_0029 as us1850c_0029,
	a.us1850c_0030 as us1850c_0030,
	a.us1850c_0031 as us1850c_0031,
	a.us1850c_0032 as us1850c_0032,
	a.us1850c_0033 as us1850c_0033,
	a.us1850c_0034 as us1850c_0034,
	a.us1850c_0035 as us1850c_0035,
	a.us1850c_0036 as us1850c_0036,
	a.us1850c_0037 as us1850c_0037,
	a.us1850c_0038 as us1850c_0038,
	a.us1850c_0039 as us1850c_0039,
	a.us1850c_0040 as us1850c_0040,
	a.us1850c_0041 as us1850c_0041,
	a.us1850c_0042 as us1850c_0042,
	a.us1850c_0043 as us1850c_0043,
	a.us1850c_0044 as us1850c_0044,
	a.us1850c_0045 as us1850c_0045,
	a.us1850c_0046 as us1850c_0046,
	a.us1850c_0047 as us1850c_0047,
	a.us1850c_0048 as us1850c_0048,
	a.us1850c_0049 as us1850c_0049,
	a.us1850c_0050 as us1850c_0050,
	a.us1850c_0051 as us1850c_0051,
	a.us1850c_0052 as us1850c_0052,
	a.us1850c_0053 as us1850c_0053,
	a.us1850c_0054 as us1850c_0054,
	a.us1850c_0055 as us1850c_0055,
	a.us1850c_0056 as us1850c_0056,
	a.us1850c_0057 as us1850c_0057,
	a.us1850c_0058 as us1850c_0058,
	b.rectypep as rectypep,
	b.yearp as yearp,
	b.datanump as datanump,
	b.serialp as serialp,
	b.pernum as pernum,
	b.momloc as momloc,
	b.stepmom as stepmom,
	b.momrule_hist as momrule_hist,
	b.poploc as poploc,
	b.steppop as steppop,
	b.poprule_hist as poprule_hist,
	b.sploc as sploc,
	b.sprule_hist as sprule_hist,
	b.famsize as famsize,
	b.nchild as nchild,
	b.nchlt5 as nchlt5,
	b.famunit as famunit,
	b.eldch as eldch,
	b.yngch as yngch,
	b.nsibs as nsibs,
	b.relate as relate,
	b.age as age,
	b.sex as sex,
	b.race as race,
	b.marst as marst,
	b.marrinyr as marrinyr,
	b.bpl as bpl,
	b.hispan as hispan,
	b.spanname as spanname,
	b.school as school,
	b.lit as lit,
	b.labforce as labforce,
	b.occ1950 as occ1950,
	b.occscore as occscore,
	b.sei as sei,
	b.ind1950 as ind1950,
	b.realprop as realprop,
	b.imppop as imppop,
	b.impsp as impsp,
	b.imprel as imprel,
	b.qage as qage,
	b.qagemont as qagemont,
	b.qbpl as qbpl,
	b.qocc as qocc,
	b.qrace as qrace,
	b.qschool as qschool,
	b.qsex as qsex,
	b.racamind as racamind,
	b.racasian as racasian,
	b.racblk as racblk,
	b.racpacis as racpacis,
	b.racother as racother,
	b.racwht as racwht,
	b.racesing as racesing,
	b.probwht as probwht,
	b.proboth as proboth,
	b.probblk as probblk,
	b.probapi as probapi,
	b.probai as probai,
	b.hisprule as hisprule,
	b.presgl as presgl,
	b.erscor50 as erscor50,
	b.edscor50 as edscor50,
	b.npboss50 as npboss50,
	b.occstr as occstr,
	b.slwt as slwt,
	b.perwt as perwt,
	b.birthyr as birthyr,
	b.occ as occ,
	b.namelast as namelast,
	b.namefrst as namefrst,
	b.bplstr as bplstr,
	b.agemonth as agemonth,
	b.blind as blind,
	b.deaf as deaf,
	b.idiotic as idiotic,
	b.insane as insane,
	b.crime as crime,
	b.pauper as pauper,
	b.sursim as sursim,
	b.impmom as impmom,
	b.qlit as qlit,
	b.qmarinyr as qmarinyr,
	b.pid as pid,
	b.us1850c_1000 as us1850c_1000,
	b.us1850c_1001 as us1850c_1001,
	b.us1850c_1002 as us1850c_1002,
	b.us1850c_1003 as us1850c_1003,
	b.us1850c_1004 as us1850c_1004,
	b.us1850c_1005 as us1850c_1005,
	b.us1850c_1006 as us1850c_1006,
	b.us1850c_1007 as us1850c_1007,
	b.us1850c_1008 as us1850c_1008,
	b.us1850c_1009 as us1850c_1009,
	b.us1850c_1010 as us1850c_1010,
	b.us1850c_1011 as us1850c_1011,
	b.us1850c_1012 as us1850c_1012,
	b.us1850c_1013 as us1850c_1013,
	b.us1850c_1014 as us1850c_1014,
	b.us1850c_1015 as us1850c_1015,
	b.us1850c_1016 as us1850c_1016,
	b.us1850c_1017 as us1850c_1017,
	b.us1850c_1018 as us1850c_1018,
	b.us1850c_1019 as us1850c_1019,
	b.us1850c_1020 as us1850c_1020,
	b.us1850c_1021 as us1850c_1021,
	b.us1850c_1022 as us1850c_1022,
	b.us1850c_1023 as us1850c_1023,
	b.us1850c_1024 as us1850c_1024,
	b.us1850c_1025 as us1850c_1025,
	b.us1850c_1026 as us1850c_1026,
	b.us1850c_1027 as us1850c_1027,
	b.us1850c_1028 as us1850c_1028,
	b.us1850c_1029 as us1850c_1029,
	b.us1850c_1030 as us1850c_1030,
	b.us1850c_1031 as us1850c_1031,
	b.us1850c_1032 as us1850c_1032,
	b.us1850c_1033 as us1850c_1033,
	b.us1850c_1034 as us1850c_1034,
	b.us1850c_1035 as us1850c_1035,
	b.us1850c_1036 as us1850c_1036,
	b.us1850c_1037 as us1850c_1037,
	b.us1850c_1038 as us1850c_1038,
	b.us1850c_1039 as us1850c_1039,
	b.us1850c_1040 as us1850c_1040,
	b.us1850c_1041 as us1850c_1041,
	b.us1850c_1042 as us1850c_1042,
	b.us1850c_1043 as us1850c_1043,
	b.us1850c_1044 as us1850c_1044,
	b.us1850c_1045 as us1850c_1045,
	b.us1850c_1046 as us1850c_1046,
	b.us1850c_1047 as us1850c_1047,
	b.us1850c_1048 as us1850c_1048,
	b.us1850c_1049 as us1850c_1049,
	b.us1850c_1050 as us1850c_1050,
	b.us1850c_1051 as us1850c_1051,
	b.us1850c_1052 as us1850c_1052,
	b.us1850c_1053 as us1850c_1053
	from IPUMS.us1850c_usa(where=(rectype="H")) as a full join IPUMS.us1850c_usa(where=(rectype="P")) as b
	on a.serial=b.serialp;
quit;

proc sort data=IPUMS.cens1850;
	by serialp pernum;
run;
