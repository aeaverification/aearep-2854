libname storage "1930_3-3";
libname data "Files1930";

proc format cntlin=storage.us1930d_usa_f;
run;

data import(rename=(bpl=bpld));
	set storage.cens1930(keep=serial pernum sex age namefrst namelast bpl);
	where sex=1 and namelast~="" and namefrst~="" and namefrst~="[MR]";
	namelast=upcase(namelast);
	namefrst=upcase(namefrst);

	namegivcorr=compress(namefrst,,'ask');
	namelastcorr=compress(namelast,,'ask');
run;

data import;
	set import;
	bpl=floor(bpld/100);
run;

data census1930;
	set import;
	mark=0;
run;

data census1930(drop=position);
	set census1930;
	position=prxmatch("/ [A-Z]$/",strip(namegivcorr));
	if position=0 then namefrstcorr=namegivcorr;
	else namefrstcorr=strip(substr(namegivcorr,1,position));
	if position>0 then namemidcorr=strip(substr(namegivcorr,position,2));
run;

data census1930;
	set census1930;

	namefrstcorr=compress(namefrstcorr,,'ak');
	namelastcorr=compress(namelastcorr,,'ak');
	namegivcorr=compress(namegivcorr,,'ak');
run;

data data.census1930_cleaned_target;
	set census1930(keep=serial pernum age namefrstcorr namelastcorr namemidcorr mark bpl);
	where namefrstcorr~="" and namelastcorr~="" and length(namefrstcorr)>1 and length(namelastcorr)>2;
run;
