//cd 

clear
insheet using Uniques5080New.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1850_1880, replace

clear
insheet using Uniques7000.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1870_1900, replace

clear
insheet using Uniques8010New.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1880_1910, replace

clear
insheet using Uniques0030.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1900_1930, replace

clear
insheet using Uniques1040.csv, comma names
keep histid*
foreach var of varlist histid* {
	replace `var'=upper(`var')
}
compress
save crosswalk_1910_1940, replace
