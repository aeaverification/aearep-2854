#!/bin/bash

cd 0_Crosswalk_Creation/0.1_Import
sas us1850c_usa_mod.sas
sas us1870c_usa_mod.sas
sas us1880e_usa_mod.sas
sas us1900m_usa_mod.sas
sas us1910m_usa_mod.sas
sas us1920c_usa_mod.sas
sas us1930d_usa_mod.sas
sas us1940b_usa_mod.sas

sas SQL1850.sas
sas SQL1870.sas
sas SQL1880New.sas
sas SQL1900.sas
sas SQL1910.sas
sas SQL1920.sas
sas SQL1930.sas
sas SQL1940.sas

cd ../0.2_Linkage/0.2.1_Clean
sas 1850Clean.sas
sas 1850SelfLink.sas
sas 1850StartScrub.sas

sas 1870Clean.sas
sas 1870SelfLink.sas
sas 1870StartScrub.sas
sas 1870TargetScrub.sas

sas 1880CleanNew.sas
sas 1880SelfLinkNew.sas
sas 1880StartScrubNew.sas
sas 1880TargetScrubNew.sas

sas 1900Clean.sas
sas 1900SelfLink.sas
sas 1900StartScrub.sas
sas 1900TargetScrub.sas

sas 1910Clean.sas
sas 1910SelfLink.sas
sas 1910StartScrub.sas
sas 1910TargetScrub.sas

sas 1920Clean.sas
sas 1920SelfLink.sas
sas 1920StartScrub.sas
sas 1920TargetScrub.sas

sas 1930Clean.sas
sas 1930TargetScrub.sas

sas 1940Clean.sas
sas 1940TargetScrub.sas

cd ../0.2.2_Link
sas Link5080New.sas
sas Link7000.sas
sas Link8010New.sas
sas Link0030.sas
sas Link1040.sas

sas Link5070.sas
sas Link8000New.sas
sas Link0020.sas
sas Link1030.sas
sas Link2040.sas

cd ../0.2.3_Export
stata-se -b do Export30YrLinks.do
stata-se -b do Export20YrLinks.do

cd ../../0.3_Unclassified_Occupations
sas UnclassifiedOccModes1900.sas
sas UnclassifiedOccModes1910.sas
sas UnclassifiedOccModes1920.sas
sas UnclassifiedOccModes1930.sas

stata-se -b do ExportUnclassifiedOccModes.do
