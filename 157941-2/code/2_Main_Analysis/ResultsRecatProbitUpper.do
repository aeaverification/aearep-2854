cd "$repfolder/data/analysis"

use FileRecatProbit_1850_1880, clear

_pctile link_prob_upper_50, percentiles(0.5)
replace link_prob_upper_50=r(r1) if link_prob_upper_50<r(r1)

gen bplalt=1 if bpl_50<=56 //USA
replace bplalt=2 if bpl_50==404 //Norway
replace bplalt=3 if bpl_50>=410 & bpl_50<=413 //UK
replace bplalt=4 if bpl_50==414 //Ireland
replace bplalt=5 if bpl_50==453 //Germany
replace bplalt=6 if bpl_50==434 //Italy
replace bplalt=7 if bpl_50==465 //Russia
replace bplalt=8 if bpl_50==455 //Poland
replace bplalt=9 if bpl_50==405 //Sweden
replace bplalt=10 if bpl_50==452 //Czechoslovakia
replace bplalt=11 if bpl_50==450 //Austria
replace bplalt=12 if bpl_50>56 & bpl_50<. & bplalt==. //Other
label define bplalt_lbl 1 "USA" 2 "Norway" 3 "United Kingdom" 4 "Ireland" 5 "Germany" 6 "Italy" 7 "Russia" 8 "Poland" 9 "Sweden" 10 "Czechoslovakia" 11 "Austria"
label values bplalt bplalt_lbl

capture texdoc close
texdoc init "$repfolder/results/TableA4c", replace force

tex \begin{threeparttable}
tex \newcolumntype{d}{D{.}{.}{3}} 
tex \begin{tabular}{lddddd}
tex \hline
tex \addlinespace[2pt]
tex & \multicolumn{1}{c}{(1)} & \multicolumn{1}{c}{(2)} & \multicolumn{1}{c}{(3)} & \multicolumn{1}{c}{(4)} & \multicolumn{1}{c}{(5)} \\
tex & \multicolumn{1}{c}{Avg Rank} & \multicolumn{1}{c}{log(Occ.\ Wealth)} & \multicolumn{1}{c}{log(PH Score)} & \multicolumn{1}{c}{Occ.\ Wealth Rank} & \multicolumn{1}{c}{PH Rank} \\
tex \hline
tex \addlinespace[2pt]

reg avg_rank_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if avg_rank_upper_50~=. & avg_rank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est50
reg avg_rank_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_upper_50~=. & avg_rank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est80
suest est50 est80, vce(robust)
lincom _b[est50_mean:foreign_50]
	global b1: di %4.3f r(estimate)
	global s1: di %4.3f r(se)
test [est50_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global b1="$b1^a"
	}
	else if `pval'<=.05 {
		global b1="$b1^b"
	}
	else if `pval'<=.1 {
		global b1="$b1^c"
	}
	else {
		global b1="$b1"
	}
lincom _b[est80_mean:foreign_50]
	global d1: di %4.3f r(estimate)
	global r1: di %4.3f r(se)
test [est80_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global d1="$d1^a"
	}
	else if `pval'<=.05 {
		global d1="$d1^b"
	}
	else if `pval'<=.1 {
		global d1="$d1^c"
	}
	else {
		global d1="$d1"
	}
lincom _b[est80_mean:foreign_50]-_b[est50_mean:foreign_50]
	global diff1: di %4.3f r(estimate)
	global sdiff1: di %4.3f r(se)
test [est80_mean]foreign_50-[est50_mean]foreign_50=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff1="$diff1^a"
	}
	else if `pval'<=.05 {
		global diff1="$diff1^b"
	}
	else if `pval'<=.1 {
		global diff1="$diff1^c"
	}
	else {
		global diff1="$diff1"
	}
	
reg lntotprop_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if lntotprop_upper_50~=. & lntotprop_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est50
reg lntotprop_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if lntotprop_upper_50~=. & lntotprop_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est80
suest est50 est80, vce(robust)
lincom _b[est50_mean:foreign_50]
	global b2: di %4.3f r(estimate)
	global s2: di %4.3f r(se)
test [est50_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global b2="$b2^a"
	}
	else if `pval'<=.05 {
		global b2="$b2^b"
	}
	else if `pval'<=.1 {
		global b2="$b2^c"
	}
	else {
		global b2="$b2"
	}
lincom _b[est80_mean:foreign_50]
	global d2: di %4.3f r(estimate)
	global r2: di %4.3f r(se)
test [est80_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global d2="$d2^a"
	}
	else if `pval'<=.05 {
		global d2="$d2^b"
	}
	else if `pval'<=.1 {
		global d2="$d2^c"
	}
	else {
		global d2="$d2"
	}
lincom _b[est80_mean:foreign_50]-_b[est50_mean:foreign_50]
	global diff2: di %4.3f r(estimate)
	global sdiff2: di %4.3f r(se)
test [est80_mean]foreign_50-[est50_mean]foreign_50=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff2="$diff2^a"
	}
	else if `pval'<=.05 {
		global diff2="$diff2^b"
	}
	else if `pval'<=.1 {
		global diff2="$diff2^c"
	}
	else {
		global diff2="$diff2"
	}
	
reg l_inc1900_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if l_inc1900_upper_50~=. & l_inc1900_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est50
reg l_inc1900_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if l_inc1900_upper_50~=. & l_inc1900_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est80
suest est50 est80, vce(robust)
lincom _b[est50_mean:foreign_50]
	global b3: di %4.3f r(estimate)
	global s3: di %4.3f r(se)
test [est50_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global b3="$b3^a"
	}
	else if `pval'<=.05 {
		global b3="$b3^b"
	}
	else if `pval'<=.1 {
		global b3="$b3^c"
	}
	else {
		global b3="$b3"
	}
lincom _b[est80_mean:foreign_50]
	global d3: di %4.3f r(estimate)
	global r3: di %4.3f r(se)
test [est80_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global d3="$d3^a"
	}
	else if `pval'<=.05 {
		global d3="$d3^b"
	}
	else if `pval'<=.1 {
		global d3="$d3^c"
	}
	else {
		global d3="$d3"
	}
lincom _b[est80_mean:foreign_50]-_b[est50_mean:foreign_50]
	global diff3: di %4.3f r(estimate)
	global sdiff3: di %4.3f r(se)
test [est80_mean]foreign_50-[est50_mean]foreign_50=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff3="$diff3^a"
	}
	else if `pval'<=.05 {
		global diff3="$diff3^b"
	}
	else if `pval'<=.1 {
		global diff3="$diff3^c"
	}
	else {
		global diff3="$diff3"
	}
	
reg proprank_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if proprank_upper_50~=. & proprank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est50
reg proprank_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if proprank_upper_50~=. & proprank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est80
suest est50 est80, vce(robust)
lincom _b[est50_mean:foreign_50]
	global b4: di %4.3f r(estimate)
	global s4: di %4.3f r(se)
test [est50_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global b4="$b4^a"
	}
	else if `pval'<=.05 {
		global b4="$b4^b"
	}
	else if `pval'<=.1 {
		global b4="$b4^c"
	}
	else {
		global b4="$b4"
	}
lincom _b[est80_mean:foreign_50]
	global d4: di %4.3f r(estimate)
	global r4: di %4.3f r(se)
test [est80_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global d4="$d4^a"
	}
	else if `pval'<=.05 {
		global d4="$d4^b"
	}
	else if `pval'<=.1 {
		global d4="$d4^c"
	}
	else {
		global d4="$d4"
	}
lincom _b[est80_mean:foreign_50]-_b[est50_mean:foreign_50]
	global diff4: di %4.3f r(estimate)
	global sdiff4: di %4.3f r(se)
test [est80_mean]foreign_50-[est50_mean]foreign_50=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff4="$diff4^a"
	}
	else if `pval'<=.05 {
		global diff4="$diff4^b"
	}
	else if `pval'<=.1 {
		global diff4="$diff4^c"
	}
	else {
		global diff4="$diff4"
	}
	
reg phrank_upper_50 foreign_50 c.age_50##c.age_50##c.age_50##c.age_50 if phrank_upper_50~=. & phrank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est50
reg phrank_upper_80 foreign_50 c.age_80##c.age_80##c.age_80##c.age_80 if phrank_upper_50~=. & phrank_upper_80~=. [aw=1/link_prob_upper_50]
	est sto est80
suest est50 est80, vce(robust)
lincom _b[est50_mean:foreign_50]
	global b5: di %4.3f r(estimate)
	global s5: di %4.3f r(se)
test [est50_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global b5="$b5^a"
	}
	else if `pval'<=.05 {
		global b5="$b5^b"
	}
	else if `pval'<=.1 {
		global b5="$b5^c"
	}
	else {
		global b5="$b5"
	}
lincom _b[est80_mean:foreign_50]
	global d5: di %4.3f r(estimate)
	global r5: di %4.3f r(se)
test [est80_mean]foreign_50
	local pval=r(p)
	if `pval'<=.01 {
		global d5="$d5^a"
	}
	else if `pval'<=.05 {
		global d5="$d5^b"
	}
	else if `pval'<=.1 {
		global d5="$d5^c"
	}
	else {
		global d5="$d5"
	}
lincom _b[est80_mean:foreign_50]-_b[est50_mean:foreign_50]
	global diff5: di %4.3f r(estimate)
	global sdiff5: di %4.3f r(se)
test [est80_mean]foreign_50-[est50_mean]foreign_50=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff5="$diff5^a"
	}
	else if `pval'<=.05 {
		global diff5="$diff5^b"
	}
	else if `pval'<=.1 {
		global diff5="$diff5^c"
	}
	else {
		global diff5="$diff5"
	}
	
tex \multicolumn{6}{l}{\emph{Panel A: 1850--1880}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial & $b1 & $b2 & $b3 & $b4 & $b5 \\
tex & ($s1) & ($s2) & ($s3) & ($s4) & ($s5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final & $d1 & $d2 & $d3 & $d4 & $d5 \\
tex & ($r1) & ($r2) & ($r3) & ($r4) & ($r5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Difference & $diff1 & $diff2 & $diff3 & $diff4 & $diff5 \\
tex & ($sdiff1) & ($sdiff2) & ($sdiff3) & ($sdiff4) & ($sdiff5) \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

texdoc close

//////////////////////////////

use File_1870_1900, clear

_pctile link_prob_upper_00, percentiles(0.5)
replace link_prob_upper_00=r(r1) if link_prob_upper_00<r(r1)

gen bplalt=1 if bpl_70<=56 //USA
replace bplalt=2 if bpl_70==404 //Norway
replace bplalt=3 if bpl_70>=410 & bpl_70<=413 //UK
replace bplalt=4 if bpl_70==414 //Ireland
replace bplalt=5 if bpl_70==453 //Germany
replace bplalt=6 if bpl_70==434 //Italy
replace bplalt=7 if bpl_70==465 //Russia
replace bplalt=8 if bpl_70==455 //Poland
replace bplalt=9 if bpl_70==405 //Sweden
replace bplalt=10 if bpl_70==452 //Czechoslovakia
replace bplalt=11 if bpl_70==450 //Austria
replace bplalt=12 if bpl_70>56 & bpl_70<. & bplalt==. //Other
label define bplalt_lbl 1 "USA" 2 "Norway" 3 "United Kingdom" 4 "Ireland" 5 "Germany" 6 "Italy" 7 "Russia" 8 "Poland" 9 "Sweden" 10 "Czechoslovakia" 11 "Austria"
label values bplalt bplalt_lbl

capture texdoc close
texdoc init "$repfolder/results/TableA4c", append force

reg avg_rank_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if avg_rank_upper_70~=. & avg_rank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est70
reg avg_rank_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_upper_70~=. & avg_rank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est00
suest est70 est00, vce(robust)
lincom _b[est70_mean:foreign_00]
	global b1: di %4.3f r(estimate)
	global s1: di %4.3f r(se)
test [est70_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global b1="$b1^a"
	}
	else if `pval'<=.05 {
		global b1="$b1^b"
	}
	else if `pval'<=.1 {
		global b1="$b1^c"
	}
	else {
		global b1="$b1"
	}
lincom _b[est00_mean:foreign_00]
	global d1: di %4.3f r(estimate)
	global r1: di %4.3f r(se)
test [est00_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global d1="$d1^a"
	}
	else if `pval'<=.05 {
		global d1="$d1^b"
	}
	else if `pval'<=.1 {
		global d1="$d1^c"
	}
	else {
		global d1="$d1"
	}
lincom _b[est00_mean:foreign_00]-_b[est70_mean:foreign_00]
	global diff1: di %4.3f r(estimate)
	global sdiff1: di %4.3f r(se)
test [est00_mean]foreign_00-[est70_mean]foreign_00=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff1="$diff1^a"
	}
	else if `pval'<=.05 {
		global diff1="$diff1^b"
	}
	else if `pval'<=.1 {
		global diff1="$diff1^c"
	}
	else {
		global diff1="$diff1"
	}
	
reg lntotprop_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if lntotprop_upper_70~=. & lntotprop_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est70
reg lntotprop_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if lntotprop_upper_70~=. & lntotprop_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est00
suest est70 est00, vce(robust)
lincom _b[est70_mean:foreign_00]
	global b2: di %4.3f r(estimate)
	global s2: di %4.3f r(se)
test [est70_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global b2="$b2^a"
	}
	else if `pval'<=.05 {
		global b2="$b2^b"
	}
	else if `pval'<=.1 {
		global b2="$b2^c"
	}
	else {
		global b2="$b2"
	}
lincom _b[est00_mean:foreign_00]
	global d2: di %4.3f r(estimate)
	global r2: di %4.3f r(se)
test [est00_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global d2="$d2^a"
	}
	else if `pval'<=.05 {
		global d2="$d2^b"
	}
	else if `pval'<=.1 {
		global d2="$d2^c"
	}
	else {
		global d2="$d2"
	}
lincom _b[est00_mean:foreign_00]-_b[est70_mean:foreign_00]
	global diff2: di %4.3f r(estimate)
	global sdiff2: di %4.3f r(se)
test [est00_mean]foreign_00-[est70_mean]foreign_00=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff2="$diff2^a"
	}
	else if `pval'<=.05 {
		global diff2="$diff2^b"
	}
	else if `pval'<=.1 {
		global diff2="$diff2^c"
	}
	else {
		global diff2="$diff2"
	}
	
reg l_inc1900_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if l_inc1900_upper_70~=. & l_inc1900_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est70
reg l_inc1900_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if l_inc1900_upper_70~=. & l_inc1900_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est00
suest est70 est00, vce(robust)
lincom _b[est70_mean:foreign_00]
	global b3: di %4.3f r(estimate)
	global s3: di %4.3f r(se)
test [est70_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global b3="$b3^a"
	}
	else if `pval'<=.05 {
		global b3="$b3^b"
	}
	else if `pval'<=.1 {
		global b3="$b3^c"
	}
	else {
		global b3="$b3"
	}
lincom _b[est00_mean:foreign_00]
	global d3: di %4.3f r(estimate)
	global r3: di %4.3f r(se)
test [est00_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global d3="$d3^a"
	}
	else if `pval'<=.05 {
		global d3="$d3^b"
	}
	else if `pval'<=.1 {
		global d3="$d3^c"
	}
	else {
		global d3="$d3"
	}
lincom _b[est00_mean:foreign_00]-_b[est70_mean:foreign_00]
	global diff3: di %4.3f r(estimate)
	global sdiff3: di %4.3f r(se)
test [est00_mean]foreign_00-[est70_mean]foreign_00=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff3="$diff3^a"
	}
	else if `pval'<=.05 {
		global diff3="$diff3^b"
	}
	else if `pval'<=.1 {
		global diff3="$diff3^c"
	}
	else {
		global diff3="$diff3"
	}
	
reg proprank_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if proprank_upper_70~=. & proprank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est70
reg proprank_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if proprank_upper_70~=. & proprank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est00
suest est70 est00, vce(robust)
lincom _b[est70_mean:foreign_00]
	global b4: di %4.3f r(estimate)
	global s4: di %4.3f r(se)
test [est70_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global b4="$b4^a"
	}
	else if `pval'<=.05 {
		global b4="$b4^b"
	}
	else if `pval'<=.1 {
		global b4="$b4^c"
	}
	else {
		global b4="$b4"
	}
lincom _b[est00_mean:foreign_00]
	global d4: di %4.3f r(estimate)
	global r4: di %4.3f r(se)
test [est00_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global d4="$d4^a"
	}
	else if `pval'<=.05 {
		global d4="$d4^b"
	}
	else if `pval'<=.1 {
		global d4="$d4^c"
	}
	else {
		global d4="$d4"
	}
lincom _b[est00_mean:foreign_00]-_b[est70_mean:foreign_00]
	global diff4: di %4.3f r(estimate)
	global sdiff4: di %4.3f r(se)
test [est00_mean]foreign_00-[est70_mean]foreign_00=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff4="$diff4^a"
	}
	else if `pval'<=.05 {
		global diff4="$diff4^b"
	}
	else if `pval'<=.1 {
		global diff4="$diff4^c"
	}
	else {
		global diff4="$diff4"
	}
	
reg phrank_upper_70 foreign_00 c.age_70##c.age_70##c.age_70##c.age_70 if phrank_upper_70~=. & phrank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est70
reg phrank_upper_00 foreign_00 c.age_00##c.age_00##c.age_00##c.age_00 if phrank_upper_70~=. & phrank_upper_00~=. [aw=1/link_prob_upper_00]
	est sto est00
suest est70 est00, vce(robust)
lincom _b[est70_mean:foreign_00]
	global b5: di %4.3f r(estimate)
	global s5: di %4.3f r(se)
test [est70_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global b5="$b5^a"
	}
	else if `pval'<=.05 {
		global b5="$b5^b"
	}
	else if `pval'<=.1 {
		global b5="$b5^c"
	}
	else {
		global b5="$b5"
	}
lincom _b[est00_mean:foreign_00]
	global d5: di %4.3f r(estimate)
	global r5: di %4.3f r(se)
test [est00_mean]foreign_00
	local pval=r(p)
	if `pval'<=.01 {
		global d5="$d5^a"
	}
	else if `pval'<=.05 {
		global d5="$d5^b"
	}
	else if `pval'<=.1 {
		global d5="$d5^c"
	}
	else {
		global d5="$d5"
	}
lincom _b[est00_mean:foreign_00]-_b[est70_mean:foreign_00]
	global diff5: di %4.3f r(estimate)
	global sdiff5: di %4.3f r(se)
test [est00_mean]foreign_00-[est70_mean]foreign_00=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff5="$diff5^a"
	}
	else if `pval'<=.05 {
		global diff5="$diff5^b"
	}
	else if `pval'<=.1 {
		global diff5="$diff5^c"
	}
	else {
		global diff5="$diff5"
	}
	
tex \multicolumn{6}{l}{\emph{Panel B: 1870--1900}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial & $b1 & $b2 & $b3 & $b4 & $b5 \\
tex & ($s1) & ($s2) & ($s3) & ($s4) & ($s5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final & $d1 & $d2 & $d3 & $d4 & $d5 \\
tex & ($r1) & ($r2) & ($r3) & ($r4) & ($r5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Difference & $diff1 & $diff2 & $diff3 & $diff4 & $diff5 \\
tex & ($sdiff1) & ($sdiff2) & ($sdiff3) & ($sdiff4) & ($sdiff5) \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

texdoc close

//////////////////////////////

use File_1880_1910, clear

_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)

gen bplalt=1 if bpl_80<=56 //USA
replace bplalt=2 if bpl_80==404 //Norway
replace bplalt=3 if bpl_80>=410 & bpl_80<=413 //UK
replace bplalt=4 if bpl_80==414 //Ireland
replace bplalt=5 if bpl_80==453 //Germany
replace bplalt=6 if bpl_80==434 //Italy
replace bplalt=7 if bpl_80==465 //Russia
replace bplalt=8 if bpl_80==455 //Poland
replace bplalt=9 if bpl_80==405 //Sweden
replace bplalt=10 if bpl_80==452 //Czechoslovakia
replace bplalt=11 if bpl_80==450 //Austria
replace bplalt=12 if bpl_80>56 & bpl_80<. & bplalt==. //Other
label define bplalt_lbl 1 "USA" 2 "Norway" 3 "United Kingdom" 4 "Ireland" 5 "Germany" 6 "Italy" 7 "Russia" 8 "Poland" 9 "Sweden" 10 "Czechoslovakia" 11 "Austria"
label values bplalt bplalt_lbl

capture texdoc close
texdoc init "$repfolder/results/TableA4c", append force

reg avg_rank_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if avg_rank_upper_80~=. & avg_rank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est80
reg avg_rank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_upper_80~=. & avg_rank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est10
suest est80 est10, vce(robust)
lincom _b[est80_mean:foreign_10]
	global b1: di %4.3f r(estimate)
	global s1: di %4.3f r(se)
test [est80_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b1="$b1^a"
	}
	else if `pval'<=.05 {
		global b1="$b1^b"
	}
	else if `pval'<=.1 {
		global b1="$b1^c"
	}
	else {
		global b1="$b1"
	}
lincom _b[est10_mean:foreign_10]
	global d1: di %4.3f r(estimate)
	global r1: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d1="$d1^a"
	}
	else if `pval'<=.05 {
		global d1="$d1^b"
	}
	else if `pval'<=.1 {
		global d1="$d1^c"
	}
	else {
		global d1="$d1"
	}
lincom _b[est10_mean:foreign_10]-_b[est80_mean:foreign_10]
	global diff1: di %4.3f r(estimate)
	global sdiff1: di %4.3f r(se)
test [est10_mean]foreign_10-[est80_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff1="$diff1^a"
	}
	else if `pval'<=.05 {
		global diff1="$diff1^b"
	}
	else if `pval'<=.1 {
		global diff1="$diff1^c"
	}
	else {
		global diff1="$diff1"
	}
	
reg lntotprop_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if lntotprop_upper_80~=. & lntotprop_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est80
reg lntotprop_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if lntotprop_upper_80~=. & lntotprop_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est10
suest est80 est10, vce(robust)
lincom _b[est80_mean:foreign_10]
	global b2: di %4.3f r(estimate)
	global s2: di %4.3f r(se)
test [est80_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b2="$b2^a"
	}
	else if `pval'<=.05 {
		global b2="$b2^b"
	}
	else if `pval'<=.1 {
		global b2="$b2^c"
	}
	else {
		global b2="$b2"
	}
lincom _b[est10_mean:foreign_10]
	global d2: di %4.3f r(estimate)
	global r2: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d2="$d2^a"
	}
	else if `pval'<=.05 {
		global d2="$d2^b"
	}
	else if `pval'<=.1 {
		global d2="$d2^c"
	}
	else {
		global d2="$d2"
	}
lincom _b[est10_mean:foreign_10]-_b[est80_mean:foreign_10]
	global diff2: di %4.3f r(estimate)
	global sdiff2: di %4.3f r(se)
test [est10_mean]foreign_10-[est80_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff2="$diff2^a"
	}
	else if `pval'<=.05 {
		global diff2="$diff2^b"
	}
	else if `pval'<=.1 {
		global diff2="$diff2^c"
	}
	else {
		global diff2="$diff2"
	}
	
reg l_inc1900_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if l_inc1900_upper_80~=. & l_inc1900_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est80
reg l_inc1900_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if l_inc1900_upper_80~=. & l_inc1900_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est10
suest est80 est10, vce(robust)
lincom _b[est80_mean:foreign_10]
	global b3: di %4.3f r(estimate)
	global s3: di %4.3f r(se)
test [est80_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b3="$b3^a"
	}
	else if `pval'<=.05 {
		global b3="$b3^b"
	}
	else if `pval'<=.1 {
		global b3="$b3^c"
	}
	else {
		global b3="$b3"
	}
lincom _b[est10_mean:foreign_10]
	global d3: di %4.3f r(estimate)
	global r3: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d3="$d3^a"
	}
	else if `pval'<=.05 {
		global d3="$d3^b"
	}
	else if `pval'<=.1 {
		global d3="$d3^c"
	}
	else {
		global d3="$d3"
	}
lincom _b[est10_mean:foreign_10]-_b[est80_mean:foreign_10]
	global diff3: di %4.3f r(estimate)
	global sdiff3: di %4.3f r(se)
test [est10_mean]foreign_10-[est80_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff3="$diff3^a"
	}
	else if `pval'<=.05 {
		global diff3="$diff3^b"
	}
	else if `pval'<=.1 {
		global diff3="$diff3^c"
	}
	else {
		global diff3="$diff3"
	}
	
reg proprank_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if proprank_upper_80~=. & proprank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est80
reg proprank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if proprank_upper_80~=. & proprank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est10
suest est80 est10, vce(robust)
lincom _b[est80_mean:foreign_10]
	global b4: di %4.3f r(estimate)
	global s4: di %4.3f r(se)
test [est80_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b4="$b4^a"
	}
	else if `pval'<=.05 {
		global b4="$b4^b"
	}
	else if `pval'<=.1 {
		global b4="$b4^c"
	}
	else {
		global b4="$b4"
	}
lincom _b[est10_mean:foreign_10]
	global d4: di %4.3f r(estimate)
	global r4: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d4="$d4^a"
	}
	else if `pval'<=.05 {
		global d4="$d4^b"
	}
	else if `pval'<=.1 {
		global d4="$d4^c"
	}
	else {
		global d4="$d4"
	}
lincom _b[est10_mean:foreign_10]-_b[est80_mean:foreign_10]
	global diff4: di %4.3f r(estimate)
	global sdiff4: di %4.3f r(se)
test [est10_mean]foreign_10-[est80_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff4="$diff4^a"
	}
	else if `pval'<=.05 {
		global diff4="$diff4^b"
	}
	else if `pval'<=.1 {
		global diff4="$diff4^c"
	}
	else {
		global diff4="$diff4"
	}
	
reg phrank_upper_80 foreign_10 c.age_80##c.age_80##c.age_80##c.age_80 if phrank_upper_80~=. & phrank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est80
reg phrank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if phrank_upper_80~=. & phrank_upper_10~=. [aw=1/link_prob_upper_10]
	est sto est10
suest est80 est10, vce(robust)
lincom _b[est80_mean:foreign_10]
	global b5: di %4.3f r(estimate)
	global s5: di %4.3f r(se)
test [est80_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b5="$b5^a"
	}
	else if `pval'<=.05 {
		global b5="$b5^b"
	}
	else if `pval'<=.1 {
		global b5="$b5^c"
	}
	else {
		global b5="$b5"
	}
lincom _b[est10_mean:foreign_10]
	global d5: di %4.3f r(estimate)
	global r5: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d5="$d5^a"
	}
	else if `pval'<=.05 {
		global d5="$d5^b"
	}
	else if `pval'<=.1 {
		global d5="$d5^c"
	}
	else {
		global d5="$d5"
	}
lincom _b[est10_mean:foreign_10]-_b[est80_mean:foreign_10]
	global diff5: di %4.3f r(estimate)
	global sdiff5: di %4.3f r(se)
test [est10_mean]foreign_10-[est80_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff5="$diff5^a"
	}
	else if `pval'<=.05 {
		global diff5="$diff5^b"
	}
	else if `pval'<=.1 {
		global diff5="$diff5^c"
	}
	else {
		global diff5="$diff5"
	}
	
tex \multicolumn{6}{l}{\emph{Panel C: 1880--1910}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial & $b1 & $b2 & $b3 & $b4 & $b5 \\
tex & ($s1) & ($s2) & ($s3) & ($s4) & ($s5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final & $d1 & $d2 & $d3 & $d4 & $d5 \\
tex & ($r1) & ($r2) & ($r3) & ($r4) & ($r5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Difference & $diff1 & $diff2 & $diff3 & $diff4 & $diff5 \\
tex & ($sdiff1) & ($sdiff2) & ($sdiff3) & ($sdiff4) & ($sdiff5) \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

texdoc close

//////////////////////////////

use File_1900_1930, clear

_pctile link_prob_upper_30, percentiles(0.5)
replace link_prob_upper_30=r(r1) if link_prob_upper_30<r(r1)

gen bplalt=1 if bpl_00<=56 //USA
replace bplalt=2 if bpl_00==404 //Norway
replace bplalt=3 if bpl_00>=410 & bpl_00<=413 //UK
replace bplalt=4 if bpl_00==414 //Ireland
replace bplalt=5 if bpl_00==453 //Germany
replace bplalt=6 if bpl_00==434 //Italy
replace bplalt=7 if bpl_00==465 //Russia
replace bplalt=8 if bpl_00==455 //Poland
replace bplalt=9 if bpl_00==405 //Sweden
replace bplalt=10 if bpl_00==452 //Czechoslovakia
replace bplalt=11 if bpl_00==450 //Austria
replace bplalt=12 if bpl_00>56 & bpl_00<. & bplalt==. //Other
label define bplalt_lbl 1 "USA" 2 "Norway" 3 "United Kingdom" 4 "Ireland" 5 "Germany" 6 "Italy" 7 "Russia" 8 "Poland" 9 "Sweden" 10 "Czechoslovakia" 11 "Austria"
label values bplalt bplalt_lbl

capture texdoc close
texdoc init "$repfolder/results/TableA4c", append force

reg avg_rank_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if avg_rank_upper_00~=. & avg_rank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est00
reg avg_rank_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if avg_rank_upper_00~=. & avg_rank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est30
suest est00 est30, vce(robust)
lincom _b[est00_mean:foreign_30]
	global b1: di %4.3f r(estimate)
	global s1: di %4.3f r(se)
test [est00_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global b1="$b1^a"
	}
	else if `pval'<=.05 {
		global b1="$b1^b"
	}
	else if `pval'<=.1 {
		global b1="$b1^c"
	}
	else {
		global b1="$b1"
	}
lincom _b[est30_mean:foreign_30]
	global d1: di %4.3f r(estimate)
	global r1: di %4.3f r(se)
test [est30_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global d1="$d1^a"
	}
	else if `pval'<=.05 {
		global d1="$d1^b"
	}
	else if `pval'<=.1 {
		global d1="$d1^c"
	}
	else {
		global d1="$d1"
	}
lincom _b[est30_mean:foreign_30]-_b[est00_mean:foreign_30]
	global diff1: di %4.3f r(estimate)
	global sdiff1: di %4.3f r(se)
test [est30_mean]foreign_30-[est00_mean]foreign_30=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff1="$diff1^a"
	}
	else if `pval'<=.05 {
		global diff1="$diff1^b"
	}
	else if `pval'<=.1 {
		global diff1="$diff1^c"
	}
	else {
		global diff1="$diff1"
	}
	
reg lntotprop_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if lntotprop_upper_00~=. & lntotprop_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est00
reg lntotprop_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if lntotprop_upper_00~=. & lntotprop_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est30
suest est00 est30, vce(robust)
lincom _b[est00_mean:foreign_30]
	global b2: di %4.3f r(estimate)
	global s2: di %4.3f r(se)
test [est00_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global b2="$b2^a"
	}
	else if `pval'<=.05 {
		global b2="$b2^b"
	}
	else if `pval'<=.1 {
		global b2="$b2^c"
	}
	else {
		global b2="$b2"
	}
lincom _b[est30_mean:foreign_30]
	global d2: di %4.3f r(estimate)
	global r2: di %4.3f r(se)
test [est30_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global d2="$d2^a"
	}
	else if `pval'<=.05 {
		global d2="$d2^b"
	}
	else if `pval'<=.1 {
		global d2="$d2^c"
	}
	else {
		global d2="$d2"
	}
lincom _b[est30_mean:foreign_30]-_b[est00_mean:foreign_30]
	global diff2: di %4.3f r(estimate)
	global sdiff2: di %4.3f r(se)
test [est30_mean]foreign_30-[est00_mean]foreign_30=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff2="$diff2^a"
	}
	else if `pval'<=.05 {
		global diff2="$diff2^b"
	}
	else if `pval'<=.1 {
		global diff2="$diff2^c"
	}
	else {
		global diff2="$diff2"
	}
	
reg l_inc1900_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if l_inc1900_upper_00~=. & l_inc1900_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est00
reg l_inc1900_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if l_inc1900_upper_00~=. & l_inc1900_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est30
suest est00 est30, vce(robust)
lincom _b[est00_mean:foreign_30]
	global b3: di %4.3f r(estimate)
	global s3: di %4.3f r(se)
test [est00_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global b3="$b3^a"
	}
	else if `pval'<=.05 {
		global b3="$b3^b"
	}
	else if `pval'<=.1 {
		global b3="$b3^c"
	}
	else {
		global b3="$b3"
	}
lincom _b[est30_mean:foreign_30]
	global d3: di %4.3f r(estimate)
	global r3: di %4.3f r(se)
test [est30_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global d3="$d3^a"
	}
	else if `pval'<=.05 {
		global d3="$d3^b"
	}
	else if `pval'<=.1 {
		global d3="$d3^c"
	}
	else {
		global d3="$d3"
	}
lincom _b[est30_mean:foreign_30]-_b[est00_mean:foreign_30]
	global diff3: di %4.3f r(estimate)
	global sdiff3: di %4.3f r(se)
test [est30_mean]foreign_30-[est00_mean]foreign_30=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff3="$diff3^a"
	}
	else if `pval'<=.05 {
		global diff3="$diff3^b"
	}
	else if `pval'<=.1 {
		global diff3="$diff3^c"
	}
	else {
		global diff3="$diff3"
	}
	
reg proprank_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if proprank_upper_00~=. & proprank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est00
reg proprank_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if proprank_upper_00~=. & proprank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est30
suest est00 est30, vce(robust)
lincom _b[est00_mean:foreign_30]
	global b4: di %4.3f r(estimate)
	global s4: di %4.3f r(se)
test [est00_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global b4="$b4^a"
	}
	else if `pval'<=.05 {
		global b4="$b4^b"
	}
	else if `pval'<=.1 {
		global b4="$b4^c"
	}
	else {
		global b4="$b4"
	}
lincom _b[est30_mean:foreign_30]
	global d4: di %4.3f r(estimate)
	global r4: di %4.3f r(se)
test [est30_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global d4="$d4^a"
	}
	else if `pval'<=.05 {
		global d4="$d4^b"
	}
	else if `pval'<=.1 {
		global d4="$d4^c"
	}
	else {
		global d4="$d4"
	}
lincom _b[est30_mean:foreign_30]-_b[est00_mean:foreign_30]
	global diff4: di %4.3f r(estimate)
	global sdiff4: di %4.3f r(se)
test [est30_mean]foreign_30-[est00_mean]foreign_30=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff4="$diff4^a"
	}
	else if `pval'<=.05 {
		global diff4="$diff4^b"
	}
	else if `pval'<=.1 {
		global diff4="$diff4^c"
	}
	else {
		global diff4="$diff4"
	}
	
reg phrank_upper_00 foreign_30 c.age_00##c.age_00##c.age_00##c.age_00 if phrank_upper_00~=. & phrank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est00
reg phrank_upper_30 foreign_30 c.age_30##c.age_30##c.age_30##c.age_30 if phrank_upper_00~=. & phrank_upper_30~=. [aw=1/link_prob_upper_30]
	est sto est30
suest est00 est30, vce(robust)
lincom _b[est00_mean:foreign_30]
	global b5: di %4.3f r(estimate)
	global s5: di %4.3f r(se)
test [est00_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global b5="$b5^a"
	}
	else if `pval'<=.05 {
		global b5="$b5^b"
	}
	else if `pval'<=.1 {
		global b5="$b5^c"
	}
	else {
		global b5="$b5"
	}
lincom _b[est30_mean:foreign_30]
	global d5: di %4.3f r(estimate)
	global r5: di %4.3f r(se)
test [est30_mean]foreign_30
	local pval=r(p)
	if `pval'<=.01 {
		global d5="$d5^a"
	}
	else if `pval'<=.05 {
		global d5="$d5^b"
	}
	else if `pval'<=.1 {
		global d5="$d5^c"
	}
	else {
		global d5="$d5"
	}
lincom _b[est30_mean:foreign_30]-_b[est00_mean:foreign_30]
	global diff5: di %4.3f r(estimate)
	global sdiff5: di %4.3f r(se)
test [est30_mean]foreign_30-[est00_mean]foreign_30=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff5="$diff5^a"
	}
	else if `pval'<=.05 {
		global diff5="$diff5^b"
	}
	else if `pval'<=.1 {
		global diff5="$diff5^c"
	}
	else {
		global diff5="$diff5"
	}
	
tex \multicolumn{6}{l}{\emph{Panel D: 1900--1930}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial & $b1 & $b2 & $b3 & $b4 & $b5 \\
tex & ($s1) & ($s2) & ($s3) & ($s4) & ($s5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final & $d1 & $d2 & $d3 & $d4 & $d5 \\
tex & ($r1) & ($r2) & ($r3) & ($r4) & ($r5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Difference & $diff1 & $diff2 & $diff3 & $diff4 & $diff5 \\
tex & ($sdiff1) & ($sdiff2) & ($sdiff3) & ($sdiff4) & ($sdiff5) \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

texdoc close

//////////////////////////////

use File_1910_1940, clear

_pctile link_prob_upper_10, percentiles(0.5)
replace link_prob_upper_10=r(r1) if link_prob_upper_10<r(r1)

gen bplalt=1 if bpl_10<=56 //USA
replace bplalt=2 if bpl_10==404 //Norway
replace bplalt=3 if bpl_10>=410 & bpl_10<=413 //UK
replace bplalt=4 if bpl_10==414 //Ireland
replace bplalt=5 if bpl_10==453 //Germany
replace bplalt=6 if bpl_10==434 //Italy
replace bplalt=7 if bpl_10==465 //Russia
replace bplalt=8 if bpl_10==455 //Poland
replace bplalt=9 if bpl_10==405 //Sweden
replace bplalt=10 if bpl_10==452 //Czechoslovakia
replace bplalt=11 if bpl_10==450 //Austria
replace bplalt=12 if bpl_10>56 & bpl_10<. & bplalt==. //Other
label define bplalt_lbl 1 "USA" 2 "Norway" 3 "United Kingdom" 4 "Ireland" 5 "Germany" 6 "Italy" 7 "Russia" 8 "Poland" 9 "Sweden" 10 "Czechoslovakia" 11 "Austria"
label values bplalt bplalt_lbl

capture texdoc close
texdoc init "$repfolder/results/TableA4c", append force

reg avg_rank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if avg_rank_upper_10~=. & avg_rank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est10
reg avg_rank_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if avg_rank_upper_10~=. & avg_rank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est40
suest est10 est40, vce(robust)
lincom _b[est10_mean:foreign_10]
	global b1: di %4.3f r(estimate)
	global s1: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b1="$b1^a"
	}
	else if `pval'<=.05 {
		global b1="$b1^b"
	}
	else if `pval'<=.1 {
		global b1="$b1^c"
	}
	else {
		global b1="$b1"
	}
lincom _b[est40_mean:foreign_10]
	global d1: di %4.3f r(estimate)
	global r1: di %4.3f r(se)
test [est40_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d1="$d1^a"
	}
	else if `pval'<=.05 {
		global d1="$d1^b"
	}
	else if `pval'<=.1 {
		global d1="$d1^c"
	}
	else {
		global d1="$d1"
	}
lincom _b[est40_mean:foreign_10]-_b[est10_mean:foreign_10]
	global diff1: di %4.3f r(estimate)
	global sdiff1: di %4.3f r(se)
test [est40_mean]foreign_10-[est10_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff1="$diff1^a"
	}
	else if `pval'<=.05 {
		global diff1="$diff1^b"
	}
	else if `pval'<=.1 {
		global diff1="$diff1^c"
	}
	else {
		global diff1="$diff1"
	}
	
reg lntotprop_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if lntotprop_upper_10~=. & lntotprop_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est10
reg lntotprop_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if lntotprop_upper_10~=. & lntotprop_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est40
suest est10 est40, vce(robust)
lincom _b[est10_mean:foreign_10]
	global b2: di %4.3f r(estimate)
	global s2: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b2="$b2^a"
	}
	else if `pval'<=.05 {
		global b2="$b2^b"
	}
	else if `pval'<=.1 {
		global b2="$b2^c"
	}
	else {
		global b2="$b2"
	}
lincom _b[est40_mean:foreign_10]
	global d2: di %4.3f r(estimate)
	global r2: di %4.3f r(se)
test [est40_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d2="$d2^a"
	}
	else if `pval'<=.05 {
		global d2="$d2^b"
	}
	else if `pval'<=.1 {
		global d2="$d2^c"
	}
	else {
		global d2="$d2"
	}
lincom _b[est40_mean:foreign_10]-_b[est10_mean:foreign_10]
	global diff2: di %4.3f r(estimate)
	global sdiff2: di %4.3f r(se)
test [est40_mean]foreign_10-[est10_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff2="$diff2^a"
	}
	else if `pval'<=.05 {
		global diff2="$diff2^b"
	}
	else if `pval'<=.1 {
		global diff2="$diff2^c"
	}
	else {
		global diff2="$diff2"
	}
	
reg l_inc1900_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if l_inc1900_upper_10~=. & l_inc1900_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est10
reg l_inc1900_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if l_inc1900_upper_10~=. & l_inc1900_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est40
suest est10 est40, vce(robust)
lincom _b[est10_mean:foreign_10]
	global b3: di %4.3f r(estimate)
	global s3: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b3="$b3^a"
	}
	else if `pval'<=.05 {
		global b3="$b3^b"
	}
	else if `pval'<=.1 {
		global b3="$b3^c"
	}
	else {
		global b3="$b3"
	}
lincom _b[est40_mean:foreign_10]
	global d3: di %4.3f r(estimate)
	global r3: di %4.3f r(se)
test [est40_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d3="$d3^a"
	}
	else if `pval'<=.05 {
		global d3="$d3^b"
	}
	else if `pval'<=.1 {
		global d3="$d3^c"
	}
	else {
		global d3="$d3"
	}
lincom _b[est40_mean:foreign_10]-_b[est10_mean:foreign_10]
	global diff3: di %4.3f r(estimate)
	global sdiff3: di %4.3f r(se)
test [est40_mean]foreign_10-[est10_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff3="$diff3^a"
	}
	else if `pval'<=.05 {
		global diff3="$diff3^b"
	}
	else if `pval'<=.1 {
		global diff3="$diff3^c"
	}
	else {
		global diff3="$diff3"
	}
	
reg proprank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if proprank_upper_10~=. & proprank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est10
reg proprank_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if proprank_upper_10~=. & proprank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est40
suest est10 est40, vce(robust)
lincom _b[est10_mean:foreign_10]
	global b4: di %4.3f r(estimate)
	global s4: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b4="$b4^a"
	}
	else if `pval'<=.05 {
		global b4="$b4^b"
	}
	else if `pval'<=.1 {
		global b4="$b4^c"
	}
	else {
		global b4="$b4"
	}
lincom _b[est40_mean:foreign_10]
	global d4: di %4.3f r(estimate)
	global r4: di %4.3f r(se)
test [est40_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d4="$d4^a"
	}
	else if `pval'<=.05 {
		global d4="$d4^b"
	}
	else if `pval'<=.1 {
		global d4="$d4^c"
	}
	else {
		global d4="$d4"
	}
lincom _b[est40_mean:foreign_10]-_b[est10_mean:foreign_10]
	global diff4: di %4.3f r(estimate)
	global sdiff4: di %4.3f r(se)
test [est40_mean]foreign_10-[est10_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff4="$diff4^a"
	}
	else if `pval'<=.05 {
		global diff4="$diff4^b"
	}
	else if `pval'<=.1 {
		global diff4="$diff4^c"
	}
	else {
		global diff4="$diff4"
	}
	
reg phrank_upper_10 foreign_10 c.age_10##c.age_10##c.age_10##c.age_10 if phrank_upper_10~=. & phrank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est10
reg phrank_upper_40 foreign_10 c.age_40##c.age_40##c.age_40##c.age_40 if phrank_upper_10~=. & phrank_upper_40~=. [aw=1/link_prob_upper_10]
	est sto est40
suest est10 est40, vce(robust)
lincom _b[est10_mean:foreign_10]
	global b5: di %4.3f r(estimate)
	global s5: di %4.3f r(se)
test [est10_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global b5="$b5^a"
	}
	else if `pval'<=.05 {
		global b5="$b5^b"
	}
	else if `pval'<=.1 {
		global b5="$b5^c"
	}
	else {
		global b5="$b5"
	}
lincom _b[est40_mean:foreign_10]
	global d5: di %4.3f r(estimate)
	global r5: di %4.3f r(se)
test [est40_mean]foreign_10
	local pval=r(p)
	if `pval'<=.01 {
		global d5="$d5^a"
	}
	else if `pval'<=.05 {
		global d5="$d5^b"
	}
	else if `pval'<=.1 {
		global d5="$d5^c"
	}
	else {
		global d5="$d5"
	}
lincom _b[est40_mean:foreign_10]-_b[est10_mean:foreign_10]
	global diff5: di %4.3f r(estimate)
	global sdiff5: di %4.3f r(se)
test [est40_mean]foreign_10-[est10_mean]foreign_10=0
	local pval=r(p)
	if `pval'<=.01 {
		global diff5="$diff5^a"
	}
	else if `pval'<=.05 {
		global diff5="$diff5^b"
	}
	else if `pval'<=.1 {
		global diff5="$diff5^c"
	}
	else {
		global diff5="$diff5"
	}
	
tex \multicolumn{6}{l}{\emph{Panel E: 1910--1940}} \\
tex \addlinespace[2pt]
tex \hspace*{1em}Initial & $b1 & $b2 & $b3 & $b4 & $b5 \\
tex & ($s1) & ($s2) & ($s3) & ($s4) & ($s5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Final & $d1 & $d2 & $d3 & $d4 & $d5 \\
tex & ($r1) & ($r2) & ($r3) & ($r4) & ($r5) \\
tex \addlinespace[2pt]
tex \hspace*{1em}Difference & $diff1 & $diff2 & $diff3 & $diff4 & $diff5 \\
tex & ($sdiff1) & ($sdiff2) & ($sdiff3) & ($sdiff4) & ($sdiff5) \\
tex \addlinespace[2pt]
tex \hline
tex \addlinespace[2pt]

tex \end{tabular}
tex \end{threeparttable}

texdoc close

/////////////////////////////
